.class abstract Lcom/mediatek/internal/telephony/cat/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/internal/telephony/cat/Channel$UICCServerThread;,
        Lcom/mediatek/internal/telephony/cat/Channel$TcpReceiverThread;,
        Lcom/mediatek/internal/telephony/cat/Channel$UdpReceiverThread;
    }
.end annotation


# static fields
.field protected static final DEFAULT_IOTTEST_VALUE:I

.field protected static final DISABLE_IOTTEST_CONFIG:Ljava/lang/String; = "0"

.field protected static final ENABLE_IOTTEST_CONFIG:Ljava/lang/String; = "1"

.field protected static final PROPERTY_IOT_TEST:Ljava/lang/String; = "persist.service.bip.iot.test"

.field protected static final SOCKET_TIMEOUT:I = 0xbb8


# instance fields
.field protected isChannelOpened:Z

.field protected mAddress:Ljava/net/InetAddress;

.field protected mBipChannelManager:Lcom/mediatek/internal/telephony/cat/BipChannelManager;

.field protected mBipManger:Lcom/mediatek/internal/telephony/cat/BipManager;

.field protected mBufferSize:I

.field protected mChannelId:I

.field protected mChannelStatus:I

.field protected mChannelStatusData:Lcom/mediatek/common/telephony/ChannelStatus;

.field private mHandler:Lcom/android/internal/telephony/cat/CatService;

.field protected mIOTTest:I

.field protected mLinkMode:I

.field protected mLock:Ljava/lang/Object;

.field protected mPort:I

.field protected mProtocolType:I

.field protected mRecvDataRet:Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

.field protected mRxBuffer:[B

.field protected mRxBufferCacheCount:I

.field protected mRxBufferCount:I

.field protected mRxBufferOffset:I

.field private volatile mStop:Z

.field protected mTxBuffer:[B

.field protected mTxBufferCount:I

.field protected needCopy:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "1"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/mediatek/internal/telephony/cat/Channel;->DEFAULT_IOTTEST_VALUE:I

    return-void
.end method

.method constructor <init>(IIILjava/net/InetAddress;IILcom/android/internal/telephony/cat/CatService;Lcom/mediatek/internal/telephony/cat/BipManager;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/net/InetAddress;
    .param p5    # I
    .param p6    # I
    .param p7    # Lcom/android/internal/telephony/cat/CatService;
    .param p8    # Lcom/mediatek/internal/telephony/cat/BipManager;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelId:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelStatus:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mLinkMode:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mAddress:Ljava/net/InetAddress;

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mPort:I

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mHandler:Lcom/android/internal/telephony/cat/CatService;

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBipManger:Lcom/mediatek/internal/telephony/cat/BipManager;

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBipChannelManager:Lcom/mediatek/internal/telephony/cat/BipChannelManager;

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBuffer:[B

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mTxBuffer:[B

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBufferCount:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBufferOffset:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mTxBufferCount:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBufferCacheCount:I

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRecvDataRet:Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->needCopy:I

    iput-boolean v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->isChannelOpened:Z

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelStatusData:Lcom/mediatek/common/telephony/ChannelStatus;

    sget v0, Lcom/mediatek/internal/telephony/cat/Channel;->DEFAULT_IOTTEST_VALUE:I

    iput v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mIOTTest:I

    iput-boolean v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mStop:Z

    iput p1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelId:I

    iput p2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mLinkMode:I

    iput p3, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    iput-object p4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mAddress:Ljava/net/InetAddress;

    iput p5, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mPort:I

    iput p6, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mLock:Ljava/lang/Object;

    iput-object p7, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mHandler:Lcom/android/internal/telephony/cat/CatService;

    iput-object p8, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBipManger:Lcom/mediatek/internal/telephony/cat/BipManager;

    iget-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBipManger:Lcom/mediatek/internal/telephony/cat/BipManager;

    invoke-virtual {v0}, Lcom/mediatek/internal/telephony/cat/BipManager;->getBipChannelManager()Lcom/mediatek/internal/telephony/cat/BipChannelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBipChannelManager:Lcom/mediatek/internal/telephony/cat/BipChannelManager;

    new-instance v0, Lcom/mediatek/common/telephony/ChannelStatus;

    invoke-direct {v0, p1, v1, v1}, Lcom/mediatek/common/telephony/ChannelStatus;-><init>(III)V

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelStatusData:Lcom/mediatek/common/telephony/ChannelStatus;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/internal/telephony/cat/Channel;)Z
    .locals 1
    .param p0    # Lcom/mediatek/internal/telephony/cat/Channel;

    iget-boolean v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mStop:Z

    return v0
.end method


# virtual methods
.method public changeChannelStatus(B)V
    .locals 0
    .param p1    # B

    return-void
.end method

.method protected checkBufferSize()I
    .locals 8

    const/16 v7, 0xd2

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    if-ne v4, v3, :cond_3

    :cond_0
    const/16 v2, 0xff

    const/16 v1, 0x578

    const/16 v0, 0x400

    :cond_1
    :goto_0
    const-string v4, "[BIP]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mBufferSize:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " minBufferSize:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " maxBufferSize:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    if-lt v4, v2, :cond_5

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    if-gt v4, v1, :cond_5

    const-string v3, "[BIP]"

    const-string v4, "buffer size is normal"

    invoke-static {v3, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    :cond_2
    :goto_1
    return v3

    :cond_3
    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mProtocolType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    :cond_4
    const/16 v2, 0xff

    const/16 v1, 0x578

    const/16 v0, 0x400

    goto :goto_0

    :cond_5
    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    if-le v4, v1, :cond_6

    const-string v4, "[BIP]"

    const-string v5, "buffer size is too large, change it to maximum value"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    :goto_2
    iget v4, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    if-ge v4, v7, :cond_2

    const-string v4, "[BIP]"

    const-string v5, "buffer size is smaller than 255, change it to MAX_APDU_SIZE"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    goto :goto_1

    :cond_6
    const-string v4, "[BIP]"

    const-string v5, "buffer size is too small, change it to default value"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mBufferSize:I

    goto :goto_2
.end method

.method public clearChannelBuffer(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBuffer:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    iget-object v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mTxBuffer:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    :goto_0
    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBufferCount:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBufferOffset:I

    iput v1, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mTxBufferCount:I

    return-void

    :cond_0
    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mRxBuffer:[B

    iput-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mTxBuffer:[B

    goto :goto_0
.end method

.method public abstract closeChannel()I
.end method

.method public dataAvailable(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v1, Lcom/android/internal/telephony/cat/CatResponseMessage;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/android/internal/telephony/cat/CatResponseMessage;-><init>(I)V

    const/4 v2, 0x7

    new-array v0, v2, [B

    const/16 v2, -0x48

    aput-byte v2, v0, v4

    aput-byte v6, v0, v5

    invoke-virtual {p0}, Lcom/mediatek/internal/telephony/cat/Channel;->getChannelId()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelStatusData:Lcom/mediatek/common/telephony/ChannelStatus;

    iget v3, v3, Lcom/mediatek/common/telephony/ChannelStatus;->mChannelStatus:I

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v6

    const/4 v2, 0x3

    aput-byte v4, v0, v2

    const/4 v2, 0x4

    const/16 v3, -0x49

    aput-byte v3, v0, v2

    const/4 v2, 0x5

    aput-byte v5, v0, v2

    const/16 v2, 0xff

    if-le p1, v2, :cond_0

    const/4 v2, -0x1

    aput-byte v2, v0, v7

    :goto_0
    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setSourceId(I)V

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setDestinationId(I)V

    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setAdditionalInfo([B)V

    invoke-virtual {v1, v4}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setOneShot(Z)V

    const-string v2, "onEventDownload for dataAvailable"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mHandler:Lcom/android/internal/telephony/cat/CatService;

    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/cat/CatService;->onEventDownload(Lcom/android/internal/telephony/cat/CatResponseMessage;)V

    return-void

    :cond_0
    int-to-byte v2, p1

    aput-byte v2, v0, v7

    goto :goto_0
.end method

.method public getChannelId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelId:I

    return v0
.end method

.method public getChannelStatus()I
    .locals 1

    iget v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mChannelStatus:I

    return v0
.end method

.method public abstract getTxAvailBufferSize()I
.end method

.method public abstract openChannel(Lcom/android/internal/telephony/cat/CatCmdMessage;)I
.end method

.method public abstract receiveData(ILcom/mediatek/internal/telephony/cat/ReceiveDataResult;)I
.end method

.method public abstract receiveData(I)Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;
.end method

.method protected declared-synchronized requestStop()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mStop:Z

    const-string v0, "[BIP]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestStop: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/internal/telephony/cat/Channel;->mStop:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract sendData([BI)I
.end method
