.class public Lcom/mediatek/common/telephony/IccSmsStorageStatus;
.super Ljava/lang/Object;
.source "IccSmsStorageStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mediatek/common/telephony/IccSmsStorageStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mTotal:I

.field public mUsed:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/common/telephony/IccSmsStorageStatus$1;

    invoke-direct {v0}, Lcom/mediatek/common/telephony/IccSmsStorageStatus$1;-><init>()V

    sput-object v0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    iput v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    iput p2, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getTotal()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    return v0
.end method

.method public getUnused()I
    .locals 2

    iget v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    iget v1, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getUsed()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    return v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    iput v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mUsed:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/IccSmsStorageStatus;->mTotal:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
