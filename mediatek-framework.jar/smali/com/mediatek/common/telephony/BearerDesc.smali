.class public Lcom/mediatek/common/telephony/BearerDesc;
.super Ljava/lang/Object;
.source "BearerDesc.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mediatek/common/telephony/BearerDesc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bearerService:I

.field public bearerType:I

.field public connectionElement:I

.field public dataCompression:I

.field public dataRate:I

.field public delay:I

.field public headerCompression:I

.field public mean:I

.field public pdpType:I

.field public peak:I

.field public precedence:I

.field public reliability:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/common/telephony/BearerDesc$1;

    invoke-direct {v0}, Lcom/mediatek/common/telephony/BearerDesc$1;-><init>()V

    sput-object v0, Lcom/mediatek/common/telephony/BearerDesc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerType:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->precedence:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->delay:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->reliability:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->peak:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->mean:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->pdpType:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataCompression:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->headerCompression:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataRate:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerService:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->connectionElement:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerType:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->precedence:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->delay:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->reliability:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->peak:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->mean:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->pdpType:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataCompression:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->headerCompression:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataRate:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerService:I

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->connectionElement:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->precedence:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->delay:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->reliability:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->peak:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->mean:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->pdpType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataCompression:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->headerCompression:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerService:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->connectionElement:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mediatek/common/telephony/BearerDesc$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mediatek/common/telephony/BearerDesc$1;

    invoke-direct {p0, p1}, Lcom/mediatek/common/telephony/BearerDesc;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->precedence:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->delay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->reliability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->peak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->mean:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->pdpType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataCompression:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->headerCompression:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->dataRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->bearerService:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/common/telephony/BearerDesc;->connectionElement:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
