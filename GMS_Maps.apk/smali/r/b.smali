.class public abstract Lr/b;
.super Lr/h;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:[[B

.field final synthetic c:Lr/a;


# direct methods
.method protected constructor <init>(Lr/a;)V
    .locals 1

    const/16 v0, 0x8

    iput-object p1, p0, Lr/b;->c:Lr/a;

    invoke-direct {p0, v0}, Lr/h;-><init>(I)V

    new-array v0, v0, [[B

    iput-object v0, p0, Lr/b;->b:[[B

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;
    .locals 8

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iget-object v3, p0, Lr/b;->c:Lr/a;

    invoke-static {v3}, Lr/a;->g(Lr/a;)I

    move-result v3

    sub-int v3, v0, v3

    new-instance v4, Lo/aB;

    invoke-direct {v4}, Lo/aB;-><init>()V

    invoke-static {}, Lo/av;->values()[Lo/av;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    invoke-virtual {v7, p1}, Lo/av;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/at;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v4, v7}, Lo/aB;->a(Lo/at;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, LA/c;->a(I)LA/c;

    move-result-object v0

    new-instance v5, Lo/aq;

    invoke-direct {v5, v3, v1, v2, v4}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-static {v0, v5}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hs;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, Lr/b;->a:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lr/b;->c:Lr/a;

    invoke-virtual {v1}, Lr/a;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received tile response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b(Ljava/io/InputStream;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v3, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lr/b;->c()I

    move-result v5

    move v0, v1

    :goto_0
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hs;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v2

    if-ne v2, v3, :cond_1

    if-eq v0, v5, :cond_0

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    :goto_1
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v7

    if-eqz v7, :cond_5

    array-length v4, v7

    :goto_2
    invoke-virtual {p0, v4, v2}, Lr/b;->a(II)[B

    move-result-object v2

    if-eqz v7, :cond_2

    array-length v8, v2

    sub-int/2addr v8, v4

    invoke-static {v7, v1, v2, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    if-lt v0, v5, :cond_6

    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    move v4, v1

    goto :goto_2

    :cond_6
    invoke-direct {p0, v6}, Lr/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;

    move-result-object v6

    invoke-virtual {p0, v6}, Lr/b;->a(Landroid/util/Pair;)Ljava/lang/Integer;

    move-result-object v6

    if-nez v6, :cond_7

    iget-object v2, p0, Lr/b;->c:Lr/a;

    invoke-virtual {v2}, Lr/a;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Received wrong tile"

    invoke-static {v2, v4}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    if-eqz v4, :cond_3

    iget-object v4, p0, Lr/b;->b:[[B

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput-object v2, v4, v6

    goto :goto_3
.end method

.method private k()Z
    .locals 1

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->F()Z

    move-result v0

    return v0
.end method

.method private l()Lr/c;
    .locals 5

    sget-object v1, Lr/c;->a:Lr/c;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lr/b;->c()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lr/b;->a(I)Lr/k;

    move-result-object v2

    invoke-virtual {v2}, Lr/k;->d()Lr/c;

    move-result-object v2

    sget-object v3, Lr/c;->a:Lr/c;

    if-eq v1, v3, :cond_0

    invoke-virtual {v2}, Lr/c;->a()I

    move-result v3

    invoke-virtual {v1}, Lr/c;->a()I

    move-result v4

    if-ge v3, v4, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method protected a()I
    .locals 1

    iget v0, p0, Lr/b;->a:I

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 12

    const/4 v4, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x3

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hs;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hs;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->a(Lr/a;)I

    move-result v0

    invoke-virtual {v1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->b(Lr/a;)I

    move-result v0

    invoke-virtual {v1, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    invoke-virtual {v0}, Lr/c;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->c(Lr/a;)F

    move-result v0

    const/high16 v3, 0x3f800000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    const/4 v0, 0x6

    iget-object v3, p0, Lr/b;->c:Lr/a;

    invoke-static {v3}, Lr/a;->c(Lr/a;)F

    move-result v3

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->d(Lr/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->e(Lr/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v8, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_2
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {v1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, v8, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_4
    invoke-direct {p0}, Lr/b;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1, v8, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_5
    iget-object v0, p0, Lr/b;->c:Lr/a;

    invoke-static {v0}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v1, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_6
    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    sget-object v3, Lr/c;->a:Lr/c;

    if-eq v0, v3, :cond_7

    invoke-direct {p0}, Lr/b;->l()Lr/c;

    move-result-object v0

    invoke-virtual {v0}, Lr/c;->a()I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_7
    const/4 v0, 0x6

    invoke-virtual {v1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {p0}, Lr/b;->c()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_a

    invoke-virtual {p0, v0}, Lr/b;->a(I)Lr/k;

    move-result-object v3

    invoke-virtual {v3}, Lr/k;->c()Lo/aq;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/hs;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v6

    invoke-virtual {v5, v10, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v6

    invoke-virtual {v5, v8, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lo/aq;->b()I

    move-result v6

    iget-object v7, p0, Lr/b;->c:Lr/a;

    invoke-static {v7}, Lr/a;->g(Lr/a;)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v5, v11, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lr/b;->c:Lr/a;

    iget-object v6, v6, Lr/a;->a:LA/c;

    iget v6, v6, LA/c;->v:I

    invoke-virtual {v5, v9, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v6, 0x7

    iget-object v7, p0, Lr/b;->c:Lr/a;

    iget-object v7, v7, Lr/a;->a:LA/c;

    iget v7, v7, LA/c;->w:I

    invoke-virtual {v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lr/b;->c:Lr/a;

    invoke-static {v6}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    if-eqz v6, :cond_8

    const/16 v6, 0x1b

    iget-object v7, p0, Lr/b;->c:Lr/a;

    invoke-static {v7}, Lr/a;->f(Lr/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_8
    iget-object v6, p0, Lr/b;->c:Lr/a;

    iget-object v6, v6, Lr/a;->a:LA/c;

    invoke-virtual {v4, v6, v5}, Lo/aq;->a(LA/c;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->u()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0x8

    invoke-virtual {v3}, Lr/k;->g()I

    move-result v3

    invoke-virtual {v5, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_9
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    invoke-static {p1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lr/b;->a(Ljava/io/InputStream;)V

    invoke-direct {p0, v1}, Lr/b;->b(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method protected a(Lr/k;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lr/b;->c()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v1}, Lr/b;->a(I)Lr/k;

    move-result-object v2

    invoke-virtual {v2}, Lr/k;->c()Lo/aq;

    move-result-object v2

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-virtual {p1}, Lr/k;->c()Lo/aq;

    move-result-object v3

    invoke-virtual {v3}, Lo/aq;->b()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected a(II)[B
    .locals 1

    new-array v0, p1, [B

    return-object v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x6c

    return v0
.end method
