.class public LO/I;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[F

.field private static final c:[F


# instance fields
.field a:D

.field private final d:LO/r;

.field private e:LaH/h;

.field private f:D

.field private g:D

.field private h:LO/D;

.field private i:[LO/j;

.field private j:I

.field private k:Z

.field private l:D

.field private volatile m:LO/s;

.field private n:D

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x42700000

    const/4 v3, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [F

    sput-object v0, LO/I;->b:[F

    const/4 v0, 0x6

    new-array v0, v0, [F

    sput-object v0, LO/I;->c:[F

    sget-object v0, LO/I;->b:[F

    aput v4, v0, v5

    sget-object v0, LO/I;->b:[F

    const/high16 v1, 0x40400000

    aput v1, v0, v6

    sget-object v0, LO/I;->b:[F

    const/high16 v1, 0x40d00000

    aput v1, v0, v7

    sget-object v0, LO/I;->b:[F

    const/4 v1, 0x3

    const/high16 v2, 0x41f00000

    aput v2, v0, v1

    sget-object v0, LO/I;->b:[F

    const/4 v1, 0x4

    aput v4, v0, v1

    sget-object v0, LO/I;->c:[F

    aput v3, v0, v5

    sget-object v0, LO/I;->c:[F

    const v1, 0x3f333333

    aput v1, v0, v6

    sget-object v0, LO/I;->c:[F

    const v1, 0x3ecccccd

    aput v1, v0, v7

    sget-object v0, LO/I;->c:[F

    const/4 v1, 0x3

    const v2, 0x3e19999a

    aput v2, v0, v1

    sget-object v0, LO/I;->c:[F

    const/4 v1, 0x4

    aput v3, v0, v1

    return-void
.end method

.method constructor <init>(LO/r;)V
    .locals 3

    const/4 v2, 0x0

    const-wide/high16 v0, -0x4010000000000000L

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, LO/I;->f:D

    iput-wide v0, p0, LO/I;->g:D

    iput-wide v0, p0, LO/I;->l:D

    const-wide v0, 0x7fefffffffffffffL

    iput-wide v0, p0, LO/I;->n:D

    iput-object p1, p0, LO/I;->d:LO/r;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lo/T;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, LO/I;->a:D

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v2, v1}, LO/I;->a([LO/z;ILO/N;I)V

    return-void
.end method

.method private a(LO/j;)D
    .locals 4

    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-direct {p0, v0}, LO/I;->b(LO/N;)D

    move-result-wide v0

    invoke-virtual {p1}, LO/j;->b()I

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private a(D)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, LO/I;->c(D)LO/N;

    move-result-object v0

    iget-object v1, p0, LO/I;->m:LO/s;

    invoke-virtual {v1}, LO/s;->h()LO/N;

    move-result-object v1

    if-eq v0, v1, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LO/N;->j()LO/N;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-direct {p0, p1, p2}, LO/I;->d(D)V

    :cond_1
    invoke-direct {p0, v0}, LO/I;->a(LO/N;)V

    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->d()V

    :cond_2
    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, LO/I;->j:I

    :goto_0
    invoke-direct {p0, p1, p2, v0, v1}, LO/I;->a(DD)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, LO/I;->j:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LO/I;->j:I

    goto :goto_0

    :cond_3
    iget v0, p0, LO/I;->j:I

    if-le v0, v2, :cond_4

    iput-boolean v4, p0, LO/I;->k:Z

    iget-object v0, p0, LO/I;->i:[LO/j;

    iget v1, p0, LO/I;->j:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, LO/I;->a(LO/j;)D

    move-result-wide v1

    cmpg-double v1, p1, v1

    if-gtz v1, :cond_4

    invoke-virtual {v0}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-direct {p0, v1}, LO/I;->c(LO/N;)D

    move-result-wide v1

    double-to-int v1, v1

    iget-wide v2, p0, LO/I;->g:D

    double-to-int v2, v2

    sub-int/2addr v1, v2

    iget-object v2, p0, LO/I;->d:LO/r;

    invoke-virtual {v2, v0, v1}, LO/r;->a(LO/j;I)V

    :cond_4
    iget v0, p0, LO/I;->j:I

    iget-object v1, p0, LO/I;->i:[LO/j;

    array-length v1, v1

    if-lt v0, v1, :cond_6

    invoke-virtual {p0, v4}, LO/I;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->b()V

    :cond_5
    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->k()V

    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->f()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LO/I;->a(LO/N;)V

    :goto_1
    return-void

    :cond_6
    invoke-direct {p0, p1, p2}, LO/I;->b(D)V

    goto :goto_1
.end method

.method private a(LO/N;)V
    .locals 1

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    if-eq v0, p1, :cond_0

    invoke-direct {p0, p1}, LO/I;->e(LO/N;)V

    :cond_0
    return-void
.end method

.method private a([LO/z;ILO/N;I)V
    .locals 12

    const/4 v11, 0x0

    iget-object v0, p0, LO/I;->m:LO/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->i()LO/z;

    move-result-object v11

    :cond_0
    if-eqz p1, :cond_1

    if-ltz p2, :cond_1

    if-eqz p3, :cond_1

    iget-wide v0, p0, LO/I;->f:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    new-instance v0, LO/s;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v11}, LO/s;-><init>([LO/z;ILO/N;IIIIZIZLO/z;)V

    iput-object v0, p0, LO/I;->m:LO/s;

    :goto_0
    return-void

    :cond_2
    aget-object v2, p1, p2

    invoke-virtual {p3}, LO/N;->z()I

    move-result v0

    invoke-virtual {v2, v0}, LO/z;->c(I)D

    move-result-wide v0

    iget-wide v3, p0, LO/I;->g:D

    sub-double/2addr v0, v3

    double-to-int v5, v0

    iget-wide v0, p0, LO/I;->f:D

    invoke-virtual {v2, v0, v1}, LO/z;->b(D)I

    move-result v7

    int-to-float v1, v5

    invoke-virtual {p3}, LO/N;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_1
    invoke-virtual {v2}, LO/z;->k()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v2, v0}, LO/z;->a(I)LO/N;

    move-result-object v3

    invoke-virtual {v3}, LO/N;->e()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, -0x1

    iget-object v0, p0, LO/I;->h:LO/D;

    if-eqz v0, :cond_4

    iget-object v0, p0, LO/I;->h:LO/D;

    invoke-virtual {v0}, LO/D;->e()I

    move-result v4

    :cond_4
    invoke-virtual {v2}, LO/z;->p()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v1

    const/high16 v2, 0x43480000

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    const/4 v8, 0x1

    :goto_2
    aget-object v0, p1, p2

    invoke-virtual {v0}, LO/z;->a()Z

    move-result v0

    if-nez v0, :cond_5

    aget-object v11, p1, p2

    :cond_5
    new-instance v0, LO/s;

    float-to-int v6, v1

    invoke-direct {p0}, LO/I;->f()Z

    move-result v10

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v9, p4

    invoke-direct/range {v0 .. v11}, LO/s;-><init>([LO/z;ILO/N;IIIIZIZLO/z;)V

    iput-object v0, p0, LO/I;->m:LO/s;

    goto :goto_0

    :cond_6
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private a(DD)Z
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, LO/I;->b(DD)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LO/N;D)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v4

    invoke-virtual {p1}, LO/N;->k()LO/N;

    move-result-object v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    :goto_1
    invoke-virtual {p1}, LO/N;->z()I

    move-result v5

    invoke-virtual {v4}, LO/z;->n()Lo/X;

    move-result-object v4

    invoke-virtual {v4}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v5, v4, :cond_3

    cmpl-double v0, p2, v0

    if-ltz v0, :cond_2

    move v0, v2

    :goto_2
    move v3, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, LO/I;->b(LO/N;)D

    move-result-wide v0

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    invoke-direct {p0, p1}, LO/I;->b(LO/N;)D

    move-result-wide v4

    cmpg-double v0, v0, p2

    if-gtz v0, :cond_4

    cmpl-double v0, v4, p2

    if-lez v0, :cond_4

    :goto_3
    move v3, v2

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_3
.end method

.method private a(LaH/h;)Z
    .locals 8

    const-wide/high16 v6, 0x4049000000000000L

    const/4 v2, 0x0

    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->i()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, LO/z;->l()LO/U;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-wide v0, p0, LO/I;->n:D

    iget-wide v4, p0, LO/I;->a:D

    div-double/2addr v0, v4

    invoke-virtual {p1}, LaH/h;->getAccuracy()F

    move-result v4

    float-to-double v4, v4

    add-double/2addr v0, v4

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, LO/z;->d()I

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    add-double/2addr v0, v6

    :cond_2
    invoke-virtual {v3}, LO/z;->l()LO/U;

    move-result-object v4

    invoke-virtual {v4}, LO/U;->c()Lo/u;

    move-result-object v4

    invoke-virtual {p1, v4}, LaH/h;->b(Lo/u;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v0, v4, v0

    if-gez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, LO/z;->d()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, LaH/h;->m()Lo/T;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    iget-wide v0, p0, LO/I;->a:D

    mul-double v4, v6, v0

    invoke-virtual {p1}, LaH/h;->m()Lo/T;

    move-result-object v0

    invoke-virtual {v3, v0, v4, v5}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v0

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(DD)D
    .locals 9

    iget v0, p0, LO/I;->j:I

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p0, LO/I;->j:I

    iget-object v1, p0, LO/I;->i:[LO/j;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    const-wide v0, 0x47efffffe0000000L

    goto :goto_0

    :cond_1
    iget-object v0, p0, LO/I;->i:[LO/j;

    iget v1, p0, LO/I;->j:I

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, LO/I;->a(LO/j;)D

    move-result-wide v1

    invoke-virtual {v0}, LO/j;->d()I

    move-result v3

    int-to-double v3, v3

    iget-wide v5, p0, LO/I;->a:D

    mul-double/2addr v5, p3

    invoke-virtual {v0}, LO/j;->c()I

    move-result v0

    int-to-double v7, v0

    mul-double/2addr v5, v7

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v3

    sub-double v0, v1, v3

    sub-double/2addr v0, p1

    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_2

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_2
    const-wide/16 v2, 0x0

    cmpl-double v2, p3, v2

    if-lez v2, :cond_3

    iget-wide v2, p0, LO/I;->a:D

    mul-double/2addr v2, p3

    div-double/2addr v0, v2

    goto :goto_0

    :cond_3
    const-wide v0, 0x47efffffe0000000L

    goto :goto_0
.end method

.method private b(LO/N;)D
    .locals 4

    invoke-virtual {p1}, LO/N;->z()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LO/I;->d(LO/N;)D

    move-result-wide v0

    invoke-virtual {p1}, LO/N;->a()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->e()D

    move-result-wide v2

    mul-double/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {p1}, LO/N;->z()I

    move-result v1

    invoke-virtual {v0, v1}, LO/z;->b(I)D

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Landroid/location/Location;)LO/D;
    .locals 1

    instance-of v0, p1, LaH/h;

    if-eqz v0, :cond_0

    check-cast p1, LaH/h;

    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(D)V
    .locals 10

    const/4 v9, 0x1

    iget-boolean v0, p0, LO/I;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, LO/I;->j:I

    iget-object v1, p0, LO/I;->i:[LO/j;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LO/I;->i:[LO/j;

    iget v1, p0, LO/I;->j:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, LO/j;->a()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LO/I;->d:LO/r;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, LO/r;->b(LO/j;I)V

    iput-boolean v9, p0, LO/I;->k:Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, LO/I;->e:LaH/h;

    invoke-virtual {v1}, LaH/h;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LO/I;->e:LaH/h;

    invoke-virtual {v1}, LaH/h;->getSpeed()F

    move-result v1

    float-to-double v1, v1

    invoke-direct {p0, p1, p2, v1, v2}, LO/I;->b(DD)D

    move-result-wide v3

    const-wide/high16 v5, 0x4014000000000000L

    cmpg-double v5, v3, v5

    if-gtz v5, :cond_0

    invoke-virtual {v0}, LO/j;->e()LO/N;

    move-result-object v5

    invoke-direct {p0, v5}, LO/I;->c(LO/N;)D

    move-result-wide v5

    iget-wide v7, p0, LO/I;->g:D

    mul-double/2addr v1, v3

    add-double/2addr v1, v7

    sub-double v1, v5, v1

    const-wide/16 v3, 0x0

    cmpl-double v3, v1, v3

    if-lez v3, :cond_0

    iget-object v3, p0, LO/I;->d:LO/r;

    double-to-int v1, v1

    invoke-virtual {v3, v0, v1}, LO/r;->b(LO/j;I)V

    iput-boolean v9, p0, LO/I;->k:Z

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v0

    iget-object v1, p0, LO/I;->m:LO/s;

    invoke-virtual {v1}, LO/s;->m()I

    move-result v1

    iget-object v2, p0, LO/I;->m:LO/s;

    invoke-virtual {v2}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p1}, LO/I;->a([LO/z;ILO/N;I)V

    return-void
.end method

.method private c(LO/N;)D
    .locals 2

    invoke-virtual {p1}, LO/N;->z()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LO/I;->d(LO/N;)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {p1}, LO/N;->z()I

    move-result v1

    invoke-virtual {v0, v1}, LO/z;->c(I)D

    move-result-wide v0

    goto :goto_0
.end method

.method private c(D)LO/N;
    .locals 4

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, LO/I;->a(LO/N;D)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LO/N;->j()LO/N;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2}, LO/I;->a(LO/N;D)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LO/N;->j()LO/N;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, LO/z;->k()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {v2, v0}, LO/z;->a(I)LO/N;

    move-result-object v1

    invoke-virtual {v2, v0}, LO/z;->a(I)LO/N;

    move-result-object v3

    invoke-direct {p0, v3, p1, p2}, LO/I;->a(LO/N;D)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LO/N;)D
    .locals 2

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->g()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, LO/I;->o:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LO/z;->d()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, LO/N;->j()LO/N;

    move-result-object v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x41a00000

    invoke-virtual {p1}, LO/N;->j()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->e()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-double v0, v0

    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private d(D)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, LO/I;->j:I

    iput v3, p0, LO/I;->j:I

    :goto_0
    iget v1, p0, LO/I;->j:I

    iget-object v2, p0, LO/I;->i:[LO/j;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, LO/I;->i:[LO/j;

    iget v2, p0, LO/I;->j:I

    aget-object v1, v1, v2

    invoke-direct {p0, v1}, LO/I;->a(LO/j;)D

    move-result-wide v1

    cmpl-double v1, v1, p1

    if-lez v1, :cond_2

    :cond_0
    iget v1, p0, LO/I;->j:I

    if-eq v0, v1, :cond_1

    iput-boolean v3, p0, LO/I;->k:Z

    :cond_1
    return-void

    :cond_2
    iget v1, p0, LO/I;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LO/I;->j:I

    goto :goto_0
.end method

.method private e()V
    .locals 6

    iget-wide v0, p0, LO/I;->l:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    iget-wide v0, p0, LO/I;->f:D

    iget-wide v2, p0, LO/I;->l:D

    sub-double/2addr v0, v2

    const-wide v2, 0x407f400000000000L

    iget-wide v4, p0, LO/I;->a:D

    mul-double/2addr v2, v4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, LO/I;->f:D

    iput-wide v0, p0, LO/I;->l:D

    invoke-virtual {p0}, LO/I;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LO/I;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->b()V

    goto :goto_0
.end method

.method private e(LO/N;)V
    .locals 3

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v0

    iget-object v1, p0, LO/I;->m:LO/s;

    invoke-virtual {v1}, LO/s;->m()I

    move-result v1

    iget-object v2, p0, LO/I;->m:LO/s;

    invoke-virtual {v2}, LO/s;->k()I

    move-result v2

    invoke-direct {p0, v0, v1, p1, v2}, LO/I;->a([LO/z;ILO/N;I)V

    return-void
.end method

.method private f()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LO/I;->e:LaH/h;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, LO/I;->e:LaH/h;

    invoke-virtual {v2}, LaH/h;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->s()D

    move-result-wide v2

    iget-object v4, p0, LO/I;->e:LaH/h;

    invoke-virtual {v4}, LaH/h;->p()D

    move-result-wide v4

    cmpl-double v2, v4, v2

    if-ltz v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v2, p0, LO/I;->h:LO/D;

    if-eqz v2, :cond_0

    iget-object v2, p0, LO/I;->h:LO/D;

    invoke-virtual {v2}, LO/D;->d()D

    move-result-wide v2

    iget-wide v4, p0, LO/I;->a:D

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4049000000000000L

    const/high16 v6, 0x40000000

    iget-object v7, p0, LO/I;->e:LaH/h;

    invoke-virtual {v7}, LaH/h;->getAccuracy()F

    move-result v7

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_4

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private g()V
    .locals 8

    const-wide/high16 v6, -0x4010000000000000L

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Lo/T;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, LO/I;->a:D

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-direct {p0, v0}, LO/I;->b(Landroid/location/Location;)LO/D;

    move-result-object v0

    iget-object v1, p0, LO/I;->m:LO/s;

    invoke-virtual {v1}, LO/s;->g()LO/z;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/D;->a()LO/z;

    move-result-object v2

    if-ne v2, v1, :cond_0

    iput-object v0, p0, LO/I;->h:LO/D;

    :goto_0
    iget-object v0, p0, LO/I;->h:LO/D;

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/I;->h:LO/D;

    invoke-virtual {v1, v0}, LO/z;->a(LO/D;)D

    move-result-wide v2

    iput-wide v2, p0, LO/I;->f:D

    iget-object v0, p0, LO/I;->h:LO/D;

    invoke-virtual {v0}, LO/D;->e()I

    move-result v0

    invoke-virtual {v1, v0}, LO/z;->c(I)D

    move-result-wide v2

    invoke-virtual {v1}, LO/z;->n()Lo/X;

    move-result-object v0

    iget-object v1, p0, LO/I;->h:LO/D;

    invoke-virtual {v1}, LO/D;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    iget-object v1, p0, LO/I;->h:LO/D;

    invoke-virtual {v1}, LO/D;->b()Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v0, v0

    iget-wide v4, p0, LO/I;->a:D

    div-double/2addr v0, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LO/I;->g:D

    iget-wide v0, p0, LO/I;->n:D

    iget-object v2, p0, LO/I;->h:LO/D;

    invoke-virtual {v2}, LO/D;->d()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, LO/I;->n:D

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->getLatitude()D

    move-result-wide v2

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    const-wide v2, 0x40c3880000000000L

    iget-wide v4, p0, LO/I;->a:D

    mul-double/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    iput-object v0, p0, LO/I;->h:LO/D;

    goto :goto_0

    :cond_1
    iput-wide v6, p0, LO/I;->f:D

    iput-wide v6, p0, LO/I;->g:D

    goto :goto_1
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    invoke-direct {p0, v0}, LO/I;->e(LO/N;)V

    return-void
.end method


# virtual methods
.method a([D)D
    .locals 13

    const-wide/16 v1, 0x0

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v5

    invoke-virtual {v5}, LO/z;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/I;->h:LO/D;

    if-nez v0, :cond_1

    :cond_0
    move-wide v0, v1

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {v5}, LO/z;->u()Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_2

    move-wide v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v4, 0x0

    iget-object v0, p0, LO/I;->h:LO/D;

    invoke-virtual {v0}, LO/D;->e()I

    move-result v3

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/F;

    invoke-virtual {v0, v3}, LO/F;->a(I)Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_1
    if-nez v0, :cond_4

    move-wide v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v5}, LO/z;->n()Lo/X;

    move-result-object v4

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v5

    invoke-virtual {v5}, LR/m;->r()I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-double v7, v5

    iget-wide v9, p0, LO/I;->a:D

    mul-double/2addr v7, v9

    :cond_5
    :goto_2
    cmpg-double v5, v1, v7

    if-gez v5, :cond_7

    invoke-virtual {v4, v3}, Lo/X;->b(I)F

    move-result v5

    float-to-double v9, v5

    add-double/2addr v1, v9

    invoke-virtual {v0, v3}, LO/F;->a(I)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, LO/F;->a()I

    move-result v5

    aget-wide v11, p1, v5

    add-double/2addr v9, v11

    aput-wide v9, p1, v5

    :cond_6
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, LO/F;->a(I)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/F;

    goto :goto_2

    :cond_7
    move-wide v0, v1

    goto :goto_0

    :cond_8
    move-object v0, v4

    goto :goto_1
.end method

.method a([DD)I
    .locals 13

    const-wide/16 v3, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    move-wide v11, v1

    move-wide v1, v3

    move-wide v3, v11

    :goto_0
    const/4 v5, 0x4

    if-gt v0, v5, :cond_0

    aget-wide v5, p1, v0

    sget-object v7, LO/I;->b:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    sget-object v8, LO/I;->b:[F

    aget v8, v8, v0

    div-float/2addr v7, v8

    const/high16 v8, 0x3f800000

    sub-float/2addr v7, v8

    float-to-double v7, v7

    mul-double/2addr v5, v7

    add-double/2addr v5, v1

    aget-wide v1, p1, v0

    add-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    move-wide v3, v1

    move-wide v1, v5

    goto :goto_0

    :cond_0
    div-double v5, v1, v3

    const-wide v0, 0x408f400000000000L

    iget-wide v7, p0, LO/I;->a:D

    mul-double/2addr v7, v0

    const-wide/16 v1, 0x0

    const/4 v0, 0x1

    :goto_1
    const/4 v9, 0x4

    if-ge v0, v9, :cond_2

    aget-wide v9, p1, v0

    add-double/2addr v1, v9

    cmpl-double v9, v1, v7

    if-lez v9, :cond_1

    sget-object v9, LO/I;->c:[F

    aget v9, v9, v0

    float-to-double v9, v9

    cmpl-double v9, v5, v9

    if-lez v9, :cond_1

    :goto_2
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    const-wide/high16 v5, 0x3fe0000000000000L

    mul-double v2, v3, v5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_2

    :cond_3
    const/4 v0, 0x5

    goto :goto_2
.end method

.method public a()LO/s;
    .locals 1

    iget-object v0, p0, LO/I;->m:LO/s;

    return-object v0
.end method

.method public a(Landroid/location/Location;)V
    .locals 7

    const-wide/high16 v5, -0x4010000000000000L

    const/4 v2, 0x0

    const/4 v1, 0x1

    check-cast p1, LaH/h;

    iput-object p1, p0, LO/I;->e:LaH/h;

    iget-object v0, p0, LO/I;->i:[LO/j;

    if-eqz v0, :cond_3

    iget v0, p0, LO/I;->j:I

    iget-object v3, p0, LO/I;->i:[LO/j;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->l()LO/U;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LO/I;->e:LaH/h;

    iget-object v3, p0, LO/I;->m:LO/s;

    invoke-virtual {v3}, LO/s;->g()LO/z;

    move-result-object v3

    invoke-virtual {v3}, LO/z;->l()LO/U;

    move-result-object v3

    invoke-virtual {v3}, LO/U;->c()Lo/u;

    move-result-object v3

    invoke-virtual {v0, v3}, LaH/h;->b(Lo/u;)F

    move-result v0

    const/high16 v3, 0x43480000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    move v0, v1

    :goto_0
    invoke-direct {p0}, LO/I;->g()V

    invoke-direct {p0}, LO/I;->f()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v4, p0, LO/I;->e:LaH/h;

    invoke-direct {p0, v4}, LO/I;->a(LaH/h;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput-boolean v2, p0, LO/I;->k:Z

    const/4 v0, -0x1

    iput v0, p0, LO/I;->j:I

    iput-wide v5, p0, LO/I;->f:D

    iput-wide v5, p0, LO/I;->g:D

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LO/I;->a(LO/N;)V

    move v0, v1

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LO/I;->d:LO/r;

    iget-object v4, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0, v4}, LO/r;->a(LaH/h;)V

    :cond_1
    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->i()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v4

    const/16 v0, 0x64

    invoke-virtual {v4}, LO/z;->d()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    const/16 v0, 0xa

    :cond_2
    :goto_1
    iget-object v5, p0, LO/I;->e:LaH/h;

    invoke-virtual {v4}, LO/z;->m()LO/U;

    move-result-object v6

    invoke-virtual {v6}, LO/U;->c()Lo/u;

    move-result-object v6

    invoke-virtual {v5, v6}, LaH/h;->b(Lo/u;)F

    move-result v5

    int-to-float v0, v0

    cmpg-float v0, v5, v0

    if-gez v0, :cond_7

    invoke-virtual {v4}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, LO/z;->b(I)D

    move-result-wide v4

    invoke-direct {p0, v4, v5}, LO/I;->a(D)V

    :goto_2
    if-eqz v3, :cond_3

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    iget-wide v0, p0, LO/I;->f:D

    invoke-direct {p0, v0, v1}, LO/I;->a(D)V

    :cond_3
    invoke-direct {p0}, LO/I;->h()V

    iget-object v0, p0, LO/I;->h:LO/D;

    if-eqz v0, :cond_4

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, LO/I;->e()V

    :cond_4
    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->e()V

    return-void

    :cond_5
    invoke-virtual {v4}, LO/z;->d()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    const/16 v0, 0x1e

    goto :goto_1

    :cond_6
    iput-boolean v1, p0, LO/I;->o:Z

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method public a([LO/z;IZ)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iput-object v6, p0, LO/I;->h:LO/D;

    iput-wide v2, p0, LO/I;->f:D

    iput-wide v2, p0, LO/I;->g:D

    const-wide/high16 v4, -0x4010000000000000L

    iput-wide v4, p0, LO/I;->l:D

    iput v1, p0, LO/I;->j:I

    iput-boolean v1, p0, LO/I;->k:Z

    const-wide v4, 0x7fefffffffffffffL

    iput-wide v4, p0, LO/I;->n:D

    iget-object v0, p0, LO/I;->e:LaH/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LO/I;->o:Z

    invoke-direct {p0, p1, p2, v6, v1}, LO/I;->a([LO/z;ILO/N;I)V

    invoke-direct {p0, v2, v3}, LO/I;->c(D)LO/N;

    move-result-object v0

    invoke-direct {p0, v0}, LO/I;->a(LO/N;)V

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->l()LO/U;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, LO/z;->l()LO/U;

    move-result-object v4

    invoke-virtual {v4}, LO/U;->c()Lo/u;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, LO/z;->l()LO/U;

    move-result-object v4

    invoke-virtual {v4}, LO/U;->c()Lo/u;

    move-result-object v4

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v5

    invoke-virtual {v5, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-static {v4}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v4

    invoke-virtual {v1, v4}, Lo/T;->c(Lo/T;)F

    move-result v1

    float-to-double v4, v1

    iput-wide v4, p0, LO/I;->n:D

    :cond_0
    invoke-virtual {p0, v0}, LO/I;->a(LO/z;)[LO/j;

    move-result-object v0

    iput-object v0, p0, LO/I;->i:[LO/j;

    iget-object v0, p0, LO/I;->e:LaH/h;

    if-eqz v0, :cond_1

    invoke-direct {p0}, LO/I;->g()V

    iget-wide v0, p0, LO/I;->f:D

    invoke-direct {p0, v0, v1}, LO/I;->c(D)LO/N;

    move-result-object v0

    invoke-direct {p0, v0}, LO/I;->a(LO/N;)V

    invoke-virtual {p0}, LO/I;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LO/I;->a(I)Z

    :cond_1
    iget-object v0, p0, LO/I;->d:LO/r;

    invoke-virtual {v0}, LO/r;->a()V

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    if-eqz p3, :cond_3

    iget-object v0, p0, LO/I;->e:LaH/h;

    if-eqz v0, :cond_6

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LO/I;->e:LaH/h;

    invoke-virtual {v0}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    :goto_1
    iget-wide v2, p0, LO/I;->f:D

    invoke-direct {p0, v2, v3, v0, v1}, LO/I;->a(DD)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, LO/I;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LO/I;->j:I

    goto :goto_1

    :cond_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, LO/I;->e:LaH/h;

    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, LO/N;->z()I

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, v2, v3}, LO/I;->a(D)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    iget-wide v0, p0, LO/I;->f:D

    invoke-direct {p0, v0, v1}, LO/I;->d(D)V

    goto :goto_2

    :cond_6
    move-wide v0, v2

    goto :goto_1
.end method

.method a(I)Z
    .locals 1

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->k()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-direct {p0, p1}, LO/I;->b(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(LO/z;)[LO/j;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LO/z;->k()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v0}, LO/z;->a(I)LO/N;

    move-result-object v2

    invoke-virtual {v2}, LO/N;->v()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/j;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/j;

    return-object v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, LO/I;->m:LO/s;

    invoke-virtual {v0}, LO/s;->h()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()I
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [D

    invoke-virtual {p0, v0}, LO/I;->a([D)D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpl-double v3, v1, v3

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0, v1, v2}, LO/I;->a([DD)I

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    const-wide/high16 v0, -0x4010000000000000L

    const/4 v2, 0x0

    iput-object v2, p0, LO/I;->h:LO/D;

    iput-wide v0, p0, LO/I;->g:D

    iput-wide v0, p0, LO/I;->f:D

    iput-wide v0, p0, LO/I;->l:D

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v2, v1}, LO/I;->a([LO/z;ILO/N;I)V

    iput-object v2, p0, LO/I;->i:[LO/j;

    return-void
.end method
