.class public LO/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method constructor <init>(IZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LO/l;->a:I

    iput-boolean p2, p0, LO/l;->b:Z

    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/l;
    .locals 3

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x6

    if-lt v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v2

    new-instance v0, LO/l;

    invoke-direct {v0, v1, v2}, LO/l;-><init>(IZ)V

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, LO/l;->a:I

    packed-switch v0, :pswitch_data_0

    const-string v0, " "

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "\u2191"

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "\u2197"

    goto :goto_0

    :cond_0
    const-string v0, "\u2196"

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "\u21b1"

    goto :goto_0

    :cond_1
    const-string v0, "\u21b0"

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "\u2198"

    goto :goto_0

    :cond_2
    const-string v0, "\u2199"

    goto :goto_0

    :pswitch_4
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "\u21b7"

    goto :goto_0

    :cond_3
    const-string v0, "\u21b6"

    goto :goto_0

    :pswitch_5
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "\u21bf"

    goto :goto_0

    :cond_4
    const-string v0, "\u21be"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
