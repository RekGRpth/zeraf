.class public LO/t;
.super LR/c;
.source "SourceFile"

# interfaces
.implements LM/b;
.implements LP/t;


# static fields
.field private static a:LO/t;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Looper;

.field private d:Z

.field private final e:LO/r;

.field private f:Landroid/location/Location;

.field private g:LP/u;

.field private h:Z

.field private i:LO/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, LO/t;->a:LO/t;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, LR/c;-><init>()V

    new-instance v0, LO/y;

    invoke-direct {v0, p0, v1}, LO/y;-><init>(LO/t;LO/u;)V

    iput-object v0, p0, LO/t;->i:LO/y;

    iput-object v1, p0, LO/t;->e:LO/r;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Law/p;Ljava/io/File;)V
    .locals 6

    const-string v0, "NavigationThread"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    new-instance v0, LO/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LO/y;-><init>(LO/t;LO/u;)V

    iput-object v0, p0, LO/t;->i:LO/y;

    new-instance v1, LO/J;

    invoke-direct {v1, p2, p0}, LO/J;-><init>(Law/p;LO/t;)V

    sget-object v0, LA/c;->i:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    new-instance v2, LP/u;

    invoke-direct {v2, p1, v0}, LP/u;-><init>(Landroid/content/Context;Lr/z;)V

    iput-object v2, p0, LO/t;->g:LP/u;

    iget-object v0, p0, LO/t;->g:LP/u;

    invoke-virtual {v0, p0}, LP/u;->a(LP/t;)V

    new-instance v0, LO/r;

    iget-object v2, p0, LO/t;->g:LP/u;

    move-object v3, p0

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LO/r;-><init>(LO/J;LP/u;Ljava/lang/Thread;Ljava/io/File;Landroid/content/Context;)V

    iput-object v0, p0, LO/t;->e:LO/r;

    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->d:Z

    invoke-virtual {p0}, LO/t;->start()V

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LO/t;
    .locals 4

    const-class v1, LO/t;

    monitor-enter v1

    :try_start_0
    sget-object v0, LO/t;->a:LO/t;

    if-nez v0, :cond_0

    new-instance v0, LO/t;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v2

    invoke-static {p0}, LJ/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, LO/t;-><init>(Landroid/content/Context;Law/p;Ljava/io/File;)V

    sput-object v0, LO/t;->a:LO/t;

    :cond_0
    sget-object v0, LO/t;->a:LO/t;

    iget-object v0, v0, LO/t;->i:LO/y;

    invoke-static {v0, p0}, LO/y;->a(LO/y;Landroid/content/Context;)V

    sget-object v0, LO/t;->a:LO/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(LO/t;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    iput-object p1, p0, LO/t;->f:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic a(LO/t;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, LO/t;->a(Landroid/os/Message;)V

    return-void
.end method

.method private a(Landroid/location/Location;)V
    .locals 3

    invoke-direct {p0}, LO/t;->m()V

    iput-object p1, p0, LO/t;->f:Landroid/location/Location;

    iget-boolean v0, p0, LO/t;->h:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, LO/v;

    const-string v2, "NavigationThread.IdleHandler"

    invoke-direct {v1, p0, v2}, LO/v;-><init>(LO/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->h:Z

    :cond_0
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, LO/t;->j()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, LO/t;->i()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, LO/t;->k()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, LO/w;

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-static {v5}, LO/w;->a(LO/w;)LaH/h;

    move-result-object v1

    invoke-static {v5}, LO/w;->b(LO/w;)[LO/U;

    move-result-object v2

    invoke-static {v5}, LO/w;->c(LO/w;)I

    move-result v3

    invoke-static {v5}, LO/w;->d(LO/w;)I

    move-result v4

    invoke-static {v5}, LO/w;->e(LO/w;)[LO/b;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LO/r;->a(LaH/h;[LO/U;II[LO/b;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/w;

    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/w;->a(LO/w;)LaH/h;

    move-result-object v2

    invoke-static {v0}, LO/w;->f(LO/w;)LO/z;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LO/r;->a(LaH/h;LO/z;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/w;

    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/w;->b(LO/w;)[LO/U;

    move-result-object v2

    invoke-static {v0}, LO/w;->c(LO/w;)I

    move-result v3

    invoke-static {v0}, LO/w;->e(LO/w;)[LO/b;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LO/r;->a([LO/U;I[LO/b;)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/z;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LO/r;->a(LO/z;Z)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/a;

    invoke-virtual {v1, v0}, LO/r;->a(LO/a;)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-direct {p0, v0}, LO/t;->a(Landroid/location/Location;)V

    goto :goto_0

    :pswitch_9
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/g;

    invoke-virtual {v1, v0}, LO/r;->a(LO/g;)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->h()V

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->j()V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->l()V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/x;

    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/x;->a(LO/x;)LO/z;

    move-result-object v2

    invoke-static {v0}, LO/x;->b(LO/x;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, LO/r;->a(LO/z;I)V

    goto/16 :goto_0

    :pswitch_e
    invoke-direct {p0}, LO/t;->j()V

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->m()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_b
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(LO/t;)Z
    .locals 1

    iget-boolean v0, p0, LO/t;->d:Z

    return v0
.end method

.method static synthetic a(LO/t;Z)Z
    .locals 0

    iput-boolean p1, p0, LO/t;->h:Z

    return p1
.end method

.method static synthetic b(LO/t;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, LO/t;->f:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic c(LO/t;)LO/r;
    .locals 1

    iget-object v0, p0, LO/t;->e:LO/r;

    return-object v0
.end method

.method private declared-synchronized h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LO/t;->c:Landroid/os/Looper;

    new-instance v0, LO/u;

    invoke-direct {v0, p0}, LO/u;-><init>(LO/t;)V

    iput-object v0, p0, LO/t;->b:Landroid/os/Handler;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()V
    .locals 1

    invoke-direct {p0}, LO/t;->m()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LO/t;->d:Z

    return-void
.end method

.method private j()V
    .locals 1

    invoke-direct {p0}, LO/t;->m()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->d:Z

    return-void
.end method

.method private k()V
    .locals 1

    invoke-direct {p0}, LO/t;->m()V

    iget-object v0, p0, LO/t;->c:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/t;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, LO/t;->c:Landroid/os/Looper;

    :cond_0
    return-void
.end method

.method private final m()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on NavigationThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LO/t;->i:LO/y;

    invoke-static {v0}, LO/y;->a(LO/y;)V

    return-void
.end method

.method public a(IZ)V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LM/C;)V
    .locals 0

    return-void
.end method

.method public a(LO/a;)V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LO/g;)V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LO/q;)V
    .locals 1

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->a(LO/q;)V

    return-void
.end method

.method public a(LO/z;)V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LO/z;I)V
    .locals 5

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xb

    new-instance v3, LO/x;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, v4}, LO/x;-><init>(LO/z;ILO/u;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LaH/h;LO/z;)V
    .locals 5

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x4

    new-instance v3, LO/w;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, v4}, LO/w;-><init>(LaH/h;LO/z;LO/u;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LaH/h;LO/z;[LO/b;)V
    .locals 10

    const/4 v0, 0x1

    new-array v2, v0, [LO/U;

    const/4 v0, 0x0

    invoke-virtual {p2}, LO/z;->m()LO/U;

    move-result-object v1

    aput-object v1, v2, v0

    iget-object v7, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v8, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v9, 0x3

    new-instance v0, LO/w;

    invoke-virtual {p2}, LO/z;->d()I

    move-result v3

    const/4 v4, 0x5

    const/4 v6, 0x0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LO/w;-><init>(LaH/h;[LO/U;II[LO/b;LO/u;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LaH/h;[LO/U;I[LO/b;)V
    .locals 10

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A to location must be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v7, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v8, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v9, 0x3

    new-instance v0, LO/w;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LO/w;-><init>(LaH/h;[LO/U;II[LO/b;LO/u;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->a(Z)V

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public b(LO/q;)V
    .locals 1

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->b(LO/q;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, LO/t;->e:LO/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->b(Z)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected e()V
    .locals 3

    iget-object v0, p0, LO/t;->g:LP/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/t;->g:LP/u;

    invoke-virtual {v0}, LP/u;->a()V

    :cond_0
    iget-object v0, p0, LO/t;->e:LO/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->n()V

    :cond_1
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :try_start_0
    invoke-virtual {p0}, LO/t;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    sput-object v0, LO/t;->a:LO/t;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public f()V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public g()V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public l()V
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, LO/t;->h()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "NavigationThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 3

    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
