.class public LO/J;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Law/p;

.field private final b:LO/t;

.field private final c:LO/L;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LO/J;->a:Law/p;

    iput-object v1, p0, LO/J;->b:LO/t;

    new-instance v0, LO/L;

    invoke-direct {v0, p0, v1}, LO/L;-><init>(LO/J;LO/K;)V

    iput-object v0, p0, LO/J;->c:LO/L;

    return-void
.end method

.method constructor <init>(Law/p;LO/t;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LO/J;->a:Law/p;

    iput-object p2, p0, LO/J;->b:LO/t;

    new-instance v0, LO/L;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LO/L;-><init>(LO/J;LO/K;)V

    iput-object v0, p0, LO/J;->c:LO/L;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LO/J;->c:LO/L;

    invoke-virtual {v0, v1}, Law/h;->a(Law/q;)V

    return-void
.end method

.method protected static a(LaH/h;)LO/U;
    .locals 19

    invoke-virtual/range {p0 .. p0}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual/range {p0 .. p0}, LaH/h;->hasBearing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual/range {p0 .. p0}, LaH/h;->getSpeed()F

    move-result v0

    const/high16 v1, 0x41200000

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x4051800000000000L

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-wide v2, 0x4072c00000000000L

    cmpl-double v2, v0, v2

    if-lez v2, :cond_3

    const-wide v0, 0x4072c00000000000L

    :cond_3
    invoke-virtual/range {p0 .. p0}, LaH/h;->l()Lo/af;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Lo/af;->c()I

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {v7}, Lo/af;->b()Lo/X;

    move-result-object v9

    invoke-virtual {v9}, Lo/X;->b()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, LaH/h;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->b()D

    move-result-wide v3

    invoke-static {v3, v4}, Lo/T;->a(D)D

    move-result-wide v11

    new-instance v3, Lo/T;

    invoke-direct {v3}, Lo/T;-><init>()V

    const/4 v6, 0x0

    :goto_1
    add-int/lit8 v4, v10, -0x1

    if-ge v6, v4, :cond_10

    invoke-virtual {v9, v6}, Lo/X;->a(I)Lo/T;

    move-result-object v8

    add-int/lit8 v4, v6, 0x1

    invoke-virtual {v9, v4}, Lo/X;->a(I)Lo/T;

    move-result-object v13

    invoke-static {v8, v13, v2, v3}, Lo/T;->a(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v14, 0x4014000000000000L

    cmpg-double v4, v4, v14

    if-gtz v4, :cond_f

    const-wide/16 v4, 0x0

    invoke-static {v8, v13}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v2

    float-to-double v13, v2

    invoke-virtual/range {p0 .. p0}, LaH/h;->getBearing()F

    move-result v2

    float-to-double v15, v2

    sub-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    const-wide v15, 0x4056800000000000L

    cmpg-double v2, v13, v15

    if-ltz v2, :cond_6

    const-wide v15, 0x4070e00000000000L

    cmpl-double v2, v13, v15

    if-lez v2, :cond_9

    :cond_6
    const/4 v2, 0x1

    move v8, v2

    move v2, v6

    :goto_2
    const/4 v6, 0x1

    if-ne v8, v6, :cond_7

    invoke-virtual {v7}, Lo/af;->k()Z

    move-result v6

    if-nez v6, :cond_8

    :cond_7
    const/4 v6, -0x1

    if-ne v8, v6, :cond_a

    invoke-virtual {v7}, Lo/af;->n()Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v6, v6, 0x1

    const/4 v2, -0x1

    move v8, v2

    move v2, v6

    goto :goto_2

    :cond_a
    move v7, v2

    move-object v2, v3

    move-wide/from16 v17, v4

    move-wide/from16 v5, v17

    move-object v4, v3

    :goto_3
    cmpg-double v13, v5, v0

    if-gez v13, :cond_d

    if-lez v8, :cond_c

    add-int/lit8 v13, v10, -0x1

    if-ge v7, v13, :cond_d

    :cond_b
    add-int v4, v7, v8

    invoke-virtual {v9, v4}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    invoke-virtual {v2, v4}, Lo/T;->c(Lo/T;)F

    move-result v13

    float-to-double v13, v13

    div-double/2addr v13, v11

    add-double/2addr v5, v13

    add-int/2addr v7, v8

    move-object/from16 v17, v4

    move-object v4, v2

    move-object/from16 v2, v17

    goto :goto_3

    :cond_c
    if-gtz v7, :cond_b

    :cond_d
    cmpl-double v7, v5, v0

    if-lez v7, :cond_11

    invoke-virtual {v4}, Lo/T;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    sub-double v0, v5, v0

    mul-double/2addr v0, v7

    double-to-float v0, v0

    invoke-virtual {v2, v4}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-static {v1, v0, v1}, Lo/T;->b(Lo/T;FLo/T;)V

    invoke-virtual {v2, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_e
    new-instance v1, Lo/u;

    invoke-virtual {v0}, Lo/T;->a()I

    move-result v2

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    invoke-direct {v1, v2, v0}, Lo/u;-><init>(II)V

    new-instance v0, LO/U;

    invoke-direct {v0, v1}, LO/U;-><init>(Lo/u;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LO/U;->a(I)V

    goto/16 :goto_0

    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_11
    move-object v0, v2

    goto :goto_4
.end method

.method private a(LO/z;ILandroid/location/Location;)LO/g;
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, LO/z;->v()[LO/W;

    move-result-object v1

    const/4 v0, 0x0

    array-length v2, v1

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    aget-object v2, v1, v4

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v0, v1, v4

    :cond_0
    new-instance v2, LO/i;

    invoke-virtual {p1}, LO/z;->d()I

    move-result v3

    invoke-direct {v2, v1, v3, p2}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {p1}, LO/z;->e()F

    move-result v1

    invoke-virtual {p1}, LO/z;->f()F

    move-result v3

    invoke-virtual {v2, v1, v3}, LO/i;->a(FF)LO/i;

    move-result-object v1

    invoke-virtual {p1}, LO/z;->j()Z

    move-result v2

    invoke-virtual {v1, v2}, LO/i;->a(Z)LO/i;

    move-result-object v1

    invoke-virtual {v1, v0}, LO/i;->a(LO/U;)LO/i;

    move-result-object v0

    invoke-virtual {p1}, LO/z;->r()I

    move-result v1

    invoke-virtual {v0, v1}, LO/i;->a(I)LO/i;

    move-result-object v0

    invoke-virtual {v0, v4}, LO/i;->b(I)LO/i;

    move-result-object v0

    invoke-virtual {p1}, LO/z;->D()[LO/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0, p3}, LO/i;->a(Landroid/location/Location;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    return-object v0
.end method

.method static synthetic a(LO/J;)LO/t;
    .locals 1

    iget-object v0, p0, LO/J;->b:LO/t;

    return-object v0
.end method

.method private b(LaH/h;)F
    .locals 1

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/h;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method private c(LaH/h;)F
    .locals 1

    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method


# virtual methods
.method public a(LO/z;)LO/g;
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LO/J;->a(LO/z;ILandroid/location/Location;)LO/g;

    move-result-object v0

    return-object v0
.end method

.method public a(LO/z;Landroid/location/Location;)LO/g;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, LO/J;->a(LO/z;ILandroid/location/Location;)LO/g;

    move-result-object v0

    return-object v0
.end method

.method public a(LaH/h;LO/z;I)LO/g;
    .locals 9

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, LO/U;

    invoke-virtual {p1}, LaH/h;->q()Lo/u;

    move-result-object v1

    invoke-direct {v0, v1}, LO/U;-><init>(Lo/u;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-virtual {p2}, LO/z;->d()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v1, 0x4

    if-ne p3, v1, :cond_3

    invoke-static {p1}, LO/J;->a(LaH/h;)LO/U;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v1, v0

    :goto_0
    const-wide/16 v2, 0x0

    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LO/D;->a()LO/z;

    move-result-object v5

    if-ne v5, p2, :cond_1

    invoke-virtual {p2, v0}, LO/z;->a(LO/D;)D

    move-result-wide v2

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v5

    add-double/2addr v2, v5

    :cond_1
    const-wide/high16 v5, 0x4069000000000000L

    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    add-double/2addr v2, v5

    invoke-virtual {p2, v2, v3}, LO/z;->c(D)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p2}, LO/z;->m()LO/U;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, LO/i;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/U;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/U;

    invoke-virtual {p2}, LO/z;->d()I

    move-result v3

    invoke-direct {v2, v0, v3, p3}, LO/i;-><init>([LO/U;II)V

    invoke-direct {p0, p1}, LO/J;->b(LaH/h;)F

    move-result v0

    invoke-direct {p0, p1}, LO/J;->c(LaH/h;)F

    move-result v3

    invoke-virtual {v2, v0, v3}, LO/i;->a(FF)LO/i;

    move-result-object v2

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, LO/i;->a(Z)LO/i;

    move-result-object v0

    invoke-virtual {v0, v1}, LO/i;->a(LO/U;)LO/i;

    move-result-object v0

    invoke-virtual {p2}, LO/z;->r()I

    move-result v1

    invoke-virtual {v0, v1}, LO/i;->a(I)LO/i;

    move-result-object v0

    invoke-virtual {p2}, LO/z;->D()[LO/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public a(LaH/h;[LO/U;II[LO/b;)LO/g;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    move v0, v1

    :goto_0
    array-length v3, p2

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [LO/U;

    new-instance v4, LO/U;

    invoke-virtual {p1}, LaH/h;->q()Lo/u;

    move-result-object v5

    invoke-direct {v4, v5}, LO/U;-><init>(Lo/u;)V

    aput-object v4, v3, v2

    array-length v4, p2

    invoke-static {p2, v2, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v4, LO/i;

    invoke-direct {v4, v3, p3, p4}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v4, v1}, LO/i;->a(Z)LO/i;

    move-result-object v1

    invoke-virtual {v1, p5}, LO/i;->a([LO/b;)LO/i;

    move-result-object v1

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LO/J;->b(LaH/h;)F

    move-result v0

    invoke-direct {p0, p1}, LO/J;->c(LaH/h;)F

    move-result v2

    invoke-virtual {v1, v0, v2}, LO/i;->a(FF)LO/i;

    :cond_0
    invoke-virtual {v1}, LO/i;->a()LO/g;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public a([LO/U;I[LO/b;)LO/g;
    .locals 2

    new-instance v0, LO/i;

    const/4 v1, 0x7

    invoke-direct {v0, p1, p2, v1}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {v0, p3}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LO/J;->c:LO/L;

    invoke-virtual {v0, v1}, Law/h;->b(Law/q;)V

    return-void
.end method

.method protected a(LO/g;)V
    .locals 2

    invoke-virtual {p1}, LO/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ll/g;

    const-string v1, "addRequest"

    invoke-direct {v0, v1, p1}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget-object v0, p0, LO/J;->a:Law/p;

    invoke-interface {v0, p1}, Law/p;->c(Law/g;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Router"

    const-string v1, "Invalid request"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LO/J;->b:LO/t;

    invoke-virtual {v0, p1}, LO/t;->a(LO/g;)V

    goto :goto_0
.end method
