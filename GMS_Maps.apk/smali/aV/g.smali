.class public LaV/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(LaH/m;LaN/B;)I
    .locals 2

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LaV/g;->a(LaH/m;)LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1, p1}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(LaH/m;)LaN/B;
    .locals 2

    const/4 v0, 0x0

    invoke-interface {p0}, LaH/m;->g()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/DistanceView;->setLocationProvider(LaH/m;)V

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setInitialVisibility(LaN/B;)V

    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setDestination(LaN/B;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->setOrientationProvider(LaV/h;)V

    invoke-virtual {p1, v1}, Lcom/google/googlenav/ui/view/android/HeadingView;->setLocationProvider(LaH/m;)V

    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setInitialVisibility(LaN/B;)V

    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setDestination(LaN/B;)V

    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
