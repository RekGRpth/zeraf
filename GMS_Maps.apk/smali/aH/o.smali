.class public LaH/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/m;
.implements LaH/y;
.implements Ljava/lang/Runnable;


# static fields
.field private static volatile k:LaH/o;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field c:Z

.field d:Z

.field private final e:LaH/q;

.field private volatile f:Ljava/util/Map;

.field private g:D

.field private h:LaH/h;

.field private i:LaH/h;

.field private j:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LaH/r;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaH/r;-><init>(LaH/p;)V

    iput-object v0, p0, LaH/o;->e:LaH/q;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LaH/o;->f:Ljava/util/Map;

    const-wide/high16 v0, -0x3c20000000000000L

    iput-wide v0, p0, LaH/o;->g:D

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaH/o;->b:Ljava/util/List;

    iput-boolean v2, p0, LaH/o;->c:Z

    iput-boolean v2, p0, LaH/o;->d:Z

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    iput-boolean v0, p0, LaH/o;->j:Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {p2}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-direct {p0}, LaH/o;->v()V

    :cond_1
    invoke-direct {p0}, LaH/o;->t()V

    invoke-direct {p0}, LaH/o;->u()V

    goto :goto_0
.end method

.method private static a(LaN/B;)LaN/B;
    .locals 1

    invoke-static {p0}, LaH/E;->e(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v0

    invoke-virtual {v0, p0}, LaH/E;->d(LaN/B;)LaN/B;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private a(LaH/h;)V
    .locals 3

    if-eqz p1, :cond_1

    iput-object p1, p0, LaH/o;->h:LaH/h;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaH/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/n;

    iget-object v2, p0, LaH/o;->h:LaH/h;

    invoke-interface {v0, v2}, LaH/n;->a(LaH/h;)LaH/h;

    move-result-object v0

    iput-object v0, p0, LaH/o;->h:LaH/h;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, LaH/o;->h:LaH/h;

    iput-object v0, p0, LaH/o;->i:LaH/h;

    iget-object v0, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-static {v0}, LaH/o;->a(LaN/B;)LaN/B;

    move-result-object v0

    iget-object v1, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    iget-object v2, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v1, v2}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    iput-object v0, p0, LaH/o;->i:LaH/h;

    :cond_1
    return-void
.end method

.method public static a(LaH/o;)V
    .locals 0

    sput-object p0, LaH/o;->k:LaH/o;

    return-void
.end method

.method private a(LaH/s;)Z
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LaH/s;->r()LaH/h;

    move-result-object v0

    invoke-direct {p0, v0}, LaH/o;->a(LaH/h;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static a(LaH/s;LaH/s;)Z
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_3

    invoke-interface {p0}, LaH/s;->o()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, LaH/s;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {p0}, LaH/s;->r()LaH/h;

    move-result-object v2

    invoke-interface {p1}, LaH/s;->r()LaH/h;

    move-result-object v3

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_0

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v4

    invoke-virtual {v3}, LaH/h;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x2af8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    invoke-virtual {v2}, LaH/h;->hasAccuracy()Z

    move-result v4

    if-nez v4, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, LaH/h;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v2

    invoke-virtual {v3}, LaH/h;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(LaH/o;)V
    .locals 0

    invoke-direct {p0}, LaH/o;->u()V

    return-void
.end method

.method public static m()LaH/m;
    .locals 1

    sget-object v0, LaH/o;->k:LaH/o;

    return-object v0
.end method

.method public static n()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, LaH/o;->k:LaH/o;

    return-void
.end method

.method private t()V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LaH/p;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, LaH/p;-><init>(LaH/o;Landroid/os/Handler;)V

    invoke-static {v0, v1}, LaH/x;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private declared-synchronized u()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LaH/o;->j:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LaH/o;->j:Z

    iget-boolean v0, p0, LaH/o;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_2

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-direct {p0}, LaH/o;->y()V

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LaH/o;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LaH/o;->x()V

    iget-object v0, p0, LaH/o;->e:LaH/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, LaH/q;->a(LaN/B;LaH/o;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0, p0}, LaH/s;->a(LaH/y;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private w()V
    .locals 7

    iget-object v0, p0, LaH/o;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    new-instance v2, LaH/B;

    invoke-interface {v0}, LaH/s;->o()Z

    move-result v3

    invoke-interface {v0}, LaH/s;->p()Z

    move-result v4

    invoke-interface {v0}, LaH/s;->r()LaH/h;

    move-result-object v5

    invoke-interface {v0}, LaH/s;->d()LaH/h;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, LaH/B;-><init>(ZZLaH/h;LaH/h;)V

    iget-object v3, p0, LaH/o;->f:Ljava/util/Map;

    invoke-interface {v0}, LaH/s;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, LaH/o;->o()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, LaH/o;->g:D

    return-void
.end method

.method private x()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LaH/o;->h:LaH/h;

    iput-object v0, p0, LaH/o;->i:LaH/h;

    return-void
.end method

.method private y()V
    .locals 2

    iget-boolean v0, p0, LaH/o;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/o;->d:Z

    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->f()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->g()V

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LaH/s;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(ILaH/s;)V
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1, p0}, LaH/q;->a(ILaH/o;)V

    goto :goto_0
.end method

.method public a(LaH/A;)V
    .locals 1

    iget-boolean v0, p0, LaH/o;->d:Z

    if-eqz v0, :cond_0

    :cond_0
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1}, LaH/q;->a(LaH/A;)V

    return-void
.end method

.method public a(LaN/B;LaH/s;)V
    .locals 1

    invoke-direct {p0, p2}, LaH/o;->a(LaH/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1, p0}, LaH/q;->a(LaN/B;LaH/o;)V

    :cond_0
    return-void
.end method

.method public b()LaH/h;
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LaH/o;->r()LaH/h;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->d()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public b(LaH/A;)V
    .locals 1

    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1}, LaH/q;->b(LaH/A;)V

    return-void
.end method

.method public declared-synchronized c()Ljava/util/Map;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LaH/o;->g:D

    const-wide v2, 0x409f400000000000L

    add-double/2addr v0, v2

    invoke-virtual {p0}, LaH/o;->o()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    long-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-direct {p0}, LaH/o;->w()V

    :cond_0
    iget-object v0, p0, LaH/o;->f:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, LaH/s;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->h()Z

    move-result v4

    or-int/2addr v1, v4

    invoke-interface {v0}, LaH/s;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    const/4 v1, 0x1

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LaH/s;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, LaH/s;->j()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, LaH/s;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, LaH/o;->s()LaH/h;

    move-result-object v1

    invoke-virtual {p0}, LaH/o;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LaH/h;->c(Landroid/location/Location;)I

    move-result v1

    if-lt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, LaH/o;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v0}, LaH/s;->q()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, LaH/s;->s()Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-interface {v0}, LaH/s;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized k()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaH/o;->c:Z

    invoke-direct {p0}, LaH/o;->y()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized l()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaH/o;->c:Z

    invoke-direct {p0}, LaH/o;->y()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()Lcom/google/googlenav/common/a;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, LaH/s;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "none"

    goto :goto_0
.end method

.method protected q()LaH/s;
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    invoke-static {v0, v1}, LaH/o;->a(LaH/s;LaH/s;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public r()LaH/h;
    .locals 1

    iget-object v0, p0, LaH/o;->h:LaH/h;

    return-object v0
.end method

.method public declared-synchronized run()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LaH/o;->y()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public s()LaH/h;
    .locals 1

    iget-object v0, p0, LaH/o;->i:LaH/h;

    return-object v0
.end method
