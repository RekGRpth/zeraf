.class public Lah/e;
.super Lah/f;
.source "SourceFile"


# static fields
.field private static b:Lah/e;


# instance fields
.field private final c:Lcom/google/googlenav/ui/s;

.field private d:Ljava/util/TreeSet;

.field private e:Lcom/google/android/maps/driveabout/vector/f;

.field private f:Lcom/google/android/maps/driveabout/vector/c;

.field private g:Lah/c;


# direct methods
.method public static b()Lah/e;
    .locals 1

    sget-object v0, Lah/e;->b:Lah/e;

    return-object v0
.end method

.method public static c()Z
    .locals 1

    sget-object v0, Lah/e;->b:Lah/e;

    if-eqz v0, :cond_0

    sget-object v0, Lah/e;->b:Lah/e;

    iget-object v0, v0, Lah/e;->a:Landroid/view/accessibility/AccessibilityManager;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lah/e;->b:Lah/e;

    iget-object v0, v0, Lah/e;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, Le/a;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    goto :goto_0
.end method

.method private f()V
    .locals 7

    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    iget-object v1, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v1

    invoke-static {v1}, Lah/a;->a(LaN/u;)Lah/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lah/e;->e:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah/e;->f:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    iget-object v1, p0, Lah/e;->e:Lcom/google/android/maps/driveabout/vector/f;

    iget-object v2, p0, Lah/e;->f:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v3, p0, Lah/e;->g:Lah/c;

    iget-object v4, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lah/a;->a(Lcom/google/android/maps/driveabout/vector/f;Lcom/google/android/maps/driveabout/vector/c;Lah/c;LaN/u;)Lah/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lah/e;->f:Lcom/google/android/maps/driveabout/vector/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v0, :cond_0

    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    iget-object v1, p0, Lah/e;->e:Lcom/google/android/maps/driveabout/vector/f;

    iget-object v2, p0, Lah/e;->f:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v3, p0, Lah/e;->g:Lah/c;

    iget-object v4, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lah/a;->b(Lcom/google/android/maps/driveabout/vector/f;Lcom/google/android/maps/driveabout/vector/c;Lah/c;LaN/u;)Lah/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    iget-object v1, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    iget-object v2, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v2

    invoke-static {v1, v2}, Lah/a;->a(Lcom/google/googlenav/ui/ak;LaN/u;)Lah/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->I()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_4

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v2, v1, :cond_3

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v6

    invoke-virtual {v5, v6}, LaN/u;->d(LaN/B;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Lcom/google/googlenav/E;->d()I

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lah/e;->d:Ljava/util/TreeSet;

    check-cast v1, Lcom/google/googlenav/ai;

    iget-object v6, p0, Lah/e;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v6

    invoke-static {v1, v6}, Lah/a;->a(Lcom/google/googlenav/ai;LaN/u;)Lah/a;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method protected a(Lah/a;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lah/a;->c()I

    move-result v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lah/a;

    invoke-virtual {p0, p1}, Lah/e;->a(Lah/a;)I

    move-result v0

    return v0
.end method

.method protected a(Lah/a;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, " "

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lah/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected a(Lah/a;Le/f;)V
    .locals 2

    const/4 v1, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lah/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Le/f;->c(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v1}, Le/f;->a(Z)V

    invoke-virtual {p2, v1}, Le/f;->c(Z)V

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Le/f;->a(I)V

    invoke-virtual {p1}, Lah/a;->d()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Le/f;->b(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    check-cast p1, Lah/a;

    invoke-virtual {p0, p1, p2}, Lah/e;->a(Lah/a;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Le/f;)V
    .locals 0

    check-cast p1, Lah/a;

    invoke-virtual {p0, p1, p2}, Lah/e;->a(Lah/a;Le/f;)V

    return-void
.end method

.method protected a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lah/e;->f()V

    :cond_0
    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method protected a(Lah/a;ILandroid/os/Bundle;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .locals 1

    check-cast p1, Lah/a;

    invoke-virtual {p0, p1, p2, p3}, Lah/e;->a(Lah/a;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method protected b(I)Lah/a;
    .locals 3

    iget-object v0, p0, Lah/e;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lah/a;

    invoke-virtual {v0}, Lah/a;->c()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic c(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lah/e;->b(I)Lah/a;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    invoke-direct {p0}, Lah/e;->f()V

    invoke-virtual {p0}, Lah/e;->e()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lah/e;->b(I)Lah/a;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {p0, v0, v1}, Lah/e;->a(Ljava/lang/Object;I)V

    return-void
.end method
