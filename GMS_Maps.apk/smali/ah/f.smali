.class public abstract Lah/f;
.super Le/m;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/view/accessibility/AccessibilityManager;

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Rect;

.field private final d:Landroid/graphics/Rect;

.field private e:Landroid/view/View;

.field private f:I

.field private final g:Landroid/util/SparseArray;

.field private final h:Lvedroid/support/v4/view/a;


# direct methods
.method private b(Ljava/lang/Object;I)Landroid/view/accessibility/AccessibilityEvent;
    .locals 4

    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    new-instance v1, Le/u;

    invoke-direct {v1, v0}, Le/u;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lah/f;->a(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {p0, p1, v0}, Lah/f;->a(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "You must add text or a content description in populateEventForItem()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v1, v3, v2}, Le/u;->a(Landroid/view/View;I)V

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Le/f;
    .locals 5

    const/4 v2, 0x1

    invoke-static {}, Le/f;->b()Le/f;

    move-result-object v0

    invoke-virtual {p0, p1}, Lah/f;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v2}, Le/f;->d(Z)V

    invoke-virtual {v0, v2}, Le/f;->b(Z)V

    invoke-virtual {p0, p1, v0}, Lah/f;->a(Ljava/lang/Object;Le/f;)V

    invoke-virtual {v0}, Le/f;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Le/f;->d()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "You must add text or a content description in populateNodeForItem()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v2, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/f;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Le/f;->b(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Le/f;->b(Landroid/view/View;)V

    iget-object v2, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Le/f;->a(Landroid/view/View;I)V

    iget v2, p0, Lah/f;->f:I

    if-ne v2, v1, :cond_1

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Le/f;->a(I)V

    :goto_0
    iget-object v1, p0, Lah/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Le/f;->a(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Le/f;->c(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lah/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "You must set parent or screen bounds in populateNodeForItem()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Le/f;->a(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lah/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lah/f;->e:Landroid/view/View;

    iget-object v2, p0, Lah/f;->d:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v1, p0, Lah/f;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lah/f;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lah/f;->b:Landroid/graphics/Rect;

    iget-object v4, p0, Lah/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Le/f;->d(Landroid/graphics/Rect;)V

    :cond_4
    :goto_1
    return-object v0

    :cond_5
    iget-object v3, p0, Lah/f;->c:Landroid/graphics/Rect;

    iget-object v4, p0, Lah/f;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lah/f;->c:Landroid/graphics/Rect;

    neg-int v1, v1

    neg-int v2, v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p0, Lah/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Le/f;->b(Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method private b()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/f;

    invoke-virtual {v0}, Le/f;->e()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method private c()Le/f;
    .locals 4

    iget-object v0, p0, Lah/f;->e:Landroid/view/View;

    invoke-static {v0}, Le/f;->a(Landroid/view/View;)Le/f;

    move-result-object v0

    iget-object v1, p0, Lah/f;->e:Landroid/view/View;

    invoke-static {v1, v0}, Lvedroid/support/v4/view/M;->a(Landroid/view/View;Le/f;)V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0, v1}, Lah/f;->a(Ljava/util/List;)V

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lah/f;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0, v3, v2}, Le/f;->b(Landroid/view/View;I)V

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)I
.end method

.method public a(I)Le/f;
    .locals 3

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lah/f;->c()Le/f;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/f;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lah/f;->c(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lah/f;->b(Ljava/lang/Object;)Le/f;

    move-result-object v1

    iget-object v2, p0, Lah/f;->g:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/Object;I)V
    .locals 3

    iget-object v0, p0, Lah/f;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, Le/a;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lah/f;->b(Ljava/lang/Object;I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    iget-object v0, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
.end method

.method protected abstract a(Ljava/lang/Object;Le/f;)V
.end method

.method protected abstract a(Ljava/util/List;)V
.end method

.method public a(IILandroid/os/Bundle;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lah/f;->h:Lvedroid/support/v4/view/a;

    iget-object v1, p0, Lah/f;->e:Landroid/view/View;

    invoke-virtual {v0, v1, p2, p3}, Lvedroid/support/v4/view/a;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, p1}, Lah/f;->c(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    sparse-switch p2, :sswitch_data_0

    :cond_2
    move v0, v1

    :goto_1
    invoke-virtual {p0, v2, p2, p3}, Lah/f;->a(Ljava/lang/Object;ILandroid/os/Bundle;)Z

    move-result v1

    or-int/2addr v1, v0

    goto :goto_0

    :sswitch_0
    iget v3, p0, Lah/f;->f:I

    if-eq v3, p1, :cond_2

    iput p1, p0, Lah/f;->f:I

    const v1, 0x8000

    invoke-virtual {p0, v2, v1}, Lah/f;->a(Ljava/lang/Object;I)V

    goto :goto_1

    :sswitch_1
    iget v3, p0, Lah/f;->f:I

    if-ne v3, p1, :cond_2

    const/high16 v1, -0x80000000

    iput v1, p0, Lah/f;->f:I

    const/high16 v1, 0x10000

    invoke-virtual {p0, v2, v1}, Lah/f;->a(Ljava/lang/Object;I)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method protected abstract a(Ljava/lang/Object;ILandroid/os/Bundle;)Z
.end method

.method protected abstract c(I)Ljava/lang/Object;
.end method

.method public e()V
    .locals 2

    invoke-direct {p0}, Lah/f;->b()V

    iget-object v0, p0, Lah/f;->e:Landroid/view/View;

    iget-object v1, p0, Lah/f;->h:Lvedroid/support/v4/view/a;

    invoke-static {v0, v1}, Lvedroid/support/v4/view/M;->a(Landroid/view/View;Lvedroid/support/v4/view/a;)V

    iget-object v0, p0, Lah/f;->e:Landroid/view/View;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    return-void
.end method
