.class public Lbj/aP;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Ljava/lang/String;

.field c:Ljava/util/List;

.field d:Ljava/lang/CharSequence;

.field e:Ljava/util/List;


# direct methods
.method public constructor <init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbj/aP;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbj/aP;->e:Ljava/util/List;

    iput-boolean p1, p0, Lbj/aP;->a:Z

    invoke-direct {p0, p2, p3}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/CharSequence;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-virtual {p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v3, -0x1

    if-ge v0, v4, :cond_0

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-static {p3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    sget-object v2, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p1

    :pswitch_0
    const/16 v0, 0x41b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x41a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x41c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x419

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/aa;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbj/aP;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v3, p0, Lbj/aP;->c:Ljava/util/List;

    invoke-direct {p0, v2}, Lbj/aP;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbj/aQ;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p1}, Lbj/aP;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbj/aQ;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    new-instance v1, Lbj/aQ;

    invoke-direct {v1}, Lbj/aQ;-><init>()V

    iput-object p1, v1, Lbj/aQ;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v0}, Lcom/google/googlenav/aY;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->b:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->c:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iput-object v0, v1, Lbj/aQ;->d:Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lbj/aP;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->f:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->g:Ljava/lang/CharSequence;

    invoke-virtual {p0, p1, v4}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->h:Ljava/lang/CharSequence;

    new-instance v0, Lcom/google/googlenav/aY;

    invoke-direct {v0, p1}, Lcom/google/googlenav/aY;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lcom/google/googlenav/aY;->a()Z

    move-result v0

    iput-boolean v0, v1, Lbj/aQ;->k:Z

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0}, Lbj/aP;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->e:Ljava/lang/CharSequence;

    iput-object v5, v1, Lbj/aQ;->i:Ljava/lang/CharSequence;

    iput-object v5, v1, Lbj/aQ;->j:Ljava/lang/CharSequence;

    :goto_1
    return-object v1

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iput-object v5, v1, Lbj/aQ;->e:Ljava/lang/CharSequence;

    const/16 v0, 0x9

    const/16 v2, 0x3f

    invoke-direct {p0, p1, v0, v2}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->i:Ljava/lang/CharSequence;

    const/16 v0, 0xa

    const/16 v2, 0x3e

    invoke-direct {p0, p1, v0, v2}, Lbj/aP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lbj/aQ;->j:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    :goto_0
    const/4 v2, 0x4

    invoke-static {p2, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lbj/aE;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;

    move-result-object v3

    if-nez v0, :cond_2

    if-nez v3, :cond_2

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p1, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-static {v2, v8}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    sget-object v5, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v5}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    iput-object v4, p0, Lbj/aP;->d:Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    iget-object v2, p0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move v3, v1

    :goto_2
    if-ge v3, v0, :cond_0

    invoke-virtual {p1, v9, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v2, 0x5

    invoke-static {v4, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v9}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v5

    if-eqz v2, :cond_5

    if-eqz v5, :cond_4

    const/16 v6, 0x416

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/String;

    aput-object v2, v7, v1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-static {v6, v7}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_4
    invoke-static {v4, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v6

    long-to-int v4, v6

    if-nez v4, :cond_5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v2, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    iget-object v4, p0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_6
    const/16 v2, 0x618

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/CharSequence;
    .locals 7

    const/4 v6, 0x2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-virtual {p1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/google/googlenav/ui/aV;->bh:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    const-string v4, "\u00a0"

    invoke-virtual {v1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, v3}, Lbj/aP;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->bi:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    add-int/lit8 v3, v2, -0x1

    if-ge v0, v3, :cond_0

    const-string v3, "  "

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/CharSequence;
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    const-string v0, " "

    sget-object v2, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    :cond_0
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\<.*?>"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    :goto_0
    invoke-static {v2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    const-string v2, " "

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    :goto_1
    invoke-static {v2, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    :cond_1
    return-object v1

    :cond_2
    sget-object v0, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Ljava/lang/CharSequence;
    .locals 5

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lbj/aP;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v4

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    invoke-static {p1}, Lbj/aE;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz p2, :cond_2

    :goto_1
    const-string v1, "\\<.*?>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    :cond_0
    return-object v3

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    :goto_2
    sget-object v1, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    sget-object v4, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v0, v1, v4}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method
