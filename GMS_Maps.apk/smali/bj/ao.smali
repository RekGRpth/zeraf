.class public Lbj/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field private c:Lcom/google/googlenav/ai;

.field private d:Lbf/aQ;

.field private e:Lbf/m;

.field private f:I

.field private final g:Lcom/google/googlenav/bh;

.field private final h:Z

.field private i:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/aQ;Lbf/m;ILcom/google/googlenav/bh;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbj/ao;->a:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbj/ao;->b:Ljava/util/List;

    iput-object p1, p0, Lbj/ao;->c:Lcom/google/googlenav/ai;

    iput-object p2, p0, Lbj/ao;->d:Lbf/aQ;

    iput-object p3, p0, Lbj/ao;->e:Lbf/m;

    iput p4, p0, Lbj/ao;->f:I

    iput-object p5, p0, Lbj/ao;->g:Lcom/google/googlenav/bh;

    iput-boolean p6, p0, Lbj/ao;->h:Z

    return-void
.end method

.method static synthetic a(Lbj/ao;)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lbj/ao;->c:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lbl/h;)V
    .locals 5

    invoke-virtual {p2}, Lbl/h;->k()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lbj/aq;

    invoke-direct {v1, p0, p2, v0}, Lbj/aq;-><init>(Lbj/ao;Lbl/h;Ljava/lang/String;)V

    new-instance v0, Lbj/at;

    invoke-direct {v0, p0, p2}, Lbj/at;-><init>(Lbj/ao;Lbl/h;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lbl/h;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lbl/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_0
    invoke-virtual {p2}, Lbl/h;->m()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Lbl/h;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_1
    const/16 v3, 0x68

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lbj/au;

    invoke-direct {v4, p0, p2}, Lbj/au;-><init>(Lbj/ao;Lbl/h;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lbj/ao;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lbj/ao;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbj/ao;->i:Landroid/app/AlertDialog;

    return-void
.end method

.method private a(Landroid/content/Context;Lbl/h;Lbl/c;)V
    .locals 5

    invoke-virtual {p3}, Lbl/c;->d()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbj/aw;

    invoke-direct {v1, p0}, Lbj/aw;-><init>(Lbj/ao;)V

    invoke-static {v0, v1}, Lcom/google/common/collect/bx;->a(Ljava/util/List;Lcom/google/common/base/x;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3}, Lbl/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Lbj/ax;

    invoke-direct {v1, p0, p3, p2, p1}, Lbj/ax;-><init>(Lbj/ao;Lbl/c;Lbl/h;Landroid/content/Context;)V

    new-instance v2, Lbj/ay;

    invoke-direct {v2, p0, p2, p1}, Lbj/ay;-><init>(Lbj/ao;Lbl/h;Landroid/content/Context;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3}, Lbl/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbj/ao;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbj/ao;->i:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lbj/ao;Landroid/content/Context;Lbl/h;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbj/ao;->a(Landroid/content/Context;Lbl/h;)V

    return-void
.end method

.method static synthetic a(Lbj/ao;Landroid/content/Context;Lbl/h;Lbl/c;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lbj/ao;->a(Landroid/content/Context;Lbl/h;Lbl/c;)V

    return-void
.end method

.method static synthetic b(Lbj/ao;)Lcom/google/googlenav/bh;
    .locals 1

    iget-object v0, p0, Lbj/ao;->g:Lcom/google/googlenav/bh;

    return-object v0
.end method

.method private b(Landroid/content/Context;Lbl/h;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lbj/ao;->e:Lbf/m;

    if-eqz v0, :cond_0

    const/16 v0, 0x6a

    const-string v1, "el"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lbj/ao;->e:Lbf/m;

    iget-object v2, p0, Lbj/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    const/16 v4, 0x45c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {p2}, Lbl/h;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/ui/wizard/dF;->b(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    new-instance v3, Lbj/av;

    invoke-direct {v3, p0, p2}, Lbj/av;-><init>(Lbj/ao;Lbl/h;)V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lbf/m;->a(LaN/B;Lcom/google/googlenav/ui/wizard/dF;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lbj/ao;Landroid/content/Context;Lbl/h;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbj/ao;->b(Landroid/content/Context;Lbl/h;)V

    return-void
.end method

.method static synthetic c(Lbj/ao;)Lbf/aQ;
    .locals 1

    iget-object v0, p0, Lbj/ao;->d:Lbf/aQ;

    return-object v0
.end method

.method static synthetic d(Lbj/ao;)Lbf/m;
    .locals 1

    iget-object v0, p0, Lbj/ao;->e:Lbf/m;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/ao;->f:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    iget-boolean v0, p0, Lbj/ao;->h:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    new-instance v1, Lbj/aC;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/aC;-><init>(Lbj/ap;)V

    const v0, 0x7f100287

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aC;->a:Landroid/view/ViewGroup;

    const v0, 0x7f100289

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aC;->b:Landroid/view/ViewGroup;

    const v0, 0x7f10028a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aC;->c:Landroid/view/ViewGroup;

    const v0, 0x7f10028d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aC;->e:Landroid/widget/TextView;

    const v0, 0x7f10028b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aC;->d:Landroid/widget/ImageView;

    const v0, 0x7f10028e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aC;->f:Landroid/view/ViewGroup;

    const v0, 0x7f100292

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aC;->g:Landroid/view/View;

    iget-object v0, p0, Lbj/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0}, Lbl/o;->k()Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbl/h;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f100290

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aC;->f:Landroid/view/ViewGroup;

    const v0, 0x7f10028f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aC;->g:Landroid/view/View;

    :cond_1
    const v0, 0x7f100291

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aC;->h:Landroid/view/View;

    return-object v1
.end method

.method public a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v3, 0x0

    check-cast p2, Lbj/aC;

    iget-object v0, p0, Lbj/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0}, Lbl/o;->k()Lbl/h;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v0, p2, Lbj/aC;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, Lbj/ao;->a(Landroid/view/View;Z)V

    invoke-virtual {v4}, Lbl/h;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100290

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f10028f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p2, Lbj/aC;->g:Landroid/view/View;

    :goto_0
    move v2, v3

    :goto_1
    invoke-virtual {v4}, Lbl/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    new-instance v5, Lbj/aD;

    invoke-virtual {v4}, Lbl/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/c;

    invoke-direct {v5, v0}, Lbj/aD;-><init>(Lbl/c;)V

    invoke-virtual {v4}, Lbl/h;->i()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f040140

    iget-object v1, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v5, Lbj/aD;->a:Landroid/widget/TextView;

    iget-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    iget-object v1, v5, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_2
    iget-object v0, p2, Lbj/aC;->i:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f10028e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100292

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p2, Lbj/aC;->g:Landroid/view/View;

    goto :goto_0

    :cond_1
    const v0, 0x7f040141

    iget-object v1, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f10036d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v5, Lbj/aD;->a:Landroid/widget/TextView;

    iget-object v1, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->b:Landroid/view/ViewGroup;

    new-instance v1, Lbj/ap;

    invoke-direct {v1, p0, p1}, Lbj/ap;-><init>(Lbj/ao;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p2, Lbj/aC;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Lbl/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbj/aC;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v1, p2, Lbj/aC;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lbj/ao;->d:Lbf/aQ;

    invoke-virtual {v0}, Lbf/aQ;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f020473

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbj/ao;->d:Lbf/aQ;

    invoke-virtual {v1}, Lbf/aQ;->d()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->g:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lbj/aC;->g:Landroid/view/View;

    iget-object v1, p0, Lbj/ao;->d:Lbf/aQ;

    invoke-virtual {v1}, Lbf/aQ;->d()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbj/ao;->a(Landroid/view/View;Z)V

    :cond_3
    iget-object v0, p2, Lbj/aC;->h:Landroid/view/View;

    invoke-virtual {p0, v0, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aD;

    iget-object v2, v0, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v6}, Lbj/ao;->a(Landroid/view/View;Z)V

    invoke-virtual {v4}, Lbl/h;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, Lbj/aD;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v3}, Lbl/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v2, v0, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    new-instance v2, Lbj/aA;

    invoke-direct {v2, p0, v0, v4, p2}, Lbj/aA;-><init>(Lbj/ao;Lbj/aD;Lbl/h;Lbj/aC;)V

    iget-object v0, v0, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbj/ao;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    const v0, 0x7f020474

    goto :goto_3

    :cond_6
    iget-object v2, v0, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v2}, Lbl/c;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->q:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iget-object v3, v0, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_7
    iget-object v0, p2, Lbj/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, Lbj/ao;->a(Landroid/view/View;Z)V

    iget-object v0, p2, Lbj/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, Lbj/ao;->a(Landroid/view/View;Z)V

    :cond_8
    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400d3

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
