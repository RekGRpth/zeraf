.class public Lbj/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:I

.field final b:Lcom/google/googlenav/ai;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbj/I;->a:I

    iput-object p2, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    return-void
.end method

.method private a(Lbj/M;ILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 2

    iget-object v0, p1, Lbj/M;->b:Landroid/widget/ImageView;

    invoke-static {v0, p2}, Lbj/G;->a(Landroid/widget/ImageView;I)V

    iget-object v0, p1, Lbj/M;->c:Landroid/widget/TextView;

    invoke-static {v0, p3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lbj/M;->a:Landroid/view/View;

    invoke-static {v0, p4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    if-nez p3, :cond_0

    const/16 v0, 0x8

    :goto_0
    iget-object v1, p1, Lbj/M;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p1, Lbj/M;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/View;)Lbj/M;
    .locals 2

    new-instance v1, Lbj/M;

    invoke-direct {v1}, Lbj/M;-><init>()V

    iput-object p1, v1, Lbj/M;->a:Landroid/view/View;

    const v0, 0x7f100091

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/M;->b:Landroid/widget/ImageView;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/M;->c:Landroid/widget/TextView;

    const v0, 0x7f100297

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/M;->d:Landroid/view/View;

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbj/I;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    new-instance v1, Lbj/L;

    invoke-direct {v1}, Lbj/L;-><init>()V

    iput-object p1, v1, Lbj/L;->a:Landroid/view/View;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/L;->b:Landroid/widget/TextView;

    const v0, 0x7f10001f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/L;->c:Landroid/view/View;

    const v0, 0x7f100298

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lbj/I;->b(Landroid/view/View;)Lbj/M;

    move-result-object v0

    iput-object v0, v1, Lbj/L;->d:Lbj/M;

    const v0, 0x7f100299

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lbj/I;->b(Landroid/view/View;)Lbj/M;

    move-result-object v0

    iput-object v0, v1, Lbj/L;->e:Lbj/M;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 9

    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v1, 0x0

    check-cast p2, Lbj/L;

    iget-object v0, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v0}, Lbf/aS;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;

    move-result-object v4

    const v2, 0x7f02027e

    new-instance v0, Lbj/J;

    invoke-direct {v0, p0, p1, v5}, Lbj/J;-><init>(Lbj/I;Lcom/google/googlenav/ui/e;Ljava/lang/String;)V

    iget-object v5, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    const/16 v7, 0x1c

    invoke-virtual {v5, v7}, Lcom/google/googlenav/ai;->o(I)V

    iget-object v5, p2, Lbj/L;->d:Lbj/M;

    :goto_0
    iget-object v7, p2, Lbj/L;->d:Lbj/M;

    invoke-direct {p0, v7, v2, v4, v0}, Lbj/I;->a(Lbj/M;ILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bE()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x2a7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    iget-object v0, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "opentable"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f020271

    :goto_1
    new-instance v2, Lbj/K;

    invoke-direct {v2, p0, p1}, Lbj/K;-><init>(Lbj/I;Lcom/google/googlenav/ui/e;)V

    iget-object v5, p0, Lbj/I;->b:Lcom/google/googlenav/ai;

    const/16 v7, 0x10

    invoke-virtual {v5, v7}, Lcom/google/googlenav/ai;->o(I)V

    iget-object v5, p2, Lbj/L;->e:Lbj/M;

    move-object v8, v2

    move v2, v0

    move-object v0, v8

    :goto_2
    iget-object v7, p2, Lbj/L;->e:Lbj/M;

    invoke-direct {p0, v7, v2, v4, v0}, Lbj/I;->a(Lbj/M;ILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    if-eqz v5, :cond_0

    iget-object v0, v5, Lbj/M;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-nez v5, :cond_3

    :goto_3
    iget-object v0, p2, Lbj/L;->b:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    if-nez v1, :cond_1

    move v3, v6

    :cond_1
    iget-object v0, p2, Lbj/L;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lbj/L;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    const v0, 0x7f020226

    goto :goto_1

    :cond_3
    const/16 v0, 0x123

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_3

    :cond_4
    move-object v0, v1

    move v2, v3

    move-object v4, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    move v2, v3

    move-object v4, v1

    move-object v5, v1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400da

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
