.class public Lbj/by;
.super Lbj/a;
.source "SourceFile"


# instance fields
.field private final e:Ljava/lang/CharSequence;

.field private final f:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V
    .locals 0

    invoke-direct {p0, p1, p4, p5, p6}, Lbj/a;-><init>(Ljava/lang/CharSequence;III)V

    iput-object p2, p0, Lbj/by;->f:Ljava/lang/CharSequence;

    iput-object p3, p0, Lbj/by;->e:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/bA;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/bA;-><init>(Lbj/bz;)V

    const v0, 0x7f100034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/bA;->a(Lbj/bA;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100480

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/bA;->b(Lbj/bA;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100481

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/bA;->c(Lbj/bA;Landroid/widget/TextView;)Landroid/widget/TextView;

    invoke-static {v1, p1}, Lbj/bA;->a(Lbj/bA;Landroid/view/View;)Landroid/view/View;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p2, Lbj/bA;

    invoke-static {p2}, Lbj/bA;->a(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbj/by;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbj/bA;->a(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-static {p2}, Lbj/bA;->b(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbj/by;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbj/by;->e:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lbj/bA;->c(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lbj/bA;->c(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p2}, Lbj/bA;->c(Lbj/bA;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbj/by;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0401c3

    return v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lbj/by;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
