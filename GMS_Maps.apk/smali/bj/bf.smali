.class Lbj/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaB/p;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/google/googlenav/ui/br;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/br;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lbj/bf;->a:Ljava/util/Map;

    iput-object p1, p0, Lbj/bf;->b:Lcom/google/googlenav/ui/br;

    return-void
.end method


# virtual methods
.method public Q_()V
    .locals 5

    iget-object v0, p0, Lbj/bf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v4, p0, Lbj/bf;->b:Lcom/google/googlenav/ui/br;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/googlenav/ui/ab;

    invoke-virtual {v4, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v2

    check-cast v2, Lan/f;

    invoke-virtual {v2}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    :cond_0
    return-void
.end method
