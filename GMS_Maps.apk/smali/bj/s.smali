.class public Lbj/s;
.super Lbj/a;
.source "SourceFile"


# instance fields
.field private final e:Lax/t;

.field private final f:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Lax/t;IIII)V
    .locals 0

    invoke-direct {p0, p1, p3, p4, p6}, Lbj/a;-><init>(Ljava/lang/CharSequence;III)V

    iput-object p2, p0, Lbj/s;->e:Lax/t;

    iput p5, p0, Lbj/s;->f:I

    return-void
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 2

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbj/s;->e:Lax/t;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/H;->a(Landroid/content/Context;Lax/t;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbj/u;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/u;-><init>(Lbj/t;)V

    const v0, 0x7f100034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbj/u;->a(Lbj/u;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f10001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/u;->a(Lbj/u;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    invoke-static {v1, p1}, Lbj/u;->a(Lbj/u;Landroid/view/View;)Landroid/view/View;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbj/u;

    invoke-static {p2}, Lbj/u;->a(Lbj/u;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbj/s;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbj/u;->a(Lbj/u;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-static {p2}, Lbj/u;->b(Lbj/u;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-direct {p0, v0}, Lbj/s;->a(Landroid/widget/ImageView;)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f04006a

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lbj/s;->f:I

    return v0
.end method
