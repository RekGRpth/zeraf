.class public LaD/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaD/n;


# instance fields
.field protected a:LaD/m;

.field private b:LaD/k;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LaD/g;->a:LaD/m;

    return-void
.end method

.method public a(Landroid/content/Context;LaD/m;)V
    .locals 1

    iput-object p2, p0, LaD/g;->a:LaD/m;

    new-instance v0, LaD/k;

    invoke-direct {v0, p1, p0}, LaD/k;-><init>(Landroid/content/Context;LaD/n;)V

    iput-object v0, p0, LaD/g;->b:LaD/k;

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    iget-object v0, p0, LaD/g;->b:LaD/k;

    invoke-virtual {v0, p1}, LaD/k;->a(Z)V

    return-void
.end method

.method public a(LaD/k;)Z
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    move-result v0

    return v0
.end method

.method public a(LaD/k;Z)Z
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->b:LaD/k;

    invoke-virtual {v0, p1}, LaD/k;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public b(LaD/k;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, LaD/g;->c:Z

    :cond_0
    return v0
.end method

.method public b(LaD/k;Z)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LaD/g;->a:LaD/m;

    new-instance v2, LaD/c;

    invoke-direct {v2, v0, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v1, v2}, LaD/m;->a(LaD/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public c(LaD/k;)V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, LaD/g;->c:Z

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    return-void
.end method

.method public c(LaD/k;Z)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    goto :goto_0
.end method

.method public d(LaD/k;)Z
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public e(LaD/k;)Z
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public f(LaD/k;)V
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    return-void
.end method

.method public g(LaD/k;)Z
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public h(LaD/k;)Z
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public i(LaD/k;)V
    .locals 3

    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1, p2, p3, p4}, LaD/m;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-boolean v0, p0, LaD/g;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1, p2, p3, p4}, LaD/m;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onShowPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
