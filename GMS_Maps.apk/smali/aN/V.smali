.class public LaN/V;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/h;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:[LaN/U;


# instance fields
.field private volatile b:LaN/W;

.field private final c:Ljava/util/Hashtable;

.field private final d:Ljava/util/Hashtable;

.field private final e:J

.field private final f:Las/d;

.field private volatile g:Z

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [LaN/U;

    sput-object v0, LaN/V;->a:[LaN/U;

    return-void
.end method

.method public constructor <init>(JLas/c;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/V;->g:Z

    iput-wide p1, p0, LaN/V;->e:J

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaN/V;->h:J

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/V;->c:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/V;->d:Ljava/util/Hashtable;

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    new-instance v0, Las/d;

    invoke-direct {v0, p3, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, LaN/V;->f:Las/d;

    iget-object v0, p0, LaN/V;->f:Las/d;

    const-wide/16 v1, 0x4e91

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    invoke-virtual {p0}, LaN/V;->c()V

    return-void
.end method

.method static synthetic a(LaN/V;)Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, LaN/V;->d:Ljava/util/Hashtable;

    return-object v0
.end method

.method private declared-synchronized a(LaN/P;LaN/X;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/V;->b:LaN/W;

    if-nez v0, :cond_0

    new-instance v0, LaN/W;

    invoke-direct {v0, p0}, LaN/W;-><init>(LaN/V;)V

    iput-object v0, p0, LaN/V;->b:LaN/W;

    :cond_0
    iget-object v0, p0, LaN/V;->b:LaN/W;

    invoke-virtual {v0, p2}, LaN/W;->a(LaN/X;)V

    iget-object v0, p0, LaN/V;->d:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(LaN/V;)Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, LaN/V;->c:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic c(LaN/V;)V
    .locals 0

    invoke-direct {p0}, LaN/V;->f()V

    return-void
.end method

.method static synthetic e()[LaN/U;
    .locals 1

    sget-object v0, LaN/V;->a:[LaN/U;

    return-object v0
.end method

.method private f()V
    .locals 1

    iget-boolean v0, p0, LaN/V;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaN/V;->f:Las/d;

    invoke-virtual {v0}, Las/d;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/V;->f:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(LaN/P;Z)LaN/X;
    .locals 9

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LaN/P;->e()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v1, 0x16

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LaN/V;->c:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/X;

    iget-object v1, p0, LaN/V;->d:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/X;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-virtual {v0}, LaN/X;->d()J

    move-result-wide v3

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    sub-long/2addr v5, v3

    if-eqz p2, :cond_0

    if-nez v1, :cond_0

    const-wide/high16 v7, -0x8000000000000000L

    cmp-long v1, v3, v7

    if-eqz v1, :cond_0

    iget-wide v3, p0, LaN/V;->e:J

    const-wide/16 v7, 0x2

    div-long/2addr v3, v7

    cmp-long v1, v5, v3

    if-lez v1, :cond_0

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-wide v3, p0, LaN/V;->h:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_2

    iget-object v1, p0, LaN/V;->b:LaN/W;

    if-eqz v1, :cond_0

    :cond_2
    invoke-direct {p0, p1, v0}, LaN/V;->a(LaN/P;LaN/X;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    if-eqz v1, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    :try_start_2
    new-instance v0, LaN/X;

    invoke-direct {v0, p1}, LaN/X;-><init>(LaN/P;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p1, v0}, LaN/V;->a(LaN/P;LaN/X;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    invoke-virtual {p0}, LaN/V;->d()V

    return-void
.end method

.method declared-synchronized a(J)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/V;->c:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    iget-object v1, p0, LaN/V;->c:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/X;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    invoke-virtual {v1}, LaN/X;->e()J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v1, v3, p1

    if-lez v1, :cond_0

    iget-object v1, p0, LaN/V;->c:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, LaN/V;->c:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    return-void
.end method

.method public declared-synchronized b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/V;->b:LaN/W;

    if-eqz v0, :cond_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LaN/V;->b:LaN/W;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaN/V;->b:LaN/W;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LaN/V;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LaN/V;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaN/V;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaN/V;->g:Z

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    iget-object v1, p0, LaN/V;->f:Las/d;

    invoke-virtual {v0, v1}, Las/c;->b(Las/a;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    const-wide/32 v0, 0xea60

    :try_start_0
    invoke-virtual {p0, v0, v1}, LaN/V;->a(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "TrafficService BG"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0
.end method
