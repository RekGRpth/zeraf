.class public LaN/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/o;
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/util/Hashtable;

.field b:LaN/K;

.field volatile c:Z

.field private final d:Z

.field private final e:Ljava/util/Vector;

.field private f:LaN/m;

.field private final g:Ljava/util/Vector;

.field private h:I

.field private i:I

.field private volatile j:Z

.field private k:LaN/G;

.field private l:I

.field private final m:Ljava/util/Hashtable;

.field private final n:Ljava/lang/Object;

.field private final o:Ljava/lang/Object;

.field private p:J

.field private final q:Ljava/util/List;

.field private r:I

.field private s:J


# direct methods
.method constructor <init>(IIIILjava/lang/String;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, LaN/D;->k:LaN/G;

    iput v2, p0, LaN/D;->l:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaN/D;->o:Ljava/lang/Object;

    iput-boolean v4, p0, LaN/D;->c:Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaN/D;->q:Ljava/util/List;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaN/D;->s:J

    const/16 v0, 0x1a

    iput v0, p0, LaN/D;->r:I

    if-ne p1, v3, :cond_0

    iput-boolean v4, p0, LaN/D;->d:Z

    const/16 v0, 0x61a8

    iput v0, p0, LaN/D;->h:I

    invoke-direct {p0}, LaN/D;->s()V

    :goto_0
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    iput-boolean v2, p0, LaN/D;->j:Z

    new-instance v0, LaN/E;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    move-object v1, p0

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LaN/E;-><init>(LaN/D;Las/c;IILjava/lang/String;)V

    invoke-virtual {v0}, LaN/E;->g()V

    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    iput-wide v0, p0, LaN/D;->p:J

    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, LaN/F;

    invoke-direct {v1, p0}, LaN/F;-><init>(LaN/D;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    return-void

    :cond_0
    iput-boolean v2, p0, LaN/D;->d:Z

    iput p1, p0, LaN/D;->h:I

    if-ne p2, v3, :cond_1

    invoke-direct {p0}, LaN/D;->s()V

    goto :goto_0

    :cond_1
    iput p2, p0, LaN/D;->i:I

    goto :goto_0
.end method

.method private a([J[LaN/P;III)I
    .locals 6

    aget-wide v2, p1, p5

    invoke-direct {p0, p1, p2, p5, p4}, LaN/D;->a([J[LaN/P;II)V

    move v1, p3

    :goto_0
    if-ge p3, p4, :cond_0

    aget-wide v4, p1, p3

    cmp-long v0, v4, v2

    if-ltz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    invoke-direct {p0, p1, p2, p3, v1}, LaN/D;->a([J[LaN/P;II)V

    :goto_1
    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    aget-wide v2, p1, p4

    aget-wide v4, p1, v1

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    invoke-direct {p0, p1, p2, p4, v1}, LaN/D;->a([J[LaN/P;II)V

    :goto_2
    return v1

    :cond_1
    move v1, p4

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static a(LaN/P;JJ)J
    .locals 2

    sub-long v0, p1, p3

    return-wide v0
.end method

.method private a(LaN/P;I)Lam/f;
    .locals 1

    packed-switch p2, :pswitch_data_0

    invoke-direct {p0, p1}, LaN/D;->c(LaN/P;)Lam/f;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, LaN/D;->b(LaN/P;)Lam/f;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LaN/P;LaN/P;LaN/I;)Lam/f;
    .locals 7

    const/16 v5, 0x100

    const/4 v0, 0x0

    const/16 v3, 0x80

    invoke-virtual {p1}, LaN/P;->c()I

    move-result v1

    invoke-virtual {p2}, LaN/P;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    if-ne v1, v2, :cond_0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LaN/P;->d()I

    move-result v2

    invoke-virtual {p2}, LaN/P;->d()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    if-ne v2, v4, :cond_1

    move v2, v0

    :goto_1
    move-object v0, p3

    move v4, v3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, LaN/I;->a(IIIIII)Lam/f;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private a(I)V
    .locals 7

    const/4 v0, 0x0

    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v3

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, LaN/D;->j:Z

    invoke-virtual {p0}, LaN/D;->i()[LaN/P;

    move-result-object v4

    move v2, v0

    move v1, p1

    :goto_0
    array-length v0, v4

    if-ge v2, v0, :cond_1

    iget v0, p0, LaN/D;->i:I

    if-le v1, v0, :cond_1

    aget-object v5, v4, v2

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->e()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, LaN/I;->v()Z

    move-result v6

    if-nez v6, :cond_2

    :cond_0
    iget-object v6, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v6, v5}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, LaN/I;->j()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    sub-int v0, v1, v0

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LaN/D;->j:Z

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, LaN/D;->j:Z

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(IILjava/lang/String;)V
    .locals 1

    if-lez p1, :cond_0

    new-instance v0, LaN/x;

    invoke-direct {v0, p0, p3, p1, p2}, LaN/x;-><init>(LaN/D;Ljava/lang/String;II)V

    iput-object v0, p0, LaN/D;->b:LaN/K;

    :goto_0
    return-void

    :cond_0
    new-instance v0, LaN/L;

    invoke-direct {v0}, LaN/L;-><init>()V

    iput-object v0, p0, LaN/D;->b:LaN/K;

    goto :goto_0
.end method

.method static synthetic a(LaN/D;)V
    .locals 0

    invoke-direct {p0}, LaN/D;->w()V

    return-void
.end method

.method static synthetic a(LaN/D;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LaN/D;->a(IILjava/lang/String;)V

    return-void
.end method

.method private a(LaN/I;)V
    .locals 2

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v0

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(LaN/I;I)V
    .locals 2

    iget-object v0, p0, LaN/D;->k:LaN/G;

    if-nez v0, :cond_0

    new-instance v0, LaN/G;

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v1

    invoke-virtual {v1}, LaN/P;->b()B

    move-result v1

    invoke-direct {v0, p0, v1}, LaN/G;-><init>(LaN/D;B)V

    iput-object v0, p0, LaN/D;->k:LaN/G;

    :cond_0
    iget-object v0, p0, LaN/D;->k:LaN/G;

    invoke-virtual {v0, p1, p2}, LaN/G;->a(LaN/I;I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LaN/I;->a(Z)V

    return-void
.end method

.method private a([J[LaN/P;)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, LaN/D;->b([J[LaN/P;II)V

    return-void
.end method

.method private a([J[LaN/P;II)V
    .locals 4

    aget-object v0, p2, p4

    aget-object v1, p2, p3

    aput-object v1, p2, p4

    aput-object v0, p2, p3

    aget-wide v0, p1, p4

    aget-wide v2, p1, p3

    aput-wide v2, p1, p4

    aput-wide v0, p1, p3

    return-void
.end method

.method static synthetic b(LaN/D;)I
    .locals 1

    iget v0, p0, LaN/D;->r:I

    return v0
.end method

.method private b(LaN/P;)Lam/f;
    .locals 1

    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    return-object v0
.end method

.method private b([J[LaN/P;II)V
    .locals 6

    if-le p4, p3, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    invoke-direct/range {v0 .. v5}, LaN/D;->a([J[LaN/P;III)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, p1, p2, p3, v1}, LaN/D;->b([J[LaN/P;II)V

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, p2, v0, p4}, LaN/D;->b([J[LaN/P;II)V

    :cond_0
    return-void
.end method

.method static synthetic c(LaN/D;)I
    .locals 2

    iget v0, p0, LaN/D;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LaN/D;->l:I

    return v0
.end method

.method private c(LaN/P;)Lam/f;
    .locals 2

    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LaN/D;->d(LaN/P;)Lam/f;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method static synthetic d(LaN/D;)I
    .locals 2

    iget v0, p0, LaN/D;->l:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LaN/D;->l:I

    return v0
.end method

.method private d(LaN/P;)Lam/f;
    .locals 8

    const/4 v0, 0x0

    invoke-static {}, LaN/D;->u()J

    move-result-wide v2

    iget-wide v4, p0, LaN/D;->s:J

    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, LaN/P;->h()LaN/P;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaN/P;->e()LaN/Y;

    move-result-object v4

    invoke-virtual {p1}, LaN/P;->e()LaN/Y;

    move-result-object v5

    invoke-virtual {v4, v5}, LaN/Y;->a(LaN/Y;)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v1, v5, v6, v7}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v5

    const/4 v6, 0x2

    if-ne v4, v6, :cond_0

    invoke-direct {p0, p1, v1, v5}, LaN/D;->a(LaN/P;LaN/P;LaN/I;)Lam/f;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-direct {p0}, LaN/D;->t()V

    iput-wide v2, p0, LaN/D;->s:J

    const-string v2, "Map Service image scaling"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 11

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x7d0

    move-wide v2, v0

    :goto_0
    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v5

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v6

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/I;

    invoke-virtual {v1}, LaN/I;->g()J

    move-result-wide v9

    add-long/2addr v9, v2

    cmp-long v4, v9, v6

    if-gez v4, :cond_0

    invoke-virtual {v1}, LaN/I;->d()V

    iget-object v1, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    :goto_1
    if-ltz v4, :cond_0

    iget-object v1, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/k;

    const/16 v9, 0x8

    invoke-static {v9, v0}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v9

    invoke-virtual {v1, v9}, LaN/k;->b(LaN/P;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v1, v4, -0x1

    move v4, v1

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0xfa0

    move-wide v2, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V

    monitor-exit v5

    return-void

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private e(LaN/P;)J
    .locals 2

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic e(LaN/D;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaN/D;->q:Ljava/util/List;

    return-object v0
.end method

.method private s()V
    .locals 1

    iget v0, p0, LaN/D;->h:I

    mul-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, LaN/D;->i:I

    return-void
.end method

.method private t()V
    .locals 3

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaN/D;->j:Z

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LaN/D;->j:Z

    invoke-virtual {p0}, LaN/D;->b()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static u()J
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method private v()V
    .locals 2

    iget-boolean v0, p0, LaN/D;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/D;->c:Z

    iget-object v1, p0, LaN/D;->o:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/D;->o:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private w()V
    .locals 2

    iget-boolean v0, p0, LaN/D;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/D;->c:Z

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "MapService"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method


# virtual methods
.method a(LaN/P;IZIJ)LaN/I;
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    iget-object v1, p0, LaN/D;->b:LaN/K;

    if-nez v1, :cond_0

    move p3, v4

    :cond_0
    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v1, p5, v1

    if-nez v1, :cond_8

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    :goto_0
    if-nez v0, :cond_6

    iget-boolean v0, p0, LaN/D;->j:Z

    if-nez v0, :cond_5

    iget-object v4, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v4

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1}, LaN/K;->a(LaN/P;)LaN/I;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_3

    invoke-direct {p0, p1, p4}, LaN/D;->a(LaN/P;I)Lam/f;

    move-result-object v3

    if-eqz p3, :cond_2

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, LaN/I;

    invoke-direct {v0, p1, v3}, LaN/I;-><init>(LaN/P;Lam/f;)V

    invoke-direct {p0, v0, p2}, LaN/D;->a(LaN/I;I)V

    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {p0, v3}, LaN/D;->b(Z)V

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_3
    invoke-virtual {v0, v1, v2}, LaN/I;->a(J)V

    return-object v0

    :cond_1
    move-object v0, v3

    goto :goto_1

    :cond_2
    :try_start_3
    new-instance v0, LaN/I;

    const/4 v5, 0x1

    invoke-direct {v0, p1, v3, v5}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_4
    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_3
    if-nez p3, :cond_4

    const-wide/16 v5, 0x4e20

    sub-long/2addr v1, v5

    :cond_4
    :try_start_5
    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :cond_5
    new-instance v0, LaN/I;

    invoke-direct {v0, p1, v3, v5}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, LaN/I;->e()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, LaN/I;->v()Z

    move-result v3

    if-nez v3, :cond_7

    if-eqz p3, :cond_7

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    invoke-virtual {v3}, Law/h;->n()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, v0, p2}, LaN/D;->a(LaN/I;I)V

    invoke-static {v4}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->c()V

    goto :goto_3

    :cond_7
    invoke-static {v4}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->a()V

    goto :goto_3

    :cond_8
    move-wide v1, p5

    goto/16 :goto_0
.end method

.method public a(LaN/P;IZZ)LaN/I;
    .locals 7

    const-wide/high16 v5, -0x8000000000000000L

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, LaN/D;->a(LaN/P;IZZJ)LaN/I;

    move-result-object v0

    return-object v0
.end method

.method a(LaN/P;IZZJ)LaN/I;
    .locals 7

    if-eqz p4, :cond_0

    const/4 v4, 0x2

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, LaN/D;->a(LaN/P;IZIJ)LaN/I;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public a(LaN/P;)LaN/n;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0, v0}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v0

    invoke-virtual {v0}, LaN/I;->p()LaN/n;

    move-result-object v0

    return-object v0
.end method

.method a()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    return-object v0
.end method

.method public a(LaN/P;ZJ)Ljava/util/Vector;
    .locals 3

    iget-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x8

    invoke-static {v2, p1}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LaN/k;->a(LaN/P;Z)LaN/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaN/n;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LaN/D;->g:Ljava/util/Vector;

    invoke-virtual {v0, p3, p4}, LaN/n;->b(J)Lam/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p3, p4}, LaN/n;->a(J)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    return-object v0
.end method

.method a(II)V
    .locals 5

    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1, p2}, LaN/K;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v2, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/I;

    invoke-virtual {v1}, LaN/I;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    return-void
.end method

.method public a(LaN/k;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    :cond_0
    return-void
.end method

.method public a(LaN/m;)V
    .locals 0

    iput-object p1, p0, LaN/D;->f:LaN/m;

    return-void
.end method

.method a(Lat/d;)V
    .locals 1

    iget-object v0, p0, LaN/D;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 3

    invoke-static {}, LaN/f;->f()V

    invoke-direct {p0}, LaN/D;->t()V

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, LaN/D;->d(Z)V

    iget-boolean v0, p0, LaN/D;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x61a8

    iput v0, p0, LaN/D;->h:I

    invoke-direct {p0}, LaN/D;->s()V

    :goto_0
    invoke-virtual {p0}, LaN/D;->d()V

    monitor-exit v1

    return-void

    :cond_0
    iget v0, p0, LaN/D;->h:I

    add-int/lit16 v0, v0, -0x1f40

    const/16 v2, 0x61a8

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LaN/D;->h:I

    invoke-direct {p0}, LaN/D;->s()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b()V
    .locals 3

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    invoke-interface {v0}, Lam/f;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(LaN/k;)V
    .locals 1

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    return-void
.end method

.method b(Z)V
    .locals 0

    iput-boolean p1, p0, LaN/D;->j:Z

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->f()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method c(Z)V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    invoke-direct {p0}, LaN/D;->v()V

    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1}, LaN/K;->a(Z)V

    :cond_0
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->d()V

    invoke-static {v0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    return-void
.end method

.method d()V
    .locals 5

    invoke-virtual {p0}, LaN/D;->e()I

    move-result v0

    iget v1, p0, LaN/D;->h:I

    if-le v0, v1, :cond_0

    iget-boolean v1, p0, LaN/D;->d:Z

    if-eqz v1, :cond_1

    invoke-static {}, Ljava/lang/System;->gc()V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    const-wide/32 v3, 0x9c40

    sub-long/2addr v1, v3

    long-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    long-to-int v2, v2

    div-int/lit8 v2, v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/16 v2, 0x61a8

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LaN/D;->h:I

    invoke-direct {p0}, LaN/D;->s()V

    iget v1, p0, LaN/D;->h:I

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, LaN/D;->a(I)V

    goto :goto_0
.end method

.method e()I
    .locals 4

    const/4 v0, 0x0

    iget-object v2, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->j()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit v2

    return v1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()I
    .locals 6

    const/4 v0, 0x0

    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    invoke-virtual {p0}, LaN/D;->h()I

    move-result v1

    const/16 v2, 0x30

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, LaN/D;->i()[LaN/P;

    move-result-object v4

    move v2, v0

    :goto_0
    array-length v0, v4

    if-ge v2, v0, :cond_0

    const/16 v0, 0x18

    if-le v1, v0, :cond_0

    aget-object v0, v4, v2

    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->t()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, LaN/I;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v1, -0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V

    monitor-exit v3

    return v1

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v5

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v7

    move v1, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->x()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v7, "images"

    invoke-direct {v0, v7, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "data"

    invoke-virtual {p0}, LaN/D;->e()I

    move-result v7

    invoke-direct {v0, v1, v7}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "mapCache"

    invoke-direct {v0, v1, v3, v6}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    invoke-interface {v0}, Lam/f;->g()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "tempScaledImages"

    invoke-direct {v0, v3, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "layerServices"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    move v1, v2

    move-object v3, v0

    :goto_2
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "MapService"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method h()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v1
.end method

.method i()[LaN/P;
    .locals 9

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v4, v0, [LaN/P;

    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v5, v0, [J

    const/4 v0, 0x0

    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    move v1, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    aput-object v0, v4, v1

    aget-object v0, v4, v1

    aget-object v7, v4, v1

    invoke-direct {p0, v7}, LaN/D;->e(LaN/P;)J

    move-result-wide v7

    invoke-static {v0, v2, v3, v7, v8}, LaN/D;->a(LaN/P;JJ)J

    move-result-wide v7

    aput-wide v7, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v5, v4}, LaN/D;->a([J[LaN/P;)V

    return-object v4
.end method

.method j()Z
    .locals 2

    iget-object v0, p0, LaN/D;->k:LaN/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/D;->k:LaN/G;

    const/4 v1, 0x0

    iput-object v1, p0, LaN/D;->k:LaN/G;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 3

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LaN/k;->e()V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, LaN/D;->f:LaN/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/D;->f:LaN/m;

    invoke-interface {v0}, LaN/m;->c()V

    :cond_0
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->c()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public m()Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget v0, p0, LaN/D;->l:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()V
    .locals 2

    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    iput-wide v0, p0, LaN/D;->p:J

    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0}, LaN/K;->g()V

    :cond_0
    iget-object v1, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method p()V
    .locals 0

    invoke-direct {p0}, LaN/D;->v()V

    return-void
.end method

.method q()V
    .locals 0

    invoke-direct {p0}, LaN/D;->w()V

    return-void
.end method

.method r()V
    .locals 1

    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0}, LaN/K;->h()V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 14

    const-wide/16 v12, 0xc29

    const-wide/16 v10, 0x835

    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    add-long v2, v0, v10

    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    add-long/2addr v0, v12

    :cond_0
    :goto_0
    iget-boolean v4, p0, LaN/D;->c:Z

    if-nez v4, :cond_5

    :try_start_0
    iget-object v6, p0, LaN/D;->o:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {}, LaN/D;->u()J

    move-result-wide v7

    cmp-long v4, v2, v0

    if-gez v4, :cond_4

    move-wide v4, v2

    :goto_1
    sub-long/2addr v4, v7

    const-wide/16 v7, 0x0

    cmp-long v7, v4, v7

    if-lez v7, :cond_1

    iget-object v7, p0, LaN/D;->o:Ljava/lang/Object;

    invoke-virtual {v7, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_2
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-boolean v4, p0, LaN/D;->c:Z

    if-nez v4, :cond_0

    invoke-static {}, LaN/D;->u()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_2

    invoke-virtual {p0}, LaN/D;->d()V

    add-long v2, v4, v10

    :cond_2
    cmp-long v6, v0, v4

    if-gez v6, :cond_3

    const/4 v6, 0x0

    invoke-direct {p0, v6}, LaN/D;->d(Z)V

    add-long v0, v4, v12

    :cond_3
    iget-object v6, p0, LaN/D;->b:LaN/K;

    if-eqz v6, :cond_0

    iget-object v6, p0, LaN/D;->b:LaN/K;

    invoke-interface {v6}, LaN/K;->i()Z

    move-result v6

    if-nez v6, :cond_0

    iget-wide v6, p0, LaN/D;->p:J

    const-wide/16 v8, 0xfa0

    add-long/2addr v6, v8

    cmp-long v4, v6, v4

    if-gez v4, :cond_0

    iget-object v5, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v5
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    iget-object v4, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_3
    :try_start_5
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1

    :catch_0
    move-exception v4

    const-string v5, "MapService BG"

    invoke-static {v5, v4}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    move-wide v4, v0

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_7
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception v4

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_0

    :cond_5
    return-void

    :catch_2
    move-exception v4

    goto :goto_3

    :catch_3
    move-exception v4

    goto :goto_2
.end method
