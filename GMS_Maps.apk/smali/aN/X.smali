.class public LaN/X;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaN/P;

.field private b:[LaN/U;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>(LaN/P;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaN/X;->c:J

    iput-object p1, p0, LaN/X;->a:LaN/P;

    return-void
.end method


# virtual methods
.method public a()LaN/P;
    .locals 1

    iget-object v0, p0, LaN/X;->a:LaN/P;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, LaN/X;->d:J

    return-void
.end method

.method protected a(J[LaN/U;)V
    .locals 0

    iput-wide p1, p0, LaN/X;->c:J

    iput-object p3, p0, LaN/X;->b:[LaN/U;

    return-void
.end method

.method public a([B)V
    .locals 7

    const/4 v4, 0x6

    const/4 v0, 0x0

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1, v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const v2, 0x54524133

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Bad traffic header"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    array-length v2, p1

    sub-int/2addr v2, v4

    invoke-static {p1, v4, v2, v1}, Lcom/google/googlenav/common/io/i;->a([BIII)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->a([B)Ljava/io/DataInput;

    move-result-object v1

    invoke-interface {v1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v2

    invoke-interface {v1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v3

    invoke-interface {v1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v4

    new-array v5, v4, [LaN/U;

    :goto_0
    if-ge v0, v4, :cond_1

    invoke-static {v1, v2, v3}, LaN/U;->a(Ljava/io/DataInput;II)LaN/U;

    move-result-object v6

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, v5}, LaN/X;->a(J[LaN/U;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, LaN/X;->b:[LaN/U;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()[LaN/U;
    .locals 1

    iget-object v0, p0, LaN/X;->b:[LaN/U;

    return-object v0
.end method

.method public d()J
    .locals 2

    invoke-virtual {p0}, LaN/X;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LaN/X;->c:J

    goto :goto_0
.end method

.method e()J
    .locals 2

    iget-wide v0, p0, LaN/X;->d:J

    return-wide v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LaN/X;->b:[LaN/U;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/X;->b:[LaN/U;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
