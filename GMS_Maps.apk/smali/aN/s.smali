.class public LaN/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:J

.field private static final j:LaN/Y;


# instance fields
.field private final b:[LaN/B;

.field private final c:I

.field private final d:I

.field private final e:Lcom/google/googlenav/common/a;

.field private f:Z

.field private g:J

.field private h:J

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x3e8

    sput-wide v0, LaN/s;->a:J

    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, LaN/s;->j:LaN/Y;

    return-void
.end method

.method public constructor <init>(LaN/B;LaN/B;II)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v1, v0, [LaN/B;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    int-to-long v4, p3

    move-object v0, p0

    move v6, p4

    invoke-direct/range {v0 .. v6}, LaN/s;-><init>([LaN/B;IIJI)V

    return-void
.end method

.method public constructor <init>([LaN/B;IIJI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaN/s;->b:[LaN/B;

    iput p2, p0, LaN/s;->c:I

    iput p3, p0, LaN/s;->d:I

    iput p6, p0, LaN/s;->i:I

    iput-wide p4, p0, LaN/s;->h:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, LaN/s;->e:Lcom/google/googlenav/common/a;

    iget-object v0, p0, LaN/s;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/s;->g:J

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/s;->f:Z

    return-void
.end method

.method private a(III)I
    .locals 4

    sub-int v0, p2, p1

    int-to-long v0, v0

    int-to-long v2, p3

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/2addr v0, p1

    return v0
.end method

.method static a(JJJI)J
    .locals 6

    const-wide/16 v0, 0x0

    cmp-long v2, p4, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x1

    if-ne p6, v0, :cond_1

    mul-long v0, p0, p2

    div-long/2addr v0, p4

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    if-ne p6, v0, :cond_2

    mul-long v0, p4, p4

    sub-long v2, p4, p2

    sub-long v4, p4, p2

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    mul-long/2addr v0, p0

    mul-long v2, p4, p4

    div-long/2addr v0, v2

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x3

    mul-long/2addr v0, p2

    mul-long/2addr v0, p2

    mul-long/2addr v0, p4

    const-wide/16 v2, 0x2

    mul-long/2addr v2, p2

    mul-long/2addr v2, p2

    mul-long/2addr v2, p2

    sub-long/2addr v0, v2

    mul-long/2addr v0, p0

    mul-long v2, p4, p4

    mul-long/2addr v2, p4

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a(LaN/B;IILaN/Y;)LaN/s;
    .locals 10

    const/4 v2, 0x0

    mul-int v0, p1, p1

    mul-int v1, p2, p2

    add-int/2addr v0, v1

    invoke-static {}, LaN/s;->d()I

    move-result v1

    div-int v1, v0, v1

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(I)I

    move-result v0

    invoke-static {}, LaN/s;->c()I

    move-result v3

    if-le v1, v3, :cond_2

    invoke-static {}, LaN/s;->e()I

    move-result v1

    mul-int/2addr v1, p1

    div-int p1, v1, v0

    invoke-static {}, LaN/s;->e()I

    move-result v1

    mul-int/2addr v1, p2

    div-int p2, v1, v0

    invoke-static {}, LaN/s;->c()I

    move-result v1

    invoke-static {}, LaN/s;->e()I

    move-result v0

    move v3, v0

    move v0, v1

    :goto_0
    if-nez v3, :cond_0

    move v0, v2

    move v1, v2

    :goto_1
    const-wide/16 v4, 0x3e8

    int-to-long v6, v3

    mul-long v3, v4, v6

    invoke-static {}, LaN/s;->d()I

    move-result v5

    int-to-long v5, v5

    div-long v4, v3, v5

    int-to-long v6, v1

    const-wide/32 v8, 0x15752a00

    mul-long/2addr v6, v8

    invoke-virtual {p3}, LaN/Y;->b()I

    move-result v3

    int-to-long v8, v3

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0xaba9500

    div-long/2addr v6, v8

    long-to-int v3, v6

    add-int/lit8 v3, v3, 0x1

    div-int v6, v1, v3

    div-int v7, v0, v3

    add-int/lit8 v0, v3, 0x1

    new-array v1, v0, [LaN/B;

    aput-object p0, v1, v2

    move v0, v2

    :goto_2
    if-ge v0, v3, :cond_1

    add-int/lit8 v8, v0, 0x1

    aget-object v9, v1, v0

    invoke-virtual {v9, v6, v7, p3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v9

    aput-object v9, v1, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    mul-int v1, v0, p1

    div-int/2addr v1, v3

    neg-int v1, v1

    mul-int/2addr v0, p2

    div-int/2addr v0, v3

    neg-int v0, v0

    goto :goto_1

    :cond_1
    new-instance v0, LaN/s;

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, LaN/s;-><init>([LaN/B;IIJI)V

    return-object v0

    :cond_2
    move v3, v0

    move v0, v1

    goto :goto_0
.end method

.method private static c()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->K()I

    move-result v0

    mul-int/lit16 v0, v0, 0x359

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method private static d()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->s()I

    move-result v0

    mul-int/lit16 v0, v0, 0x1e24

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method private static e()I
    .locals 2

    invoke-static {}, LaN/s;->c()I

    move-result v0

    invoke-static {}, LaN/s;->d()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()LaN/B;
    .locals 11

    const/4 v7, 0x1

    const-wide/16 v9, 0x2710

    const/4 v8, 0x0

    iget-object v0, p0, LaN/s;->e:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iget-wide v0, p0, LaN/s;->g:J

    iget-wide v4, p0, LaN/s;->h:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    iget v0, p0, LaN/s;->d:I

    iget-object v1, p0, LaN/s;->b:[LaN/B;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1

    :cond_0
    iput-boolean v7, p0, LaN/s;->f:Z

    iget-object v0, p0, LaN/s;->b:[LaN/B;

    iget v1, p0, LaN/s;->d:I

    iget-object v2, p0, LaN/s;->b:[LaN/B;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, LaN/s;->d:I

    iget v1, p0, LaN/s;->c:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    mul-long/2addr v0, v9

    iget-wide v4, p0, LaN/s;->g:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, LaN/s;->h:J

    iget v6, p0, LaN/s;->i:I

    invoke-static/range {v0 .. v6}, LaN/s;->a(JJJI)J

    move-result-wide v1

    iget v0, p0, LaN/s;->c:I

    int-to-long v3, v0

    div-long v5, v1, v9

    add-long/2addr v3, v5

    long-to-int v0, v3

    iget-object v3, p0, LaN/s;->b:[LaN/B;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v0, p0, LaN/s;->d:I

    iget v4, p0, LaN/s;->c:I

    if-le v0, v4, :cond_3

    move v0, v7

    :goto_1
    add-int/2addr v0, v3

    iget-object v4, p0, LaN/s;->b:[LaN/B;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    rem-long v0, v1, v9

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    long-to-int v1, v0

    iget-object v0, p0, LaN/s;->b:[LaN/B;

    aget-object v0, v0, v3

    sget-object v2, LaN/s;->j:LaN/Y;

    invoke-virtual {v0, v2}, LaN/B;->a(LaN/Y;)I

    move-result v0

    iget-object v2, p0, LaN/s;->b:[LaN/B;

    aget-object v2, v2, v4

    sget-object v5, LaN/s;->j:LaN/Y;

    invoke-virtual {v2, v5}, LaN/B;->a(LaN/Y;)I

    move-result v2

    sub-int v0, v2, v0

    sget-object v2, LaN/s;->j:LaN/Y;

    invoke-virtual {v2}, LaN/Y;->b()I

    move-result v2

    neg-int v5, v2

    div-int/lit8 v5, v5, 0x2

    if-ge v0, v5, :cond_4

    add-int/2addr v0, v2

    :cond_2
    :goto_2
    iget-object v2, p0, LaN/s;->b:[LaN/B;

    aget-object v2, v2, v3

    sget-object v5, LaN/s;->j:LaN/Y;

    invoke-virtual {v2, v5}, LaN/B;->b(LaN/Y;)I

    move-result v2

    iget-object v5, p0, LaN/s;->b:[LaN/B;

    aget-object v4, v5, v4

    sget-object v5, LaN/s;->j:LaN/Y;

    invoke-virtual {v4, v5}, LaN/B;->b(LaN/Y;)I

    move-result v4

    iget-object v5, p0, LaN/s;->b:[LaN/B;

    aget-object v3, v5, v3

    invoke-direct {p0, v8, v0, v1}, LaN/s;->a(III)I

    move-result v0

    sub-int v2, v4, v2

    invoke-direct {p0, v8, v2, v1}, LaN/s;->a(III)I

    move-result v1

    sget-object v2, LaN/s;->j:LaN/Y;

    invoke-virtual {v3, v0, v1, v2}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1

    :cond_4
    div-int/lit8 v5, v2, 0x2

    if-le v0, v5, :cond_2

    sub-int/2addr v0, v2

    goto :goto_2
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, LaN/s;->f:Z

    return v0
.end method
