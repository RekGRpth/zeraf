.class public LaN/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/o;
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;


# instance fields
.field private final a:Ljava/util/Hashtable;

.field private final b:Ljava/util/Hashtable;

.field private volatile c:LaN/l;

.field private final d:Ljava/util/Vector;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaN/k;->e:J

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LaN/k;->f:J

    return-void
.end method

.method static synthetic a(LaN/k;)Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    return-object v0
.end method

.method private declared-synchronized a(LaN/P;LaN/n;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LaN/k;->c(LaN/P;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LaN/k;->c:LaN/l;

    if-nez v0, :cond_1

    new-instance v0, LaN/l;

    invoke-direct {v0, p0}, LaN/l;-><init>(LaN/k;)V

    iput-object v0, p0, LaN/k;->c:LaN/l;

    :cond_1
    iget-object v0, p0, LaN/k;->c:LaN/l;

    invoke-virtual {v0, p2}, LaN/l;->a(LaN/n;)V

    iget-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(LaN/k;)Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic c(LaN/k;)Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    return-object v0
.end method

.method private c(LaN/P;)Z
    .locals 3

    invoke-virtual {p1}, LaN/P;->e()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v2

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/m;

    invoke-interface {v0}, LaN/m;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/layer/m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized a(LaN/P;)LaN/n;
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, LaN/k;->a(LaN/P;Z)LaN/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/P;Z)LaN/n;
    .locals 10

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    iget-object v1, p0, LaN/k;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/n;

    if-eqz v0, :cond_2

    iget-wide v2, p0, LaN/k;->e:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    invoke-virtual {v0}, LaN/n;->a()J

    move-result-wide v4

    sub-long v6, v2, v4

    if-eqz p2, :cond_1

    if-nez v1, :cond_1

    const-wide/high16 v8, -0x8000000000000000L

    cmp-long v1, v4, v8

    if-eqz v1, :cond_1

    iget-wide v4, p0, LaN/k;->e:J

    cmp-long v1, v6, v4

    if-lez v1, :cond_1

    iget-wide v4, p0, LaN/k;->f:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    iget-object v1, p0, LaN/k;->c:LaN/l;

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0, p1, v0}, LaN/k;->a(LaN/P;LaN/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    if-eqz v1, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    if-nez p2, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    :try_start_1
    new-instance v0, LaN/n;

    invoke-direct {v0, p1}, LaN/n;-><init>(LaN/P;)V

    invoke-direct {p0, p1, v0}, LaN/k;->a(LaN/P;LaN/n;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    return-void
.end method

.method public a(LaN/m;)V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, LaN/m;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    iget-wide v2, p0, LaN/k;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, LaN/k;->e:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    :cond_0
    iput-wide v0, p0, LaN/k;->e:J

    :cond_1
    invoke-virtual {p0}, LaN/k;->a()V

    return-void
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/m;

    invoke-interface {v0, p1, p2}, LaN/m;->a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    invoke-virtual {p0}, LaN/k;->a()V

    return-void
.end method

.method public declared-synchronized b(LaN/P;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaN/n;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LaN/n;->d()V

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LaN/m;)V
    .locals 8

    const-wide/16 v6, -0x1

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    iput-wide v6, p0, LaN/k;->e:J

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/m;

    invoke-interface {v0}, LaN/m;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-eqz v0, :cond_1

    iget-wide v4, p0, LaN/k;->e:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    iget-wide v4, p0, LaN/k;->e:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    :cond_0
    iput-wide v2, p0, LaN/k;->e:J

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LaN/k;->a()V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, LaN/k;->d:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/m;

    invoke-interface {v0}, LaN/m;->c()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    return-void
.end method

.method public declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/k;->c:LaN/l;

    if-eqz v0, :cond_0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LaN/k;->c:LaN/l;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaN/k;->c:LaN/l;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LaN/k;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LaN/k;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 9

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    iget-object v1, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/n;

    invoke-virtual {v1}, LaN/n;->b()J

    move-result-wide v5

    const-wide/16 v7, 0xfa0

    add-long/2addr v5, v7

    cmp-long v1, v5, v2

    if-gez v1, :cond_0

    invoke-virtual {p0, v0}, LaN/k;->b(LaN/P;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized g()Lcom/google/googlenav/common/util/l;
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v0, "LayerService"

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    iget-object v0, p0, LaN/k;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    invoke-virtual {v0}, LaN/n;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    invoke-virtual {v0}, LaN/n;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_1
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/k;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
