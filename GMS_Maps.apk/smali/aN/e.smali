.class LaN/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaN/P;

.field private b:I

.field private final c:I

.field private d:LaN/f;


# direct methods
.method public constructor <init>(LaN/I;)V
    .locals 4

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v0

    invoke-virtual {p1}, LaN/I;->g()J

    move-result-wide v1

    invoke-virtual {p1}, LaN/I;->j()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, LaN/e;-><init>(LaN/P;JI)V

    return-void
.end method

.method private constructor <init>(LaN/P;JI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaN/e;->a:LaN/P;

    invoke-virtual {p0, p2, p3}, LaN/e;->a(J)V

    iput p4, p0, LaN/e;->c:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;)LaN/e;
    .locals 6

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v1

    invoke-static {p0}, LaN/P;->a(Ljava/io/DataInput;)LaN/P;

    move-result-object v2

    new-instance v3, LaN/e;

    int-to-long v4, v0

    invoke-direct {v3, v2, v4, v5, v1}, LaN/e;-><init>(LaN/P;JI)V

    return-object v3
.end method


# virtual methods
.method public a()LaN/P;
    .locals 1

    iget-object v0, p0, LaN/e;->a:LaN/P;

    return-object v0
.end method

.method public a(J)V
    .locals 4

    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/32 v2, 0x424b1f68

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, LaN/e;->b:I

    return-void
.end method

.method public a(LaN/f;)V
    .locals 2

    iget-object v0, p0, LaN/e;->d:LaN/f;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FlashRecord already set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, LaN/e;->d:LaN/f;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget v0, p0, LaN/e;->b:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget v0, p0, LaN/e;->c:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeShort(I)V

    iget-object v0, p0, LaN/e;->a:LaN/P;

    invoke-virtual {v0, p1}, LaN/P;->a(Ljava/io/DataOutput;)V

    return-void
.end method

.method public b()J
    .locals 4

    iget v0, p0, LaN/e;->b:I

    int-to-long v0, v0

    const-wide/32 v2, 0x424b1f68

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LaN/e;->c:I

    add-int/lit8 v0, v0, 0xc

    return v0
.end method

.method public d()LaN/f;
    .locals 1

    iget-object v0, p0, LaN/e;->d:LaN/f;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LaN/e;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LaN/e;

    iget v2, p0, LaN/e;->c:I

    iget v3, p1, LaN/e;->c:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, LaN/e;->a:LaN/P;

    if-nez v2, :cond_4

    iget-object v2, p1, LaN/e;->a:LaN/P;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, LaN/e;->a:LaN/P;

    iget-object v1, p1, LaN/e;->a:LaN/P;

    invoke-virtual {v0, v1}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, LaN/e;->a:LaN/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/e;->a:LaN/P;

    invoke-virtual {v0}, LaN/P;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, LaN/e;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaN/e;->a:LaN/P;

    invoke-virtual {v1}, LaN/P;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaN/e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
