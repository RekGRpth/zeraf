.class LaN/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field final synthetic a:LaN/k;

.field private b:Z

.field private final c:Ljava/util/Vector;


# direct methods
.method public constructor <init>(LaN/k;)V
    .locals 1

    iput-object p1, p0, LaN/l;->a:LaN/k;

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/l;->c:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/l;->b:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized a(LaN/n;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaN/l;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Adding tiles to closed request!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaN/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    iget-object v0, p0, LaN/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 8

    const/16 v7, 0xd

    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p1, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v4, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_0

    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    aput-object v5, v4, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-virtual {v0, v3, v4}, LaN/k;->a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZI)V
    .locals 8

    const/4 v1, 0x1

    const/4 v5, 0x3

    const/4 v7, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, LaN/P;->a(BIILaN/Y;)LaN/P;

    move-result-object v1

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v2, v0, [Lcom/google/googlenav/layer/j;

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    new-instance v3, Lcom/google/googlenav/layer/j;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/layer/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-static {v0}, LaN/k;->b(LaN/k;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    if-nez v0, :cond_4

    new-instance v0, LaN/n;

    invoke-direct {v0, v1}, LaN/n;-><init>(LaN/P;)V

    :cond_1
    :goto_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    int-to-long v5, p3

    add-long/2addr v3, v5

    invoke-virtual {v0}, LaN/n;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz p2, :cond_5

    :cond_2
    invoke-virtual {v0, v2, v3, v4}, LaN/n;->a([Lcom/google/googlenav/layer/j;J)V

    :goto_2
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    invoke-virtual {v0, v2, v3, v4}, LaN/n;->a([BJ)V

    :cond_3
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LaN/l;->a:LaN/k;

    invoke-static {v2}, LaN/k;->c(LaN/k;)Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-static {v0}, LaN/k;->b(LaN/k;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-virtual {v0}, LaN/k;->c()V

    return-void

    :cond_4
    invoke-virtual {v0}, LaN/n;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p2, :cond_1

    new-instance v0, LaN/n;

    invoke-direct {v0, v1}, LaN/n;-><init>(LaN/P;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v2, v3, v4}, LaN/n;->b([Lcom/google/googlenav/layer/j;J)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 10

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/4 v3, 0x1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaN/l;->b:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dB;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v0, 0x100

    invoke-virtual {v4, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    iget-object v1, p0, LaN/l;->c:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v5

    move v1, v0

    move v2, v3

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v0, p0, LaN/l;->c:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/n;

    invoke-virtual {v0}, LaN/n;->c()LaN/P;

    move-result-object v0

    invoke-virtual {v0}, LaN/P;->e()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/wireless/googlenav/proto/j2me/hs;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v7, 0x8

    invoke-virtual {v6, v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, LaN/P;->c()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, LaN/P;->d()I

    move-result v0

    invoke-virtual {v6, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    invoke-virtual {v6, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v8, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-static {v0}, LaN/k;->a(LaN/k;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    iget-object v0, p0, LaN/l;->a:LaN/k;

    invoke-static {v0}, LaN/k;->a(LaN/k;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/m;

    invoke-interface {v0}, LaN/m;->b()Lcom/google/googlenav/layer/m;

    move-result-object v3

    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v0, 0x15

    invoke-virtual {v3}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3}, Lcom/google/googlenav/layer/m;->e()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    if-eqz v6, :cond_1

    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_1

    const/16 v7, 0x16

    aget-object v8, v6, v0

    invoke-virtual {v5, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_1
    invoke-virtual {v3, v2}, Lcom/google/googlenav/layer/m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4, v9, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_3
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v4, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dB;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    invoke-virtual {p0, v0}, LaN/l;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    const/4 v0, 0x0

    :cond_0
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/dB;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v3

    if-eq v3, v4, :cond_1

    invoke-virtual {p0, v2, v5, v0}, LaN/l;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZI)V

    add-int/lit8 v0, v0, 0x1

    :cond_1
    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return v5
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x24

    return v0
.end method
