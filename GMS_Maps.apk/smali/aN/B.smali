.class public LaN/B;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/g;


# static fields
.field static a:[I

.field static b:[I

.field private static final g:LaN/Y;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, LaN/B;->a:[I

    sput-object v0, LaN/B;->b:[I

    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, LaN/B;->g:LaN/Y;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 3

    const v1, 0x4c4b400

    const v0, -0x4c4b400

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, LaN/B;->a(I)I

    move-result v2

    if-le p1, v1, :cond_1

    :goto_0
    if-ge v1, v0, :cond_0

    :goto_1
    iput v0, p0, LaN/B;->e:I

    iput v2, p0, LaN/B;->f:I

    sget-object v1, LaN/B;->g:LaN/Y;

    invoke-static {v2, v1}, LaN/B;->a(ILaN/Y;)I

    move-result v1

    iput v1, p0, LaN/B;->c:I

    sget-object v1, LaN/B;->g:LaN/Y;

    invoke-static {v0, v1}, LaN/B;->b(ILaN/Y;)I

    move-result v0

    iput v0, p0, LaN/B;->d:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, p1

    goto :goto_0
.end method

.method public constructor <init>(III)V
    .locals 4

    const/16 v3, 0x16

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->b()I

    move-result v1

    invoke-static {p1, v1}, LaN/B;->d(II)I

    move-result v1

    invoke-virtual {v0}, LaN/Y;->b()I

    move-result v2

    invoke-static {p2, v2}, LaN/B;->e(II)I

    move-result v2

    invoke-virtual {v0, v1, v3}, LaN/Y;->a(II)I

    move-result v1

    iput v1, p0, LaN/B;->c:I

    invoke-virtual {v0, v2, v3}, LaN/Y;->a(II)I

    move-result v0

    iput v0, p0, LaN/B;->d:I

    iget v0, p0, LaN/B;->d:I

    sget-object v1, LaN/B;->g:LaN/Y;

    invoke-static {v0, v1}, LaN/B;->c(ILaN/Y;)I

    move-result v0

    iput v0, p0, LaN/B;->e:I

    iget v0, p0, LaN/B;->c:I

    sget-object v1, LaN/B;->g:LaN/Y;

    invoke-static {v0, v1}, LaN/B;->d(ILaN/Y;)I

    move-result v0

    iput v0, p0, LaN/B;->f:I

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 4

    const/16 v3, 0x16

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p5}, LaN/B;->a(I)I

    move-result v0

    iput p4, p0, LaN/B;->e:I

    iput v0, p0, LaN/B;->f:I

    invoke-static {p3}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->b()I

    move-result v1

    invoke-static {p1, v1}, LaN/B;->d(II)I

    move-result v1

    invoke-virtual {v0}, LaN/Y;->b()I

    move-result v2

    invoke-static {p2, v2}, LaN/B;->e(II)I

    move-result v2

    invoke-virtual {v0, v1, v3}, LaN/Y;->a(II)I

    move-result v1

    iput v1, p0, LaN/B;->c:I

    invoke-virtual {v0, v2, v3}, LaN/Y;->a(II)I

    move-result v0

    iput v0, p0, LaN/B;->d:I

    return-void
.end method

.method private static a(I)I
    .locals 3

    const v2, 0x15752a00

    move v0, p0

    :goto_0
    const v1, -0xaba9500

    if-ge v0, v1, :cond_0

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    :goto_1
    const v1, 0xaba9500

    if-le v0, v1, :cond_1

    sub-int/2addr v0, v2

    goto :goto_1

    :cond_1
    return v0
.end method

.method public static a(ILaN/Y;)I
    .locals 6

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    int-to-long v2, p0

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x15752a00

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static a(JI)J
    .locals 4

    invoke-static {p0, p1}, LaN/B;->b(J)J

    move-result-wide v0

    const-wide/16 v2, 0xfa

    mul-long/2addr v0, v2

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const v3, 0xf4240

    div-int/2addr v2, v3

    invoke-static {v2}, Lcom/google/googlenav/common/util/j;->a(I)I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(III)LaN/B;
    .locals 1

    new-instance v0, LaN/B;

    invoke-direct {v0, p0, p1, p2}, LaN/B;-><init>(III)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    new-instance v2, LaN/B;

    invoke-direct {v2, v0, v1}, LaN/B;-><init>(II)V

    return-object v2
.end method

.method public static a(Ljava/io/DataInput;)LaN/B;
    .locals 3

    new-instance v0, LaN/B;

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LaN/B;
    .locals 4

    const/4 v1, 0x0

    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/16 v0, 0x2c

    :try_start_0
    invoke-static {p0, v0}, Lau/b;->a(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, LaN/B;->b(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, LaN/B;->b(Ljava/lang/String;)I

    move-result v3

    new-instance v0, LaN/B;

    invoke-direct {v0, v2, v3}, LaN/B;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(II)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaN/B;Ljava/io/DataOutput;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-direct {p0, p1}, LaN/B;->a(Ljava/io/DataOutput;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    goto :goto_0
.end method

.method private static a(Ljava/io/DataInput;[I)V
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    aput v1, p1, v0

    const/4 v0, 0x1

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    aget v1, p1, v1

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    shl-int/lit8 v2, v2, 0x10

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    or-int/2addr v2, v3

    add-int/2addr v1, v2

    aput v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/io/DataOutput;)V
    .locals 1

    iget v0, p0, LaN/B;->e:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget v0, p0, LaN/B;->f:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    return-void
.end method

.method public static a(D)Z
    .locals 2

    const-wide v0, -0x3f99800000000000L

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    const-wide v0, 0x4066800000000000L

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)I
    .locals 2

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const v1, 0xf4240

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static b(ILaN/Y;)I
    .locals 12

    const-wide/32 v10, 0xf4240

    invoke-static {p0}, LaN/B;->b(I)I

    move-result v0

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0xf4240

    rem-int/2addr v1, v2

    invoke-static {}, LaN/B;->i()[I

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    aget v3, v2, v3

    mul-int/lit8 v3, v3, -0x1

    aget v4, v2, v0

    mul-int/lit8 v4, v4, 0x3

    add-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x1

    aget v4, v2, v4

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x2

    aget v4, v2, v4

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x6

    add-int/lit8 v4, v0, -0x1

    aget v4, v2, v4

    mul-int/lit8 v4, v4, 0x3

    aget v5, v2, v0

    mul-int/lit8 v5, v5, 0x6

    sub-int/2addr v4, v5

    add-int/lit8 v5, v0, 0x1

    aget v5, v2, v5

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v0, -0x1

    aget v5, v2, v5

    mul-int/lit8 v5, v5, -0x2

    aget v6, v2, v0

    mul-int/lit8 v6, v6, 0x3

    sub-int/2addr v5, v6

    add-int/lit8 v6, v0, 0x1

    aget v6, v2, v6

    mul-int/lit8 v6, v6, 0x6

    add-int/2addr v5, v6

    add-int/lit8 v6, v0, 0x2

    aget v6, v2, v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x6

    aget v0, v2, v0

    int-to-long v2, v3

    int-to-long v6, v1

    mul-long/2addr v2, v6

    int-to-long v6, v1

    mul-long/2addr v2, v6

    div-long/2addr v2, v10

    int-to-long v6, v1

    mul-long/2addr v2, v6

    div-long/2addr v2, v10

    div-long/2addr v2, v10

    int-to-long v6, v4

    int-to-long v8, v1

    mul-long/2addr v6, v8

    int-to-long v8, v1

    mul-long/2addr v6, v8

    div-long/2addr v6, v10

    div-long/2addr v6, v10

    add-long/2addr v2, v6

    int-to-long v4, v5

    int-to-long v6, v1

    mul-long/2addr v4, v6

    div-long/2addr v4, v10

    add-long v1, v2, v4

    int-to-long v3, v0

    add-long v0, v1, v3

    long-to-int v0, v0

    if-gez p0, :cond_0

    neg-int v0, v0

    :cond_0
    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v1

    int-to-long v1, v1

    const-wide/16 v3, 0x2

    div-long/2addr v1, v3

    invoke-static {v0, p1}, LaN/B;->e(ILaN/Y;)I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    int-to-long v3, v0

    sub-long v0, v1, v3

    long-to-int v0, v0

    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 10

    const/16 v5, 0x2e

    const/4 v9, 0x6

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v3, :cond_5

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v5

    array-length v0, v5

    const/4 v6, 0x2

    if-le v0, v6, :cond_0

    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Coordinate has more than one decimal point: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget-object v0, v5, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget-object v0, v5, v2

    const-string v7, "-"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_0
    aget-object v7, v5, v1

    aget-object v5, v5, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v9, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x30

    if-lt v7, v8, :cond_1

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v7, 0x39

    if-le v2, v7, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid fractional part in \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const/16 v2, 0x30

    invoke-static {v5, v9, v2}, Lau/b;->a(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const v4, 0xf4240

    mul-int/2addr v4, v6

    if-eqz v0, :cond_4

    move v1, v3

    :cond_4
    mul-int v0, v2, v1

    add-int/2addr v0, v4

    :goto_1
    return v0

    :cond_5
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const v1, 0xf4240

    mul-int/2addr v0, v1

    goto :goto_1
.end method

.method public static b(J)J
    .locals 4

    const-wide/16 v0, 0x24

    mul-long/2addr v0, p0

    const-wide/16 v2, 0x4

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static b(D)Z
    .locals 2

    const-wide/high16 v0, -0x3fac000000000000L

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    const-wide/high16 v0, 0x4054000000000000L

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(ILaN/Y;)I
    .locals 14

    const v5, 0x4c4b40

    const v0, 0x4c4b400

    const-wide/32 v12, 0x4c4b40

    invoke-static {}, LaN/B;->j()[I

    move-result-object v1

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, p0

    invoke-static {v2, p1}, LaN/B;->f(ILaN/Y;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    div-int/2addr v3, v5

    add-int/lit8 v3, v3, 0x1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x2

    if-lt v3, v4, :cond_2

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v0, -0x4c4b400

    goto :goto_0

    :cond_2
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    rem-int/2addr v4, v5

    add-int/lit8 v5, v3, -0x1

    aget v5, v1, v5

    mul-int/lit8 v5, v5, -0x1

    aget v6, v1, v3

    mul-int/lit8 v6, v6, 0x3

    add-int/2addr v5, v6

    add-int/lit8 v6, v3, 0x1

    aget v6, v1, v6

    mul-int/lit8 v6, v6, 0x3

    sub-int/2addr v5, v6

    add-int/lit8 v6, v3, 0x2

    aget v6, v1, v6

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x6

    add-int/lit8 v6, v3, -0x1

    aget v6, v1, v6

    mul-int/lit8 v6, v6, 0x3

    aget v7, v1, v3

    mul-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    add-int/lit8 v7, v3, 0x1

    aget v7, v1, v7

    mul-int/lit8 v7, v7, 0x3

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x6

    add-int/lit8 v7, v3, -0x1

    aget v7, v1, v7

    mul-int/lit8 v7, v7, -0x2

    aget v8, v1, v3

    mul-int/lit8 v8, v8, 0x3

    sub-int/2addr v7, v8

    add-int/lit8 v8, v3, 0x1

    aget v8, v1, v8

    mul-int/lit8 v8, v8, 0x6

    add-int/2addr v7, v8

    add-int/lit8 v8, v3, 0x2

    aget v8, v1, v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x6

    aget v1, v1, v3

    int-to-long v8, v5

    int-to-long v10, v4

    mul-long/2addr v8, v10

    int-to-long v10, v4

    mul-long/2addr v8, v10

    div-long/2addr v8, v12

    int-to-long v10, v4

    mul-long/2addr v8, v10

    div-long/2addr v8, v12

    div-long/2addr v8, v12

    int-to-long v5, v6

    int-to-long v10, v4

    mul-long/2addr v5, v10

    int-to-long v10, v4

    mul-long/2addr v5, v10

    div-long/2addr v5, v12

    div-long/2addr v5, v12

    add-long/2addr v5, v8

    int-to-long v7, v7

    int-to-long v3, v4

    mul-long/2addr v3, v7

    div-long/2addr v3, v12

    add-long/2addr v3, v5

    int-to-long v5, v1

    add-long/2addr v3, v5

    long-to-int v1, v3

    if-le v1, v0, :cond_3

    :goto_1
    if-gez v2, :cond_0

    neg-int v0, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static c(J)J
    .locals 4

    const-wide/16 v0, 0x4

    mul-long/2addr v0, p0

    const-wide/16 v2, 0x24

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private static d(II)I
    .locals 1

    move v0, p0

    :goto_0
    if-lt v0, p1, :cond_0

    sub-int/2addr v0, p1

    goto :goto_0

    :cond_0
    :goto_1
    if-gez v0, :cond_1

    add-int/2addr v0, p1

    goto :goto_1

    :cond_1
    return v0
.end method

.method private static d(ILaN/Y;)I
    .locals 4

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p0, v0

    int-to-long v0, v0

    const-wide/32 v2, 0x15752a00

    mul-long/2addr v0, v2

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static e(II)I
    .locals 0

    if-gez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return p0

    :cond_1
    if-lt p0, p1, :cond_0

    add-int/lit8 p0, p1, -0x1

    goto :goto_0
.end method

.method private static e(ILaN/Y;)I
    .locals 4

    int-to-long v0, p0

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xa

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15752a00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static f(ILaN/Y;)I
    .locals 4

    int-to-long v0, p0

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    invoke-virtual {p1}, LaN/Y;->b()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static declared-synchronized i()[I
    .locals 3

    const-class v1, LaN/B;

    monitor-enter v1

    :try_start_0
    sget-object v0, LaN/B;->a:[I

    if-nez v0, :cond_0

    const/16 v0, 0x54

    new-array v0, v0, [I

    sput-object v0, LaN/B;->a:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0xfd

    :try_start_1
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a([B)Ljava/io/DataInput;

    move-result-object v0

    sget-object v2, LaN/B;->a:[I

    invoke-static {v0, v2}, LaN/B;->a(Ljava/io/DataInput;[I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    sget-object v0, LaN/B;->a:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Can\'t read mercator.dat"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :array_0
    .array-data 1
        -0x1t
        -0x10t
        -0x43t
        -0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x43t
        -0x5dt
        0xft
        0x46t
        0x6t
        0xft
        0x49t
        -0x67t
        0xft
        0x4et
        0x61t
        0xft
        0x54t
        0x5et
        0xft
        0x5bt
        -0x6dt
        0xft
        0x64t
        0x2t
        0xft
        0x6dt
        -0x50t
        0xft
        0x78t
        -0x61t
        0xft
        -0x7ct
        -0x2ct
        0xft
        -0x6et
        0x54t
        0xft
        -0x5ft
        0x26t
        0xft
        -0x4ft
        0x4et
        0xft
        -0x3et
        -0x2dt
        0xft
        -0x2bt
        -0x43t
        0xft
        -0x16t
        0x15t
        0xft
        -0x1t
        -0x1ft
        0x10t
        0x17t
        0x2dt
        0x10t
        0x30t
        0x1t
        0x10t
        0x4at
        0x6bt
        0x10t
        0x66t
        0x74t
        0x10t
        -0x7ct
        0x2bt
        0x10t
        -0x5dt
        -0x64t
        0x10t
        -0x3ct
        -0x29t
        0x10t
        -0x19t
        -0x13t
        0x11t
        0xct
        -0x12t
        0x11t
        0x33t
        -0x14t
        0x11t
        0x5ct
        -0x4t
        0x11t
        -0x78t
        0x34t
        0x11t
        -0x4bt
        -0x57t
        0x11t
        -0x1bt
        0x76t
        0x12t
        0x17t
        -0x4ct
        0x12t
        0x4ct
        -0x7ft
        0x12t
        -0x7dt
        -0x3t
        0x12t
        -0x42t
        0x46t
        0x12t
        -0x5t
        -0x7ct
        0x13t
        0x3bt
        -0x25t
        0x13t
        0x7ft
        0x77t
        0x13t
        -0x3at
        -0x7at
        0x14t
        0x11t
        0x38t
        0x14t
        0x5ft
        -0x3ct
        0x14t
        -0x4et
        0x64t
        0x15t
        0x9t
        0x57t
        0x15t
        0x64t
        -0x1bt
        0x15t
        -0x3bt
        0x56t
        0x16t
        0x2at
        -0x1t
        0x16t
        -0x6at
        0x3at
        0x17t
        0x7t
        0x6dt
        0x17t
        0x7ft
        0x2t
        0x17t
        -0x3t
        0x75t
        0x18t
        -0x7dt
        0x48t
        0x19t
        0x11t
        0x14t
        0x19t
        -0x59t
        0x78t
        0x1at
        0x47t
        0x2et
        0x1at
        -0xft
        0x3t
        0x1bt
        -0x5bt
        -0x27t
        0x1ct
        0x66t
        -0x4dt
        0x1dt
        0x34t
        -0x4dt
        0x1et
        0x11t
        0x1ft
        0x1et
        -0x3t
        0x6ft
        0x1ft
        -0x5t
        0x4at
        0x21t
        0xct
        -0x69t
        0x22t
        0x33t
        -0x78t
        0x23t
        0x72t
        -0x5bt
        0x24t
        -0x34t
        -0x1et
        0x26t
        0x45t
        -0x4ct
        0x27t
        -0x1ft
        0x2at
        0x29t
        -0x5ct
        0x19t
        0x2bt
        -0x6ct
        0x46t
        0x2dt
        -0x48t
        -0x5bt
        0x30t
        0x19t
        -0x54t
        0x32t
        -0x3ft
        -0x3ft
        0x35t
        -0x43t
        -0x2ft
        0x39t
        0x1et
        0x1ct
        0x3ct
        -0x9t
        0x69t
        0x41t
        0x64t
        -0x60t
        0x46t
        -0x77t
        0x52t
        0x4ct
        -0x6bt
        0x73t
        0x53t
        -0x35t
        0x4ft
        0x5ct
        -0x77t
        0x34t
        0x67t
        0x5at
        0xct
    .end array-data
.end method

.method private static declared-synchronized j()[I
    .locals 3

    const-class v1, LaN/B;

    monitor-enter v1

    :try_start_0
    sget-object v0, LaN/B;->b:[I

    if-nez v0, :cond_0

    const/16 v0, 0x8d

    new-array v0, v0, [I

    sput-object v0, LaN/B;->b:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x1a8

    :try_start_1
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a([B)Ljava/io/DataInput;

    move-result-object v0

    sget-object v2, LaN/B;->b:[I

    invoke-static {v0, v2}, LaN/B;->a(Ljava/io/DataInput;[I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    sget-object v0, LaN/B;->b:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "rmercator.dat is incorrect"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :array_0
    .array-data 1
        -0x1t
        -0x1ct
        -0x77t
        -0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x6ft
        0x2at
        0x1bt
        0x61t
        0x55t
        0x1bt
        0x4ct
        -0x52t
        0x1bt
        0x31t
        0x4bt
        0x1bt
        0xft
        0x4ft
        0x1at
        -0x1at
        -0x1at
        0x1at
        -0x48t
        0x3dt
        0x1at
        -0x7dt
        -0x72t
        0x1at
        0x49t
        0x16t
        0x1at
        0x9t
        0x19t
        0x19t
        -0x3dt
        -0x23t
        0x19t
        0x79t
        -0x51t
        0x19t
        0x2at
        -0x22t
        0x18t
        -0x29t
        -0x46t
        0x18t
        -0x80t
        -0x67t
        0x18t
        0x25t
        -0x33t
        0x17t
        -0x39t
        -0x52t
        0x17t
        0x66t
        -0x73t
        0x17t
        0x2t
        -0x40t
        0x16t
        -0x64t
        -0x65t
        0x16t
        0x34t
        0x6dt
        0x15t
        -0x36t
        -0x7at
        0x15t
        0x5ft
        0x30t
        0x14t
        -0xet
        -0x48t
        0x14t
        -0x7bt
        0x61t
        0x14t
        0x17t
        0x6ft
        0x13t
        -0x57t
        0x21t
        0x13t
        0x3at
        -0x4ct
        0x12t
        -0x34t
        0x5et
        0x12t
        0x5et
        0x55t
        0x11t
        -0x10t
        -0x38t
        0x11t
        -0x7dt
        -0x1at
        0x11t
        0x17t
        -0x2at
        0x10t
        -0x54t
        -0x40t
        0x10t
        0x42t
        -0x3dt
        0xft
        -0x26t
        0x2t
        0xft
        0x72t
        -0x6at
        0xft
        0xct
        -0x67t
        0xet
        -0x58t
        0x20t
        0xet
        0x45t
        0x40t
        0xdt
        -0x1ct
        0x7t
        0xdt
        -0x7ct
        -0x7at
        0xdt
        0x26t
        -0x3at
        0xct
        -0x36t
        -0x2dt
        0xct
        0x70t
        -0x4bt
        0xct
        0x18t
        0x71t
        0xbt
        -0x3et
        0xat
        0xbt
        0x6dt
        -0x79t
        0xbt
        0x1at
        -0x1bt
        0xat
        -0x36t
        0x28t
        0xat
        0x7bt
        0x4dt
        0xat
        0x2et
        0x52t
        0x9t
        -0x1dt
        0x34t
        0x9t
        -0x67t
        -0x10t
        0x9t
        0x52t
        -0x7et
        0x9t
        0xct
        -0x1dt
        0x8t
        -0x37t
        0xet
        0x8t
        -0x7at
        -0x2t
        0x8t
        0x46t
        -0x56t
        0x8t
        0x8t
        0xdt
        0x7t
        -0x35t
        0x1et
        0x7t
        -0x71t
        -0x29t
        0x7t
        0x56t
        0x2dt
        0x7t
        0x1et
        0x1bt
        0x6t
        -0x19t
        -0x68t
        0x6t
        -0x4et
        -0x66t
        0x6t
        0x7ft
        0x1at
        0x6t
        0x4dt
        0xft
        0x6t
        0x1ct
        0x72t
        0x5t
        -0x13t
        0x38t
        0x5t
        -0x41t
        0x59t
        0x5t
        -0x6et
        -0x32t
        0x5t
        0x67t
        -0x72t
        0x5t
        0x3dt
        -0x6ft
        0x5t
        0x14t
        -0x33t
        0x4t
        -0x13t
        0x3bt
        0x4t
        -0x3at
        -0x2bt
        0x4t
        -0x5ft
        -0x71t
        0x4t
        0x7dt
        0x66t
        0x4t
        0x5at
        0x4et
        0x4t
        0x38t
        0x41t
        0x4t
        0x17t
        0x3at
        0x3t
        -0x9t
        0x2ft
        0x3t
        -0x28t
        0x1at
        0x3t
        -0x47t
        -0xdt
        0x3t
        -0x64t
        -0x4bt
        0x3t
        -0x80t
        0x58t
        0x3t
        0x64t
        -0x29t
        0x3t
        0x4at
        0x2bt
        0x3t
        0x30t
        0x4ct
        0x3t
        0x17t
        0x37t
        0x2t
        -0x2t
        -0x1ct
        0x2t
        -0x19t
        0x4ft
        0x2t
        -0x30t
        0x72t
        0x2t
        -0x46t
        0x46t
        0x2t
        -0x5ct
        -0x38t
        0x2t
        -0x71t
        -0xft
        0x2t
        0x7bt
        -0x43t
        0x2t
        0x68t
        0x28t
        0x2t
        0x55t
        0x2bt
        0x2t
        0x42t
        -0x3dt
        0x2t
        0x30t
        -0x14t
        0x2t
        0x1ft
        -0x60t
        0x2t
        0xet
        -0x23t
        0x1t
        -0x2t
        -0x64t
        0x1t
        -0x12t
        -0x24t
        0x1t
        -0x21t
        -0x6at
        0x1t
        -0x30t
        -0x35t
        0x1t
        -0x3et
        0x72t
        0x1t
        -0x4ct
        -0x76t
        0x1t
        -0x59t
        0x11t
        0x1t
        -0x66t
        0x1t
        0x1t
        -0x73t
        0x58t
        0x1t
        -0x7ft
        0x12t
        0x1t
        0x75t
        0x2ft
        0x1t
        0x69t
        -0x59t
        0x1t
        0x5et
        0x7ct
        0x1t
        0x53t
        -0x58t
        0x1t
        0x49t
        0x2at
        0x1t
        0x3et
        -0x1t
        0x1t
        0x35t
        0x23t
        0x1t
        0x2bt
        -0x69t
        0x1t
        0x22t
        0x54t
        0x1t
        0x19t
        0x5bt
        0x1t
        0x10t
        -0x56t
        0x1t
        0x8t
        0x3ct
        0x1t
        0x0t
        0x11t
        0x0t
        -0x8t
        0x28t
        0x0t
        -0x10t
        0x7ct
        0x0t
        -0x17t
        0xdt
        0x0t
        -0x1ft
        -0x28t
        0x0t
        -0x26t
        -0x22t
        0x0t
        -0x2ct
        0x19t
        0x0t
        -0x33t
        -0x75t
        0x0t
        -0x39t
        0x30t
        0x0t
        -0x3ft
        0x8t
        0x0t
        -0x45t
        0x10t
        0x0t
        -0x4bt
        0x47t
    .end array-data
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(LaN/Y;)I
    .locals 3

    sget-object v0, LaN/B;->g:LaN/Y;

    iget v1, p0, LaN/B;->c:I

    invoke-virtual {p1}, LaN/Y;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/Y;->a(II)I

    move-result v0

    return v0
.end method

.method public a(J)J
    .locals 2

    iget v0, p0, LaN/B;->e:I

    invoke-static {p1, p2, v0}, LaN/B;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LaN/B;)J
    .locals 8

    const-wide/16 v6, 0x64

    iget v0, p0, LaN/B;->e:I

    iget v1, p1, LaN/B;->e:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, LaN/B;->e:I

    iget v3, p1, LaN/B;->e:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    mul-long/2addr v0, v2

    div-long/2addr v0, v6

    iget v2, p0, LaN/B;->f:I

    iget v3, p1, LaN/B;->f:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    iget v4, p0, LaN/B;->f:I

    iget v5, p1, LaN/B;->f:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    mul-long/2addr v2, v4

    div-long/2addr v2, v6

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(LaN/B;LaN/Y;)J
    .locals 6

    invoke-virtual {p1, p2}, LaN/B;->a(LaN/Y;)I

    move-result v0

    invoke-virtual {p0, p2}, LaN/B;->a(LaN/Y;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, p2}, LaN/B;->b(LaN/Y;)I

    move-result v1

    invoke-virtual {p0, p2}, LaN/B;->b(LaN/Y;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-long v2, v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    int-to-long v4, v1

    int-to-long v0, v1

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(IILaN/Y;)LaN/B;
    .locals 10

    const/16 v3, 0x16

    invoke-virtual {p3, p1, v3}, LaN/Y;->a(II)I

    move-result v0

    iget v1, p0, LaN/B;->c:I

    add-int/2addr v1, v0

    invoke-virtual {p3, p2, v3}, LaN/Y;->a(II)I

    move-result v0

    iget v2, p0, LaN/B;->d:I

    add-int/2addr v2, v0

    iget v4, p0, LaN/B;->e:I

    if-eqz p2, :cond_0

    sget-object v0, LaN/B;->g:LaN/Y;

    invoke-static {v2, v0}, LaN/B;->c(ILaN/Y;)I

    move-result v4

    :cond_0
    iget v5, p0, LaN/B;->f:I

    if-eqz p1, :cond_1

    int-to-long v6, p1

    const-wide/32 v8, 0x15752a00

    mul-long/2addr v6, v8

    invoke-virtual {p3}, LaN/Y;->b()I

    move-result v0

    int-to-long v8, v0

    div-long/2addr v6, v8

    long-to-int v0, v6

    add-int/2addr v5, v0

    :cond_1
    new-instance v0, LaN/B;

    invoke-direct/range {v0 .. v5}, LaN/B;-><init>(IIIII)V

    return-object v0
.end method

.method public b(LaN/Y;)I
    .locals 3

    sget-object v0, LaN/B;->g:LaN/Y;

    iget v1, p0, LaN/B;->d:I

    invoke-virtual {p1}, LaN/Y;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/Y;->a(II)I

    move-result v0

    return v0
.end method

.method public b(LaN/B;)J
    .locals 6

    iget v0, p0, LaN/B;->e:I

    iget v1, p1, LaN/B;->e:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    iget v0, p0, LaN/B;->f:I

    iget v1, p1, LaN/B;->f:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v4, 0xaba9500

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const-wide/32 v4, 0x15752a00

    sub-long v0, v4, v0

    :cond_0
    iget v4, p0, LaN/B;->e:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p1, LaN/B;->e:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    add-int/2addr v4, v5

    const v5, 0x1e8480

    div-int/2addr v4, v5

    invoke-static {v4}, Lcom/google/googlenav/common/util/j;->a(I)I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v0, v4

    const-wide/16 v4, 0xfa

    div-long/2addr v0, v4

    invoke-static {v2, v3}, LaN/B;->c(J)J

    move-result-wide v4

    invoke-static {v2, v3}, LaN/B;->c(J)J

    move-result-wide v2

    mul-long/2addr v2, v4

    invoke-static {v0, v1}, LaN/B;->c(J)J

    move-result-wide v4

    invoke-static {v0, v1}, LaN/B;->c(J)J

    move-result-wide v0

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public b()LaN/B;
    .locals 0

    return-object p0
.end method

.method public b(II)LaN/B;
    .locals 6

    iget v0, p0, LaN/B;->e:I

    add-int v4, v0, p1

    iget v0, p0, LaN/B;->f:I

    add-int v5, v0, p2

    iget v1, p0, LaN/B;->c:I

    iget v2, p0, LaN/B;->d:I

    if-eqz p1, :cond_0

    sget-object v0, LaN/B;->g:LaN/Y;

    invoke-static {v4, v0}, LaN/B;->b(ILaN/Y;)I

    move-result v2

    :cond_0
    if-eqz p2, :cond_1

    sget-object v0, LaN/B;->g:LaN/Y;

    invoke-static {v5, v0}, LaN/B;->a(ILaN/Y;)I

    move-result v1

    :cond_1
    new-instance v0, LaN/B;

    const/16 v3, 0x16

    invoke-direct/range {v0 .. v5}, LaN/B;-><init>(IIIII)V

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LaN/B;->e:I

    return v0
.end method

.method public c(II)LaN/B;
    .locals 3

    int-to-long v0, p1

    invoke-static {v0, v1}, LaN/B;->b(J)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-long v1, p2

    invoke-virtual {p0, v1, v2}, LaN/B;->a(J)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p0, v0, v1}, LaN/B;->b(II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LaN/B;->e:I

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, LaN/B;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LaN/B;

    if-eqz v2, :cond_0

    check-cast p1, LaN/B;

    iget v2, p0, LaN/B;->e:I

    iget v3, p1, LaN/B;->e:I

    if-ne v2, v3, :cond_2

    iget v2, p0, LaN/B;->f:I

    iget v3, p1, LaN/B;->f:I

    if-eq v2, v3, :cond_3

    :cond_2
    iget v2, p0, LaN/B;->c:I

    iget v3, p1, LaN/B;->c:I

    if-ne v2, v3, :cond_0

    iget v2, p0, LaN/B;->d:I

    iget v3, p1, LaN/B;->d:I

    if-ne v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget v0, p0, LaN/B;->f:I

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget v2, p0, LaN/B;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget v2, p0, LaN/B;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public h()I
    .locals 1

    const/16 v0, 0x18

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, LaN/B;->e:I

    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, LaN/B;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget v0, p0, LaN/B;->e:I

    iget v1, p0, LaN/B;->f:I

    invoke-static {v0, v1}, LaN/B;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
