.class public Lbg/j;
.super LF/H;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/List;

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method public constructor <init>(Lbf/i;Lcom/google/googlenav/E;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIZ)V
    .locals 10

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-static {v1, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    invoke-static {p2}, Lbg/j;->a(Lcom/google/googlenav/E;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v9, p9

    invoke-direct/range {v1 .. v9}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p2}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lbg/j;->e:Ljava/util/List;

    move/from16 v0, p7

    iput v0, p0, Lbg/j;->f:I

    move/from16 v0, p8

    iput v0, p0, Lbg/j;->g:I

    iget v1, p0, Lbg/j;->g:I

    invoke-virtual {p0, v1}, Lbg/j;->a(I)I

    move-result v1

    iput v1, p0, Lbg/j;->h:I

    invoke-interface {p2}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lbg/j;->d:Ljava/lang/Object;

    return-void
.end method

.method private static a(Lcom/google/googlenav/E;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_0

    check-cast p0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/e;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lbg/j;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbg/j;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lbg/j;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lbg/j;->b:I

    invoke-virtual {p1}, Lcom/google/googlenav/e;->b()I

    move-result v3

    if-ne v0, v3, :cond_3

    iget v0, p0, Lbg/j;->c:I

    invoke-virtual {p1}, Lcom/google/googlenav/e;->c()I

    move-result v3

    if-eq v0, v3, :cond_4

    :cond_3
    move v2, v1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public f()Lo/D;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->t()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    :pswitch_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lbg/j;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v0

    invoke-virtual {v0}, Ln/s;->g()Ljava/util/Set;

    move-result-object v1

    iget-object v0, p0, Lbg/j;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lbg/j;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbg/j;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lbg/j;->f:I

    return v0
.end method

.method public h_()I
    .locals 1

    iget v0, p0, Lbg/j;->h:I

    neg-int v0, v0

    return v0
.end method

.method public p()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbg/j;->d:Ljava/lang/Object;

    return-object v0
.end method
