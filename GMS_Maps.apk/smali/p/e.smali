.class public Lp/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lp/f;

.field private static final b:[Lp/a;


# instance fields
.field private final c:I

.field private final d:J

.field private final e:[Lp/f;

.field private final f:Lo/X;

.field private final g:I

.field private h:[Lp/a;

.field private final i:I

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lp/f;

    const-string v1, "Unknown Road"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lp/e;->a:Lp/f;

    new-array v0, v3, [Lp/a;

    sput-object v0, Lp/e;->b:[Lp/a;

    return-void
.end method

.method public constructor <init>(J[Lp/f;Lo/X;IIII)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p3

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Segments must have at least one name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p5, p0, Lp/e;->c:I

    iput-wide p1, p0, Lp/e;->d:J

    iput-object p3, p0, Lp/e;->e:[Lp/f;

    iput-object p4, p0, Lp/e;->f:Lo/X;

    iput p6, p0, Lp/e;->g:I

    sget-object v0, Lp/e;->b:[Lp/a;

    iput-object v0, p0, Lp/e;->h:[Lp/a;

    iput p7, p0, Lp/e;->i:I

    iput p8, p0, Lp/e;->j:I

    return-void
.end method

.method public static a(Lo/aq;I)J
    .locals 5

    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x30

    shl-long/2addr v0, v2

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(I)Lp/a;
    .locals 1

    iget-object v0, p0, Lp/e;->h:[Lp/a;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(ILo/T;)V
    .locals 1

    iget v0, p0, Lp/e;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lp/e;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    iget-object v0, p0, Lp/e;->f:Lo/X;

    invoke-virtual {v0, p1, p2}, Lo/X;->a(ILo/T;)V

    return-void
.end method

.method public a(Lo/T;)V
    .locals 1

    iget-object v0, p0, Lp/e;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, p1}, Lp/e;->a(ILo/T;)V

    return-void
.end method

.method public varargs a([Lp/a;)V
    .locals 1

    array-length v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lp/e;->b:[Lp/a;

    iput-object v0, p0, Lp/e;->h:[Lp/a;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lp/e;->h:[Lp/a;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget v0, p0, Lp/e;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lp/e;)Z
    .locals 2

    iget v0, p0, Lp/e;->c:I

    and-int/lit8 v0, v0, 0x4

    iget v1, p1, Lp/e;->c:I

    and-int/lit8 v1, v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lp/e;->f:Lo/X;

    iget-object v1, p1, Lp/e;->f:Lo/X;

    invoke-virtual {v0, v1}, Lo/X;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lo/T;
    .locals 1

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    invoke-virtual {p0, p1, v0}, Lp/e;->a(ILo/T;)V

    return-object v0
.end method

.method public b()Z
    .locals 1

    iget v0, p0, Lp/e;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)Lp/f;
    .locals 1

    iget-object v0, p0, Lp/e;->e:[Lp/f;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Lp/e;->c:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lp/e;->h:[Lp/a;

    array-length v0, v0

    return v0
.end method

.method public e()Lo/T;
    .locals 1

    iget-object v0, p0, Lp/e;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lp/e;->b(I)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lp/e;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lp/e;->d:J

    check-cast p1, Lp/e;

    iget-wide v3, p1, Lp/e;->d:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lp/e;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lp/e;->i:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lp/e;->j:I

    return v0
.end method

.method public hashCode()I
    .locals 7

    const-wide/16 v5, 0xff

    iget-wide v0, p0, Lp/e;->d:J

    const/16 v2, 0x30

    ushr-long/2addr v0, v2

    and-long/2addr v0, v5

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lp/e;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-wide v2, p0, Lp/e;->d:J

    const-wide/32 v4, 0xffff

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lp/e;->e:[Lp/f;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " unroutable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lp/e;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " leaves-region: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lp/e;->a()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " enters-region: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lp/e;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " num-points: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lp/e;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " first-point: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lp/e;->b(I)Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " last-point: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lp/e;->e()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " num-arcs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lp/e;->h:[Lp/a;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
