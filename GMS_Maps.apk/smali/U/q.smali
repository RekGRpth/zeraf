.class public final LU/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LU/l;

.field private final b:LU/u;

.field private final c:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method constructor <init>(LU/l;LU/u;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LU/q;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/l;

    iput-object v0, p0, LU/q;->a:LU/l;

    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/u;

    iput-object v0, p0, LU/q;->b:LU/u;

    invoke-direct {p0}, LU/q;->e()V

    return-void
.end method

.method public constructor <init>(LU/u;)V
    .locals 2

    new-instance v0, LU/m;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, LU/m;-><init>(LU/u;Z)V

    invoke-direct {p0, v0, p1}, LU/q;-><init>(LU/l;LU/u;)V

    return-void
.end method

.method static synthetic a(LU/q;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    iget-object v0, p0, LU/q;->c:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, LU/q;->a:LU/l;

    invoke-interface {v0}, LU/l;->a()LX/i;

    move-result-object v0

    new-instance v1, LU/s;

    invoke-direct {v1, p0}, LU/s;-><init>(LU/q;)V

    invoke-interface {v0, v1}, LX/i;->b(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, LU/q;->a:LU/l;

    invoke-interface {v0}, LU/l;->a()LX/i;

    move-result-object v0

    new-instance v1, LU/t;

    invoke-direct {v1, p0}, LU/t;-><init>(LU/q;)V

    invoke-interface {v0, v1}, LX/i;->b(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private e()V
    .locals 5

    iget-object v0, p0, LU/q;->b:LU/u;

    invoke-interface {v0}, LU/u;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, LU/q;->b:LU/u;

    iget-object v3, p0, LU/q;->a:LU/l;

    invoke-interface {v2, v0, v3}, LU/u;->a(Ljava/lang/String;LU/l;)LU/b;

    move-result-object v2

    iget-object v3, p0, LU/q;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, LU/q;->b:LU/u;

    invoke-interface {v3, v0}, LU/u;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LU/q;->a:LU/l;

    invoke-interface {v4, v0, v2}, LU/l;->a(Ljava/lang/String;LU/T;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-direct {p0}, LU/q;->c()V

    iget-object v0, p0, LU/q;->a:LU/l;

    invoke-interface {v0}, LU/l;->a()LX/i;

    move-result-object v0

    new-instance v1, LU/r;

    invoke-direct {v1, p0}, LU/r;-><init>(LU/q;)V

    invoke-interface {v0, v1}, LX/i;->b(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(LU/T;)V
    .locals 2

    iget-object v0, p0, LU/q;->a:LU/l;

    const-string v1, "integrated_location_provider"

    invoke-interface {v0, v1, p1}, LU/l;->a(Ljava/lang/String;LU/T;)V

    return-void
.end method

.method a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, LU/q;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LU/b;->e()Z

    move-result v0

    goto :goto_0
.end method

.method a(Ljava/lang/String;Z)Z
    .locals 1

    iget-object v0, p0, LU/q;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v0}, LU/b;->b()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, LU/b;->d()V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    invoke-direct {p0}, LU/q;->d()V

    iget-object v0, p0, LU/q;->a:LU/l;

    invoke-interface {v0}, LU/l;->b()V

    return-void
.end method
