.class public LU/R;
.super Landroid/location/Location;
.source "SourceFile"


# instance fields
.field private a:LU/S;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/location/Location;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LU/R;->b:Ljava/util/List;

    instance-of v0, p1, LU/R;

    if-eqz v0, :cond_0

    check-cast p1, LU/R;

    iget-object v0, p1, LU/R;->a:LU/S;

    iput-object v0, p0, LU/R;->a:LU/S;

    iget-object v0, p1, LU/R;->b:Ljava/util/List;

    iput-object v0, p0, LU/R;->b:Ljava/util/List;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()LU/S;
    .locals 1

    iget-object v0, p0, LU/R;->a:LU/S;

    return-object v0
.end method

.method public a(LU/S;)V
    .locals 0

    iput-object p1, p0, LU/R;->a:LU/S;

    return-void
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, LU/R;->a:LU/S;

    sget-object v1, LU/S;->a:LU/S;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LU/R;->a:LU/S;

    sget-object v1, LU/S;->b:LU/S;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
