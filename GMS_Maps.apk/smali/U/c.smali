.class public final LU/c;
.super LU/b;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field private final a:Landroid/location/LocationManager;

.field private final b:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .locals 1

    const-string v0, "gps"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/a;

    iput-object v0, p0, LU/c;->b:Lcom/google/googlenav/common/a;

    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    return-void
.end method

.method private static a()I
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->c:I

    return v0
.end method


# virtual methods
.method public b()V
    .locals 7

    invoke-virtual {p0}, LU/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LU/c;->a()I

    move-result v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    const-string v1, "gps"

    int-to-long v2, v2

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    invoke-super {p0}, LU/b;->b()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    invoke-virtual {p0}, LU/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, LU/b;->d()V

    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 3

    new-instance v0, LU/R;

    invoke-direct {v0, p1}, LU/R;-><init>(Landroid/location/Location;)V

    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    iget-object v1, p0, LU/c;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    invoke-super {p0, v0}, LU/b;->a(LU/R;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p1}, LU/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p1}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p1, p2}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
