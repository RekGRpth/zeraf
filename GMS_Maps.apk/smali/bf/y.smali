.class public Lbf/y;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaN/m;


# instance fields
.field protected B:LaN/k;

.field private C:Lcom/google/googlenav/layer/m;

.field private D:Z

.field private final E:Lcom/google/googlenav/layer/s;

.field private F:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/y;->D:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/y;->F:Z

    iput-object p5, p0, Lbf/y;->B:LaN/k;

    new-instance v0, Lcom/google/googlenav/layer/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V
    .locals 6

    new-instance v5, Lcom/google/googlenav/T;

    invoke-direct {v5, p5, p6, p2, p3}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/y;->D:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/y;->F:Z

    iput-object p5, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    iput-object p6, p0, Lbf/y;->B:LaN/k;

    new-instance v0, Lcom/google/googlenav/layer/s;

    invoke-direct {v0, p0, p5}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/o;)V
    .locals 6

    new-instance v5, Lcom/google/googlenav/T;

    invoke-direct {v5, p5, p6, p2, p3}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/y;->D:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/y;->F:Z

    iput-object p5, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    new-instance v0, Lcom/google/googlenav/layer/s;

    invoke-direct {v0, p0, p5}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    return-void
.end method

.method private bK()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lbf/y;->D:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/T;->a()V

    invoke-virtual {p0}, Lbf/y;->d()V

    iput-boolean v0, p0, Lbf/y;->D:Z

    invoke-virtual {v1}, Lcom/google/googlenav/T;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/y;->Z()V

    :cond_0
    invoke-virtual {p0}, Lbf/y;->R()V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .locals 2

    invoke-super {p0}, Lbf/m;->X()Z

    invoke-virtual {p0}, Lbf/y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/y;->bK()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->b()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/y;->e(Z)V

    invoke-super {p0}, Lbf/m;->Y()V

    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 0

    iput-object p1, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    invoke-virtual {p0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V
    .locals 6

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->n()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lbf/y;->c:LaN/p;

    iget-object v4, p0, Lbf/y;->e:Landroid/graphics/Point;

    invoke-virtual {v3, v2, v4}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v2, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/layer/m;->a(I)Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v2

    iget-object v3, p0, Lbf/y;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    invoke-interface {v0}, Lam/f;->a()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget-object v4, p0, Lbf/y;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-interface {v0}, Lam/f;->b()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    add-int/2addr v1, v4

    invoke-interface {v2, v0, v3, v1}, Lam/e;->a(Lam/f;II)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .locals 7

    invoke-virtual {p0}, Lbf/y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbf/y;->bK()Z

    :cond_0
    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_1
    return-void

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {p0, v0, p2}, Lbf/y;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v3

    int-to-long v5, p3

    cmp-long v5, v3, v5

    if-gez v5, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v5

    invoke-static {p0, v0, v5, v3, v4}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->n()Z

    move-result v0

    return v0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lbf/y;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Lcom/google/googlenav/W;->a(Z)V

    iget-object v2, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lbf/y;->w:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UI - null placemark "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " at "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/y;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->c()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1f8
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 5

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, LaN/k;

    invoke-direct {v0}, LaN/k;-><init>()V

    iput-object v0, p0, Lbf/y;->B:LaN/k;

    :cond_0
    new-instance v0, Lcom/google/googlenav/T;

    iget-object v1, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    iget-object v2, p0, Lbf/y;->B:LaN/k;

    iget-object v3, p0, Lbf/y;->c:LaN/p;

    iget-object v4, p0, Lbf/y;->d:LaN/u;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    iput-object v0, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aL()Lam/f;
    .locals 2

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    iget-object v1, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->a(I)Lcom/google/googlenav/e;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/y;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->aj:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected aP()V
    .locals 2

    invoke-virtual {p0}, Lbf/y;->bH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbf/m;->aP()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-static {p0}, Lbf/am;->m(Lbf/i;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public aT()Z
    .locals 2

    iget-object v0, p0, Lbf/y;->B:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->a(LaN/m;)V

    iget-object v0, p0, Lbf/y;->c:LaN/p;

    iget-object v1, p0, Lbf/y;->B:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/k;)V

    invoke-virtual {p0}, Lbf/y;->bG()V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 2

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    iget-object v0, p0, Lbf/y;->B:LaN/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/y;->B:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    iget-object v0, p0, Lbf/y;->B:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/y;->c:LaN/p;

    iget-object v1, p0, Lbf/y;->B:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    :cond_0
    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    invoke-super {p0}, Lbf/m;->aU()V

    return-void
.end method

.method protected am()V
    .locals 4

    invoke-virtual {p0}, Lbf/y;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/googlenav/W;

    const/16 v2, 0x43

    const-string v3, "o"

    invoke-virtual {v1}, Lcom/google/googlenav/W;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    :cond_0
    return-void
.end method

.method protected aq()V
    .locals 0

    return-void
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public b(LaN/B;)I
    .locals 6

    const/4 v5, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0}, Lbf/y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbf/y;->bK()Z

    :cond_0
    iget-object v0, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v2, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v5}, Lbf/y;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {p0, v0, p1, v5}, Lbf/y;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method protected bG()V
    .locals 2

    invoke-virtual {p0}, Lbf/y;->f()Lcom/google/googlenav/T;

    move-result-object v0

    new-instance v1, Lbf/z;

    invoke-direct {v1, p0}, Lbf/z;-><init>(Lbf/y;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/T;->a(Lcom/google/googlenav/V;)V

    return-void
.end method

.method public bH()Z
    .locals 1

    iget-boolean v0, p0, Lbf/y;->F:Z

    return v0
.end method

.method public bI()V
    .locals 1

    iget-boolean v0, p0, Lbf/y;->F:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lbf/y;->k(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bJ()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected bx()Z
    .locals 2

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->h()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    invoke-virtual {p0, p1}, Lbf/y;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/y;->D:Z

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lbf/m;->d(Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .locals 1

    iget-object v0, p0, Lbf/y;->C:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lbf/m;->e(Lcom/google/googlenav/ui/r;)V

    :cond_0
    return-void
.end method

.method public f()Lcom/google/googlenav/T;
    .locals 1

    iget-object v0, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method protected f(Lat/a;)Z
    .locals 3

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/y;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/y;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const-string v1, "s"

    const-string v2, "k"

    invoke-virtual {p0, v0, v1, v2}, Lbf/y;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/y;->m()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lbf/m;->g(I)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 1

    iget-object v0, p0, Lbf/y;->E:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/c;

    invoke-direct {v0, p0}, Lbh/c;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected k(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/y;->F:Z

    return-void
.end method

.method protected l()V
    .locals 0

    return-void
.end method
