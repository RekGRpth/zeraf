.class public Lbf/E;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/O;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    return-void
.end method

.method private c()Lax/b;
    .locals 1

    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lax/b;

    return-object v0
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lbf/E;->a()Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v3

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    invoke-direct {p0}, Lbf/E;->c()Lax/b;

    move-result-object v1

    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->bp()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x107

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, v2, v7, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;Lam/f;)V

    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    sget-object v0, Lbf/E;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/E;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lbf/E;->c()Lax/b;

    move-result-object v0

    invoke-static {v0, v3, v8}, Lbf/G;->a(Lax/b;IZ)Lbf/H;

    move-result-object v0

    invoke-virtual {v0}, Lbf/H;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-static {v8}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v2, v4}, Lbf/E;->b(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v2, v7}, Lbf/E;->c(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    invoke-virtual {p0, v2, v9}, Lbf/E;->a(Landroid/view/View;Z)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    check-cast v0, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-virtual {v0, v3, v1, v8}, Lax/w;->a(ILcom/google/googlenav/ui/m;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/ui/bi;->f()Lam/g;

    move-result-object v6

    invoke-static {v0, v4, v5, v3, v8}, Lbf/G;->a(Lax/w;Ljava/util/Vector;Ljava/util/Vector;IZ)I

    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v2, v4}, Lbf/E;->b(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v2, v5}, Lbf/E;->c(Landroid/view/View;Ljava/util/List;)V

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v8, :cond_3

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :goto_1
    invoke-interface {v6, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    invoke-virtual {p0, v2, v8}, Lbf/E;->a(Landroid/view/View;Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    invoke-virtual {p0, v2, v9}, Lbf/E;->a(Landroid/view/View;Z)V

    goto/16 :goto_0
.end method
