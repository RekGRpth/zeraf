.class Lbf/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Ljava/io/DataInput;

.field final synthetic d:I

.field final synthetic e:Z

.field final synthetic f:Z

.field final synthetic g:Lbf/am;


# direct methods
.method constructor <init>(Lbf/am;IZLjava/io/DataInput;IZZ)V
    .locals 0

    iput-object p1, p0, Lbf/ar;->g:Lbf/am;

    iput p2, p0, Lbf/ar;->a:I

    iput-boolean p3, p0, Lbf/ar;->b:Z

    iput-object p4, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iput p5, p0, Lbf/ar;->d:I

    iput-boolean p6, p0, Lbf/ar;->e:Z

    iput-boolean p7, p0, Lbf/ar;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget v0, p0, Lbf/ar;->a:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->a(Lbf/am;Ljava/io/DataInput;IZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_MANAGER-LayersManager load ex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbf/ar;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    invoke-static {v0}, Lbf/am;->a(Lbf/am;)V

    goto :goto_0

    :pswitch_2
    :try_start_1
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->b(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->c(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->d(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_0

    :pswitch_5
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->e(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->f(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->g(Lbf/am;Ljava/io/DataInput;IZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
