.class public abstract Lbf/F;
.super Lbf/i;
.source "SourceFile"


# static fields
.field private static final v:LaN/Y;

.field private static final w:I

.field private static final x:I

.field private static final y:I


# instance fields
.field protected u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v1, 0x9

    const/4 v2, 0x7

    const/16 v0, 0x12

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lbf/F;->v:LaN/Y;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    sput v0, Lbf/F;->w:I

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    sput v1, Lbf/F;->x:I

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x5

    :cond_1
    sput v2, Lbf/F;->y:I

    return-void

    :cond_2
    const/16 v0, 0xe

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/F;->u:Z

    return-void
.end method

.method private a([LaN/B;I)[LaN/B;
    .locals 3

    const/4 v2, 0x0

    new-array v0, p2, [LaN/B;

    array-length v1, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static b()I
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    sget v0, Lbf/F;->y:I

    neg-int v0, v0

    :goto_0
    return v0

    :cond_0
    const-wide/high16 v2, 0x3ff8000000000000L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    sget v0, Lbf/F;->x:I

    neg-int v0, v0

    goto :goto_0

    :cond_1
    sget v0, Lbf/F;->w:I

    neg-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lax/b;
    .locals 1

    iget-object v0, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/b;

    return-object v0
.end method

.method protected a(IZZ)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v4

    iget-object v0, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/k;

    iget-object v2, p0, Lbf/F;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v5

    iget-boolean v2, p0, Lbf/F;->u:Z

    if-eqz v2, :cond_0

    sget-object v2, Lbf/F;->v:LaN/Y;

    invoke-virtual {v2, v5}, LaN/Y;->b(LaN/Y;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v5, Lbf/F;->v:LaN/Y;

    :cond_0
    iput-boolean v1, p0, Lbf/F;->u:Z

    invoke-virtual {p0, p1}, Lbf/F;->b(I)V

    invoke-virtual {p0}, Lbf/F;->an()Z

    if-ltz p1, :cond_1

    iget-object v2, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge p1, v2, :cond_1

    if-eqz p2, :cond_1

    if-eqz p3, :cond_4

    const/4 v2, -0x1

    if-eq v4, v2, :cond_5

    invoke-interface {v0, v4}, Lax/k;->m(I)I

    move-result v2

    invoke-interface {v0, p1}, Lax/k;->m(I)I

    move-result v3

    :goto_0
    if-ne v2, v3, :cond_2

    invoke-virtual {p0, v1, v5}, Lbf/F;->a(ZLaN/Y;)V

    :goto_1
    const/16 v0, 0x12

    const-string v1, "dd"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v0}, Lax/k;->ad()[LaN/B;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    array-length v1, v0

    invoke-direct {p0, v0, v1}, Lbf/F;->a([LaN/B;I)[LaN/B;

    move-result-object v1

    iget-object v0, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lbf/F;->a(Lcom/google/googlenav/E;LaN/Y;)LaN/B;

    move-result-object v0

    aput-object v0, v1, v3

    :cond_3
    iget-object v0, p0, Lbf/F;->d:LaN/u;

    sub-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-virtual/range {v0 .. v5}, LaN/u;->a([LaN/B;IIILaN/Y;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v5}, Lbf/F;->a(ZLaN/Y;)V

    goto :goto_1

    :cond_5
    move v3, v1

    move v2, v1

    goto :goto_0
.end method

.method protected a(Lax/y;I)V
    .locals 4

    iget-object v0, p0, Lbf/F;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v1, p2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/F;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x4

    const-string v1, "v"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/view/t;I)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lbf/F;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/F;->g:Lcom/google/googlenav/ui/view/d;

    if-eq p1, v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lbf/i;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbf/F;->h:Lcom/google/googlenav/E;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/F;->h:Lcom/google/googlenav/E;

    instance-of v0, v0, Lax/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/F;->h:Lcom/google/googlenav/E;

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/F;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0, p2}, Lbf/F;->a(Lax/y;I)V

    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x23

    const/16 v2, 0xd

    invoke-virtual {p0, v0, v2}, Lbf/F;->a(CI)Z

    move v0, v1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 2

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-static {}, Lbf/F;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lbf/F;->f(Lcom/google/googlenav/E;)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public e(Lat/a;)Z
    .locals 5

    const/16 v4, 0x36

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/F;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Lat/a;->b()I

    move-result v3

    if-eq v3, v4, :cond_2

    const/16 v0, 0x34

    if-ne v3, v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Lbf/F;->ac()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lbf/F;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->ay()I

    move-result v0

    if-ltz v0, :cond_3

    iget-object v3, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    if-lt v0, v3, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    iget-object v1, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->a(I)V

    invoke-virtual {p0}, Lbf/F;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lbf/am;->a(Lbf/i;I)V

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ne v3, v4, :cond_6

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lbf/F;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_1
    invoke-virtual {p0, v0, v2, v2}, Lbf/F;->a(IZZ)V

    if-ne v3, v4, :cond_7

    const-string v0, "cn"

    :goto_2
    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    :cond_7
    const-string v0, "cp"

    goto :goto_2
.end method

.method protected p()I
    .locals 1

    invoke-virtual {p0}, Lbf/F;->q()I

    move-result v0

    neg-int v0, v0

    return v0
.end method
