.class public Lbf/I;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:I

.field private static c:I


# instance fields
.field private final a:Lbf/O;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, -0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/I;->b:I

    const/4 v0, 0x0

    sput v0, Lbf/I;->c:I

    return-void
.end method

.method public constructor <init>(Lbf/O;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbf/I;->a:Lbf/O;

    return-void
.end method

.method static synthetic a(Lbf/I;)Lbf/O;
    .locals 1

    iget-object v0, p0, Lbf/I;->a:Lbf/O;

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;
    .locals 3

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    sget v1, Lbf/I;->b:I

    invoke-virtual {v0, v2, v1, v2, v2}, Lcom/google/googlenav/ui/aW;->a(IIII)V

    return-object v0
.end method

.method public static a(Lax/b;)Ljava/lang/CharSequence;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lax/b;->ac()Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/g;

    invoke-virtual {v0}, Lax/g;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/google/googlenav/ui/aV;->aL:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lbf/O;Lax/b;)Ljava/util/Vector;
    .locals 3

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p0}, Lbf/O;->bh()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-char v0, Lcom/google/googlenav/ui/bi;->aG:C

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-eqz v0, :cond_4

    check-cast p1, Lax/w;

    invoke-virtual {p1}, Lax/w;->ao()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lbf/O;->bo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/aV;->x:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-char v0, Lcom/google/googlenav/ui/bi;->aH:C

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-char v0, Lcom/google/googlenav/ui/bi;->aJ:C

    goto :goto_0

    :cond_3
    sget-char v0, Lcom/google/googlenav/ui/bi;->aI:C

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lbf/O;->bm()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v2, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_5
    invoke-virtual {p0}, Lbf/O;->bn()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Lax/y;)Ljava/util/Vector;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, -0x1

    if-eqz p1, :cond_0

    invoke-static {}, Lbf/O;->bq()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v4, :cond_2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lbf/I;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v4, :cond_0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aK:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lbf/I;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic b()I
    .locals 1

    sget v0, Lbf/I;->c:I

    return v0
.end method

.method private c()Lax/b;
    .locals 1

    iget-object v0, p0, Lbf/I;->a:Lbf/O;

    invoke-virtual {v0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lax/b;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/view/android/aL;
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v1, p0, Lbf/I;->a:Lbf/O;

    invoke-direct {p0}, Lbf/I;->c()Lax/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/ah;-><init>(Lcom/google/googlenav/ui/e;Lax/b;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/ui/view/dialog/r;
    .locals 7

    const/4 v2, -0x1

    new-instance v5, Ljava/util/Vector;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Ljava/util/Vector;-><init>(I)V

    iget-object v0, p0, Lbf/I;->a:Lbf/O;

    invoke-virtual {v0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->u()Z

    move-result v6

    if-eqz v6, :cond_1

    const/16 v1, 0x59b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    :goto_0
    if-eqz v6, :cond_2

    sget-object v1, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    move-object v3, v1

    :goto_1
    if-eqz v6, :cond_3

    const/16 v1, 0xe5

    :goto_2
    new-instance v6, Lbf/L;

    invoke-static {v4, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lbf/J;

    invoke-direct {v4, p0, v1, p1}, Lbf/J;-><init>(Lbf/I;II)V

    invoke-direct {v6, v3, v4}, Lbf/L;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, Lbf/I;->a:Lbf/O;

    invoke-virtual {v1}, Lbf/O;->bg()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lbf/I;->a:Lbf/O;

    invoke-virtual {v0}, Lax/m;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbf/O;->c(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    move v1, v0

    :goto_3
    if-eqz v1, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    :goto_4
    if-eqz v1, :cond_0

    const/16 v2, 0x5e6

    :cond_0
    new-instance v1, Lbf/L;

    const/16 v3, 0x446

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v3, Lbf/K;

    invoke-direct {v3, p0, v2, p1}, Lbf/K;-><init>(Lbf/I;II)V

    invoke-direct {v1, v0, v3}, Lbf/L;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    new-instance v0, Lbf/N;

    iget-object v1, p0, Lbf/I;->a:Lbf/O;

    invoke-direct {v0, v1, v5}, Lbf/N;-><init>(Lcom/google/googlenav/ui/e;Ljava/util/Vector;)V

    return-object v0

    :cond_1
    const/16 v1, 0x59d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/googlenav/ui/aV;->u:Lcom/google/googlenav/ui/aV;

    move-object v3, v1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    :cond_5
    sget-object v0, Lcom/google/googlenav/ui/aV;->u:Lcom/google/googlenav/ui/aV;

    goto :goto_4
.end method

.method b(I)Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lbf/I;->c()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->f()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-direct {p0}, Lbf/I;->c()Lax/b;

    move-result-object v2

    iget-object v3, p0, Lbf/I;->a:Lbf/O;

    const/4 v4, 0x1

    invoke-static {v2, v0, p1, v3, v4}, Lbf/G;->a(Lax/b;IILcom/google/googlenav/ui/e;Z)Lbj/H;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
