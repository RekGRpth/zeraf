.class public Lbf/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/au;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private final b:Lcom/google/android/maps/driveabout/vector/aZ;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lbf/ah;->c:Z

    iput-boolean v0, p0, Lbf/ah;->d:Z

    iput-object p1, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iput-object p2, p0, Lbf/ah;->b:Lcom/google/android/maps/driveabout/vector/aZ;

    return-void
.end method

.method private declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbf/ah;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/ah;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lbf/i;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbf/ah;->c:Z

    iget-object v1, p0, Lbf/ah;->b:Lcom/google/android/maps/driveabout/vector/aZ;

    new-instance v2, Lo/w;

    invoke-direct {v2}, Lo/w;-><init>()V

    invoke-virtual {v2, p1}, Lo/w;->a(Ljava/lang/String;)Lo/w;

    move-result-object v2

    invoke-virtual {v2}, Lo/w;->a()Lo/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lo/at;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lbf/ah;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/ah;->d:Z

    return-void
.end method

.method public b(Lbf/i;)V
    .locals 1

    iget-boolean v0, p0, Lbf/ah;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lbf/bk;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/ah;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/ah;->a()V

    goto :goto_0
.end method

.method public c(Lbf/i;)V
    .locals 3

    instance-of v0, p1, Lbf/bk;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->z()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    if-ne v0, v1, :cond_0

    check-cast p1, Lbf/bk;

    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ap()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lbf/ah;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/ah;->a()V

    goto :goto_0
.end method
