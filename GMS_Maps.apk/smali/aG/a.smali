.class public LaG/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:LaG/h;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private d:Lcom/google/googlenav/a;

.field private e:LaG/n;

.field private f:LaG/c;


# direct methods
.method public constructor <init>(LaG/h;)V
    .locals 3

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ed;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    sget v2, Lcom/google/googlenav/ui/bi;->bF:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p1, p0, LaG/a;->a:LaG/h;

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;)I
    .locals 2

    sget-object v0, LaG/b;->a:[I

    invoke-virtual {p2}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;
    .locals 8

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    const/4 v3, 0x2

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    if-gez v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LaG/a;->a:LaG/h;

    invoke-virtual {v6, v4, v5}, LaG/h;->a(J)LaG/m;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, LaG/m;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-direct {p0, v2, p2}, LaG/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;)I

    move-result v2

    new-instance v5, LaG/f;

    add-int v6, p3, v0

    invoke-direct {v5, v6, v3, v2, v4}, LaG/f;-><init>(IIILaG/m;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private a(Lcom/google/googlenav/a;Z)V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/googlenav/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ed;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/googlenav/a;->b()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/a;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Lcom/google/googlenav/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/az;->a(Ljava/lang/String;)J

    move-result-wide v1

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/google/googlenav/a;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    const/16 v2, 0x18

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    :cond_4
    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private k()I
    .locals 3

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private l()V
    .locals 8

    const-wide/16 v6, 0x3e8

    const/4 v5, 0x0

    invoke-direct {p0}, LaG/a;->m()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-object v1, p0, LaG/a;->a:LaG/h;

    invoke-virtual {v1, v0}, LaG/h;->a(Ljava/util/List;)V

    iget-object v1, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v1, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {p0}, LaG/a;->n()LaG/g;

    move-result-object v1

    invoke-static {v1}, LaG/d;->a(LaG/g;)LaG/d;

    move-result-object v4

    invoke-direct {p0, v2, v1, v5}, LaG/a;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4, v2}, LaG/d;->a(Ljava/util/List;)V

    invoke-static {v1}, LaG/d;->a(LaG/g;)LaG/d;

    move-result-object v2

    invoke-direct {p0, v3, v1, v5}, LaG/a;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, LaG/d;->a(Ljava/util/List;)V

    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-direct {p0}, LaG/a;->n()LaG/g;

    move-result-object v5

    invoke-virtual {v3, v5}, LaG/n;->a(LaG/g;)V

    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v3, v4}, LaG/n;->a(LaG/d;)V

    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v3, v2}, LaG/n;->b(LaG/d;)V

    iget-object v2, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v2, v0}, LaG/n;->a(Lcom/google/common/collect/ImmutableList;)V

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x7

    invoke-static {v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v4

    iget-object v0, p0, LaG/a;->e:LaG/n;

    div-long/2addr v2, v6

    div-long/2addr v4, v6

    invoke-virtual/range {v0 .. v5}, LaG/n;->a(LaG/g;JJ)V

    return-void
.end method

.method private m()Lcom/google/common/collect/ImmutableList;
    .locals 14

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-nez v0, :cond_0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v7

    array-length v8, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_2

    aget-object v9, v2, v1

    const/4 v0, 0x1

    invoke-static {v9, v0}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v0, v5, v12

    if-eqz v0, :cond_1

    cmp-long v0, v5, v10

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v10, LaG/m;

    invoke-direct {v10, v9, v0, v3, v4}, LaG/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    invoke-virtual {v7, v10}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v7}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private n()LaG/g;
    .locals 2

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, LaG/g;->b:LaG/g;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, LaG/g;->a:LaG/g;

    goto :goto_0

    :pswitch_1
    sget-object v0, LaG/g;->b:LaG/g;

    goto :goto_0

    :pswitch_2
    sget-object v0, LaG/g;->c:LaG/g;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(I)LaG/a;
    .locals 3

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public a(II)LaG/a;
    .locals 3

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public a(LaG/c;)LaG/a;
    .locals 0

    iput-object p1, p0, LaG/a;->f:LaG/c;

    return-object p0
.end method

.method public a(LaG/g;)LaG/a;
    .locals 3

    sget-object v0, LaG/b;->a:[I

    invoke-virtual {p1}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p0

    :pswitch_0
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/util/List;)LaG/a;
    .locals 5

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LaG/a;->d:Lcom/google/googlenav/a;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-nez v3, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/a;

    invoke-direct {p0, v0, v1}, LaG/a;->a(Lcom/google/googlenav/a;Z)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/a;

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    return-object p0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ed;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, LaG/a;->k()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, LaG/n;

    iget-object v1, p0, LaG/a;->d:Lcom/google/googlenav/a;

    invoke-direct {v0, v1}, LaG/n;-><init>(Lcom/google/googlenav/a;)V

    iput-object v0, p0, LaG/a;->e:LaG/n;

    invoke-direct {p0}, LaG/a;->l()V

    iget-object v0, p0, LaG/a;->f:LaG/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaG/a;->f:LaG/c;

    iget-object v1, p0, LaG/a;->e:LaG/n;

    invoke-interface {v0, v1}, LaG/c;->a(LaG/n;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    new-instance v0, LaG/n;

    iget-object v1, p0, LaG/a;->d:Lcom/google/googlenav/a;

    invoke-direct {v0, v1}, LaG/n;-><init>(Lcom/google/googlenav/a;)V

    iput-object v0, p0, LaG/a;->e:LaG/n;

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x7c

    return v0
.end method

.method public d_()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaG/a;->f:LaG/c;

    const/4 v1, 0x0

    iput-object v1, p0, LaG/a;->f:LaG/c;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LaG/a;->k()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LaG/a;->e:LaG/n;

    invoke-interface {v0, v1}, LaG/c;->b(LaG/n;)V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    invoke-interface {v0}, LaG/c;->a()V

    goto :goto_0
.end method

.method public u_()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaG/a;->f:LaG/c;

    const/4 v1, 0x0

    iput-object v1, p0, LaG/a;->f:LaG/c;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, LaG/c;->a()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
