.class public final Laf/c;
.super LbH/k;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, LbH/k;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Laf/c;->b:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Laf/c;->c:Ljava/lang/Object;

    invoke-direct {p0}, Laf/c;->h()V

    return-void
.end method

.method static synthetic g()Laf/c;
    .locals 1

    invoke-static {}, Laf/c;->i()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 0

    return-void
.end method

.method private static i()Laf/c;
    .locals 1

    new-instance v0, Laf/c;

    invoke-direct {v0}, Laf/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Laf/c;
    .locals 2

    invoke-static {}, Laf/c;->i()Laf/c;

    move-result-object v0

    invoke-virtual {p0}, Laf/c;->c()Laf/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/c;->a(Laf/a;)Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Laf/a;)Laf/c;
    .locals 1

    invoke-static {}, Laf/a;->a()Laf/a;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Laf/a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Laf/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/c;->a:I

    invoke-static {p1}, Laf/a;->a(Laf/a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/c;->b:Ljava/lang/Object;

    :cond_2
    invoke-virtual {p1}, Laf/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Laf/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/c;->a:I

    invoke-static {p1}, Laf/a;->b(Laf/a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/c;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(LbH/f;LbH/i;)Laf/c;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    sget-object v0, Laf/a;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/a;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Laf/c;->a(Laf/a;)Laf/c;

    :cond_0
    return-object p0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Laf/c;->a(Laf/a;)Laf/c;

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Laf/c;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/c;->a:I

    iput-object p1, p0, Laf/c;->b:Ljava/lang/Object;

    return-object p0
.end method

.method public b()Laf/a;
    .locals 2

    invoke-virtual {p0}, Laf/c;->c()Laf/a;

    move-result-object v0

    invoke-virtual {v0}, Laf/a;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Laf/c;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/String;)Laf/c;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/c;->a:I

    iput-object p1, p0, Laf/c;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .locals 1

    invoke-virtual {p0, p1, p2}, Laf/c;->a(LbH/f;LbH/i;)Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/a;
    .locals 5

    const/4 v0, 0x1

    new-instance v2, Laf/a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Laf/a;-><init>(LbH/k;Laf/b;)V

    iget v3, p0, Laf/c;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Laf/c;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/a;->a(Laf/a;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Laf/c;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/a;->b(Laf/a;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Laf/a;->a(Laf/a;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Laf/c;->a()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .locals 1

    invoke-virtual {p0}, Laf/c;->a()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .locals 1

    invoke-virtual {p0}, Laf/c;->a()Laf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .locals 1

    invoke-virtual {p0}, Laf/c;->b()Laf/a;

    move-result-object v0

    return-object v0
.end method
