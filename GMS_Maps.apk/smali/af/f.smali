.class public final Laf/f;
.super LbH/k;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/util/List;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, LbH/k;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Laf/f;->c:Ljava/lang/Object;

    invoke-direct {p0}, Laf/f;->h()V

    return-void
.end method

.method static synthetic g()Laf/f;
    .locals 1

    invoke-static {}, Laf/f;->i()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 0

    return-void
.end method

.method private static i()Laf/f;
    .locals 1

    new-instance v0, Laf/f;

    invoke-direct {v0}, Laf/f;-><init>()V

    return-object v0
.end method

.method private j()V
    .locals 2

    iget v0, p0, Laf/f;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Laf/f;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/f;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Laf/f;
    .locals 2

    invoke-static {}, Laf/f;->i()Laf/f;

    move-result-object v0

    invoke-virtual {p0}, Laf/f;->c()Laf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/f;->a(Laf/d;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public a(Laf/c;)Laf/f;
    .locals 2

    invoke-direct {p0}, Laf/f;->j()V

    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-virtual {p1}, Laf/c;->b()Laf/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Laf/d;)Laf/f;
    .locals 2

    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    iget v0, p0, Laf/f;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Laf/f;->a:I

    :cond_2
    :goto_1
    invoke-virtual {p1}, Laf/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/f;->a:I

    invoke-static {p1}, Laf/d;->c(Laf/d;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/f;->c:Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Laf/f;->j()V

    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(LbH/f;LbH/i;)Laf/f;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    sget-object v0, Laf/d;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/d;
    :try_end_0
    .catch LbH/l; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Laf/f;->a(Laf/d;)Laf/f;

    :cond_0
    return-object p0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Laf/f;->a(Laf/d;)Laf/f;

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Laf/f;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/f;->a:I

    iput-object p1, p0, Laf/f;->c:Ljava/lang/Object;

    return-object p0
.end method

.method public b()Laf/d;
    .locals 2

    invoke-virtual {p0}, Laf/f;->c()Laf/d;

    move-result-object v0

    invoke-virtual {v0}, Laf/d;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Laf/f;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .locals 1

    invoke-virtual {p0, p1, p2}, Laf/f;->a(LbH/f;LbH/i;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/d;
    .locals 5

    const/4 v0, 0x1

    new-instance v2, Laf/d;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Laf/d;-><init>(LbH/k;Laf/e;)V

    iget v3, p0, Laf/f;->a:I

    const/4 v1, 0x0

    iget v4, p0, Laf/f;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Laf/f;->b:Ljava/util/List;

    iget v4, p0, Laf/f;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Laf/f;->a:I

    :cond_0
    iget-object v4, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {v2, v4}, Laf/d;->a(Laf/d;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v1, p0, Laf/f;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/d;->a(Laf/d;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v0}, Laf/d;->a(Laf/d;I)I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .locals 1

    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .locals 1

    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .locals 1

    invoke-virtual {p0}, Laf/f;->b()Laf/d;

    move-result-object v0

    return-object v0
.end method
