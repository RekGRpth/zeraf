.class public Le/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Le/n;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Le/o;

    invoke-direct {v0}, Le/o;-><init>()V

    sput-object v0, Le/m;->a:Le/n;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Le/q;

    invoke-direct {v0}, Le/q;-><init>()V

    sput-object v0, Le/m;->a:Le/n;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Le/m;->a:Le/n;

    invoke-interface {v0, p0}, Le/n;->a(Le/m;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Le/m;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/m;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(I)Le/f;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Le/m;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Ljava/lang/String;I)Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(IILandroid/os/Bundle;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
