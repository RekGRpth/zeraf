.class LI/e;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:LI/c;

.field private final b:I


# direct methods
.method constructor <init>(LI/c;Landroid/os/Looper;I)V
    .locals 0

    iput-object p1, p0, LI/e;->a:LI/c;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput p3, p0, LI/e;->b:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LI/f;

    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LI/e;->a:LI/c;

    invoke-static {v2}, LI/c;->g(LI/c;)I

    move-result v2

    invoke-static {v0}, LI/f;->a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->h(LI/c;)Lcom/google/android/location/internal/a;

    move-result-object v1

    iget v3, p0, LI/e;->b:I

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/location/internal/INetworkLocationInternal;->a(ILcom/google/android/location/internal/ILocationListener;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LI/f;

    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_3
    invoke-static {v0}, LI/f;->a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v2

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_1

    :try_start_4
    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->h(LI/c;)Lcom/google/android/location/internal/a;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/location/internal/INetworkLocationInternal;->a(Lcom/google/android/location/internal/ILocationListener;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_1
    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->i(LI/c;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :pswitch_2
    iget-object v0, p0, LI/e;->a:LI/c;

    invoke-static {v0}, LI/c;->j(LI/c;)Lcom/google/android/location/internal/d;

    move-result-object v0

    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_6
    iget-object v2, v0, Lcom/google/android/location/internal/d;->a:Lcom/google/android/location/internal/e;

    iget-object v3, p0, LI/e;->a:LI/c;

    invoke-static {v3}, LI/c;->k(LI/c;)Lcom/google/android/location/internal/d;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/internal/d;->a:Lcom/google/android/location/internal/e;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, LI/e;->a:LI/c;

    invoke-static {v2}, LI/c;->c(LI/c;)LI/f;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LI/e;->a:LI/c;

    invoke-static {v2}, LI/c;->d(LI/c;)LI/e;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, LI/e;->a:LI/c;

    invoke-static {v4}, LI/c;->c(LI/c;)LI/f;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    iget-object v2, p0, LI/e;->a:LI/c;

    invoke-static {v2, v0}, LI/c;->a(LI/c;Lcom/google/android/location/internal/d;)V

    monitor-exit v1

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    :pswitch_3
    iget-object v0, p0, LI/e;->a:LI/c;

    invoke-static {v0}, LI/c;->l(LI/c;)Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LI/f;

    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_7
    invoke-static {v0}, LI/f;->a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :try_start_8
    invoke-interface {v0, v1}, Lcom/google/android/location/internal/INetworkLocationInternal;->a(Z)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LI/f;

    iget-object v1, p0, LI/e;->a:LI/c;

    invoke-static {v1}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_a
    invoke-static {v0}, LI/f;->a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v0

    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_b
    invoke-interface {v0, v1}, Lcom/google/android/location/internal/INetworkLocationInternal;->a(Z)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_c
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v0

    :catch_3
    move-exception v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
