.class LI/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:LI/c;

.field private b:Lcom/google/android/location/internal/INetworkLocationInternal;


# direct methods
.method private constructor <init>(LI/c;)V
    .locals 0

    iput-object p1, p0, LI/f;->a:LI/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LI/c;LI/d;)V
    .locals 0

    invoke-direct {p0, p1}, LI/f;-><init>(LI/c;)V

    return-void
.end method

.method static synthetic a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;
    .locals 1

    iget-object v0, p0, LI/f;->b:Lcom/google/android/location/internal/INetworkLocationInternal;

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->c(LI/c;)LI/f;

    move-result-object v0

    if-eq p0, v0, :cond_0

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->d(LI/c;)LI/e;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->e(LI/c;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, LI/f;->b:Lcom/google/android/location/internal/INetworkLocationInternal;

    if-eqz v0, :cond_3

    :cond_3
    invoke-static {p2}, Lcom/google/android/location/internal/INetworkLocationInternal$Stub;->a(Landroid/os/IBinder;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v0

    iput-object v0, p0, LI/f;->b:Lcom/google/android/location/internal/INetworkLocationInternal;

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->d(LI/c;)LI/e;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->f(LI/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->d(LI/c;)LI/e;

    move-result-object v0

    const/4 v2, 0x5

    invoke-static {v0, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, LI/f;->a:LI/c;

    invoke-static {v0}, LI/c;->b(LI/c;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LI/f;->b:Lcom/google/android/location/internal/INetworkLocationInternal;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
