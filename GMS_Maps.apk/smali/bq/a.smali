.class Lbq/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lbu/f;

.field private final b:Landroid/graphics/Paint;

.field private final c:I

.field private final d:I

.field private final e:Lbu/f;

.field private final f:Landroid/graphics/Bitmap;

.field private final g:Lbu/b;


# direct methods
.method public constructor <init>(IILandroid/graphics/Paint;Landroid/graphics/Bitmap;Lbu/f;)V
    .locals 7

    const-wide/high16 v5, 0x3fe0000000000000L

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lbq/a;->c:I

    iput p2, p0, Lbq/a;->d:I

    iput-object p3, p0, Lbq/a;->b:Landroid/graphics/Paint;

    iput-object p4, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    new-instance v0, Lbu/f;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    add-double/2addr v1, v5

    iget v3, p5, Lbu/f;->a:F

    float-to-double v3, v3

    mul-double/2addr v1, v3

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    add-double/2addr v3, v5

    iget v5, p5, Lbu/f;->b:F

    float-to-double v5, v5

    mul-double/2addr v3, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lbu/f;-><init>(DD)V

    iput-object v0, p0, Lbq/a;->e:Lbu/f;

    new-instance v0, Lbu/b;

    invoke-direct {v0}, Lbu/b;-><init>()V

    iput-object v0, p0, Lbq/a;->g:Lbu/b;

    new-instance v0, Lbu/f;

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    iget-object v4, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v4, p1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    iget-object v5, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v5, p2

    int-to-double v5, v5

    mul-double/2addr v3, v5

    double-to-float v3, v3

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lbu/f;-><init>(FF)V

    iput-object v0, p0, Lbq/a;->a:Lbu/f;

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbu/b;->a(F)V

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const v1, 0x3f666666

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    return-void
.end method

.method public a(Landroid/graphics/Canvas;Z)V
    .locals 5

    const/high16 v4, 0x41200000

    const/high16 v3, 0x40000000

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->a()V

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v1, v0, Lbu/f;->a:F

    iget-object v2, p0, Lbq/a;->e:Lbu/f;

    iget v2, v2, Lbu/f;->a:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->a:F

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v1, v0, Lbu/f;->b:F

    iget-object v2, p0, Lbq/a;->e:Lbu/f;

    iget v2, v2, Lbu/f;->b:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->b:F

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget v1, p0, Lbq/a;->c:I

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    iput v1, v0, Lbu/f;->a:F

    :cond_0
    :goto_0
    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v0, v0, Lbu/f;->b:F

    iget v1, p0, Lbq/a;->d:I

    int-to-float v1, v1

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iput v1, v0, Lbu/f;->b:F

    :cond_1
    :goto_1
    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    const v1, 0x3d4ccccd

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget-object v0, p0, Lbq/a;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget-object v1, p0, Lbq/a;->a:Lbu/f;

    iget v1, v1, Lbu/f;->b:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    invoke-virtual {v0}, Lbu/b;->b()F

    move-result v0

    iget-object v1, p0, Lbq/a;->g:Lbu/b;

    invoke-virtual {v1}, Lbu/b;->b()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    iget-object v3, p0, Lbq/a;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v0, v0, Lbu/f;->a:F

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v1, p0, Lbq/a;->c:I

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, v0, Lbu/f;->a:F

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v0, v0, Lbu/f;->b:F

    iget-object v1, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lbq/a;->a:Lbu/f;

    iget v1, p0, Lbq/a;->d:I

    int-to-float v1, v1

    iget-object v2, p0, Lbq/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Lbu/f;->b:F

    goto/16 :goto_1
.end method

.method public a(Lbu/f;)V
    .locals 7

    const-wide/high16 v5, 0x3fe0000000000000L

    iget-object v0, p0, Lbq/a;->e:Lbu/f;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    add-double/2addr v1, v5

    iget v3, p1, Lbu/f;->a:F

    float-to-double v3, v3

    mul-double/2addr v1, v3

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    add-double/2addr v3, v5

    iget v5, p1, Lbu/f;->b:F

    float-to-double v5, v5

    mul-double/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lbu/f;->a(DD)V

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lbu/b;->b(F)V

    iget-object v0, p0, Lbq/a;->g:Lbu/b;

    const-wide/high16 v1, 0x402e000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    const-wide/high16 v5, 0x4039000000000000L

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Lbu/b;->a(I)V

    return-void
.end method
