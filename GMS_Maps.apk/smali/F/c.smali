.class LF/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LE/o;

.field private final b:LE/d;

.field private final c:LE/i;

.field private final d:LE/o;

.field private final e:LE/i;

.field private final f:I


# direct methods
.method public constructor <init>(Lo/ad;Ljava/util/List;Ljava/util/List;LF/d;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lx/h;->a(Ljava/util/List;)I

    move-result v0

    new-instance v1, LE/r;

    invoke-direct {v1, v0}, LE/r;-><init>(I)V

    iput-object v1, p0, LF/c;->a:LE/o;

    new-instance v1, LE/l;

    invoke-direct {v1, v0}, LE/l;-><init>(I)V

    iput-object v1, p0, LF/c;->c:LE/i;

    new-instance v0, LE/f;

    invoke-static {p2}, Lx/h;->b(Ljava/util/List;)I

    move-result v1

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/c;->b:LE/d;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-direct {p0, p1, v0, p4}, LF/c;->a(Lo/ad;Lo/X;LF/d;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/f;

    invoke-static {v0}, LF/a;->a(Lo/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p4}, LF/d;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    new-instance v0, LE/r;

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/c;->d:LE/o;

    new-instance v0, LE/l;

    invoke-direct {v0, v1}, LE/l;-><init>(I)V

    iput-object v0, p0, LF/c;->e:LE/i;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/f;

    invoke-direct {p0, p1, v0, p4}, LF/c;->a(Lo/ad;Lo/f;LF/d;)V

    goto :goto_2

    :cond_2
    iput-object v3, p0, LF/c;->d:LE/o;

    iput-object v3, p0, LF/c;->e:LE/i;

    :cond_3
    invoke-virtual {p4}, LF/d;->h()I

    move-result v0

    iput v0, p0, LF/c;->f:I

    return-void
.end method

.method private a(Lo/ad;Lo/X;LF/d;)V
    .locals 11

    const/high16 v0, 0x10000

    const/4 v1, 0x0

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    invoke-virtual {p3}, LF/d;->d()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-virtual {p3}, LF/d;->e()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    new-instance v4, Lo/T;

    invoke-direct {v4, v2, v3}, Lo/T;-><init>(II)V

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v5

    invoke-virtual {p3}, LF/d;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move v6, v0

    :goto_0
    invoke-virtual {p3}, LF/d;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    move v7, v1

    :goto_1
    invoke-virtual {p3}, LF/d;->g()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    invoke-virtual {p3}, LF/d;->g()F

    move-result v0

    invoke-virtual {p2, v0}, Lo/X;->c(F)Lo/X;

    move-result-object v1

    :goto_2
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v0

    invoke-virtual {p3}, LF/d;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p3}, LF/d;->b()F

    move-result v2

    :goto_3
    invoke-virtual {p3}, LF/d;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p3}, LF/d;->a()F

    move-result v3

    :goto_4
    iget-object v8, p0, LF/c;->a:LE/o;

    iget-object v9, p0, LF/c;->b:LE/d;

    iget-object v10, p0, LF/c;->c:LE/i;

    invoke-virtual/range {v0 .. v10}, Lx/h;->a(Lo/X;FFLo/T;IIILE/q;LE/e;LE/k;)V

    return-void

    :cond_0
    move v6, v1

    goto :goto_0

    :cond_1
    move v7, v0

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, LF/d;->a()F

    move-result v2

    goto :goto_3

    :cond_3
    invoke-virtual {p3}, LF/d;->b()F

    move-result v3

    goto :goto_4

    :cond_4
    move-object v1, p2

    goto :goto_2
.end method

.method private a(Lo/ad;Lo/f;LF/d;)V
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p2}, Lo/f;->e()Lo/aj;

    move-result-object v0

    invoke-virtual {v0}, Lo/aj;->c()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lo/f;->b()Lo/aF;

    move-result-object v0

    invoke-virtual {v0}, Lo/aF;->a()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    invoke-virtual {p3}, LF/d;->d()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    invoke-virtual {p3}, LF/d;->e()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v3, v2

    new-instance v2, Lo/T;

    invoke-direct {v2, v1, v3}, Lo/T;-><init>(II)V

    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v9

    invoke-static {}, LF/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lo/T;

    aget-object v3, v1, v6

    invoke-static {}, LF/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lo/T;

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {}, LF/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lo/T;

    const/4 v5, 0x2

    aget-object v5, v1, v5

    move v1, v6

    move v7, v6

    :goto_1
    if-ge v1, v8, :cond_2

    invoke-virtual/range {v0 .. v5}, Lo/aF;->a(ILo/T;Lo/T;Lo/T;Lo/T;)V

    iget-object v10, p0, LF/c;->d:LE/o;

    invoke-virtual {v10, v3, v9}, LE/o;->a(Lo/T;I)V

    iget-object v10, p0, LF/c;->d:LE/o;

    invoke-virtual {v10, v4, v9}, LE/o;->a(Lo/T;I)V

    iget-object v10, p0, LF/c;->d:LE/o;

    invoke-virtual {v10, v5, v9}, LE/o;->a(Lo/T;I)V

    add-int/lit8 v7, v7, 0x3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, LF/d;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x10000

    :goto_2
    iget-object v1, p0, LF/c;->e:LE/i;

    invoke-virtual {v1, v0, v6, v7}, LE/i;->a(III)V

    goto :goto_0

    :cond_3
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, LF/c;->a:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v2, p0, LF/c;->b:LE/d;

    invoke-virtual {v2}, LE/d;->c()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, LF/c;->c:LE/i;

    invoke-virtual {v2}, LE/i;->b()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, LF/c;->d:LE/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/c;->d:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, LF/c;->e:LE/i;

    if-eqz v2, :cond_0

    iget-object v1, p0, LF/c;->e:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/c;->a:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    iget-object v0, p0, LF/c;->b:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    iget-object v0, p0, LF/c;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v3, 0x10000

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, LF/c;->f:I

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    const/16 v0, 0x17

    invoke-static {p1, v0}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, LF/c;->a:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/c;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    iget-object v0, p0, LF/c;->b:LE/d;

    invoke-virtual {v0, p1, v4}, LE/d;->a(LD/a;I)V

    iget-object v0, p0, LF/c;->d:LE/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/c;->e:LE/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/c;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    iget-object v0, p0, LF/c;->e:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, LF/c;->d:LE/o;

    invoke-virtual {v2}, LE/o;->a()I

    move-result v2

    invoke-interface {v0, v4, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method public b()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, LF/c;->a:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x24

    iget-object v2, p0, LF/c;->b:LE/d;

    invoke-virtual {v2}, LE/d;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, LF/c;->c:LE/i;

    invoke-virtual {v2}, LE/i;->c()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, LF/c;->d:LE/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/c;->d:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, LF/c;->e:LE/i;

    if-eqz v2, :cond_0

    iget-object v1, p0, LF/c;->e:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/c;->a:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    iget-object v0, p0, LF/c;->b:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    iget-object v0, p0, LF/c;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    return-void
.end method
