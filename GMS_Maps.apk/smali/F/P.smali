.class public LF/P;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final h:Ljava/util/Map;


# instance fields
.field private volatile b:LD/b;

.field private c:LE/i;

.field private final d:[B

.field private e:Lh/e;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LF/P;->h:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>([BLjava/util/Set;Lo/aq;LD/a;)V
    .locals 2

    invoke-direct {p0, p2}, LF/i;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, LF/P;->d:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, LF/P;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LF/P;->g:J

    return-void
.end method

.method private static a(F)LE/i;
    .locals 3

    const/4 v2, 0x0

    sget-object v0, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LE/i;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    const/high16 v1, 0x47800000

    mul-float/2addr v1, p0

    float-to-int v1, v1

    invoke-virtual {v0, v2, v2}, LE/i;->a(II)V

    invoke-virtual {v0, v2, v1}, LE/i;->a(II)V

    invoke-virtual {v0, v1, v2}, LE/i;->a(II)V

    invoke-virtual {v0, v1, v1}, LE/i;->a(II)V

    sget-object v1, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;LD/a;)LF/P;
    .locals 7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ac;

    invoke-virtual {v0}, Lo/ac;->l()[I

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    if-ltz v5, :cond_0

    array-length v6, p1

    if-ge v5, v6, :cond_0

    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, LF/P;

    invoke-virtual {v0}, Lo/ac;->b()[B

    move-result-object v0

    invoke-direct {v1, v0, v2, p0, p3}, LF/P;-><init>([BLjava/util/Set;Lo/aq;LD/a;)V

    return-object v1
.end method

.method public static a([BLo/aq;LD/a;)LF/P;
    .locals 2

    new-instance v0, LF/P;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {v0, p0, v1, p1, p2}, LF/P;-><init>([BLjava/util/Set;Lo/aq;LD/a;)V

    return-object v0
.end method

.method private a(LD/a;LC/a;LD/b;)V
    .locals 4

    const/high16 v1, 0x10000

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v0, p0, LF/P;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    invoke-virtual {p3, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, LF/P;->e:Lh/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, LF/P;->e:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, LF/P;->e:Lh/e;

    iput-boolean v3, p0, LF/P;->f:Z

    :cond_0
    :goto_0
    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v2, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 4

    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    invoke-virtual {p0}, LD/a;->p()V

    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LF/P;->b:LD/b;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LD/b;->i()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, LF/P;->g:J

    return-void
.end method

.method public a(LD/a;)V
    .locals 1

    iget-object v0, p0, LF/P;->b:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/P;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, LF/P;->b:LD/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, LF/P;->f:Z

    :cond_0
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 8

    const/16 v2, 0x4e20

    iget-object v6, p0, LF/P;->b:LD/b;

    if-nez v6, :cond_2

    const/4 v0, 0x0

    iget-boolean v1, p0, LF/P;->f:Z

    if-nez v1, :cond_0

    invoke-virtual {p1, v2}, LD/a;->b(I)V

    invoke-virtual {p0, p1}, LF/P;->c(LD/a;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_2

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LD/b;->c(Z)V

    invoke-virtual {v0, v1}, LD/b;->b(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, LF/P;->b:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v2

    invoke-static {v2}, LF/P;->a(F)LE/i;

    move-result-object v2

    iput-object v2, p0, LF/P;->c:LE/i;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :goto_1
    if-nez v0, :cond_1

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p1, v2}, LD/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, LF/P;->c(LD/a;)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v0, Lh/e;

    iget-wide v1, p0, LF/P;->g:J

    const-wide/16 v3, 0xfa

    sget-object v5, Lh/g;->a:Lh/g;

    invoke-direct/range {v0 .. v5}, Lh/e;-><init>(JJLh/g;)V

    iput-object v0, p0, LF/P;->e:Lh/e;

    move-object v1, v7

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2, v0}, LF/P;->a(LD/a;LC/a;LD/b;)V

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    const/16 v0, 0x60

    iget-object v1, p0, LF/P;->d:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, LF/P;->d:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public b(LD/a;)V
    .locals 1

    invoke-virtual {p0, p1}, LF/P;->a(LD/a;)V

    iget-object v0, p0, LF/P;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method c(LD/a;)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    :try_start_0
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v1

    iget-object v2, p0, LF/P;->d:[B

    invoke-virtual {v1, v2, v0}, Lx/k;->a([BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, LF/P;->f:Z

    return v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, LF/P;->f:Z

    return-void
.end method
