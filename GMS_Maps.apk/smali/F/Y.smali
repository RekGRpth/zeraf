.class public LF/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# static fields
.field private static L:[F

.field public static final a:LR/a;

.field private static final b:LF/d;

.field private static final c:LF/d;

.field private static final d:LF/d;

.field private static final e:LF/d;

.field private static final f:LF/d;

.field private static final g:LF/d;


# instance fields
.field private A:LA/c;

.field private B:I

.field private C:[F

.field private D:J

.field private volatile E:I

.field private F:Ly/b;

.field private G:Ljava/lang/Boolean;

.field private H:J

.field private I:J

.field private J:I

.field private K:J

.field private M:Z

.field private h:Lx/m;

.field private i:Lx/m;

.field private final j:[F

.field private k:[LF/P;

.field private l:[LF/i;

.field private m:[LF/w;

.field private n:[[LF/Q;

.field private o:[LF/i;

.field private p:[LF/w;

.field private q:[LF/e;

.field private r:[LF/V;

.field private s:LF/m;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/Set;

.field private final v:Lo/aq;

.field private final w:Lo/ad;

.field private final x:Ljava/util/HashSet;

.field private y:I

.field private z:Lcom/google/android/maps/driveabout/vector/bE;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const v15, -0x33000001

    const/high16 v11, -0x80000000

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    new-instance v0, LF/d;

    const/high16 v2, 0x42f00000

    const/high16 v4, -0x3dc00000

    const/high16 v5, 0x42400000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->b:LF/d;

    new-instance v0, LF/d;

    const/high16 v2, 0x42700000

    const/high16 v4, -0x3e400000

    const/high16 v5, 0x41c00000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->d:LF/d;

    new-instance v0, LF/d;

    const/high16 v2, 0x41f00000

    const/high16 v4, -0x3f400000

    const/high16 v5, 0x40c00000

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->f:LF/d;

    new-instance v7, LF/d;

    const/high16 v8, 0x42400000

    const/high16 v9, 0x42f00000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->c:LF/d;

    new-instance v7, LF/d;

    const/high16 v8, 0x41c00000

    const/high16 v9, 0x42700000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->e:LF/d;

    new-instance v7, LF/d;

    const/high16 v8, 0x41800000

    const/high16 v9, 0x42200000

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->g:LF/d;

    sget-object v0, LR/a;->a:LR/a;

    sput-object v0, LF/Y;->a:LR/a;

    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, LF/Y;->L:[F

    return-void
.end method

.method private constructor <init>(Lo/aq;LD/a;)V
    .locals 8

    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LF/Y;->h:Lx/m;

    iput-object v1, p0, LF/Y;->i:Lx/m;

    new-array v0, v3, [F

    iput-object v0, p0, LF/Y;->j:[F

    new-array v0, v3, [F

    iput-object v0, p0, LF/Y;->C:[F

    iput-wide v6, p0, LF/Y;->D:J

    const/4 v0, -0x1

    iput v0, p0, LF/Y;->E:I

    iput-object v1, p0, LF/Y;->F:Ly/b;

    iput-object v1, p0, LF/Y;->G:Ljava/lang/Boolean;

    iput-wide v4, p0, LF/Y;->H:J

    iput-wide v4, p0, LF/Y;->I:J

    iput v2, p0, LF/Y;->J:I

    iput-wide v6, p0, LF/Y;->K:J

    iput-boolean v2, p0, LF/Y;->M:Z

    iput-object p1, p0, LF/Y;->v:Lo/aq;

    iget-object v0, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/Y;->w:Lo/ad;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LF/Y;->x:Ljava/util/HashSet;

    return-void
.end method

.method public static a(Lo/ap;LR/a;LD/a;)LF/Y;
    .locals 4

    invoke-interface {p0}, Lo/ap;->d()Lo/aq;

    move-result-object v0

    new-instance v1, LF/Y;

    invoke-direct {v1, v0, p2}, LF/Y;-><init>(Lo/aq;LD/a;)V

    instance-of v0, p0, Lo/aL;

    if-eqz v0, :cond_0

    check-cast p0, Lo/aL;

    invoke-direct {v1, p0, p1, p2}, LF/Y;->a(Lo/aL;LR/a;LD/a;)V

    invoke-virtual {p0}, Lo/aL;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LF/Y;->b(J)V

    :cond_0
    return-object v1
.end method

.method private a(Lo/aL;)LF/d;
    .locals 5

    const/high16 v4, 0x41940000

    const/high16 v3, 0x41840000

    const/4 v1, 0x0

    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    sget-object v2, Lo/av;->c:Lo/av;

    invoke-virtual {v0, v2}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v0

    check-cast v0, Lo/E;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lo/E;->b()Lo/r;

    move-result-object v0

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v0}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v2

    if-nez v2, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lo/z;->e()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_4

    sget-object v0, LF/Y;->f:LF/d;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    sget-object v0, LF/Y;->d:LF/d;

    goto :goto_0

    :cond_5
    sget-object v0, LF/Y;->b:LF/d;

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Lo/z;->e()I

    move-result v0

    if-gez v0, :cond_9

    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_7

    sget-object v0, LF/Y;->g:LF/d;

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    sget-object v0, LF/Y;->e:LF/d;

    goto :goto_0

    :cond_8
    sget-object v0, LF/Y;->c:LF/d;

    goto :goto_0

    :cond_9
    move-object v0, v1

    goto :goto_0
.end method

.method private a(LD/a;F)V
    .locals 5

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    invoke-virtual {p1}, LD/a;->r()V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sub-float v0, v4, p2

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    mul-float v2, v4, v0

    invoke-interface {v1, v3, v3, v2, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    iget-object v0, p1, LD/a;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, LD/a;->s()V

    return-void
.end method

.method private a(Lo/aL;LR/a;LD/a;)V
    .locals 29

    invoke-virtual/range {p1 .. p1}, Lo/aL;->p()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, LF/Y;->y:I

    invoke-virtual/range {p1 .. p1}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    move-object/from16 v0, p0

    iget-object v5, v0, LF/Y;->x:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lo/aL;->g()LA/c;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->A:LA/c;

    invoke-virtual/range {p1 .. p1}, Lo/aL;->h()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, LF/Y;->B:I

    invoke-virtual/range {p1 .. p1}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v2

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, LF/y;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-direct {v10, v1, v2}, LF/y;-><init>(Lo/aq;[Ljava/lang/String;)V

    new-instance v15, LF/y;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-direct {v15, v1, v2}, LF/y;-><init>(Lo/aq;[Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct/range {p0 .. p1}, LF/Y;->a(Lo/aL;)LF/d;

    move-result-object v11

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lo/aL;->k()Lo/aO;

    move-result-object v3

    const/4 v1, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-wide/from16 v19, v4

    move/from16 v21, v1

    :goto_1
    invoke-interface {v3}, Lo/aO;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v3}, Lo/aO;->b()Lo/n;

    move-result-object v1

    invoke-interface {v1}, Lo/n;->h()I

    move-result v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LR/a;->a(I)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v3}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-interface {v1}, Lo/n;->h()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    invoke-interface {v3}, Lo/aO;->next()Ljava/lang/Object;

    move/from16 v1, v21

    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v7, v4, v19

    const-wide/16 v12, 0xa

    cmp-long v7, v7, v12

    if-lez v7, :cond_15

    invoke-static {}, Ljava/lang/Thread;->yield()V

    :goto_3
    move-wide/from16 v19, v4

    move/from16 v21, v1

    goto :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    move-object/from16 v0, p0

    iget-object v4, v0, LF/Y;->i:Lx/m;

    move-object/from16 v0, p0

    iget-object v5, v0, LF/Y;->h:Lx/m;

    move-object/from16 v7, p3

    invoke-static/range {v1 .. v7}, LF/Q;->a(Lo/aq;[Ljava/lang/String;Lo/aO;Lz/D;Lz/D;Lx/n;LD/a;)LF/Q;

    move-result-object v1

    if-eqz v21, :cond_2

    invoke-virtual {v1}, LF/Q;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v21, 0x0

    :cond_2
    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_2

    :pswitch_2
    if-eqz v21, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, LF/Y;->v:Lo/aq;

    const/4 v12, 0x2

    move-object v8, v2

    move-object v9, v3

    move-object/from16 v13, p3

    invoke-static/range {v7 .. v13}, LF/a;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, LF/Y;->v:Lo/aq;

    const/16 v17, 0x9

    move-object v13, v2

    move-object v14, v3

    move-object/from16 v16, v11

    move-object/from16 v18, p3

    invoke-static/range {v12 .. v18}, LF/a;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_2

    :pswitch_3
    if-eqz v21, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/16 v4, 0xa

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :pswitch_4
    check-cast v1, Lo/K;

    invoke-static {v1}, LF/G;->a(Lo/K;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v21, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/16 v4, 0xa

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :cond_6
    if-eqz v21, :cond_7

    invoke-virtual {v10, v3}, LF/y;->a(Lo/aO;)V

    move/from16 v1, v21

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v15, v3}, LF/y;->a(Lo/aO;)V

    move/from16 v1, v21

    goto/16 :goto_2

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-static {v1, v2, v3}, LF/e;->a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/e;

    move-result-object v1

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v0}, LF/P;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LD/a;)LF/P;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-static {v1, v2, v3}, LF/V;->a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/V;

    move-result-object v1

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_2

    :cond_8
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/P;

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/P;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->k:[LF/P;

    :cond_9
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/i;

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/i;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->l:[LF/i;

    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_b

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [[LF/Q;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->n:[[LF/Q;

    const/4 v1, 0x0

    move v3, v1

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->n:[[LF/Q;

    array-length v1, v1

    if-ge v3, v1, :cond_b

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, LF/Y;->n:[[LF/Q;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [LF/Q;

    invoke-interface {v1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/Q;

    aput-object v1, v4, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    :cond_b
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_c

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/i;

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/i;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->o:[LF/i;

    :cond_c
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/e;

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/e;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->q:[LF/e;

    :cond_d
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_e

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/V;

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/V;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->r:[LF/V;

    :cond_e
    const/4 v1, 0x3

    invoke-virtual {v10, v1}, LF/y;->a(I)[LF/w;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->m:[LF/w;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_f

    :cond_f
    const/16 v1, 0xa

    invoke-virtual {v15, v1}, LF/y;->a(I)[LF/w;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->p:[LF/w;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_10

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lo/aL;->j()I

    move-result v5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->t:Ljava/util/ArrayList;

    const/4 v1, 0x0

    move v4, v1

    :goto_5
    if-ge v4, v5, :cond_13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lo/aL;->a(I)Lo/n;

    move-result-object v3

    invoke-interface {v3}, Lo/n;->h()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_11
    :goto_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5

    :sswitch_0
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    move-object v1, v3

    check-cast v1, Lo/U;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LF/Y;->a(Lo/U;)V

    invoke-interface {v3}, Lo/n;->l()[I

    move-result-object v3

    array-length v6, v3

    const/4 v1, 0x0

    :goto_7
    if-ge v1, v6, :cond_11

    aget v7, v3, v1

    if-ltz v7, :cond_12

    array-length v8, v2

    if-ge v7, v8, :cond_12

    move-object/from16 v0, p0

    iget-object v8, v0, LF/Y;->x:Ljava/util/HashSet;

    aget-object v7, v2, v7

    invoke-virtual {v8, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :sswitch_1
    move-object v1, v3

    check-cast v1, Lo/K;

    invoke-virtual {v1}, Lo/K;->c()I

    move-result v1

    if-lez v1, :cond_11

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    goto :goto_6

    :sswitch_2
    move-object v1, v3

    check-cast v1, Lo/af;

    invoke-virtual {v1}, Lo/af;->d()I

    move-result v1

    if-lez v1, :cond_11

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    goto :goto_6

    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v2, LF/Z;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LF/Z;-><init>(LF/Y;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v0, p1

    instance-of v1, v0, Lo/P;

    if-eqz v1, :cond_14

    check-cast p1, Lo/P;

    invoke-virtual/range {p1 .. p1}, Lo/P;->l()Ljava/util/Set;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->u:Ljava/util/Set;

    :cond_14
    return-void

    :cond_15
    move-wide/from16 v4, v19

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Lo/n;)V
    .locals 3

    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v1, Ly/c;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ly/c;-><init>(Lo/n;Ly/b;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private k()I
    .locals 3

    const/16 v0, 0x18

    iget-object v1, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    invoke-virtual {v0}, Ly/c;->d()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_3

    invoke-static {p1, p2}, LF/Q;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v1

    or-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_4

    or-int/lit16 v0, v0, 0x200

    :cond_4
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_5

    or-int/lit16 v0, v0, 0x400

    :cond_5
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_7

    invoke-virtual {p1}, LC/a;->q()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    or-int/lit16 v0, v0, 0x800

    :cond_6
    or-int/lit16 v0, v0, 0x1000

    :cond_7
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_8

    or-int/lit16 v0, v0, 0x2000

    :cond_8
    sget-boolean v1, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v1, :cond_9

    const v1, 0x8000

    or-int/2addr v0, v1

    :cond_9
    return v0
.end method

.method public a(J)V
    .locals 4

    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_0

    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1, p2}, LF/P;->a(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, LF/Y;->x:Ljava/util/HashSet;

    invoke-interface {p3, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_0

    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_0

    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/P;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_1

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_1

    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/i;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_2

    and-int/lit8 v1, p2, 0x8

    if-eqz v1, :cond_2

    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/w;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_4

    and-int/lit16 v1, p2, 0x1f0

    if-eqz v1, :cond_4

    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7}, LF/Q;->d()Ljava/util/Set;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_5

    and-int/lit16 v1, p2, 0x200

    if-eqz v1, :cond_5

    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/i;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_6

    and-int/lit16 v1, p2, 0x400

    if-eqz v1, :cond_6

    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/w;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_7

    and-int/lit16 v1, p2, 0x1800

    if-eqz v1, :cond_7

    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/e;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_8

    and-int/lit16 v1, p2, 0x2000

    if-eqz v1, :cond_8

    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    invoke-virtual {v3}, LF/V;->d()Ljava/util/Set;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_8
    return-void
.end method

.method public a(LC/a;LD/a;)V
    .locals 0

    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public a(LD/a;)V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_0

    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/P;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_1

    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, LF/h;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_2

    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/w;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_4

    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, LF/Q;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_5

    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, LF/h;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_6

    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/w;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_7

    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/e;->a(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, LF/Y;->s:LF/m;

    if-eqz v1, :cond_8

    iget-object v1, p0, LF/Y;->s:LF/m;

    invoke-virtual {v1, p1}, LF/m;->a(LD/a;)V

    const/4 v1, 0x0

    iput-object v1, p0, LF/Y;->s:LF/m;

    :cond_8
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_9

    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, LF/V;->a(LD/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 11

    const/high16 v2, 0x3f800000

    invoke-virtual {p1}, LD/a;->I()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p1}, LD/a;->J()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v4

    iget-wide v6, p0, LF/Y;->D:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v4

    iput-wide v4, p0, LF/Y;->D:J

    iget-object v1, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {p2}, LC/a;->j()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p2}, LC/a;->q()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    invoke-virtual {p2}, LC/a;->p()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    invoke-virtual {p2}, LC/a;->r()F

    move-result v4

    invoke-virtual {p2}, LC/a;->r()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    sget-object v4, LF/Y;->L:[F

    invoke-virtual {p2, v1, v4}, LC/a;->a(Lo/T;[F)V

    sget-object v1, LF/Y;->L:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    sget-object v4, LF/Y;->L:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2, v1, v4}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    :cond_0
    iget-object v4, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v4}, Lo/ad;->g()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LF/Y;->C:[F

    invoke-static {p1, p2, v1, v4, v5}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F[F)V

    :cond_1
    iget-object v1, p0, LF/Y;->C:[F

    invoke-static {v3, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_11

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_11

    iget-object v0, p0, LF/Y;->l:[LF/i;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->m:[LF/w;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->o:[LF/i;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->p:[LF/w;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->q:[LF/e;

    if-nez v0, :cond_10

    iget-object v0, p0, LF/Y;->r:[LF/V;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    :cond_3
    :goto_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, LF/P;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_1
    iget-object v0, p0, LF/Y;->l:[LF/i;

    if-eqz v0, :cond_5

    iget-object v1, p0, LF/Y;->l:[LF/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v2, :cond_5

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, LF/h;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_2

    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v6, :cond_6

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :pswitch_2
    iget-object v0, p0, LF/Y;->m:[LF/w;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->m:[LF/w;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, LF/w;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_3
    if-nez v0, :cond_2

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_2

    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_9
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v6, :cond_7

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :pswitch_4
    if-eqz v0, :cond_c

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_c

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-static {p2, v0}, LF/Q;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v2

    new-instance v4, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v1, 0x4

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    new-instance v5, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v1, 0x6

    invoke-direct {v5, v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    iget-object v6, p0, LF/Y;->n:[[LF/Q;

    array-length v7, v6

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v7, :cond_2

    aget-object v8, v6, v1

    and-int/lit8 v0, v2, 0x10

    if-eqz v0, :cond_8

    invoke-virtual {p0, p1, p2, v4}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v9, :cond_8

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, v4}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_8
    and-int/lit8 v0, v2, 0x20

    if-eqz v0, :cond_9

    invoke-virtual {p0, p1, p2, p3}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v9, :cond_9

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_9
    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_a

    invoke-virtual {p1}, LD/a;->o()V

    invoke-virtual {p0, p1, p2, v5}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_e
    if-ge v0, v9, :cond_a

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, v5}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_a
    and-int/lit16 v0, v2, 0x180

    if-eqz v0, :cond_b

    invoke-virtual {p1}, LD/a;->n()V

    invoke-virtual {p0, p1, p2, p3}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    array-length v9, v8

    const/4 v0, 0x0

    :goto_f
    if-ge v0, v9, :cond_b

    aget-object v10, v8, v0

    invoke-virtual {v10, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_c
    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_2

    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_10
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_11
    if-ge v0, v6, :cond_d

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    :pswitch_5
    if-nez v0, :cond_2

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_2

    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_12
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_13
    if-ge v0, v6, :cond_e

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    :pswitch_6
    if-nez v0, :cond_2

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_2

    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_14
    if-ge v1, v4, :cond_2

    aget-object v5, v2, v1

    array-length v6, v5

    const/4 v0, 0x0

    :goto_15
    if-ge v0, v6, :cond_f

    aget-object v7, v5, v0

    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    :pswitch_7
    iget-object v0, p0, LF/Y;->o:[LF/i;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->o:[LF/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_16
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-interface {v4, p1, p2, p3}, LF/h;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :pswitch_8
    iget-object v0, p0, LF/Y;->p:[LF/w;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->p:[LF/w;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_17
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, LF/w;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    :pswitch_9
    iget-object v0, p0, LF/Y;->q:[LF/e;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->q:[LF/e;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, LF/e;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :pswitch_a
    iget-object v0, p0, LF/Y;->r:[LF/V;

    if-eqz v0, :cond_2

    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v2, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4, p1, p2, p3}, LF/V;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    :pswitch_b
    const/high16 v0, 0x3f800000

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, v2}, LF/Y;->a(LD/a;F)V

    goto/16 :goto_1

    :pswitch_c
    sget-object v0, LF/U;->a:LF/U;

    invoke-virtual {v0, p1, p2, p3}, LF/U;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public a(LF/m;)V
    .locals 0

    iput-object p1, p0, LF/Y;->s:LF/m;

    return-void
.end method

.method public a(Lo/U;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    if-nez v0, :cond_0

    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v0

    iget-object v1, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    iget-object v2, p0, LF/Y;->A:LA/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    iput-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    :cond_0
    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    invoke-virtual {p1}, Lo/U;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bE;->d(I)F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    invoke-virtual {p1}, Lo/U;->g()I

    move-result v0

    int-to-float v0, v0

    :cond_1
    invoke-virtual {p1, v0}, Lo/U;->a(F)V

    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    invoke-virtual {p1}, Lo/U;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bE;->e(I)F

    move-result v0

    cmpg-float v1, v0, v3

    if-gez v1, :cond_2

    const/16 v0, 0x16

    invoke-virtual {p1}, Lo/U;->j()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    :cond_2
    invoke-virtual {p1, v0}, Lo/U;->b(F)V

    return-void
.end method

.method public a(Ly/b;)V
    .locals 4

    iget-object v0, p0, LF/Y;->F:Ly/b;

    invoke-static {p1, v0}, Ly/b;->a(Ly/b;Ly/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, LF/Y;->F:Ly/b;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v0

    iget-object v2, p0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v3, Ly/c;

    invoke-direct {v3, v0, p1}, Ly/c;-><init>(Lo/n;Ly/b;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, LF/Y;->E:I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, LF/Y;->M:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, LF/Y;->M:Z

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .locals 1

    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/aF;->a(Ljava/util/Iterator;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, LF/Y;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LF/Y;->H:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)Z
    .locals 1

    iget-object v0, p0, LF/Y;->u:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LF/Y;->u:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    iput-object v0, p0, LF/Y;->u:Ljava/util/Set;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lo/aq;
    .locals 1

    iget-object v0, p0, LF/Y;->v:Lo/aq;

    return-object v0
.end method

.method public b(J)V
    .locals 4

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, LF/Y;->H:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, LF/Y;->H:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    :cond_2
    iput-wide p1, p0, LF/Y;->H:J

    const-wide/32 v0, 0xea60

    add-long/2addr v0, p1

    iput-wide v0, p0, LF/Y;->I:J

    goto :goto_0
.end method

.method public b(LD/a;)V
    .locals 8

    const/4 v0, 0x0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, p0, LF/Y;->G:Ljava/lang/Boolean;

    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_0

    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/P;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_1

    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, LF/h;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_2

    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/w;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_4

    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7, p1}, LF/Q;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_5

    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    invoke-interface {v4, p1}, LF/h;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_6

    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/w;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_7

    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, LF/e;->b(LD/a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    iget-object v1, p0, LF/Y;->s:LF/m;

    if-eqz v1, :cond_8

    iget-object v1, p0, LF/Y;->s:LF/m;

    invoke-virtual {v1, p1}, LF/m;->b(LD/a;)V

    const/4 v1, 0x0

    iput-object v1, p0, LF/Y;->s:LF/m;

    :cond_8
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_9

    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_8
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, LF/V;->b(LD/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 3

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {p1, v0}, LF/P;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1, p3}, LF/a;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v0}, LF/w;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->a(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->b(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    invoke-static {p1, v1, v0}, LF/Q;->a(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->c(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    invoke-static {p1, v1, v0}, LF/Q;->b(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_9
    invoke-static {p1, p3}, LF/a;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0

    :pswitch_a
    invoke-static {p1, v0}, LF/w;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_b
    invoke-static {p1, v1}, LF/e;->a(LD/a;I)V

    goto :goto_0

    :pswitch_c
    invoke-static {p1, v1}, LF/e;->a(LD/a;I)V

    goto :goto_0

    :pswitch_d
    invoke-static {p1, v0}, LF/V;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :pswitch_e
    invoke-static {p1, p3}, LF/U;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .locals 4

    iget-wide v0, p0, LF/Y;->I:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LF/Y;->I:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LA/c;
    .locals 1

    iget-object v0, p0, LF/Y;->A:LA/c;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, LF/Y;->B:I

    return v0
.end method

.method public e()LF/m;
    .locals 1

    iget-object v0, p0, LF/Y;->s:LF/m;

    return-object v0
.end method

.method public f()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_0

    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/P;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_0

    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, LF/P;->e()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h()I
    .locals 1

    iget v0, p0, LF/Y;->y:I

    return v0
.end method

.method public i()I
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_0

    iget-object v4, p0, LF/Y;->k:[LF/P;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/P;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, LF/Y;->m:[LF/w;

    if-eqz v2, :cond_2

    iget-object v4, p0, LF/Y;->m:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    if-eqz v2, :cond_4

    iget-object v4, p0, LF/Y;->n:[[LF/Q;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_3

    aget-object v8, v6, v0

    invoke-virtual {v8}, LF/Q;->a()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v2, p0, LF/Y;->p:[LF/w;

    if-eqz v2, :cond_5

    iget-object v4, p0, LF/Y;->p:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_5
    iget-object v2, p0, LF/Y;->q:[LF/e;

    if-eqz v2, :cond_6

    iget-object v4, p0, LF/Y;->q:[LF/e;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/e;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_6
    iget-object v2, p0, LF/Y;->r:[LF/V;

    if-eqz v2, :cond_7

    iget-object v4, p0, LF/Y;->r:[LF/V;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/V;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_7
    iget-object v2, p0, LF/Y;->l:[LF/i;

    if-eqz v2, :cond_8

    iget-object v4, p0, LF/Y;->l:[LF/i;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_8

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/i;->a()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_8
    iget-object v2, p0, LF/Y;->o:[LF/i;

    if-eqz v2, :cond_9

    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    :goto_8
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    invoke-virtual {v4}, LF/i;->a()I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    return v0
.end method

.method public j()I
    .locals 9

    const/4 v1, 0x0

    const/16 v0, 0x100

    iget-object v2, p0, LF/Y;->k:[LF/P;

    if-eqz v2, :cond_0

    const/16 v0, 0x110

    iget-object v4, p0, LF/Y;->k:[LF/P;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/P;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    iget-object v2, p0, LF/Y;->m:[LF/w;

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->m:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_1
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    if-eqz v2, :cond_3

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->n:[[LF/Q;

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    invoke-virtual {v8}, LF/Q;->b()I

    move-result v8

    add-int/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v2, p0, LF/Y;->p:[LF/w;

    if-eqz v2, :cond_4

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->p:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_4

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    :cond_4
    iget-object v2, p0, LF/Y;->q:[LF/e;

    if-eqz v2, :cond_5

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->q:[LF/e;

    array-length v5, v4

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/e;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    :cond_5
    iget-object v2, p0, LF/Y;->r:[LF/V;

    if-eqz v2, :cond_6

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->r:[LF/V;

    array-length v5, v4

    move v2, v1

    :goto_6
    if-ge v2, v5, :cond_6

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/V;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    :cond_6
    iget-object v2, p0, LF/Y;->l:[LF/i;

    if-eqz v2, :cond_7

    add-int/lit8 v0, v0, 0x10

    iget-object v4, p0, LF/Y;->l:[LF/i;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_7

    aget-object v3, v4, v2

    invoke-virtual {v3}, LF/i;->b()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_7

    :cond_7
    iget-object v2, p0, LF/Y;->o:[LF/i;

    if-eqz v2, :cond_8

    add-int/lit8 v0, v0, 0x10

    iget-object v3, p0, LF/Y;->o:[LF/i;

    array-length v4, v3

    :goto_8
    if-ge v1, v4, :cond_8

    aget-object v2, v3, v1

    invoke-virtual {v2}, LF/i;->b()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_8

    :cond_8
    iget v1, p0, LF/Y;->E:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_9

    invoke-direct {p0}, LF/Y;->k()I

    move-result v1

    iput v1, p0, LF/Y;->E:I

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method
