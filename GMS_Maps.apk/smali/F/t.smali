.class public LF/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/u;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/aV;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lo/aj;

.field private final e:Lcom/google/android/maps/driveabout/vector/aX;

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:F

.field private final j:I


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    iput-object p2, p0, LF/t;->b:Ljava/lang/String;

    iput p3, p0, LF/t;->c:I

    iput-object p4, p0, LF/t;->d:Lo/aj;

    const/high16 v6, 0x3f800000

    invoke-virtual {p4}, Lo/aj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4}, Lo/aj;->i()Lo/an;

    move-result-object v0

    invoke-virtual {v0}, Lo/an;->b()I

    move-result v0

    :goto_0
    iput v0, p0, LF/t;->j:I

    invoke-virtual {p4}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->g()F

    move-result v6

    :cond_0
    iput-object p5, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v0, p0, LF/t;->d:Lo/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v0}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_1
    int-to-float v4, p3

    move-object v0, p1

    move-object v1, p2

    move-object v2, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZF)[F

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->c:Lcom/google/android/maps/driveabout/vector/aX;

    if-ne p5, v1, :cond_3

    aget v1, v0, v7

    const v2, 0x3f4ccccd

    mul-float/2addr v1, v2

    iput v1, p0, LF/t;->f:F

    :goto_2
    aget v1, v0, v5

    iput v1, p0, LF/t;->g:F

    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, LF/t;->h:F

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, LF/t;->i:F

    return-void

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    aget v1, v0, v7

    iput v1, p0, LF/t;->f:F

    goto :goto_2
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, LF/t;->f:F

    return v0
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .locals 9

    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-static {v0, p2}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v7

    iget-object v0, p0, LF/t;->d:Lo/aj;

    invoke-static {v0, p2}, LF/m;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    iget v0, p0, LF/t;->j:I

    if-eqz v0, :cond_1

    const/4 v7, 0x0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_1

    :cond_0
    iget v0, p0, LF/t;->j:I

    invoke-static {v0}, LF/m;->c(I)I

    move-result v6

    :cond_1
    iget-object v0, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LF/t;->b:Ljava/lang/String;

    iget-object v3, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v1, p0, LF/t;->d:Lo/aj;

    if-eqz v1, :cond_2

    iget-object v1, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v1}, Lo/aj;->h()Lo/ao;

    move-result-object v4

    :goto_0
    iget v1, p0, LF/t;->c:I

    int-to-float v5, v1

    iget v8, p0, LF/t;->j:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;
    .locals 8

    iget-object v0, p0, LF/t;->a:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LF/t;->b:Ljava/lang/String;

    iget-object v2, p0, LF/t;->e:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v3, p0, LF/t;->d:Lo/aj;

    if-eqz v3, :cond_0

    iget-object v3, p0, LF/t;->d:Lo/aj;

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_0
    iget v4, p0, LF/t;->c:I

    int-to-float v4, v4

    iget-object v5, p0, LF/t;->d:Lo/aj;

    invoke-static {v5, p1}, LF/m;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v5

    iget-object v6, p0, LF/t;->d:Lo/aj;

    invoke-static {v6, p1}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    iget v7, p0, LF/t;->j:I

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public b()F
    .locals 1

    iget v0, p0, LF/t;->g:F

    return v0
.end method

.method public c()F
    .locals 1

    iget v0, p0, LF/t;->h:F

    return v0
.end method

.method public d()F
    .locals 1

    iget v0, p0, LF/t;->i:F

    return v0
.end method

.method public e()F
    .locals 2

    iget v0, p0, LF/t;->g:F

    iget v1, p0, LF/t;->h:F

    sub-float/2addr v0, v1

    iget v1, p0, LF/t;->i:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public f()V
    .locals 0

    return-void
.end method
