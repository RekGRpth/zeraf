.class public Lam/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lam/g;

.field public static final b:Lam/c;

.field private static c:Lcom/google/googlenav/common/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-object v0, Lam/r;->a:Lam/g;

    sput-object v0, Lam/r;->b:Lam/c;

    new-instance v0, Lcom/google/googlenav/common/f;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/f;-><init>(I)V

    sput-object v0, Lam/r;->c:Lcom/google/googlenav/common/f;

    return-void
.end method

.method private static a(Ljava/lang/String;C)I
    .locals 3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0, p1}, Lam/n;->a(CC)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v1, v2}, Lam/n;->a(CC)Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 2

    const-string v0, " \u2003"

    invoke-static {p0, v0, p1}, Lam/r;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    return v0
.end method

.method public static a(Ljava/lang/String;Lam/d;I)I
    .locals 4

    const/4 v1, 0x0

    if-gtz p2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {p1, p0}, Lam/d;->a(Ljava/lang/String;)I

    move-result v0

    if-gt v0, p2, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    mul-int/lit8 v3, p2, 0x2

    if-le v0, v3, :cond_4

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-interface {p1, p0, v1, v0}, Lam/d;->a(Ljava/lang/String;II)I

    move-result v3

    if-ge v3, p2, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-eq v3, p2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_4
    move v0, v2

    :goto_2
    if-lez v0, :cond_0

    invoke-interface {p1, p0, v1, v0}, Lam/d;->a(Ljava/lang/String;II)I

    move-result v2

    if-le v2, p2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p2, v1, :cond_1

    invoke-virtual {p0, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_1
    return p2

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    move p2, v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const-string v0, " "

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lam/r;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    const/4 v0, -0x1

    if-ne v2, v0, :cond_2

    const-string p0, ""

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    :goto_1
    if-ltz v0, :cond_3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lam/d;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-interface {p1, p0}, Lam/d;->a(Ljava/lang/String;)I

    move-result v0

    if-gt v0, p2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lam/r;->b(Ljava/lang/String;Lam/d;ILjava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Lam/e;Lam/d;Ljava/lang/String;IIII[ILam/g;Lam/c;)V
    .locals 9

    const/4 v3, 0x0

    invoke-interface {p1}, Lam/d;->a()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/common/Config;->j()Z

    move-result v1

    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Lam/e;->a(Lam/d;)V

    :cond_0
    invoke-static {p2, p3, p4}, Lam/r;->a(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v5

    if-eqz v1, :cond_1

    array-length v4, v5

    const/4 v1, 0x0

    :goto_0
    div-int/lit8 v6, v4, 0x2

    if-ge v1, v6, :cond_1

    sub-int v6, v4, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v7, v5, v1

    aget-object v8, v5, v6

    aput-object v8, v5, v1

    aput-object v7, v5, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    array-length v4, v5

    if-ge v1, v4, :cond_7

    aget-object v4, v5, v1

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-gez v4, :cond_4

    if-eqz p0, :cond_2

    aget-object v4, v5, v1

    add-int v6, p5, v3

    invoke-interface {p1, p0, v4, v6, p6}, Lam/d;->a(Lam/e;Ljava/lang/String;II)V

    :cond_2
    aget-object v4, v5, v1

    invoke-interface {p1, v4}, Lam/d;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    aget-object v4, v5, v1

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const v4, 0xff00

    if-lt v6, v4, :cond_5

    const v4, 0xff00

    sub-int v4, v6, v4

    int-to-char v4, v4

    if-eqz p9, :cond_3

    move-object/from16 v0, p9

    invoke-interface {v0, v4}, Lam/c;->a(C)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object/from16 v0, p9

    invoke-interface {v0, v4}, Lam/c;->b(C)Lam/d;

    move-result-object p1

    invoke-interface {p1}, Lam/d;->a()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-eqz p0, :cond_3

    invoke-interface {p0, p1}, Lam/e;->a(Lam/d;)V

    goto :goto_2

    :cond_5
    if-eqz p8, :cond_3

    move-object/from16 v0, p8

    invoke-interface {v0, v6}, Lam/g;->a(C)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p8

    invoke-interface {v0, v6}, Lam/g;->b(C)I

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-eqz p0, :cond_6

    move-object/from16 v0, p8

    invoke-interface {v0, v6}, Lam/g;->d(C)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v2, p6

    :goto_3
    add-int v7, p5, v3

    invoke-static {p6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p8

    invoke-interface {v0, v6, p0, v7, v2}, Lam/g;->a(CLam/e;II)Z

    :cond_6
    move-object/from16 v0, p8

    invoke-interface {v0, v6}, Lam/g;->c(C)I

    move-result v2

    add-int/2addr v3, v2

    move v2, v4

    goto :goto_2

    :pswitch_0
    sub-int v2, v4, v7

    add-int/2addr v2, p6

    goto :goto_3

    :pswitch_1
    sub-int v2, v4, v7

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p6

    goto :goto_3

    :pswitch_2
    invoke-interface {p1}, Lam/d;->c()I

    move-result v2

    sub-int/2addr v2, v7

    add-int/2addr v2, p6

    goto :goto_3

    :cond_7
    if-eqz p7, :cond_8

    const/4 v1, 0x0

    aput v3, p7, v1

    const/4 v1, 0x1

    aput v2, p7, v1

    :cond_8
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lam/e;Lam/d;Ljava/lang/String;II[ILam/g;Lam/c;)V
    .locals 10

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-static/range {v0 .. v9}, Lam/r;->a(Lam/e;Lam/d;Ljava/lang/String;IIII[ILam/g;Lam/c;)V

    return-void
.end method

.method private static a(C)Z
    .locals 2

    const-string v0, " \u2003"

    invoke-virtual {v0, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lam/e;IIII)Z
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p0}, Lam/e;->b()I

    move-result v1

    add-int v2, p2, p4

    if-ge v1, v2, :cond_0

    invoke-interface {p0}, Lam/e;->d()I

    move-result v2

    add-int/2addr v1, v2

    if-ge p2, v1, :cond_0

    invoke-interface {p0}, Lam/e;->a()I

    move-result v1

    add-int v2, p1, p3

    if-ge v1, v2, :cond_0

    invoke-interface {p0}, Lam/e;->c()I

    move-result v2

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Landroid/graphics/Point;Landroid/graphics/Point;I)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, p2, :cond_0

    iget v1, p0, Landroid/graphics/Point;->y:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lam/d;Ljava/lang/String;Lam/g;Lam/c;)[I
    .locals 10

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v0, v4, 0x2

    new-array v5, v0, [I

    const/4 v3, 0x0

    invoke-interface {p0}, Lam/d;->a()I

    move-result v0

    const/4 v2, 0x0

    move-object v1, p0

    :goto_0
    if-ge v2, v4, :cond_5

    add-int/lit8 v6, v2, 0x2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_0

    invoke-interface {v1, v7}, Lam/d;->a(C)I

    move-result v7

    aput v7, v5, v6

    aget v6, v5, v6

    add-int/2addr v3, v6

    move v9, v2

    move v2, v3

    move-object v3, v1

    move v1, v0

    move v0, v9

    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v9, v0

    move v0, v1

    move-object v1, v3

    move v3, v2

    move v2, v9

    goto :goto_0

    :cond_0
    add-int/lit8 v7, v2, 0x1

    if-lt v7, v4, :cond_1

    const/4 v7, 0x0

    aput v7, v5, v6

    move v9, v2

    move v2, v3

    move-object v3, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    :cond_1
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const v8, 0xff00

    if-lt v7, v8, :cond_3

    if-eqz p3, :cond_2

    const v1, 0xff00

    sub-int v1, v7, v1

    int-to-char v1, v1

    invoke-interface {p3, v1}, Lam/c;->b(C)Lam/d;

    move-result-object v1

    invoke-interface {v1}, Lam/d;->a()I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_2
    const/4 v7, 0x0

    aput v7, v5, v6

    :goto_2
    aget v7, v5, v6

    add-int/2addr v3, v7

    add-int/lit8 v6, v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    add-int/lit8 v2, v2, 0x1

    move v9, v2

    move v2, v3

    move-object v3, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    invoke-interface {p2, v7}, Lam/g;->c(C)I

    move-result v8

    aput v8, v5, v6

    invoke-interface {p2, v7}, Lam/g;->b(C)I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_4
    add-int/lit8 v7, v2, 0x2

    invoke-virtual {p1, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7}, Lam/d;->a(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    aput v3, v5, v1

    const/4 v1, 0x1

    aput v0, v5, v1

    return-object v5
.end method

.method public static a(IILam/d;Ljava/lang/String;IZ)[Ljava/lang/String;
    .locals 9

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-lez p4, :cond_5

    invoke-static {p3}, Lam/r;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lam/d;->a(Ljava/lang/String;)I

    move-result v0

    :goto_1
    const/4 v1, 0x0

    move v8, v1

    move v1, v0

    move v0, v8

    :goto_2
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-interface {p2, v2}, Lam/d;->a(C)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    invoke-interface {p2, p3}, Lam/d;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :cond_2
    const-string v7, ""

    sub-int v0, p0, p4

    if-le v0, v1, :cond_3

    sub-int v2, p0, p4

    const/4 v3, 0x1

    sget-object v4, Lam/r;->a:Lam/g;

    sget-object v5, Lam/r;->b:Lam/c;

    const/4 v6, 0x1

    move-object v0, p2

    move-object v1, p3

    invoke-static/range {v0 .. v6}, Lam/r;->a(Lam/d;Ljava/lang/String;IILam/g;Lam/c;Z)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_3

    const/4 v1, 0x0

    aget-object v0, v0, v1

    move-object v7, v0

    :cond_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p3, v0}, Lam/r;->a(Ljava/lang/String;I)I

    move-result v1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const v3, 0x7fffffff

    sget-object v4, Lam/r;->a:Lam/g;

    sget-object v5, Lam/r;->b:Lam/c;

    const/4 v6, 0x1

    move-object v0, p2

    move v2, p0

    invoke-static/range {v0 .. v6}, Lam/r;->a(Lam/d;Ljava/lang/String;IILam/g;Lam/c;Z)[Ljava/lang/String;

    move-result-object v0

    :cond_4
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v7, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    array-length v4, v0

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    :goto_3
    array-length v1, v0

    if-le v1, p1, :cond_7

    new-array v1, p1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, p1, -0x1

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, p1, -0x1

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v2, p1, -0x1

    const-string v3, "..."

    invoke-static {v0, p2, p0, v3}, Lam/r;->a(Ljava/lang/String;Lam/d;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    :goto_4
    if-eqz p5, :cond_6

    const/4 v0, 0x0

    :goto_5
    array-length v2, v1

    if-ge v0, v2, :cond_6

    aget-object v2, v1, v0

    invoke-static {v2}, Lam/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v3, -0x1

    sget-object v4, Lam/r;->a:Lam/g;

    sget-object v5, Lam/r;->b:Lam/c;

    const/4 v6, 0x1

    move-object v0, p2

    move-object v1, p3

    move v2, p0

    invoke-static/range {v0 .. v6}, Lam/r;->a(Lam/d;Ljava/lang/String;IILam/g;Lam/c;Z)[Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    :cond_7
    move-object v1, v0

    goto :goto_4
.end method

.method public static a(Lam/d;Ljava/lang/String;IILam/g;Lam/c;Z)[Ljava/lang/String;
    .locals 13

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_1
    if-lez p3, :cond_2

    :goto_1
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-static {p0, p1, v0, v1}, Lam/r;->a(Lam/d;Ljava/lang/String;Lam/g;Lam/c;)[I

    move-result-object v7

    if-nez p6, :cond_3

    const/4 v2, 0x0

    aget v2, v7, v2

    if-gt v2, p2, :cond_3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    goto :goto_0

    :cond_2
    const p3, 0x7fffffff

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-static {p1}, Lam/r;->c(Ljava/lang/String;)I

    move-result v10

    if-nez p6, :cond_4

    if-ltz v10, :cond_b

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p3

    if-gt v2, v0, :cond_b

    :cond_4
    const/4 v3, 0x0

    const/4 v2, 0x2

    :goto_2
    array-length v4, v7

    if-ge v2, v4, :cond_b

    const/4 v4, 0x0

    const/4 v6, 0x0

    move v12, v4

    move v4, v2

    move v2, v12

    :goto_3
    array-length v5, v7

    if-ge v4, v5, :cond_12

    aget v5, v7, v4

    add-int/2addr v5, v2

    if-le v5, p2, :cond_6

    if-eqz p6, :cond_5

    add-int/lit8 v5, v4, -0x2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v11, 0xa

    if-eq v5, v11, :cond_6

    :cond_5
    add-int/lit8 v5, v4, -0x2

    if-ne v5, v3, :cond_12

    :cond_6
    if-eqz p6, :cond_7

    add-int/lit8 v5, v4, -0x2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v11, 0xa

    if-ne v5, v11, :cond_7

    const/4 v2, 0x1

    add-int/lit8 v4, v4, 0x1

    move v5, v2

    move v6, v4

    :goto_4
    add-int/lit8 v4, v6, -0x2

    if-eqz v5, :cond_8

    if-le v4, v3, :cond_8

    add-int/lit8 v2, v4, -0x1

    :goto_5
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_6
    if-ge v4, v9, :cond_9

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lam/r;->a(C)Z

    move-result v11

    if-eqz v11, :cond_9

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_7
    add-int/lit8 v5, v4, 0x1

    aget v4, v7, v4

    add-int/2addr v2, v4

    move v4, v5

    goto :goto_3

    :cond_8
    move v2, v4

    goto :goto_5

    :cond_9
    if-nez v5, :cond_11

    add-int/lit8 v5, v6, -0x2

    if-ne v4, v5, :cond_11

    array-length v5, v7

    if-ge v6, v5, :cond_11

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v2, v5}, Lam/r;->a(Ljava/lang/String;C)I

    move-result v5

    if-ltz v5, :cond_11

    add-int v4, v3, v5

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move v3, v4

    :goto_7
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lam/r;->a(C)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    :cond_a
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x2

    goto/16 :goto_2

    :cond_b
    if-nez p6, :cond_10

    if-ltz v10, :cond_c

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p3

    if-le v2, v0, :cond_10

    :cond_c
    const/4 v3, 0x0

    const/4 v2, 0x2

    invoke-interface {v8}, Ljava/util/List;->clear()V

    :goto_8
    array-length v4, v7

    if-ge v2, v4, :cond_10

    const/4 v4, 0x0

    move v12, v4

    move v4, v2

    move v2, v12

    :goto_9
    array-length v5, v7

    if-ge v4, v5, :cond_e

    aget v5, v7, v4

    add-int/2addr v5, v2

    if-le v5, p2, :cond_d

    add-int/lit8 v5, v4, -0x2

    if-ne v5, v3, :cond_e

    :cond_d
    add-int/lit8 v5, v4, 0x1

    aget v4, v7, v4

    add-int/2addr v2, v4

    move v4, v5

    goto :goto_9

    :cond_e
    add-int/lit8 v2, v4, -0x2

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move v3, v2

    :goto_a
    if-ge v3, v9, :cond_f

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lam/r;->a(C)Z

    move-result v2

    if-eqz v2, :cond_f

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_f
    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x2

    goto :goto_8

    :cond_10
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto/16 :goto_0

    :cond_11
    move v3, v4

    goto :goto_7

    :cond_12
    move v5, v6

    move v6, v4

    goto/16 :goto_4
.end method

.method public static a(Ljava/lang/String;II)[Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    add-int v1, p1, p2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v4, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    :goto_0
    if-ltz v0, :cond_1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v0, 0x1

    if-ge v3, v1, :cond_0

    add-int/lit8 v3, v0, 0x2

    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p1, v0, 0x2

    invoke-virtual {p0, v4, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    goto :goto_0

    :cond_1
    if-ge p1, v1, :cond_2

    if-nez p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v1, v0, :cond_3

    :goto_1
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-object v0

    :cond_3
    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    const/4 v2, -0x1

    const-string v0, " \u2003"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lam/r;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, " \u2003"

    invoke-static {p0, v1, v0}, Lam/r;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lam/d;ILjava/lang/String;)I
    .locals 2

    invoke-interface {p1, p3}, Lam/d;->a(Ljava/lang/String;)I

    move-result v0

    sub-int v0, p2, v0

    invoke-static {p0, p1, v0}, Lam/r;->a(Ljava/lang/String;Lam/d;I)I

    move-result v0

    :goto_0
    if-lez v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lau/b;->a(C)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p2, v1, :cond_1

    invoke-virtual {p0, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_1
    return p2

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    move p2, v0

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v1, v2}, Lam/n;->a(CC)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
