.class public LaW/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/view/View;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LaW/l;->b:I

    iput-object p2, p0, LaW/l;->c:Ljava/lang/String;

    iput-object p3, p0, LaW/l;->d:Ljava/lang/String;

    iput-boolean p4, p0, LaW/l;->e:Z

    iput-boolean p5, p0, LaW/l;->f:Z

    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;ZZLaW/m;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, LaW/l;-><init>(ILjava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    iput p1, p0, LaW/l;->b:I

    iput-object p2, p0, LaW/l;->c:Ljava/lang/String;

    iput-object p3, p0, LaW/l;->d:Ljava/lang/String;

    iput-boolean p4, p0, LaW/l;->e:Z

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LaW/l;->b()V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    const/16 v1, 0x8

    const/4 v0, 0x0

    iget-boolean v2, p0, LaW/l;->e:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LaW/l;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, p0, LaW/l;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, LaW/l;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, LaW/l;->b:I

    if-lez v2, :cond_2

    iget-object v2, p0, LaW/l;->i:Landroid/widget/ImageView;

    iget v3, p0, LaW/l;->b:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, LaW/l;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, LaW/l;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, LaW/l;->g:Landroid/widget/TextView;

    iget-object v3, p0, LaW/l;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, LaW/l;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v2, p0, LaW/l;->d:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LaW/l;->h:Landroid/widget/TextView;

    iget-object v3, p0, LaW/l;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, LaW/l;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    iget-object v2, p0, LaW/l;->j:Landroid/view/View;

    iget-boolean v3, p0, LaW/l;->f:Z

    if-eqz v3, :cond_5

    :goto_4
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, LaW/l;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, LaW/l;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget-object v2, p0, LaW/l;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 3

    const v0, 0x7f040125

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/l;->a:Landroid/view/View;

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    const v1, 0x7f100329

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaW/l;->g:Landroid/widget/TextView;

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    const v1, 0x7f10032a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaW/l;->h:Landroid/widget/TextView;

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    const v1, 0x7f100033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LaW/l;->i:Landroid/widget/ImageView;

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    const v1, 0x7f10001f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/l;->j:Landroid/view/View;

    invoke-direct {p0}, LaW/l;->b()V

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(ILjava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LaW/l;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, LaW/l;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
