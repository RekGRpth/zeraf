.class public LaW/g;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaW/v;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Ljava/lang/String;

.field private final d:LaW/a;

.field private final l:LaW/A;

.field private final m:LaW/x;

.field private n:LaW/y;

.field private o:LaW/s;

.field private final p:LaW/O;

.field private q:Lcom/google/googlenav/ui/wizard/gj;

.field private r:Ljava/lang/String;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:LaW/e;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZZLcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/bu;Lcom/google/googlenav/aV;)V
    .locals 2

    const v0, 0x7f0f001a

    invoke-direct {p0, p9, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-boolean p7, p0, LaW/g;->a:Z

    iput-boolean p8, p0, LaW/g;->b:Z

    iput-object p3, p0, LaW/g;->c:Ljava/lang/String;

    iput-object p2, p0, LaW/g;->r:Ljava/lang/String;

    iput-object p10, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    new-instance v0, LaW/a;

    invoke-direct {v0, p1}, LaW/a;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LaW/g;->d:LaW/a;

    new-instance v0, LaW/x;

    invoke-direct {p0, p11}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-direct {v0, p6, v1}, LaW/x;-><init>(Ljava/util/List;LaW/w;)V

    iput-object v0, p0, LaW/g;->m:LaW/x;

    new-instance v0, LaW/A;

    invoke-direct {v0, p10, p12}, LaW/A;-><init>(Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/aV;)V

    iput-object v0, p0, LaW/g;->l:LaW/A;

    new-instance v0, LaW/y;

    invoke-direct {v0, p4, p5, p10}, LaW/y;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->n:LaW/y;

    new-instance v0, LaW/s;

    invoke-direct {v0, p11, p0}, LaW/s;-><init>(Lcom/google/googlenav/bu;LaW/v;)V

    iput-object v0, p0, LaW/g;->o:LaW/s;

    new-instance v0, LaW/O;

    invoke-direct {v0, p7, p3, p10}, LaW/O;-><init>(ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->p:LaW/O;

    new-instance v0, LaW/e;

    invoke-direct {v0, p10}, LaW/e;-><init>(Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->u:LaW/e;

    return-void
.end method

.method static synthetic a(LaW/g;)Lcom/google/googlenav/ui/wizard/gj;
    .locals 1

    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    const/16 v2, 0x8

    const v0, 0x7f100039

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x38e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10031e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LaW/h;

    invoke-direct {v1, p0}, LaW/h;-><init>(LaW/g;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1001b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f1001b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02021a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v2, LaW/i;

    invoke-direct {v2, p0}, LaW/i;-><init>(LaW/g;)V

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f1001b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(LaW/g;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, LaW/g;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/bu;)LaW/w;
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-boolean v1, p1, Lcom/google/googlenav/bu;->j:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/bu;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, LaW/k;

    const v1, 0x7f020360

    const/16 v2, 0x3a3

    const/16 v3, 0x3a2

    const/16 v4, 0x3a1

    const v5, 0x7f020483

    invoke-direct/range {v0 .. v5}, LaW/k;-><init>(IIIII)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/bu;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, LaW/k;

    const v1, 0x7f02035f

    const/16 v2, 0x3a0

    const/16 v3, 0x39f

    const/16 v4, 0x39e

    const v5, 0x7f020476

    invoke-direct/range {v0 .. v5}, LaW/k;-><init>(IIIII)V

    goto :goto_0
.end method

.method static synthetic c(LaW/g;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, LaW/g;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0}, LaW/y;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v1}, LaW/x;->a()Ljava/util/List;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaW/g;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .locals 2

    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ab_()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    const v0, 0x7f020319

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    const/16 v0, 0x38e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v1, LaW/j;

    invoke-direct {v1, p0}, LaW/j;-><init>(LaW/g;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 1

    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0, p1}, LaW/A;->a(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/bu;)V
    .locals 2

    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-direct {p0, p1}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-virtual {v0, v1}, LaW/x;->a(LaW/w;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, LaW/g;->c:Ljava/lang/String;

    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/g;->p:LaW/O;

    iget-boolean v1, p0, LaW/g;->a:Z

    iget-object v2, p0, LaW/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LaW/O;->a(ZLjava/lang/String;)V

    :cond_0
    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, p2, p3}, LaW/y;->a(Ljava/util/List;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    invoke-direct {p0}, LaW/g;->l()V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    iput-boolean p2, p0, LaW/g;->a:Z

    iget-object v0, p0, LaW/g;->u:LaW/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/g;->u:LaW/e;

    iget-boolean v1, p0, LaW/g;->b:Z

    invoke-virtual {v0, p1, p2, v1}, LaW/e;->a(Ljava/lang/String;ZZ)V

    :cond_0
    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaW/g;->p:LaW/O;

    iget-object v1, p0, LaW/g;->c:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, LaW/O;->a(ZLjava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;ZZ)V
    .locals 2

    const/4 v1, 0x0

    iput-boolean p2, p0, LaW/g;->b:Z

    iput-object p1, p0, LaW/g;->r:Ljava/lang/String;

    iput-boolean p3, p0, LaW/g;->a:Z

    iput-object v1, p0, LaW/g;->c:Ljava/lang/String;

    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, v1, v1}, LaW/y;->a(Ljava/util/List;Ljava/lang/String;)V

    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v0, v1, v1}, LaW/x;->a(Ljava/util/List;LaW/w;)V

    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0}, LaW/A;->b()V

    iget-object v0, p0, LaW/g;->r:Ljava/lang/String;

    invoke-virtual {p0, v0, p3}, LaW/g;->a(Ljava/lang/String;Z)V

    invoke-direct {p0}, LaW/g;->l()V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, LaW/g;->d:LaW/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaW/g;->d:LaW/a;

    iget-object v1, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-virtual {v0, v1, p1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bu;)V
    .locals 2

    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-direct {p0, p2}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LaW/x;->a(Ljava/util/List;LaW/w;)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, LaW/g;->l()V

    :cond_0
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004b7

    if-ne v0, v1, :cond_1

    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->Z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004c1

    if-ne v0, v1, :cond_2

    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/d;->h()V

    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    goto :goto_0

    :cond_2
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004d0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->U_()V

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/bu;)V
    .locals 1

    iget-object v0, p0, LaW/g;->o:LaW/s;

    invoke-virtual {v0, p1}, LaW/s;->a(Lcom/google/googlenav/bu;)V

    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, LaW/g;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f10031d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v0, 0x7f10033c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/g;->s:Landroid/view/View;

    const v0, 0x7f10033b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/g;->t:Landroid/view/View;

    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    const v2, 0x7f100182

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x3a5

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1}, LaW/g;->a(Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->u:LaW/e;

    const v2, 0x7f100319

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LaW/e;->a(Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->r:Ljava/lang/String;

    iget-boolean v2, p0, LaW/g;->a:Z

    invoke-virtual {p0, v0, v2}, LaW/g;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, LaW/g;->d:LaW/a;

    iget-object v2, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-virtual {v0, v2, v1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v0, v1}, LaW/x;->a(Landroid/view/View;)V

    const/16 v0, 0x57

    const-string v2, "rps"

    invoke-static {v0, v2}, Lbm/m;->a(ILjava/lang/String;)V

    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0, v1}, LaW/A;->a(Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, v1}, LaW/y;->a(Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->o:LaW/s;

    invoke-virtual {v0, v1}, LaW/s;->a(Landroid/view/View;)V

    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaW/g;->p:LaW/O;

    invoke-virtual {v0, v1}, LaW/O;->a(Landroid/view/View;)V

    :cond_1
    return-object v1
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .locals 1

    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0}, LaW/A;->a()V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ad_()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ac_()V

    return-void
.end method

.method protected i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1004d0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x21

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f1004c1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x526

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f1004b7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x1d9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f10049c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/16 v0, 0x4f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    iget-object v3, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v3}, Lcom/google/googlenav/ui/wizard/gj;->w()Lcom/google/googlenav/actionbar/b;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x38e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
