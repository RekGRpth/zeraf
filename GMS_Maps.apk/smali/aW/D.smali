.class public LaW/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:LaR/t;

.field public d:Lcom/google/googlenav/ai;

.field final synthetic e:LaW/A;

.field private f:LaR/D;

.field private g:LaR/H;


# direct methods
.method public constructor <init>(LaW/A;Lcom/google/googlenav/ai;)V
    .locals 0

    iput-object p1, p0, LaW/D;->e:LaW/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LaW/D;->d:Lcom/google/googlenav/ai;

    return-void
.end method

.method public constructor <init>(LaW/A;Ljava/lang/String;ZLaR/t;)V
    .locals 0

    iput-object p1, p0, LaW/D;->e:LaW/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LaW/D;->a:Ljava/lang/String;

    iput-boolean p3, p0, LaW/D;->b:Z

    iput-object p4, p0, LaW/D;->c:LaR/t;

    return-void
.end method


# virtual methods
.method public a(LaW/D;)I
    .locals 5

    iget-object v0, p0, LaW/D;->c:LaR/t;

    invoke-virtual {v0}, LaR/t;->i()J

    move-result-wide v0

    iget-object v2, p1, LaW/D;->c:LaR/t;

    invoke-virtual {v2}, LaR/t;->i()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a()LaR/D;
    .locals 2

    iget-object v0, p0, LaW/D;->f:LaR/D;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LaW/D;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->a(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    iput-object v0, p0, LaW/D;->f:LaR/D;

    :cond_0
    :goto_0
    iget-object v0, p0, LaW/D;->f:LaR/D;

    return-object v0

    :cond_1
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->b(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    iput-object v0, p0, LaW/D;->f:LaR/D;

    goto :goto_0
.end method

.method public b()LaR/H;
    .locals 2

    iget-boolean v0, p0, LaW/D;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->a(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    iput-object v0, p0, LaW/D;->g:LaR/H;

    :goto_0
    iget-object v0, p0, LaW/D;->g:LaR/H;

    return-object v0

    :cond_0
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->b(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    iput-object v0, p0, LaW/D;->g:LaR/H;

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LaW/D;

    invoke-virtual {p0, p1}, LaW/D;->a(LaW/D;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, LaW/D;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, LaW/D;

    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    iget-object v1, p1, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaW/D;->d:Lcom/google/googlenav/ai;

    iget-object v1, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
