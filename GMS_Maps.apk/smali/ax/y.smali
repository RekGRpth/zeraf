.class public Lax/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Z

.field private static final b:Lax/y;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:LaN/B;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/googlenav/ai;

.field private final j:Lo/D;

.field private final k:Ljava/lang/Integer;

.field private final l:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    sput-object v0, Lax/y;->b:Lax/y;

    const/4 v0, 0x1

    sput-boolean v0, Lax/y;->a:Z

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lax/y;->c:I

    iput-object p2, p0, Lax/y;->d:Ljava/lang/String;

    iput-object p3, p0, Lax/y;->e:LaN/B;

    iput-object p4, p0, Lax/y;->f:Ljava/lang/String;

    iput-object p5, p0, Lax/y;->g:Ljava/lang/String;

    iput-object p6, p0, Lax/y;->h:Ljava/lang/String;

    iput-object p7, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    if-nez p8, :cond_0

    if-eqz p7, :cond_0

    invoke-virtual {p7}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    move-object p8, v0

    :cond_0
    iput-object p8, p0, Lax/y;->j:Lo/D;

    iput-object p9, p0, Lax/y;->k:Ljava/lang/Integer;

    iput-object p10, p0, Lax/y;->l:Ljava/lang/Integer;

    return-void
.end method

.method public static a()Lax/y;
    .locals 1

    sget-object v0, Lax/y;->b:Lax/y;

    return-object v0
.end method

.method private static a(ILcom/google/googlenav/ai;)Lax/y;
    .locals 11

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    :goto_0
    new-instance v0, Lax/y;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v6

    move v1, p0

    move-object v7, p1

    move-object v9, v8

    move-object v10, v8

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static a(ILcom/google/googlenav/ai;Ljava/lang/Integer;Ljava/lang/Integer;)Lax/y;
    .locals 11

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    :goto_0
    new-instance v0, Lax/y;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    move v1, p0

    move-object v7, p1

    move-object v9, p2

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(LaN/B;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;
    .locals 11

    const/4 v4, 0x0

    new-instance v0, Lax/y;

    const/4 v1, 0x1

    move-object v2, p2

    move-object v3, p0

    move-object v5, p1

    move-object v6, v4

    move-object v7, v4

    move-object v8, p3

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;
    .locals 11

    const/4 v4, 0x0

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v2, v4

    :goto_0
    new-instance v0, Lax/y;

    const/4 v1, 0x1

    move-object v3, p0

    move-object v5, p1

    move-object v6, v4

    move-object v7, v4

    move-object v8, p2

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    move-object v2, p1

    goto :goto_0
.end method

.method public static a(LaN/B;Lo/D;)Lax/y;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lax/y;->a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lax/y;Ljava/lang/String;Ljava/lang/String;)Lax/y;
    .locals 11

    const/4 v9, 0x0

    new-instance v0, Lax/y;

    iget v1, p0, Lax/y;->c:I

    iget-object v2, p0, Lax/y;->d:Ljava/lang/String;

    iget-object v3, p0, Lax/y;->e:LaN/B;

    iget-object v4, p0, Lax/y;->f:Ljava/lang/String;

    iget-object v7, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v8

    move-object v5, p1

    move-object v6, p2

    move-object v10, v9

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lax/y;Lo/D;)Lax/y;
    .locals 11

    const/4 v9, 0x0

    new-instance v0, Lax/y;

    iget v1, p0, Lax/y;->c:I

    iget-object v2, p0, Lax/y;->d:Ljava/lang/String;

    iget-object v3, p0, Lax/y;->e:LaN/B;

    iget-object v4, p0, Lax/y;->f:Ljava/lang/String;

    iget-object v5, p0, Lax/y;->g:Ljava/lang/String;

    iget-object v6, p0, Lax/y;->h:Ljava/lang/String;

    iget-object v7, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    move-object v8, p1

    move-object v10, v9

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;)Lax/y;
    .locals 1

    const/4 v0, 0x7

    invoke-static {v0, p0}, Lax/y;->a(ILcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;II)Lax/y;
    .locals 3

    const/4 v0, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lax/y;->a(ILcom/google/googlenav/ai;Ljava/lang/Integer;Ljava/lang/Integer;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lax/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;
    .locals 11

    const/16 v8, 0x8

    const/4 v7, 0x7

    const/4 v1, 0x3

    const/4 v5, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v3

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v7

    :goto_0
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v8

    :goto_1
    new-instance v0, Lax/y;

    move-object v9, v5

    move-object v10, v5

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    move-object v8, v5

    goto :goto_1

    :cond_1
    move-object v7, v5

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/friend/aI;)Lax/y;
    .locals 11

    const/4 v2, 0x0

    new-instance v0, Lax/y;

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->e()Lo/D;

    move-result-object v8

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lax/y;
    .locals 11

    const/4 v3, 0x0

    new-instance v0, Lax/y;

    const/4 v1, 0x0

    move-object v2, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;LaN/B;)Lax/y;
    .locals 11

    const/4 v4, 0x0

    new-instance v0, Lax/y;

    const/4 v1, 0x5

    move-object v2, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;
    .locals 11

    new-instance v0, Lax/y;

    const/4 v1, 0x3

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v2

    invoke-static {v2}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    invoke-virtual {p0}, Lax/y;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x3

    invoke-virtual {p0}, Lax/y;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v1

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v2

    invoke-virtual {v2}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    const/16 v1, 0x9

    iget v2, p0, Lax/y;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-nez p0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(LaN/B;Lo/D;)Lax/y;
    .locals 11

    const/4 v2, 0x0

    new-instance v0, Lax/y;

    const/4 v1, 0x2

    move-object v3, p0

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, p1

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;)Lax/y;
    .locals 1

    const/4 v0, 0x3

    invoke-static {v0, p0}, Lax/y;->a(ILcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;
    .locals 11

    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v6, 0x0

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-eqz p0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v7, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v3

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v8

    :goto_1
    new-instance v0, Lax/y;

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    move-object v8, v6

    goto :goto_1

    :cond_1
    move-object v7, v6

    goto :goto_0

    :cond_2
    move-object v7, v6

    move-object v5, v6

    goto :goto_0
.end method

.method public static c(Lcom/google/googlenav/ai;)Lax/y;
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0, p0}, Lax/y;->a(ILcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LaN/B;)Lax/y;
    .locals 11

    const/4 v7, 0x0

    new-instance v0, Lax/y;

    iget v1, p0, Lax/y;->c:I

    iget-object v2, p0, Lax/y;->d:Ljava/lang/String;

    iget-object v4, p0, Lax/y;->f:Ljava/lang/String;

    iget-object v5, p0, Lax/y;->g:Ljava/lang/String;

    iget-object v6, p0, Lax/y;->h:Ljava/lang/String;

    iget-object v8, p0, Lax/y;->j:Lo/D;

    move-object v3, p1

    move-object v9, v7

    move-object v10, v7

    invoke-direct/range {v0 .. v10}, Lax/y;-><init>(ILjava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/D;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lax/y;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lax/y;

    iget-object v2, p0, Lax/y;->j:Lo/D;

    iget-object v3, p1, Lax/y;->j:Lo/D;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lax/y;->e:LaN/B;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lax/y;->e:LaN/B;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lax/y;->e:LaN/B;

    iget-object v3, p1, Lax/y;->e:LaN/B;

    invoke-virtual {v2, v3}, LaN/B;->a(LaN/B;)J

    move-result-wide v2

    const-wide/16 v4, 0x19

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lax/y;->d:Ljava/lang/String;

    iget-object v1, p1, Lax/y;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget v2, p0, Lax/y;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lax/y;->g:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, Lax/y;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bM()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-object v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lax/y;->p()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/y;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/y;->u()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lax/y;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lax/y;->c:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/y;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lax/y;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lax/y;

    iget v2, p0, Lax/y;->c:I

    iget v3, p1, Lax/y;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lax/y;->d:Ljava/lang/String;

    iget-object v3, p1, Lax/y;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->e:LaN/B;

    iget-object v3, p1, Lax/y;->e:LaN/B;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->f:Ljava/lang/String;

    iget-object v3, p1, Lax/y;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->g:Ljava/lang/String;

    iget-object v3, p1, Lax/y;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->h:Ljava/lang/String;

    iget-object v3, p1, Lax/y;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    iget-object v3, p1, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lax/y;->j:Lo/D;

    iget-object v3, p1, Lax/y;->j:Lo/D;

    invoke-static {v2, v3}, Lax/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()LaN/B;
    .locals 1

    iget-object v0, p0, Lax/y;->e:LaN/B;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/y;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/y;->g:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lax/y;->c:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->e:LaN/B;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lax/y;->j:Lo/D;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lax/y;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lax/y;->e:LaN/B;

    invoke-virtual {v0}, LaN/B;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lax/y;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lax/y;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lax/y;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lax/y;->j:Lo/D;

    invoke-virtual {v1}, Lo/D;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/y;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Lo/D;
    .locals 1

    iget-object v0, p0, Lax/y;->j:Lo/D;

    return-object v0
.end method

.method public m()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lax/y;->k:Ljava/lang/Integer;

    return-object v0
.end method

.method public n()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lax/y;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public o()Z
    .locals 1

    iget v0, p0, Lax/y;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lax/y;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    iget v0, p0, Lax/y;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 2

    iget v0, p0, Lax/y;->c:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    iget v0, p0, Lax/y;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    iget v0, p0, Lax/y;->c:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Waypoint {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lax/y;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lax/y;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", query=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lax/y;->e:LaN/B;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", coordinates="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->e:LaN/B;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Lax/y;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", fingerprint=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, p0, Lax/y;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, p0, Lax/y;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", streetViewPanoramaId=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v1, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", placemark=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v1, p0, Lax/y;->j:Lo/D;

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", level=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lax/y;->j:Lo/D;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 2

    iget v0, p0, Lax/y;->c:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
