.class public Lax/a;
.super Lax/m;
.source "SourceFile"


# instance fields
.field private b:Ljava/util/Vector;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lax/t;IILaN/B;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v3, 0x4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    iput v7, p0, Lax/a;->c:I

    iput v7, p0, Lax/a;->d:I

    return-void
.end method


# virtual methods
.method public a(I)Lax/m;
    .locals 1

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/m;

    return-object v0
.end method

.method public a(Lax/m;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lax/m;->b(I)V

    :cond_0
    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lax/a;->a(I)Lax/m;

    move-result-object v0

    invoke-virtual {v0, v2}, Lax/m;->b(I)V

    :cond_1
    iput-object p0, p1, Lax/m;->a:Lax/a;

    iget v0, p0, Lax/a;->c:I

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->v()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lax/a;->c:I

    iget v0, p0, Lax/a;->d:I

    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->x()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lax/a;->d:I

    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v0, "AggregateDirectionsFeature"

    const/16 v2, 0x8

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lax/m;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lax/a;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lax/a;->c:I

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lax/a;->d:I

    return v0
.end method
