.class public final enum Lax/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lax/f;

.field public static final enum b:Lax/f;

.field public static final enum c:Lax/f;

.field public static final enum d:Lax/f;

.field private static final synthetic e:[Lax/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lax/f;

    const-string v1, "GREEN"

    invoke-direct {v0, v1, v2}, Lax/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lax/f;->a:Lax/f;

    new-instance v0, Lax/f;

    const-string v1, "YELLOW"

    invoke-direct {v0, v1, v3}, Lax/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lax/f;->b:Lax/f;

    new-instance v0, Lax/f;

    const-string v1, "RED"

    invoke-direct {v0, v1, v4}, Lax/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lax/f;->c:Lax/f;

    new-instance v0, Lax/f;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lax/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lax/f;->d:Lax/f;

    const/4 v0, 0x4

    new-array v0, v0, [Lax/f;

    sget-object v1, Lax/f;->a:Lax/f;

    aput-object v1, v0, v2

    sget-object v1, Lax/f;->b:Lax/f;

    aput-object v1, v0, v3

    sget-object v1, Lax/f;->c:Lax/f;

    aput-object v1, v0, v4

    sget-object v1, Lax/f;->d:Lax/f;

    aput-object v1, v0, v5

    sput-object v0, Lax/f;->e:[Lax/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(II)Lax/f;
    .locals 2

    if-nez p1, :cond_0

    sget-object v0, Lax/f;->d:Lax/f;

    :goto_0
    return-object v0

    :cond_0
    mul-int/lit8 v0, p0, 0x64

    div-int/2addr v0, p1

    sget-object v1, Lax/f;->b:Lax/f;

    invoke-virtual {v1}, Lax/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    sget-object v0, Lax/f;->a:Lax/f;

    goto :goto_0

    :cond_1
    sget-object v1, Lax/f;->c:Lax/f;

    invoke-virtual {v1}, Lax/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    sget-object v0, Lax/f;->b:Lax/f;

    goto :goto_0

    :cond_2
    sget-object v0, Lax/f;->c:Lax/f;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lax/f;
    .locals 1

    const-class v0, Lax/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lax/f;

    return-object v0
.end method

.method public static values()[Lax/f;
    .locals 1

    sget-object v0, Lax/f;->e:[Lax/f;

    invoke-virtual {v0}, [Lax/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lax/f;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-object v0, Lax/c;->a:[I

    invoke-virtual {p0}, Lax/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x28

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
