.class public Lax/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[Lax/t;

.field protected b:Ljava/util/List;

.field final synthetic c:Lax/b;

.field private d:[LaN/B;

.field private e:[I

.field private f:[Lax/m;

.field private g:Ljava/util/List;

.field private h:[LaN/B;

.field private i:LaN/B;

.field private j:LaN/B;

.field private k:I

.field private l:Z

.field private m:I

.field private n:Z

.field private o:I

.field private p:Lax/f;

.field private q:Ljava/lang/String;

.field private r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:I

.field private x:I

.field private y:LaN/B;


# direct methods
.method constructor <init>(Lax/b;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lax/h;->c:Lax/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lax/h;->e:[I

    iput-boolean v1, p0, Lax/h;->t:Z

    return-void
.end method

.method constructor <init>(Lax/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lax/h;->c:Lax/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lax/h;->e:[I

    iput-boolean v1, p0, Lax/h;->t:Z

    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, p2}, Lax/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0, p2}, Lax/h;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lax/h;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lax/h;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->t:Z

    invoke-direct {p0, v0}, Lax/h;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lax/h;->v()V

    invoke-direct {p0, v0}, Lax/h;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Lax/h;)I
    .locals 1

    iget v0, p0, Lax/h;->x:I

    return v0
.end method

.method static synthetic a(Lax/h;I)I
    .locals 0

    iput p1, p0, Lax/h;->o:I

    return p1
.end method

.method static synthetic a(Lax/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 2

    const/4 v1, 0x2

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/h;->o:I

    if-eqz p2, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    iput-boolean v0, p0, Lax/h;->l:Z

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/h;->k:I

    :cond_0
    iget v0, p0, Lax/h;->o:I

    iget v1, p0, Lax/h;->k:I

    invoke-static {v0, v1}, Lax/f;->a(II)Lax/f;

    move-result-object v0

    iput-object v0, p0, Lax/h;->p:Lax/f;

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z
    .locals 11

    const/4 v1, 0x0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v2}, Lax/b;->a(Lax/b;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v3

    invoke-static {v2, v3}, Lax/b;->a(Lax/b;I)I

    :cond_0
    iget-object v3, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v2, p2, 0x1

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v4

    aput-object v4, v3, p2

    invoke-static {v0}, LaN/C;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/Y;

    move-result-object v3

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    if-nez v0, :cond_1

    new-array v0, v1, [B

    :cond_1
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_0
    :try_start_0
    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v5

    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v6

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v2, -0x1

    aget-object v7, v0, v7

    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->a(Lax/b;)I

    move-result v0

    const/4 v8, 0x4

    if-ne v0, v8, :cond_2

    iget-object v8, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v7, v5, v6, v3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v5

    aput-object v5, v8, v2

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v0, v2, 0x1

    new-instance v9, LaN/B;

    invoke-virtual {v7}, LaN/B;->c()I

    move-result v10

    add-int/2addr v5, v10

    invoke-virtual {v7}, LaN/B;->e()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v9, v5, v6}, LaN/B;-><init>(II)V

    aput-object v9, v8, v2
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :goto_2
    return v0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_2
.end method

.method static synthetic b(Lax/h;)I
    .locals 1

    iget v0, p0, Lax/h;->w:I

    return v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lax/h;->v:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->l:Z

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->k:I

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->n:Z

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->m:I

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lax/h;->q:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->o:I

    iget v1, p0, Lax/h;->o:I

    iget v2, p0, Lax/h;->k:I

    invoke-static {v1, v2}, Lax/f;->a(II)Lax/f;

    move-result-object v1

    iput-object v1, p0, Lax/h;->p:Lax/f;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method static synthetic c(Lax/h;)LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->y:LaN/B;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 6

    const/16 v5, 0x1a

    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v3, Lax/g;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lax/g;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method static synthetic d(Lax/h;)LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->i:LaN/B;

    return-object v0
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 20

    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->d:[LaN/B;

    array-length v12, v2

    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v13

    if-nez v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lax/h;->k:I

    move-object/from16 v0, p0

    iget v3, v0, Lax/h;->o:I

    add-int/2addr v3, v2

    new-array v2, v12, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lax/h;->e:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->e:[I

    const/4 v4, 0x0

    aput v3, v2, v4

    new-array v14, v12, [I

    const/4 v4, 0x0

    const/4 v2, 0x1

    :goto_1
    if-ge v2, v12, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v2, -0x1

    aget-object v6, v6, v7

    sget-object v7, Lax/b;->d:LaN/Y;

    invoke-virtual {v5, v6, v7}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v5

    long-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-int v5, v5

    aput v5, v14, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    move v8, v2

    move v10, v3

    :goto_2
    if-ge v8, v13, :cond_0

    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    if-lt v9, v12, :cond_3

    const/4 v2, 0x0

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lax/h;->e:[I

    goto :goto_0

    :cond_3
    if-eqz v9, :cond_6

    const-wide/16 v2, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v9, :cond_4

    add-int/lit8 v6, v5, 0x1

    aget v6, v14, v6

    int-to-long v6, v6

    add-long/2addr v6, v2

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-wide v2, v6

    goto :goto_3

    :cond_4
    const-wide/16 v5, 0x0

    const-wide/16 v15, 0x0

    cmp-long v7, v2, v15

    if-nez v7, :cond_5

    const-wide/16 v2, 0x1

    :cond_5
    :goto_4
    if-ge v4, v9, :cond_7

    add-int/lit8 v7, v4, 0x1

    aget v7, v14, v7

    int-to-long v15, v7

    add-long/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v7, v0, Lax/h;->e:[I

    add-int/lit8 v15, v4, 0x1

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-int v18, v11, v10

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    mul-long v18, v18, v5

    div-long v18, v18, v2

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    aput v16, v7, v15

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->e:[I

    const/4 v3, 0x0

    aput v11, v2, v3

    :cond_7
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v4, v9

    move v10, v11

    goto :goto_2
.end method

.method private e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 7

    const/4 v6, 0x7

    const/4 v0, 0x0

    iget-object v1, p0, Lax/h;->c:Lax/b;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lax/b;->a(Lax/b;I)I

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lax/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-array v1, v2, [LaN/B;

    iput-object v1, p0, Lax/h;->d:[LaN/B;

    move v1, v0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {p0, v4, v1}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v5

    if-nez v5, :cond_1

    :goto_2
    return v0

    :cond_1
    invoke-static {v4}, Lax/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v1, v0

    iput-object v0, p0, Lax/h;->i:LaN/B;

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    iget-object v1, p0, Lax/h;->d:[LaN/B;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lax/h;->j:LaN/B;

    const/4 v0, 0x1

    goto :goto_2
.end method

.method private f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 14

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_1

    const/16 v1, 0xa

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/aY;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x7

    iget-object v3, p0, Lax/h;->c:Lax/b;

    invoke-static {v3}, Lax/b;->b(Lax/b;)Lax/y;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    iget-object v3, p0, Lax/h;->d:[LaN/B;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xa

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-eqz v2, :cond_0

    const/16 v3, 0x19

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    new-array v1, v0, [Lax/t;

    iput-object v1, p0, Lax/h;->a:[Lax/t;

    new-array v1, v0, [LaN/B;

    iput-object v1, p0, Lax/h;->h:[LaN/B;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lax/h;->g:Ljava/util/List;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lax/h;->u:Z

    const/4 v2, 0x0

    const/4 v1, 0x0

    move v13, v1

    move v1, v2

    move v2, v13

    :goto_0
    if-ge v2, v0, :cond_8

    iget-object v3, p0, Lax/h;->a:[Lax/t;

    new-instance v4, Lax/t;

    const/16 v5, 0xa

    invoke-virtual {p1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct {v4, v5}, Lax/t;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v4, v3, v2

    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->B()I

    move-result v3

    iget-object v4, p0, Lax/h;->d:[LaN/B;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-lt v3, v4, :cond_2

    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    iget-object v4, p0, Lax/h;->d:[LaN/B;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lax/t;->c(I)V

    :cond_2
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    iget-object v4, p0, Lax/h;->h:[LaN/B;

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    iget-object v6, p0, Lax/h;->a:[Lax/t;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lax/t;->B()I

    move-result v6

    aget-object v5, v5, v6

    aput-object v5, v4, v2

    invoke-virtual {v3, v5}, Lax/t;->a(LaN/B;)V

    iget-boolean v3, p0, Lax/h;->u:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v3}, Lax/b;->m()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->E()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lax/h;->u:Z

    :cond_3
    const/4 v1, 0x1

    :cond_4
    :goto_1
    if-nez v2, :cond_7

    iget-object v3, p0, Lax/h;->g:Ljava/util/List;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->h()Lo/D;

    move-result-object v3

    iget-object v4, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v5, v2, -0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lax/t;->h()Lo/D;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lax/h;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v2, 0x0

    :goto_3
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    if-ge v2, v0, :cond_d

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v1, v0, v2

    invoke-virtual {v1}, Lax/t;->B()I

    move-result v4

    invoke-virtual {v1}, Lax/t;->C()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v11

    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v11, v0

    goto :goto_3

    :cond_9
    invoke-virtual {v1}, Lax/t;->E()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_b

    new-instance v0, Lax/m;

    const/4 v3, 0x0

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    invoke-virtual {p0}, Lax/h;->u()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v7, :cond_11

    new-instance v5, Lax/a;

    iget-object v3, p0, Lax/h;->d:[LaN/B;

    aget-object v9, v3, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v10

    move-object v6, v1

    move v7, v2

    move v8, v4

    invoke-direct/range {v5 .. v10}, Lax/a;-><init>(Lax/t;IILaN/B;Ljava/lang/String;)V

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    invoke-virtual {v5, v0}, Lax/a;->a(Lax/m;)V

    :goto_6
    move v0, v11

    move-object v7, v5

    goto :goto_4

    :cond_a
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v7

    goto :goto_6

    :cond_b
    invoke-virtual {v1}, Lax/t;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v3, 0x0

    :goto_7
    if-eqz v7, :cond_10

    const/4 v0, 0x3

    if-ne v3, v0, :cond_c

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v7}, Lax/a;->m()Lax/t;

    move-result-object v5

    invoke-virtual {v5, v0}, Lax/t;->a(Ljava/lang/String;)V

    const/4 v8, 0x0

    :goto_9
    new-instance v0, Lax/m;

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v4}, Lax/b;->m()Z

    move-result v4

    if-eqz v4, :cond_f

    const/4 v4, 0x3

    if-eq v3, v4, :cond_f

    if-nez v11, :cond_e

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lax/m;->a(Z)V

    const/4 v7, 0x1

    :goto_a
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v3, v2, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lax/t;->B()I

    move-result v4

    new-instance v0, Lax/m;

    const/4 v3, 0x2

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    iget-object v6, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v9, v2, 0x1

    aget-object v6, v6, v9

    invoke-virtual {v6}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v7

    move-object v7, v8

    goto/16 :goto_4

    :pswitch_1
    const/4 v3, 0x3

    goto :goto_7

    :pswitch_2
    const/4 v3, 0x1

    goto :goto_7

    :cond_c
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    :cond_d
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lax/m;

    invoke-interface {v12, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lax/m;

    iput-object v0, p0, Lax/h;->f:[Lax/m;

    return-void

    :cond_e
    move v7, v11

    goto :goto_a

    :cond_f
    move v0, v11

    move-object v7, v8

    goto/16 :goto_4

    :cond_10
    move-object v8, v7

    goto :goto_9

    :cond_11
    move-object v5, v7

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private j(I)I
    .locals 3

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-static {v1}, Lax/b;->a(Lax/b;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, p1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    iget-object v2, p0, Lax/h;->d:[LaN/B;

    aget-object v2, v2, p1

    invoke-virtual {v2, v1}, LaN/B;->a(LaN/Y;)I

    move-result v2

    invoke-virtual {v0, v1}, LaN/B;->a(LaN/Y;)I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_0
.end method

.method private k(I)I
    .locals 3

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-static {v1}, Lax/b;->a(Lax/b;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, p1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    iget-object v2, p0, Lax/h;->d:[LaN/B;

    aget-object v2, v2, p1

    invoke-virtual {v2, v1}, LaN/B;->b(LaN/Y;)I

    move-result v2

    invoke-virtual {v0, v1}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_0
.end method

.method private l(I)Z
    .locals 1

    const/16 v0, -0x8000

    if-lt p1, v0, :cond_0

    const/16 v0, 0x7fff

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lax/m;
    .locals 1

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/h;->v:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 11

    const/4 v9, 0x7

    const/4 v0, 0x1

    const/4 v4, 0x0

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x9

    invoke-virtual {p1, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lax/h;->v:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x14

    iget-object v2, p0, Lax/h;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-virtual {p0}, Lax/h;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lax/h;->k()I

    move-result v2

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-virtual {p0}, Lax/h;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lax/h;->m()I

    move-result v1

    invoke-virtual {v6, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, Lax/h;->q:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x6

    iget-object v2, p0, Lax/h;->q:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_4
    iget-object v1, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_5

    const/4 v1, 0x4

    iget-object v2, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    iget-object v2, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v3, v1, v4

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move v5, v0

    move-object v0, v1

    move-object v1, v2

    :goto_0
    :try_start_0
    iget-object v2, p0, Lax/h;->d:[LaN/B;

    array-length v2, v2

    if-ge v5, v2, :cond_7

    invoke-direct {p0, v5}, Lax/h;->j(I)I

    move-result v2

    invoke-direct {p0, v5}, Lax/h;->k(I)I

    move-result v7

    invoke-direct {p0, v2}, Lax/h;->l(I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-direct {p0, v7}, Lax/h;->l(I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0, v2}, Ljava/io/DataOutput;->writeShort(I)V

    invoke-interface {v0, v7}, Ljava/io/DataOutput;->writeShort(I)V

    move-object v2, v3

    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_0

    :cond_6
    const/4 v0, 0x7

    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v2}, Lax/b;->a(Lax/b;)I

    move-result v2

    invoke-static {v3, v2}, Lax/b;->a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-static {v2, v7}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_7
    :goto_2
    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->a(Lax/b;)I

    move-result v0

    invoke-static {v3, v0}, Lax/b;->a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v6, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v4

    :goto_3
    iget-object v1, p0, Lax/h;->a:[Lax/t;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Lax/h;->a:[Lax/t;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    return-void

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method a(III)Z
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-ge p3, p2, :cond_2

    if-le p1, p3, :cond_0

    if-lt p1, p2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-gt p1, p3, :cond_3

    if-lt p1, p2, :cond_3

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public b(I)Lax/t;
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lax/h;->t:Z

    return v0
.end method

.method public c(I)LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->g()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lax/h;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    array-length v0, v0

    goto :goto_0
.end method

.method public d(I)Lo/D;
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->h()Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    return v0
.end method

.method public e(I)Z
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)I
    .locals 1

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->B()I

    move-result v0

    return v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lax/h;->g:Ljava/util/List;

    return-object v0
.end method

.method public g(I)I
    .locals 1

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/m;->l()I

    move-result v0

    goto :goto_0
.end method

.method public g()[LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    return-object v0
.end method

.method public h(I)J
    .locals 2

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lax/h;->a:[Lax/t;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lax/t;->h()Lo/D;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public i()LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->i:LaN/B;

    return-object v0
.end method

.method public i(I)Z
    .locals 2

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()LaN/B;
    .locals 1

    iget-object v0, p0, Lax/h;->j:LaN/B;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lax/h;->k:I

    return v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lax/h;->l:Z

    return v0
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lax/h;->m:I

    return v0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lax/h;->n:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lax/h;->m()I

    move-result v0

    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/h;->q:Ljava/lang/String;

    return-object v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, Lax/h;->o:I

    return v0
.end method

.method public r()Lax/f;
    .locals 1

    iget-object v0, p0, Lax/h;->p:Lax/f;

    return-object v0
.end method

.method public s()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public u()Z
    .locals 1

    iget-boolean v0, p0, Lax/h;->u:Z

    return v0
.end method

.method v()V
    .locals 10

    const v9, 0x15752a00

    const/4 v5, 0x0

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    array-length v0, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v4

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v3

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v2

    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    iput v5, p0, Lax/h;->x:I

    iput v5, p0, Lax/h;->w:I

    new-instance v0, LaN/B;

    invoke-direct {v0, v5, v5}, LaN/B;-><init>(II)V

    iput-object v0, p0, Lax/h;->y:LaN/B;

    const/4 v0, 0x1

    :goto_1
    iget-object v5, p0, Lax/h;->d:[LaN/B;

    array-length v5, v5

    if-ge v0, v5, :cond_8

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LaN/B;->e()I

    move-result v5

    iget-object v6, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, LaN/B;->e()I

    move-result v8

    sub-int v6, v5, v8

    if-gez v6, :cond_b

    add-int/2addr v6, v9

    move v7, v6

    :goto_2
    sub-int v6, v8, v5

    if-gez v6, :cond_1

    add-int/2addr v6, v9

    :cond_1
    if-ge v6, v7, :cond_5

    invoke-virtual {p0, v4, v5, v8}, Lax/h;->a(III)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eq v4, v5, :cond_3

    invoke-virtual {p0, v5, v4, v3}, Lax/h;->a(III)Z

    move-result v4

    if-eqz v4, :cond_2

    iput v9, p0, Lax/h;->x:I

    :cond_2
    move v4, v5

    :cond_3
    :goto_3
    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LaN/B;->c()I

    move-result v5

    if-le v5, v2, :cond_7

    move v2, v5

    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v3, v8, v5}, Lax/h;->a(III)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eq v3, v5, :cond_3

    invoke-virtual {p0, v5, v4, v3}, Lax/h;->a(III)Z

    move-result v3

    if-eqz v3, :cond_6

    iput v9, p0, Lax/h;->x:I

    :cond_6
    move v3, v5

    goto :goto_3

    :cond_7
    if-ge v5, v1, :cond_4

    move v1, v5

    goto :goto_4

    :cond_8
    if-lt v3, v4, :cond_9

    sub-int v0, v3, v4

    :goto_5
    iget v3, p0, Lax/h;->x:I

    if-le v0, v3, :cond_a

    :goto_6
    iput v0, p0, Lax/h;->x:I

    sub-int v0, v2, v1

    iput v0, p0, Lax/h;->w:I

    new-instance v0, LaN/B;

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lax/h;->x:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    iput-object v0, p0, Lax/h;->y:LaN/B;

    goto/16 :goto_0

    :cond_9
    sub-int v0, v4, v3

    sub-int v0, v9, v0

    goto :goto_5

    :cond_a
    iget v0, p0, Lax/h;->x:I

    goto :goto_6

    :cond_b
    move v7, v6

    goto :goto_2
.end method

.method public w()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_1
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lax/m;->r()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lax/m;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    check-cast v0, Lax/a;

    move v3, v2

    :goto_2
    invoke-virtual {v0}, Lax/a;->h()I

    move-result v5

    if-ge v3, v5, :cond_2

    invoke-virtual {v0, v3}, Lax/a;->a(I)Lax/m;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lax/m;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lax/m;

    iput-object v0, p0, Lax/h;->f:[Lax/m;

    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0, v2}, Lax/b;->b(Lax/b;I)I

    goto :goto_0
.end method

.method public x()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lax/h;->u()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    iget-object v2, p0, Lax/h;->f:[Lax/m;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lax/h;->f:[Lax/m;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/m;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public y()Z
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lax/h;->a:[Lax/t;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lax/h;->a:[Lax/t;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
