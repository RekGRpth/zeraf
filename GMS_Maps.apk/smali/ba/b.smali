.class public Lba/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Lba/f;I)I
    .locals 2

    aget-object v0, p0, p1

    invoke-virtual {v0}, Lba/f;->b()LaN/B;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lba/f;->c()LaN/B;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lba/f;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lba/f;->c()LaN/B;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lba/a;->a(LaN/B;LaN/B;)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lba/f;->c()LaN/B;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lba/f;->b()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lba/f;->a()LaN/B;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lba/f;->b()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lba/f;->a()LaN/B;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(I[Lba/f;LaN/Y;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    aget-object v0, p1, p0

    invoke-virtual {v0}, Lba/f;->a()LaN/B;

    move-result-object v0

    invoke-static {p1, p0}, Lba/b;->a([Lba/f;I)I

    move-result v1

    invoke-static {v1}, Lba/a;->a(I)I

    move-result v1

    invoke-static {v0, v2, v1, v2, p2}, Lba/b;->a(LaN/B;Ljava/lang/String;ILaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaN/B;LaN/Y;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Lba/b;->a(LaN/B;Ljava/lang/String;ILaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LaN/B;Ljava/lang/String;ILaN/B;LaN/Y;)Ljava/lang/String;
    .locals 5

    invoke-static {p2}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "0"

    const-string v0, "5"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "google.streetview:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ""

    if-eqz p0, :cond_0

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-static {v0}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v3

    invoke-static {v3}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "cbll="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    :cond_0
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "panoid="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, LaN/B;->c()I

    move-result v0

    invoke-static {v0}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, LaN/B;->e()I

    move-result v3

    invoke-static {v3}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "faceto_ll="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&"

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "cbp=1,"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&mz="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;ILaN/Y;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0, p1, v0, p2}, Lba/b;->a(LaN/B;Ljava/lang/String;ILaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;LaN/B;LaN/Y;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, p0, v1, p1, p2}, Lba/b;->a(LaN/B;Ljava/lang/String;ILaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
