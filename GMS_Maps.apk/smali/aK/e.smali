.class public LaK/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected volatile a:Landroid/os/Handler;

.field public b:LaK/i;

.field protected final c:LaK/c;

.field protected d:LaK/b;

.field private e:LaK/d;

.field private final f:LaK/h;

.field private g:Ljava/lang/Thread;

.field private h:Landroid/content/Context;

.field private final i:J

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/Object;

.field private n:Z

.field private o:Z

.field private p:J


# direct methods
.method public constructor <init>(LaK/h;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, LaK/e;->k:Z

    iput-boolean v1, p0, LaK/e;->l:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaK/e;->m:Ljava/lang/Object;

    iput-boolean v1, p0, LaK/e;->n:Z

    iput-boolean v2, p0, LaK/e;->o:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaK/e;->p:J

    iput-object p1, p0, LaK/e;->f:LaK/h;

    invoke-direct {p0}, LaK/e;->k()J

    move-result-wide v0

    iput-wide v0, p0, LaK/e;->i:J

    iput-object p2, p0, LaK/e;->h:Landroid/content/Context;

    new-instance v0, LaK/c;

    invoke-direct {v0}, LaK/c;-><init>()V

    iput-object v0, p0, LaK/e;->c:LaK/c;

    new-instance v0, LaK/b;

    iget-object v1, p0, LaK/e;->f:LaK/h;

    iget-object v2, p0, LaK/e;->c:LaK/c;

    invoke-direct {v0, v1, v2}, LaK/b;-><init>(LaK/h;LaK/c;)V

    iput-object v0, p0, LaK/e;->d:LaK/b;

    new-instance v0, LaK/d;

    invoke-direct {v0}, LaK/d;-><init>()V

    iput-object v0, p0, LaK/e;->e:LaK/d;

    return-void
.end method

.method private a(J)J
    .locals 2

    iget-boolean v0, p0, LaK/e;->j:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, LaK/e;->i:J

    add-long/2addr p1, v0

    :cond_0
    return-wide p1
.end method

.method static synthetic a(LaK/e;)V
    .locals 0

    invoke-direct {p0}, LaK/e;->f()V

    return-void
.end method

.method private b(J)Z
    .locals 4

    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v1}, LaK/c;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, LaK/e;->d(J)Lbw/b;

    move-result-object v0

    :cond_0
    iget-object v1, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v1}, LaK/c;->f()Lo/D;

    move-result-object v1

    invoke-static {v1}, LaK/a;->a(Lo/D;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "wifi"

    invoke-static {p1, p2, v0, v1, v2}, LaK/a;->a(JLbw/b;Landroid/os/Bundle;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, LaK/e;->d:LaK/b;

    invoke-virtual {v1, v0}, LaK/b;->a(Landroid/location/Location;)Z

    return-void
.end method

.method private d(J)Lbw/b;
    .locals 7

    iget-object v0, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v0, p1, p2}, LaK/d;->a(J)Lbw/b;

    move-result-object v0

    invoke-virtual {p0}, LaK/e;->d()J

    move-result-wide v1

    if-eqz v0, :cond_1

    iget v3, v0, Lbw/b;->d:I

    div-int/lit16 v3, v3, 0x3e8

    const/16 v4, 0x28

    if-le v3, v4, :cond_1

    iget-wide v3, p0, LaK/e;->p:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    iget-wide v3, p0, LaK/e;->p:J

    sub-long v3, v1, v3

    const-wide/16 v5, 0x7530

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    :cond_0
    iput-wide v1, p0, LaK/e;->p:J

    invoke-direct {p0}, LaK/e;->g()V

    iget-object v0, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v0, p1, p2}, LaK/d;->a(J)Lbw/b;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaK/e;->o:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->b()V

    invoke-direct {p0}, LaK/e;->j()V

    iget-object v0, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v0}, LaK/d;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/e;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LaK/e;->o:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->b()V

    invoke-direct {p0}, LaK/e;->j()V

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->a()V

    invoke-virtual {p0}, LaK/e;->c()V

    return-void
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, LaK/e;->b:LaK/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/e;->b:LaK/i;

    invoke-virtual {v0}, LaK/i;->a()V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, LaK/e;->b:LaK/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/e;->b:LaK/i;

    invoke-virtual {v0}, LaK/i;->b()V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    iget-object v1, p0, LaK/e;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaK/e;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v0}, LaK/d;->b()V

    invoke-direct {p0}, LaK/e;->i()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/e;->l:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()J
    .locals 6

    invoke-virtual {p0}, LaK/e;->e()J

    move-result-wide v0

    invoke-virtual {p0}, LaK/e;->d()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    sub-long v0, v2, v0

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, LaK/e;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, LaK/e;->g:Ljava/lang/Thread;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, LaK/e;->g:Ljava/lang/Thread;

    iget-object v0, p0, LaK/e;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-enter p0

    :goto_1
    :try_start_4
    iget-object v0, p0, LaK/e;->a:Landroid/os/Handler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-nez v0, :cond_2

    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_7
    iput-boolean v0, p0, LaK/e;->o:Z

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->a()V

    invoke-virtual {p0}, LaK/e;->c()V

    return-void
.end method

.method protected a(Landroid/hardware/SensorEvent;)V
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, LaK/e;->k:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, LaK/e;->i:J

    invoke-virtual {p0}, LaK/e;->e()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long/2addr v0, v2

    const-wide v2, 0x4e94914f0000L

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iput-boolean v5, p0, LaK/e;->j:Z

    :goto_0
    iput-boolean v4, p0, LaK/e;->k:Z

    :cond_0
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-direct {p0, v0, v1}, LaK/e;->a(J)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, LaK/e;->b(J)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iput-boolean v4, p0, LaK/e;->j:Z

    goto :goto_0

    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_2
    invoke-direct {p0, v1, v2}, LaK/e;->c(J)V

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LaK/e;->e:LaK/d;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v5

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    invoke-virtual/range {v0 .. v5}, LaK/d;->a(JFFF)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/location/Location;Lo/D;)V
    .locals 3

    iget-object v0, p0, LaK/e;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    new-instance v2, LaK/g;

    invoke-direct {v2, p1, p2}, LaK/g;-><init>(Landroid/location/Location;Lo/D;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;)Z
    .locals 4

    invoke-virtual {p0}, LaK/e;->d()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1b58

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaK/e;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, LaK/e;->n:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, LaK/e;->n:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected b(Landroid/location/Location;Lo/D;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0, p2}, LaK/c;->a(Lo/D;)V

    if-eqz p2, :cond_3

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaK/e;->d:LaK/b;

    invoke-virtual {v0, p1}, LaK/b;->b(Landroid/location/Location;)Z

    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->c()V

    :cond_2
    invoke-virtual {p0, p1}, LaK/e;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaK/e;->c()V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    const/high16 v2, -0x40800000

    invoke-static {p1, p2, v2}, LaK/a;->a(Landroid/location/Location;Lo/D;F)Lbw/b;

    move-result-object v2

    iget-object v3, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v3, v0, v1, v2}, LaK/d;->a(JLbw/b;)V

    invoke-direct {p0, v0, v1}, LaK/e;->c(J)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LaK/e;->c:LaK/c;

    invoke-virtual {v0}, LaK/c;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, LaK/e;->j()V

    :cond_4
    iget-object v0, p0, LaK/e;->d:LaK/b;

    invoke-virtual {v0, p1}, LaK/b;->b(Landroid/location/Location;)Z

    goto :goto_0
.end method

.method protected c()V
    .locals 2

    iget-object v1, p0, LaK/e;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaK/e;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaK/e;->e:LaK/d;

    invoke-virtual {v0}, LaK/d;->a()V

    invoke-direct {p0}, LaK/e;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaK/e;->l:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected d()J
    .locals 2

    iget-object v0, p0, LaK/e;->f:LaK/h;

    invoke-virtual {v0}, LaK/h;->t()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method protected e()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final run()V
    .locals 4

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, LaK/f;

    invoke-direct {v0, p0}, LaK/f;-><init>(LaK/e;)V

    iput-object v0, p0, LaK/e;->a:Landroid/os/Handler;

    new-instance v0, LaK/i;

    iget-object v1, p0, LaK/e;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    iget-object v3, p0, LaK/e;->h:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3}, LaK/i;-><init>(Landroid/os/Handler;ILandroid/content/Context;)V

    iput-object v0, p0, LaK/e;->b:LaK/i;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
