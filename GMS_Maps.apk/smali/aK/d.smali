.class public LaK/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lbx/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)Lbw/b;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaK/d;->a:Lbx/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/d;->a:Lbx/c;

    invoke-virtual {v0, p1, p2}, Lbx/c;->b(J)Lbw/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, LaK/d;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JFFF)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaK/d;->a:Lbx/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/d;->a:Lbx/c;

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lbx/c;->a(JFFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JLbw/b;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaK/d;->a:Lbx/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaK/d;->a:Lbx/c;

    invoke-virtual {v0, p1, p2, p3}, Lbx/c;->a(JLbw/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaK/d;->a:Lbx/c;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    new-instance v0, Lbx/c;

    const v1, 0x3dcccccd

    const/16 v2, 0x1770

    const/high16 v3, 0x3f400000

    const/16 v4, 0x12c

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lbx/c;-><init>(FIFIZ)V

    iput-object v0, p0, LaK/d;->a:Lbx/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Lbx/c;

    invoke-direct {v0}, Lbx/c;-><init>()V

    iput-object v0, p0, LaK/d;->a:Lbx/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LaK/d;->a:Lbx/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
