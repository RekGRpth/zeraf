.class public Lbe/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private a:Landroid/graphics/drawable/BitmapDrawable;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbe/o;->c:Ljava/lang/String;

    iput-object p2, p0, Lbe/o;->d:Ljava/lang/String;

    iput p3, p0, Lbe/o;->b:I

    iput p4, p0, Lbe/o;->e:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lbe/o;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbe/q;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/q;-><init>(Lbe/p;)V

    const v0, 0x7f1001f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lbe/q;->a(Lbe/q;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    const v0, 0x7f100281

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbe/q;->a(Lbe/q;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    const v0, 0x7f100283

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/q;->a(Lbe/q;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100284

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/q;->b(Lbe/q;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100280

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lbe/q;->a(Lbe/q;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    return-object v1
.end method

.method public a(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    iput-object p1, p0, Lbe/o;->a:Landroid/graphics/drawable/BitmapDrawable;

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 4

    const/4 v3, -0x1

    check-cast p2, Lbe/q;

    iget-object v0, p0, Lbe/o;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    invoke-static {p2}, Lbe/q;->a(Lbe/q;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    invoke-static {p2}, Lbe/q;->b(Lbe/q;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-static {p2}, Lbe/q;->b(Lbe/q;)Landroid/widget/LinearLayout;

    move-result-object v0

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lbe/o;->e:I

    invoke-direct {v1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    invoke-static {p2}, Lbe/q;->d(Lbe/q;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbe/q;->e(Lbe/q;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/o;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-static {p2}, Lbe/q;->a(Lbe/q;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    invoke-static {p2}, Lbe/q;->b(Lbe/q;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-static {p2}, Lbe/q;->c(Lbe/q;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lbe/o;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400d0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
