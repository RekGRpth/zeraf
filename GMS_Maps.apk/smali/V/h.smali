.class public LV/h;
.super LU/b;
.source "SourceFile"


# instance fields
.field private final a:LX/a;

.field private final b:Landroid/content/Context;

.field private final c:LX/b;

.field private final d:Lcom/google/common/collect/di;

.field private final e:LU/T;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/common/collect/di;LX/a;)V
    .locals 2

    const-string v0, "playback_signal_provider"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    new-instance v0, LV/i;

    invoke-direct {v0, p0}, LV/i;-><init>(LV/h;)V

    iput-object v0, p0, LV/h;->f:Ljava/lang/Runnable;

    iput-object p2, p0, LV/h;->b:Landroid/content/Context;

    iput-object p1, p0, LV/h;->e:LU/T;

    iput-object p3, p0, LV/h;->d:Lcom/google/common/collect/di;

    iput-object p4, p0, LV/h;->a:LX/a;

    new-instance v0, LX/b;

    invoke-direct {v0, p4}, LX/b;-><init>(LX/a;)V

    iput-object v0, p0, LV/h;->c:LX/b;

    invoke-virtual {p4}, LX/a;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LV/h;->a(J)V

    return-void
.end method

.method static synthetic a(LV/h;)Lcom/google/common/collect/di;
    .locals 1

    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    return-object v0
.end method

.method private a(J)V
    .locals 2

    :cond_0
    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    invoke-interface {v0}, Lcom/google/common/collect/di;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    invoke-interface {v0}, Lcom/google/common/collect/di;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LV/g;

    iget-wide v0, v0, LV/g;->c:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_0

    :cond_1
    return-void
.end method

.method static synthetic b(LV/h;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, LV/h;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(LV/h;)LX/a;
    .locals 1

    iget-object v0, p0, LV/h;->a:LX/a;

    return-object v0
.end method

.method static synthetic d(LV/h;)LU/T;
    .locals 1

    iget-object v0, p0, LV/h;->e:LU/T;

    return-object v0
.end method

.method static synthetic e(LV/h;)LX/b;
    .locals 1

    iget-object v0, p0, LV/h;->c:LX/b;

    return-object v0
.end method

.method static synthetic f(LV/h;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LV/h;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 2

    invoke-virtual {p0}, LV/h;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, LU/b;->b()V

    iget-object v0, p0, LV/h;->c:LX/b;

    iget-object v1, p0, LV/h;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/b;->b(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    iget-object v1, p0, LV/h;->f:Ljava/lang/Runnable;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LV/h;->c:LX/b;

    iget-object v2, p0, LV/h;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, LX/b;->a(Ljava/lang/Runnable;)V

    invoke-super {p0}, LU/b;->d()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
