.class public Lbk/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/android/aV;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Lcom/google/googlenav/ui/br;

.field private final d:Lcom/google/googlenav/ui/wizard/jv;

.field private final e:Lbk/e;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/view/android/aV;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/br;Lbk/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "parentDialog cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    iput-object p4, p0, Lbk/a;->c:Lcom/google/googlenav/ui/br;

    iput-object p1, p0, Lbk/a;->b:Landroid/view/LayoutInflater;

    iput-object p3, p0, Lbk/a;->d:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p5, p0, Lbk/a;->e:Lbk/e;

    invoke-direct {p0}, Lbk/a;->c()V

    return-void
.end method

.method static synthetic a(Lbk/a;)Lcom/google/googlenav/ui/br;
    .locals 1

    iget-object v0, p0, Lbk/a;->c:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;I)V
    .locals 1

    new-instance v0, Lbk/d;

    invoke-direct {v0, p0, p2, p3}, Lbk/d;-><init>(Lbk/a;Ljava/lang/String;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 3

    iget-object v0, p0, Lbk/a;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400df

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1002a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x3c9

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lbk/c;

    invoke-direct {v0, p0}, Lbk/c;-><init>(Lbk/a;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4

    if-eqz p2, :cond_1

    new-instance v2, Lcom/google/googlenav/ui/bs;

    sget v0, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v2, p2, v0}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbk/a;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400e0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lbk/a;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v1

    check-cast v1, Lan/f;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    iget-object v1, p0, Lbk/a;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-virtual {v1, v2}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v0, p3, v1}, Lbk/a;->a(Landroid/view/View;Ljava/lang/String;I)V

    :cond_0
    invoke-interface {p5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lbk/a;Landroid/view/View;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lbk/a;->a(Landroid/view/View;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic b(Lbk/a;)Lcom/google/googlenav/ui/view/android/aV;
    .locals 1

    iget-object v0, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    return-object v0
.end method

.method static synthetic c(Lbk/a;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lbk/a;->d:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private c()V
    .locals 5

    iget-object v0, p0, Lbk/a;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400de

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbk/a;->f:Landroid/view/View;

    iget-object v0, p0, Lbk/a;->f:Landroid/view/View;

    const v1, 0x7f10029e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aV;->v()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x25f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lbk/a;->b()V

    return-void

    :cond_0
    const/16 v2, 0x260

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lbk/a;->f:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 10

    const/4 v8, 0x0

    iget-object v0, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aV;->v()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->P()[Lcom/google/googlenav/aw;

    move-result-object v9

    iget-object v0, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aV;->v()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->O()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lbk/a;->f:Landroid/view/View;

    const v1, 0x7f10029f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lbk/a;->a(Landroid/view/ViewGroup;)V

    :cond_0
    if-eqz v9, :cond_2

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    iget-object v0, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aV;->v()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move v7, v8

    :goto_0
    array-length v0, v9

    if-ge v7, v0, :cond_1

    aget-object v0, v9, v7

    iget-object v2, v0, Lcom/google/googlenav/aw;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbk/a;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbk/a;->c:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    new-instance v2, Lbk/b;

    invoke-direct {v2, p0, v5, v6, v3}, Lbk/b;-><init>(Lbk/a;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    :cond_2
    iget-object v0, p0, Lbk/a;->e:Lbk/e;

    iget-object v2, p0, Lbk/a;->a:Lcom/google/googlenav/ui/view/android/aV;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aV;->v()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v0, v3, v2, p0}, Lbk/e;->a(Ljava/lang/String;Lcom/google/googlenav/ai;Lbk/a;)V

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method
