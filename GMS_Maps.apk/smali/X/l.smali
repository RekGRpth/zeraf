.class LX/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/Long;

.field private final d:LX/e;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;LX/e;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, LX/l;->a:Landroid/content/Context;

    iput-object p2, p0, LX/l;->b:Ljava/lang/String;

    iput-object p3, p0, LX/l;->c:Ljava/lang/Long;

    iput-object p4, p0, LX/l;->d:LX/e;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;LX/e;LX/k;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, LX/l;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;LX/e;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x7

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, LX/l;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget-object v2, p0, LX/l;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {v1}, Lcom/google/googlenav/clientparam/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 8

    const/16 v7, 0x9

    const/4 v6, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_0

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/l;->a:Landroid/content/Context;

    iget-object v5, p0, LX/l;->b:Ljava/lang/String;

    invoke-static {v4, v3, v5}, LX/r;->a(Landroid/content/Context;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    new-instance v4, LX/q;

    invoke-direct {v4, v3}, LX/q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {v4}, LX/j;->a(LX/q;)LX/q;

    invoke-static {}, LX/j;->d()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sput-boolean v6, LX/j;->a:Z

    iget-object v0, p0, LX/l;->d:LX/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/l;->d:LX/e;

    invoke-interface {v0}, LX/e;->a()V

    :cond_2
    return v6
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
