.class public LX/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/graphics/Point;

.field private static final b:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, LX/g;->a:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, LX/g;->b:Landroid/graphics/Point;

    return-void
.end method

.method public static a(LaN/B;LaN/B;)F
    .locals 2

    invoke-virtual {p0, p1}, LaN/B;->b(LaN/B;)J

    move-result-wide v0

    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(LaN/B;LaN/B;LaN/B;)F
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, LX/g;->a(LaN/B;LaN/B;LaN/B;Z)F

    move-result v0

    return v0
.end method

.method private static a(LaN/B;LaN/B;LaN/B;Z)F
    .locals 4

    invoke-static {p0, p1, p2}, LX/g;->c(LaN/B;LaN/B;LaN/B;)F

    move-result v0

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_1

    const/high16 v1, 0x3f800000

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    :cond_0
    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    sget-object v2, LX/g;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v2

    sget-object v3, LX/g;->b:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    add-int/2addr v0, v2

    new-instance v2, LaN/B;

    invoke-direct {v2, v1, v0}, LaN/B;-><init>(II)V

    invoke-static {v2, p0}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Point;)F
    .locals 2

    invoke-static {p0}, LX/g;->b(Landroid/graphics/Point;)J

    move-result-wide v0

    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(Lbi/d;Lbi/a;Lbi/a;)F
    .locals 7

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    iget-object v1, p2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->a(Lbi/t;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v0}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->y()F

    move-result v0

    iget v1, p2, Lbi/a;->b:F

    iget v2, p1, Lbi/a;->b:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-lez v1, :cond_4

    :goto_1
    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v0}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    invoke-virtual {v0}, Lbi/h;->y()F

    move-result v0

    const/high16 v3, 0x3f800000

    iget v4, p2, Lbi/a;->b:F

    sub-float/2addr v3, v4

    mul-float/2addr v0, v3

    new-instance v3, Lbi/v;

    invoke-direct {v3, p0}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v4, p2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v3, v4}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v3

    :goto_2
    invoke-virtual {v3}, Lbi/v;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lbi/v;->a()Lbi/t;

    move-result-object v4

    iget-object v5, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v4, v5}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    invoke-virtual {v2}, Lbi/h;->y()F

    move-result v2

    iget v3, p1, Lbi/a;->b:F

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    if-lez v1, :cond_0

    neg-float v0, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v4

    invoke-virtual {v4}, Lbi/h;->y()F

    move-result v4

    add-float/2addr v0, v4

    goto :goto_2

    :cond_4
    move-object v6, p2

    move-object p2, p1

    move-object p1, v6

    goto :goto_1
.end method

.method static a(D)I
    .locals 2

    const-wide v0, 0x412e848000000000L

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method

.method static a(Landroid/graphics/Point;Landroid/graphics/Point;)J
    .locals 6

    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-long v0, v0

    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Landroid/graphics/Point;->y:I

    int-to-long v2, v2

    iget v4, p1, Landroid/graphics/Point;->y:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/location/Location;)LaN/B;
    .locals 4

    new-instance v0, LaN/B;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-static {v1, v2}, LX/g;->a(D)I

    move-result v1

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, LX/g;->a(D)I

    move-result v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public static a(Lbi/d;Lbi/a;F)Lbi/a;
    .locals 8

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v0}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->y()F

    move-result v2

    cmpl-float v0, p2, v6

    if-lez v0, :cond_4

    iget v0, p1, Lbi/a;->b:F

    sub-float v0, v5, v0

    mul-float v1, v2, v0

    cmpl-float v0, v1, p2

    if-lez v0, :cond_1

    iget v0, p1, Lbi/a;->b:F

    div-float v1, p2, v2

    add-float/2addr v1, v0

    new-instance v0, Lbi/a;

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-direct {v0, v2, v1}, Lbi/a;-><init>(Lbi/t;F)V

    move-object p1, v0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lbi/v;

    invoke-direct {v0, p0}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v3, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v3}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v3

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    :goto_1
    invoke-virtual {v3}, Lbi/v;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    cmpg-float v4, v1, p2

    if-gez v4, :cond_2

    invoke-virtual {v3}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->y()F

    move-result v2

    add-float/2addr v1, v2

    goto :goto_1

    :cond_2
    cmpg-float v3, v1, p2

    if-gez v3, :cond_3

    new-instance p1, Lbi/a;

    invoke-direct {p1, v0, v5}, Lbi/a;-><init>(Lbi/t;F)V

    goto :goto_0

    :cond_3
    sub-float v1, p2, v1

    add-float/2addr v1, v2

    div-float/2addr v1, v2

    new-instance p1, Lbi/a;

    invoke-direct {p1, v0, v1}, Lbi/a;-><init>(Lbi/t;F)V

    goto :goto_0

    :cond_4
    cmpg-float v0, p2, v6

    if-gez v0, :cond_0

    neg-float v0, v2

    iget v1, p1, Lbi/a;->b:F

    mul-float v3, v0, v1

    cmpg-float v0, v3, p2

    if-gez v0, :cond_5

    iget v0, p1, Lbi/a;->b:F

    div-float v1, p2, v2

    add-float/2addr v1, v0

    new-instance v0, Lbi/a;

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-direct {v0, v2, v1}, Lbi/a;-><init>(Lbi/t;F)V

    move-object p1, v0

    goto :goto_0

    :cond_5
    new-instance v4, Lbi/v;

    invoke-direct {v4, p0}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v4, v0}, Lbi/v;->c(Lbi/t;)Lbi/t;

    move-result-object v0

    move v7, v3

    move v3, v2

    move v2, v7

    :goto_2
    if-eqz v0, :cond_6

    cmpl-float v5, v2, p2

    if-lez v5, :cond_6

    invoke-virtual {v4, v0}, Lbi/v;->c(Lbi/t;)Lbi/t;

    move-result-object v1

    invoke-virtual {p0, v0}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v3

    invoke-virtual {v3}, Lbi/h;->y()F

    move-result v3

    sub-float/2addr v2, v3

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_2

    :cond_6
    cmpl-float v0, v2, p2

    if-lez v0, :cond_7

    new-instance p1, Lbi/a;

    invoke-direct {p1, v1, v6}, Lbi/a;-><init>(Lbi/t;F)V

    goto/16 :goto_0

    :cond_7
    sub-float v0, p2, v2

    div-float/2addr v0, v3

    new-instance p1, Lbi/a;

    invoke-direct {p1, v1, v0}, Lbi/a;-><init>(Lbi/t;F)V

    goto/16 :goto_0
.end method

.method static a(Landroid/graphics/Point;IIII)V
    .locals 7

    const-wide v5, 0x41b5752a00000000L

    sub-int v0, p4, p2

    int-to-double v1, v0

    const-wide v3, -0x3e5a8ad600000000L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    int-to-double v0, v0

    add-double/2addr v0, v5

    double-to-int v0, v0

    :cond_0
    :goto_0
    sub-int v1, p3, p1

    iput v1, p0, Landroid/graphics/Point;->x:I

    iput v0, p0, Landroid/graphics/Point;->y:I

    return-void

    :cond_1
    int-to-double v1, v0

    const-wide v3, 0x41a5752a00000000L

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    int-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-int v0, v0

    goto :goto_0
.end method

.method public static b(LaN/B;LaN/B;LaN/B;)F
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/g;->a(LaN/B;LaN/B;LaN/B;Z)F

    move-result v0

    return v0
.end method

.method private static b(Landroid/graphics/Point;)J
    .locals 2

    invoke-static {p0, p0}, LX/g;->a(Landroid/graphics/Point;Landroid/graphics/Point;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(LaN/B;LaN/B;LaN/B;)F
    .locals 7

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v1

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v2

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v3

    invoke-virtual {p2}, LaN/B;->c()I

    move-result v4

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v5

    sget-object v6, LX/g;->a:Landroid/graphics/Point;

    invoke-static {v6, v2, v3, v0, v1}, LX/g;->a(Landroid/graphics/Point;IIII)V

    sget-object v0, LX/g;->b:Landroid/graphics/Point;

    invoke-static {v0, v2, v3, v4, v5}, LX/g;->a(Landroid/graphics/Point;IIII)V

    sget-object v0, LX/g;->b:Landroid/graphics/Point;

    sget-object v1, LX/g;->a:Landroid/graphics/Point;

    invoke-static {v0, v1}, LX/g;->a(Landroid/graphics/Point;Landroid/graphics/Point;)J

    move-result-wide v0

    long-to-float v0, v0

    sget-object v1, LX/g;->b:Landroid/graphics/Point;

    invoke-static {v1}, LX/g;->b(Landroid/graphics/Point;)J

    move-result-wide v1

    long-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static d(LaN/B;LaN/B;LaN/B;)F
    .locals 5

    sget-object v0, LX/g;->a:Landroid/graphics/Point;

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v3

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/g;->a(Landroid/graphics/Point;IIII)V

    sget-object v0, LX/g;->b:Landroid/graphics/Point;

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    invoke-virtual {p2}, LaN/B;->c()I

    move-result v3

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/g;->a(Landroid/graphics/Point;IIII)V

    sget-object v0, LX/g;->a:Landroid/graphics/Point;

    sget-object v1, LX/g;->b:Landroid/graphics/Point;

    invoke-static {v0, v1}, LX/g;->a(Landroid/graphics/Point;Landroid/graphics/Point;)J

    move-result-wide v0

    long-to-float v0, v0

    sget-object v1, LX/g;->a:Landroid/graphics/Point;

    invoke-static {v1}, LX/g;->a(Landroid/graphics/Point;)F

    move-result v1

    sget-object v2, LX/g;->b:Landroid/graphics/Point;

    invoke-static {v2}, LX/g;->a(Landroid/graphics/Point;)F

    move-result v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method
