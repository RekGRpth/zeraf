.class Lm/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected b:Lm/a;

.field protected c:[I

.field d:I


# direct methods
.method protected constructor <init>(Lm/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm/z;->b:Lm/a;

    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lm/z;->c:[I

    const/4 v0, 0x0

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method protected constructor <init>(Lm/a;[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm/z;->b:Lm/a;

    iput-object p2, p0, Lm/z;->c:[I

    array-length v0, p2

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method protected constructor <init>([D)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lm/r;->a([D)Lm/r;

    move-result-object v0

    iput-object v0, p0, Lm/z;->b:Lm/a;

    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lm/z;->c:[I

    const/4 v0, 0x0

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method private static a([II)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    return v0

    :cond_1
    const/4 v1, 0x1

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ge v1, p1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    aget v2, p0, v1

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method private static a([II[II)Z
    .locals 4

    const/4 v1, -0x1

    const/4 v0, 0x0

    if-ne p1, v1, :cond_0

    array-length p1, p0

    :cond_0
    if-ne p3, v1, :cond_1

    array-length p3, p2

    :cond_1
    if-ne p1, p3, :cond_2

    if-eqz p0, :cond_2

    if-eqz p2, :cond_2

    array-length v1, p0

    if-lt v1, p1, :cond_2

    array-length v1, p2

    if-ge v1, p1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    move v1, v0

    :goto_1
    if-ge v1, p1, :cond_4

    aget v2, p0, v1

    aget v3, p2, v1

    if-ne v2, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private q(I)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lm/z;->d:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lm/z;->c:[I

    array-length v1, v1

    if-ge v1, v0, :cond_0

    iget v0, p0, Lm/z;->d:I

    iget-object v1, p0, Lm/z;->c:[I

    array-length v1, v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x3ff0000000000000L

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    new-array v0, v0, [I

    iget-object v1, p0, Lm/z;->c:[I

    iget-object v2, p0, Lm/z;->c:[I

    array-length v2, v2

    invoke-static {v1, v5, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lm/z;->c:[I

    :cond_0
    return-void
.end method


# virtual methods
.method public a(III)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1, p2, p3}, Lm/a;->a(III)D

    move-result-wide v0

    return-wide v0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method public a(I)V
    .locals 4

    iget-object v0, p0, Lm/z;->c:[I

    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lm/z;->c:[I

    iget v3, p0, Lm/z;->d:I

    sub-int/2addr v3, p1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lm/z;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lm/z;

    return v0
.end method

.method public b(I)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public b(III)D
    .locals 8

    iget-object v0, p0, Lm/z;->b:Lm/a;

    iget-object v1, p0, Lm/z;->c:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lm/a;->a(I)D

    move-result-wide v0

    iget-object v2, p0, Lm/z;->b:Lm/a;

    iget-object v3, p0, Lm/z;->c:[I

    aget v3, v3, p2

    invoke-virtual {v2, v3}, Lm/a;->a(I)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, Lm/z;->b:Lm/a;

    iget-object v3, p0, Lm/z;->c:[I

    aget v3, v3, p3

    invoke-virtual {v2, v3}, Lm/a;->b(I)D

    move-result-wide v2

    iget-object v4, p0, Lm/z;->b:Lm/a;

    iget-object v5, p0, Lm/z;->c:[I

    aget v5, v5, p2

    invoke-virtual {v4, v5}, Lm/a;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-object v2, p0, Lm/z;->b:Lm/a;

    iget-object v3, p0, Lm/z;->c:[I

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Lm/a;->b(I)D

    move-result-wide v2

    iget-object v4, p0, Lm/z;->b:Lm/a;

    iget-object v5, p0, Lm/z;->c:[I

    aget v5, v5, p2

    invoke-virtual {v4, v5}, Lm/a;->b(I)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v4, p0, Lm/z;->b:Lm/a;

    iget-object v5, p0, Lm/z;->c:[I

    aget v5, v5, p3

    invoke-virtual {v4, v5}, Lm/a;->a(I)D

    move-result-wide v4

    iget-object v6, p0, Lm/z;->b:Lm/a;

    iget-object v7, p0, Lm/z;->c:[I

    aget v7, v7, p2

    invoke-virtual {v6, v7}, Lm/a;->a(I)D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    neg-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public b()V
    .locals 1

    iget v0, p0, Lm/z;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method public varargs b([I)V
    .locals 5

    array-length v0, p1

    invoke-direct {p0, v0}, Lm/z;->q(I)V

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    iget-object v3, p0, Lm/z;->c:[I

    iget v4, p0, Lm/z;->d:I

    invoke-virtual {p0, v2}, Lm/z;->o(I)I

    move-result v2

    aput v2, v3, v4

    iget v2, p0, Lm/z;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lm/z;->d:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(I)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public c(III)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1, p2, p3}, Lm/a;->b(III)D

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lm/z;
    .locals 2

    new-instance v0, Lm/z;

    iget-object v1, p0, Lm/z;->b:Lm/a;

    invoke-direct {v0, v1}, Lm/z;-><init>(Lm/a;)V

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0}, Lm/a;->a()I

    move-result v0

    return v0
.end method

.method public d(I)I
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->c(I)I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    iget v0, v0, Lm/a;->a:I

    return v0
.end method

.method public e(I)I
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->d(I)I

    move-result v0

    return v0
.end method

.method public e(II)V
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lm/z;->q(I)V

    iget-object v0, p0, Lm/z;->c:[I

    iget-object v1, p0, Lm/z;->c:[I

    add-int/lit8 v2, p1, 0x1

    iget v3, p0, Lm/z;->d:I

    sub-int/2addr v3, p1

    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lm/z;->c:[I

    invoke-virtual {p0, p2}, Lm/z;->o(I)I

    move-result v1

    aput v1, v0, p1

    iget v0, p0, Lm/z;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lm/z;->d:I

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lm/z;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lm/z;

    invoke-virtual {p1, p0}, Lm/z;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lm/z;->b:Lm/a;

    iget-object v3, p1, Lm/z;->b:Lm/a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lm/z;->c:[I

    iget v3, p0, Lm/z;->d:I

    iget-object v4, p1, Lm/z;->c:[I

    iget v5, p1, Lm/z;->d:I

    invoke-static {v2, v3, v4, v5}, Lm/z;->a([II[II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    iget v1, p0, Lm/z;->d:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v1}, Lm/z;->j(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lm/a;->h(I)I

    move-result v0

    return v0
.end method

.method public f(I)Lm/c;
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->e(I)Lm/c;

    move-result-object v0

    return-object v0
.end method

.method public f(II)V
    .locals 3

    iget-object v0, p0, Lm/z;->c:[I

    invoke-virtual {p0, p1}, Lm/z;->n(I)I

    move-result v1

    invoke-virtual {p0, p2}, Lm/z;->o(I)I

    move-result v2

    aput v2, v0, v1

    return-void
.end method

.method public g(I)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {p0, p1}, Lm/z;->j(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lm/a;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public g(II)I
    .locals 1

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1, p2}, Lm/a;->a(II)I

    move-result v0

    return v0
.end method

.method public h(I)D
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {p0, p1}, Lm/z;->j(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lm/a;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public h(II)I
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->h(I)I

    move-result v0

    :goto_0
    iget v1, p0, Lm/z;->d:I

    if-ge p2, v1, :cond_1

    invoke-virtual {p0, p2}, Lm/z;->j(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_1
    return p2

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, -0x1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lm/z;->c:[I

    iget v2, p0, Lm/z;->d:I

    invoke-static {v1, v2}, Lm/z;->a([II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method

.method public i(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lm/z;->j(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(II)Z
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {p0, p2}, Lm/z;->j(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lm/a;->h(I)I

    move-result v0

    iget-object v1, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v1, p1}, Lm/a;->h(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j(I)I
    .locals 2

    iget-object v0, p0, Lm/z;->c:[I

    invoke-virtual {p0, p1}, Lm/z;->n(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public k(I)I
    .locals 2

    iget-object v0, p0, Lm/z;->b:Lm/a;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lm/z;->j(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lm/a;->h(I)I

    move-result v0

    return v0
.end method

.method public l(I)Z
    .locals 3

    invoke-virtual {p0, p1}, Lm/z;->k(I)I

    move-result v0

    iget-object v1, p0, Lm/z;->b:Lm/a;

    invoke-virtual {p0, p1}, Lm/z;->j(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lm/a;->d(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m(I)Z
    .locals 3

    invoke-virtual {p0, p1}, Lm/z;->k(I)I

    move-result v0

    iget-object v1, p0, Lm/z;->b:Lm/a;

    invoke-virtual {p0, p1}, Lm/z;->j(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lm/a;->c(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n(I)I
    .locals 2

    iget v0, p0, Lm/z;->d:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lm/z;->d:I

    rem-int v0, p1, v0

    if-gez v0, :cond_1

    iget v1, p0, Lm/z;->d:I

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method protected o(I)I
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, p1}, Lm/a;->h(I)I

    move-result v0

    goto :goto_0
.end method

.method p(I)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lm/z;->d:I

    add-int/lit8 v2, v0, -0x1

    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    const-string v0, "[]"

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x5b

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lm/z;->c:[I

    aget v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne v0, v2, :cond_1

    const/16 v0, 0x5d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lm/z;->p(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lm/z;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lm/z;->b:Lm/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
