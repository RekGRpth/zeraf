.class public Lt/K;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt/M;


# static fields
.field protected static final b:Lo/ap;


# instance fields
.field protected final a:LR/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lo/N;

    invoke-direct {v0}, Lo/N;-><init>()V

    sput-object v0, Lt/K;->b:Lo/ap;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LR/h;

    invoke-direct {v0, p1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/K;->a:LR/h;

    return-void
.end method


# virtual methods
.method public a(Lo/aq;Lo/ap;)V
    .locals 3

    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/K;->a:LR/h;

    new-instance v2, Lt/L;

    invoke-direct {v2, p2}, Lt/L;-><init>(Lo/ap;)V

    invoke-virtual {v0, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/K;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lo/ap;)Z
    .locals 1

    sget-object v0, Lt/K;->b:Lo/ap;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Lo/aq;)V
    .locals 1

    sget-object v0, Lt/K;->b:Lo/ap;

    invoke-virtual {p0, p1, v0}, Lt/K;->a(Lo/aq;Lo/ap;)V

    return-void
.end method

.method public b(Lo/aq;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lt/K;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lo/aq;)Lo/ap;
    .locals 3

    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/K;->a:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/L;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lt/L;->a()Lo/ap;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lt/K;->a:LR/h;

    invoke-virtual {v2, p1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
