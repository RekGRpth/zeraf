.class public Lt/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LR/h;

.field private final b:LR/h;

.field private c:Lt/a;

.field private final d:Lcom/google/googlenav/common/a;

.field private e:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lcom/google/googlenav/common/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LR/h;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/u;->a:LR/h;

    new-instance v0, LR/h;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/u;->b:LR/h;

    iput-object p2, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    iput-object p1, p0, Lt/u;->e:Ljava/util/Locale;

    return-void
.end method

.method private a(Lo/r;Z)Lo/y;
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lt/u;->b:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    move-object p1, v0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lt/u;->a:LR/h;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    if-eqz v0, :cond_2

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz p2, :cond_3

    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_4
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {p1}, Lo/r;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lt/a;->a(Ljava/lang/String;)Lt/d;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v2

    if-nez v2, :cond_6

    move-object v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p1, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v2}, Lo/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lt/a;->a(Ljava/lang/String;)Lt/d;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    invoke-virtual {v2, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    :try_start_5
    iget-object v3, p0, Lt/u;->b:LR/h;

    invoke-virtual {v3, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_8
    iget-object v1, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v2, v0, Lt/d;->b:J

    invoke-static {v1, v2, v3}, Lo/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lo/y;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lt/u;->b(Lo/y;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private b(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {p1}, Lo/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lt/a;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbJ/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v3}, Lo/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v2}, Lt/a;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b(Lo/y;)V
    .locals 5

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    iget-object v2, p0, Lt/u;->a:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0, v1, p1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v2, p0, Lt/u;->b:LR/h;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p1}, Lo/y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    iget-object v4, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method


# virtual methods
.method public a(Lo/r;)Lo/y;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lt/u;->a(Lo/r;Z)Lo/y;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/y;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iget-object v4, p0, Lt/u;->c:Lt/a;

    if-eqz v4, :cond_2

    invoke-direct {p0, v1, p2}, Lt/u;->b(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    invoke-static {p2, v2, v3}, Lo/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lo/y;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lt/u;->b(Lo/y;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Ljava/io/File;)V
    .locals 7

    new-instance v0, Lt/a;

    iget-object v1, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    const-string v2, "bd"

    sget-object v3, LbJ/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v4, 0xbb8

    const-wide/32 v5, 0x5265c00

    invoke-direct/range {v0 .. v6}, Lt/a;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;IJ)V

    invoke-virtual {v0, p1}, Lt/a;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lt/u;->e:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lt/a;->a(Ljava/util/Locale;)Z

    iput-object v0, p0, Lt/u;->c:Lt/a;

    :cond_0
    return-void
.end method

.method public a(Lo/y;)Z
    .locals 1

    instance-of v0, p1, Lt/w;

    return v0
.end method

.method public b(Lo/r;)Lo/y;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lt/u;->a(Lo/r;Z)Lo/y;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->b()Z

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public c()V
    .locals 2

    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->a()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public c(Lo/r;)V
    .locals 3

    new-instance v0, Lt/w;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lt/w;-><init>(Lo/r;Lt/v;)V

    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lt/u;->a:LR/h;

    invoke-virtual {v2, p1, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
