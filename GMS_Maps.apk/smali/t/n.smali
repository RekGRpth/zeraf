.class final Lt/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private b:I

.field private final c:Lj/c;

.field private d:I


# direct methods
.method constructor <init>(Lj/c;I[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lt/n;->a:[B

    const/4 v0, 0x0

    iput v0, p0, Lt/n;->b:I

    iput-object p1, p0, Lt/n;->c:Lj/c;

    iput p2, p0, Lt/n;->d:I

    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lt/n;->b:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lt/n;->c:Lj/c;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/n;->c:Lj/c;

    iget v2, p0, Lt/n;->d:I

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lj/c;->a(J)V

    iget-object v0, p0, Lt/n;->c:Lj/c;

    iget-object v2, p0, Lt/n;->a:[B

    const/4 v3, 0x0

    iget v4, p0, Lt/n;->b:I

    invoke-interface {v0, v2, v3, v4}, Lj/c;->b([BII)V

    iget-object v0, p0, Lt/n;->c:Lj/c;

    invoke-interface {v0}, Lj/c;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lt/n;->d:I

    iget v1, p0, Lt/n;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lt/n;->d:I

    iput v5, p0, Lt/n;->b:I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a([B)V
    .locals 4

    array-length v0, p1

    iget v1, p0, Lt/n;->b:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lt/n;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lt/n;->a()V

    :cond_0
    array-length v0, p1

    iget-object v1, p0, Lt/n;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_1

    iget-object v1, p0, Lt/n;->c:Lj/c;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lt/n;->c:Lj/c;

    iget v2, p0, Lt/n;->d:I

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lj/c;->a(J)V

    iget-object v0, p0, Lt/n;->c:Lj/c;

    invoke-interface {v0, p1}, Lj/c;->b([B)V

    iget-object v0, p0, Lt/n;->c:Lj/c;

    invoke-interface {v0}, Lj/c;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lt/n;->d:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lt/n;->d:I

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lt/n;->a:[B

    iget v2, p0, Lt/n;->b:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lt/n;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lt/n;->b:I

    goto :goto_0
.end method

.method b()V
    .locals 0

    invoke-virtual {p0}, Lt/n;->a()V

    return-void
.end method
