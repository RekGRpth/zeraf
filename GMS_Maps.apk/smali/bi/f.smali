.class Lbi/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/collect/P;

.field private b:I

.field private c:I

.field private final d:Lax/b;

.field private e:Lcom/google/googlenav/ui/view/android/rideabout/m;

.field private f:Lcom/google/googlenav/ui/view/android/rideabout/m;

.field private g:I


# direct methods
.method public constructor <init>(Lax/b;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/P;->g()Lcom/google/common/collect/P;

    move-result-object v0

    iput-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    iput v1, p0, Lbi/f;->b:I

    iput v1, p0, Lbi/f;->c:I

    iput-object p1, p0, Lbi/f;->d:Lax/b;

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    iput-object v0, p0, Lbi/f;->f:Lcom/google/googlenav/ui/view/android/rideabout/m;

    invoke-direct {p0}, Lbi/f;->o()V

    invoke-direct {p0}, Lbi/f;->m()V

    return-void
.end method

.method private static a(Lax/t;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lax/t;->q()LaN/B;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lax/t;->k()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lax/t;->Q()[Lax/u;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lax/u;->d()LaN/B;

    move-result-object v4

    if-eqz v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(I)Lax/t;
    .locals 3

    if-ltz p1, :cond_0

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Step at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method private d(I)I
    .locals 1

    invoke-direct {p0, p1}, Lbi/f;->f(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lbi/f;->e(I)I

    move-result v0

    return v0
.end method

.method private e(I)I
    .locals 3

    move v0, p1

    :goto_0
    invoke-virtual {p0, v0}, Lbi/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, p1

    goto :goto_1
.end method

.method private f(I)I
    .locals 2

    const/4 v1, 0x2

    :goto_0
    invoke-virtual {p0, p1}, Lbi/f;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lbi/f;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return p1
.end method

.method private g(I)Z
    .locals 2

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-direct {p0, p1}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()I
    .locals 3

    invoke-virtual {p0}, Lbi/f;->j()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v2

    iget v0, p0, Lbi/f;->c:I

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0, v1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v0

    if-ne v0, v2, :cond_0

    iput v1, p0, Lbi/f;->c:I

    iget v0, p0, Lbi/f;->c:I

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private m()V
    .locals 3

    invoke-virtual {p0}, Lbi/f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lbi/f;->n()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    iget v1, p0, Lbi/f;->b:I

    iget v2, p0, Lbi/f;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lax/b;->a(II)[LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbi/f;->d:Lax/b;

    iget v2, p0, Lbi/f;->b:I

    invoke-virtual {v1, v2}, Lax/b;->n(I)Lax/t;

    move-result-object v1

    invoke-static {v1}, Lbi/f;->a(Lax/t;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v0, v1, v2}, Lbi/x;->a([LaN/B;Lax/t;Lcom/google/common/collect/P;)V

    goto :goto_0
.end method

.method private n()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-virtual {v0}, Lcom/google/common/collect/P;->e()V

    iget v0, p0, Lbi/f;->b:I

    :goto_0
    invoke-direct {p0, v0}, Lbi/f;->g(I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget v1, p0, Lbi/f;->b:I

    if-eq v0, v1, :cond_0

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->q()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lbi/f;->d:Lax/b;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Lax/b;->a(II)[LaN/B;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private o()V
    .locals 2

    invoke-virtual {p0}, Lbi/f;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    :goto_0
    iput-object v0, p0, Lbi/f;->e:Lcom/google/googlenav/ui/view/android/rideabout/m;

    return-void

    :cond_0
    invoke-virtual {p0}, Lbi/f;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_0
.end method


# virtual methods
.method public a()Lax/t;
    .locals 2

    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->d(I)I

    move-result v0

    iget-object v1, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v1}, Lax/b;->ae()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lbi/f;->g(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lax/t;
    .locals 1

    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lbi/f;->g(I)Z

    move-result v0

    return v0
.end method

.method public c()Lax/t;
    .locals 1

    iget v0, p0, Lbi/f;->g:I

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lbi/f;->b:I

    :goto_0
    iget-object v2, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v2}, Lax/b;->ae()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v2

    invoke-virtual {v2}, Lax/t;->E()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v2}, Lbi/f;->g(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public f()I
    .locals 1

    invoke-direct {p0}, Lbi/f;->l()I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    invoke-virtual {p0}, Lbi/f;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public h()V
    .locals 1

    iget v0, p0, Lbi/f;->b:I

    iput v0, p0, Lbi/f;->g:I

    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->d(I)I

    move-result v0

    iput v0, p0, Lbi/f;->b:I

    iget-object v0, p0, Lbi/f;->e:Lcom/google/googlenav/ui/view/android/rideabout/m;

    iput-object v0, p0, Lbi/f;->f:Lcom/google/googlenav/ui/view/android/rideabout/m;

    invoke-direct {p0}, Lbi/f;->o()V

    invoke-direct {p0}, Lbi/f;->m()V

    return-void
.end method

.method public i()Lcom/google/common/collect/P;
    .locals 1

    iget-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget v0, p0, Lbi/f;->b:I

    invoke-virtual {p0, v0}, Lbi/f;->a(I)Z

    move-result v0

    return v0
.end method

.method public k()Lcom/google/googlenav/ui/view/android/rideabout/m;
    .locals 1

    iget-object v0, p0, Lbi/f;->f:Lcom/google/googlenav/ui/view/android/rideabout/m;

    return-object v0
.end method
