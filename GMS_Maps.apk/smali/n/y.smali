.class public Ln/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/d;
.implements Ln/e;


# instance fields
.field private final b:Lr/z;

.field private final c:Lr/n;

.field private final d:Lr/A;

.field private final e:Ljava/util/Map;

.field private final f:LR/h;

.field private final g:Ljava/util/Set;

.field private volatile h:I

.field private volatile i:I

.field private volatile j:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ln/y;->e:Ljava/util/Map;

    new-instance v0, LR/h;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/y;->f:LR/h;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ln/y;->g:Ljava/util/Set;

    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v2, p0, Ln/y;->b:Lr/z;

    iput-object v2, p0, Ln/y;->c:Lr/n;

    iput-object v2, p0, Ln/y;->d:Lr/A;

    :goto_0
    return-void

    :cond_0
    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    iput-object v0, p0, Ln/y;->b:Lr/z;

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    iput-object v0, p0, Ln/y;->c:Lr/n;

    new-instance v0, Ln/z;

    invoke-direct {v0, p0}, Ln/z;-><init>(Ln/y;)V

    iput-object v0, p0, Ln/y;->d:Lr/A;

    iget-object v0, p0, Ln/y;->b:Lr/z;

    iget-object v1, p0, Ln/y;->d:Lr/A;

    invoke-interface {v0, v1}, Lr/z;->a(Lr/A;)V

    goto :goto_0
.end method

.method static synthetic a(Ln/y;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Ln/y;)LR/h;
    .locals 1

    iget-object v0, p0, Ln/y;->f:LR/h;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Ln/y;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/f;

    invoke-interface {v0}, Ln/f;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Ln/y;)V
    .locals 0

    invoke-direct {p0}, Ln/y;->b()V

    return-void
.end method


# virtual methods
.method public a(Lo/aq;)Ljava/util/Collection;
    .locals 5

    const/16 v1, 0xe

    iget v0, p0, Ln/y;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ln/y;->h:I

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    if-le v0, v1, :cond_3

    invoke-virtual {p1, v1}, Lo/aq;->a(I)Lo/aq;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, Ln/y;->f:LR/h;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Ln/y;->f:LR/h;

    invoke-virtual {v0, v1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget v1, p0, Ln/y;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ln/y;->i:I

    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-static {v0, v1}, Ln/a;->a(Ljava/util/Collection;Lo/ae;)Ljava/util/Collection;

    move-result-object v0

    :goto_1
    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    const/4 v2, 0x0

    iget-object v3, p0, Ln/y;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/b;

    if-nez v0, :cond_2

    new-instance v2, Ln/b;

    iget-object v0, p0, Ln/y;->b:Lr/z;

    iget-object v4, p0, Ln/y;->c:Lr/n;

    invoke-direct {v2, v0, v4, v1}, Ln/b;-><init>(Lr/z;Lr/n;Lo/aq;)V

    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    move-object v1, v2

    :goto_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_1

    invoke-virtual {v1, p0}, Ln/b;->a(Ln/d;)V

    iget v0, p0, Ln/y;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ln/y;->j:I

    :cond_1
    sget-object v0, Ln/e;->a:Ljava/util/Collection;

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object v1, v0

    move v0, v2

    goto :goto_2

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Ln/y;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Ln/y;->f:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Ln/y;->f:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Ln/y;->b()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Ln/b;Ljava/util/Collection;)V
    .locals 3

    iget-object v1, p0, Ln/y;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ln/b;->a()Lo/aq;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/b;

    if-eq v0, p1, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ln/y;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ln/b;->a()Lo/aq;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Ln/y;->f:LR/h;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Ln/y;->f:LR/h;

    invoke-virtual {p1}, Ln/b;->a()Lo/aq;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Ln/y;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Ln/f;)V
    .locals 1

    iget-object v0, p0, Ln/y;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lo/o;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
