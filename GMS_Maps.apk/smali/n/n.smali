.class public Ln/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# static fields
.field public static final a:Ln/l;


# instance fields
.field private final b:Lr/z;

.field private final c:Lr/A;

.field private final d:LR/h;

.field private e:I

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ln/l;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1}, Ln/l;-><init>(Ljava/util/List;)V

    sput-object v0, Ln/n;->a:Ln/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/n;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ln/n;->g:Ljava/util/Set;

    sget-object v0, LA/c;->n:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LA/c;->n:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    iput-object v0, p0, Ln/n;->b:Lr/z;

    new-instance v0, LR/h;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/n;->d:LR/h;

    new-instance v0, Ln/o;

    invoke-direct {v0, p0}, Ln/o;-><init>(Ln/n;)V

    iput-object v0, p0, Ln/n;->c:Lr/A;

    iget-object v0, p0, Ln/n;->b:Lr/z;

    iget-object v1, p0, Ln/n;->c:Lr/A;

    invoke-interface {v0, v1}, Lr/z;->a(Lr/A;)V

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Ln/n;->b:Lr/z;

    iput-object v1, p0, Ln/n;->d:LR/h;

    iput-object v1, p0, Ln/n;->c:Lr/A;

    goto :goto_0
.end method

.method static a(Lo/aL;)Ln/l;
    .locals 4

    new-instance v1, Ln/m;

    invoke-direct {v1}, Ln/m;-><init>()V

    invoke-virtual {p0}, Lo/aL;->k()Lo/aO;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    instance-of v3, v0, Lo/f;

    if-eqz v3, :cond_0

    check-cast v0, Lo/f;

    invoke-virtual {v0}, Lo/f;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ln/m;->a(Lo/f;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ln/m;->a()Ln/l;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/aq;Ln/l;)V
    .locals 2

    iget-object v0, p0, Ln/n;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/r;

    invoke-interface {v0, p1, p2}, Ln/r;->a(Lo/aq;Ln/l;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private declared-synchronized b(Lo/aq;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Ln/n;->b:Lr/z;

    invoke-interface {v0, p1, p0}, Lr/z;->a(Lo/aq;Ls/e;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lo/aq;)Ln/l;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Ln/n;->b(Lo/aq;)V

    iget v0, p0, Ln/n;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ln/n;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/Collection;Ln/q;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Ln/p;

    invoke-direct {v0, p0, p1, p2}, Ln/p;-><init>(Ln/n;Ljava/util/Collection;Ln/q;)V

    invoke-virtual {v0}, Ln/p;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ln/r;)V
    .locals 1

    iget-object v0, p0, Ln/n;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lo/aq;ILo/ap;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v1, v0

    :goto_0
    :pswitch_1
    if-eqz v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v2}, Ln/n;->a(Lo/aq;Ln/l;)V

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    return-void

    :pswitch_2
    instance-of v0, p3, Lo/aL;

    if-eqz v0, :cond_2

    check-cast p3, Lo/aL;

    invoke-static {p3}, Ln/n;->a(Lo/aL;)Ln/l;

    move-result-object v0

    :goto_1
    move-object v2, v0

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v0, Ln/n;->a:Ln/l;

    goto :goto_1

    :pswitch_3
    sget-object v0, Ln/n;->a:Ln/l;

    move-object v2, v0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Ln/r;)V
    .locals 1

    iget-object v0, p0, Ln/n;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
