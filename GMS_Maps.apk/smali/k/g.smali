.class public abstract Lk/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lg/a;


# instance fields
.field protected final a:LA/c;

.field protected final b:LA/b;

.field protected final c:Lcom/google/android/maps/driveabout/vector/bF;


# direct methods
.method public constructor <init>(LA/c;LA/b;)V
    .locals 1

    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lk/g;-><init>(LA/c;Lcom/google/android/maps/driveabout/vector/bF;LA/b;)V

    return-void
.end method

.method protected constructor <init>(LA/c;Lcom/google/android/maps/driveabout/vector/bF;LA/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lk/g;->a:LA/c;

    iput-object p2, p0, Lk/g;->c:Lcom/google/android/maps/driveabout/vector/bF;

    iput-object p3, p0, Lk/g;->b:LA/b;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Null zoom table"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lo/T;)F
    .locals 1

    invoke-virtual {p0, p1}, Lk/g;->b(Lo/T;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bE;->a()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public a(ILo/T;)Ljava/util/List;
    .locals 9

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p2}, Lk/g;->b(Lo/T;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v5

    move v3, v1

    :goto_0
    if-gt v3, p1, :cond_3

    invoke-virtual {v5, v3}, Lcom/google/android/maps/driveabout/vector/bE;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    shl-int v6, v0, v3

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_2

    new-instance v7, Lo/aq;

    iget-object v8, p0, Lk/g;->b:LA/b;

    invoke-interface {v8}, LA/b;->a()Lo/aB;

    move-result-object v8

    invoke-direct {v7, v3, v2, v0, v8}, Lo/aq;-><init>(IIILo/aB;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    return-object v4
.end method

.method public a(Lo/aq;Lo/T;)Lo/aq;
    .locals 2

    invoke-virtual {p0, p2}, Lk/g;->b(Lo/T;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bE;->a(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lo/aq;->a(I)Lo/aq;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Lo/T;)Lcom/google/android/maps/driveabout/vector/bE;
    .locals 2

    iget-object v0, p0, Lk/g;->c:Lcom/google/android/maps/driveabout/vector/bF;

    iget-object v1, p0, Lk/g;->a:LA/c;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    return-object v0
.end method

.method public b(Lo/aq;Lo/T;)Ljava/util/List;
    .locals 9

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p2}, Lk/g;->b(Lo/T;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v1

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/bE;->b(I)I

    move-result v4

    if-gez v4, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v1

    sub-int v5, v4, v1

    const/4 v1, 0x1

    shl-int v6, v1, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v7

    shl-int/2addr v7, v5

    add-int/2addr v7, v1

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v8

    shl-int/2addr v8, v5

    add-int/2addr v8, v3

    invoke-virtual {p1, v4, v7, v8}, Lo/aq;->a(III)Lo/aq;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0
.end method
