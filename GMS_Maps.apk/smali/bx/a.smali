.class public Lbx/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:D

.field public g:F

.field public h:F

.field public i:[Lbx/b;

.field private j:Ljava/util/Random;

.field private k:F


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12c

    iput v0, p0, Lbx/a;->a:I

    iput v2, p0, Lbx/a;->b:F

    iput v2, p0, Lbx/a;->c:F

    iput v2, p0, Lbx/a;->d:F

    iput v2, p0, Lbx/a;->e:F

    const-wide/high16 v0, 0x3fe0000000000000L

    iput-wide v0, p0, Lbx/a;->f:D

    iput v2, p0, Lbx/a;->g:F

    iput v2, p0, Lbx/a;->h:F

    new-instance v0, Ljava/util/Random;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lbx/a;->j:Ljava/util/Random;

    const v0, 0x3f666666

    iput v0, p0, Lbx/a;->k:F

    invoke-direct {p0}, Lbx/a;->c()V

    return-void
.end method

.method public constructor <init>(FFFFDI)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12c

    iput v0, p0, Lbx/a;->a:I

    iput v2, p0, Lbx/a;->b:F

    iput v2, p0, Lbx/a;->c:F

    iput v2, p0, Lbx/a;->d:F

    iput v2, p0, Lbx/a;->e:F

    const-wide/high16 v0, 0x3fe0000000000000L

    iput-wide v0, p0, Lbx/a;->f:D

    iput v2, p0, Lbx/a;->g:F

    iput v2, p0, Lbx/a;->h:F

    new-instance v0, Ljava/util/Random;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lbx/a;->j:Ljava/util/Random;

    const v0, 0x3f666666

    iput v0, p0, Lbx/a;->k:F

    iput p1, p0, Lbx/a;->b:F

    iput p2, p0, Lbx/a;->c:F

    iput p3, p0, Lbx/a;->d:F

    iput p4, p0, Lbx/a;->e:F

    iput-wide p5, p0, Lbx/a;->f:D

    iput p7, p0, Lbx/a;->a:I

    invoke-direct {p0}, Lbx/a;->c()V

    return-void
.end method

.method private a(Lbx/b;I)V
    .locals 3

    invoke-direct {p0, p1}, Lbx/a;->b(Lbx/b;)Lbx/b;

    move-result-object v0

    iget-object v1, p0, Lbx/a;->i:[Lbx/b;

    aget-object v1, v1, p2

    iget v2, v0, Lbx/b;->a:F

    iput v2, v1, Lbx/b;->a:F

    iget-object v1, p0, Lbx/a;->i:[Lbx/b;

    aget-object v1, v1, p2

    iget v2, v0, Lbx/b;->b:F

    iput v2, v1, Lbx/b;->b:F

    iget-object v1, p0, Lbx/a;->i:[Lbx/b;

    aget-object v1, v1, p2

    iget v0, v0, Lbx/b;->d:F

    iput v0, v1, Lbx/b;->d:F

    return-void
.end method

.method private b(Lbx/b;)Lbx/b;
    .locals 10

    const-wide v8, 0x401921fb54442d18L

    const-wide/high16 v6, 0x3fe0000000000000L

    iget v0, p0, Lbx/a;->g:F

    const/high16 v1, 0x3f800000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    iget v0, p1, Lbx/b;->a:F

    iget-object v1, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v1

    sub-double/2addr v1, v6

    iget-wide v3, p0, Lbx/a;->f:D

    mul-double/2addr v1, v3

    double-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Lbx/b;->a:F

    iget v0, p1, Lbx/b;->b:F

    iget-object v1, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v1

    sub-double/2addr v1, v6

    iget-wide v3, p0, Lbx/a;->f:D

    mul-double/2addr v1, v3

    double-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Lbx/b;->b:F

    iget v0, p1, Lbx/b;->d:F

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L

    iget-object v4, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p1, Lbx/b;->d:F

    :goto_0
    iget v0, p1, Lbx/b;->d:F

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p1, Lbx/b;->d:F

    float-to-double v0, v0

    sub-double/2addr v0, v8

    double-to-float v0, v0

    iput v0, p1, Lbx/b;->d:F

    goto :goto_0

    :cond_0
    :goto_1
    iget v0, p1, Lbx/b;->d:F

    float-to-double v0, v0

    const-wide v2, -0x3ff6de04abbbd2e8L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    iget v0, p1, Lbx/b;->d:F

    float-to-double v0, v0

    add-double/2addr v0, v8

    double-to-float v0, v0

    iput v0, p1, Lbx/b;->d:F

    goto :goto_1

    :cond_1
    return-object p1
.end method

.method private c()V
    .locals 8

    const-wide v6, 0x400921fb54442d18L

    const/4 v0, 0x0

    iput-object v0, p0, Lbx/a;->i:[Lbx/b;

    iget v0, p0, Lbx/a;->a:I

    new-array v0, v0, [Lbx/b;

    iput-object v0, p0, Lbx/a;->i:[Lbx/b;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lbx/a;->a:I

    if-ge v0, v1, :cond_0

    new-instance v1, Lbx/b;

    invoke-direct {v1}, Lbx/b;-><init>()V

    iget-object v2, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    iget v4, p0, Lbx/a;->c:F

    iget v5, p0, Lbx/a;->b:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p0, Lbx/a;->b:F

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lbx/b;->a:F

    iget-object v2, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    iget v4, p0, Lbx/a;->e:F

    iget v5, p0, Lbx/a;->d:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p0, Lbx/a;->d:F

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lbx/b;->b:F

    iget-object v2, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L

    mul-double/2addr v2, v4

    mul-double/2addr v2, v6

    sub-double/2addr v2, v6

    double-to-float v2, v2

    iput v2, v1, Lbx/b;->d:F

    iget-object v2, p0, Lbx/a;->i:[Lbx/b;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private d()V
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lbx/a;->i:[Lbx/b;

    array-length v4, v2

    move v2, v0

    move v3, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v5, p0, Lbx/a;->i:[Lbx/b;

    aget-object v5, v5, v2

    iget v5, v5, Lbx/b;->e:F

    add-float/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput v3, p0, Lbx/a;->g:F

    cmpl-float v2, v3, v1

    if-lez v2, :cond_1

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v5, p0, Lbx/a;->i:[Lbx/b;

    aget-object v5, v5, v2

    iget v6, v5, Lbx/b;->e:F

    div-float/2addr v6, v3

    iput v6, v5, Lbx/b;->f:F

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_2

    iget-object v3, p0, Lbx/a;->i:[Lbx/b;

    aget-object v3, v3, v2

    const/high16 v5, 0x3f800000

    iget v6, p0, Lbx/a;->a:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    iput v5, v3, Lbx/b;->f:F

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    :goto_3
    if-ge v0, v4, :cond_3

    iget-object v2, p0, Lbx/a;->i:[Lbx/b;

    aget-object v2, v2, v0

    iget v3, v2, Lbx/b;->f:F

    add-float/2addr v3, v1

    iput v3, v2, Lbx/b;->g:F

    iget v2, v2, Lbx/b;->f:F

    add-float/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lbx/a;->i:[Lbx/b;

    array-length v1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lbx/a;->i:[Lbx/b;

    aget-object v2, v2, v0

    const/4 v3, 0x0

    iput v3, v2, Lbx/b;->e:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbx/b;)I
    .locals 10

    const/16 v0, 0xbb8

    const-wide/16 v2, 0x0

    iget-object v1, p0, Lbx/a;->i:[Lbx/b;

    array-length v4, v1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v5, p0, Lbx/a;->i:[Lbx/b;

    aget-object v5, v5, v1

    iget v6, p1, Lbx/b;->a:F

    iget v7, v5, Lbx/b;->a:F

    sub-float/2addr v6, v7

    iget v7, p1, Lbx/b;->a:F

    iget v8, v5, Lbx/b;->a:F

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget v7, p1, Lbx/b;->b:F

    iget v8, v5, Lbx/b;->b:F

    sub-float/2addr v7, v8

    iget v8, p1, Lbx/b;->b:F

    iget v9, v5, Lbx/b;->b:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    float-to-double v6, v6

    iget v5, v5, Lbx/b;->f:F

    float-to-double v8, v5

    mul-double v5, v8, v6

    add-double/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    const-wide/high16 v3, 0x4000000000000000L

    mul-double/2addr v1, v3

    const-wide v3, 0x408f400000000000L

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    if-ge v1, v0, :cond_1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a()Lbx/b;
    .locals 1

    iget v0, p0, Lbx/a;->k:F

    invoke-virtual {p0, v0}, Lbx/a;->a(F)Lbx/b;

    move-result-object v0

    return-object v0
.end method

.method public a(F)Lbx/b;
    .locals 8

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    cmpg-float v1, p1, v7

    if-gez v1, :cond_1

    :cond_0
    return-object v0

    :cond_1
    const/high16 v1, 0x3f800000

    cmpl-float v1, p1, v1

    if-gtz v1, :cond_0

    invoke-direct {p0}, Lbx/a;->d()V

    iget v0, p0, Lbx/a;->a:I

    new-array v2, v0, [Lbx/b;

    iget-object v0, p0, Lbx/a;->i:[Lbx/b;

    iget v1, p0, Lbx/a;->a:I

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    new-instance v0, Lbx/b;

    invoke-direct {v0}, Lbx/b;-><init>()V

    iget v1, p0, Lbx/a;->a:I

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    aget-object v3, v2, v1

    iget v4, v3, Lbx/b;->f:F

    add-float/2addr v4, v7

    cmpg-float v4, v4, p1

    if-gtz v4, :cond_0

    iget v4, v0, Lbx/b;->a:F

    iget v5, v3, Lbx/b;->a:F

    iget v6, v3, Lbx/b;->f:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, v0, Lbx/b;->a:F

    iget v4, v0, Lbx/b;->b:F

    iget v5, v3, Lbx/b;->b:F

    iget v6, v3, Lbx/b;->f:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, v0, Lbx/b;->b:F

    iget v4, v0, Lbx/b;->d:F

    iget v5, v3, Lbx/b;->d:F

    iget v3, v3, Lbx/b;->f:F

    mul-float/2addr v3, v5

    add-float/2addr v3, v4

    iput v3, v0, Lbx/b;->d:F

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public b()V
    .locals 12

    const/4 v1, 0x0

    invoke-direct {p0}, Lbx/a;->d()V

    iget v0, p0, Lbx/a;->a:I

    new-array v7, v0, [Lbx/b;

    iget-object v0, p0, Lbx/a;->i:[Lbx/b;

    iget v2, p0, Lbx/a;->a:I

    invoke-static {v0, v1, v7, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    double-to-float v0, v2

    const/high16 v2, 0x41200000

    mul-float/2addr v0, v2

    iget v2, p0, Lbx/a;->a:I

    int-to-float v2, v2

    div-float v5, v0, v2

    const/high16 v0, 0x3f800000

    iget v2, p0, Lbx/a;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v5

    :goto_0
    iget v6, p0, Lbx/a;->a:I

    if-ge v0, v6, :cond_2

    move v6, v2

    :goto_1
    iget v8, p0, Lbx/a;->a:I

    if-ge v6, v8, :cond_4

    add-int/lit8 v8, v6, 0x1

    iget v9, p0, Lbx/a;->a:I

    if-ne v8, v9, :cond_0

    :goto_2
    aget-object v3, v7, v6

    invoke-direct {p0, v3, v0}, Lbx/a;->a(Lbx/b;I)V

    add-float/2addr v4, v5

    float-to-double v8, v4

    const-wide/high16 v10, 0x3ff0000000000000L

    cmpl-double v3, v8, v10

    if-ltz v3, :cond_3

    const/4 v4, 0x0

    move v2, v1

    move v3, v1

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lbx/a;->i:[Lbx/b;

    aget-object v8, v8, v6

    iget v8, v8, Lbx/b;->g:F

    cmpl-float v8, v4, v8

    if-lez v8, :cond_1

    iget-object v8, p0, Lbx/a;->i:[Lbx/b;

    add-int/lit8 v9, v6, 0x1

    aget-object v8, v8, v9

    iget v8, v8, Lbx/b;->g:F

    cmpg-float v8, v4, v8

    if-gtz v8, :cond_1

    add-int/lit8 v3, v6, 0x1

    add-int/lit8 v2, v6, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v6, v3

    goto :goto_2

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lbx/a;->e()V

    return-void

    :cond_3
    move v3, v6

    goto :goto_3

    :cond_4
    move v6, v3

    goto :goto_2
.end method

.method public b(F)V
    .locals 11

    const-wide/high16 v9, 0x3fe0000000000000L

    iget-object v0, p0, Lbx/a;->i:[Lbx/b;

    array-length v1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lbx/a;->i:[Lbx/b;

    aget-object v2, v2, v0

    iget v3, v2, Lbx/b;->d:F

    float-to-double v4, p1

    neg-float v6, v3

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iget-object v6, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextDouble()D

    move-result-wide v6

    sub-double/2addr v6, v9

    mul-double/2addr v6, v9

    add-double/2addr v4, v6

    double-to-float v4, v4

    float-to-double v5, p1

    neg-float v3, v3

    float-to-double v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    iget-object v3, p0, Lbx/a;->j:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v7

    sub-double/2addr v7, v9

    mul-double/2addr v7, v9

    add-double/2addr v5, v7

    double-to-float v3, v5

    iget v5, v2, Lbx/b;->a:F

    add-float/2addr v4, v5

    iput v4, v2, Lbx/b;->a:F

    iget v4, v2, Lbx/b;->b:F

    add-float/2addr v3, v4

    iput v3, v2, Lbx/b;->b:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lbx/a;->i:[Lbx/b;

    array-length v2, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lbx/a;->i:[Lbx/b;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lbx/b;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
