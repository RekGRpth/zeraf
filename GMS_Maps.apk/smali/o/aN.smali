.class public Lo/aN;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lo/aq;

.field private b:I

.field private c:B

.field private d:I

.field private e:[Lo/n;

.field private f:Lo/al;

.field private g:[Ljava/lang/String;

.field private h:J

.field private i:[Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:I

.field private l:LA/c;

.field private m:[Lo/aI;

.field private n:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lo/aN;->d:I

    iput-wide v1, p0, Lo/aN;->h:J

    iput v0, p0, Lo/aN;->k:I

    sget-object v0, LA/c;->a:LA/c;

    iput-object v0, p0, Lo/aN;->l:LA/c;

    iput-wide v1, p0, Lo/aN;->n:J

    return-void
.end method


# virtual methods
.method public a()Lo/aL;
    .locals 19

    new-instance v2, Lo/aL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lo/aN;->f:Lo/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lo/aN;->g:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lo/aN;->a:Lo/aq;

    move-object/from16 v0, p0

    iget v6, v0, Lo/aN;->b:I

    move-object/from16 v0, p0

    iget-byte v7, v0, Lo/aN;->c:B

    move-object/from16 v0, p0

    iget v8, v0, Lo/aN;->d:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lo/aN;->i:[Ljava/lang/String;

    if-nez v9, :cond_0

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lo/aN;->j:[Ljava/lang/String;

    if-nez v10, :cond_1

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lo/aN;->k:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lo/aN;->e:[Lo/n;

    if-nez v12, :cond_2

    const/4 v12, 0x0

    new-array v12, v12, [Lo/n;

    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lo/aN;->l:LA/c;

    move-object/from16 v0, p0

    iget-object v14, v0, Lo/aN;->m:[Lo/aI;

    move-object/from16 v0, p0

    iget-wide v15, v0, Lo/aN;->h:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lo/aN;->n:J

    move-wide/from16 v17, v0

    invoke-direct/range {v2 .. v18}, Lo/aL;-><init>(Lo/al;[Ljava/lang/String;Lo/aq;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lo/n;LA/c;[Lo/aI;JJ)V

    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lo/aN;->i:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lo/aN;->j:[Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lo/aN;->e:[Lo/n;

    goto :goto_2
.end method

.method public a(I)Lo/aN;
    .locals 0

    iput p1, p0, Lo/aN;->d:I

    return-object p0
.end method

.method public a(J)Lo/aN;
    .locals 0

    iput-wide p1, p0, Lo/aN;->h:J

    return-object p0
.end method

.method public a(LA/c;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->l:LA/c;

    return-object p0
.end method

.method public a(Lo/al;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->f:Lo/al;

    return-object p0
.end method

.method public a(Lo/aq;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->a:Lo/aq;

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->i:[Ljava/lang/String;

    return-object p0
.end method

.method public a([Lo/n;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->e:[Lo/n;

    return-object p0
.end method

.method public b(I)Lo/aN;
    .locals 0

    iput p1, p0, Lo/aN;->b:I

    return-object p0
.end method

.method public b(J)Lo/aN;
    .locals 0

    iput-wide p1, p0, Lo/aN;->n:J

    return-object p0
.end method

.method public b([Ljava/lang/String;)Lo/aN;
    .locals 0

    iput-object p1, p0, Lo/aN;->j:[Ljava/lang/String;

    return-object p0
.end method

.method public c(I)Lo/aN;
    .locals 0

    iput p1, p0, Lo/aN;->k:I

    return-object p0
.end method
