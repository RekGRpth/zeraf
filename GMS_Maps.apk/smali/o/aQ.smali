.class public final Lo/aQ;
.super Lo/aS;
.source "SourceFile"


# instance fields
.field private final b:[Lo/T;

.field private final c:[Lo/T;

.field private final d:Lo/t;

.field private final e:Lo/aR;

.field private final f:Lo/ad;

.field private g:[[Lo/T;


# direct methods
.method private constructor <init>([Lo/T;)V
    .locals 3

    invoke-direct {p0}, Lo/aS;-><init>()V

    array-length v0, p1

    new-array v0, v0, [Lo/T;

    iput-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    aput-object v2, v1, v0

    aget-object v1, p1, v0

    iget-object v2, p0, Lo/aQ;->c:[Lo/T;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lo/T;->i(Lo/T;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lo/aQ;->b:[Lo/T;

    new-instance v0, Lo/t;

    invoke-direct {v0, p1}, Lo/t;-><init>([Lo/T;)V

    iput-object v0, p0, Lo/aQ;->d:Lo/t;

    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    invoke-virtual {v0}, Lo/t;->a()Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lo/aQ;->f:Lo/ad;

    iget-object v0, p0, Lo/aQ;->f:Lo/ad;

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    iput-object v0, p0, Lo/aQ;->e:Lo/aR;

    iget-object v0, p0, Lo/aQ;->e:Lo/aR;

    iget-boolean v0, v0, Lo/aR;->a:Z

    iput-boolean v0, p0, Lo/aQ;->a:Z

    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lo/aQ;->j()V

    :cond_1
    return-void
.end method

.method public static a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [Lo/T;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    new-instance v1, Lo/aQ;

    invoke-direct {v1, v0}, Lo/aQ;-><init>([Lo/T;)V

    return-object v1
.end method

.method private a(Lo/T;Lo/T;I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v1, 0x20000001

    const v2, -0x20000001

    iget v0, p2, Lo/T;->a:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {p1, p2, v0}, Lo/aQ;->b(Lo/T;Lo/T;I)I

    move-result v0

    iget v3, p1, Lo/T;->a:I

    iget v4, p2, Lo/T;->a:I

    if-le v3, v4, :cond_1

    iget-object v3, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lo/T;

    invoke-direct {v4, v2, v0}, Lo/T;-><init>(II)V

    aput-object v4, v3, v6

    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v2, v2, p3

    new-instance v3, Lo/T;

    invoke-direct {v3, v1, v0}, Lo/T;-><init>(II)V

    aput-object v3, v2, v5

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lo/T;

    invoke-direct {v4, v1, v0}, Lo/T;-><init>(II)V

    aput-object v4, v3, v6

    iget-object v1, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v1, v1, p3

    new-instance v3, Lo/T;

    invoke-direct {v3, v2, v0}, Lo/T;-><init>(II)V

    aput-object v3, v1, v5

    goto :goto_1
.end method

.method private static b(Lo/T;Lo/T;I)I
    .locals 4

    iget v0, p0, Lo/T;->a:I

    sub-int v0, p2, v0

    int-to-double v0, v0

    iget v2, p1, Lo/T;->a:I

    iget v3, p0, Lo/T;->a:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p1, Lo/T;->b:I

    iget v3, p0, Lo/T;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lo/T;->b:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private j()V
    .locals 9

    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    filled-new-array {v8, v0}, [I

    move-result-object v0

    const-class v1, Lo/T;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lo/T;

    iput-object v0, p0, Lo/aQ;->g:[[Lo/T;

    move v5, v4

    move v0, v4

    move v2, v4

    :goto_0
    const/4 v1, 0x4

    if-ge v5, v1, :cond_3

    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    aget-object v1, v1, v5

    iget-object v6, p0, Lo/aQ;->b:[Lo/T;

    aget-object v6, v6, v5

    invoke-virtual {v1, v6}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v3

    :goto_1
    if-eq v1, v2, :cond_5

    if-lez v5, :cond_0

    if-ge v0, v7, :cond_0

    iget-object v2, p0, Lo/aQ;->b:[Lo/T;

    add-int/lit8 v6, v5, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lo/aQ;->b:[Lo/T;

    aget-object v6, v6, v5

    invoke-direct {p0, v2, v6, v0}, Lo/aQ;->a(Lo/T;Lo/T;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_2
    if-lez v5, :cond_1

    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    add-int/lit8 v6, v0, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lo/aQ;->c:[Lo/T;

    aget-object v6, v6, v5

    aput-object v6, v2, v3

    :cond_1
    iget-object v2, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v2, v2, v0

    iget-object v6, p0, Lo/aQ;->c:[Lo/T;

    aget-object v6, v6, v5

    aput-object v6, v2, v4

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v2

    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v4

    goto :goto_1

    :cond_3
    if-ge v0, v8, :cond_4

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lo/aQ;->b:[Lo/T;

    aget-object v2, v2, v4

    invoke-direct {p0, v1, v2, v0}, Lo/aQ;->a(Lo/T;Lo/T;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_4
    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, v7

    iget-object v1, p0, Lo/aQ;->c:[Lo/T;

    aget-object v1, v1, v4

    aput-object v1, v0, v3

    return-void

    :cond_5
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public a(I)Lo/T;
    .locals 1

    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lo/aR;
    .locals 1

    iget-object v0, p0, Lo/aQ;->e:Lo/aR;

    return-object v0
.end method

.method public a(I[Lo/T;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, p1

    aget-object v0, v0, v1

    aput-object v0, p2, v1

    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, p1

    aget-object v0, v0, v2

    aput-object v0, p2, v2

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lo/aQ;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v1

    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lo/aQ;->a(I)Lo/T;

    move-result-object v0

    aput-object v0, p2, v2

    goto :goto_0
.end method

.method public a(Lo/T;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    invoke-virtual {v0, p1}, Lo/t;->a(Lo/T;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v3, v2

    move v0, v2

    :goto_1
    const/4 v4, 0x6

    if-ge v3, v4, :cond_2

    iget-object v4, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v4, v4, v3

    aget-object v4, v4, v2

    iget-object v5, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v5, v5, v3

    aget-object v5, v5, v1

    invoke-static {v4, v5, p1}, Lo/V;->b(Lo/T;Lo/T;Lo/T;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    if-ne v0, v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public a(Lo/ae;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p1}, Lo/ae;->a()Lo/ad;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/aR;->b(Lo/ae;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lo/ae;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p1, v0}, Lo/ae;->a(I)Lo/T;

    move-result-object v2

    invoke-virtual {p0, v2}, Lo/aQ;->a(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public b()Lo/ad;
    .locals 1

    iget-object v0, p0, Lo/aQ;->f:Lo/ad;

    return-object v0
.end method

.method public c()Lo/ae;
    .locals 1

    iget-object v0, p0, Lo/aQ;->d:Lo/t;

    return-object v0
.end method

.method public d()Lo/T;
    .locals 2

    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public e()Lo/T;
    .locals 2

    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lo/aQ;

    if-eqz v0, :cond_1

    check-cast p1, Lo/aQ;

    iget-object v0, p0, Lo/aQ;->b:[Lo/T;

    iget-object v1, p1, Lo/aQ;->b:[Lo/T;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lo/T;
    .locals 2

    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public g()Lo/T;
    .locals 2

    iget-object v0, p0, Lo/aQ;->c:[Lo/T;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lo/aQ;->b:[Lo/T;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()[Lo/ae;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lo/aQ;->a:Z

    if-nez v0, :cond_0

    new-array v0, v6, [Lo/ae;

    iget-object v1, p0, Lo/aQ;->d:Lo/t;

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    aget-object v0, v0, v6

    move v1, v3

    move-object v2, v0

    :goto_1
    const/4 v0, 0x6

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, v1

    aget-object v5, v0, v3

    if-eq v2, v5, :cond_1

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v4, v3, v6}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    :cond_1
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lo/aQ;->g:[[Lo/T;

    aget-object v0, v0, v1

    aget-object v2, v0, v6

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    new-array v1, v7, [Lo/ae;

    move v2, v3

    :goto_2
    if-ge v2, v7, :cond_3

    new-instance v5, Lo/W;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-array v6, v3, [Lo/T;

    invoke-interface {v0, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lo/T;

    invoke-direct {v5, v0}, Lo/W;-><init>([Lo/T;)V

    aput-object v5, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/aQ;->b:[Lo/T;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
