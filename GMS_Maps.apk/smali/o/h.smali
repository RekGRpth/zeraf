.class public Lo/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lo/ae;

.field private b:I

.field private c:[Lo/T;

.field private d:Z

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private g:I


# direct methods
.method public constructor <init>(Lo/ae;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lo/h;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lo/h;->f:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lo/h;->a(Lo/ae;)V

    return-void
.end method

.method private a(ILo/T;IZ)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lo/h;->b:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2, p3, p4}, Lo/h;->a(Lo/T;IZ)V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_3

    iget-object v0, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {v0}, Lo/ae;->c()Lo/T;

    move-result-object v1

    iget-object v0, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {v0, v4}, Lo/ae;->a(I)Lo/T;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0, p2}, Lo/V;->a(Lo/T;Lo/T;Lo/T;)I

    move-result v2

    if-ltz v2, :cond_4

    if-nez p4, :cond_1

    iget-object v2, p0, Lo/h;->c:[Lo/T;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lo/V;->a(Lo/T;Lo/T;Lo/T;)I

    move-result v2

    if-gez v2, :cond_1

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    iget-object v3, p0, Lo/h;->c:[Lo/T;

    aget-object v3, v3, p1

    invoke-static {v1, v0, p2, v3, v2}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;Lo/T;)Z

    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, p3, v1}, Lo/h;->a(ILo/T;IZ)V

    :cond_1
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, p2, p3, p4}, Lo/h;->a(ILo/T;IZ)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lo/h;->c:[Lo/T;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lo/T;->b(Lo/T;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lo/h;->a:Lo/ae;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lo/ae;->a(I)Lo/T;

    move-result-object v1

    iget-object v0, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {v0, p1}, Lo/ae;->a(I)Lo/T;

    move-result-object v0

    goto :goto_1

    :cond_4
    if-nez p4, :cond_2

    iget-object v2, p0, Lo/h;->c:[Lo/T;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lo/V;->a(Lo/T;Lo/T;Lo/T;)I

    move-result v2

    if-ltz v2, :cond_2

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    iget-object v3, p0, Lo/h;->c:[Lo/T;

    aget-object v3, v3, p1

    invoke-static {v1, v0, v3, p2, v2}, Lo/V;->a(Lo/T;Lo/T;Lo/T;Lo/T;Lo/T;)Z

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, v2, p3, v4}, Lo/h;->a(ILo/T;IZ)V

    goto :goto_2
.end method

.method private a(Lo/T;IZ)V
    .locals 2

    if-eqz p3, :cond_1

    iget v0, p0, Lo/h;->g:I

    iget-object v1, p0, Lo/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lo/h;->e:Ljava/util/ArrayList;

    new-instance v1, Lo/Z;

    invoke-direct {v1}, Lo/Z;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lo/h;->f:Ljava/util/ArrayList;

    new-instance v1, Lo/i;

    invoke-direct {v1}, Lo/i;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lo/h;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lo/h;->g:I

    :cond_1
    iget-object v0, p0, Lo/h;->e:Ljava/util/ArrayList;

    iget v1, p0, Lo/h;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/Z;

    invoke-virtual {v0, p1}, Lo/Z;->a(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lo/h;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lo/h;->f:Ljava/util/ArrayList;

    iget v1, p0, Lo/h;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/i;

    invoke-virtual {v0, p2}, Lo/i;->a(I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lo/X;Ljava/util/ArrayList;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lo/h;->g:I

    iput-boolean v0, p0, Lo/h;->d:Z

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1, v2}, Lo/X;->a(ILo/T;)V

    invoke-direct {p0, v0, v2, v0, v5}, Lo/h;->a(ILo/T;IZ)V

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v4, v2}, Lo/X;->a(ILo/T;)V

    invoke-direct {p0, v0, v2, v0, v0}, Lo/h;->a(ILo/T;IZ)V

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_1
    iget v0, p0, Lo/h;->g:I

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lo/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/Z;

    invoke-virtual {v0}, Lo/Z;->a()I

    move-result v2

    if-le v2, v5, :cond_1

    invoke-virtual {v0}, Lo/Z;->d()Lo/X;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lo/Z;->c()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(Lo/X;Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, v0}, Lo/h;->a(Lo/X;[ILjava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public a(Lo/X;[ILjava/util/List;Ljava/util/List;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput v2, p0, Lo/h;->g:I

    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lo/h;->d:Z

    invoke-virtual {p1}, Lo/X;->a()Lo/ad;

    move-result-object v0

    iget-object v3, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {v3, v0}, Lo/ae;->a(Lo/ae;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {v3, v0}, Lo/ae;->b(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lo/h;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v5

    invoke-virtual {p1, v2, v4}, Lo/X;->a(ILo/T;)V

    iget-boolean v0, p0, Lo/h;->d:Z

    if-eqz v0, :cond_4

    aget v0, p2, v2

    :goto_2
    invoke-direct {p0, v2, v4, v0, v1}, Lo/h;->a(ILo/T;IZ)V

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_6

    invoke-virtual {p1, v3, v4}, Lo/X;->a(ILo/T;)V

    iget-boolean v0, p0, Lo/h;->d:Z

    if-eqz v0, :cond_5

    aget v0, p2, v3

    :goto_4
    invoke-direct {p0, v2, v4, v0, v2}, Lo/h;->a(ILo/T;IZ)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    :goto_5
    iget v0, p0, Lo/h;->g:I

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lo/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/Z;

    invoke-virtual {v0}, Lo/Z;->a()I

    move-result v3

    if-le v3, v1, :cond_7

    invoke-virtual {v0}, Lo/Z;->d()Lo/X;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v0}, Lo/Z;->c()V

    iget-boolean v0, p0, Lo/h;->d:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lo/h;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/i;

    invoke-virtual {v0}, Lo/i;->c()I

    move-result v3

    if-le v3, v1, :cond_8

    invoke-virtual {v0}, Lo/i;->a()[I

    move-result-object v3

    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-virtual {v0}, Lo/i;->b()V

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5
.end method

.method public a(Lo/ae;)V
    .locals 3

    iput-object p1, p0, Lo/h;->a:Lo/ae;

    invoke-virtual {p1}, Lo/ae;->b()I

    move-result v0

    iput v0, p0, Lo/h;->b:I

    iget v0, p0, Lo/h;->b:I

    new-array v0, v0, [Lo/T;

    iput-object v0, p0, Lo/h;->c:[Lo/T;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lo/h;->c:[Lo/T;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lo/h;->c:[Lo/T;

    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
