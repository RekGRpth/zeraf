.class public abstract Lz/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field protected c:Z

.field protected d:I

.field protected e:Lz/k;

.field final f:Lz/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lz/p;->values()[Lz/p;

    move-result-object v0

    array-length v0, v0

    sput v0, Lz/o;->a:I

    sget-object v0, Lz/p;->b:Lz/p;

    invoke-virtual {v0}, Lz/p;->a()I

    move-result v0

    sput v0, Lz/o;->b:I

    return-void
.end method

.method protected constructor <init>(Lz/p;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lz/o;->c:Z

    iput v0, p0, Lz/o;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lz/o;->e:Lz/k;

    iput-object p1, p0, Lz/o;->f:Lz/p;

    return-void
.end method


# virtual methods
.method a(Lz/i;)V
    .locals 1

    iget v0, p0, Lz/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/o;->d:I

    return-void
.end method

.method abstract a(Lz/k;Lz/o;)V
.end method

.method a(Lz/k;Lz/j;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p2, Lz/j;->e:Z

    iget-boolean v2, p0, Lz/o;->c:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p2, Lz/j;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_2

    iget v1, p0, Lz/o;->d:I

    if-nez v1, :cond_0

    :cond_2
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/o;->c:Z

    iget-boolean v0, p0, Lz/o;->c:Z

    if-eqz v0, :cond_3

    :goto_1
    iput-object p1, p0, Lz/o;->e:Lz/k;

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    goto :goto_1
.end method

.method b(Lz/i;)V
    .locals 1

    iget v0, p0, Lz/o;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lz/o;->d:I

    return-void
.end method
