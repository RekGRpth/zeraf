.class public Lz/J;
.super Lz/o;
.source "SourceFile"


# instance fields
.field g:[I

.field h:Lz/w;

.field private i:Lz/u;

.field private volatile j:I

.field private volatile k:I

.field private volatile l:Z

.field private volatile m:I

.field private volatile n:I

.field private volatile o:Z

.field private final p:I

.field private q:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lz/J;-><init>(Lz/u;)V

    return-void
.end method

.method public constructor <init>(Lz/u;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lz/J;-><init>(Lz/u;I)V

    return-void
.end method

.method public constructor <init>(Lz/u;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lz/J;-><init>(Lz/u;IZ)V

    return-void
.end method

.method public constructor <init>(Lz/u;IZ)V
    .locals 3

    const/16 v2, 0x2901

    const/4 v1, 0x0

    invoke-static {p2}, Lz/J;->a(I)Lz/p;

    move-result-object v0

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lz/J;->i:Lz/u;

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lz/J;->g:[I

    const/16 v0, 0x2600

    iput v0, p0, Lz/J;->j:I

    const/16 v0, 0x2601

    iput v0, p0, Lz/J;->k:I

    iput-boolean v1, p0, Lz/J;->l:Z

    iput v2, p0, Lz/J;->m:I

    iput v2, p0, Lz/J;->n:I

    iput-boolean v1, p0, Lz/J;->o:Z

    iput-boolean v1, p0, Lz/J;->q:Z

    new-instance v0, Lz/w;

    invoke-direct {v0}, Lz/w;-><init>()V

    iput-object v0, p0, Lz/J;->h:Lz/w;

    iput-object p1, p0, Lz/J;->i:Lz/u;

    iput-boolean p3, p0, Lz/J;->q:Z

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported texture unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x84c0

    iput v0, p0, Lz/J;->p:I

    :goto_0
    return-void

    :pswitch_1
    const v0, 0x84c1

    iput v0, p0, Lz/J;->p:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(I)Lz/p;
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported texture unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lz/p;->b:Lz/p;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lz/p;->c:Lz/p;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(II)V
    .locals 4

    const v3, 0x8370

    const v2, 0x812f

    const/16 v1, 0x2901

    iget-boolean v0, p0, Lz/J;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    if-eq p1, v1, :cond_1

    if-eq p1, v2, :cond_1

    if-ne p1, v3, :cond_2

    :cond_1
    if-eq p2, v1, :cond_3

    if-eq p2, v2, :cond_3

    if-eq p2, v3, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Wrap Mode: wrapS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wrapT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lz/J;->m:I

    iput p2, p0, Lz/J;->n:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/J;->o:Z

    return-void
.end method

.method a(Lz/k;Lz/o;)V
    .locals 2

    iget-object v0, p0, Lz/J;->i:Lz/u;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget v0, p0, Lz/J;->p:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const/16 v0, 0xde1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method

.method protected a(Lz/u;Z)V
    .locals 2

    iget-boolean v0, p0, Lz/J;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called BEFORE set live"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean p2, p0, Lz/J;->q:Z

    iput-object p1, p0, Lz/J;->i:Lz/u;

    return-void
.end method

.method a(Lz/k;Lz/j;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xde1

    invoke-super {p0, p1, p2}, Lz/o;->a(Lz/k;Lz/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p2, Lz/j;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lz/J;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    iget-object v1, p0, Lz/J;->g:[I

    aget v1, v1, v2

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget-object v1, p0, Lz/J;->i:Lz/u;

    invoke-virtual {v1, p1, p2}, Lz/u;->a(Lz/k;Lz/j;)Z

    const/16 v1, 0x2801

    iget v2, p0, Lz/J;->j:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2800

    iget v2, p0, Lz/J;->k:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2802

    iget v2, p0, Lz/J;->m:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    iget v2, p0, Lz/J;->n:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-boolean v1, p0, Lz/J;->q:Z

    if-eqz v1, :cond_0

    invoke-static {v3}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lz/J;->i:Lz/u;

    invoke-virtual {v1, p1, p2}, Lz/u;->a(Lz/k;Lz/j;)Z

    iget-object v1, p0, Lz/J;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 3

    const/16 v2, 0x2601

    const/16 v1, 0x2600

    iget-boolean v0, p0, Lz/J;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_1

    const/16 v0, 0x2703

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2701

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2702

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2700

    if-ne p1, v0, :cond_2

    :cond_1
    if-eq p2, v2, :cond_3

    if-eq p2, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Filter Mode: min = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lz/J;->j:I

    iput p2, p0, Lz/J;->k:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/J;->l:Z

    return-void
.end method
