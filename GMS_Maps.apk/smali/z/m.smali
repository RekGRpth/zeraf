.class public Lz/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lz/l;

.field private b:Lz/l;


# direct methods
.method private declared-synchronized a()Lz/l;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lz/m;->a:Lz/l;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lz/l;->a(Lz/l;)Lz/l;

    move-result-object v1

    iput-object v1, p0, Lz/m;->a:Lz/l;

    iget-object v1, p0, Lz/m;->a:Lz/l;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lz/m;->b:Lz/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lz/m;)Lz/l;
    .locals 1

    invoke-direct {p0}, Lz/m;->a()Lz/l;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lz/l;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lz/m;->b:Lz/l;

    iput-object p1, p0, Lz/m;->b:Lz/l;

    if-nez v0, :cond_0

    iput-object p1, p0, Lz/m;->a:Lz/l;

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lz/l;->a(Lz/l;Lz/l;)Lz/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {v0, p1}, Lz/l;->a(Lz/l;Lz/l;)Lz/l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lz/m;Lz/l;)V
    .locals 0

    invoke-direct {p0, p1}, Lz/m;->a(Lz/l;)V

    return-void
.end method
