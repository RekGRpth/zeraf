.class public Lz/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Lz/K;

.field protected b:Z

.field protected c:[Lz/K;

.field protected d:[I

.field e:[[F

.field protected f:Z

.field private final g:[[Lz/o;

.field private final h:[Lz/M;

.field private i:Ljava/lang/String;

.field private j:B

.field private k:B

.field private l:Lz/k;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/i;->b:Z

    iput-boolean v1, p0, Lz/i;->f:Z

    iput-byte v1, p0, Lz/i;->j:B

    const/4 v0, -0x1

    iput-byte v0, p0, Lz/i;->k:B

    new-array v0, v2, [Lz/M;

    iput-object v0, p0, Lz/i;->h:[Lz/M;

    sget v0, Lz/o;->a:I

    filled-new-array {v2, v0}, [I

    move-result-object v0

    const-class v1, Lz/o;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lz/o;

    iput-object v0, p0, Lz/i;->g:[[Lz/o;

    new-instance v0, Lz/K;

    invoke-direct {v0}, Lz/K;-><init>()V

    iput-object v0, p0, Lz/i;->a:Lz/K;

    new-array v0, v2, [Lz/K;

    iput-object v0, p0, Lz/i;->c:[Lz/K;

    new-array v0, v2, [[F

    iput-object v0, p0, Lz/i;->e:[[F

    new-array v0, v2, [I

    iput-object v0, p0, Lz/i;->d:[I

    return-void
.end method


# virtual methods
.method public a()B
    .locals 1

    iget-byte v0, p0, Lz/i;->j:B

    return v0
.end method

.method public a(Lz/p;)I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lz/i;->g:[[Lz/o;

    aget-object v2, v2, v1

    invoke-virtual {p1}, Lz/p;->a()I

    move-result v3

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    shl-int/2addr v2, v1

    or-int/2addr v0, v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public a(I)V
    .locals 1

    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    int-to-byte v0, p1

    iput-byte v0, p0, Lz/i;->k:B

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lz/i;->i:Ljava/lang/String;

    return-void
.end method

.method public a(Lz/K;)V
    .locals 1

    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iget-object v0, p0, Lz/i;->a:Lz/K;

    invoke-virtual {v0, p1}, Lz/K;->a(Lz/K;)Lz/K;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/i;->b:Z

    return-void
.end method

.method public a(Lz/M;I)V
    .locals 4

    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    iget-byte v0, p0, Lz/i;->j:B

    or-int/2addr v0, p2

    int-to-byte v0, v0

    iput-byte v0, p0, Lz/i;->j:B

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_5

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    iget-object v1, p0, Lz/i;->h:[Lz/M;

    aget-object v1, v1, v0

    iget-object v2, p0, Lz/i;->h:[Lz/M;

    aput-object p1, v2, v0

    iget-object v2, p0, Lz/i;->c:[Lz/K;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    iget-object v2, p0, Lz/i;->c:[Lz/K;

    new-instance v3, Lz/K;

    invoke-direct {v3}, Lz/K;-><init>()V

    aput-object v3, v2, v0

    :cond_1
    iget-object v2, p0, Lz/i;->e:[[F

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    iget-object v2, p0, Lz/i;->e:[[F

    const/16 v3, 0x10

    new-array v3, v3, [F

    aput-object v3, v2, v0

    :cond_2
    iget-boolean v2, p0, Lz/i;->f:Z

    if-eqz v2, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {v1, p0}, Lz/M;->a(Lz/i;)V

    iget-object v2, p0, Lz/i;->l:Lz/k;

    sget-object v3, Lz/j;->a:Lz/j;

    invoke-virtual {v1, v2, v3}, Lz/M;->a(Lz/k;Lz/j;)Z

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1, p0}, Lz/M;->b(Lz/i;)V

    iget-object v1, p0, Lz/i;->l:Lz/k;

    sget-object v2, Lz/j;->c:Lz/j;

    invoke-virtual {p1, v1, v2}, Lz/M;->a(Lz/k;Lz/j;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public a(Lz/o;I)V
    .locals 4

    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lz/k;->c()V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_3

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p2

    if-eqz v1, :cond_2

    iget-object v1, p0, Lz/i;->g:[[Lz/o;

    aget-object v1, v1, v0

    iget-object v2, p1, Lz/o;->f:Lz/p;

    invoke-virtual {v2}, Lz/p;->a()I

    move-result v2

    aget-object v1, v1, v2

    iget-object v2, p0, Lz/i;->g:[[Lz/o;

    aget-object v2, v2, v0

    iget-object v3, p1, Lz/o;->f:Lz/p;

    invoke-virtual {v3}, Lz/p;->a()I

    move-result v3

    aput-object p1, v2, v3

    iget-boolean v2, p0, Lz/i;->f:Z

    if-eqz v2, :cond_2

    if-eqz v1, :cond_1

    invoke-virtual {v1, p0}, Lz/o;->b(Lz/i;)V

    iget-object v2, p0, Lz/i;->l:Lz/k;

    sget-object v3, Lz/j;->a:Lz/j;

    invoke-virtual {v1, v2, v3}, Lz/o;->a(Lz/k;Lz/j;)Z

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Lz/o;->a(Lz/i;)V

    iget-object v1, p0, Lz/i;->l:Lz/k;

    sget-object v2, Lz/j;->c:Lz/j;

    invoke-virtual {p1, v1, v2}, Lz/o;->a(Lz/k;Lz/j;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method a(Lz/k;Lz/j;)Z
    .locals 9

    const/4 v0, 0x0

    iget-boolean v1, p2, Lz/j;->e:Z

    iget-boolean v2, p0, Lz/i;->f:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lz/i;->l:Lz/k;

    iget-object v2, p0, Lz/i;->h:[Lz/M;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    iget-boolean v5, p2, Lz/j;->f:Z

    if-nez v5, :cond_1

    iget-boolean v5, p2, Lz/j;->e:Z

    if-eqz v5, :cond_3

    invoke-virtual {v4, p0}, Lz/M;->b(Lz/i;)V

    :cond_1
    :goto_2
    invoke-virtual {v4, p1, p2}, Lz/M;->a(Lz/k;Lz/j;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4, p0}, Lz/M;->a(Lz/i;)V

    goto :goto_2

    :cond_4
    const-string v1, "Entity"

    const-string v2, "vertex data setLive"

    invoke-static {v1, v2}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lz/i;->g:[[Lz/o;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_8

    aget-object v7, v5, v1

    if-eqz v7, :cond_6

    iget-boolean v8, p2, Lz/j;->f:Z

    if-nez v8, :cond_5

    iget-boolean v8, p2, Lz/j;->e:Z

    if-eqz v8, :cond_7

    invoke-virtual {v7, p0}, Lz/o;->a(Lz/i;)V

    :cond_5
    :goto_5
    invoke-virtual {v7, p1, p2}, Lz/o;->a(Lz/k;Lz/j;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v7, p0}, Lz/o;->b(Lz/i;)V

    goto :goto_5

    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_9
    const-string v0, "Entity"

    const-string v1, "entity state setLive"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/i;->f:Z

    const/4 v0, 0x1

    goto :goto_0
.end method
