.class public Ly/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/n;

.field private final b:Ly/b;


# direct methods
.method public constructor <init>(Lo/n;Ly/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Ly/c;->a:Lo/n;

    iput-object p2, p0, Ly/c;->b:Ly/b;

    return-void
.end method


# virtual methods
.method public a()Lo/n;
    .locals 1

    iget-object v0, p0, Ly/c;->a:Lo/n;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Ly/c;->a:Lo/n;

    invoke-interface {v0}, Lo/n;->i()I

    move-result v0

    return v0
.end method

.method public c()Ly/b;
    .locals 1

    iget-object v0, p0, Ly/c;->b:Ly/b;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Ly/c;->a:Lo/n;

    invoke-interface {v0}, Lo/n;->m()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Ly/c;

    iget-object v2, p0, Ly/c;->a:Lo/n;

    iget-object v3, p1, Ly/c;->a:Lo/n;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Ly/c;->b:Ly/b;

    iget-object v3, p1, Ly/c;->b:Ly/b;

    invoke-static {v2, v3}, Ly/b;->a(Ly/b;Ly/b;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Ly/c;->a:Lo/n;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Ly/c;->b:Ly/b;

    if-eqz v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ly/c;->b:Ly/b;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
