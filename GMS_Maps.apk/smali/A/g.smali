.class abstract LA/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method private constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, LA/g;->b:I

    const-string v0, ""

    iput-object v0, p0, LA/g;->c:Ljava/lang/String;

    iput-boolean v1, p0, LA/g;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LA/g;->e:Z

    iput-boolean v1, p0, LA/g;->f:Z

    iput-boolean v1, p0, LA/g;->g:Z

    iput p1, p0, LA/g;->a:I

    return-void
.end method

.method synthetic constructor <init>(ILA/d;)V
    .locals 0

    invoke-direct {p0, p1}, LA/g;-><init>(I)V

    return-void
.end method

.method static synthetic a(LA/g;)I
    .locals 1

    iget v0, p0, LA/g;->a:I

    return v0
.end method

.method static synthetic b(LA/g;)I
    .locals 1

    iget v0, p0, LA/g;->b:I

    return v0
.end method

.method static synthetic c(LA/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LA/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(LA/g;)Z
    .locals 1

    iget-boolean v0, p0, LA/g;->d:Z

    return v0
.end method

.method static synthetic e(LA/g;)Z
    .locals 1

    iget-boolean v0, p0, LA/g;->e:Z

    return v0
.end method

.method static synthetic f(LA/g;)Z
    .locals 1

    iget-boolean v0, p0, LA/g;->f:Z

    return v0
.end method

.method static synthetic g(LA/g;)Z
    .locals 1

    iget-boolean v0, p0, LA/g;->g:Z

    return v0
.end method


# virtual methods
.method abstract a()LA/c;
.end method

.method a(I)LA/g;
    .locals 0

    iput p1, p0, LA/g;->b:I

    return-object p0
.end method

.method a(Ljava/lang/String;)LA/g;
    .locals 0

    iput-object p1, p0, LA/g;->c:Ljava/lang/String;

    return-object p0
.end method

.method b(Z)LA/g;
    .locals 0

    iput-boolean p1, p0, LA/g;->d:Z

    return-object p0
.end method

.method c(Z)LA/g;
    .locals 0

    iput-boolean p1, p0, LA/g;->e:Z

    return-object p0
.end method

.method d(Z)LA/g;
    .locals 0

    iput-boolean p1, p0, LA/g;->f:Z

    return-object p0
.end method

.method e(Z)LA/g;
    .locals 0

    iput-boolean p1, p0, LA/g;->g:Z

    return-object p0
.end method
