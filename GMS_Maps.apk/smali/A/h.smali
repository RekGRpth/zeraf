.class LA/h;
.super LA/c;
.source "SourceFile"


# direct methods
.method private constructor <init>(LA/i;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LA/c;-><init>(LA/g;LA/d;)V

    return-void
.end method

.method synthetic constructor <init>(LA/i;LA/d;)V
    .locals 0

    invoke-direct {p0, p1}, LA/h;-><init>(LA/i;)V

    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    const/16 v0, 0x800

    return v0
.end method

.method public a(Lo/ad;)Lo/T;
    .locals 1

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
    .locals 9

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v0}, LA/c;->b(I)I

    move-result v3

    sget-object v0, LA/c;->d:LA/c;

    if-ne p0, v0, :cond_0

    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Landroid/content/res/Resources;I)I

    move-result v4

    :goto_0
    invoke-static {p0}, LA/c;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    :goto_1
    new-instance v0, Lr/l;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lr/l;-><init>(Law/p;LA/c;IIFLjava/util/Locale;Ljava/io/File;Lt/g;)V

    return-object v0

    :cond_0
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v4

    goto :goto_0

    :cond_1
    const/high16 v5, 0x3f800000

    goto :goto_1
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v0, 0x3f800000

    const v1, 0x3e99999a

    invoke-interface {p1, v2, v2, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LA/c;

    invoke-super {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public j()LF/O;
    .locals 3

    new-instance v0, LF/O;

    sget-object v1, LF/N;->h:LF/N;

    sget-object v2, LF/s;->b:LF/s;

    invoke-direct {v0, v1, v2}, LF/O;-><init>(LF/N;LF/s;)V

    return-object v0
.end method

.method public k()Lo/aj;
    .locals 1

    invoke-static {}, LA/c;->p()Lo/aj;

    move-result-object v0

    return-object v0
.end method

.method public l()Lt/J;
    .locals 1

    new-instance v0, LA/j;

    invoke-direct {v0, p0}, LA/j;-><init>(LA/c;)V

    return-object v0
.end method
