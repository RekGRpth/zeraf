.class public abstract LaR/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:J

.field protected c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, LaR/t;->a:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaR/t;->b:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaR/t;->c:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, LaR/t;-><init>(Ljava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, LaR/t;->a:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaR/t;->b:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaR/t;->c:J

    iput-object p1, p0, LaR/t;->a:Ljava/lang/String;

    iput-wide p2, p0, LaR/t;->b:J

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, LaR/t;->b:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LaR/t;->a:Ljava/lang/String;

    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, LaR/t;->c:J

    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaR/t;->a:Ljava/lang/String;

    return-object v0
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, LaR/t;->b:J

    return-wide v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, LaR/t;->c:J

    return-wide v0
.end method

.method protected final k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/er;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, LaR/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v1, p0, LaR/t;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    iget-wide v2, p0, LaR/t;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-wide v1, p0, LaR/t;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    const/16 v1, 0xc

    iget-wide v2, p0, LaR/t;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-object v0
.end method
