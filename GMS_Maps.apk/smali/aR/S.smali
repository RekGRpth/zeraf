.class public LaR/S;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/R;


# instance fields
.field private a:LaR/Y;

.field private b:LaR/ac;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LaR/Y;

    invoke-direct {v0, p0, p1}, LaR/Y;-><init>(LaR/aa;Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, LaR/S;->a:LaR/Y;

    new-instance v0, LaR/ac;

    iget-object v1, p0, LaR/S;->a:LaR/Y;

    invoke-direct {v0, v1}, LaR/ac;-><init>(LaR/X;)V

    iput-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {p0}, LaR/S;->i()V

    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    .locals 2

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0, p2, p3}, LaR/S;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public C_()V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->g()V

    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->h()V

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public a(I)LaR/T;
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->d(I)LaR/T;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaR/O;)V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->e(I)V

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, LaR/S;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V
    .locals 1

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1, p2, p3, p4}, LaR/ac;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    return-void
.end method

.method public a(LaR/ab;)V
    .locals 1

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1}, LaR/ac;->a(LaR/ab;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    .locals 1

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1, p2, p3}, LaR/ac;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method public a(ZLaR/O;)V
    .locals 2

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->b()V

    if-eqz p1, :cond_0

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->c()V

    :cond_0
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p2}, LaR/S;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    return-void
.end method

.method public b(LaR/ab;)V
    .locals 1

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1}, LaR/ac;->b(LaR/ab;)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->d()V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->e()V

    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0}, LaR/ac;->h()V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->f()V

    return-void
.end method
