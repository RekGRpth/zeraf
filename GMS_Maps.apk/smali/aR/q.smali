.class public LaR/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/C;
.implements LaR/n;
.implements Lcom/google/googlenav/g;


# instance fields
.field private a:LaR/o;

.field private final b:I

.field private final c:LaR/R;

.field private d:Law/h;

.field private final e:Ljava/util/List;

.field private f:Lcom/google/googlenav/ui/wizard/jv;

.field private g:LaR/u;

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaR/q;->e:Ljava/util/List;

    iput-boolean v1, p0, LaR/q;->h:Z

    iput-boolean v1, p0, LaR/q;->i:Z

    iput-boolean v1, p0, LaR/q;->j:Z

    iput-object p1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p2, p0, LaR/q;->c:LaR/R;

    iput-object p3, p0, LaR/q;->d:Law/h;

    iput p4, p0, LaR/q;->b:I

    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p6, p5}, LaR/q;->a(LaR/I;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    return-void
.end method

.method private a(LaR/I;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    .locals 3

    iget v0, p0, LaR/q;->b:I

    iget-object v1, p0, LaR/q;->c:LaR/R;

    iget v2, p0, LaR/q;->b:I

    invoke-interface {v1, v2}, LaR/R;->a(I)LaR/T;

    move-result-object v1

    invoke-static {v0, v1, p1}, LaR/v;->a(ILaR/T;LaR/I;)LaR/v;

    move-result-object v0

    iput-object v0, p0, LaR/q;->g:LaR/u;

    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, p0}, LaR/u;->a(LaR/C;)V

    return-void
.end method

.method static synthetic a(LaR/q;)V
    .locals 0

    invoke-direct {p0}, LaR/q;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, LaR/q;->i:Z

    iget-boolean v0, p0, LaR/q;->h:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, LaR/q;->h:Z

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    :cond_0
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->C_()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->j:Z

    return-void
.end method


# virtual methods
.method public B_()V
    .locals 3

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->h:Z

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x581

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1b2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    :cond_0
    return-void
.end method

.method public C_()V
    .locals 3

    iget-boolean v0, p0, LaR/q;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->i:Z

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, LaR/s;

    invoke-direct {v2, p0}, LaR/s;-><init>(LaR/q;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LaR/q;->g()V

    goto :goto_0
.end method

.method public D_()V
    .locals 2

    iget-boolean v0, p0, LaR/q;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/q;->h:Z

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    :cond_0
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->D_()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 2

    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    invoke-interface {v0}, LaM/h;->M_()V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, LaR/q;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/q;->j:Z

    :cond_1
    return-void
.end method

.method public N_()V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    instance-of v1, v0, LaR/M;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/t;

    check-cast v0, LaR/D;

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "v="

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    const-string v2, "s"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/M;

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/M;->a(Ljava/lang/String;)LaR/t;

    move-result-object v1

    check-cast v1, LaR/D;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaR/D;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, LaR/D;->a(Z)V

    invoke-virtual {v0, v1}, LaR/M;->a(LaR/t;)Z

    const-string v0, "d"

    invoke-virtual {v1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2, p4}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LaR/D;->a(Z)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, LaR/D;->a(J)V

    invoke-virtual {v0, p1}, LaR/M;->a(LaR/t;)Z

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2, v3}, LaR/M;->a(LaR/H;Z)V

    :cond_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p3, v3}, Lbf/am;->a(Z)Lbf/by;

    :cond_2
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lbf/am;->f(Ljava/lang/String;)V

    :cond_3
    const-string v0, "c"

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p4}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    goto :goto_0
.end method

.method public a(LaR/O;)V
    .locals 2

    iget-object v0, p0, LaR/q;->c:LaR/R;

    iget v1, p0, LaR/q;->b:I

    invoke-interface {v0, v1, p1}, LaR/R;->a(ILaR/O;)V

    return-void
.end method

.method public a(LaR/o;)V
    .locals 0

    iput-object p1, p0, LaR/q;->a:LaR/o;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V
    .locals 2

    iget-object v0, p0, LaR/q;->c:LaR/R;

    iget v1, p0, LaR/q;->b:I

    invoke-interface {v0, v1, p1, p2, p3}, LaR/R;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    iput-object p1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/f;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    iget-object v1, p0, LaR/q;->d:Law/h;

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    new-instance v0, LaR/r;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, LaR/r;-><init>(LaR/q;Las/c;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, LaR/r;->g()V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "p"

    invoke-virtual {p0, p1, p2, v0}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    const-string v2, "f"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, p1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, LaR/D;

    if-eqz v1, :cond_0

    check-cast v0, LaR/D;

    iget-object v1, p0, LaR/q;->g:LaR/u;

    invoke-interface {v1, p1}, LaR/u;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fC;->p:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x90

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v0}, LaR/D;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1}, LaR/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/H;

    move-result-object v2

    invoke-virtual {v2, v4}, LaR/H;->a(I)V

    :try_start_0
    invoke-virtual {v0}, LaR/D;->d()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {v0}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-static {v0}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LaR/H;->a(Z)V

    iget-object v0, p0, LaR/q;->g:LaR/u;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, LaR/u;->a(LaR/H;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 6

    const/16 v3, 0x90

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2, v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, v3}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, v3}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v4

    if-nez v4, :cond_5

    new-instance v0, LaR/H;

    invoke-direct {v0}, LaR/H;-><init>()V

    invoke-virtual {v0, v3}, LaR/H;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LaR/H;->a(I)V

    :goto_1
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, LbM/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v4, v2, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, LaR/H;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, LaR/H;->a(Z)V

    iget-object v1, p0, LaR/q;->g:LaR/u;

    invoke-interface {v1, v0, v2}, LaR/u;->a(LaR/H;Z)V

    iget-object v0, p0, LaR/q;->a:LaR/o;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaR/q;->a:LaR/o;

    invoke-interface {v0, v3}, LaR/o;->a(Ljava/lang/String;)V

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    new-instance v0, LaR/H;

    invoke-virtual {v4}, LaR/H;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v0, v4}, LaR/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1
.end method

.method public e()LaR/u;
    .locals 1

    iget-object v0, p0, LaR/q;->g:LaR/u;

    return-object v0
.end method

.method public t()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
