.class public LaR/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/C;
.implements Lcom/google/googlenav/g;


# instance fields
.field private final a:Law/h;

.field private final b:LaR/I;

.field private final c:Ljava/util/Set;

.field private d:Z

.field private e:Z

.field private final f:Ljava/lang/Object;

.field private g:Z

.field private final h:Ljava/util/Set;


# direct methods
.method public constructor <init>(Law/h;LaR/I;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaR/b;->c:Ljava/util/Set;

    iput-boolean v1, p0, LaR/b;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/b;->e:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaR/b;->f:Ljava/lang/Object;

    iput-boolean v1, p0, LaR/b;->g:Z

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaR/b;->h:Ljava/util/Set;

    iput-object p1, p0, LaR/b;->a:Law/h;

    iput-object p2, p0, LaR/b;->b:LaR/I;

    return-void
.end method

.method static synthetic a(LaR/b;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, LaR/b;->c:Ljava/util/Set;

    return-object v0
.end method

.method private a(LaR/u;Ljava/util/List;)V
    .locals 5

    const-string v0, "BackgroundPlaceDetailsFetcher.fetchDetailsIfNecessary"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, LaR/b;->h:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v0}, LaR/u;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LaR/b;->c:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, LaR/b;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_2

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const-string v0, "BackgroundPlaceDetailsFetcher.fetchDetailsIfNecessary"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :cond_2
    :try_start_1
    iget-object v3, p0, LaR/b;->c:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    iget-object v0, p0, LaR/b;->a:Law/h;

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method static synthetic a(LaR/b;LaR/n;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, LaR/b;->a(LaR/n;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(LaR/b;Z)Z
    .locals 0

    iput-boolean p1, p0, LaR/b;->d:Z

    return p1
.end method

.method private a(LaR/n;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-interface {p1}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-interface {v2, p2}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, p2, p3}, LaR/n;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, LaR/b;->h:Ljava/util/Set;

    invoke-interface {v3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v3, "BPDF2"

    invoke-static {v3, p2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, p2}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v4, LaR/H;

    invoke-virtual {v3}, LaR/H;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v4, v3}, LaR/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v4, v0}, LaR/H;->a(Z)V

    invoke-interface {v2, v4, v1}, LaR/u;->a(LaR/H;Z)V

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(LaR/b;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LaR/b;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(LaR/b;Z)Z
    .locals 0

    iput-boolean p1, p0, LaR/b;->e:Z

    return p1
.end method

.method private c()I
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x5

    new-array v2, v0, [LaR/n;

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v3

    invoke-virtual {v3}, LaR/l;->h()LaR/n;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v3

    invoke-virtual {v3}, LaR/l;->i()LaR/n;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v3

    invoke-virtual {v3}, LaR/l;->e()LaR/n;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x4

    iget-boolean v0, p0, LaR/b;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    move v0, v1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    if-nez v3, :cond_1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    aget-object v3, v2, v0

    invoke-interface {v3}, LaR/n;->e()LaR/u;

    move-result-object v3

    invoke-interface {v3}, LaR/u;->b()Ljava/util/List;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-interface {v4}, LaR/n;->e()LaR/u;

    move-result-object v4

    invoke-direct {p0, v4, v3}, LaR/b;->a(LaR/u;Ljava/util/List;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_2

    :cond_2
    return v1
.end method

.method static synthetic c(LaR/b;)Z
    .locals 1

    iget-boolean v0, p0, LaR/b;->d:Z

    return v0
.end method

.method static synthetic d(LaR/b;)Z
    .locals 1

    iget-boolean v0, p0, LaR/b;->e:Z

    return v0
.end method

.method static synthetic e(LaR/b;)I
    .locals 1

    invoke-direct {p0}, LaR/b;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    new-instance v0, LaR/c;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaR/c;-><init>(LaR/b;Las/c;)V

    invoke-virtual {v0}, LaR/c;->g()V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    new-instance v0, LaR/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, LaR/d;-><init>(LaR/b;Las/c;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, LaR/d;->g()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LaR/b;->g:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LaR/b;->b()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, LaR/b;->f:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaR/b;->d:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    iget-object v1, p0, LaR/b;->b:LaR/I;

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, LaR/l;->a(Ljava/lang/String;)LaR/D;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, LaR/b;->b:LaR/I;

    invoke-virtual {v1, p1}, LaR/I;->d(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, LaR/l;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LaR/b;->b()V

    goto :goto_0
.end method
