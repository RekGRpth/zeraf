.class public LaM/a;
.super LaM/f;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/googlenav/android/aa;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Z

.field private final h:Lbm/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/googlenav/android/aa;Lbm/c;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, LaM/f;-><init>()V

    iput-object v0, p0, LaM/a;->c:Ljava/lang/String;

    iput-object v0, p0, LaM/a;->d:Ljava/lang/String;

    iput-object v0, p0, LaM/a;->e:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, LaM/a;->a:Landroid/content/Context;

    iput-object p2, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    iput-object p3, p0, LaM/a;->h:Lbm/c;

    invoke-direct {p0}, LaM/a;->y()V

    invoke-direct {p0}, LaM/a;->x()V

    invoke-super {p0}, LaM/f;->i()V

    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "sid_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "lsid_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private C()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    iget-object v2, p0, LaM/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v3, v3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private D()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    new-instance v1, LaM/b;

    invoke-direct {v1, p0}, LaM/b;-><init>(LaM/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :goto_0
    invoke-virtual {p0}, LaM/a;->u()V

    return-void

    :cond_0
    invoke-virtual {p0}, LaM/a;->p()V

    goto :goto_0
.end method

.method private E()V
    .locals 0

    invoke-virtual {p0}, LaM/a;->m()V

    invoke-virtual {p0}, LaM/a;->s()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;
    .locals 2

    const-string v0, "AndroidLoginHelper.load"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    new-instance v1, LaM/a;

    invoke-direct {v1, p0, p1, v0}, LaM/a;-><init>(Landroid/content/Context;Lcom/google/googlenav/android/aa;Lbm/c;)V

    invoke-static {v1}, LaM/f;->a(LaM/f;)V

    :goto_0
    const-string v0, "AndroidLoginHelper.load"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, LaM/d;

    invoke-direct {v0}, LaM/d;-><init>()V

    invoke-static {v0}, LaM/f;->a(LaM/f;)V

    goto :goto_0
.end method

.method private a(Landroid/content/SharedPreferences;Z)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    const-string v0, "auth_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "auth_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "sid_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "lsid_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-direct {p0, v2}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v2, v1, v0}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V
    .locals 3

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-interface {p3, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p3, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LoginHelper - setting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 6

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    :cond_0
    if-eqz p2, :cond_4

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    if-eqz p2, :cond_1

    if-eqz v1, :cond_1

    const-string v0, "com.google"

    invoke-virtual {v3, v0, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "com.google"

    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, LaM/j;->a([Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    if-nez v4, :cond_2

    if-eqz p1, :cond_2

    invoke-static {p2}, LaM/j;->a(Z)V

    :cond_2
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/android/b;->i()Landroid/app/Activity;

    move-result-object v5

    if-nez v5, :cond_5

    :cond_3
    invoke-static {v3, v4, p1, p2, v2}, LaM/j;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    :goto_1
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_0

    :cond_5
    invoke-direct {p0, p2, v1}, LaM/a;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->i()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v3, v4, p1, p2, v0}, LaM/j;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaM/a;->h:Lbm/c;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lbm/c;->a([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-direct {p0, p1, p2, p3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, LaM/a;->n()V

    invoke-virtual {p0}, LaM/a;->t()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    if-eqz p4, :cond_0

    invoke-direct {p0, p1, p2, p3}, LaM/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, LaM/a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->b([B)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    iget-object v2, p0, LaM/a;->h:Lbm/c;

    invoke-virtual {v2, v0}, Lbm/c;->b([B)[B

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v0, v2

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Lcom/google/googlenav/common/util/d; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, LaM/a;->s()V

    return-void
.end method

.method private c(Z)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-direct {p0}, LaM/a;->D()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, LaM/a;->E()V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, LaM/a;->c:Ljava/lang/String;

    iput-object p2, p0, LaM/a;->d:Ljava/lang/String;

    iput-object p3, p0, LaM/a;->e:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "auth_token_encrypted"

    iget-object v2, p0, LaM/a;->c:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    const-string v1, "sid_token_encrypted"

    iget-object v2, p0, LaM/a;->d:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    const-string v1, "lsid_token_encrypted"

    iget-object v2, p0, LaM/a;->e:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    return-void
.end method

.method private x()V
    .locals 2

    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-direct {p0}, LaM/a;->z()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->c:Ljava/lang/String;

    invoke-direct {p0}, LaM/a;->A()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->d:Ljava/lang/String;

    invoke-direct {p0}, LaM/a;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->e:Ljava/lang/String;

    return-void
.end method

.method private y()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    const-string v1, "login_helper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, v0, v2}, LaM/a;->a(Landroid/content/SharedPreferences;Z)V

    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    const-string v1, "ids"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LaM/a;->a(Landroid/content/SharedPreferences;Z)V

    return-void
.end method

.method private z()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "auth_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, LaM/a;->x()V

    invoke-super {p0}, LaM/f;->a()V

    return-void
.end method

.method public a(I)V
    .locals 3

    if-gez p1, :cond_0

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_1
    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, LaM/c;

    invoke-direct {v2, p0}, LaM/c;-><init>(LaM/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, LaM/a;->h()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LaM/a;->p()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(LaM/g;Z)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0}, LaM/a;->b(Z)V

    invoke-virtual {p0, p1}, LaM/a;->d(LaM/g;)V

    if-nez p2, :cond_0

    :goto_0
    invoke-direct {p0, v0, v1}, LaM/a;->a(ZZ)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, LaM/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    invoke-direct {p0, p1}, LaM/a;->c(Z)V

    return-void
.end method

.method protected b()Z
    .locals 1

    invoke-virtual {p0}, LaM/a;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaM/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaM/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected f()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, LaM/a;->C()V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->b(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, LaM/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, LaM/a;->a(ZZ)V

    return-void
.end method

.method public h()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LaM/a;->g:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, LaM/a;->g:Z

    invoke-direct {p0, v2, v1}, LaM/a;->a(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, LaM/a;->g:Z

    goto :goto_0
.end method
