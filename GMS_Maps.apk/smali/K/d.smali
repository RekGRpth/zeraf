.class public LK/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/T;


# static fields
.field private static final a:[B


# instance fields
.field private b:Z

.field private final c:Landroid/content/Context;

.field private final d:Landroid/os/Handler;

.field private e:LK/ab;

.field private volatile f:Z

.field private g:LK/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x10000

    new-array v0, v0, [B

    sput-object v0, LK/d;->a:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;LK/ab;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, LK/d;->f:Z

    new-instance v0, LK/i;

    invoke-direct {v0}, LK/i;-><init>()V

    iput-object v0, p0, LK/d;->g:LK/i;

    iput-object p1, p0, LK/d;->c:Landroid/content/Context;

    iput-object p2, p0, LK/d;->d:Landroid/os/Handler;

    iput-object p3, p0, LK/d;->e:LK/ab;

    iget-object v2, p0, LK/d;->g:LK/i;

    monitor-enter v2

    move v0, v1

    :goto_0
    const/16 v3, 0x14

    if-gt v0, v3, :cond_0

    :try_start_0
    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->a:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "._speech_nav_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".wav"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v1, p0, LK/d;->b:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_2

    iget-object v0, p0, LK/d;->e:LK/ab;

    new-instance v1, LK/k;

    invoke-direct {v1, p0, v6}, LK/k;-><init>(LK/d;LK/e;)V

    invoke-interface {v0, v1}, LK/ab;->a(Landroid/speech/tts/UtteranceProgressListener;)I

    move-result v0

    :goto_1
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, LK/d;->f:Z

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    iget-object v0, p0, LK/d;->e:LK/ab;

    new-instance v1, LK/h;

    invoke-direct {v1, p0, v6}, LK/h;-><init>(LK/d;LK/e;)V

    invoke-interface {v0, v1}, LK/ab;->a(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    move-result v0

    goto :goto_1
.end method

.method static synthetic a(LK/d;)LK/i;
    .locals 1

    iget-object v0, p0, LK/d;->g:LK/i;

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Ljava/io/File;
    .locals 3

    if-eqz p2, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LK/d;->c:Landroid/content/Context;

    invoke-static {v1}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    const-string v1, "da_speech"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(LK/d;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LK/d;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(LK/l;)V
    .locals 2

    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, LK/d;->e()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 5

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->c:LK/l;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, LK/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, LK/l;->d()LK/f;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, LK/l;->a()V

    :cond_1
    :goto_1
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, LK/d;->g:LK/i;

    const/4 v2, 0x0

    iput-object v2, v0, LK/i;->c:LK/l;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    invoke-direct {p0}, LK/d;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    iget-object v2, p0, LK/d;->g:LK/i;

    monitor-enter v2

    :try_start_3
    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->d:LR/h;

    invoke-virtual {v0}, LK/l;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v0, v1}, LK/l;->a(LK/a;)V

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_3
    const-string v1, "x"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, LK/l;->a()V

    goto :goto_1

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public static a(Ljava/io/File;)Z
    .locals 8

    const/16 v6, 0x46

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9

    const/16 v3, 0x2c

    :try_start_1
    new-array v3, v3, [B

    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    array-length v5, v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v4, v5, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    :goto_0
    return v0

    :cond_0
    const/4 v4, 0x0

    :try_start_3
    aget-byte v4, v3, v4

    const/16 v5, 0x52

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    aget-byte v4, v3, v4

    const/16 v5, 0x49

    if-ne v4, v5, :cond_1

    const/4 v4, 0x2

    aget-byte v4, v3, v4

    if-ne v4, v6, :cond_1

    const/4 v4, 0x3

    aget-byte v4, v3, v4

    if-ne v4, v6, :cond_1

    const/16 v4, 0x8

    aget-byte v4, v3, v4

    const/16 v5, 0x57

    if-ne v4, v5, :cond_1

    const/16 v4, 0x9

    aget-byte v4, v3, v4

    const/16 v5, 0x41

    if-ne v4, v5, :cond_1

    const/16 v4, 0xa

    aget-byte v4, v3, v4

    const/16 v5, 0x56

    if-ne v4, v5, :cond_1

    const/16 v4, 0xb

    aget-byte v4, v3, v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/16 v5, 0x45

    if-eq v4, v5, :cond_2

    :cond_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v4, 0x28

    :try_start_5
    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0x29

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/16 v5, 0x2a

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/16 v5, 0x2b

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v3, v4

    if-lez v3, :cond_3

    add-int/lit8 v4, v3, 0x2c

    int-to-long v4, v4

    invoke-virtual {p0}, Ljava/io/File;->length()J
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    :cond_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_4
    :try_start_7
    sget-object v4, LK/d;->a:[B

    array-length v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sget-object v4, LK/d;->a:[B

    array-length v4, v4

    new-array v4, v4, [B

    invoke-virtual {v2, v4}, Ljava/io/FileInputStream;->read([B)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v5

    if-eq v5, v3, :cond_5

    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    goto/16 :goto_0

    :cond_5
    :try_start_9
    sget-object v3, LK/d;->a:[B

    invoke-static {v4, v3}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v3

    if-eqz v3, :cond_6

    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto/16 :goto_0

    :cond_6
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :try_start_c
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    :goto_3
    throw v0

    :catch_5
    move-exception v1

    goto/16 :goto_0

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto/16 :goto_0
.end method

.method private a(Z)Z
    .locals 6

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, LK/d;->c()Landroid/os/StatFs;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    invoke-virtual {p0}, LK/d;->d()Landroid/os/StatFs;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x80000

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic b(LK/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(LK/d;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, LK/d;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, LK/d;->g:LK/i;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->c:LK/l;

    if-eqz v0, :cond_1

    monitor-exit v4

    :cond_0
    :goto_0
    return-void

    :cond_1
    :goto_1
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/l;

    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->d:LR/h;

    invoke-virtual {v0}, LK/l;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v1, p0, LK/d;->g:LK/i;

    iput-object v0, v1, LK/i;->c:LK/l;

    move-object v2, v0

    :goto_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LK/l;

    iget-object v4, p0, LK/d;->g:LK/i;

    monitor-enter v4

    :try_start_2
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v1}, LK/l;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/a;

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, LK/l;->a(LK/a;)V

    goto :goto_3

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    if-eqz v2, :cond_0

    iget-boolean v0, p0, LK/d;->b:Z

    if-eqz v0, :cond_5

    invoke-direct {p0, v7}, LK/d;->a(Z)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "d"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    iput-boolean v6, p0, LK/d;->b:Z

    :cond_5
    iget-boolean v0, p0, LK/d;->b:Z

    if-nez v0, :cond_6

    invoke-direct {p0, v6}, LK/d;->a(Z)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "s"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LK/d;->a()V

    iput-boolean v7, p0, LK/d;->f:Z

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_4
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v0}, LR/h;->g()Ljava/lang/Object;

    :cond_7
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->a:Ljava/util/ArrayList;

    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v0, :cond_0

    iget-boolean v1, p0, LK/d;->b:Z

    invoke-direct {p0, v0, v1}, LK/d;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LK/l;->a(Ljava/lang/String;Ljava/io/File;)V

    iget-object v3, p0, LK/d;->e:LK/ab;

    invoke-virtual {v2}, LK/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v2, v0, v1, v6}, LK/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v6, v0}, LK/d;->a(ZLjava/lang/String;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_8
    move-object v2, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public a(LK/V;)LK/a;
    .locals 3

    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {p1}, LK/V;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/a;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()V
    .locals 4

    iget-boolean v0, p0, LK/d;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    iget-object v2, p0, LK/d;->g:LK/i;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, v2, LK/i;->b:Ljava/util/LinkedList;

    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->c:LK/l;

    if-eqz v2, :cond_2

    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->c:LK/l;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, LK/d;->e:LK/ab;

    invoke-interface {v2}, LK/ab;->c()I

    iget-object v2, p0, LK/d;->g:LK/i;

    const/4 v3, 0x0

    iput-object v3, v2, LK/i;->c:LK/l;

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/l;

    invoke-virtual {v0}, LK/l;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(LK/V;LK/U;)V
    .locals 2

    iget-boolean v0, p0, LK/d;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, LK/U;->a(LK/V;LK/a;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, LK/d;->a(LK/V;)LK/a;

    move-result-object v0

    check-cast v0, LK/f;

    if-eqz v0, :cond_2

    const-string v1, "h"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-interface {p2, p1, v0}, LK/U;->a(LK/V;LK/a;)V

    goto :goto_0

    :cond_2
    const-string v0, "m"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    new-instance v0, LK/l;

    invoke-direct {v0, p0, p1, p2}, LK/l;-><init>(LK/d;LK/V;LK/U;)V

    invoke-direct {p0, v0}, LK/d;->a(LK/l;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, LK/d;->e:LK/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, LK/d;->e:LK/ab;

    invoke-interface {v0}, LK/ab;->c()I

    iget-object v0, p0, LK/d;->e:LK/ab;

    invoke-interface {v0}, LK/ab;->a()V

    :cond_0
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected c()Landroid/os/StatFs;
    .locals 2

    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    invoke-static {v0}, LJ/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected d()Landroid/os/StatFs;
    .locals 2

    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, LK/d;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
