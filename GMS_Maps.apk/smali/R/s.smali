.class public LR/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;)LR/u;
    .locals 3

    new-instance v0, LR/u;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LR/u;-><init>(Landroid/content/Context;Ljava/lang/String;ZLR/t;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)LR/u;
    .locals 3

    new-instance v0, LR/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LR/u;-><init>(Landroid/content/Context;Ljava/lang/String;ZLR/t;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->c(Ljava/lang/String;Ljava/lang/String;)LR/u;

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
