.class public LR/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# instance fields
.field private volatile a:I

.field private final b:Law/h;


# direct methods
.method public constructor <init>(Law/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LR/l;->a:I

    iput-object p1, p0, LR/l;->b:Law/h;

    return-void
.end method

.method private a()V
    .locals 7

    new-instance v0, Ll/i;

    iget v1, p0, LR/l;->a:I

    iget-object v2, p0, LR/l;->b:Law/h;

    invoke-virtual {v2}, Law/h;->r()I

    move-result v2

    iget-object v3, p0, LR/l;->b:Law/h;

    invoke-virtual {v3}, Law/h;->s()I

    move-result v3

    iget-object v4, p0, LR/l;->b:Law/h;

    invoke-virtual {v4}, Law/h;->o()J

    move-result-wide v4

    iget-object v6, p0, LR/l;->b:Law/h;

    invoke-virtual {v6}, Law/h;->t()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ll/i;-><init>(IIIJI)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/String;)V
    .locals 1

    new-instance v0, Ll/h;

    invoke-direct {v0, p1, p2}, Ll/h;-><init>(IZ)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-direct {p0}, LR/l;->a()V

    return-void
.end method

.method public a(Law/g;)V
    .locals 2

    new-instance v0, Ll/g;

    const-string v1, "onComplete"

    invoke-direct {v0, v1, p1}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget v0, p0, LR/l;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LR/l;->a:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    invoke-direct {p0}, LR/l;->a()V

    :cond_0
    return-void
.end method

.method public b(Law/g;)V
    .locals 0

    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method
