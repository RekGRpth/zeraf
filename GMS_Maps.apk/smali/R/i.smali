.class public LR/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:LR/k;


# direct methods
.method public constructor <init>(LR/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LR/i;->a:LR/k;

    return-void
.end method


# virtual methods
.method public a()LR/j;
    .locals 3

    new-instance v0, LR/j;

    iget-object v1, p0, LR/i;->a:LR/k;

    iget-object v1, v1, LR/k;->c:Ljava/lang/Object;

    iget-object v2, p0, LR/i;->a:LR/k;

    iget-object v2, v2, LR/k;->d:Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, LR/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, LR/i;->a:LR/k;

    iget-object v1, v1, LR/k;->b:LR/k;

    iput-object v1, p0, LR/i;->a:LR/k;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, LR/i;->a:LR/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LR/i;->a()LR/j;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
