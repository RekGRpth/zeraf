.class public Lq/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lr/z;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lr/z;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lq/e;->a:Lr/z;

    return-void
.end method

.method static synthetic a(F)F
    .locals 1

    invoke-static {p0}, Lq/e;->b(F)F

    move-result v0

    return v0
.end method

.method private a(Lo/aL;Lq/f;I)Lq/f;
    .locals 10

    const/high16 v9, 0x42340000

    const/4 v2, 0x0

    invoke-virtual {p2}, Lq/f;->a()Lo/T;

    move-result-object v3

    invoke-virtual {p2}, Lq/f;->b()F

    move-result v4

    mul-int v0, p3, p3

    int-to-float v5, v0

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lo/aL;->j()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p1, v1}, Lo/aL;->a(I)Lo/n;

    move-result-object v0

    instance-of v6, v0, Lo/af;

    if-eqz v6, :cond_0

    check-cast v0, Lo/af;

    iget-object v6, p2, Lq/f;->a:Lo/af;

    if-ne v0, v6, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v6

    iget-object v7, p2, Lq/f;->a:Lo/af;

    invoke-virtual {v7}, Lo/af;->f()I

    move-result v7

    invoke-virtual {v0}, Lo/af;->f()I

    move-result v8

    if-ne v7, v8, :cond_0

    invoke-virtual {p2, v0}, Lq/f;->a(Lo/af;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v3, v7}, Lo/T;->d(Lo/T;)F

    move-result v7

    cmpg-float v7, v7, v5

    if-gez v7, :cond_2

    invoke-virtual {v6, v2}, Lo/X;->d(I)F

    move-result v7

    invoke-static {v4, v7}, Lo/V;->a(FF)F

    move-result v7

    cmpg-float v7, v7, v9

    if-gez v7, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p2, Lq/f;->b:Z

    iput-object v0, p2, Lq/f;->a:Lo/af;

    :goto_2
    return-object p2

    :cond_2
    invoke-virtual {v6}, Lo/X;->c()Lo/T;

    move-result-object v7

    invoke-virtual {v3, v7}, Lo/T;->d(Lo/T;)F

    move-result v7

    cmpg-float v7, v7, v5

    if-gez v7, :cond_0

    invoke-virtual {v6}, Lo/X;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v6, v7}, Lo/X;->d(I)F

    move-result v6

    invoke-static {v6}, Lq/e;->b(F)F

    move-result v6

    invoke-static {v4, v6}, Lo/V;->a(FF)F

    move-result v6

    cmpg-float v6, v6, v9

    if-gez v6, :cond_0

    goto :goto_1

    :cond_3
    const/4 p2, 0x0

    goto :goto_2
.end method

.method private a(Lq/f;I)Lq/f;
    .locals 5

    invoke-virtual {p1}, Lq/f;->a()Lo/T;

    move-result-object v0

    invoke-static {v0, p2}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    const/4 v4, 0x1

    invoke-interface {v3, v0, v4}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lo/aL;

    invoke-direct {p0, v0, p1, p2}, Lq/e;->a(Lo/aL;Lq/f;I)Lq/f;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lo/aL;Lo/h;ZLq/d;)V
    .locals 16

    new-instance v15, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v13, v1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lo/aL;->j()I

    move-result v1

    if-ge v13, v1, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lo/aL;->a(I)Lo/n;

    move-result-object v1

    instance-of v2, v1, Lo/af;

    if-eqz v2, :cond_2

    check-cast v1, Lo/af;

    invoke-virtual {v1}, Lo/af;->b()Lo/X;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v15}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/X;

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Lq/d;->a(Lo/af;Lo/X;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_0
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    :cond_1
    :goto_2
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_1

    instance-of v2, v1, Lo/M;

    if-eqz v2, :cond_1

    move-object v12, v1

    check-cast v12, Lo/M;

    invoke-virtual {v12}, Lo/M;->b()Lo/X;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v15}, Lo/h;->a(Lo/X;Ljava/util/ArrayList;)V

    const/4 v1, 0x0

    new-array v11, v1, [I

    const/4 v1, 0x0

    move v14, v1

    :goto_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v14, v1, :cond_3

    new-instance v1, Lo/af;

    const/4 v2, 0x0

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lo/X;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/16 v8, 0x10

    invoke-virtual {v12}, Lo/M;->i()I

    move-result v9

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v11}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/X;

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Lq/d;->a(Lo/af;Lo/X;)V

    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :cond_4
    return-void
.end method

.method private static b(F)F
    .locals 2

    const/high16 v1, 0x43340000

    cmpl-float v0, p0, v1

    if-lez v0, :cond_0

    sub-float v0, p0, v1

    :goto_0
    return v0

    :cond_0
    add-float v0, p0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/af;FFI)Ljava/util/List;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lq/f;

    invoke-direct {v0, p1, p2}, Lq/f;-><init>(Lo/af;F)V

    :goto_0
    if-eqz v0, :cond_0

    const/4 v2, 0x0

    cmpl-float v2, p3, v2

    if-lez v2, :cond_0

    iget-object v2, v0, Lq/f;->a:Lo/af;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lq/f;->a:Lo/af;

    invoke-virtual {v2}, Lo/af;->b()Lo/X;

    move-result-object v2

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    sub-float/2addr p3, v2

    invoke-direct {p0, v0, p4}, Lq/e;->a(Lq/f;I)Lq/f;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public a(Lo/ad;ZJ)Lq/d;
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-static {p1}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v2

    move v3, v0

    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v7, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-interface {v7, v0, v2}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, -0x1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-lez v3, :cond_5

    new-instance v7, Ls/a;

    invoke-direct {v7, v3}, Ls/a;-><init>(I)V

    move v1, v2

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-interface {v3, v0, v7}, Lr/z;->a(Lo/aq;Ls/e;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_7

    invoke-virtual {v7}, Ls/a;->b()V

    :cond_4
    invoke-virtual {v7}, Ls/a;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iput-object v6, p0, Lq/e;->b:Ljava/util/List;

    new-instance v1, Lq/d;

    invoke-direct {v1}, Lq/d;-><init>()V

    new-instance v3, Lo/h;

    invoke-direct {v3, p1}, Lo/h;-><init>(Lo/ae;)V

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    instance-of v4, v0, Lo/aL;

    if-eqz v4, :cond_6

    check-cast v0, Lo/aL;

    invoke-direct {p0, v0, v3, p2, v1}, Lq/e;->a(Lo/aL;Lo/h;ZLq/d;)V

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    invoke-virtual {v7, p3, p4}, Ls/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v4

    :goto_3
    return-object v0

    :cond_8
    move-object v0, v1

    goto :goto_3
.end method
