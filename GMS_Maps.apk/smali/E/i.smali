.class public LE/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LE/k;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/Buffer;

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field i:I

.field protected j:Li/h;

.field private k:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    const/16 v0, 0x140c

    iput v0, p0, LE/i;->f:I

    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    const/4 v0, 0x1

    iput v0, p0, LE/i;->h:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/i;-><init>(IZ)V

    return-void
.end method

.method protected constructor <init>(IIIZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    iput-boolean p4, p0, LE/i;->k:Z

    iput p1, p0, LE/i;->c:I

    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "glNativeType must be one of GL_FIXED, GL_SHORT or GL_BYTE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    :goto_0
    iput p2, p0, LE/i;->f:I

    iput p3, p0, LE/i;->h:I

    invoke-direct {p0}, LE/i;->d()V

    return-void

    :sswitch_1
    const/4 v0, 0x2

    iput v0, p0, LE/i;->g:I

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x1

    iput v0, p0, LE/i;->g:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1400 -> :sswitch_2
        0x1401 -> :sswitch_2
        0x1402 -> :sswitch_1
        0x140c -> :sswitch_0
    .end sparse-switch
.end method

.method public constructor <init>(IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LE/i;->a:I

    iput-boolean p2, p0, LE/i;->k:Z

    iput p1, p0, LE/i;->c:I

    const/16 v0, 0x140c

    iput v0, p0, LE/i;->f:I

    const/4 v0, 0x4

    iput v0, p0, LE/i;->g:I

    const/4 v0, 0x1

    iput v0, p0, LE/i;->h:I

    invoke-direct {p0}, LE/i;->d()V

    return-void
.end method

.method private d()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, LE/i;->i:I

    iget-object v0, p0, LE/i;->b:[I

    if-nez v0, :cond_3

    iget v0, p0, LE/i;->c:I

    mul-int/lit8 v0, v0, 0x2

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    iget-boolean v1, p0, LE/i;->k:Z

    if-eqz v1, :cond_2

    :cond_0
    new-array v0, v0, [I

    iput-object v0, p0, LE/i;->b:[I

    :cond_1
    :goto_0
    iput v2, p0, LE/i;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    return-void

    :cond_2
    new-instance v1, Li/h;

    invoke-direct {v1, v0}, Li/h;-><init>(I)V

    iput-object v1, p0, LE/i;->j:Li/h;

    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->a()V

    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 2

    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/i;->j:Li/h;

    iget v1, p0, LE/i;->i:I

    invoke-virtual {v0, v1}, Li/h;->b(I)V

    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/i;->b:[I

    iget-object v0, p0, LE/i;->j:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/i;->i:I

    :cond_0
    return-void
.end method

.method public a(FF)V
    .locals 4

    const/high16 v3, 0x47800000

    iget v0, p0, LE/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/i;->d:I

    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    mul-float v2, p1, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    mul-float v2, p2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v1

    iget v0, p0, LE/i;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LE/i;->a()V

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 3

    iget v0, p0, LE/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/i;->d:I

    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    aput p1, v0, v1

    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/i;->i:I

    aput p2, v0, v1

    iget v0, p0, LE/i;->i:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, LE/i;->a()V

    :cond_0
    return-void
.end method

.method public a(III)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    iget-object v1, p0, LE/i;->b:[I

    iget v2, p0, LE/i;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LE/i;->i:I

    aput p1, v1, v2

    iget-object v1, p0, LE/i;->b:[I

    iget v2, p0, LE/i;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LE/i;->i:I

    aput p2, v1, v2

    iget v1, p0, LE/i;->i:I

    const/16 v2, 0x400

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, LE/i;->a()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, LE/i;->d:I

    add-int/2addr v0, p3

    iput v0, p0, LE/i;->d:I

    return-void
.end method

.method public a(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, LE/i;->b(LD/a;)V

    invoke-direct {p0}, LE/i;->d()V

    return-void
.end method

.method public a(LD/a;I)V
    .locals 5

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LE/i;->e(LD/a;)V

    :cond_0
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    mul-int/lit8 v1, p2, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    sub-int/2addr v0, p2

    iget v1, p0, LE/i;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, LE/i;->a:I

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, p0, LE/i;->f:I

    const/4 v3, 0x0

    iget-object v4, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    return-void
.end method

.method protected a(Ljava/nio/ByteBuffer;I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v1, p0, LE/i;->b:[I

    aget v1, v1, v0

    iget v2, p0, LE/i;->h:I

    div-int/2addr v1, v2

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Ljava/nio/ShortBuffer;I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v1, p0, LE/i;->b:[I

    aget v1, v1, v0

    iget v2, p0, LE/i;->h:I

    div-int/2addr v1, v2

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a([I)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LE/i;->a([III)V

    return-void
.end method

.method public a([III)V
    .locals 4

    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_0

    iget v0, p0, LE/i;->i:I

    add-int/2addr v0, p3

    const/16 v1, 0x400

    if-ge v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, LE/i;->b:[I

    iget v1, p0, LE/i;->i:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, LE/i;->i:I

    add-int/2addr v0, p3

    iput v0, p0, LE/i;->i:I

    :cond_1
    iget v0, p0, LE/i;->d:I

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    iput v0, p0, LE/i;->d:I

    return-void

    :cond_2
    add-int v0, p2, p3

    :goto_0
    if-ge p2, v0, :cond_1

    sub-int v1, v0, p2

    iget v2, p0, LE/i;->i:I

    rsub-int v2, v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-static {p1, p2, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v1

    iget v2, p0, LE/i;->i:I

    add-int/2addr v1, v2

    iput v1, p0, LE/i;->i:I

    invoke-virtual {p0}, LE/i;->a()V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LE/i;->a:I

    return v0
.end method

.method public b(LD/a;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 3

    const/16 v0, 0x2c

    iget-object v1, p0, LE/i;->j:Li/h;

    if-eqz v1, :cond_2

    iget-object v1, p0, LE/i;->j:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, LE/i;->e:Ljava/nio/Buffer;

    if-eqz v1, :cond_1

    iget-object v1, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v1

    iget v2, p0, LE/i;->g:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, LE/i;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, LE/i;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(I)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, LE/i;->c:I

    if-le p1, v0, :cond_3

    iget v0, p0, LE/i;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v0, v1, 0x2

    iget-object v2, p0, LE/i;->j:Li/h;

    if-nez v2, :cond_5

    const/16 v2, 0x400

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, LE/i;->k:Z

    if-eqz v2, :cond_4

    :cond_0
    iget-boolean v2, p0, LE/i;->k:Z

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-array v0, v0, [I

    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, LE/i;->b:[I

    :goto_0
    iput v1, p0, LE/i;->c:I

    :cond_3
    return-void

    :cond_4
    new-instance v2, Li/h;

    invoke-direct {v2, v0}, Li/h;-><init>(I)V

    iput-object v2, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v2, p0, LE/i;->b:[I

    iget v3, p0, LE/i;->i:I

    invoke-virtual {v0, v2, v3}, Li/h;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, LE/i;->j:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/i;->b:[I

    iget-object v0, p0, LE/i;->j:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/i;->i:I

    goto :goto_0

    :cond_5
    iget-object v2, p0, LE/i;->j:Li/h;

    invoke-virtual {v2, v0}, Li/h;->c(I)V

    goto :goto_0
.end method

.method public c(LD/a;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, LE/i;->b(LD/a;)V

    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    iput-object v1, p0, LE/i;->j:Li/h;

    :cond_0
    iput-object v1, p0, LE/i;->b:[I

    return-void
.end method

.method public d(LD/a;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LE/i;->e(LD/a;)V

    :cond_0
    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    iget v1, p0, LE/i;->g:I

    mul-int/2addr v0, v1

    iput v0, p0, LE/i;->a:I

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, p0, LE/i;->f:I

    iget-object v3, p0, LE/i;->e:Ljava/nio/Buffer;

    invoke-interface {v0, v1, v2, v4, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    return-void
.end method

.method protected e(LD/a;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, LE/i;->d:I

    mul-int/lit8 v1, v0, 0x2

    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v0

    iget v2, p0, LE/i;->g:I

    mul-int/2addr v2, v1

    invoke-virtual {v0, v2}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1402

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_1

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-virtual {p0, v0, v1}, LE/i;->a(Ljava/nio/ShortBuffer;I)V

    :goto_0
    iget-object v0, p0, LE/i;->j:Li/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LE/i;->j:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    iput-object v4, p0, LE/i;->j:Li/h;

    :cond_0
    iput-object v4, p0, LE/i;->b:[I

    return-void

    :cond_1
    invoke-virtual {p0}, LE/i;->a()V

    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    iget v2, p0, LE/i;->h:I

    invoke-virtual {v1, v0, v2}, Li/h;->a(Ljava/nio/ShortBuffer;I)V

    goto :goto_0

    :cond_2
    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1400

    if-eq v2, v3, :cond_3

    iget v2, p0, LE/i;->f:I

    const/16 v3, 0x1401

    if-ne v2, v3, :cond_5

    :cond_3
    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_4

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0, v1}, LE/i;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, LE/i;->a()V

    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    iget v2, p0, LE/i;->h:I

    invoke-virtual {v1, v0, v2}, Li/h;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    iget-object v0, p0, LE/i;->j:Li/h;

    if-nez v0, :cond_6

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/i;->b:[I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, LE/i;->a()V

    iget-object v1, p0, LE/i;->j:Li/h;

    iget-object v0, p0, LE/i;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0}, Li/h;->a(Ljava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public g()I
    .locals 1

    iget v0, p0, LE/i;->d:I

    return v0
.end method
