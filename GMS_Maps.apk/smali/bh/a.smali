.class public Lbh/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lbf/i;

.field protected b:Lcom/google/googlenav/ui/bi;


# direct methods
.method public constructor <init>(Lbf/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbh/a;->a:Lbf/i;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lbh/a;->b:Lcom/google/googlenav/ui/bi;

    return-void
.end method

.method private static c(Lcom/google/googlenav/E;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(ZBI)C
    .locals 1

    const/16 v0, 0x20

    return v0
.end method

.method public a(Lcom/google/googlenav/E;)I
    .locals 2

    iget-object v0, p0, Lbh/a;->b:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v0

    return v0
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(I)Z
    .locals 4

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbh/a;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v2

    invoke-virtual {v2}, Ln/s;->e()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v3

    invoke-virtual {v3, v0}, Ln/s;->d(Lo/D;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 2

    iget-object v0, p0, Lbh/a;->b:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v0

    return v0
.end method

.method public b(I)Lcom/google/googlenav/e;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lbh/a;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/googlenav/E;->c()B

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {v0}, Lbh/a;->c(Lcom/google/googlenav/E;)Z

    move-result v2

    if-eqz v2, :cond_4

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v1

    :cond_2
    iget-object v2, p0, Lbh/a;->b:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v0

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/ui/bi;->a(BLam/f;)Lcom/google/googlenav/e;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_0

    :cond_4
    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-nez v1, :cond_5

    move-object v1, v0

    check-cast v1, Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->af()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 p1, -0x1

    :cond_5
    const/4 v1, 0x0

    invoke-interface {v0}, Lcom/google/googlenav/E;->c()B

    move-result v2

    invoke-virtual {p0, v1, v2, p1}, Lbh/a;->a(ZBI)C

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    invoke-interface {v0}, Lcom/google/googlenav/E;->c()B

    move-result v0

    :goto_1
    iget-object v2, p0, Lbh/a;->b:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/ui/bi;->a(BC)Lcom/google/googlenav/e;

    move-result-object v1

    goto :goto_0

    :cond_6
    invoke-static {v1}, Lcom/google/googlenav/ui/aO;->j(C)B

    move-result v0

    goto :goto_1
.end method
