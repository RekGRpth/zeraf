.class public Lbh/i;
.super Lbh/a;
.source "SourceFile"


# static fields
.field private static final g:[C


# instance fields
.field private c:B

.field private d:I

.field private e:[Lcom/google/googlenav/e;

.field private f:[Lcom/google/googlenav/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lbh/i;->g:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x10s
        0x11s
        0x12s
        0x13s
    .end array-data
.end method

.method public constructor <init>(Lbf/i;)V
    .locals 1

    invoke-direct {p0, p1}, Lbh/a;-><init>(Lbf/i;)V

    const/16 v0, 0x12

    iput-byte v0, p0, Lbh/i;->c:B

    sget-object v0, Lbh/i;->g:[C

    array-length v0, v0

    new-array v0, v0, [Lcom/google/googlenav/e;

    iput-object v0, p0, Lbh/i;->e:[Lcom/google/googlenav/e;

    sget-object v0, Lbh/i;->g:[C

    array-length v0, v0

    new-array v0, v0, [Lcom/google/googlenav/e;

    iput-object v0, p0, Lbh/i;->f:[Lcom/google/googlenav/e;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbh/i;->c(I)V

    return-void
.end method

.method private a(IZZ)Lcom/google/googlenav/e;
    .locals 7

    iget-object v0, p0, Lbh/i;->b:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->i()Lam/g;

    move-result-object v6

    invoke-static {p1}, Lbh/i;->d(I)I

    const/4 v1, 0x0

    const/4 v2, -0x1

    iget v3, p0, Lbh/i;->d:I

    move v0, p2

    move v4, p1

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/aO;->a(ZZIIIZ)C

    move-result v0

    invoke-interface {v6, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/e;->a(Lam/f;)Lcom/google/googlenav/e;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/e;->a(Z)V

    return-object v0
.end method

.method private c()V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lbh/i;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ax()Z

    move-result v2

    sget-object v3, Lbh/i;->g:[C

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-char v5, v3, v0

    invoke-static {v5}, Lbh/i;->d(I)I

    move-result v6

    iget-object v7, p0, Lbh/i;->e:[Lcom/google/googlenav/e;

    invoke-direct {p0, v5, v2, v1}, Lbh/i;->a(IZZ)Lcom/google/googlenav/e;

    move-result-object v8

    aput-object v8, v7, v6

    const/4 v7, 0x1

    iget-object v8, p0, Lbh/i;->f:[Lcom/google/googlenav/e;

    invoke-direct {p0, v5, v2, v7}, Lbh/i;->a(IZZ)Lcom/google/googlenav/e;

    move-result-object v5

    aput-object v5, v8, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static d(I)I
    .locals 2

    const/16 v0, 0x12

    const/16 v1, 0x10

    if-eq p0, v1, :cond_0

    const/16 v1, 0x11

    if-eq p0, v1, :cond_0

    if-eq p0, v0, :cond_0

    const/16 v1, 0x13

    if-eq p0, v1, :cond_0

    move p0, v0

    :cond_0
    add-int/lit8 v0, p0, -0x10

    return v0
.end method


# virtual methods
.method public a(ZBI)C
    .locals 6

    iget-object v0, p0, Lbh/i;->a:Lbf/i;

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    invoke-virtual {v1}, Lbf/bk;->ax()Z

    move-result v0

    invoke-virtual {v1}, Lbf/bk;->d()I

    move-result v3

    invoke-virtual {p0, p3}, Lbh/i;->a(I)Z

    move-result v5

    move v1, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/aO;->a(ZZIIIZ)C

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/aZ;->f(I)I

    move-result v2

    goto :goto_0
.end method

.method public a()Z
    .locals 6

    const/16 v5, 0x13

    const/16 v4, 0xf

    const/16 v3, 0xc

    invoke-super {p0}, Lbh/a;->a()Z

    move-result v0

    iget-byte v1, p0, Lbh/i;->c:B

    iget-object v2, p0, Lbh/i;->a:Lbf/i;

    invoke-virtual {v2}, Lbf/i;->aw()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    if-ge v2, v3, :cond_1

    const/16 v2, 0x10

    iput-byte v2, p0, Lbh/i;->c:B

    :goto_0
    if-nez v0, :cond_0

    iget-byte v0, p0, Lbh/i;->c:B

    if-eq v1, v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    if-lt v2, v3, :cond_2

    if-ge v2, v4, :cond_2

    const/16 v2, 0x11

    iput-byte v2, p0, Lbh/i;->c:B

    goto :goto_0

    :cond_2
    if-lt v2, v4, :cond_3

    if-ge v2, v5, :cond_3

    const/16 v2, 0x12

    iput-byte v2, p0, Lbh/i;->c:B

    goto :goto_0

    :cond_3
    iput-byte v5, p0, Lbh/i;->c:B

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()B
    .locals 1

    iget-byte v0, p0, Lbh/i;->c:B

    return v0
.end method

.method public b(I)Lcom/google/googlenav/e;
    .locals 2

    iget-object v0, p0, Lbh/i;->a:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lbh/a;->b(I)Lcom/google/googlenav/e;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lbh/i;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbh/i;->f:[Lcom/google/googlenav/e;

    :goto_1
    iget-byte v1, p0, Lbh/i;->c:B

    invoke-static {v1}, Lbh/i;->d(I)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbh/i;->e:[Lcom/google/googlenav/e;

    goto :goto_1
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lbh/i;->d:I

    invoke-direct {p0}, Lbh/i;->c()V

    return-void
.end method
