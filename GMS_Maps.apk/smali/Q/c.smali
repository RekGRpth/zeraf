.class public LQ/c;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:LC/b;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/s;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/c;->d:Z

    return-void
.end method


# virtual methods
.method protected a(FF)V
    .locals 0

    invoke-virtual {p0}, LQ/c;->ah()V

    return-void
.end method

.method protected a(FFF)V
    .locals 0

    invoke-virtual {p0}, LQ/c;->ah()V

    return-void
.end method

.method protected a(LO/N;LO/N;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0, p2}, LQ/c;->a(LO/N;)V

    :cond_1
    return-void
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .locals 1

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    return-void
.end method

.method protected a(Z)V
    .locals 2

    iget-boolean v0, p0, LQ/c;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/c;->d:Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    new-array v2, v2, [LO/z;

    const/4 v3, 0x0

    iget-object v4, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    :cond_0
    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/c;->a(LO/N;)V

    invoke-virtual {p0}, LQ/c;->ah()V

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->f(Z)V

    return-void
.end method

.method protected g()V
    .locals 1

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/c;->d:Z

    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    iput-object v0, p0, LQ/c;->c:LC/b;

    return-void
.end method

.method public q()V
    .locals 2

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/c;->c:LC/b;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LC/c;)V

    return-void
.end method

.method protected r()V
    .locals 3

    iget-object v0, p0, LQ/c;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    iget-object v0, p0, LQ/c;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method protected s()V
    .locals 3

    iget-object v0, p0, LQ/c;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method protected t()V
    .locals 3

    iget-object v0, p0, LQ/c;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method
