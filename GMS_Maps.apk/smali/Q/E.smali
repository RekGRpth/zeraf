.class public abstract LQ/E;
.super LQ/s;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LQ/s;-><init>()V

    return-void
.end method


# virtual methods
.method protected B()V
    .locals 0

    return-void
.end method

.method public F()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected L()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(FF)V
    .locals 0

    return-void
.end method

.method protected a(FFF)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .locals 1

    const v0, 0x7f100119

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 0

    return-void
.end method

.method protected a(Lo/T;)V
    .locals 0

    return-void
.end method

.method protected a(Z)V
    .locals 4

    iget-object v0, p0, LQ/E;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/E;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    const/high16 v2, -0x40800000

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;FZ)V

    return-void
.end method

.method protected am()V
    .locals 0

    return-void
.end method

.method protected d(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 0

    return-void
.end method

.method protected u()V
    .locals 3

    iget-object v0, p0, LQ/E;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method protected x()V
    .locals 2

    iget-object v0, p0, LQ/E;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    return-void
.end method
