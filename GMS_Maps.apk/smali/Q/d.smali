.class public LQ/d;
.super LQ/f;
.source "SourceFile"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LQ/f;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/d;->c:Z

    return-void
.end method

.method private b(LO/N;)LO/N;
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private c(LO/N;)V
    .locals 2

    const/16 v1, 0x10

    if-eqz p1, :cond_2

    invoke-virtual {p1}, LO/N;->b()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, LO/N;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, LO/N;->b()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, LO/N;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->B()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->C()V

    goto :goto_0
.end method

.method private c(LO/N;Z)V
    .locals 2

    invoke-virtual {p0, p1}, LQ/d;->a(LO/N;)V

    invoke-virtual {p0}, LQ/d;->ah()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    invoke-direct {p0, p1}, LQ/d;->c(LO/N;)V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->setNavigationImageStep(LO/N;)V

    return-void
.end method


# virtual methods
.method protected a(FF)V
    .locals 0

    return-void
.end method

.method protected a(LO/N;LO/N;)V
    .locals 1

    iget-boolean v0, p0, LQ/d;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, LQ/d;->c(LO/N;Z)V

    goto :goto_0
.end method

.method protected a(LO/N;Z)V
    .locals 2

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, LQ/d;->c(LO/N;Z)V

    invoke-virtual {p0, v1}, LQ/d;->b_(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/maps/driveabout/app/cq;->a(LO/N;ZZ)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->A()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    invoke-direct {p0, v0}, LQ/d;->b(LO/N;)LO/N;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setNavigationImageStep(LO/N;)V

    const-string v2, "w"

    invoke-virtual {v1}, LO/N;->b()I

    move-result v0

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    :cond_0
    invoke-direct {p0, v1}, LQ/d;->c(LO/N;)V

    invoke-virtual {p0}, LQ/d;->ah()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b_(Z)V
    .locals 0

    iput-boolean p1, p0, LQ/d;->c:Z

    return-void
.end method

.method public e()V
    .locals 2

    invoke-super {p0}, LQ/f;->e()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->C()V

    iget-object v0, p0, LQ/d;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setNavigationImageStep(LO/N;)V

    return-void
.end method
