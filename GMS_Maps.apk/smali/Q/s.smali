.class public abstract LQ/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:I

.field private static final e:[LQ/s;


# instance fields
.field protected a:LQ/p;

.field private c:LQ/x;

.field private d:I

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, LQ/x;->values()[LQ/x;

    move-result-object v0

    array-length v0, v0

    sput v0, LQ/s;->b:I

    sget v0, LQ/s;->b:I

    new-array v0, v0, [LQ/s;

    sput-object v0, LQ/s;->e:[LQ/s;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/s;->f:Z

    return-void
.end method

.method static synthetic a(LQ/s;LQ/x;)LQ/x;
    .locals 0

    iput-object p1, p0, LQ/s;->c:LQ/x;

    return-object p1
.end method

.method private a(LO/U;I[LO/b;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;I[LO/b;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "UserRequestedReroute"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(LQ/s;Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LQ/s;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    return-void
.end method

.method private final a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/eG;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/eG;-><init>(Lcom/google/android/maps/driveabout/app/cQ;)V

    new-instance v1, LQ/v;

    invoke-direct {v1, p0, p2}, LQ/v;-><init>(LQ/s;Lcom/google/android/maps/driveabout/app/da;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/eG;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/eP;)V

    return-void
.end method

.method private aA()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LQ/s;->v()V

    goto :goto_0
.end method

.method private aB()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0068

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0069

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, "__route_status"

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setStatusBarContent(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, LQ/s;->w()V

    goto :goto_0
.end method

.method private aC()Z
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v0

    const/16 v1, 0x1324

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aD()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    return-void
.end method

.method private final aE()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->j()V

    return-void
.end method

.method private ap()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->invalidateOptionsMenu()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "UIState"

    const-string v1, "NavigationApplicationDelegate.getInstance() should never be null."

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aq()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->a()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZZ)V

    return-void
.end method

.method private ar()V
    .locals 2

    iget v0, p0, LQ/s;->d:I

    if-lez v0, :cond_0

    const-string v0, "z"

    iget v1, p0, LQ/s;->d:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    const/4 v0, 0x0

    iput v0, p0, LQ/s;->d:I

    :cond_0
    return-void
.end method

.method private as()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v2, :cond_0

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    :cond_0
    invoke-static {v1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->t()V

    :cond_1
    :goto_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->i()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V

    return-void

    :cond_2
    invoke-static {v1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LaH/h;->k()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->u()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->s()I

    move-result v0

    const-string v1, "T"

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto/16 :goto_0
.end method

.method private at()V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0}, LQ/s;->af()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v1

    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v3

    invoke-virtual {v2, v3}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v2

    invoke-virtual {v2, v1}, LaH/j;->b(F)LaH/j;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    :goto_0
    invoke-virtual {p0}, LQ/s;->ag()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setCurrentRoadName(Landroid/location/Location;)V

    return-void

    :cond_0
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, LaH/h;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private au()I
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LQ/s;->f()I

    move-result v0

    goto :goto_0
.end method

.method private av()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->G()V

    :goto_0
    invoke-virtual {p0}, LQ/s;->x()V

    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d0095

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->m()LO/U;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v3

    invoke-virtual {v3, v0}, LaH/h;->b(Lo/u;)F

    move-result v0

    const/high16 v3, 0x42700000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v4

    invoke-virtual {v4}, LO/U;->k()Z

    move-result v5

    const/4 v3, 0x0

    if-eqz v5, :cond_2

    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v5

    invoke-virtual {v5}, Lbl/h;->d()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v4}, LO/U;->h()Lbl/h;

    move-result-object v3

    invoke-virtual {v3}, Lbl/h;->b()Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v5, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v5

    invoke-virtual {v4}, LO/U;->i()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v0, v3, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    iput-boolean v1, p0, LQ/s;->f:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, LQ/s;->u()V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method private aw()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    invoke-virtual {v1}, LO/V;->a()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ax()V
    .locals 0

    invoke-virtual {p0}, LQ/s;->ai()V

    return-void
.end method

.method private ay()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->d()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->j()V

    goto :goto_0
.end method

.method private az()V
    .locals 4

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->p()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cQ;->a(IIZ)V

    return-void
.end method

.method private j(Z)V
    .locals 4

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-direct {p0}, LQ/s;->au()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cq;->a(ILcom/google/android/maps/driveabout/vector/q;I)V

    invoke-virtual {p0, p1}, LQ/s;->a(Z)V

    return-void
.end method


# virtual methods
.method protected A()V
    .locals 0

    return-void
.end method

.method protected B()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->h(Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->x()V

    :cond_0
    return-void
.end method

.method public C()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->f:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->C()V

    :cond_0
    return-void
.end method

.method public D()V
    .locals 0

    return-void
.end method

.method public E()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public F()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final G()LQ/x;
    .locals 1

    iget-object v0, p0, LQ/s;->c:LQ/x;

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LQ/s;->c:LQ/x;

    invoke-virtual {v0}, LQ/x;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()V
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-direct {p0}, LQ/s;->aq()V

    invoke-direct {p0}, LQ/s;->as()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    invoke-direct {p0}, LQ/s;->av()V

    invoke-direct {p0}, LQ/s;->ap()V

    invoke-direct {p0}, LQ/s;->aB()V

    invoke-direct {p0}, LQ/s;->ax()V

    invoke-direct {p0}, LQ/s;->ay()V

    invoke-direct {p0}, LQ/s;->az()V

    invoke-direct {p0}, LQ/s;->aA()V

    invoke-virtual {p0}, LQ/s;->m()V

    return-void
.end method

.method public J()V
    .locals 1

    invoke-direct {p0}, LQ/s;->at()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LQ/s;->av()V

    :cond_0
    return-void
.end method

.method public final K()V
    .locals 3

    invoke-virtual {p0}, LQ/s;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->c()F

    move-result v0

    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {v1, v2}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->b(F)LaH/j;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setMyLocation(Landroid/location/Location;Z)V

    :cond_0
    return-void
.end method

.method protected L()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final M()V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, LL/D;

    const-string v2, "compassButton"

    invoke-direct {v1, v2, v0}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v1}, Ll/f;->b(Ll/j;)V

    if-eqz v0, :cond_1

    const-string v0, "3"

    :goto_1
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->g()V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "2"

    goto :goto_1
.end method

.method public final N()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "zoomOut"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget v0, p0, LQ/s;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LQ/s;->d:I

    invoke-virtual {p0}, LQ/s;->i()V

    return-void
.end method

.method public final O()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "zoomIn"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget v0, p0, LQ/s;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LQ/s;->d:I

    invoke-virtual {p0}, LQ/s;->h()V

    return-void
.end method

.method public final P()Z
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "back"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-virtual {p0}, LQ/s;->j()Z

    move-result v0

    return v0
.end method

.method public final Q()V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, LL/C;

    const-string v1, "routeOverview"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "r"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->f:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_0
.end method

.method public final R()V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficOverview"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->g:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_0
.end method

.method public S()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficConfirm"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-virtual {p0}, LQ/s;->y()V

    return-void
.end method

.method public T()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficCancel"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-virtual {p0}, LQ/s;->z()V

    return-void
.end method

.method public U()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "routeAroundTrafficTimeout"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-virtual {p0}, LQ/s;->A()V

    return-void
.end method

.method public final V()V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, LL/C;

    const-string v1, "viewList"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "l"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->h:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_0
.end method

.method public final W()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "backToLocation"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "n"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->k()V

    return-void
.end method

.method public final X()V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "TravelMode"

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "ForceNewDestination"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x10020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final Y()V
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    const-string v0, "+"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->r()V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method public final Z()V
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    const-string v0, "-"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->B()V

    invoke-direct {p0}, LQ/s;->aD()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->g()V

    :cond_0
    return-void
.end method

.method public a()V
    .locals 1

    const-string v0, "UIState"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->b()V

    return-void
.end method

.method protected a(FF)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method protected a(FFF)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method public final a(I)V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(IIZ)V

    invoke-virtual {p0, p1}, LQ/s;->b(I)V

    return-void
.end method

.method public final a(IIFIIII)V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/cq;->a(IIF)V

    invoke-virtual {v0, p7}, Lcom/google/android/maps/driveabout/app/cq;->a(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LQ/s;->j(Z)V

    return-void
.end method

.method protected final a(LO/N;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->a(LO/N;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3}, Lcom/google/android/maps/driveabout/app/cQ;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    new-instance v2, LQ/t;

    invoke-direct {v2, p0, p1}, LQ/t;-><init>(LQ/s;LO/N;)V

    invoke-interface {v1, v0, p1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    goto :goto_0
.end method

.method protected a(LO/N;LO/N;)V
    .locals 0

    invoke-virtual {p0, p2}, LQ/s;->a(LO/N;)V

    return-void
.end method

.method protected a(LO/N;Z)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->k:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LQ/s;->a(LO/N;Z)V

    :cond_0
    return-void
.end method

.method public final a(LO/U;Z)V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/D;

    const-string v1, "newDestination"

    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LL/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    if-eqz p2, :cond_0

    const-string v0, "d"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->H()[LO/b;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LQ/s;->a(LO/U;I[LO/b;)V

    return-void
.end method

.method protected a(LO/g;)V
    .locals 3

    invoke-virtual {p0, p1}, LQ/s;->c(LO/g;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final a(LO/j;I)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-direct {p0}, LQ/s;->ar()V

    invoke-virtual {p0, p1, p2}, LQ/s;->c(LO/j;I)V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method public a(LO/z;)V
    .locals 0

    return-void
.end method

.method protected a(LO/z;[LO/z;)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [LO/z;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, p1, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    return-void
.end method

.method protected a(Landroid/os/Bundle;Lo/ae;)V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ae;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eE;)V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "search"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "?"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, LQ/s;->a(Landroid/os/Bundle;Lo/ae;)V

    invoke-interface {p3, p1}, Lcom/google/android/maps/driveabout/app/eE;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 3

    const v0, 0x7f10011a

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f020113

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d007a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d007b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .locals 2

    const v1, 0x7f100119

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    invoke-direct {p0}, LQ/s;->aD()V

    :cond_0
    return-void
.end method

.method public final a(Ll/f;)V
    .locals 4

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->d(Z)V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "da_voice-rmi.3gp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/da;

    invoke-direct {v1, v0}, Lcom/google/android/maps/driveabout/app/da;-><init>(Ljava/io/File;)V

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/app/da;->a(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/cS;)V

    if-eqz p1, :cond_1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/da;->a(Ll/f;Lcom/google/android/maps/driveabout/app/cQ;)V

    :cond_1
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v0, v1}, LQ/s;->a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v2

    new-instance v3, LQ/u;

    invoke-direct {v3, p0, v0, v1}, LQ/u;-><init>(LQ/s;Ljava/io/File;Lcom/google/android/maps/driveabout/app/da;)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_0
.end method

.method protected a(Lo/T;)V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->H()V

    invoke-virtual {p0}, LQ/s;->ah()V

    return-void
.end method

.method public final a(Lo/aR;Lcom/google/android/maps/driveabout/app/by;)V
    .locals 9

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "layers"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v1, "s"

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(Lo/aR;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->l:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->b(LQ/x;)Z

    move-result v5

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bC;->b()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bC;->c()Lcom/google/android/maps/driveabout/app/bI;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v3

    iget-object v7, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v7}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v7

    sget-object v8, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v7, v8, :cond_1

    :goto_1
    iget-object v6, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v6}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/app/cS;->i()Z

    move-result v6

    move-object v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    return-void

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v4, v6

    goto :goto_1
.end method

.method protected a(Z)V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    return-void
.end method

.method public a(LQ/r;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p0}, LQ/s;->L()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    const/4 v0, 0x0

    return v0
.end method

.method public final aa()V
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->i()V

    :cond_0
    return-void
.end method

.method ab()Z
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ac()V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0}, LQ/s;->l()V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method public final ad()V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0}, LQ/s;->t()V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method public ae()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, LQ/s;->aE()V

    const-string v0, "."

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    const v0, 0x7f0d0012

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    if-ne v1, v3, :cond_2

    const v0, 0x7f0d0010

    :cond_0
    :goto_0
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;I)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/cQ;->g(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    invoke-direct {p0}, LQ/s;->av()V

    invoke-direct {p0}, LQ/s;->aB()V

    invoke-virtual {p0}, LQ/s;->n()V

    iget-boolean v0, p0, LQ/s;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LO/U;->h()Lbl/h;

    move-result-object v0

    invoke-static {v1, v0, v4}, Lbl/a;->a(Ljava/lang/String;Lbl/h;Z)V

    :cond_1
    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dr;->b(Z)V

    return-void

    :cond_2
    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->c()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const v0, 0x7f0d0011

    goto :goto_0
.end method

.method protected af()Z
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ag()V
    .locals 0

    return-void
.end method

.method protected final ah()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, LO/N;->k()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3}, LO/N;->j()LO/N;

    move-result-object v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    iget-object v4, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->g()LQ/s;

    move-result-object v4

    invoke-virtual {v4}, LQ/s;->G()LQ/x;

    move-result-object v4

    sget-object v5, LQ/x;->m:LQ/x;

    if-eq v4, v5, :cond_2

    sget-object v5, LQ/x;->f:LQ/x;

    if-eq v4, v5, :cond_2

    sget-object v5, LQ/x;->h:LQ/x;

    if-eq v4, v5, :cond_2

    :goto_2
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(ZZZ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v3, v2

    move v0, v2

    goto :goto_1
.end method

.method protected ai()V
    .locals 7

    const/4 v0, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v2

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->E()[LO/U;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->Q()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d0047

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    :goto_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->l()Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v0

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->H()[LO/b;

    move-result-object v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/cT;->a([LO/U;I[LO/b;Ljava/lang/CharSequence;ZZ)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0068

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    goto :goto_1

    :cond_3
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->v()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0069

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, v6

    move v6, v0

    goto :goto_1

    :cond_4
    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v3

    iget-object v4, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v4

    iget-object v5, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v5

    invoke-virtual {v5}, LO/z;->q()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(ILO/N;I)Landroid/text/Spannable;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->x()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->b()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0095

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v6, v0

    move v5, v0

    goto/16 :goto_1

    :cond_6
    move v6, v0

    move v5, v0

    goto/16 :goto_1
.end method

.method public final aj()V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/C;

    const-string v1, "showStreetView"

    invoke-direct {v0, v1}, LL/C;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    invoke-virtual {p0}, LQ/s;->o()V

    return-void
.end method

.method protected ak()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->c()V

    invoke-direct {p0}, LQ/s;->ay()V

    return-void
.end method

.method public final al()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->c(Z)V

    invoke-virtual {p0}, LQ/s;->am()V

    return-void
.end method

.method protected am()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->l()V

    return-void
.end method

.method public an()V
    .locals 4

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/o;->a(Ljava/lang/String;)Lo/o;

    move-result-object v2

    iget-object v3, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lo/o;)V

    invoke-virtual {v0}, LO/U;->h()Lbl/h;

    move-result-object v2

    invoke-virtual {v0}, LO/U;->j()Lbl/c;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lbl/a;->a(Lbl/h;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public ao()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/U;)V

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public final b(FF)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0, p1, p2}, LQ/s;->a(FF)V

    return-void
.end method

.method public final b(FFF)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0, p1, p2, p3}, LQ/s;->a(FFF)V

    return-void
.end method

.method protected b(I)V
    .locals 0

    return-void
.end method

.method public final b(LO/N;LO/N;)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/N;LO/N;)V

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method public final b(LO/N;Z)V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    if-eqz p1, :cond_0

    new-instance v0, LL/D;

    const-string v1, "step"

    invoke-virtual {p1}, LO/N;->i()I

    move-result v2

    invoke-direct {v0, v1, v2}, LL/D;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "p"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/N;Z)V

    invoke-direct {p0}, LQ/s;->aD()V

    :cond_0
    return-void
.end method

.method public final b(LO/g;)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0, p1}, LQ/s;->a(LO/g;)V

    return-void
.end method

.method public final b(LO/j;I)V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0}, LQ/s;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->b(LO/j;II)V

    :cond_0
    return-void
.end method

.method public b(LO/z;[LO/z;)V
    .locals 3

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p1}, LO/z;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->i()I

    move-result v0

    invoke-virtual {p1}, LO/z;->k()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-virtual {p1, v0}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    :goto_0
    invoke-virtual {p0, p1, p2}, LQ/s;->a(LO/z;[LO/z;)V

    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(LO/N;)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->f()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/maps/driveabout/app/bS;)V
    .locals 7

    const v6, 0x7f1000f6

    const v5, 0x7f1000f3

    const v4, 0x7f1000f2

    const v3, 0x7f1004a7

    const v2, 0x7f1004a6

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f100120

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    :goto_0
    const v0, 0x7f1004a2

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a3

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    const v0, 0x7f10011e

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a4

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a5

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    :goto_1
    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004ab

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f1004aa

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a9

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004a8

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f1004ab

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f100120

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    :goto_2
    invoke-virtual {p1, v4}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    const v0, 0x7f10011e

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    const v0, 0x7f1004a4

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p1, v5}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    const v0, 0x7f1004a5

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p1, v6}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    goto :goto_2
.end method

.method public final b(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LL/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    return-void
.end method

.method public final b(Lo/T;)V
    .locals 0

    invoke-direct {p0}, LQ/s;->aE()V

    invoke-virtual {p0, p1}, LQ/s;->a(Lo/T;)V

    return-void
.end method

.method protected b(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->l:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    :cond_0
    return-void
.end method

.method protected final c(LO/g;)I
    .locals 2

    invoke-virtual {p1}, LO/g;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LO/g;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0d00c7

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LO/g;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d00c6

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LO/g;->q()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, LO/g;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_3

    const v0, 0x7f0d00cc

    goto :goto_0

    :cond_3
    const v0, 0x7f0d00c5

    goto :goto_0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected c(I)V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->a(I)V

    invoke-direct {p0}, LQ/s;->ay()V

    return-void
.end method

.method protected c(LO/j;I)V
    .locals 2

    invoke-virtual {p0}, LQ/s;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;II)V

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/B;

    const-string v1, "marker"

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LL/B;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "t"

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1}, LQ/s;->d(Lcom/google/android/maps/driveabout/vector/c;)V

    return-void
.end method

.method public final c(Z)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v3, LL/D;

    const-string v4, "mute"

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v3}, Ll/f;->b(Ll/j;)V

    const-string v0, "u"

    if-nez p1, :cond_1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/a;->c(Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->o()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Z)V

    invoke-direct {p0}, LQ/s;->aq()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public d()V
    .locals 0

    invoke-virtual {p0}, LQ/s;->e()V

    return-void
.end method

.method protected d(Lcom/google/android/maps/driveabout/vector/c;)V
    .locals 3

    instance-of v0, p1, Lcom/google/android/maps/driveabout/app/cu;

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/s;->a(LO/N;)V

    invoke-virtual {p0}, LQ/s;->aj()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lo/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    check-cast p1, Lo/G;

    invoke-interface {p1}, Lo/G;->a()Lo/U;

    move-result-object v2

    invoke-virtual {v2}, Lo/U;->a()Lo/o;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/bp;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cQ;Lo/o;)V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/D;

    const-string v1, "viewTraffic"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "f"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1}, LQ/s;->b(Z)V

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public final e(Z)V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/D;

    const-string v1, "viewSatellite"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "S"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1}, LQ/s;->h(Z)V

    return-void
.end method

.method protected f()I
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->hasBearing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 2

    invoke-direct {p0}, LQ/s;->aE()V

    new-instance v0, LL/D;

    const-string v1, "viewBicycling"

    invoke-direct {v0, v1, p1}, LL/D;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    const-string v0, "b"

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, p1}, LQ/s;->i(Z)V

    return-void
.end method

.method protected g()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->f()V

    return-void
.end method

.method public final g(Z)V
    .locals 1

    invoke-direct {p0}, LQ/s;->aE()V

    const-string v0, ","

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LQ/s;->s()V

    invoke-direct {p0}, LQ/s;->aD()V

    if-eqz p1, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->h()V

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(F)F

    invoke-virtual {p0}, LQ/s;->ah()V

    :cond_0
    return-void
.end method

.method protected h(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    :goto_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "SatelliteImagery"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    invoke-direct {p0}, LQ/s;->aD()V

    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cS;->a(Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_0
.end method

.method protected i()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->j:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    const/high16 v1, -0x40800000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(F)F

    invoke-virtual {p0}, LQ/s;->ah()V

    :cond_0
    return-void
.end method

.method protected i(Z)V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/cS;->b(Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    invoke-static {v1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    const-string v1, "BicyclingLayer"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    invoke-direct {p0}, LQ/s;->aD()V

    return-void
.end method

.method protected j()Z
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    move-result v0

    return v0
.end method

.method protected k()V
    .locals 3

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method protected l()V
    .locals 0

    return-void
.end method

.method protected m()V
    .locals 0

    return-void
.end method

.method protected n()V
    .locals 0

    return-void
.end method

.method protected o()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->a(LQ/x;)LQ/s;

    move-result-object v0

    check-cast v0, LQ/d;

    invoke-virtual {v0, v2}, LQ/d;->b_(Z)V

    iget-object v0, p0, LQ/s;->a:LQ/p;

    sget-object v1, LQ/x;->m:LQ/x;

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    return-void
.end method

.method public p()V
    .locals 0

    return-void
.end method

.method public q()V
    .locals 0

    return-void
.end method

.method protected r()V
    .locals 1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    return-void
.end method

.method protected s()V
    .locals 0

    return-void
.end method

.method protected t()V
    .locals 0

    return-void
.end method

.method protected u()V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->b()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d006a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    iget-object v2, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v2

    invoke-direct {p0}, LQ/s;->aC()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V

    goto :goto_0
.end method

.method protected v()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    return-void
.end method

.method protected w()V
    .locals 5

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/cQ;->setTimeRemaining(IZIZ)V

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v0

    goto :goto_0
.end method

.method protected x()V
    .locals 2

    iget-object v0, p0, LQ/s;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-direct {p0}, LQ/s;->aw()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected y()V
    .locals 0

    return-void
.end method

.method protected z()V
    .locals 0

    return-void
.end method
