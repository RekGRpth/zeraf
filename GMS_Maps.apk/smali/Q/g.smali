.class public LQ/g;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:J

.field private volatile d:Z

.field private e:Z

.field private f:LQ/i;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, LQ/s;-><init>()V

    new-instance v0, LQ/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LQ/i;-><init>(LQ/g;LQ/h;)V

    iput-object v0, p0, LQ/g;->f:LQ/i;

    return-void
.end method

.method static synthetic a(LQ/g;Z)Z
    .locals 0

    iput-boolean p1, p0, LQ/g;->d:Z

    return p1
.end method


# virtual methods
.method protected a(Z)V
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LQ/g;->c:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v2, 0x320

    add-long/2addr v0, v2

    iput-wide v0, p0, LQ/g;->c:J

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x47435000

    :goto_1
    iget-object v1, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    iget-object v2, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    iget-object v3, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/app/aQ;->a(F)Lo/am;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/app/cq;->a(LaH/h;Lo/am;)V

    goto :goto_0

    :pswitch_0
    const v0, 0x463b8000

    goto :goto_1

    :pswitch_1
    const v0, 0x453b8000

    goto :goto_1

    :cond_1
    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->a(LaH/h;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x1

    iput-boolean v5, p0, LQ/g;->d:Z

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/g;->f:LQ/i;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->p()V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficButtonState(Z)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->F()V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    invoke-virtual {p0, v0}, LQ/g;->a(LO/N;)V

    :cond_0
    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    new-array v2, v5, [LO/z;

    const/4 v3, 0x0

    iget-object v4, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    :cond_1
    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v5, :cond_2

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/maps/driveabout/app/cQ;->c(Z)V

    :cond_3
    return-void
.end method

.method protected b(Z)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/g;->f:LQ/i;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->q()V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->J()V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficButtonState(Z)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->f(Z)V

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LQ/g;->c:J

    return-void
.end method

.method protected n()V
    .locals 1

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    return-void
.end method

.method public p()V
    .locals 1

    iget-boolean v0, p0, LQ/g;->d:Z

    iput-boolean v0, p0, LQ/g;->e:Z

    return-void
.end method

.method public q()V
    .locals 2

    iget-boolean v0, p0, LQ/g;->e:Z

    iput-boolean v0, p0, LQ/g;->d:Z

    iget-boolean v0, p0, LQ/g;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/g;->f:LQ/i;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    :cond_0
    return-void
.end method

.method protected v()V
    .locals 2

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    return-void
.end method

.method protected w()V
    .locals 5

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    iget-object v0, p0, LQ/g;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v1

    const/4 v4, 0x1

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/maps/driveabout/app/cQ;->setTimeRemaining(IZIZ)V

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v0

    goto :goto_0
.end method
