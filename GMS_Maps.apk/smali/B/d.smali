.class LB/d;
.super LR/h;
.source "SourceFile"


# instance fields
.field final synthetic a:LB/a;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/util/List;


# direct methods
.method public constructor <init>(LB/a;J)V
    .locals 1

    iput-object p1, p0, LB/d;->a:LB/a;

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, LR/h;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LB/d;->c:Ljava/lang/Long;

    return-void
.end method

.method static synthetic a(LB/d;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, LB/d;->f()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, LR/i;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LR/i;->a()LR/j;

    move-result-object v2

    iget-object v3, v2, LR/j;->a:Ljava/lang/Object;

    sget-object v4, LB/a;->a:Lo/aq;

    if-ne v3, v4, :cond_1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {p0, v0}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v2, v2, LR/j;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(LB/b;)V
    .locals 1

    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(LD/a;)V
    .locals 4

    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;

    iget-object v2, v0, LB/b;->a:LF/T;

    invoke-interface {v2, p1}, LF/T;->b(LD/a;)V

    iget-object v2, p0, LB/d;->a:LB/a;

    iget v3, v0, LB/b;->b:I

    invoke-static {v2, v3}, LB/a;->a(LB/a;I)I

    iget-object v2, p0, LB/d;->a:LB/a;

    iget v0, v0, LB/b;->c:I

    invoke-static {v2, v0}, LB/a;->b(LB/a;I)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method protected a(Lo/aq;LB/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, LB/d;->a:LB/a;

    iget v1, p2, LB/b;->b:I

    invoke-static {v0, v1}, LB/a;->a(LB/a;I)I

    iget-object v0, p0, LB/d;->a:LB/a;

    iget v1, p2, LB/b;->c:I

    invoke-static {v0, v1}, LB/a;->b(LB/a;I)I

    iget-object v0, p2, LB/b;->a:LF/T;

    if-eqz v0, :cond_0

    iput v2, p2, LB/b;->b:I

    iput v2, p2, LB/b;->c:I

    invoke-virtual {p0, p2}, LB/d;->a(LB/b;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, LB/d;->f()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, LR/i;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, LR/i;->a()LR/j;

    move-result-object v3

    iget-object v0, v3, LR/j;->a:Ljava/lang/Object;

    sget-object v4, LB/a;->a:Lo/aq;

    if-ne v0, v4, :cond_1

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {p0, v0}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v0, v3, LR/j;->b:Ljava/lang/Object;

    check-cast v0, LB/b;

    iget-object v4, v0, LB/b;->a:LF/T;

    if-eqz v4, :cond_2

    invoke-interface {v4}, LF/T;->a()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    iget-object v0, v3, LR/j;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v3, p0, LB/d;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, LD/a;->b(J)LD/a;

    move-result-object v3

    invoke-interface {v4, v3}, LF/T;->a(LD/a;)V

    iget-object v3, p0, LB/d;->a:LB/a;

    iget v4, v0, LB/b;->b:I

    invoke-static {v3, v4}, LB/a;->a(LB/a;I)I

    const/4 v3, 0x0

    iput v3, v0, LB/b;->b:I

    goto :goto_0

    :cond_4
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lo/aq;

    check-cast p2, LB/b;

    invoke-virtual {p0, p1, p2}, LB/d;->a(Lo/aq;LB/b;)V

    return-void
.end method

.method public c()LR/j;
    .locals 2

    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v0

    invoke-virtual {v0}, LR/i;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LR/i;->a()LR/j;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, LB/d;->a:LB/a;

    invoke-static {v0}, LB/a;->a(LB/a;)Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    sget-object v0, LB/a;->a:Lo/aq;

    invoke-virtual {p0, v0}, LB/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;

    if-nez v0, :cond_0

    sget-object v6, LB/a;->a:Lo/aq;

    new-instance v0, LB/b;

    const/4 v1, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, LB/b;-><init>(LF/T;IIJ)V

    invoke-virtual {p0, v6, v0}, LB/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput-wide v4, v0, LB/b;->d:J

    goto :goto_0
.end method
