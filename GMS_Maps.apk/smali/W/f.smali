.class public LW/f;
.super LW/k;
.source "SourceFile"


# static fields
.field public static final a:LW/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LW/f;

    invoke-direct {v0}, LW/f;-><init>()V

    sput-object v0, LW/f;->a:LW/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, LW/k;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/f;->b:LW/h;

    sget-object v1, LW/n;->a:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->a(LU/z;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-virtual {p0}, LW/f;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/h;->a()V

    sget-object v0, LU/B;->c:LU/B;

    iget-object v0, p0, LW/f;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, LU/z;->f:LU/B;

    :cond_0
    invoke-virtual {p0}, LW/f;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/f;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->b(Z)V

    return-void
.end method

.method public b(LU/z;)V
    .locals 2

    invoke-virtual {p0}, LW/f;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/f;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->b(Z)V

    return-void
.end method

.method public c(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/f;->b:LW/h;

    sget-object v1, LW/n;->b:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    return-void
.end method

.method protected e()V
    .locals 2

    invoke-virtual {p0}, LW/f;->k()Lcom/google/android/maps/rideabout/view/j;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LW/f;->a(Z)V

    iget-object v0, p0, LW/f;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LW/f;->c:LW/j;

    invoke-virtual {v1, v0}, LW/j;->c(LU/z;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LW/f;->b(Z)V

    return-void
.end method
