.class public LW/a;
.super LW/k;
.source "SourceFile"


# static fields
.field public static final a:LW/a;


# instance fields
.field private h:Z

.field private final i:Ljava/util/HashSet;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LW/a;

    invoke-direct {v0}, LW/a;-><init>()V

    sput-object v0, LW/a;->a:LW/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, LW/k;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LW/a;->i:Ljava/util/HashSet;

    const/4 v0, 0x0

    iput-boolean v0, p0, LW/a;->j:Z

    return-void
.end method

.method private a(J)V
    .locals 2

    iget-boolean v0, p0, LW/a;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LW/a;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->n()Lcom/google/googlenav/ui/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LW/a;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    new-instance v1, LW/d;

    invoke-direct {v1, p0}, LW/d;-><init>(LW/a;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private a(LU/z;Z)V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v2

    iget-object v3, p0, LW/a;->c:LW/j;

    invoke-virtual {v3, p1}, LW/j;->c(LU/z;)V

    iget-object v3, p1, LU/z;->c:Lbi/a;

    invoke-interface {v2, v3}, Lcom/google/android/maps/rideabout/view/j;->d(Lbi/a;)V

    iget-object v3, p1, LU/z;->c:Lbi/a;

    iget-object v4, p0, LW/a;->d:LW/g;

    invoke-virtual {v4}, LW/g;->a()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;Z)V

    iget-object v2, p0, LW/a;->c:LW/j;

    invoke-virtual {v2}, LW/j;->b()Lcom/google/android/maps/rideabout/app/a;

    move-result-object v3

    iget-object v2, p0, LW/a;->c:LW/j;

    invoke-virtual {v2}, LW/j;->c()Lbi/a;

    move-result-object v2

    invoke-direct {p0, v3, v2}, LW/a;->a(Lcom/google/android/maps/rideabout/app/a;Lbi/a;)V

    if-nez p2, :cond_0

    iget-boolean v2, p0, LW/a;->h:Z

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v4

    if-eq v2, v4, :cond_5

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v4

    iput-boolean v4, p0, LW/a;->h:Z

    iget-boolean v4, p0, LW/a;->h:Z

    if-eqz v4, :cond_6

    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/maps/rideabout/view/j;->f()V

    :cond_1
    :goto_1
    if-nez v2, :cond_2

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    invoke-virtual {p0, v0}, LW/a;->a(Z)V

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/a;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/google/android/maps/rideabout/app/a;->l()Lcom/google/android/maps/rideabout/app/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/rideabout/app/c;->e:Lcom/google/android/maps/rideabout/app/c;

    if-ne v0, v1, :cond_7

    invoke-static {}, LW/a;->r()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LW/a;->a(J)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    move v2, v0

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/maps/rideabout/view/j;->e()V

    goto :goto_1

    :cond_7
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, LW/a;->a(J)V

    goto :goto_2
.end method

.method static synthetic a(LW/a;)V
    .locals 0

    invoke-direct {p0}, LW/a;->q()V

    return-void
.end method

.method private a(Lcom/google/android/maps/rideabout/app/a;Lbi/a;)V
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->l()Lcom/google/android/maps/rideabout/app/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/rideabout/app/c;->a:Lcom/google/android/maps/rideabout/app/c;

    if-ne v0, v1, :cond_2

    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget v0, v0, Lbi/t;->a:I

    invoke-direct {p0, v0}, LW/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LW/a;->n()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->l()Lcom/google/android/maps/rideabout/app/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/rideabout/app/c;->e:Lcom/google/android/maps/rideabout/app/c;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/app/a;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget v0, v0, Lbi/t;->a:I

    invoke-direct {p0, v0}, LW/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LW/a;->o()V

    iget-object v0, p0, LW/a;->f:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->o()V

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    iget-object v0, p0, LW/a;->i:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LW/a;->i:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private q()V
    .locals 4

    iget-object v0, p0, LW/a;->c:LW/j;

    invoke-virtual {v0}, LW/j;->c()Lbi/a;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(Lbi/a;)V

    iget-object v2, p0, LW/a;->d:LW/g;

    invoke-virtual {v2}, LW/g;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->c(Lbi/a;)I

    move-result v2

    invoke-interface {v1}, Lcom/google/android/maps/rideabout/view/j;->b()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-interface {v1, v2}, Lcom/google/android/maps/rideabout/view/j;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, LW/a;->d:LW/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LW/g;->a(Z)V

    goto :goto_0

    :cond_2
    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->b(Lbi/a;)V

    goto :goto_0
.end method

.method private static r()J
    .locals 2

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->u:I

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, LW/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LW/a;->j:Z

    return-void
.end method

.method public a(LU/z;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LW/a;->a(LU/z;Z)V

    return-void
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, LW/a;->c:LW/j;

    invoke-virtual {v0}, LW/j;->b()Lcom/google/android/maps/rideabout/app/a;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LW/a;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    iget-object v2, p0, LW/a;->d:LW/g;

    invoke-virtual {v2}, LW/g;->c()Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/maps/rideabout/view/i;->a(Lcom/google/android/maps/rideabout/app/a;Z)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, LW/a;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/maps/rideabout/view/i;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, LW/a;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/maps/rideabout/view/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, LW/a;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/h;->a(Lcom/google/android/maps/rideabout/app/a;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LW/a;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/h;->a()V

    invoke-virtual {p0}, LW/a;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/i;->a()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-super {p0}, LW/k;->b()V

    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    new-instance v1, LW/b;

    invoke-direct {v1, p0}, LW/b;-><init>(LW/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/a;->b:LW/h;

    sget-object v1, LW/n;->c:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    return-void
.end method

.method public c()V
    .locals 2

    invoke-super {p0}, LW/k;->c()V

    iget-object v0, p0, LW/a;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LW/a;->a(LU/z;Z)V

    invoke-virtual {p0}, LW/a;->k()Lcom/google/android/maps/rideabout/view/j;

    move-result-object v0

    new-instance v1, LW/c;

    invoke-direct {v1, p0}, LW/c;-><init>(LW/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public c(LU/z;)V
    .locals 2

    iget-object v0, p0, LW/a;->b:LW/h;

    sget-object v1, LW/n;->b:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    return-void
.end method

.method public d()V
    .locals 2

    invoke-super {p0}, LW/k;->d()V

    iget-object v0, p0, LW/a;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LW/a;->a(LU/z;Z)V

    return-void
.end method

.method protected e()V
    .locals 1

    iget-object v0, p0, LW/a;->c:LW/j;

    invoke-virtual {v0}, LW/j;->b()Lcom/google/android/maps/rideabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/app/a;->i()Z

    move-result v0

    iput-boolean v0, p0, LW/a;->h:Z

    iget-boolean v0, p0, LW/a;->h:Z

    invoke-virtual {p0, v0}, LW/a;->b(Z)V

    return-void
.end method

.method public f()V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, LW/a;->j:Z

    const/16 v0, 0x61

    const-string v1, "a"

    const-string v2, "s"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LW/a;->d:LW/g;

    invoke-virtual {v0, v3}, LW/g;->b(Z)V

    invoke-virtual {p0}, LW/a;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/i;->b()V

    return-void
.end method
