.class public LM/B;
.super LM/e;
.source "SourceFile"


# instance fields
.field private final c:LaV/f;

.field private final d:LN/a;

.field private final e:LaV/f;

.field private f:Landroid/location/Location;

.field private g:Z

.field private h:F


# direct methods
.method public constructor <init>(LM/b;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "driveabout_orientation_filter"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    new-instance v0, LaV/f;

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LM/B;->e:LaV/f;

    iput-boolean v1, p0, LM/B;->g:Z

    const/4 v0, 0x0

    iput v0, p0, LM/B;->h:F

    const/4 v0, 0x0

    iput-object v0, p0, LM/B;->d:LN/a;

    new-instance v0, LaV/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LM/B;->c:LaV/f;

    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .locals 5

    new-instance v1, LM/C;

    invoke-direct {v1, p1}, LM/C;-><init>(LM/C;)V

    const-string v0, "driveabout_orientation_filter"

    invoke-virtual {v1, v0}, LM/C;->a(Ljava/lang/String;)V

    iget-object v0, p0, LM/B;->c:LaV/f;

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v2

    invoke-virtual {v1}, LM/C;->c()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, LaV/f;->a(JF)F

    move-result v2

    iget-boolean v0, p0, LM/B;->g:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x41f00000

    :goto_0
    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    if-eqz v3, :cond_1

    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, LM/B;->h:F

    const v4, 0x3e4ccccd

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-static {v3, v2}, Lo/V;->a(FF)F

    move-result v3

    cmpg-float v0, v3, v0

    if-gez v0, :cond_1

    iget-object v0, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {v1, v0}, LM/C;->a(F)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LM/B;->g:Z

    :goto_1
    iget-object v0, p0, LM/B;->e:LaV/f;

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v2

    invoke-virtual {p1}, LM/C;->d()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, LaV/f;->a(JF)F

    move-result v0

    invoke-virtual {v1, v0}, LM/C;->b(F)V

    iget-object v0, p0, LM/B;->b:LM/b;

    invoke-interface {v0, v1}, LM/b;->a(LM/C;)V

    return-void

    :cond_0
    const/high16 v0, 0x41c80000

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, LM/C;->a(F)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LM/B;->g:Z

    goto :goto_1
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1

    iput-object p1, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    iput v0, p0, LM/B;->h:F

    :cond_0
    return-void
.end method
