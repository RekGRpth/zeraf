.class public Lh/i;
.super Lh/q;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0}, Lh/q;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh/i;->d:Z

    invoke-virtual {p0, p1}, Lh/i;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lh/i;->b:F

    return v0
.end method

.method public a(F)V
    .locals 1

    iget-boolean v0, p0, Lh/i;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p1}, Lh/i;->a(FF)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lh/i;->c:F

    iput v0, p0, Lh/i;->a:F

    iput p1, p0, Lh/i;->b:F

    goto :goto_0
.end method

.method public a(FF)V
    .locals 1

    iput p1, p0, Lh/i;->a:F

    iput p2, p0, Lh/i;->b:F

    iput p1, p0, Lh/i;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh/i;->d:Z

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lh/i;->c:F

    return v0
.end method

.method public b(J)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lh/i;->c(J)F

    move-result v0

    iget v1, p0, Lh/i;->a:F

    iget v2, p0, Lh/i;->b:F

    iget v3, p0, Lh/i;->a:F

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, Lh/i;->c:F

    return-void
.end method

.method public isInitialized()Z
    .locals 1

    iget-boolean v0, p0, Lh/i;->d:Z

    return v0
.end method
