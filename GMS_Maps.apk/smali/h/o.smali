.class public Lh/o;
.super Lh/n;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0, p1}, Lh/n;-><init>(Landroid/view/animation/Interpolator;)V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2

    iget-boolean v0, p0, Lh/o;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2, p1, p2}, Lh/o;->a(IIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v1, Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->b(Lo/T;)V

    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    goto :goto_0
.end method

.method public a(IIII)V
    .locals 1

    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p3, p4}, Lo/T;->d(II)V

    iget-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh/o;->d:Z

    return-void
.end method

.method protected a(J)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lh/o;->c(J)F

    move-result v3

    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v1, Lo/T;

    iget-object v2, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v2, Lo/T;

    invoke-static {v0, v1, v3, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->a(Lo/T;)V

    return-void
.end method

.method protected a(Lo/T;)V
    .locals 1

    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    return-void
.end method

.method public b(II)V
    .locals 0

    invoke-virtual {p0, p1, p2, p1, p2}, Lh/o;->a(IIII)V

    return-void
.end method

.method protected bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->b(Lo/T;)V

    return-void
.end method

.method protected b(Lo/T;)V
    .locals 1

    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    return-void
.end method

.method protected bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->c(Lo/T;)V

    return-void
.end method

.method protected c(Lo/T;)V
    .locals 1

    iget-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    return-void
.end method
