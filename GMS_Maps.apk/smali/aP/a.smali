.class public LaP/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const v0, 0x7f040158

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sput-object v0, LaP/a;->a:Landroid/view/View;

    return-void
.end method

.method public static a(Lcom/google/googlenav/android/BaseMapsActivity;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f020087

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020040

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_0
    const v0, 0x7f02003f

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020042

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_1
    const v0, 0x7f020041

    goto :goto_3

    :cond_2
    const v0, 0x7f020086

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return-object v0

    :sswitch_0
    const/16 v0, 0x4f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x2c4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x38e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0xf2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1bb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x231

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_7
    const/16 v0, 0xd2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x34a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x34f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_a
    const/16 v0, 0x13d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_b
    const/16 v0, 0x2dc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_c
    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_d
    const/16 v0, 0x49e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_e
    const/16 v0, 0x4b4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_f
    const/16 v0, 0x526

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_10
    const/16 v0, 0x1d9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_11
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_12
    const/16 v0, 0x307

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f10007a -> :sswitch_c
        0x7f1002f0 -> :sswitch_12
        0x7f100499 -> :sswitch_3
        0x7f10049c -> :sswitch_0
        0x7f1004ae -> :sswitch_4
        0x7f1004b7 -> :sswitch_10
        0x7f1004be -> :sswitch_7
        0x7f1004bf -> :sswitch_8
        0x7f1004c0 -> :sswitch_9
        0x7f1004c1 -> :sswitch_f
        0x7f1004c2 -> :sswitch_2
        0x7f1004c3 -> :sswitch_6
        0x7f1004c4 -> :sswitch_11
        0x7f1004c5 -> :sswitch_5
        0x7f1004c6 -> :sswitch_1
        0x7f1004c7 -> :sswitch_a
        0x7f1004c8 -> :sswitch_b
        0x7f1004d2 -> :sswitch_d
        0x7f1004d3 -> :sswitch_e
    .end sparse-switch
.end method

.method public static a(Landroid/view/Menu;)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {v2}, LaP/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-void
.end method

.method public static a(Landroid/view/MenuItem;Ljava/lang/String;)V
    .locals 5

    if-eqz p0, :cond_0

    invoke-static {p0}, LaP/a;->c(Landroid/view/MenuItem;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "a=s"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "i="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v1, p1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static b(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method public static b(Landroid/view/MenuItem;)V
    .locals 1

    sget-object v0, LaP/a;->a:Landroid/view/View;

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-void
.end method

.method private static c(Landroid/view/MenuItem;)I
    .locals 3

    const/4 v0, 0x2

    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const/4 v1, -0x1

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x11

    goto :goto_0

    :sswitch_3
    const/16 v0, 0xe

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2e

    goto :goto_0

    :sswitch_5
    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x43

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x44

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x2f

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x34

    goto :goto_0

    :sswitch_a
    const/16 v0, 0x13

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f100499 -> :sswitch_0
        0x7f10049c -> :sswitch_1
        0x7f1004ae -> :sswitch_0
        0x7f1004b7 -> :sswitch_a
        0x7f1004be -> :sswitch_5
        0x7f1004bf -> :sswitch_6
        0x7f1004c0 -> :sswitch_7
        0x7f1004c1 -> :sswitch_9
        0x7f1004c2 -> :sswitch_3
        0x7f1004c3 -> :sswitch_4
        0x7f1004c6 -> :sswitch_2
        0x7f1004c7 -> :sswitch_8
    .end sparse-switch
.end method
