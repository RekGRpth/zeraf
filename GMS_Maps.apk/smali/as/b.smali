.class public Las/b;
.super Las/a;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Las/c;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Las/c;->a()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;I)V

    return-void
.end method

.method public constructor <init>(Las/c;Ljava/lang/Runnable;)V
    .locals 1

    invoke-virtual {p1}, Las/c;->a()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;I)V

    return-void
.end method

.method public constructor <init>(Las/c;Ljava/lang/Runnable;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, Las/c;->a()I

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Las/a;-><init>(Las/c;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-direct {p0, p4}, Las/b;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 0

    iput p1, p0, Las/b;->a:I

    return-void
.end method


# virtual methods
.method d()I
    .locals 1

    iget-object v0, p0, Las/b;->c:Las/c;

    invoke-virtual {v0, p0}, Las/c;->c(Las/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method h()V
    .locals 1

    iget-object v0, p0, Las/b;->c:Las/c;

    invoke-virtual {v0, p0}, Las/c;->a(Las/b;)V

    return-void
.end method

.method public declared-synchronized j()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Las/b;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
