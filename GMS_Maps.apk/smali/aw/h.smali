.class public Law/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/p;


# static fields
.field private static I:I

.field private static volatile K:Law/h;


# instance fields
.field private volatile A:I

.field private volatile B:I

.field private volatile C:I

.field private D:Lcom/google/googlenav/bk;

.field private final E:Lcom/google/googlenav/common/a;

.field private volatile F:I

.field private G:Ljava/lang/Throwable;

.field private H:I

.field private J:I

.field private L:Z

.field protected volatile a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:Z

.field protected final e:Law/l;

.field protected f:Law/o;

.field protected volatile g:Z

.field protected h:I

.field protected i:I

.field protected j:Lcom/google/googlenav/common/io/g;

.field protected k:Law/f;

.field private volatile l:Z

.field private final m:Ljava/util/List;

.field private n:Law/p;

.field private final o:Ljava/lang/String;

.field private p:Ljava/lang/Long;

.field private final q:Ljava/util/List;

.field private final r:Ljava/util/Random;

.field private s:J

.field private volatile t:Z

.field private volatile u:I

.field private volatile v:J

.field private volatile w:J

.field private volatile x:Z

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Law/h;->I:I

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x0

    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Law/h;->q:Ljava/util/List;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Law/h;->r:Ljava/util/Random;

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Law/h;->s:J

    iput-boolean v2, p0, Law/h;->t:Z

    iput-wide v4, p0, Law/h;->v:J

    iput-wide v4, p0, Law/h;->w:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law/h;->y:J

    iput-wide v4, p0, Law/h;->z:J

    iput v2, p0, Law/h;->A:I

    iput v2, p0, Law/h;->B:I

    iput v2, p0, Law/h;->C:I

    iput v3, p0, Law/h;->F:I

    iput v3, p0, Law/h;->H:I

    iput v3, p0, Law/h;->J:I

    iput-boolean v2, p0, Law/h;->L:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p1}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Law/h;->a:Ljava/lang/String;

    iput-object p3, p0, Law/h;->c:Ljava/lang/String;

    iput-object p2, p0, Law/h;->b:Ljava/lang/String;

    iput-object p4, p0, Law/h;->o:Ljava/lang/String;

    iput-boolean p5, p0, Law/h;->d:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    iput-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    new-instance v0, Law/f;

    iget-object v1, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p0, v1}, Law/f;-><init>(Law/h;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Law/h;->k:Law/f;

    iput v2, p0, Law/h;->h:I

    iput v2, p0, Law/h;->i:I

    new-instance v0, Law/l;

    iget-object v1, p0, Law/h;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v6}, Law/l;-><init>(Law/h;Ljava/lang/String;Law/i;)V

    iput-object v0, p0, Law/h;->e:Law/l;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Law/h;->m:Ljava/util/List;

    new-instance v0, Law/o;

    invoke-direct {v0, p0, v6}, Law/o;-><init>(Law/h;Law/i;)V

    iput-object v0, p0, Law/h;->f:Law/o;

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    iget-object v1, p0, Law/h;->f:Law/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic A()I
    .locals 2

    sget v0, Law/h;->I:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Law/h;->I:I

    return v0
.end method

.method private B()Law/o;
    .locals 3

    new-instance v0, Law/o;

    iget-object v1, p0, Law/h;->f:Law/o;

    invoke-static {v1}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Law/o;-><init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Law/i;)V

    iget-object v1, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static C()J
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "SessionID"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SessionID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized D()V
    .locals 2

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    :try_start_0
    iput-wide v0, p0, Law/h;->z:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/h;->x:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law/h;->y:J

    const/4 v0, -0x1

    iput v0, p0, Law/h;->H:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private E()Lcom/google/googlenav/bk;
    .locals 1

    iget-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/bk;

    invoke-direct {v0}, Lcom/google/googlenav/bk;-><init>()V

    iput-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    :cond_0
    iget-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    return-object v0
.end method

.method static synthetic a(Law/h;J)J
    .locals 0

    iput-wide p1, p0, Law/h;->v:J

    return-wide p1
.end method

.method public static a()Law/h;
    .locals 1

    sget-object v0, Law/h;->K:Law/h;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Law/h;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Law/h;)Law/o;
    .locals 1

    invoke-direct {p0}, Law/h;->B()Law/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Law/h;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    iput-object p1, p0, Law/h;->p:Ljava/lang/Long;

    return-object p1
.end method

.method private a(II)V
    .locals 2

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(ILjava/lang/Throwable;)V
    .locals 5

    const-wide/16 v3, 0x7d0

    const/4 v0, 0x0

    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    iget-object v1, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v1}, Lcom/google/googlenav/common/io/g;->c()V

    iput-object p2, p0, Law/h;->G:Ljava/lang/Throwable;

    iput p1, p0, Law/h;->H:I

    const/4 v1, 0x4

    if-ne p1, v1, :cond_5

    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Law/h;->x:Z

    if-eqz v1, :cond_4

    :cond_1
    invoke-direct {p0}, Law/h;->D()V

    iput p1, p0, Law/h;->H:I

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Law/h;->y:J

    :cond_2
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Law/h;->a(I)V

    :cond_3
    return-void

    :cond_4
    :try_start_1
    iget-wide v1, p0, Law/h;->y:J

    iget-wide v3, p0, Law/h;->s:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, Law/h;->y:J

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    :try_start_2
    iget-boolean v1, p0, Law/h;->x:Z

    if-nez v1, :cond_7

    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Law/h;->y:J

    iget-wide v1, p0, Law/h;->z:J

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v1, v1, v3

    if-nez v1, :cond_6

    iget-object v1, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iput-wide v1, p0, Law/h;->z:J

    goto :goto_0

    :cond_6
    iget-wide v1, p0, Law/h;->z:J

    const-wide/16 v3, 0x3a98

    add-long/2addr v1, v3

    iget-object v3, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_7
    iget-wide v1, p0, Law/h;->y:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_8

    const-wide/16 v1, 0x7d0

    iput-wide v1, p0, Law/h;->y:J

    :goto_1
    iget-wide v1, p0, Law/h;->y:J

    iget-wide v3, p0, Law/h;->s:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iget-wide v1, p0, Law/h;->s:J

    iput-wide v1, p0, Law/h;->y:J

    goto :goto_0

    :cond_8
    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x5

    mul-long/2addr v1, v3

    const-wide/16 v3, 0x4

    div-long/2addr v1, v3

    iput-wide v1, p0, Law/h;->y:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 2

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(I[I)V
    .locals 6

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    array-length v3, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, p2, v1

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v5, p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Law/h;I)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->d(I)V

    return-void
.end method

.method static synthetic a(Law/h;ILjava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Law/h;->a(ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Law/h;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->g(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Law/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->c(Z)V

    return-void
.end method

.method static synthetic a(Law/h;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Law/h;->a(ZZ)V

    return-void
.end method

.method private declared-synchronized a(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->A:I

    if-eqz p1, :cond_0

    iget v0, p0, Law/h;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->B:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Law/h;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static a(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    invoke-interface {v0}, Law/g;->t_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Law/h;I)I
    .locals 0

    iput p1, p0, Law/h;->F:I

    return p1
.end method

.method static synthetic b(Law/h;J)J
    .locals 0

    iput-wide p1, p0, Law/h;->w:J

    return-wide p1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;
    .locals 7

    const-class v6, Law/h;

    monitor-enter v6

    :try_start_0
    sget-object v0, Law/h;->K:Law/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to create multiple DataRequestDispatchers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_1
    invoke-static {p0}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Law/h;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Law/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Law/h;->K:Law/h;

    sget-object v0, Law/h;->K:Law/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v6

    return-object v0
.end method

.method public static b()Law/p;
    .locals 1

    sget-object v0, Law/h;->K:Law/h;

    iget-object v0, v0, Law/h;->n:Law/p;

    return-object v0
.end method

.method static synthetic b(Law/h;)Lcom/google/googlenav/common/a;
    .locals 1

    iget-object v0, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method static b(J)V
    .locals 3

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {v1, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "SessionID"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-interface {v1}, Lcom/google/googlenav/common/io/j;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic b(Law/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->d(Z)V

    return-void
.end method

.method static synthetic b(Law/h;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Law/h;->b(ZZ)V

    return-void
.end method

.method private declared-synchronized b(ZZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->A:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->A:I

    if-eqz p1, :cond_0

    iget v0, p0, Law/h;->B:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->B:I

    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Law/h;->C:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static b(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    invoke-interface {v0}, Law/g;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static c()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Law/h;->K:Law/h;

    return-void
.end method

.method static synthetic c(Law/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->e(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    return-void
.end method

.method static synthetic c(Law/h;)Z
    .locals 1

    iget-boolean v0, p0, Law/h;->t:Z

    return v0
.end method

.method protected static c(Ljava/util/Vector;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    invoke-interface {v0}, Law/g;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private d(I)V
    .locals 2

    const/16 v0, 0xc8

    if-le p1, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    const/16 v1, 0x16

    invoke-direct {p0, v1, v0}, Law/h;->a(II)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic d(Law/h;)V
    .locals 0

    invoke-direct {p0}, Law/h;->D()V

    return-void
.end method

.method static synthetic d(Law/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Law/h;->f(Z)V

    return-void
.end method

.method private d(Z)V
    .locals 1

    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    return-void
.end method

.method private e(I)V
    .locals 4

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e(Z)V
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    return-void
.end method

.method static synthetic e(Law/h;)Z
    .locals 1

    iget-boolean v0, p0, Law/h;->l:Z

    return v0
.end method

.method static synthetic f(Law/h;)Lcom/google/googlenav/bk;
    .locals 1

    invoke-direct {p0}, Law/h;->E()Lcom/google/googlenav/bk;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private f(Z)V
    .locals 1

    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    return-void
.end method

.method static synthetic g(Law/h;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Law/h;->o:Ljava/lang/String;

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic h(Law/h;)Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic i(Law/h;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Law/h;)I
    .locals 1

    iget v0, p0, Law/h;->H:I

    return v0
.end method

.method static synthetic k(Law/h;)J
    .locals 2

    iget-wide v0, p0, Law/h;->y:J

    return-wide v0
.end method

.method static synthetic l(Law/h;)Ljava/util/Random;
    .locals 1

    iget-object v0, p0, Law/h;->r:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic z()Law/h;
    .locals 1

    sget-object v0, Law/h;->K:Law/h;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Law/h;->x:Z

    if-nez v2, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Law/h;->x:Z

    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, p0, Law/h;->z:J

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v1}, Lcom/google/googlenav/common/io/g;->d()Z

    move-result v1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, Law/h;->a(IZLjava/lang/String;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected a(IZLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2, p3}, Law/q;->a(IZLjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(I[BZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Law/h;->a(I[BZZZ)V

    return-void
.end method

.method public a(I[BZZZ)V
    .locals 6

    iget-object v0, p0, Law/h;->f:Law/o;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Law/o;->a(I[BZZZ)V

    return-void
.end method

.method public declared-synchronized a(J)V
    .locals 2

    const-wide/16 v0, 0x7d0

    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x7d0

    :try_start_0
    iput-wide v0, p0, Law/h;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput-wide p1, p0, Law/h;->s:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Law/g;)V
    .locals 4

    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Law/q;->a(Law/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Law/p;)V
    .locals 0

    iput-object p1, p0, Law/h;->n:Law/p;

    return-void
.end method

.method public declared-synchronized a(Law/q;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct {p0}, Law/h;->E()Lcom/google/googlenav/bk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Law/h;->a:Ljava/lang/String;

    iget-object v0, p0, Law/h;->e:Law/l;

    iget-object v1, p0, Law/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Law/l;->a(Law/l;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    return-void
.end method

.method public a([I)V
    .locals 1

    const/16 v0, 0x17

    invoke-direct {p0, v0}, Law/h;->e(I)V

    invoke-direct {p0, v0, p1}, Law/h;->a(I[I)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Law/h;->J:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Law/h;->J:I

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Law/h;->a(II)V

    :cond_0
    return-void
.end method

.method protected b(Law/g;)V
    .locals 4

    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Law/q;->b(Law/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized b(Law/q;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x14

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Law/h;->g:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Law/h;->u()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Law/h;->v()V

    goto :goto_0
.end method

.method public b([I)V
    .locals 1

    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Law/h;->e(I)V

    invoke-direct {p0, v0, p1}, Law/h;->a(I[I)V

    return-void
.end method

.method public c(I)V
    .locals 1

    const/16 v0, 0x19

    invoke-direct {p0, v0, p1}, Law/h;->a(II)V

    return-void
.end method

.method public c(Law/g;)V
    .locals 1

    iget-object v0, p0, Law/h;->f:Law/o;

    invoke-virtual {v0, p1}, Law/o;->c(Law/g;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Law/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x13

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Law/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Law/h;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Law/h;->k:Law/f;

    invoke-virtual {v0, p1}, Law/f;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->u:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->u:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->u:I

    invoke-virtual {p0}, Law/h;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->c(Law/l;)V

    iget-object v0, p0, Law/h;->k:Law/f;

    invoke-virtual {v0}, Law/f;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected i()J
    .locals 5

    invoke-static {}, Law/h;->C()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Law/h;->f:Law/o;

    new-instance v3, Law/k;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Law/k;-><init>(Law/h;Law/i;)V

    invoke-virtual {v2, v3}, Law/o;->c(Law/g;)V

    :cond_0
    return-wide v0
.end method

.method protected declared-synchronized j()[Law/q;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Law/q;

    iget-object v1, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected k()V
    .locals 4

    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Law/q;->k()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized l()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Law/h;->A:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized m()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Law/h;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized n()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Law/h;->t:Z

    if-eqz v0, :cond_1

    iget v0, p0, Law/h;->A:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Law/h;->A:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()J
    .locals 2

    iget-wide v0, p0, Law/h;->v:J

    return-wide v0
.end method

.method public p()J
    .locals 2

    iget-wide v0, p0, Law/h;->w:J

    return-wide v0
.end method

.method public q()Z
    .locals 4

    iget-wide v0, p0, Law/h;->w:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Law/h;->h:I

    return v0
.end method

.method public final s()I
    .locals 1

    iget v0, p0, Law/h;->i:I

    return v0
.end method

.method public t()I
    .locals 1

    iget v0, p0, Law/h;->F:I

    return v0
.end method

.method public u()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/h;->t:Z

    return-void
.end method

.method public v()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Law/h;->t:Z

    iget-object v0, p0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->e(Law/l;)V

    return-void
.end method

.method public declared-synchronized w()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Law/h;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Law/h;->p:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public x()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Law/h;->f:Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Law/h;->G:Ljava/lang/Throwable;

    return-object v0
.end method
