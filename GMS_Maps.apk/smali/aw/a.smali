.class public abstract Law/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/g;


# instance fields
.field private volatile a:Z

.field private b:I

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Law/a;->a:Z

    iput v0, p0, Law/a;->b:I

    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Law/a;->e:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public A_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 3

    invoke-virtual {p0}, Law/a;->a_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Law/a;->a:Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Law/a;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is not cancellable!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public aR()V
    .locals 1

    iget v0, p0, Law/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/a;->b:I

    return-void
.end method

.method protected aS()I
    .locals 1

    iget v0, p0, Law/a;->b:I

    return v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(J)V
    .locals 1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Law/a;->c:Ljava/lang/Long;

    return-void
.end method

.method public b_()Z
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Law/a;->b:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Law/a;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iget-object v3, p0, Law/a;->d:Ljava/lang/Long;

    if-nez v3, :cond_2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Law/a;->d:Ljava/lang/Long;

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Law/a;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object v3, p0, Law/a;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    goto :goto_0
.end method

.method public c_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 0

    return-void
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public u_()V
    .locals 0

    return-void
.end method

.method public z_()Z
    .locals 1

    iget-boolean v0, p0, Law/a;->a:Z

    return v0
.end method
