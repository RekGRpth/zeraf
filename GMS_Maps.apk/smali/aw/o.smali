.class public Law/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/p;


# instance fields
.field final synthetic a:Law/h;

.field private b:Ljava/util/Vector;

.field private c:Z

.field private final d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method private constructor <init>(Law/h;)V
    .locals 2

    iput-object p1, p0, Law/o;->a:Law/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ab;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method synthetic constructor <init>(Law/h;Law/i;)V
    .locals 0

    invoke-direct {p0, p1}, Law/o;-><init>(Law/h;)V

    return-void
.end method

.method private constructor <init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iput-object p1, p0, Law/o;->a:Law/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    invoke-virtual {p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clone()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method synthetic constructor <init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Law/i;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Law/o;-><init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Law/o;)V
    .locals 0

    invoke-direct {p0}, Law/o;->b()V

    return-void
.end method

.method static synthetic a(Law/o;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Law/o;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Law/o;Z)V
    .locals 0

    invoke-direct {p0, p1}, Law/o;->b(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method static synthetic b(Law/o;Z)Law/m;
    .locals 1

    invoke-direct {p0, p1}, Law/o;->c(Z)Law/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private b()V
    .locals 3

    const/16 v2, 0x19

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private c(Z)Law/m;
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Law/o;->b:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v2, p0, Law/o;->c:Z

    if-nez v2, :cond_1

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v2, Law/m;

    iget-object v0, p0, Law/o;->a:Law/h;

    iget-object v3, p0, Law/o;->b:Ljava/util/Vector;

    iget-object v4, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0, v3, v4}, Law/m;-><init>(Law/h;Ljava/util/Vector;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget-boolean v0, p0, Law/o;->c:Z

    return v0
.end method

.method static synthetic c(Law/o;)Z
    .locals 1

    invoke-direct {p0}, Law/o;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I[BZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Law/o;->a(I[BZZZ)V

    return-void
.end method

.method public final a(I[BZZZ)V
    .locals 7

    new-instance v0, Law/t;

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Law/t;-><init>(I[BZZZLjava/lang/Object;)V

    invoke-virtual {p0, v0}, Law/o;->c(Law/g;)V

    return-void
.end method

.method public a(Law/q;)V
    .locals 1

    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->a(Law/q;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Law/q;)V
    .locals 1

    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->b(Law/q;)V

    return-void
.end method

.method public c(I)V
    .locals 2

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x19

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public c(Law/g;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Law/o;->a:Law/h;

    iget-boolean v0, v0, Law/h;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Law/o;->a:Law/h;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Law/h;->a(IZLjava/lang/String;)V

    :cond_0
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Law/g;->s_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Law/o;->c:Z

    :cond_1
    iget-object v0, p0, Law/o;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1}, Law/g;->s_()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Law/o;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Law/o;->a:Law/h;

    iget-object v0, v0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->a(Law/l;)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public n()Z
    .locals 1

    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->n()Z

    move-result v0

    return v0
.end method

.method public p()J
    .locals 2

    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public w()J
    .locals 2

    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->w()J

    move-result-wide v0

    return-wide v0
.end method

.method public x()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
