.class public LaX/c;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:I

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;

.field private final e:LaX/d;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaX/d;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, LaX/c;->b:I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaX/c;->c:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaX/c;->d:Ljava/util/List;

    iput-object p1, p0, LaX/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p2, p0, LaX/c;->e:LaX/d;

    return-void
.end method

.method public static a(LaR/t;LaX/d;)LaX/c;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eO;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/eO;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, LaR/t;->j()J

    move-result-wide v2

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, LaX/c;

    invoke-direct {v1, v0, p1}, LaX/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaX/d;)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, LaX/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v6, 0x1

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eO;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return v6

    :cond_1
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, LaX/c;->b:I

    iget v1, p0, LaX/c;->b:I

    if-nez v1, :cond_0

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v4, p0, LaX/c;->c:Ljava/util/List;

    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_0

    iget-object v3, p0, LaX/c;->d:Ljava/util/List;

    invoke-virtual {v2, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x85

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, LaX/c;->e:LaX/d;

    if-eqz v0, :cond_0

    iget v0, p0, LaX/c;->b:I

    if-nez v0, :cond_1

    iget-object v0, p0, LaX/c;->e:LaX/d;

    iget-object v1, p0, LaX/c;->c:Ljava/util/List;

    iget-object v2, p0, LaX/c;->d:Ljava/util/List;

    invoke-interface {v0, v1, v2}, LaX/d;->a(Ljava/util/List;Ljava/util/List;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaX/c;->e:LaX/d;

    iget v1, p0, LaX/c;->b:I

    invoke-interface {v0, v1}, LaX/d;->a(I)V

    goto :goto_0
.end method

.method public u_()V
    .locals 2

    iget-object v0, p0, LaX/c;->e:LaX/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaX/c;->e:LaX/d;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LaX/d;->a(I)V

    :cond_0
    return-void
.end method
