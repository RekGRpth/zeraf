.class public Lbb/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Lbb/w;
    .locals 2

    new-instance v0, Lbb/y;

    invoke-direct {v0}, Lbb/y;-><init>()V

    const/16 v1, 0x2e7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lbb/y;->a(I)Lbb/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbb/y;->b(I)Lbb/y;

    move-result-object v0

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lbb/y;->c(I)Lbb/y;

    move-result-object v0

    const-string v1, "google.myplaces_panel:"

    invoke-virtual {v0, v1}, Lbb/y;->e(Ljava/lang/String;)Lbb/y;

    move-result-object v0

    invoke-virtual {v0}, Lbb/y;->a()Lbb/w;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaR/u;LaR/D;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1, v0}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LaR/u;LaR/D;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1}, Lbb/i;->a(LaR/u;LaR/D;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
