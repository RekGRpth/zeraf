.class public Lbb/j;
.super Lbb/f;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private b:Lbb/l;

.field private final c:Lbb/k;

.field private volatile d:I

.field private e:J

.field private final f:Lbb/w;


# direct methods
.method public constructor <init>(Lbb/w;)V
    .locals 2

    invoke-direct {p0}, Lbb/f;-><init>()V

    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lbb/j;->e:J

    iput-object p1, p0, Lbb/j;->f:Lbb/w;

    new-instance v0, Lbb/k;

    invoke-direct {v0, p0}, Lbb/k;-><init>(Lbb/j;)V

    iput-object v0, p0, Lbb/j;->c:Lbb/k;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    const/4 v0, 0x0

    iput v0, p0, Lbb/j;->d:I

    return-void
.end method

.method static synthetic a(Lbb/j;)Lbb/l;
    .locals 1

    iget-object v0, p0, Lbb/j;->b:Lbb/l;

    return-object v0
.end method

.method static synthetic a(Lbb/j;Lbb/l;)Lbb/l;
    .locals 0

    iput-object p1, p0, Lbb/j;->b:Lbb/l;

    return-object p1
.end method

.method static synthetic b(Lbb/j;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic c(Lbb/j;)J
    .locals 2

    iget-wide v0, p0, Lbb/j;->e:J

    return-wide v0
.end method

.method static synthetic d(Lbb/j;)I
    .locals 1

    iget v0, p0, Lbb/j;->d:I

    return v0
.end method

.method static synthetic e(Lbb/j;)I
    .locals 1

    invoke-super {p0}, Lbb/f;->j()I

    move-result v0

    return v0
.end method

.method static synthetic f(Lbb/j;)Lbb/w;
    .locals 1

    iget-object v0, p0, Lbb/j;->f:Lbb/w;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized L_()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lbb/f;->L_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbb/j;->b:Lbb/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)I
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected a_(Lbb/s;)V
    .locals 7

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lbb/l;

    invoke-virtual {v1}, Lbb/o;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1}, Lbb/o;->f()LaN/B;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1}, Lbb/o;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lbb/l;-><init>(Lbb/j;Lbb/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lbb/j;->b:Lbb/l;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbb/j;->c:Lbb/k;

    invoke-virtual {v0}, Lbb/k;->a()V

    invoke-static {p1}, Lbb/j;->f(Lbb/s;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbb/z;

    invoke-direct {v0, p1}, Lbb/z;-><init>(Lbb/s;)V

    invoke-virtual {p0, v0}, Lbb/j;->a(Lbb/z;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "r"

    return-object v0
.end method

.method public c()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lbb/j;->d:I

    return-void
.end method

.method public d()[I
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
