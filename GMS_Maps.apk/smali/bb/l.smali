.class Lbb/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbb/j;

.field private final b:Lbm/i;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:LaN/B;

.field private final e:Lbb/s;

.field private final f:I

.field private final g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lbb/j;Lbb/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    iput-object p1, p0, Lbb/l;->a:Lbb/j;

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p2, p0, Lbb/l;->e:Lbb/s;

    iput-object p3, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p4, p0, Lbb/l;->d:LaN/B;

    iput p5, p0, Lbb/l;->f:I

    iput-object p6, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remoteSuggest ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lbm/i;

    const-string v2, "rsd"

    const/16 v3, 0x16

    invoke-direct {v1, v0, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lbb/l;->b:Lbm/i;

    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    return-void
.end method

.method private a(I)I
    .locals 1

    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->e(Lbb/j;)I

    move-result v0

    return v0
.end method

.method private k()Lbb/z;
    .locals 14

    const/4 v13, 0x7

    const/4 v12, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x2

    new-instance v5, Lbb/z;

    iget-object v0, p0, Lbb/l;->e:Lbb/s;

    invoke-direct {v5, v0}, Lbb/z;-><init>(Lbb/s;)V

    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {v6, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-virtual {v6, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    new-instance v3, Lbb/y;

    invoke-direct {v3}, Lbb/y;-><init>()V

    invoke-virtual {v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lbb/y;->a(Ljava/lang/String;)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v8}, Lbb/y;->a(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v4}, Lbb/y;->b(I)Lbb/y;

    move-result-object v3

    invoke-direct {p0, v8}, Lbb/l;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lbb/y;->c(I)Lbb/y;

    move-result-object v3

    iget-object v9, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v9, v8}, Lbb/j;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lbb/y;->d(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v7}, Lbb/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;

    move-result-object v9

    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v10

    invoke-virtual {v10, v3}, Lbb/y;->d(Ljava/lang/String;)Lbb/y;

    :cond_0
    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lbb/y;->c(Ljava/lang/String;)Lbb/y;

    :cond_1
    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v3

    invoke-virtual {v9, v3}, Lbb/y;->a(LaN/B;)Lbb/y;

    :cond_2
    move v3, v1

    :goto_1
    const/16 v10, 0xb

    invoke-virtual {v7, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    if-ge v3, v10, :cond_3

    const/16 v10, 0xb

    invoke-virtual {v7, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    invoke-virtual {v9, v10}, Lbb/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v9}, Lbb/y;->a()Lbb/w;

    move-result-object v3

    invoke-virtual {v5, v3}, Lbb/z;->a(Lbb/w;)V

    if-ne v8, v12, :cond_4

    move v2, v4

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_5
    if-eqz v2, :cond_6

    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->f(Lbb/j;)Lbb/w;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v0}, Lbb/s;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->f(Lbb/j;)Lbb/w;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbb/z;->a(Lbb/w;)V

    :cond_6
    return-object v5
.end method

.method private l()V
    .locals 1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/l;

    if-ne v0, p0, :cond_0

    monitor-exit p0

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v0}, Lbb/l;->Z()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public Z()V
    .locals 1

    invoke-super {p0}, Law/a;->Z()V

    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gR;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x5

    iget-object v2, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v2}, Lbb/j;->d(Lbb/j;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v1, p0, Lbb/l;->f:I

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    iget v2, p0, Lbb/l;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v1, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    iget-object v1, p0, Lbb/l;->d:LaN/B;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lbb/l;->d:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3

    const/16 v1, 0xc

    iget-object v2, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gR;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    return v0
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x4c

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 3

    const/4 v1, 0x3

    const/4 v2, 0x1

    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    :cond_0
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbb/l;->z_()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lbb/l;->Z()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lbb/l;->l()V

    invoke-direct {p0}, Lbb/l;->k()Lbb/z;

    move-result-object v0

    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-static {v1}, Lbb/f;->f(Lbb/s;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    invoke-virtual {v1}, Lbb/o;->b()V

    :cond_4
    iget-object v1, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v1, v0, v2}, Lbb/j;->a(Lbb/z;Z)V

    goto :goto_0
.end method

.method public u_()V
    .locals 4

    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    invoke-direct {p0}, Lbb/l;->l()V

    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v0}, Lbb/j;->i()Z

    const/4 v0, 0x1

    iget-object v1, p0, Lbb/l;->a:Lbb/j;

    new-instance v2, Lbb/z;

    iget-object v3, p0, Lbb/l;->e:Lbb/s;

    invoke-direct {v2, v3}, Lbb/z;-><init>(Lbb/s;)V

    invoke-virtual {v1, v2, v0}, Lbb/j;->a(Lbb/z;Z)V

    return-void
.end method
