.class public Lcom/google/android/apps/circles/people/AudienceView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f040111

    invoke-direct {p0, v0}, Lcom/google/android/apps/circles/people/AudienceView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/circles/people/AudienceView;->addView(Landroid/view/View;)V

    const v0, 0x7f1002fd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/circles/people/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/circles/people/AudienceView;->a:Landroid/view/ViewGroup;

    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/circles/people/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setAudience(Ljava/util/List;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/circles/people/AudienceView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    move v2, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aD;

    const v1, 0x7f040112

    invoke-direct {p0, v1}, Lcom/google/android/apps/circles/people/AudienceView;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-eq v4, v5, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->g()I

    move-result v4

    invoke-virtual {v1, v4, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/circles/people/AudienceView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method
