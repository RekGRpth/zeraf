.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;
.super Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    return-void
.end method


# virtual methods
.method public c()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->b:Lr/I;

    invoke-virtual {v3}, Lr/I;->d()Z

    move-result v3

    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v4

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    if-nez v4, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_3

    const-string v0, "r"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_4

    const-string v0, "c"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v4, :cond_5

    const-string v0, "n"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v4, "n"

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    :cond_1
    return v1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    const-string v0, ""

    goto :goto_3
.end method

.method protected d()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->b:Lr/I;

    invoke-virtual {v0}, Lr/I;->d()Z

    move-result v0

    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_3

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_1

    const-string v0, "c"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v1, :cond_2

    const-string v0, "n"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v3, "u"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_2
.end method
