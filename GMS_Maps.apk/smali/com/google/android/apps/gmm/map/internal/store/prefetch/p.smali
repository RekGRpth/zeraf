.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
.implements Li/f;


# instance fields
.field private final a:Li/f;

.field private final b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private volatile c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Li/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->i()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    invoke-interface {v0}, Li/f;->a()V

    return-void
.end method

.method public a(Lo/aq;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    invoke-interface {v0}, Li/f;->b()Z

    move-result v0

    return v0
.end method

.method public c()Lo/aq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    return-void
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->e()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v0

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v0

    return v0
.end method

.method i()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    return-void
.end method
