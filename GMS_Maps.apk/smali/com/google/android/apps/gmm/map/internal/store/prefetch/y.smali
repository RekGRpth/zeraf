.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Class;

.field private volatile c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field private d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field private f:Z

.field private final g:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z

    return v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Li/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Li/f;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f()V

    :cond_0
    return-void
.end method

.method public declared-synchronized b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    return-object v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h()V

    return-void
.end method

.method public declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
