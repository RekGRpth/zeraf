.class public Lcom/google/android/apps/common/offerslib/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/apps/common/offerslib/b;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/common/offerslib/b;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->c:Lcom/google/android/apps/common/offerslib/b;

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/common/offerslib/a;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/common/offerslib/a;->c:Lcom/google/android/apps/common/offerslib/b;

    invoke-virtual {p0, p4}, Lcom/google/android/apps/common/offerslib/a;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p5}, Lcom/google/android/apps/common/offerslib/a;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p6}, Lcom/google/android/apps/common/offerslib/a;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/a;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v0, "appId is not initialized"

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/a;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v0, "appVersion is not initialized"

    :cond_1
    if-eqz v0, :cond_2

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/a;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->e:Ljava/lang/String;

    const-string v1, "a.1"

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/a;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/common/offerslib/a;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/common/offerslib/z;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->d:Ljava/lang/String;

    return-void

    :cond_0
    const-string p1, "https://was.sandbox.google.com/offers/m?sky=offers"

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/common/offerslib/a;->g:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/a;->f:Ljava/lang/String;

    return-void

    :cond_0
    const-string p1, "https://www.google.com/offers/mrpc"

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/android/apps/common/offerslib/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->c:Lcom/google/android/apps/common/offerslib/b;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/common/offerslib/a;->g:Z

    return v0
.end method
