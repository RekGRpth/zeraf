.class public Lcom/google/android/apps/common/offerslib/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/apps/common/offerslib/w;


# instance fields
.field private b:Laf/m;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/w;->f:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/apps/common/offerslib/w;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/apps/common/offerslib/w;
    .locals 3

    const-class v1, Lcom/google/android/apps/common/offerslib/w;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/common/offerslib/w;->a:Lcom/google/android/apps/common/offerslib/w;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/common/offerslib/w;

    const-string v2, "https://www.google.com/offers/mrpc"

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/common/offerslib/w;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/common/offerslib/w;->a:Lcom/google/android/apps/common/offerslib/w;

    :cond_0
    sget-object v0, Lcom/google/android/apps/common/offerslib/w;->a:Lcom/google/android/apps/common/offerslib/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/w;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/commerce/wireless/topiary/R;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/R;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/R;->a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/R;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/w;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/R;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/R;

    move-result-object v0

    const-string v1, "sierra"

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/R;->b(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/R;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/R;->a(Z)Lcom/google/commerce/wireless/topiary/R;

    move-result-object v0

    invoke-static {v0}, Laf/m;->a(Lcom/google/commerce/wireless/topiary/R;)Laf/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/w;->b:Laf/m;

    return-void
.end method


# virtual methods
.method public a(Laf/g;Landroid/accounts/Account;)V
    .locals 4

    new-instance v0, Lcom/google/commerce/wireless/topiary/K;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/K;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/K;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/K;

    invoke-virtual {p1}, Laf/g;->q()Laf/i;

    move-result-object v1

    const-string v2, "a.1"

    invoke-virtual {v1, v2}, Laf/i;->c(Ljava/lang/String;)Laf/i;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laf/i;->a(Ljava/lang/String;)Laf/i;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laf/i;->b(Ljava/lang/String;)Laf/i;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->b:Laf/m;

    if-nez v2, :cond_2

    const-string v0, "OfferLogClient"

    const-string v1, "stub == null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/w;->b:Laf/m;

    invoke-virtual {v1}, Laf/i;->b()Laf/g;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/common/offerslib/x;

    invoke-direct {v3, p0}, Lcom/google/android/apps/common/offerslib/x;-><init>(Lcom/google/android/apps/common/offerslib/w;)V

    invoke-virtual {v2, v0, v1, v3}, Laf/m;->a(Lcom/google/commerce/wireless/topiary/K;Laf/g;Lcom/google/commerce/wireless/topiary/I;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/common/offerslib/a;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/w;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/w;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/common/offerslib/w;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
