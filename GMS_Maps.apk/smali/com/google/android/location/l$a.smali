.class Lcom/google/android/location/l$a;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/l;


# direct methods
.method private constructor <init>(Lcom/google/android/location/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/l;Lcom/google/android/location/l$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/l$a;-><init>(Lcom/google/android/location/l;)V

    return-void
.end method

.method private j()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    iget-object v0, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/google/android/location/l;->j:J

    iget-object v0, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    iget-object v0, v0, Lcom/google/android/location/l;->b:Lcom/google/android/location/os/i;

    const/16 v1, 0x9

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    iget-object v0, v0, Lcom/google/android/location/l;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    iget-wide v2, v2, Lcom/google/android/location/l;->j:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/l$a;->a:Lcom/google/android/location/l;

    invoke-static {v0}, Lcom/google/android/location/l;->a(Lcom/google/android/location/l;)Lcom/google/android/location/m;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/m;->a(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/l$a;->j()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/l$a;->j()V

    return-void
.end method
