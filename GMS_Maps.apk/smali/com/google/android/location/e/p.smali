.class public Lcom/google/android/location/e/p;
.super Lcom/google/android/location/e/e;
.source "SourceFile"


# instance fields
.field private final o:I

.field private final p:I

.field private final q:I


# direct methods
.method public constructor <init>(JIIIIIII)V
    .locals 15

    const/4 v4, 0x4

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/location/e/p;->a:Ljava/util/List;

    const/4 v10, -0x1

    const/4 v11, -0x1

    const v12, 0x7fffffff

    const v13, 0x7fffffff

    move-object v1, p0

    move-wide/from16 v2, p1

    move/from16 v5, p5

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v14, p9

    invoke-direct/range {v1 .. v14}, Lcom/google/android/location/e/e;-><init>(JIIIILjava/lang/String;Ljava/util/List;IIIII)V

    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/location/e/p;->o:I

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/location/e/p;->p:I

    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/location/e/p;->q:I

    return-void
.end method


# virtual methods
.method public a(JI)Lcom/google/android/location/e/e;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/e/p;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e/p;->n:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/p;->n:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget v0, p0, Lcom/google/android/location/e/p;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/location/e/p;->q:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/location/e/p;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/location/e/p;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/location/e/p;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/location/e/p;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/location/e/p;

    iget v1, p0, Lcom/google/android/location/e/p;->o:I

    iget v2, p1, Lcom/google/android/location/e/p;->o:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/e/p;->p:I

    iget v2, p1, Lcom/google/android/location/e/p;->p:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " pci: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tac "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timingAdvance "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/p;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
