.class public Lcom/google/android/location/e/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:Lcom/google/android/location/e/e;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    return-void
.end method

.method public static a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/e/e;->a(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/android/location/e/e;->h()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e$a;

    const/4 v3, 0x3

    invoke-virtual {v0, p0}, Lcom/google/android/location/e/e$a;->a(Lcom/google/android/location/e/e;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/f;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/e/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iput-object p1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-static {v0, p2, p3}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/e;

    iget-object v3, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    invoke-virtual {v3}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/location/e/e;->a(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-void
.end method

.method public b()Lcom/google/android/location/e/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    return-object v0
.end method

.method protected clone()Ljava/lang/Object;
    .locals 3

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/f;

    iget-object v1, p0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    iput-object v1, v0, Lcom/google/android/location/e/f;->a:Lcom/google/android/location/e/e;

    new-instance v1, Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/google/android/location/e/f;->b:Ljava/util/List;

    return-object v0
.end method
