.class public Lcom/google/android/location/g/k;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/k$1;,
        Lcom/google/android/location/g/k$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/location/g/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/g/k$a",
            "<",
            "Lcom/google/android/location/e/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/location/g/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/g/k$a",
            "<",
            "Lcom/google/android/location/e/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/g/k$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/g/k$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    new-instance v0, Lcom/google/android/location/g/k$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/g/k$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/s;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/s;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/e/s;)V
    .locals 2

    sget-object v0, Lcom/google/android/location/g/k$1;->a:[I

    invoke-virtual {p1}, Lcom/google/android/location/e/s;->a()Lcom/google/android/location/e/s$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/e/s$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/k$a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/k$a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/g/k;->b:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/g/k;->a:Lcom/google/android/location/g/k$a;

    invoke-virtual {v0}, Lcom/google/android/location/g/k$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
