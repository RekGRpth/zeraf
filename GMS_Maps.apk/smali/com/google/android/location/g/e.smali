.class public Lcom/google/android/location/g/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/g/p;

.field private final b:Lcom/google/android/location/g/a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;Lcom/google/android/location/g/l;Lcom/google/android/location/os/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/g/l;",
            "Lcom/google/android/location/os/i;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3, p2, p4}, Lcom/google/android/location/g/p;->a(Lcom/google/android/location/g/l;Lcom/google/android/location/b/i;Lcom/google/android/location/os/i;)Lcom/google/android/location/g/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    new-instance v0, Lcom/google/android/location/g/a;

    invoke-direct {v0, p1, p4}, Lcom/google/android/location/g/a;-><init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V

    iput-object v0, p0, Lcom/google/android/location/g/e;->b:Lcom/google/android/location/g/a;

    return-void
.end method

.method private a(Lcom/google/android/location/e/E;Lcom/google/android/location/e/E;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/location/e/D;->d:Lcom/google/android/location/e/o$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v1, v2, :cond_0

    if-eq p1, p2, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/location/g/e;->a:Lcom/google/android/location/g/p;

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/g/p;->a(Ljava/util/List;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)Lcom/google/android/location/e/o;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v3, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p2, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_2

    move v3, v1

    :goto_1
    if-nez v0, :cond_3

    if-nez v3, :cond_3

    const/4 p2, 0x0

    :cond_0
    :goto_2
    return-object p2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_0

    if-nez v3, :cond_4

    move-object p2, p1

    goto :goto_2

    :cond_4
    iget-object v3, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget-object v4, p2, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    iget v5, v3, Lcom/google/android/location/e/w;->c:I

    iget v6, v4, Lcom/google/android/location/e/w;->c:I

    add-int/2addr v5, v6

    const v6, 0x3567e0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    if-gt v0, v5, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    iget v0, v3, Lcom/google/android/location/e/w;->c:I

    iget v3, v4, Lcom/google/android/location/e/w;->c:I

    if-le v0, v3, :cond_7

    :cond_5
    :goto_4
    if-nez v1, :cond_0

    move-object p2, p1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    iget v0, v3, Lcom/google/android/location/e/w;->d:I

    iget v3, v4, Lcom/google/android/location/e/w;->d:I

    if-lt v0, v3, :cond_5

    move v1, v2

    goto :goto_4
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/f;Lcom/google/android/location/e/E;Lcom/google/android/location/e/g;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/t;
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/g/e;->b:Lcom/google/android/location/g/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/g/a;->a(Lcom/google/android/location/e/f;)Lcom/google/android/location/e/c;

    move-result-object v1

    if-nez p4, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/android/location/e/c;->d:Lcom/google/android/location/e/o$a;

    sget-object v2, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v0, v2, :cond_0

    iget-object v0, v1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    new-instance p4, Lcom/google/android/location/e/w;

    iget v2, v0, Lcom/google/android/location/e/w;->a:I

    iget v3, v0, Lcom/google/android/location/e/w;->b:I

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    mul-int/lit8 v0, v0, 0x4

    invoke-direct {p4, v2, v3, v0}, Lcom/google/android/location/e/w;-><init>(III)V

    :cond_0
    if-nez p3, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p2, v0, p4}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/e/E;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/D;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/g/e;->a(Lcom/google/android/location/e/o;Lcom/google/android/location/e/o;)Lcom/google/android/location/e/o;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    iget-object v3, p3, Lcom/google/android/location/e/g;->d:Lcom/google/android/location/e/o$a;

    sget-object v4, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-ne v3, v4, :cond_1

    move-object v0, p3

    :cond_1
    new-instance v3, Lcom/google/android/location/e/t;

    invoke-direct {v3, v0, v2, v1, p3}, Lcom/google/android/location/e/t;-><init>(Lcom/google/android/location/e/o;Lcom/google/android/location/e/D;Lcom/google/android/location/e/c;Lcom/google/android/location/e/g;)V

    return-object v3

    :cond_2
    iget-object v0, p3, Lcom/google/android/location/e/g;->a:Lcom/google/android/location/e/E;

    goto :goto_0
.end method
