.class public Lcom/google/android/location/f;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f$1;,
        Lcom/google/android/location/f$a;
    }
.end annotation


# instance fields
.field g:J

.field h:J

.field i:J

.field private final j:Lcom/google/android/location/u;

.field private final k:Lcom/google/android/location/g;

.field private l:Lcom/google/android/location/c/r;

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/u;Lcom/google/android/location/g;)V
    .locals 7

    const-string v1, "CalibrationCollector"

    sget-object v6, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->g:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->i:J

    iput-object p6, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/u;

    iput-object p5, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p7, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-direct {p0, p5}, Lcom/google/android/location/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/f;->g:J

    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/f;->h:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/f;->n:I

    return v0
.end method

.method static a(JLjava/util/List;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v9, 0x2

    const-wide v7, 0x3ef4f8b588e368f1L

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-static {p2}, Lcom/google/android/location/f;->a(Ljava/util/List;)[[D

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    aget-object v2, v1, v5

    aget-wide v2, v2, v6

    cmpg-double v2, v2, v7

    if-gez v2, :cond_0

    aget-object v2, v1, v5

    aget-wide v2, v2, v5

    cmpg-double v2, v2, v7

    if-gez v2, :cond_0

    aget-object v2, v1, v5

    aget-wide v2, v2, v9

    cmpg-double v2, v2, v7

    if-gez v2, :cond_0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->X:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    aget-object v0, v1, v6

    aget-wide v3, v0, v6

    double-to-float v0, v3

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v0, v1, v6

    aget-wide v3, v0, v5

    double-to-float v0, v3

    invoke-virtual {v2, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x3

    aget-object v1, v1, v6

    aget-wide v3, v1, v9

    double-to-float v1, v3

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    invoke-virtual {v2, v0, p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method static a(Lcom/google/android/location/os/i;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    const-string v3, "calibration"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    sget-object v1, Lcom/google/android/location/j/a;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v3, v1}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(II)V
    .locals 7

    iput p1, p0, Lcom/google/android/location/f;->n:I

    iput p2, p0, Lcom/google/android/location/f;->o:I

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/c/F;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    invoke-static {v1}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v1

    const-wide/16 v3, 0x3a98

    new-instance v5, Lcom/google/android/location/f$a;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/google/android/location/f$a;-><init>(Lcom/google/android/location/f;Lcom/google/android/location/f$1;)V

    iget-object v6, p0, Lcom/google/android/location/f;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    iget-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->j(J)V

    return-void
.end method

.method public static a(Ljava/util/List;)[[D
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)[[D"
        }
    .end annotation

    if-eqz p0, :cond_0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    check-cast v2, [[D

    :goto_0
    return-object v2

    :cond_1
    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v15

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide/from16 v21, v2

    move-wide/from16 v23, v4

    move-wide/from16 v3, v21

    move-wide/from16 v25, v6

    move-wide/from16 v5, v23

    move-wide/from16 v27, v8

    move-wide/from16 v7, v25

    move-wide/from16 v29, v10

    move-wide/from16 v9, v27

    move-wide/from16 v31, v12

    move-wide/from16 v13, v31

    move-wide/from16 v11, v29

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v13, v13, v17

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v11, v11, v17

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v9, v9, v17

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v7, v7, v17

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v5, v5, v17

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v2, v3, v17

    move-wide v3, v2

    goto :goto_1

    :cond_2
    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v13, v13, v16

    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v9, v9, v16

    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v5, v5, v16

    const/4 v2, 0x2

    new-array v2, v2, [[D

    const/16 v16, 0x0

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-wide v13, v17, v18

    const/16 v18, 0x1

    aput-wide v9, v17, v18

    const/16 v18, 0x2

    aput-wide v5, v17, v18

    aput-object v17, v2, v16

    const/16 v16, 0x1

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    int-to-double v0, v15

    move-wide/from16 v19, v0

    div-double v11, v11, v19

    const-wide/high16 v19, 0x4000000000000000L

    move-wide/from16 v0, v19

    invoke-static {v13, v14, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    sub-double/2addr v11, v13

    aput-wide v11, v17, v18

    const/4 v11, 0x1

    int-to-double v12, v15

    div-double/2addr v7, v12

    const-wide/high16 v12, 0x4000000000000000L

    invoke-static {v9, v10, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    sub-double/2addr v7, v9

    aput-wide v7, v17, v11

    const/4 v7, 0x2

    int-to-double v8, v15

    div-double/2addr v3, v8

    const-wide/high16 v8, 0x4000000000000000L

    invoke-static {v5, v6, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    sub-double/2addr v3, v5

    aput-wide v3, v17, v7

    aput-object v17, v2, v16

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/location/f;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/f;->o:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/location/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J
    .locals 6

    const-wide/16 v0, -0x1

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method static b(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    const/4 v1, 0x0

    const-wide v5, 0x3ef4f8b588e368f1L

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/location/f;->a(Ljava/util/List;)[[D

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    aget-object v3, v2, v0

    aget-wide v3, v3, v1

    cmpg-double v3, v3, v5

    if-gez v3, :cond_1

    aget-object v3, v2, v0

    aget-wide v3, v3, v0

    cmpg-double v3, v3, v5

    if-gez v3, :cond_1

    aget-object v2, v2, v0

    const/4 v3, 0x2

    aget-wide v2, v2, v3

    cmpg-double v2, v2, v5

    if-gez v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/location/f;)Lcom/google/android/location/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/f;)Lcom/google/android/location/c/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    return-object v0
.end method

.method private e()J
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    iget-object v3, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    cmp-long v0, v0, v3

    if-lez v0, :cond_3

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x7

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_1
    const/16 v1, 0xb

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    :cond_2
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0

    :cond_3
    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/location/f;)J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method private j(J)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/f;->i:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/f;->i:J

    iget-object v0, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x5

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_0
    return-void
.end method

.method private k(J)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, v1}, Lcom/google/android/location/u;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->l(J)Z

    move-result v2

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private l(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/f;->h:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 2

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->i:J

    :cond_0
    return-void
.end method

.method a(IIZ)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/android/location/os/h;)V
    .locals 0

    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method a(Z)V
    .locals 0

    return-void
.end method

.method protected a(J)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/f;->f:Lcom/google/android/location/a$c;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lcom/google/android/location/e/E;)V
    .locals 0

    return-void
.end method

.method b(Z)V
    .locals 0

    return-void
.end method

.method protected b(J)Z
    .locals 6

    const-wide/32 v4, 0x36ee80

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->k(J)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/location/a$c;->g:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/f;->f:Lcom/google/android/location/a$c;

    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->f()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v2}, Lcom/google/android/location/g;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->g()I

    move-result v1

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/f;->a(II)V

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/f;->h:J

    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_1

    :cond_3
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    cmp-long v0, p1, v2

    if-ltz v0, :cond_4

    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_1

    :cond_4
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_1
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method c(Z)V
    .locals 0

    return-void
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method protected g(J)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
