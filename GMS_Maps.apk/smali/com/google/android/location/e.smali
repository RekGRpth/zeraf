.class public Lcom/google/android/location/e;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e$1;,
        Lcom/google/android/location/e$a;
    }
.end annotation


# static fields
.field static g:Z


# instance fields
.field A:J

.field h:J

.field i:Lcom/google/android/location/os/g;

.field j:J

.field k:I

.field l:Z

.field m:Z

.field n:Z

.field o:Lcom/google/android/location/e/e;

.field p:Lcom/google/android/location/e/E;

.field q:Lcom/google/android/location/e$a;

.field r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field s:Z

.field t:J

.field u:Z

.field v:J

.field w:Z

.field x:Lcom/google/android/location/os/g;

.field y:Z

.field z:Lcom/google/android/location/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/e;->g:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;)V
    .locals 7

    const-string v1, "BurstCollector"

    sget-object v6, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e;->h:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->j:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/e;->k:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->m:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->n:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    new-instance v0, Lcom/google/android/location/e$a;

    const/16 v1, 0x64

    const/16 v2, 0x2ee0

    const v3, 0xea60

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/e$a;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->s:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->t:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->v:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->w:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->y:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->A:J

    new-instance v0, Lcom/google/android/location/d;

    iget-object v1, p2, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-direct {v0, p1, p5, v1, p6}, Lcom/google/android/location/d;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/i/a;Lcom/google/android/location/os/h;Lcom/google/android/location/a/b;)V

    iput-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    return-void
.end method

.method private a(JLcom/google/android/location/os/g;)Z
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->j(J)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-interface {p3}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/e;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v2}, Lcom/google/android/location/a$b;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 10

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    iget-boolean v1, p0, Lcom/google/android/location/e;->w:Z

    iget-object v2, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/e;->h:J

    iget-boolean v6, p0, Lcom/google/android/location/e;->n:Z

    iget-object v7, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/e$a;->a(ZJJZLcom/google/android/location/os/g;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e;->d:Lcom/google/android/location/y;

    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/y;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    iput-boolean v8, p0, Lcom/google/android/location/e;->u:Z

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/e;->t:J

    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    move v0, v8

    :goto_0
    iget-object v1, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v1}, Lcom/google/android/location/e$a;->b()V

    return v0

    :cond_0
    move v0, v9

    goto :goto_0
.end method

.method private f()V
    .locals 2

    const/4 v1, 0x2

    iget-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/e;->u:Z

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    :cond_0
    return-void
.end method

.method private j(J)Z
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/location/e;->m:Z

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x75300

    :goto_0
    iget-wide v2, p0, Lcom/google/android/location/e;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/location/e;->j:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const-wide/32 v0, 0xdbba0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private k(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/e;->A:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/e;->A:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l(J)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/location/e$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v3}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v1, v2

    :cond_0
    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->m(J)Z

    move-result v3

    iget-wide v4, p0, Lcom/google/android/location/e;->h:J

    sub-long v4, p1, v4

    const-wide/32 v6, 0x222e0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->m()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/location/e;->s:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/location/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private m(J)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    sub-long v2, p1, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v2}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/16 v4, 0x55f0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private n(J)V
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    iput v4, p0, Lcom/google/android/location/e;->k:I

    :cond_1
    :goto_0
    iput-wide p1, p0, Lcom/google/android/location/e;->j:J

    :cond_2
    iput-object v5, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/e;->h:J

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    iput-object v5, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-boolean v3, p0, Lcom/google/android/location/e;->y:Z

    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    iput-wide v6, p0, Lcom/google/android/location/e;->v:J

    iput-boolean v3, p0, Lcom/google/android/location/e;->w:Z

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iget-object v1, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    const/16 v2, 0x19

    invoke-static {v0, v1, v2}, Lcom/google/android/location/e;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/location/e;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/e;->k:I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/e;->x:Lcom/google/android/location/os/g;

    iput-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    iput v4, p0, Lcom/google/android/location/e;->k:I

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(I)V

    return-void
.end method

.method a(IIZ)V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v3

    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_1

    if-nez p3, :cond_0

    float-to-double v4, v3

    const-wide v6, 0x3fc999999999999aL

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    if-eqz p3, :cond_3

    float-to-double v3, v3

    const-wide v5, 0x3feccccccccccccdL

    cmpl-double v0, v3, v5

    if-ltz v0, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/location/e;->m:Z

    :cond_1
    iput-boolean p3, p0, Lcom/google/android/location/e;->n:Z

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/d;->a(IIZ)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    goto :goto_0
.end method

.method a(Lcom/google/android/location/os/g;)V
    .locals 4

    const/4 v1, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/e;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/google/android/location/e;->g:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v3, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v2, v3, :cond_2

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-ne v0, v2, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/e;->i:Lcom/google/android/location/os/g;

    const/16 v2, 0x19

    invoke-static {v0, p1, v2}, Lcom/google/android/location/e;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/location/e;->k:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e$a;->a(Lcom/google/android/location/os/g;)V

    iput-boolean v1, p0, Lcom/google/android/location/e;->y:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/e;->v:J

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/e;->j(J)Z

    move-result v0

    goto :goto_2
.end method

.method a(Lcom/google/android/location/os/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Lcom/google/android/location/os/h;)V

    return-void
.end method

.method a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/e;->A:J

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    goto :goto_0
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method a(Z)V
    .locals 0

    return-void
.end method

.method b(Lcom/google/android/location/e/E;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    return-void
.end method

.method b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->a(Z)V

    return-void
.end method

.method protected b(J)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-wide/16 v6, -0x1

    iget-object v2, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v2}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->l(J)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->k(J)Z

    move-result v3

    if-eqz v3, :cond_4

    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    iget-object v2, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v2}, Lcom/google/android/location/d;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-wide p1, p0, Lcom/google/android/location/e;->v:J

    iput-boolean v0, p0, Lcom/google/android/location/e;->w:Z

    :goto_0
    move v1, v0

    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    iput-wide p1, p0, Lcom/google/android/location/e;->h:J

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/e;->A:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    iget-wide v2, p0, Lcom/google/android/location/e;->A:J

    sub-long v2, p1, v2

    const-wide/32 v4, 0xea60

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    if-nez v1, :cond_2

    iput-wide v6, p0, Lcom/google/android/location/e;->A:J

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    :cond_2
    return v1

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0}, Lcom/google/android/location/d;->b()V

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/location/e;->a(JLcom/google/android/location/os/g;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-wide v6, p0, Lcom/google/android/location/e;->v:J

    iput-boolean v1, p0, Lcom/google/android/location/e;->w:Z

    move v1, v0

    goto :goto_1
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method public c(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/location/e;->s:Z

    iget-object v0, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/d;->b(Z)V

    return-void
.end method

.method protected c(J)Z
    .locals 10

    const/4 v9, 0x2

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v0}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v8

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->l(J)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/e;->z:Lcom/google/android/location/d;

    invoke-virtual {v1}, Lcom/google/android/location/d;->b()V

    invoke-direct {p0}, Lcom/google/android/location/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->n(J)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/location/e;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    if-ne v1, v2, :cond_1

    iget-wide v1, p0, Lcom/google/android/location/e;->v:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    invoke-interface {v8}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x55f0

    add-long/2addr v2, v4

    invoke-interface {v1, v9, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_1
    :goto_1
    iput-boolean v7, p0, Lcom/google/android/location/e;->y:Z

    return v0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/location/e;->y:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    iget-object v1, p0, Lcom/google/android/location/e;->q:Lcom/google/android/location/e$a;

    invoke-virtual {v1}, Lcom/google/android/location/e$a;->a()Lcom/google/android/location/os/g;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/e;->e:Lcom/google/android/location/a$b;

    iget-object v5, p0, Lcom/google/android/location/e;->o:Lcom/google/android/location/e/e;

    iget-object v6, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/e$a;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/a$b;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/e;->p:Lcom/google/android/location/e/E;

    :cond_3
    move v0, v7

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    iget-wide v2, p0, Lcom/google/android/location/e;->v:J

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    invoke-interface {v1, v9, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_1
.end method

.method d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/e;->l:Z

    return v0
.end method

.method protected f(J)Z
    .locals 7

    const-wide/16 v5, 0x3a98

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/e;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/e;->n(J)V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/e;->t:J

    invoke-direct {p0}, Lcom/google/android/location/e;->f()V

    :goto_0
    return v0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/location/e;->t:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v5

    if-ltz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/e;->f()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/e;->b:Lcom/google/android/location/os/i;

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/google/android/location/e;->t:J

    add-long/2addr v3, v5

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/i;->a(IJ)V

    goto :goto_0
.end method
