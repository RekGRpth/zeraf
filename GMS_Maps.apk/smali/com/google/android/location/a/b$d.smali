.class abstract Lcom/google/android/location/a/b$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "d"
.end annotation


# instance fields
.field final synthetic b:Lcom/google/android/location/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$d;-><init>(Lcom/google/android/location/a/b;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(Lcom/google/android/location/a/n$b;)V
    .locals 0

    return-void
.end method

.method protected a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Z)Z

    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/location/os/i;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;Z)Z

    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->d(Lcom/google/android/location/a/b;)V

    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$b;

    iget-object v2, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$b;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    return-void
.end method

.method protected f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->j(Lcom/google/android/location/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->k(Lcom/google/android/location/a/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
