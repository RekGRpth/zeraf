.class Lcom/google/android/location/a/d$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:D

.field private final b:D


# direct methods
.method constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/a/d$c;->a:D

    iput-wide p3, p0, Lcom/google/android/location/a/d$c;->b:D

    return-void
.end method

.method private a()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/d$c;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/a/d$c;)D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/a/d$c;->a:D

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/location/a/d$c;)D
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/a/d$c;->a()D

    move-result-wide v0

    return-wide v0
.end method
