.class final Lcom/google/android/location/w$a;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/w;


# direct methods
.method private constructor <init>(Lcom/google/android/location/w;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/w;Lcom/google/android/location/w$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/w$a;-><init>(Lcom/google/android/location/w;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-boolean v0, v0, Lcom/google/android/location/w;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v2, v2, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    const/4 v1, 0x6

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    iget-object v1, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v1, v1, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    invoke-interface {v1, v0}, Lcom/google/android/location/c/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->c()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v0, v0, Lcom/google/android/location/w;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .locals 9

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v0, v0, Lcom/google/android/location/w;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    move v0, v8

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/location/c/H;->a()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v7, 0x3

    :goto_1
    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v1, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v1, v1, Lcom/google/android/location/w;->m:J

    iget-object v3, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v3, v3, Lcom/google/android/location/w;->m:J

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/w;->a(Lcom/google/android/location/w;JJJIZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-boolean v0, v0, Lcom/google/android/location/w;->q:Z

    if-eqz v0, :cond_2

    const/4 v7, 0x2

    goto :goto_1

    :cond_2
    move v7, v8

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    const-wide/16 v3, -0x1

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v0, v0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v0, v0, Lcom/google/android/location/w;->m:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    move v0, v8

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v1, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-wide v1, v1, Lcom/google/android/location/w;->m:J

    iget-object v5, p0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w;

    iget-object v5, v5, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    const/16 v7, 0x1d

    invoke-static/range {v0 .. v8}, Lcom/google/android/location/w;->a(Lcom/google/android/location/w;JJJIZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
