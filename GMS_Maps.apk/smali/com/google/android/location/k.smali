.class public Lcom/google/android/location/k;
.super Lcom/google/android/location/b;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/location/k/b;

.field private final g:I

.field private final h:Lcom/google/android/location/g;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/k/b;Lcom/google/android/location/g;Lcom/google/android/location/u;)V
    .locals 6

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/k;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/k/b;Lcom/google/android/location/g;Lcom/google/android/location/u;Ljava/util/Random;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/k/b;Lcom/google/android/location/g;Lcom/google/android/location/u;Ljava/util/Random;)V
    .locals 6

    const-string v1, "InOutScheduler"

    const/16 v3, 0x9

    move-object v0, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/b;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;ILcom/google/android/location/u;Ljava/util/Random;)V

    iput-object p2, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    invoke-virtual {p2}, Lcom/google/android/location/k/b;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x6

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/location/k;->g:I

    iput-object p3, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    return-void
.end method

.method private a(J)Lcom/google/android/location/e/u;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/k;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/location/k;->g:I

    div-int v1, v0, v1

    iget v2, p0, Lcom/google/android/location/k;->g:I

    iget v3, p0, Lcom/google/android/location/k;->g:I

    rem-int/2addr v0, v3

    sub-int v0, v2, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/Calendar;I)Lcom/google/android/location/e/u;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "I)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v1

    rsub-int/lit8 v0, p2, 0x6

    iget v3, p0, Lcom/google/android/location/k;->g:I

    mul-int/2addr v0, v3

    int-to-long v3, v0

    iget-object v0, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    iget-wide v5, v0, Lcom/google/android/location/k/b;->a:J

    add-long/2addr v3, v5

    cmp-long v0, v3, v1

    if-gtz v0, :cond_0

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/k;->a(J)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/google/android/location/k;->c:Ljava/util/Random;

    invoke-virtual {v3, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    int-to-long v3, v0

    add-long v0, v1, v3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {v2, v0, v1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    iget-object v0, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->n()V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/k;->a(JLcom/google/android/location/b$a;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/location/k;->g:I

    move-wide v1, v3

    goto :goto_0
.end method

.method private b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    iget-object v1, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    iget-object v1, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v1, v0}, Lcom/google/android/location/g;->a(Ljava/util/Calendar;)V

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/k;->a(Ljava/util/Calendar;I)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/util/Calendar;)I
    .locals 3

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->m()Ljava/util/Date;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v1, p1}, Lcom/google/android/location/g;->a(Ljava/util/Calendar;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {v2, p1}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v1, p1}, Lcom/google/android/location/g;->a(Ljava/util/Calendar;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->o()I

    move-result v0

    const/4 v1, 0x0

    rsub-int/lit8 v0, v0, 0x6

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/k;->c(Ljava/util/Calendar;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/location/k;->f:Lcom/google/android/location/k/b;

    invoke-virtual {v1, p1}, Lcom/google/android/location/k/b;->b(Ljava/util/Calendar;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/k;->a(Ljava/util/Calendar;I)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/location/k;->b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_0
.end method

.method protected b()Lcom/google/android/location/e/u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->k()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0
.end method

.method protected b(JLcom/google/android/location/b$a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/k;->h:Lcom/google/android/location/g;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/g;->b(J)V

    return-void
.end method
