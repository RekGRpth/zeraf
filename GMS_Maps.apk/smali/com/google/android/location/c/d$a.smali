.class Lcom/google/android/location/c/d$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Z

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZLjava/util/Set;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/c/d$a;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/location/c/d$a;->b:Z

    iput-boolean p3, p0, Lcom/google/android/location/c/d$a;->c:Z

    iput-object p4, p0, Lcom/google/android/location/c/d$a;->d:Ljava/util/Set;

    iput-boolean p5, p0, Lcom/google/android/location/c/d$a;->e:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/d$a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/d$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/location/c/j;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/location/c/j;->c()Lcom/google/android/location/c/j$a;

    move-result-object v0

    sget-object v3, Lcom/google/android/location/c/j$a;->c:Lcom/google/android/location/c/j$a;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_0
    invoke-interface {p1}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/location/c/d$a;->a(Ljava/lang/String;)Z

    move-result v3

    :goto_1
    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/location/c/d$a;->b:Z

    if-eqz v3, :cond_3

    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/c/d$a;->c:Z

    if-eqz v0, :cond_3

    :cond_1
    invoke-interface {p1}, Lcom/google/android/location/c/j;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/location/c/d$a;->e:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/c/d$a;->d:Ljava/util/Set;

    invoke-interface {p1}, Lcom/google/android/location/c/j;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    :cond_3
    return v2

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v3, v2

    goto :goto_1
.end method
