.class Lcom/google/android/location/c/s$1;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/s;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/s;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V
    .locals 8

    iput-object p1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-wide v3, p4

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-void
.end method


# virtual methods
.method protected terminated()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    invoke-static {v1}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/s;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget-object v1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v1, v1, Lcom/google/android/location/c/s;->d:Lcom/google/android/location/c/H;

    invoke-interface {v0, v1}, Lcom/google/android/location/c/l;->a(Lcom/google/android/location/c/H;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    invoke-static {v0}, Lcom/google/android/location/c/s;->b(Lcom/google/android/location/c/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->d:Lcom/google/android/location/c/H;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v1, v1, Lcom/google/android/location/c/s;->d:Lcom/google/android/location/c/H;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/H;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v1, v1, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v1, v1, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget-object v2, p0, Lcom/google/android/location/c/s$1;->a:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->d:Lcom/google/android/location/c/H;

    invoke-interface {v1, v2}, Lcom/google/android/location/c/l;->a(Lcom/google/android/location/c/H;)V

    :cond_2
    throw v0
.end method
