.class Lcom/google/android/location/c/I$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/I;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/I$a$a;
    }
.end annotation


# instance fields
.field private volatile a:Lcom/google/android/location/c/l;

.field private volatile b:Landroid/content/Context;

.field private volatile c:Lcom/google/android/location/c/j;

.field private volatile d:Lcom/google/android/location/c/y;

.field private e:Lcom/google/android/location/c/x;

.field private volatile f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lcom/google/android/location/c/r$a;

.field private h:Lcom/google/android/location/c/z;

.field private final i:Ljava/lang/Integer;

.field private final j:Lcom/google/android/location/k/a/c;

.field private final k:Lcom/google/android/location/d/a;

.field private final l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final m:Ljava/lang/Object;

.field private volatile n:Z

.field private final o:Ljava/util/concurrent/CountDownLatch;

.field private p:Lcom/google/android/location/c/l;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/j;Ljava/util/concurrent/CountDownLatch;Lcom/google/android/location/d/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Integer;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    new-instance v0, Lcom/google/android/location/c/I$a$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/I$a$1;-><init>(Lcom/google/android/location/c/I$a;)V

    iput-object v0, p0, Lcom/google/android/location/c/I$a;->p:Lcom/google/android/location/c/l;

    const-string v0, "SignalCollector.ScannerThread"

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/I$a;->setName(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iput-object p1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iput-object p7, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    iput-object p6, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    iput-object p3, p0, Lcom/google/android/location/c/I$a;->o:Ljava/util/concurrent/CountDownLatch;

    iput-object p5, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p4, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;Lcom/google/android/location/c/z;)Lcom/google/android/location/c/z;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/I$a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/c/I$a;->b()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/c/I$a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    invoke-virtual {v0}, Lcom/google/android/location/c/x;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/c/z;->a()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/r$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->g:Lcom/google/android/location/c/r$a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/c/I$a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/k/a/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/c/I$a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/c/I$a;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/c/z;->b()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 10

    const/4 v2, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/google/android/location/c/I$a$a;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/I$a$a;-><init>(Lcom/google/android/location/c/I$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    invoke-interface {v1}, Lcom/google/android/location/c/j;->i()Lcom/google/android/location/c/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->d:Lcom/google/android/location/c/y;

    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/android/location/c/x;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v0, v2}, Lcom/google/android/location/c/x;-><init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->d:Lcom/google/android/location/c/y;

    iget-object v3, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    iget-object v4, p0, Lcom/google/android/location/c/I$a;->p:Lcom/google/android/location/c/l;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/location/c/x;->a(Landroid/content/Context;Lcom/google/android/location/c/y;Lcom/google/android/location/d/a;Lcom/google/android/location/c/l;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, v7

    :goto_0
    if-nez v1, :cond_0

    :try_start_2
    iget-object v2, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    invoke-interface {v2, v0}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->o:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->release()V

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    return-void

    :cond_2
    :try_start_3
    const-string v0, "PreScanner: Nothing to scan."
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_3
    :try_start_4
    new-instance v1, Lcom/google/android/location/c/z;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v0, v2}, Lcom/google/android/location/c/z;-><init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iget-object v3, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    iget-object v4, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/c/z;->a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    move-object v0, v7

    goto :goto_0

    :cond_4
    :try_start_5
    const-string v0, "RealCollector: Nothing to scan."
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v1, v8

    :goto_1
    :try_start_6
    const-string v2, "Failed normalize configuration: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->release()V

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iput-object v7, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
