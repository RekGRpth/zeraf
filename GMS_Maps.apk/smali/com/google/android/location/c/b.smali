.class public Lcom/google/android/location/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/io/DataInputStream;

.field private d:I

.field private e:Z

.field private f:Ljava/lang/Integer;

.field private final g:Lcom/google/android/location/c/a;

.field private final h:Lcom/google/android/location/c/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/c/a;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    iput v2, p0, Lcom/google/android/location/c/b;->d:I

    iput-boolean v2, p0, Lcom/google/android/location/c/b;->e:Z

    iput-object p1, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/c/b;->g:Lcom/google/android/location/c/a;

    iput-object p3, p0, Lcom/google/android/location/c/b;->h:Lcom/google/android/location/c/a;

    return-void
.end method

.method private a(Ljava/io/DataInputStream;I)[B
    .locals 3

    new-array v1, p2, [B

    const/4 v0, 0x0

    :cond_0
    sub-int v2, p2, v0

    invoke-virtual {p1, v1, v0, v2}, Ljava/io/DataInputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_1

    add-int/2addr v0, v2

    if-ne v0, p2, :cond_0

    :cond_1
    if-eq v0, p2, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    new-instance v0, Lcom/google/android/location/c/b$a;

    const-string v1, "Unexpected end of file."

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v1
.end method

.method private c()V
    .locals 5

    const/4 v4, 0x0

    iget-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Could not found file %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    iput-boolean v4, p0, Lcom/google/android/location/c/b;->e:Z

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    :cond_1
    return-void
.end method

.method private d()V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/c/b;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()[B
    .locals 7

    const/4 v5, 0x2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/c/b;->c()V

    iget-boolean v1, p0, Lcom/google/android/location/c/b;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/location/c/b;->d:I

    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/location/c/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    new-instance v0, Lcom/google/android/location/c/b$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected end of file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v1, 0x0

    :try_start_3
    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v5, :cond_4

    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/b;->a(Ljava/io/DataInputStream;I)[B

    move-result-object v1

    move v6, v0

    move-object v0, v1

    move v1, v6

    :cond_3
    :goto_1
    iget v2, p0, Lcom/google/android/location/c/b;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/location/c/b;->d:I

    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/location/c/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    :try_start_5
    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v2, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/location/c/b;->h:Lcom/google/android/location/c/a;

    :goto_2
    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/location/c/b$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to decrypt GLocRequest: no cipher for version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/c/b;->g:Lcom/google/android/location/c/a;
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :cond_6
    :try_start_6
    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0}, Lcom/google/android/location/c/L;->b([B)[B
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/EOFException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_7
    new-instance v1, Lcom/google/android/location/c/b$a;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_7
    .catch Ljava/io/EOFException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/b;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/b;->e:Z

    :cond_0
    return-void
.end method
