.class abstract Lcom/google/android/location/c/E;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field protected final b:Lcom/google/android/location/c/l;

.field private final c:Lcom/google/android/location/c/k;

.field private volatile d:Z

.field private volatile e:Z

.field private final f:Ljava/lang/Thread;

.field private final g:Landroid/os/PowerManager$WakeLock;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/location/c/E;->d:Z

    iput-boolean v2, p0, Lcom/google/android/location/c/E;->e:Z

    new-instance v0, Lcom/google/android/location/c/E$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/E$1;-><init>(Lcom/google/android/location/c/E;)V

    iput-object v0, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    const-string v0, "No Handler specified!"

    invoke-static {p2, v0}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v3, "SignalCollector.Scanner"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-object p2, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    invoke-static {p4}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->a:Lcom/google/android/location/k/a/c;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    invoke-virtual {p2}, Lcom/google/android/location/c/k;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-ne v3, v0, :cond_1

    move v0, v1

    :goto_0
    const-string v1, "Scanner should be called in handler\'s thread."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    :cond_0
    iput-object p3, p0, Lcom/google/android/location/c/E;->b:Lcom/google/android/location/c/l;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method declared-synchronized a(J)V
    .locals 8

    const-wide v6, 0x7fffffffffffffffL

    const-wide/32 v4, 0x927c0

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/c/E;->h()V

    iget-boolean v2, p0, Lcom/google/android/location/c/E;->d:Z

    if-nez v2, :cond_3

    move v2, v0

    :goto_0
    const-string v3, "Start should be called only once!"

    invoke-static {v2, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    cmp-long v2, p1, v4

    if-lez v2, :cond_1

    :cond_0
    cmp-long v2, p1, v6

    if-nez v2, :cond_4

    :cond_1
    :goto_1
    const-string v1, "Duration should be > 0 and <= %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-wide/32 v4, 0x927c0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    cmp-long v0, p1, v6

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/location/c/k;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/location/c/E;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/E;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()V
.end method

.method declared-synchronized d()V
    .locals 2

    monitor-enter p0

    const-wide v0, 0x7fffffffffffffffL

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/c/E;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized e()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/E;->d:Z

    const-string v1, "Call start before calling stop!"

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z

    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/google/android/location/c/E;->b()V

    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected f()Lcom/google/android/location/c/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    return-object v0
.end method

.method protected declared-synchronized g()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected h()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Could not be called outside owner thread."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
