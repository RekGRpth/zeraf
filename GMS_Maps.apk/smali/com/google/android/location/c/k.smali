.class Lcom/google/android/location/c/k;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field private final b:Lcom/google/android/location/c/o;

.field private final c:Landroid/os/Handler;

.field private final d:I

.field private e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/o;Landroid/os/Handler;ILcom/google/android/location/k/a/c;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/k;->e:Z

    new-instance v0, Lcom/google/android/location/c/k$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/k$1;-><init>(Lcom/google/android/location/c/k;)V

    iput-object v0, p0, Lcom/google/android/location/c/k;->f:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    iput-object p2, p0, Lcom/google/android/location/c/k;->c:Landroid/os/Handler;

    iput p3, p0, Lcom/google/android/location/c/k;->d:I

    invoke-static {p4}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/k;->a:Lcom/google/android/location/k/a/c;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/k;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/k;->e:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/c/k;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/c/k;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/c/k;)Lcom/google/android/location/c/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/k;)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/c/k;->d:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/c/k;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/k;->c:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/k;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method a(FFFF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/c/o;->a(FFFF)V

    return-void
.end method

.method a(IFFFIJJ)V
    .locals 9

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->a(FFFIJJ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->b(FFFIJJ)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->c(FFFIJJ)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->d(FFFIJJ)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p5

    move-wide v3, p6

    move-wide/from16 v5, p8

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/c/o;->a(FIJJ)V

    goto :goto_0
.end method

.method a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/telephony/CellLocation;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;J)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/c/o;->a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    return-void
.end method

.method public a(Landroid/location/GpsStatus;J)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/c/o;->a(Landroid/location/GpsStatus;J)V

    return-void
.end method

.method a(Landroid/location/Location;J)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/c/o;->a(Landroid/location/Location;J)Z

    return-void
.end method

.method a(Lcom/google/android/location/c/E;)V
    .locals 1

    new-instance v0, Lcom/google/android/location/c/k$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/c/k$2;-><init>(Lcom/google/android/location/c/k;Lcom/google/android/location/c/E;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method a(Ljava/util/List;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;J)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/location/c/k$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/c/k$3;-><init>(Lcom/google/android/location/c/k;Ljava/util/List;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
