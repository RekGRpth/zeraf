.class Lcom/google/android/location/c/I$a$a;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/I$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/I$a;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/I$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v0}, Lcom/google/android/location/c/I$a;->b(Lcom/google/android/location/c/I$a;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v0}, Lcom/google/android/location/c/I$a;->d(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/r$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v1}, Lcom/google/android/location/c/I$a;->c(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/c/r$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v0}, Lcom/google/android/location/c/I$a;->f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v2}, Lcom/google/android/location/c/I$a;->e(Lcom/google/android/location/c/I$a;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/c/l;->a(ZZ)V

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v1}, Lcom/google/android/location/c/I$a;->e(Lcom/google/android/location/c/I$a;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    new-instance v1, Lcom/google/android/location/c/z;

    iget-object v2, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v2}, Lcom/google/android/location/c/I$a;->g(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/k/a/c;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/c/z;-><init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V

    invoke-static {v0, v1}, Lcom/google/android/location/c/I$a;->a(Lcom/google/android/location/c/I$a;Lcom/google/android/location/c/z;)Lcom/google/android/location/c/z;

    iget-object v0, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v0}, Lcom/google/android/location/c/I$a;->m(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v1}, Lcom/google/android/location/c/I$a;->h(Lcom/google/android/location/c/I$a;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v2}, Lcom/google/android/location/c/I$a;->i(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/j;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v3}, Lcom/google/android/location/c/I$a;->j(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/d/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v4}, Lcom/google/android/location/c/I$a;->k(Lcom/google/android/location/c/I$a;)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v5}, Lcom/google/android/location/c/I$a;->l(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v6}, Lcom/google/android/location/c/I$a;->f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/c/z;->a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "RealScanner: Nothing to scan."

    iget-object v2, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v2}, Lcom/google/android/location/c/I$a;->f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/c/I$a$a;->a:Lcom/google/android/location/c/I$a;

    invoke-static {v2}, Lcom/google/android/location/c/I$a;->f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;)V

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a$a;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_1
    monitor-exit v7

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/location/c/I$a$a;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
