.class public Lcom/google/android/location/c/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/f$3;,
        Lcom/google/android/location/c/f$c;,
        Lcom/google/android/location/c/f$a;,
        Lcom/google/android/location/c/f$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Landroid/os/Handler;

.field private final g:Lcom/google/android/location/c/g;

.field private final h:Lcom/google/android/location/k/a/c;

.field private i:[B

.field private j:[B

.field private volatile k:Z

.field private volatile l:Z

.field private m:Ljava/util/concurrent/CountDownLatch;

.field private n:Ljava/lang/Runnable;

.field private o:Lcom/google/android/location/c/f$c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, ".lck"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "sessionId"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "uploadedSeq"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "sessionSummary"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BZLcom/google/android/location/c/g;Landroid/os/Looper;Lcom/google/android/location/k/a/c;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/location/c/f;->k:Z

    iput-boolean v1, p0, Lcom/google/android/location/c/f;->l:Z

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lcom/google/android/location/c/f$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/f$1;-><init>(Lcom/google/android/location/c/f;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->n:Ljava/lang/Runnable;

    if-eqz p7, :cond_0

    if-eqz p8, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "looper could not be null when listener is not null."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_1

    if-eqz p5, :cond_5

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Need non empty key or keyPath."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    if-eqz p5, :cond_2

    array-length v0, p5

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    const-string v0, "signingKey needs to be 32 byte long."

    invoke-static {v1, v0}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    invoke-static {p9}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    iput-object p1, p0, Lcom/google/android/location/c/f;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/location/c/f;->c:Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    :goto_2
    iput-object p4, p0, Lcom/google/android/location/c/f;->i:[B

    iput-object p5, p0, Lcom/google/android/location/c/f;->j:[B

    iput-boolean p6, p0, Lcom/google/android/location/c/f;->e:Z

    iput-object p7, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    iput-object p2, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/location/c/f;[Ljava/lang/String;)Lcom/google/android/location/c/f$a;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a([Ljava/lang/String;)Lcom/google/android/location/c/f$a;

    move-result-object v0

    return-object v0
.end method

.method private a([Ljava/lang/String;)Lcom/google/android/location/c/f$a;
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v5

    array-length v6, p1

    move v4, v3

    move-object v0, v2

    move-object v1, v2

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v7, p1, v4

    const-string v8, "sessionId"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-direct {p0, v7}, Lcom/google/android/location/c/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v1, v2

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const-string v8, "uploadedSeq"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-direct {p0, v7}, Lcom/google/android/location/c/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v8, "sessionSummary"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/android/location/c/B;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/location/c/f;->c(Ljava/lang/String;)Z

    :cond_4
    new-instance v2, Lcom/google/android/location/c/f$a;

    invoke-direct {v2, p0, v5, v1}, Lcom/google/android/location/c/f$a;-><init>(Lcom/google/android/location/c/f;Ljava/util/List;Ljava/lang/String;)V

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    move v0, v3

    :goto_2
    if-ge v0, v4, :cond_6

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/location/c/f$a;->a(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    return-object v2

    :catch_0
    move-exception v3

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/location/c/f;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/android/location/c/f$a;)V
    .locals 12

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/location/c/f$c;

    iget-object v1, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    invoke-direct {v0, p0, p1, v1, v3}, Lcom/google/android/location/c/f$c;-><init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;Lcom/google/android/location/c/f$1;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    new-instance v0, Lcom/google/android/location/c/B;

    iget-object v1, p0, Lcom/google/android/location/c/f;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/location/c/B$c;->b:Lcom/google/android/location/c/B$c;

    iget-object v4, p0, Lcom/google/android/location/c/f;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v8

    move-object v5, v3

    move-object v6, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/B;-><init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V

    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/location/c/f;->l:Z

    if-nez v3, :cond_1

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/B;Lcom/google/android/location/c/f$a;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->a()V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/location/c/B;->d()V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    iget-boolean v0, p0, Lcom/google/android/location/c/f;->e:Z

    if-nez v0, :cond_5

    move v0, v10

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/location/c/f;->e:Z

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->c(Lcom/google/android/location/c/f$a;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v10

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f$a;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-void

    :cond_5
    move v0, v11

    goto :goto_1

    :cond_6
    move v0, v11

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f$a;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/c/f$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/c/f$2;-><init>(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/location/c/B;Lcom/google/android/location/c/f$a;Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/location/c/f;->i:[B

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/c/f;->i:[B

    iget-object v5, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    invoke-static {v0, v5}, Lcom/google/android/location/c/a;->a([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    :goto_0
    iget-object v5, p0, Lcom/google/android/location/c/f;->j:[B

    if-eqz v5, :cond_0

    iget-object v1, p0, Lcom/google/android/location/c/f;->j:[B

    iget-object v5, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    invoke-static {v1, v5}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v1

    :cond_0
    new-instance v5, Lcom/google/android/location/c/b;

    invoke-direct {v5, v4, v0, v1}, Lcom/google/android/location/c/b;-><init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/c/a;)V

    sget-object v0, Lcom/google/android/location/c/f$b;->c:Lcom/google/android/location/c/f$b;

    :cond_1
    :goto_1
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->a()[B

    move-result-object v1

    if-eqz v1, :cond_2

    iget-boolean v4, p0, Lcom/google/android/location/c/f;->l:Z

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/google/android/location/c/B;->c()V

    sget-object v0, Lcom/google/android/location/c/f$b;->b:Lcom/google/android/location/c/f$b;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/location/c/b$a; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    invoke-virtual {p2, p3, v0}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;Lcom/google/android/location/c/f$b;)V

    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    :goto_3
    return v0

    :cond_3
    :try_start_2
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "isValid returned after parsing GLocRequest"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/location/c/b$a; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    const-string v1, "File not found."
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x6

    :try_start_5
    invoke-virtual {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/c/L;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    const/4 v6, 0x3

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    invoke-virtual {p2, p3, v6}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;I)V

    invoke-virtual {p2, v6}, Lcom/google/android/location/c/f$a;->b(I)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v1, 0x1

    invoke-static {p2, v6, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/location/c/b$a; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_6
    sget-object v0, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    const-string v1, "Error reading file."
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_2

    :cond_5
    :try_start_8
    invoke-virtual {p1, v4, v1}, Lcom/google/android/location/c/B;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Fatal: can not submit task."

    invoke-virtual {v0, v1, v6, v4}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/android/location/c/b$a; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :goto_4
    move v0, v2

    goto :goto_3

    :catch_4
    move-exception v0

    :try_start_a
    sget-object v0, Lcom/google/android/location/c/f$b;->e:Lcom/google/android/location/c/f$b;

    const-string v1, "Invalid file format."
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_c
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :goto_5
    throw v0

    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v1

    goto/16 :goto_2

    :catch_8
    move-exception v1

    goto :goto_5

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Ljava/io/File;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/c/f;->k:Z

    return p1
.end method

.method private a(Ljava/io/File;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private a([Ljava/io/File;)Z
    .locals 5

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    sget-object v4, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p1

    sget-object v2, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    :try_start_2
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_1
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v2

    :goto_2
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v1, :cond_4

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_4
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_0

    :catch_4
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v1, v0

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/c/f;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/c/f$a;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->e()Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    if-eqz v0, :cond_0

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    const-string v5, "uploadedSeq"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_3
    :goto_4
    throw v0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/location/c/f;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/f;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/c/f;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    return-object v0
.end method

.method private c()Z
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/location/c/f;->a([Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private c(Lcom/google/android/location/c/f$a;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;Ljava/lang/String;)Lcom/google/android/location/c/K;

    move-result-object v2

    const-string v3, "Summary should not be null after all complete."

    invoke-static {v2, v3}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, v2, Lcom/google/android/location/c/K;->a:I

    if-ne v3, v4, :cond_1

    iget v3, v2, Lcom/google/android/location/c/K;->b:I

    if-nez v3, :cond_1

    iget v3, v2, Lcom/google/android/location/c/K;->c:I

    if-nez v3, :cond_1

    iget v3, v2, Lcom/google/android/location/c/K;->f:I

    if-eqz v3, :cond_2

    :cond_1
    iget v2, v2, Lcom/google/android/location/c/K;->d:I

    if-ne v2, v4, :cond_0

    :cond_2
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/c/f;->c()Z

    move-result v0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    const-string v2, "sessionId"

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/location/c/f;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/c/f;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/c/f;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/f$c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/c/f;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/c/f;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/location/c/f;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/location/c/f;->n:Ljava/lang/Runnable;

    const-string v2, "BatchScanResultUploader.Thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/f;->l:Z

    return-void
.end method
