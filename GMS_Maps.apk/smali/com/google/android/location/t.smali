.class public Lcom/google/android/location/t;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/t$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/g;

.field private final d:Z


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/os/i;",
            "Lcom/google/android/location/g;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/t;-><init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;Z)V

    return-void
.end method

.method constructor <init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/os/i;",
            "Lcom/google/android/location/g;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/t;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    iput-object p3, p0, Lcom/google/android/location/t;->c:Lcom/google/android/location/g;

    iput-boolean p4, p0, Lcom/google/android/location/t;->d:Z

    return-void
.end method

.method private a(Ljava/util/Calendar;JZ)J
    .locals 4

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/t;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v1

    const/4 v0, 0x0

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/location/t;->a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v0

    move v1, v2

    move-object v3, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    :cond_0
    if-eqz p4, :cond_1

    iget-object v1, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->a:J

    :goto_1
    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    invoke-direct {p0, v0}, Lcom/google/android/location/t;->c(Ljava/util/Calendar;)J

    move-result-wide v0

    return-wide v0

    :cond_1
    iget-object v1, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->b:J

    goto :goto_1

    :cond_2
    move-object v3, v1

    move v1, v0

    goto :goto_0
.end method

.method private a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/k/b;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/t;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/location/k/b;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/google/android/location/v;
    .locals 9

    const/4 v1, 0x1

    const/4 v4, 0x7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/t;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/t;->c:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->p()Ljava/util/Map;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    if-eqz v2, :cond_3

    new-instance v0, Lcom/google/android/location/v;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    sget-object v3, Lcom/google/android/location/v$a;->b:Lcom/google/android/location/v$a;

    iget-boolean v4, p0, Lcom/google/android/location/t;->d:Z

    iget-object v1, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v7

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/v;-><init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/v$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, v4, :cond_0

    invoke-virtual {p2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iget-wide v4, p1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v4, v5}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    new-instance v4, Ljava/util/Random;

    iget-object v5, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    xor-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v4}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    const-wide v6, 0x3fe999999999999aL

    cmpg-double v0, v4, v6

    if-gtz v0, :cond_2

    move v0, v1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/location/v;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v$a;

    iget-boolean v4, p0, Lcom/google/android/location/t;->d:Z

    iget-object v1, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->B()J

    move-result-wide v7

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/v;-><init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/v$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V

    goto :goto_1
.end method

.method private b(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/t;->c:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_2

    move-wide v0, v2

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    move-wide v0, v2

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v4, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc

    const/4 v1, -0x5

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v4, p1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :cond_1
    return-object v4

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method private c(Ljava/util/Calendar;)J
    .locals 4

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "J)",
            "Lcom/google/android/location/e/u",
            "<",
            "Lcom/google/android/location/k/b;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/t;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    add-long/2addr v3, p2

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object v1, v0

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/location/t;->b(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/location/t;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->b(Ljava/util/Calendar;)Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v4, :cond_1

    invoke-virtual {v0, v3}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_1
    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/location/t;->a(Lcom/google/android/location/k/b;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/google/android/location/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/v;->a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/v;->a()Lcom/google/android/location/v$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/v$a;->a:Lcom/google/android/location/v$a;

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    :goto_1
    invoke-static {v6, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v2

    :cond_2
    return-object v2

    :cond_3
    sget-object v0, Lcom/google/android/location/b$a;->b:Lcom/google/android/location/b$a;

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/t;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    iget-wide v2, p1, Lcom/google/android/location/k/b;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not find parent of subtimespan: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/util/Calendar;J)Lcom/google/android/location/t$a;
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/t;->d(Ljava/util/Calendar;J)Lcom/google/android/location/e/u;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, v4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, v4, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/b$a;

    move-object v1, v0

    :goto_1
    new-instance v5, Lcom/google/android/location/t$a;

    if-nez v4, :cond_2

    :goto_2
    invoke-direct {v5, v3, v1, v2}, Lcom/google/android/location/t$a;-><init>(ZLcom/google/android/location/b$a;Lcom/google/android/location/k/b;)V

    return-object v5

    :cond_0
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    :cond_1
    move-object v1, v2

    goto :goto_1

    :cond_2
    iget-object v0, v4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/k/b;

    move-object v2, v0

    goto :goto_2
.end method

.method b(Ljava/util/Calendar;J)J
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/t;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    return-wide v0
.end method

.method c(Ljava/util/Calendar;J)J
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/location/t;->a(Ljava/util/Calendar;JZ)J

    move-result-wide v0

    return-wide v0
.end method
