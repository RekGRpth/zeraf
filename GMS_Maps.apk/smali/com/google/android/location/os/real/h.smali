.class public Lcom/google/android/location/os/real/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/k/a;
.implements Lcom/google/android/location/os/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/k/a",
        "<",
        "Lcom/google/android/location/os/h;",
        ">;",
        "Lcom/google/android/location/os/i;"
    }
.end annotation


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/os/e;

.field private final d:Lcom/google/android/location/os/real/c;

.field private final e:Lcom/google/android/location/os/real/h$a;

.field private final f:Z

.field private final g:Landroid/app/AlarmManager;

.field private final h:[Landroid/app/PendingIntent;

.field private final i:Lcom/google/android/location/os/real/d;

.field private final j:Lcom/google/android/location/i/a;

.field private final k:Lcom/google/android/location/os/h;

.field private final m:[Landroid/os/PowerManager$WakeLock;

.field private n:[Z

.field private o:[Landroid/net/wifi/WifiManager$WifiLock;

.field private final p:Landroid/net/wifi/WifiManager;

.field private final q:Landroid/hardware/SensorManager;

.field private final r:Landroid/location/LocationManager;

.field private final s:Lcom/google/android/location/d/a;

.field private final t:Ljava/util/concurrent/ExecutorService;

.field private final u:[B

.field private final v:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NetworkLocationLocator"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NetworkLocationActiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "NetworkLocationBurstCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NetworkLocationPassiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "NetworkLocationCacheUpdater"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "NetworkLocationCalibrationCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "NetworkLocationSCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NetworkLocationSensorUploader"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "NetworkLocationActivityDetection"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "NetworkLocationInOutCollector"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "NetworkLocationBurstCollectionTrigger"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/e;Lcom/google/android/location/os/real/h$a;Z)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0xb

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v6, [Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-array v0, v6, [Landroid/os/PowerManager$WakeLock;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    new-array v0, v6, [Z

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    new-array v0, v6, [Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    iput-object p1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    iput-object p3, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    iput-boolean p4, p0, Lcom/google/android/location/os/real/h;->f:Z

    new-instance v0, Lcom/google/android/location/d/a;

    invoke-direct {v0, p1, v7}, Lcom/google/android/location/d/a;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    new-instance v0, Lcom/google/android/location/os/h;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/location/os/h;-><init>(Lcom/google/android/location/os/c;Lcom/google/android/location/os/f;Lcom/google/android/location/k/a;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->a()V

    new-instance v0, Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/location/os/real/c;-><init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/os/e;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v8

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x3

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x4

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x5

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x6

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x7

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0x8

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0x9

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0xa

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    new-instance v0, Lcom/google/android/location/os/real/h$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/os/real/h$1;-><init>(Lcom/google/android/location/os/real/h;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/location/i/a;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/i/a;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    new-instance v0, Lcom/google/android/location/os/real/d;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-direct {v0, p1, v1, v3, v4}, Lcom/google/android/location/os/real/d;-><init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/os/real/c;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_0

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    sget-object v4, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v7, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    sget-object v5, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v4, v8, v5}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->r:Landroid/location/LocationManager;

    invoke-direct {p0}, Lcom/google/android/location/os/real/h;->J()[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/os/real/h;->I()[B

    move-result-object v0

    :cond_2
    iput-object v0, p0, Lcom/google/android/location/os/real/h;->u:[B

    sget-boolean v0, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    :goto_1
    return-void

    :cond_3
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    goto :goto_1
.end method

.method public static F()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static G()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static H()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private I()[B
    .locals 6

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    const/16 v1, 0x20

    new-array v2, v1, [B

    invoke-virtual {v0, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/location/os/real/h;->g(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    :catch_2
    move-exception v1

    move-object v1, v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private J()[B
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/location/os/real/h;->g(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    const/16 v1, 0x20

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v3

    if-ltz v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    :goto_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catch_1
    move-exception v0

    :goto_3
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_4

    :catch_2
    move-exception v1

    move-object v1, v0

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lcom/google/android/location/os/real/h;->f(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lcom/google/android/location/os/real/h;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    invoke-static {}, Lcom/google/android/location/os/real/d;->a()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    const-string v0, "cache.cell"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "cache.wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "gls.platform.key"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "nlp_GlsPlatformKey"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "nlp_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/location/os/real/h;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/j;->a(Ljava/io/File;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "nlp_state"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_s"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ioh"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/location/os/real/h;->h(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static g(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ck"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static h(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    return-wide v0
.end method

.method public C()Lcom/google/android/location/i/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    return-object v0
.end method

.method public D()Lcom/google/android/location/os/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    return-object v0
.end method

.method public E()V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/c;->a()V

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/h;->a(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    :goto_1
    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/h;->c(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a()J
    .locals 2

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/location/c/g;Ljava/lang/String;)Lcom/google/android/location/c/q;
    .locals 11

    const/4 v10, 0x0

    invoke-static {}, Lcom/google/android/location/os/real/d;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    move-object v0, v10

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/location/c/f;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->j()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->l()[B

    move-result-object v5

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v8

    new-instance v9, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v2

    invoke-direct {v9, p3, v2}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v2, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BZLcom/google/android/location/c/g;Landroid/os/Looper;Lcom/google/android/location/k/a/c;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v10

    goto :goto_0
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation

    new-instance v7, Lcom/google/android/location/os/real/b;

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-direct {v7, v0, p5, v1}, Lcom/google/android/location/os/real/b;-><init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/location/c/A;

    sget-object v4, Lcom/google/android/location/c/j$a;->a:Lcom/google/android/location/c/j$a;

    const/4 v5, 0x1

    move-object v1, p1

    move-wide v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Z)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/c/F;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/location/c/j;->a(Lcom/google/android/location/c/F;I)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/android/location/c/I;

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v8, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v3

    invoke-direct {v8, p6, v3}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v3, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/c/I;-><init>(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    return-object v1
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;ZLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Z",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Z",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation

    new-instance v10, Lcom/google/android/location/os/real/b;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    move-object/from16 v0, p10

    invoke-direct {v10, v1, v0, v2}, Lcom/google/android/location/os/real/b;-><init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/location/c/A;

    sget-object v5, Lcom/google/android/location/c/j$a;->b:Lcom/google/android/location/c/j$a;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->l()[B

    move-result-object v8

    move-object v2, p1

    move-wide v3, p3

    move-object/from16 v6, p5

    move/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Ljava/lang/String;Ljava/lang/String;[BZ)V

    move/from16 v0, p7

    invoke-interface {v1, v0}, Lcom/google/android/location/c/j;->a(Z)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/c/F;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v3, v2}, Lcom/google/android/location/c/j;->a(Lcom/google/android/location/c/F;I)V

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/google/android/location/c/I;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    new-instance v9, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v4

    move-object/from16 v0, p11

    invoke-direct {v9, v0, v4}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v4, v1

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object v8, v10

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/c/I;-><init>(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    return-object v2
.end method

.method public a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->u:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(II)V

    return-void
.end method

.method public a(IJ)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/os/e;->a(IJ)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    aget-object v2, v2, p1

    invoke-virtual {v0, v1, p2, p3, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public a(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(IZ)V

    return-void
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/a/n$b;)V

    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/real/h$a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/e/t;)V

    return-void
.end method

.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/t;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v0, v0, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v0, v0, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/real/h$a;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/a;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-boolean v1, p0, Lcom/google/android/location/os/real/h;->f:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/c;->b(Z)V

    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/h;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->x:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->B:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/j;->a(Ljava/io/File;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/location/os/h;

    invoke-virtual {p0, p1}, Lcom/google/android/location/os/real/h;->a(Lcom/google/android/location/os/h;)V

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p2}, Lcom/google/android/location/os/e;->e(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/d/a;Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->c()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/google/android/location/d/a;->a(Ljava/text/Format;JLjava/io/PrintWriter;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Z)V

    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public b()J
    .locals 2

    invoke-static {}, Lcom/google/android/location/os/real/h;->G()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->c(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already acquired"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->y:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public c()J
    .locals 2

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->d(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already released"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->z:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public d()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->A:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->w:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public e()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->C:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->D:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->h(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->E:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->F:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->v:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c;->b()V

    return-void
.end method

.method public j()Ljavax/crypto/SecretKey;
    .locals 12

    const/16 v11, 0x18

    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, 0x0

    const-wide/16 v6, 0xff

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->G:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->B()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "no android ID; can\'t access encrypted cache"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v2, 0x20

    new-array v2, v2, [B

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v8

    const/4 v3, 0x1

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x5

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x6

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x7

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v9

    const/16 v3, 0x9

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xa

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xb

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xc

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xd

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xe

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xf

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v10

    const/16 v3, 0x11

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x12

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x13

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x14

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x15

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x16

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x17

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v11

    const/16 v3, 0x19

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1a

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1b

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1c

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1d

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1e

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1f

    ushr-long/2addr v0, v8

    and-long/2addr v0, v6

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, v2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public k()Ljavax/crypto/SecretKey;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->j()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()[B
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->u:[B

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->u:[B

    array-length v1, v1

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    :try_start_0
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->N:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    return-void
.end method

.method public o()Ljava/io/InputStream;
    .locals 3

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public p()Lcom/google/android/location/os/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0}, Lcom/google/android/location/os/real/h$a;->d()Lcom/google/android/location/os/g;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/google/android/location/os/i$a;
    .locals 5

    new-instance v0, Lcom/google/android/location/os/i$a;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/os/i$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public r()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->r:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/k;->a(Landroid/hardware/SensorManager;Landroid/location/LocationManager;)Z

    move-result v0

    return v0
.end method

.method public v()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public w()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public x()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reconnect()Z

    move-result v0

    return v0
.end method

.method public y()I
    .locals 1

    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    return v0
.end method

.method public z()I
    .locals 4

    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v0, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
