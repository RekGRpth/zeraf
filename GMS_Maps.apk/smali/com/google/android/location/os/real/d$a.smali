.class Lcom/google/android/location/os/real/d$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/d;

.field private final b:Lcom/google/android/location/os/real/d$b;

.field private final c:Ljava/lang/String;

.field private d:Lcom/google/android/location/os/real/e;

.field private volatile e:J


# direct methods
.method constructor <init>(Lcom/google/android/location/os/real/d;Lcom/google/android/location/os/real/d$b;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    iput-object p2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->c:Ljava/lang/String;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "GlsClient"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "GlsClient-query"

    goto :goto_0

    :pswitch_1
    const-string v0, "GlsClient-upload"

    goto :goto_0

    :pswitch_2
    const-string v0, "GlsClient-model-query"

    goto :goto_0

    :pswitch_3
    const-string v0, "GlsClient-device-location-query"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "g:loc/ql"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "g:loc/ul"

    goto :goto_0

    :pswitch_2
    const-string v0, "g:loc/dl"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->a:Lcom/google/android/location/os/real/d$b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/c;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eq p1, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    :try_start_1
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/google/android/location/j/a;->ai:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    sget-object v1, Lcom/google/android/location/os/real/d$1;->a:[I

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/d$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unhandled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_1
    :try_start_3
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/a;->a(J)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/real/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Response to unexpected request."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_1

    instance-of v0, p2, Lcom/google/android/location/h/e;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/i/a;->a(J)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Gls request still outstanding."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Ljava/util/Locale;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/os/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->b(Lcom/google/android/location/os/real/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x63

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->U:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v1, v0}, Lcom/google/android/location/os/real/d;->a(Lcom/google/android/location/os/real/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_3
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    new-instance v1, Lcom/google/android/location/os/real/e;

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/location/os/real/e;-><init>(Ljava/lang/String;I[B)V

    iput-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    invoke-virtual {v0, p0}, Lcom/google/android/location/os/real/e;->a(Lcom/google/android/location/h/b/m$a;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/e;->b(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    monitor-enter v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/e;->c()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/location/os/real/d$a;->e:J

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_0
    :try_start_6
    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->b:Lcom/google/android/location/os/real/d$b;

    sget-object v1, Lcom/google/android/location/os/real/d$b;->b:Lcom/google/android/location/os/real/d$b;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/location/os/real/d$a;->a:Lcom/google/android/location/os/real/d;

    invoke-static {v0}, Lcom/google/android/location/os/real/d;->c(Lcom/google/android/location/os/real/d;)Lcom/google/android/location/i/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/os/real/d$a;->e:J

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/i/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_7
    invoke-direct {p0}, Lcom/google/android/location/os/real/d$a;->d()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move-exception v0

    const-wide/16 v0, 0x0

    :try_start_a
    iput-wide v0, p0, Lcom/google/android/location/os/real/d$a;->e:J

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/google/android/location/d/c;->a()Lcom/google/android/location/d/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/d$a;->d:Lcom/google/android/location/os/real/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/d/c;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_1
.end method
