.class public Lcom/google/android/location/os/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/os/f;

.field private final b:Ljavax/crypto/SecretKey;

.field private final c:Lcom/google/android/location/c/a;

.field private final d:Ljava/io/File;

.field private final e:I

.field private final f:I

.field private volatile g:I

.field private final h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final i:Lcom/google/common/base/K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/K",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljavax/crypto/SecretKey;",
            "I[B",
            "Lcom/google/googlenav/common/io/protocol/ProtoBufType;",
            "Ljava/io/File;",
            "Lcom/google/common/base/K",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;",
            "Lcom/google/android/location/os/f;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/os/j;->g:I

    iput-object p5, p0, Lcom/google/android/location/os/j;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iput-object p8, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    iput-object p6, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    iput p1, p0, Lcom/google/android/location/os/j;->e:I

    iput p3, p0, Lcom/google/android/location/os/j;->f:I

    iput-object p2, p0, Lcom/google/android/location/os/j;->b:Ljavax/crypto/SecretKey;

    if-eqz p4, :cond_0

    invoke-static {p4, v1}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    :goto_0
    iput-object p7, p0, Lcom/google/android/location/os/j;->i:Lcom/google/common/base/K;

    return-void

    :cond_0
    iput-object v1, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    goto :goto_0
.end method

.method private a(ILjava/io/DataInputStream;)Ljava/io/InputStream;
    .locals 2

    iget v0, p0, Lcom/google/android/location/os/j;->e:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljavax/crypto/SecretKey;

    invoke-static {p2, v0}, Lcom/google/android/location/os/b;->b(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No cipher key specified."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v0, p0, Lcom/google/android/location/os/j;->f:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :try_start_3
    iget-object v2, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Runtime while writing protobuf to bytes."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    :try_start_5
    new-instance v0, Ljava/io/IOException;

    const-string v2, "No cipher specified."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 2

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/j;->a(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "File not found."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/j;->g:I

    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    iget v3, p0, Lcom/google/android/location/os/j;->e:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    iget v3, p0, Lcom/google/android/location/os/j;->f:I

    if-eq v0, v3, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid version, desired = %d or %d, actual = %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/location/os/j;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/location/os/j;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/location/os/j;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    :try_start_2
    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/j;->a(ILjava/io/DataInputStream;)Ljava/io/InputStream;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/location/os/j;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, v0}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/os/j;->i:Lcom/google/common/base/K;

    invoke-interface {v3, v0}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid file format."

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public a([B)V
    .locals 3

    const-string v0, "output buffer can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    iget-object v2, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    :goto_0
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[B
    .locals 1

    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V

    return-void
.end method
