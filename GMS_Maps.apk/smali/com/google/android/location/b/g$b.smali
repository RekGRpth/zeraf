.class public Lcom/google/android/location/b/g$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:J


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/b/g$b;->b:J

    iput-object p1, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private a(JJJJ)J
    .locals 2

    add-long v0, p3, p1

    sub-long/2addr v0, p5

    invoke-static {p7, p8, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private a(JJJ)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long v3, v0, v2

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/b/g$b;->a(JJJJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long v3, v0, v2

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/b/g$b;->a(JJJJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->c(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;JJJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/b/g$b;->a(JJJ)V

    return-void
.end method

.method private b(J)V
    .locals 4

    const/4 v3, 0x6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/android/location/b/g$b;->d()F

    move-result v0

    cmpl-float v1, v0, v2

    if-lez v1, :cond_0

    const v1, 0x3dcccccd

    sub-float/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/b/g$b;->a(F)V

    :cond_0
    iput-wide p1, p0, Lcom/google/android/location/b/g$b;->b:J

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/g$b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/b/g$b;->i()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/g$b;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->b(J)V

    return-void
.end method

.method private c(J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private d(J)I
    .locals 8

    const-wide/32 v6, 0x5265c00

    const-wide/32 v2, 0x927c0

    long-to-double v0, p1

    const-wide/high16 v4, 0x3ff0000000000000L

    mul-double/2addr v0, v4

    const-wide v4, 0x4194997000000000L

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    mul-long v4, v0, v6

    sub-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    sub-long v2, v6, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    :cond_0
    long-to-int v0, v0

    return v0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/b/g$b;->b:J

    return-wide v0
.end method

.method a(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method a(J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method a(JI)V
    .locals 2

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/b/g$b;->a(J)V

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public b()I
    .locals 2

    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method b(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public c()I
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public d()F
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    return-void
.end method

.method public f()F
    .locals 2

    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000

    goto :goto_0
.end method

.method g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    return-void
.end method

.method public h()I
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method
