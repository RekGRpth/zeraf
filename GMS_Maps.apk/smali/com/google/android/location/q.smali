.class public Lcom/google/android/location/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/k/a;
.implements Lcom/google/android/location/os/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/k/a",
        "<",
        "Lcom/google/android/location/u;",
        ">;",
        "Lcom/google/android/location/os/a;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/os/i;

.field final b:Lcom/google/android/location/b/f;

.field final c:Lcom/google/android/location/b/g;

.field final d:Lcom/google/android/location/b/b;

.field final e:Lcom/google/android/location/i/a;

.field final f:Lcom/google/android/location/p;

.field final g:Lcom/google/android/location/a;

.field final h:Lcom/google/android/location/y;

.field final i:Lcom/google/android/location/b/e;

.field final j:Lcom/google/android/location/u;

.field final k:Lcom/google/android/location/g;

.field final l:Lcom/google/android/location/x;

.field final m:Lcom/google/android/location/a/b;

.field final n:Lcom/google/android/location/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/b/e;Z)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    new-instance v2, Lcom/google/android/location/b/f;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/b/f;-><init>(Lcom/google/android/location/os/h;J)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    new-instance v2, Lcom/google/android/location/b/g;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/location/b/g;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    new-instance v2, Lcom/google/android/location/b/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v3, v4, v1}, Lcom/google/android/location/b/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/os/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/u;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    new-instance v12, Lcom/google/android/location/g/l;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-direct {v12, v0, v1}, Lcom/google/android/location/g/l;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/e;)V

    new-instance v7, Lcom/google/android/location/e/n;

    invoke-direct {v7}, Lcom/google/android/location/e/n;-><init>()V

    new-instance v2, Lcom/google/android/location/g;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/g;-><init>(Lcom/google/android/location/os/f;Ljavax/crypto/SecretKey;[B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    new-instance v2, Lcom/google/android/location/y;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/y;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/i/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/y;

    new-instance v2, Lcom/google/android/location/e/d;

    invoke-direct {v2}, Lcom/google/android/location/e/d;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    new-instance v8, Lcom/google/android/location/p;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/y;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    move-object/from16 v16, v0

    move-object/from16 v9, p1

    move-object v14, v7

    invoke-direct/range {v8 .. v16}, Lcom/google/android/location/p;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g/l;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/y;Lcom/google/android/location/e/d;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    new-instance v2, Lcom/google/android/location/a/b;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/android/location/a/b;-><init>(Lcom/google/android/location/os/i;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    if-eqz p5, :cond_0

    new-instance v2, Lcom/google/android/location/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/y;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-direct/range {v2 .. v12}, Lcom/google/android/location/o;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/y;Lcom/google/android/location/u;Lcom/google/android/location/i/a;Lcom/google/android/location/e/d;Lcom/google/android/location/a/b;Lcom/google/android/location/g/l;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    :goto_0
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    new-instance v2, Lcom/google/android/location/x;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/x;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/g;Lcom/google/android/location/u;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->l:Lcom/google/android/location/x;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/location/u;->a(Lcom/google/android/location/k/a;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/os/a;)V

    return-void

    :cond_0
    new-instance v2, Lcom/google/android/location/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/y;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/location/r;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/y;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/f;->a(Lcom/google/android/location/os/i;)V

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/u;->c(Z)V

    iget-object v0, p0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    invoke-virtual {v0}, Lcom/google/android/location/b/g;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/b/b;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/q;->h:Lcom/google/android/location/y;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/y;->a(J)V

    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->a()V

    return-void
.end method

.method public a(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0}, Lcom/google/android/location/p;->b()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/b;->a(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/x;

    invoke-virtual {v0, p1}, Lcom/google/android/location/x;->a(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b;->a(II)V

    return-void
.end method

.method public a(IIZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/u;->a(IIZ)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/a;->a(IIZ)V

    return-void
.end method

.method public a(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/p;->a(IZ)V

    return-void
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/n$b;)V

    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/E;)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->b(Lcom/google/android/location/e/E;)V

    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, p1}, Lcom/google/android/location/u;->a(Lcom/google/android/location/e/e;)V

    iget-object v0, p0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/e/d;->a(Lcom/google/android/location/e/e;J)V

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/e;)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/e;)V

    return-void
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v0, v0, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    iget-object v1, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    invoke-virtual {v0, v1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/e/E;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/location/os/g;->d()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/os/g;)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/os/h;)V

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, p1}, Lcom/google/android/location/u;->a(Lcom/google/android/location/os/h;)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/h;)V

    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/google/android/location/u;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0}, Lcom/google/android/location/a;->c()V

    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/x;

    invoke-virtual {v0}, Lcom/google/android/location/x;->a()V

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/location/u;

    invoke-virtual {p0, p1}, Lcom/google/android/location/q;->a(Lcom/google/android/location/u;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/u;->c(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    iget-object v1, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    iget-object v1, v1, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/f;->b(Lcom/google/android/location/os/i;)V

    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->b()V

    :cond_0
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/u;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public a(ZZI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(ZZI)V

    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/x;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/x;->a(ZZI)V

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/p;->a(ZZI)V

    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/x;

    invoke-virtual {v0, p1}, Lcom/google/android/location/x;->a(Z)V

    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public c(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/e/d;->a(ZJ)V

    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/u;

    invoke-virtual {v0, p1}, Lcom/google/android/location/u;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(Z)V

    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Z)V

    return-void
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a;->c(Z)V

    return-void
.end method
