.class public Lcom/google/android/location/w;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/w$1;,
        Lcom/google/android/location/w$a;
    }
.end annotation


# static fields
.field static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Ljava/util/Random;

.field private B:Z

.field private C:Ljava/lang/Integer;

.field private D:Lcom/google/android/location/k/b;

.field private E:Z

.field private F:Z

.field j:J

.field k:Lcom/google/android/location/b$a;

.field l:Z

.field m:J

.field n:Lcom/google/android/location/c/r;

.field o:Z

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:Z

.field r:Lcom/google/android/location/c/J;

.field s:J

.field t:J

.field private final u:Lcom/google/android/location/g;

.field private final v:Lcom/google/android/location/u;

.field private final w:Lcom/google/android/location/f;

.field private final x:Lcom/google/android/location/i/a;

.field private final y:Lcom/google/android/location/a/b;

.field private final z:Lcom/google/android/location/t;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x10

    const/16 v5, 0xb

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v4, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/w;->g:Ljava/util/Set;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/w;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/location/w;->i:Ljava/util/List;

    sget-object v0, Lcom/google/android/location/w;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    const/4 v2, 0x6

    invoke-direct {v1, v2, v3, v5, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/w;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    invoke-direct {v1, v5, v4, v6, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/w;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/location/k/b;

    const/16 v2, 0x17

    invoke-direct {v1, v6, v4, v2, v3}, Lcom/google/android/location/k/b;-><init>(IIII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;)V
    .locals 13

    invoke-static {}, Lcom/google/android/location/u;->a()Lcom/google/android/location/u;

    move-result-object v8

    new-instance v12, Lcom/google/android/location/t;

    sget-object v1, Lcom/google/android/location/w;->i:Ljava/util/List;

    move-object/from16 v0, p5

    invoke-direct {v12, v1, p1, v0}, Lcom/google/android/location/t;-><init>(Ljava/util/List;Lcom/google/android/location/os/i;Lcom/google/android/location/g;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v12}, Lcom/google/android/location/w;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/u;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;Lcom/google/android/location/t;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/f;Lcom/google/android/location/u;Lcom/google/android/location/i/a;Lcom/google/android/location/a/b;Ljava/util/Random;Lcom/google/android/location/t;)V
    .locals 8

    const-string v2, "SCollector"

    sget-object v7, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/y;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/w;->j:J

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/b$a;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->B:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->l:Z

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/w;->m:J

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->o:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->q:Z

    new-instance v1, Lcom/google/android/location/w$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/w$a;-><init>(Lcom/google/android/location/w;Lcom/google/android/location/w$1;)V

    iput-object v1, p0, Lcom/google/android/location/w;->r:Lcom/google/android/location/c/J;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/w;->s:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/w;->t:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->E:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/w;->F:Z

    iput-object p5, p0, Lcom/google/android/location/w;->u:Lcom/google/android/location/g;

    iput-object p7, p0, Lcom/google/android/location/w;->v:Lcom/google/android/location/u;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/location/w;->x:Lcom/google/android/location/i/a;

    iput-object p6, p0, Lcom/google/android/location/w;->w:Lcom/google/android/location/f;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/location/w;->y:Lcom/google/android/location/a/b;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/location/w;->A:Ljava/util/Random;

    return-void
.end method

.method private a(JJJIZ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v3

    add-long v1, p1, v3

    add-long/2addr v3, p5

    iget-object v0, p0, Lcom/google/android/location/w;->u:Lcom/google/android/location/g;

    iget-object v6, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/b$a;

    move v5, p7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/g;->a(JJILcom/google/android/location/b$a;)V

    iget-object v0, p0, Lcom/google/android/location/w;->u:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->b()V

    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-eqz v0, :cond_0

    sub-long v0, p5, p3

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/w;->x:Lcom/google/android/location/i/a;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/i/a;->b(J)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/w;->q:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/w;->m:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/w;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/w;->C:Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/w;->D:Lcom/google/android/location/k/b;

    if-eqz p8, :cond_1

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_1
    return-void
.end method

.method private a(JLcom/google/android/location/t$a;)V
    .locals 10

    iget-object v0, p3, Lcom/google/android/location/t$a;->c:Lcom/google/android/location/k/b;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v9, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/w;->D:Lcom/google/android/location/k/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w;->D:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v9}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w;->C:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-wide/16 v3, -0x1

    iget-object v0, p0, Lcom/google/android/location/w;->C:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/w;->a(JJJIZ)V

    :cond_0
    iput-object v9, p0, Lcom/google/android/location/w;->D:Lcom/google/android/location/k/b;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-object v1, p3, Lcom/google/android/location/t$a;->c:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;

    move-result-object v0

    move-object v9, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/location/w;JJJIZ)V
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/google/android/location/w;->a(JJJIZ)V

    return-void
.end method

.method private a(Ljava/util/Calendar;J)V
    .locals 4

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/w;->j(J)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/w;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-wide v2, p0, Lcom/google/android/location/w;->t:J

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/location/t;->a(Ljava/util/Calendar;J)Lcom/google/android/location/t$a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/location/t$a;->a:Z

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/w;->e(Z)V

    iget-object v0, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-wide v1, p0, Lcom/google/android/location/w;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/t;->c(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/w;->k(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/w;->e(Z)V

    invoke-virtual {p0}, Lcom/google/android/location/w;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-wide v1, p0, Lcom/google/android/location/w;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/t;->c(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/w;->k(J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-wide v1, p0, Lcom/google/android/location/w;->t:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/t;->b(Ljava/util/Calendar;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/w;->k(J)V

    goto :goto_0
.end method

.method private a(JZ)Z
    .locals 12

    const/4 v9, 0x0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    iget-object v1, p0, Lcom/google/android/location/w;->u:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    iget-object v1, p0, Lcom/google/android/location/w;->u:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    sget-object v1, Lcom/google/android/location/w;->h:Ljava/util/Set;

    const-wide/32 v3, 0x493e0

    iget-object v5, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v5}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    if-eqz p3, :cond_2

    const/16 v6, 0x12

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/w;->w:Lcom/google/android/location/f;

    invoke-virtual {v7}, Lcom/google/android/location/f;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/location/w;->r:Lcom/google/android/location/c/J;

    iget-object v11, p0, Lcom/google/android/location/w;->a:Ljava/lang/String;

    move v7, p3

    invoke-interface/range {v0 .. v11}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;ZLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/w;->m:J

    iput-boolean v9, p0, Lcom/google/android/location/w;->q:Z

    iput-boolean p3, p0, Lcom/google/android/location/w;->o:Z

    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    iget-boolean v0, p0, Lcom/google/android/location/w;->E:Z

    invoke-direct {p0, v0}, Lcom/google/android/location/w;->f(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_1

    const/4 v9, 0x1

    :cond_1
    return v9

    :cond_2
    const/16 v6, 0xe

    goto :goto_0
.end method

.method private a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E$a;)Z
    .locals 2

    iget-object v0, p2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/android/location/b/g;->b(Ljava/lang/Long;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/w;->A:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 4

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/google/android/location/w;->j:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iput-wide v2, p0, Lcom/google/android/location/w;->j:J

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 2

    const/16 v1, 0xb4

    iget-boolean v0, p0, Lcom/google/android/location/w;->l:Z

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/w;->y:Lcom/google/android/location/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->b(I)V

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/location/w;->l:Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/w;->y:Lcom/google/android/location/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/b;->c(I)V

    goto :goto_0
.end method

.method private f()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private f(Z)V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/w;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->K:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->S:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v6, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/w;->m:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/android/location/w;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_1
    return-void
.end method

.method private g()Ljava/util/Calendar;
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    return-object v0
.end method

.method private j(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/w;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/w;->t:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/w;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/w;->j:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/google/android/location/w;->j:J

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    :cond_1
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 3

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/w;->j:J

    invoke-direct {p0}, Lcom/google/android/location/w;->g()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/w;->a(Ljava/util/Calendar;J)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/w;->s:J

    :cond_0
    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/w;->F:Z

    return-void
.end method

.method a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/w;->B:Z

    return-void
.end method

.method b(Lcom/google/android/location/e/E;)V
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/google/android/location/a;->b(Lcom/google/android/location/e/E;)V

    iget-object v1, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    sget-object v2, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v1, v2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/w;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v1}, Lcom/google/android/location/a$b;->a()Lcom/google/android/location/b/g;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/w;->a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E$a;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    int-to-long v0, v1

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p1, Lcom/google/android/location/e/E;->a:J

    iput-wide v0, p0, Lcom/google/android/location/w;->t:J

    goto :goto_0
.end method

.method b(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/w;->f(Z)V

    iput-boolean p1, p0, Lcom/google/android/location/w;->E:Z

    return-void
.end method

.method protected b(J)Z
    .locals 12

    const/4 v9, 0x1

    const-wide/16 v3, -0x1

    const/4 v8, 0x0

    iget-boolean v0, p0, Lcom/google/android/location/w;->F:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/w;->d()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iput-boolean v8, p0, Lcom/google/android/location/w;->F:Z

    :cond_1
    :goto_0
    return v8

    :cond_2
    invoke-direct {p0}, Lcom/google/android/location/w;->g()Ljava/util/Calendar;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/w;->j(J)Z

    move-result v1

    iget-wide v5, p0, Lcom/google/android/location/w;->j:J

    cmp-long v2, v5, v3

    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    iget-boolean v2, p0, Lcom/google/android/location/w;->l:Z

    if-nez v2, :cond_4

    :cond_3
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/location/w;->a(Ljava/util/Calendar;J)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/location/w;->z:Lcom/google/android/location/t;

    iget-wide v5, p0, Lcom/google/android/location/w;->t:J

    invoke-virtual {v2, v0, v5, v6}, Lcom/google/android/location/t;->a(Ljava/util/Calendar;J)Lcom/google/android/location/t$a;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/w;->a(JLcom/google/android/location/t$a;)V

    iget-boolean v2, v0, Lcom/google/android/location/t$a;->a:Z

    if-eqz v2, :cond_5

    iget-object v0, v0, Lcom/google/android/location/t$a;->b:Lcom/google/android/location/b$a;

    iput-object v0, p0, Lcom/google/android/location/w;->k:Lcom/google/android/location/b$a;

    invoke-virtual {p0, v1}, Lcom/google/android/location/w;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-wide v5, p0, Lcom/google/android/location/w;->s:J

    cmp-long v0, v5, v3

    if-eqz v0, :cond_6

    iget-wide v5, p0, Lcom/google/android/location/w;->s:J

    sub-long v5, p1, v5

    const-wide/16 v10, 0x7530

    cmp-long v0, v5, v10

    if-gez v0, :cond_6

    move v0, v9

    :goto_1
    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/location/w;->e()V

    sget-object v0, Lcom/google/android/location/a$c;->h:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    invoke-direct {p0, v8}, Lcom/google/android/location/w;->e(Z)V

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/location/w;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    const/16 v7, 0x1d

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/w;->a(JJJIZ)V

    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_1

    move v8, v9

    goto :goto_0

    :cond_6
    move v0, v8

    goto :goto_1

    :cond_7
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/w;->C:Ljava/lang/Integer;

    goto :goto_2

    :cond_8
    iget-object v0, v2, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/location/w;->C:Ljava/lang/Integer;

    goto :goto_2
.end method

.method public c()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    invoke-direct {p0}, Lcom/google/android/location/w;->g()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/w;->a(Ljava/util/Calendar;J)V

    return-void
.end method

.method d(Z)Lcom/google/android/location/e/u;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/location/w;->v:Lcom/google/android/location/u;

    invoke-virtual {v0, p1}, Lcom/google/android/location/u;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/location/w;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->m()Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/location/w;->w:Lcom/google/android/location/f;

    invoke-virtual {v0}, Lcom/google/android/location/f;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/w;->x:Lcom/google/android/location/i/a;

    const-wide/32 v7, 0x493e0

    invoke-virtual {v0, v7, v8, v2}, Lcom/google/android/location/i/a;->d(JZ)Z

    move-result v7

    iget-object v0, v5, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/location/w;->B:Z

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    if-eqz v1, :cond_1

    if-eqz v7, :cond_1

    move v4, v2

    :goto_1
    if-eqz v4, :cond_a

    invoke-direct {p0}, Lcom/google/android/location/w;->f()I

    move-result v0

    const/4 v8, 0x3

    if-lt v0, v8, :cond_2

    move v0, v2

    :goto_2
    if-eqz v4, :cond_3

    if-nez v0, :cond_3

    :goto_3
    move v3, v0

    :goto_4
    if-nez v2, :cond_9

    iget-object v0, v5, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v5, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    :goto_5
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v4, v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v2, v3

    goto :goto_3

    :cond_4
    if-nez v6, :cond_5

    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_5
    if-nez v1, :cond_6

    const/16 v0, 0x1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_6
    if-nez v7, :cond_7

    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_7
    if-eqz v3, :cond_8

    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_8
    const/16 v0, 0x63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    move v2, v4

    goto :goto_4
.end method

.method d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/w;->v:Lcom/google/android/location/u;

    invoke-virtual {v0}, Lcom/google/android/location/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    return v0
.end method

.method protected h(J)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/w;->f:Lcom/google/android/location/a$c;

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/w;->v:Lcom/google/android/location/u;

    iget-boolean v2, p0, Lcom/google/android/location/w;->o:Z

    invoke-virtual {v0, v2}, Lcom/google/android/location/u;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/location/w;->q:Z

    iget-object v0, p0, Lcom/google/android/location/w;->n:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
