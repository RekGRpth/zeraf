.class public Lcom/google/android/location/h/h;
.super Lcom/google/googlenav/common/io/BaseHttpConnectionFactory;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/h/h$a;,
        Lcom/google/android/location/h/h$b;
    }
.end annotation


# static fields
.field protected static b:Lcom/google/android/location/h/h;


# instance fields
.field protected c:Las/c;

.field protected d:Lcom/google/android/location/h/b/d;

.field protected e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/b/m;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected g:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/h/b/m;",
            ">;"
        }
    .end annotation
.end field

.field protected h:J

.field protected i:J

.field protected j:Las/d;

.field protected k:Las/d;

.field private l:Lcom/google/android/location/h/a/c;

.field private m:Lcom/google/googlenav/common/io/g;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/h$b;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/Object;

.field private r:I

.field private s:I

.field private t:I

.field private final u:J

.field private v:Z

.field private w:Lcom/google/android/location/h/j;


# direct methods
.method protected constructor <init>(Lcom/google/android/location/h/h$a;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/common/io/BaseHttpConnectionFactory;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    iput v1, p0, Lcom/google/android/location/h/h;->r:I

    iput v1, p0, Lcom/google/android/location/h/h;->s:I

    iput v1, p0, Lcom/google/android/location/h/h;->t:I

    iput-boolean v1, p0, Lcom/google/android/location/h/h;->v:Z

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->a(Lcom/google/android/location/h/h$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/h;->u:J

    new-instance v0, Lcom/google/android/location/h/b/d;

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->b(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->c(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->d(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->e(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "g"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/b/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {p0}, Lcom/google/android/location/h/h;->h()Las/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->c:Las/c;

    iget-object v0, p0, Lcom/google/android/location/h/h;->c:Las/c;

    invoke-virtual {v0}, Las/c;->d()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->f(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->g(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/location/h/h;->i()Lcom/google/android/location/h/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    iget-object v0, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/h/a/c;->a()V

    new-instance v0, Lcom/google/android/location/h/j;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/location/h/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    new-instance v0, Las/d;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    new-instance v2, Lcom/google/android/location/h/h$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/h/h$1;-><init>(Lcom/google/android/location/h/h;)V

    invoke-direct {v0, v1, v2}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    new-instance v0, Lcom/google/android/location/h/h$2;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/h/h$2;-><init>(Lcom/google/android/location/h/h;Las/c;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;I)I
    .locals 1

    iget v0, p0, Lcom/google/android/location/h/h;->s:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/location/h/h;->s:I

    return v0
.end method

.method private a(Lcom/google/android/location/h/b/o;Lcom/google/android/location/h/b/h;)Lcom/google/android/location/h/b/n;
    .locals 10

    invoke-virtual {p1}, Lcom/google/android/location/h/b/o;->v()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/h/b/i;

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->d()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->g()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->e()Lcom/google/android/location/h/b/b;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/location/h/b/i;-><init>(IILcom/google/android/location/h/b/b;)V

    new-instance v5, Ljava/util/Hashtable;

    invoke-direct {v5}, Ljava/util/Hashtable;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->f()[Lcom/google/android/location/h/b/b;

    move-result-object v6

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, v6

    if-ge v2, v0, :cond_2

    aget-object v7, v6, v2

    invoke-virtual {v7}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "Content-Location"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/h/b/i;

    if-nez v1, :cond_0

    invoke-virtual {v7}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v1

    const-string v8, "X-Masf-Response-Code"

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v8, Lcom/google/android/location/h/b/i;

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->d()I

    move-result v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v8, v9, v1, v7}, Lcom/google/android/location/h/b/i;-><init>(IILcom/google/android/location/h/b/b;)V

    invoke-virtual {v5, v0, v8}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v7}, Lcom/google/android/location/h/b/i;->a(Lcom/google/android/location/h/b/b;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v7}, Lcom/google/android/location/h/b/i;->a(Lcom/google/android/location/h/b/b;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v2

    :goto_2
    :try_start_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/i;

    iget-object v6, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/i;->a()Lcom/google/android/location/h/b/h;

    move-result-object v0

    invoke-virtual {v6, v3, v0}, Lcom/google/android/location/h/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Lcom/google/android/location/h/b/i;->a()Lcom/google/android/location/h/b/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/h/h;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/location/h/h;->q()[Lcom/google/android/location/h/h$b;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-interface {v2, p1}, Lcom/google/android/location/h/h$b;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->n()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    .locals 10

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->d_()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->h()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1f6

    if-ne v1, v3, :cond_0

    new-instance v2, Lcom/google/android/location/h/e;

    invoke-direct {v2, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_1

    new-instance v2, Lcom/google/android/location/h/e;

    invoke-direct {v2, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/location/h/h;->a(I)V

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    const-string v1, "application/binary"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Bad content-type"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    :try_start_0
    iget v1, p0, Lcom/google/android/location/h/h;->t:I

    int-to-long v1, v1

    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->e()J

    move-result-wide v6

    add-long/2addr v1, v6

    long-to-int v1, v1

    iput v1, p0, Lcom/google/android/location/h/h;->t:I

    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->j()Ljava/io/DataInputStream;

    move-result-object v1

    new-instance v7, Lcom/google/android/location/h/b/l;

    invoke-direct {v7, v1}, Lcom/google/android/location/h/b/l;-><init>(Ljava/io/DataInputStream;)V

    move v6, v5

    :goto_1
    array-length v1, p2

    if-ge v6, v1, :cond_8

    invoke-virtual {v7}, Lcom/google/android/location/h/b/l;->b()Lcom/google/android/location/h/b/n;

    move-result-object v3

    if-eqz v3, :cond_8

    move v1, v5

    :goto_2
    array-length v2, p2

    if-ge v1, v2, :cond_9

    aget-object v2, p2, v1

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->d()I

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->d()I

    move-result v9

    if-ne v8, v9, :cond_5

    const/4 v8, 0x0

    aput-object v8, p2, v1

    :goto_3
    if-eqz v2, :cond_7

    instance-of v1, v3, Lcom/google/android/location/h/b/h;

    if-eqz v1, :cond_4

    instance-of v1, v2, Lcom/google/android/location/h/b/o;

    if-eqz v1, :cond_4

    move-object v0, v2

    check-cast v0, Lcom/google/android/location/h/b/o;

    move-object v1, v0

    check-cast v3, Lcom/google/android/location/h/b/h;

    invoke-direct {p0, v1, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/o;Lcom/google/android/location/h/b/h;)Lcom/google/android/location/h/b/n;

    move-result-object v3

    :cond_4
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->g()I

    move-result v1

    const/16 v8, 0x226

    if-ne v1, v8, :cond_6

    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->h()V

    new-instance v3, Lcom/google/android/location/h/e;

    invoke-direct {v3, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v8

    invoke-direct {p0, v2, v3, v8, v9}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z

    :goto_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catch_0
    move-exception v1

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_7
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->b_()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_5
    :try_start_2
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->h()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :catch_1
    move-exception v1

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_8
    :try_start_3
    invoke-virtual {v7}, Lcom/google/android/location/h/b/l;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Request didn\'t complete"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v1

    goto :goto_5

    :cond_9
    move-object v2, v4

    goto/16 :goto_3
.end method

.method private a(Lcom/google/android/location/h/b/m;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/location/h/h;->q()[Lcom/google/android/location/h/h$b;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-interface {v2, p1}, Lcom/google/android/location/h/h$b;->a(Lcom/google/android/location/h/b/m;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->i()Lcom/google/android/location/h/b/m$a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/h/b/m$a;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    throw v0
.end method

.method public static declared-synchronized a(Lcom/google/android/location/h/h$a;)V
    .locals 2

    const-class v1, Lcom/google/android/location/h/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/h/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/h/h;-><init>(Lcom/google/android/location/h/h$a;)V

    sput-object v0, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/h;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;[Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/h/h;->a([Ljava/lang/Object;)V

    return-void
.end method

.method private a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v4

    move v1, v0

    :goto_0
    :try_start_0
    array-length v5, p1

    if-ge v1, v5, :cond_1

    aget-object v5, p1, v1

    if-eqz v5, :cond_0

    aget-object v5, p1, v1

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    aput-object v5, p1, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    if-eqz v1, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/m;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/m;->i()Lcom/google/android/location/h/b/m$a;

    move-result-object v1

    if-eqz v1, :cond_2

    aget-object v2, p1, v0

    invoke-interface {v1, v2, p2}, Lcom/google/android/location/h/b/m$a;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    return-void
.end method

.method private a([Lcom/google/android/location/h/b/m;Z)V
    .locals 8

    :try_start_0
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v1}, Lcom/google/android/location/h/b/d;->b_()I

    move-result v3

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    array-length v1, p1

    if-ge v4, v1, :cond_4

    aget-object v2, p1, v4

    if-nez v2, :cond_0

    move v1, v3

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    goto :goto_0

    :cond_0
    instance-of v1, v2, Lcom/google/android/location/h/b/o;

    if-eqz v1, :cond_2

    move-object v0, v2

    check-cast v0, Lcom/google/android/location/h/b/o;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/o;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v7, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    invoke-virtual {v7, v1}, Lcom/google/android/location/h/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    const/4 v6, 0x0

    :try_start_2
    aput-object v6, p1, v4

    check-cast v1, Lcom/google/android/location/h/b/n;

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move v1, v3

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_5
    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->b_()I

    move-result v1

    add-int/2addr v1, v3

    const v6, 0x8000

    if-le v1, v6, :cond_3

    invoke-virtual {p0, v5, p2}, Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V

    iget-object v1, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v1}, Lcom/google/android/location/h/b/d;->b_()I

    move-result v3

    invoke-virtual {v5}, Ljava/util/Vector;->removeAllElements()V

    :cond_3
    const/4 v1, 0x0

    aput-object v1, p1, v4

    invoke-virtual {v5, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->b_()I

    move-result v1

    add-int/2addr v1, v3

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0, v5, p2}, Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_2
.end method

.method private a([Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    aget-object v0, p1, v1

    check-cast v0, Lcom/google/android/location/h/b/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->a()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z
    .locals 2

    invoke-virtual {p1, p3, p4}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p3, p4}, Lcom/google/android/location/h/b/m;->b(J)V

    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a([Lcom/google/android/location/h/b/m;)[Lcom/google/android/location/h/b/m;
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    move v3, v2

    move-object v0, v4

    :goto_0
    array-length v5, p1

    if-ge v3, v5, :cond_2

    aget-object v5, p1, v3

    invoke-virtual {v5}, Lcom/google/android/location/h/b/m;->l()Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez v0, :cond_0

    array-length v0, p1

    new-array v0, v0, [Lcom/google/android/location/h/b/m;

    :cond_0
    aget-object v5, p1, v3

    aput-object v5, v0, v3

    aput-object v4, p1, v3

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_0

    :cond_1
    aget-object v5, p1, v3

    invoke-virtual {v5}, Lcom/google/android/location/h/b/m;->j()Z

    move-result v5

    if-eqz v5, :cond_5

    move-object v1, v0

    move v0, v2

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    :goto_2
    array-length v1, p1

    if-ge v2, v1, :cond_4

    aget-object v1, p1, v2

    if-eqz v1, :cond_3

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    aput-object v4, p1, v2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    return-object v0

    :cond_5
    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_1
.end method

.method private b([Lcom/google/android/location/h/b/m;)Ljava/io/InputStream;
    .locals 4

    array-length v0, p1

    new-array v1, v0, [Ljava/io/InputStream;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->c_()Ljava/io/InputStream;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v2}, Lcom/google/android/location/h/b/d;->c_()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/common/io/SequenceInputStream;

    invoke-direct {v3, v1}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>([Ljava/io/InputStream;)V

    invoke-direct {v0, v2, v3}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    return-object v0
.end method

.method private b(Z)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/h/h;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method private b(J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->c(J)V

    iget-object v2, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/h/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    return-void
.end method

.method public static declared-synchronized g()Lcom/google/android/location/h/h;
    .locals 2

    const-class v0, Lcom/google/android/location/h/h;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->m()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private p()V
    .locals 9

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v7

    move-wide v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v0, v2

    :cond_0
    :goto_1
    move-wide v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->o()J

    move-result-wide v0

    cmp-long v8, v0, v4

    if-eqz v8, :cond_2

    cmp-long v8, v2, v4

    if-eqz v8, :cond_0

    cmp-long v8, v2, v0

    if-gtz v8, :cond_0

    :cond_2
    move-wide v0, v2

    goto :goto_1

    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    monitor-exit v6

    :goto_2
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0, v2, v3}, Las/d;->b(J)V

    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    monitor-exit v6

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private q()[Lcom/google/android/location/h/h$b;
    .locals 3

    iget-object v1, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/location/h/h$b;

    iget-object v2, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized r()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/location/h/h;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/h/h;->r:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .locals 1

    new-instance v0, Lcom/google/android/location/h/i;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/h/i;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public a(Lcom/google/android/location/h/b/m;Z)V
    .locals 6

    iget-object v1, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->r()V

    invoke-direct {p0}, Lcom/google/android/location/h/h;->r()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/location/h/b/m;->a(I)V

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->n()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->k()V

    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/location/h/h;->m()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v2, ""

    invoke-virtual {v0, p1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Ljava/util/Vector;Z)V
    .locals 6

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/location/h/b/m;

    invoke-virtual {p1, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/google/android/location/h/h;->b([Lcom/google/android/location/h/b/m;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/location/h/h;->b(Z)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lcom/google/android/location/h/a/c;->a(Ljava/lang/String;I)Lcom/google/android/location/h/a/b;

    move-result-object v3

    const-string v1, "POST"

    invoke-interface {v3, v1}, Lcom/google/android/location/h/a/b;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v5

    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->a(Ljava/io/InputStream;)V

    iget-wide v0, p0, Lcom/google/android/location/h/h;->u:J

    invoke-interface {v3, v0, v1}, Lcom/google/android/location/h/a/b;->a(J)V

    const-string v0, "application/binary"

    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->b(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/location/h/h$3;

    iget-object v2, p0, Lcom/google/android/location/h/h;->c:Las/c;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/h$3;-><init>(Lcom/google/android/location/h/h;Las/c;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;I)V

    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->b(Las/a;)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->e()Z

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->f()I

    move-result v0

    return v0
.end method

.method protected h()Las/c;
    .locals 4

    new-instance v0, Las/c;

    new-instance v1, Lar/b;

    invoke-direct {v1}, Lar/b;-><init>()V

    const-string v2, "MobileServiceMux TaskRunner"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Las/c;-><init>(Lar/d;Ljava/lang/String;I)V

    return-object v0
.end method

.method protected i()Lcom/google/android/location/h/a/c;
    .locals 6

    new-instance v0, Lcom/google/android/location/h/a/c;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    new-instance v2, Lar/b;

    invoke-direct {v2}, Lar/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    const-string v4, "MobileServiceMux AsyncHttpRequestFactory"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/a/c;-><init>(Las/c;Lar/d;Lcom/google/googlenav/common/io/g;Ljava/lang/String;I)V

    return-object v0
.end method

.method protected j()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->a(J)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->b(J)V

    invoke-direct {p0}, Lcom/google/android/location/h/h;->o()V

    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/location/h/b/m;

    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/location/h/h;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;)[Lcom/google/android/location/h/b/m;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Z)V

    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public k()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/h/h;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/h/h;->v:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    const-string v1, "https:/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected m()V
    .locals 8

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0xa

    add-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/android/location/h/h;->i:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    const-wide/16 v4, 0x64

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    iput-wide v2, p0, Lcom/google/android/location/h/h;->i:J

    iget-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    iget-wide v1, p0, Lcom/google/android/location/h/h;->i:J

    invoke-virtual {v0, v1, v2}, Las/d;->b(J)V

    iget-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    iput-wide v2, p0, Lcom/google/android/location/h/h;->i:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lcom/google/android/location/h/h;->i:J

    iget-wide v2, p0, Lcom/google/android/location/h/h;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    iput-wide v0, p0, Lcom/google/android/location/h/h;->i:J

    goto :goto_0
.end method
