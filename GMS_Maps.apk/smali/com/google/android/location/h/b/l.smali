.class public Lcom/google/android/location/h/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/io/DataInputStream;

.field private b:Lcom/google/android/location/h/f;


# direct methods
.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unrecognised protocol version"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    iput-object v1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    return-void
.end method

.method public b()Lcom/google/android/location/h/b/n;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-virtual {v1}, Lcom/google/android/location/h/f;->a()V

    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v2

    const v3, 0x8100

    if-ne v2, v3, :cond_1

    new-instance v0, Lcom/google/android/location/h/f;

    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    new-instance v0, Lcom/google/android/location/h/b/k;

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-direct {v0, v1}, Lcom/google/android/location/h/b/k;-><init>(Lcom/google/android/location/h/f;)V

    :goto_0
    return-object v0

    :cond_1
    const v3, 0x8101

    if-ne v2, v3, :cond_2

    new-instance v0, Lcom/google/android/location/h/f;

    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    new-instance v0, Lcom/google/android/location/h/b/h;

    iget-object v1, p0, Lcom/google/android/location/h/b/l;->b:Lcom/google/android/location/h/f;

    invoke-direct {v0, v1}, Lcom/google/android/location/h/b/h;-><init>(Lcom/google/android/location/h/f;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/location/h/b/l;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->skipBytes(I)I

    goto :goto_0
.end method
