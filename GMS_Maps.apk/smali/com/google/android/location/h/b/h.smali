.class public Lcom/google/android/location/h/b/h;
.super Lcom/google/android/location/h/b/n;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/location/h/b/b;

.field private final c:[Lcom/google/android/location/h/b/b;

.field private final d:I


# direct methods
.method public constructor <init>(IILcom/google/android/location/h/b/b;[Lcom/google/android/location/h/b/b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/n;-><init>(II)V

    iput-object p3, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    iput-object p4, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    array-length v0, p4

    iput v0, p0, Lcom/google/android/location/h/b/h;->d:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/h/f;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/location/h/b/n;-><init>(Lcom/google/android/location/h/f;)V

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-static {v1}, Lcom/google/android/location/h/b/b;->a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lcom/google/android/location/h/b/h;->d:I

    iget v0, p0, Lcom/google/android/location/h/b/h;->d:I

    new-array v0, v0, [Lcom/google/android/location/h/b/b;

    iput-object v0, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/location/h/b/h;->d:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    invoke-static {v1}, Lcom/google/android/location/h/b/b;->a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;

    move-result-object v3

    aput-object v3, v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public b_()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->f()Lcom/google/android/location/h/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->f()Lcom/google/android/location/h/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/android/location/h/b/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    return-object v0
.end method

.method public f()[Lcom/google/android/location/h/b/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    return-object v0
.end method
