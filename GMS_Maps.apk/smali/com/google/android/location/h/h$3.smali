.class Lcom/google/android/location/h/h$3;
.super Las/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/h/a/b;

.field final synthetic b:[Lcom/google/android/location/h/b/m;

.field final synthetic f:I

.field final synthetic g:Lcom/google/android/location/h/h;


# direct methods
.method constructor <init>(Lcom/google/android/location/h/h;Las/c;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iput-object p3, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    iput-object p4, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    iput p5, p0, Lcom/google/android/location/h/h$3;->f:I

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v3}, Lcom/google/android/location/h/a/b;->k()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/location/h/b/m;->d(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    monitor-exit v1

    :cond_1
    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    iget-object v2, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v2}, Lcom/google/android/location/h/a/b;->c()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;[Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    invoke-static {v0, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget v1, p0, Lcom/google/android/location/h/h$3;->f:I

    invoke-static {v0, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v0}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    iget-object v0, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v0}, Lcom/google/android/location/h/a/b;->b()V

    goto :goto_1

    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    iget-object v2, p0, Lcom/google/android/location/h/h$3;->b:[Lcom/google/android/location/h/b/m;

    invoke-static {v0, v1, v2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->g:Lcom/google/android/location/h/h;

    invoke-static {v1}, Lcom/google/android/location/h/h;->b(Lcom/google/android/location/h/h;)V

    iget-object v1, p0, Lcom/google/android/location/h/h$3;->a:Lcom/google/android/location/h/a/b;

    invoke-interface {v1}, Lcom/google/android/location/h/a/b;->b()V

    throw v0
.end method
