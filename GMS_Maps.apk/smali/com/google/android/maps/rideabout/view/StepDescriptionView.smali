.class public Lcom/google/android/maps/rideabout/view/StepDescriptionView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field protected a:Landroid/view/View;

.field protected b:Landroid/widget/LinearLayout;

.field protected c:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

.field protected d:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

.field protected e:Landroid/widget/ImageView;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/view/View;

.field private l:F

.field private m:I

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Lcom/google/android/maps/rideabout/view/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->n:Z

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->n:Z

    invoke-direct {p0, p1}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->e()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f100476

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->c:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    const v0, 0x7f100477

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->d:Lcom/google/android/maps/rideabout/view/SqueezedLabelView;

    const v0, 0x7f100470

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->h:Landroid/view/View;

    const v0, 0x7f100471

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a:Landroid/view/View;

    const v0, 0x7f100475

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->b:Landroid/widget/LinearLayout;

    const v0, 0x7f100472

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->e:Landroid/widget/ImageView;

    const v0, 0x7f10047c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->i:Landroid/widget/ImageView;

    const v0, 0x7f10047d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->j:Landroid/widget/ImageView;

    const v0, 0x7f100473

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->f:Landroid/widget/TextView;

    const v0, 0x7f100474

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->g:Landroid/widget/TextView;

    const v0, 0x7f100291

    invoke-virtual {p0, v0}, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->k:Landroid/view/View;

    return-void
.end method


# virtual methods
.method protected c(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected e()I
    .locals 1

    const v0, 0x7f0401c1

    return v0
.end method

.method public setBackgroundChangeListener(Lcom/google/android/maps/rideabout/view/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/l;

    return-void
.end method

.method public setBackgroundType(I)V
    .locals 4

    const/4 v3, 0x0

    const v0, 0x7f020425

    const v1, 0x7f020422

    iget v2, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->o:I

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->o:I

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    :goto_1
    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/l;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->q:Lcom/google/android/maps/rideabout/view/l;

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/l;->a(I)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a:Landroid/view/View;

    const v2, 0x7f020450

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->a:Landroid/view/View;

    const v2, 0x7f02044d

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setButtonVisibility(ZZZ)V
    .locals 8

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v6, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->j:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    move v6, v1

    :goto_1
    if-ne p1, v0, :cond_3

    if-ne p2, v6, :cond_3

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v6, v2

    goto :goto_1

    :cond_3
    iget-object v7, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->j:Landroid/widget/ImageView;

    if-eqz p2, :cond_5

    move v1, v2

    :goto_3
    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_6

    :goto_4
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->j:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->i:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    if-eqz p3, :cond_0

    if-eq v0, p1, :cond_4

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_7

    move v1, v4

    :goto_5
    if-eqz p1, :cond_8

    move v0, v4

    :goto_6
    invoke-direct {v2, v1, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz p1, :cond_9

    const-wide/16 v0, 0x0

    :goto_7
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    if-eq v6, p2, :cond_0

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    if-eqz v6, :cond_a

    move v0, v4

    :goto_8
    if-eqz p2, :cond_b

    :goto_9
    invoke-direct {v2, v0, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz p2, :cond_c

    const-wide/16 v0, 0x0

    :goto_a
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_5

    :cond_8
    move v0, v5

    goto :goto_6

    :cond_9
    const-wide/16 v0, 0x1f4

    goto :goto_7

    :cond_a
    move v0, v5

    goto :goto_8

    :cond_b
    move v4, v5

    goto :goto_9

    :cond_c
    const-wide/16 v0, 0x1f4

    goto :goto_a
.end method

.method public setDistanceMeters(F)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->l:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->l:F

    :cond_0
    return-void
.end method

.method public setDistanceUnits(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->m:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->m:I

    :cond_0
    return-void
.end method

.method public setShowDistance(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->n:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->n:Z

    :cond_0
    return-void
.end method

.method public setUseLongDistanceStepFormat(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->p:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/maps/rideabout/view/StepDescriptionView;->p:Z

    :cond_0
    return-void
.end method
