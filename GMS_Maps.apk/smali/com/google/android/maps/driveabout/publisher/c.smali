.class Lcom/google/android/maps/driveabout/publisher/c;
.super Lcom/google/android/maps/driveabout/publisher/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/publisher/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 7

    const/4 v6, 0x2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    iput v6, v0, Landroid/os/Message;->what:I

    :goto_0
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    const-string v4, "com.google.android.apps.maps.NAVIGATION_DATA"

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->checkCallingPermission(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v4

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    const-string v2, "P"

    invoke-static {v2, v4}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    iput v2, v0, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/publisher/c;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Landroid/os/Messenger;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    goto :goto_0

    :cond_0
    iput v6, v0, Landroid/os/Message;->what:I

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
