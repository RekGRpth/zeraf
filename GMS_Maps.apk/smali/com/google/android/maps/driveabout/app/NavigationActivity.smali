.class public Lcom/google/android/maps/driveabout/app/NavigationActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/Y;


# static fields
.field private static a:J


# instance fields
.field private b:Z

.field private c:Z

.field private d:Landroid/view/ViewGroup;

.field private e:Landroid/view/ViewGroup;

.field private f:Lcom/google/android/maps/driveabout/app/NavigationView;

.field private g:Lcom/google/android/maps/driveabout/app/LoadingView;

.field private h:Lcom/google/android/maps/driveabout/app/an;

.field private i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private j:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroid/os/Handler;

.field private final p:Landroid/content/ServiceConnection;

.field private final q:Lcom/google/android/maps/driveabout/app/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m:Z

    new-instance v0, Lcom/google/android/maps/driveabout/app/bU;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bU;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/google/android/maps/driveabout/app/l;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    return-void
.end method

.method static synthetic a(J)J
    .locals 2

    sget-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    add-long/2addr v0, p0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/LoadingView;)Lcom/google/android/maps/driveabout/app/LoadingView;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/NavigationService;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->k()LO/U;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return-object v0

    :cond_0
    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->l()I

    move-result v0

    invoke-static {v1, v0, p1}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    return-void
.end method

.method private a(I)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->v()V

    :cond_1
    return v1

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->onSearchRequested()Z

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->e()V

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->j()V

    goto :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->h()V

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->a()Lo/aR;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/app/dO;->a(Lo/aR;)V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->d()V

    goto :goto_0

    :sswitch_7
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Z)V

    goto :goto_0

    :sswitch_8
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Z)V

    goto :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->i()V

    goto :goto_0

    :sswitch_a
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/googlenav/K;->ae()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "A"

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/maps/driveabout/app/SettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "I"

    invoke-static {v2, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v3, Lcom/google/android/maps/driveabout/app/bY;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/bY;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dO;->r()Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    goto/16 :goto_0

    :sswitch_e
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->w()V

    move v0, v1

    goto/16 :goto_0

    :sswitch_f
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/dO;->g(Z)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dO;->g(Z)V

    goto/16 :goto_0

    :sswitch_11
    invoke-static {}, Ll/f;->e()Ll/f;

    move-result-object v2

    invoke-virtual {v2}, Ll/f;->c()V

    goto/16 :goto_0

    :sswitch_12
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1000f2 -> :sswitch_9
        0x7f1000f3 -> :sswitch_1
        0x7f1000f4 -> :sswitch_5
        0x7f1000f5 -> :sswitch_d
        0x7f1000f6 -> :sswitch_b
        0x7f1000f7 -> :sswitch_a
        0x7f100118 -> :sswitch_3
        0x7f100119 -> :sswitch_2
        0x7f10011a -> :sswitch_6
        0x7f10011b -> :sswitch_e
        0x7f10011d -> :sswitch_0
        0x7f10011e -> :sswitch_9
        0x7f100120 -> :sswitch_c
        0x7f1004a2 -> :sswitch_4
        0x7f1004a3 -> :sswitch_4
        0x7f1004a4 -> :sswitch_1
        0x7f1004a5 -> :sswitch_b
        0x7f1004a6 -> :sswitch_7
        0x7f1004a7 -> :sswitch_8
        0x7f1004a8 -> :sswitch_f
        0x7f1004a9 -> :sswitch_10
        0x7f1004aa -> :sswitch_11
        0x7f1004ab -> :sswitch_12
    .end sparse-switch
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    return p1
.end method

.method public static b()J
    .locals 2

    sget-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationMapView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/LoadingView;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/LoadingView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cd;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/cd;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/LoadingView;->setOnFirstDrawListener(Lcom/google/android/maps/driveabout/app/bQ;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object v0
.end method

.method private g()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/driveabout/app/ce;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/ce;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    return-void
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method private h()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private i()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v2, Lcom/google/android/maps/driveabout/app/cf;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cf;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cg;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/cg;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f()V

    return-void
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ch;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ch;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/bV;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/bV;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g()V

    return-void
.end method

.method private k()V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/bW;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bW;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic k(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l()V

    return-void
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c:Z

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    :cond_4
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    sget-object v0, Lcom/google/googlenav/z;->b:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    const-string v0, ")"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n_()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    goto :goto_0
.end method

.method static synthetic l(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h()V

    return-void
.end method

.method private m()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private n()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/NavigationView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDialogManager(Lcom/google/android/maps/driveabout/app/an;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapView(Lcom/google/android/maps/driveabout/app/NavigationMapView;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m_()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    const v2, 0x7f100110

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/app/NavigationView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public c()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->stopService(Landroid/content/Intent;)Z

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->finish()V

    return-void
.end method

.method public d()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.maps.driveabout.app.STARTING_NAVIGATION_INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-static {}, Ll/f;->e()Ll/f;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ll/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public e()Lcom/google/android/maps/driveabout/app/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method public getNfcUrl()Ljava/lang/String;
    .locals 2

    const-string v0, "n"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "N"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ca;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ca;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-static {p0, p2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/eR;->a(Landroid/content/Context;ILandroid/content/Intent;Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/eU;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->v()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/l;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v3, 0x480000

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setDefaultKeyMode(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Show Disclaimer"

    invoke-static {p0, v3, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    new-instance v0, Lcom/google/android/maps/driveabout/app/an;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v3, Lcom/google/android/maps/driveabout/app/cb;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cb;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;J)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cc;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cc;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v2, v3, v0}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    :goto_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    :goto_3
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    const-string v0, "IsActivityRestart"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g()V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->finish()V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f()V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/dO;->b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o_()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f()V

    :cond_1
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    :cond_2
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->d(Landroid/content/Context;)I

    move-result v0

    if-ne p1, v0, :cond_0

    const-string v0, "I"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->r()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/dO;->s()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    goto :goto_0

    :cond_2
    const/16 v1, 0x52

    if-ne p1, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/dO;->t()V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i()V

    :cond_0
    return-void
.end method

.method public onMenuButtonClick(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->t()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->openOptionsMenu()V

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(I)Z

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "UserRequestedReroute"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(I)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->c()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->c()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b()V

    :cond_1
    const-string v0, "("

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    :cond_2
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;)V

    :cond_3
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->e()V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    :cond_5
    return-void

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->c()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_7
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c:Z

    if-eqz v0, :cond_0

    :cond_8
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/bS;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/app/bS;-><init>(Landroid/view/Menu;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->b()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l()V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;Lcom/google/googlenav/android/Y;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "IsActivityRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_2

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->c()Lo/ae;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/bZ;

    invoke-direct {v1, p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/app/bZ;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;Ljava/lang/String;ZZ)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dO;

    move-result-object v2

    invoke-virtual {v2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eE;)V

    goto :goto_0
.end method
