.class public Lcom/google/android/maps/driveabout/app/dN;
.super Lcom/google/android/maps/driveabout/app/aP;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/Path;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Lo/T;

.field private g:Lo/X;

.field private h:LD/b;

.field private i:F

.field private j:[I

.field private k:I

.field private l:Landroid/graphics/Bitmap;

.field private final m:Lo/T;

.field private final n:Lo/T;


# direct methods
.method public constructor <init>(LO/z;LO/N;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aP;-><init>()V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->m:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->n:Lo/T;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dN;->a(LO/z;LO/N;)V

    return-void
.end method

.method static a(LC/a;Lo/X;Lo/T;I)Lo/X;
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x0

    new-instance v0, Lo/T;

    invoke-direct {v0, p3, p3}, Lo/T;-><init>(II)V

    new-instance v1, Lo/ad;

    invoke-virtual {p2, v0}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p2, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lo/h;

    invoke-direct {v0, v1}, Lo/h;-><init>(Lo/ae;)V

    invoke-virtual {v0, p1, v6}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_2
    move v1, v2

    move-object v3, v4

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    move v5, v2

    :goto_3
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v7

    if-ge v5, v7, :cond_3

    invoke-virtual {v0, v5}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v7, p2}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v3, v0

    :cond_3
    if-eqz v3, :cond_5

    move-object v0, v3

    goto :goto_1

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    move-object v0, v3

    goto :goto_1
.end method

.method static a(LO/z;I)Lo/X;
    .locals 8

    const-wide v6, 0x409f400000000000L

    invoke-virtual {p0, p1}, LO/z;->b(I)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    sub-double v4, v0, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, LO/z;->a(D)I

    move-result v2

    invoke-virtual {p0}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v3

    add-double/2addr v0, v6

    const-wide/high16 v4, 0x3ff0000000000000L

    sub-double/2addr v0, v4

    invoke-virtual {p0, v0, v1}, LO/z;->a(D)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Lo/am;

    invoke-virtual {p0}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-direct {v1, v3, v2, v0}, Lo/am;-><init>(Lo/X;II)V

    invoke-virtual {v1}, Lo/am;->e()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method private a(F)V
    .locals 11

    const/4 v10, -0x1

    const v9, -0xbbbbbc

    const/4 v8, 0x1

    const/high16 v7, 0x3f800000

    const/high16 v0, 0x40800000

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/high16 v1, 0x41980000

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/high16 v2, 0x41c00000

    mul-float/2addr v2, p1

    const/high16 v3, 0x3f000000

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/high16 v3, 0x40000000

    mul-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    int-to-float v6, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    int-to-float v6, v2

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    const/4 v6, 0x0

    neg-int v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    invoke-virtual {v5, v6, v1}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    neg-int v2, v4

    int-to-float v2, v2

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    int-to-float v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    mul-int/lit8 v2, v3, 0x2

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v0, 0x432a0000

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, LD/b;->c(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dN;->k:I

    return-void
.end method

.method private a(LD/a;LC/a;)V
    .locals 5

    invoke-virtual {p2}, LC/a;->m()F

    move-result v0

    invoke-virtual {p2}, LC/a;->y()F

    move-result v1

    mul-float/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dN;->a(F)V

    :cond_0
    const/4 v0, 0x0

    const/high16 v2, 0x44fa0000

    const/high16 v3, 0x41400000

    mul-float/2addr v3, v1

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    const/16 v0, 0x7d0

    const/high16 v2, 0x42000000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->g:Lo/X;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    invoke-static {p2, v1, v2, v0}, Lcom/google/android/maps/driveabout/app/dN;->a(LC/a;Lo/X;Lo/T;I)Lo/X;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dN;->k:I

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dN;->b(LC/a;Lo/X;Lo/T;I)[I

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->j:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->j:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->j:[I

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->m:Lo/T;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dN;->n:Lo/T;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v4, v2}, Lo/X;->a(ILo/T;)V

    invoke-virtual {v0, v3}, Lo/X;->a(Lo/T;)V

    invoke-static {v2, v3}, Lo/T;->a(Lo/T;Lo/T;)D

    move-result-wide v2

    double-to-float v0, v2

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dN;->i()Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/maps/driveabout/app/dN;->a(Landroid/graphics/Canvas;[IF)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dN;->e()V

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    invoke-virtual {v0, v2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dN;->e()V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;[IF)V
    .locals 4

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    const/4 v0, 0x0

    aget v0, p2, v0

    int-to-float v0, v0

    const/4 v2, 0x1

    aget v2, p2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x2

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    aget v2, p2, v0

    int-to-float v2, v2

    add-int/lit8 v3, v0, 0x1

    aget v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    array-length v0, p2

    add-int/lit8 v0, v0, -0x2

    aget v0, p2, v0

    int-to-float v0, v0

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    aget v1, p2, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method static b(LC/a;Lo/X;Lo/T;I)[I
    .locals 8

    const/4 v0, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f000000

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {p0}, LC/a;->y()F

    move-result v2

    new-instance v3, Lo/T;

    int-to-float v4, p3

    mul-float/2addr v4, v2

    mul-float/2addr v4, v6

    float-to-int v4, v4

    int-to-float v5, p3

    mul-float/2addr v5, v2

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Lo/T;-><init>(II)V

    invoke-virtual {p2, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    const/high16 v4, 0x3f800000

    div-float v2, v4, v2

    invoke-virtual {p1, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    invoke-static {v4, v3, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    aput v5, v1, v7

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v5, p3, v5

    aput v5, v1, v0

    :goto_0
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v5

    if-ge v0, v5, :cond_0

    invoke-virtual {p1, v0, v4}, Lo/X;->a(ILo/T;)V

    invoke-static {v4, v3, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    mul-int/lit8 v5, v0, 0x2

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    aput v6, v1, v5

    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    sub-int v6, p3, v6

    aput v6, v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->j:[I

    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->a:Landroid/graphics/Path;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->b:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->c:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->d:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->e:Landroid/graphics/Paint;

    return-void
.end method

.method private i()Landroid/graphics/Bitmap;
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->l:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dN;->k:I

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dN;->k:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->l:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->l:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 4

    const v3, 0x3dcccccd

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    invoke-virtual {v0, v1}, Lo/aQ;->a(Lo/T;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/android/maps/driveabout/app/dN;->i:F

    add-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_2

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dN;->i:F

    sub-float/2addr v2, v3

    cmpg-float v2, v1, v2

    if-gez v2, :cond_3

    :cond_2
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dN;->i:F

    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dN;->a(LD/a;LC/a;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dN;->k:I

    int-to-float v2, v2

    const/high16 v3, 0x3f000000

    mul-float/2addr v2, v3

    invoke-virtual {p2}, LC/a;->y()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    invoke-virtual {p1}, LD/a;->p()V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v1, p1, LD/a;->h:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    iget-object v1, p1, LD/a;->d:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->h:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(LO/z;LO/N;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, LO/N;->z()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/dN;->a(LO/z;I)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->g:Lo/X;

    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dN;->i:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LD/a;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dN;->e()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dN;->h()V

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dN;->i:F

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/dN;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/maps/driveabout/app/dN;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dN;->g:Lo/X;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/dN;->g:Lo/X;

    invoke-virtual {v2, v3}, Lo/X;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dN;->g:Lo/X;

    invoke-virtual {v0}, Lo/X;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dN;->f:Lo/T;

    invoke-virtual {v1}, Lo/T;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x25

    add-int/2addr v0, v1

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->o:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
