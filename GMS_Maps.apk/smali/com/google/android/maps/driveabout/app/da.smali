.class public Lcom/google/android/maps/driveabout/app/da;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Law/p;

.field private final b:Ljava/io/File;

.field private c:LaN/B;

.field private d:LaN/H;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:[B

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->a:Law/p;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/da;->b:Ljava/io/File;

    return-void
.end method

.method private a(LO/z;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v0

    invoke-virtual {v0}, LO/V;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/io/File;)[B
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/32 v3, 0x19000

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    long-to-int v1, v1

    new-array v1, v1, [B

    const/4 v2, 0x0

    :goto_1
    :try_start_1
    array-length v4, v1

    if-ge v2, v4, :cond_1

    array-length v4, v1

    sub-int/2addr v4, v2

    invoke-virtual {v3, v1, v2, v4}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-ltz v4, :cond_1

    add-int/2addr v2, v4

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_3
    throw v0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method a(LO/z;LO/z;LaH/h;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, LO/z;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, LO/z;->n()Lo/X;

    move-result-object v0

    invoke-virtual {p1}, LO/z;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lo/u;

    invoke-virtual {p1}, LO/z;->l()LO/U;

    move-result-object v2

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p3}, LaH/h;->q()Lo/u;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    new-instance v3, Lo/u;

    invoke-virtual {v0}, Lo/T;->a()I

    move-result v4

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    invoke-direct {v3, v4, v0}, Lo/u;-><init>(II)V

    aput-object v3, v1, v2

    invoke-virtual {p2}, LO/z;->l()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-virtual {p2}, LO/z;->m()LO/U;

    move-result-object v2

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/app/da;->a(LO/z;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LO/z;->d()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ll/b;->a(Lo/u;[Lo/u;Lo/u;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v4, [Lo/u;

    invoke-virtual {p3}, LaH/h;->q()Lo/u;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p1}, LO/z;->l()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-virtual {p1}, LO/z;->m()LO/U;

    move-result-object v2

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/da;->a(LO/z;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LO/z;->d()I

    move-result v4

    invoke-static {v1, v0, v2, v3, v4}, Ll/b;->a(Lo/u;[Lo/u;Lo/u;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;Lcom/google/android/maps/driveabout/app/cS;)V
    .locals 7

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->r()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->c:LaN/B;

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cq;->a()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {v0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->a()I

    move-result v1

    invoke-virtual {v0}, Lo/aR;->g()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->a()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/app/da;->g:I

    invoke-virtual {v0}, Lo/aR;->i()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->c()I

    move-result v1

    invoke-virtual {v0}, Lo/aR;->g()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/da;->h:I

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cq;->b()Lo/T;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cq;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/app/cS;->h()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_3

    const/4 v3, 0x1

    :goto_0
    new-instance v0, LaN/H;

    new-instance v1, LaN/B;

    invoke-virtual {v2}, Lo/T;->a()I

    move-result v6

    invoke-virtual {v2}, Lo/T;->c()I

    move-result v2

    invoke-direct {v1, v6, v2}, LaN/B;-><init>(II)V

    invoke-static {v5}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->d:LaN/H;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->e:Ljava/lang/String;

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->n()LO/z;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->b()LaH/h;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/da;->a(LO/z;LO/z;LaH/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->f:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method public a(Ll/f;Lcom/google/android/maps/driveabout/app/cQ;)V
    .locals 1

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-interface {p2, v0}, Lcom/google/android/maps/driveabout/app/cQ;->a(Ljava/io/OutputStream;)Z

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->i:[B

    invoke-virtual {p1}, Ll/f;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/da;->j:Ljava/lang/String;

    return-void
.end method

.method public a(ZLaZ/b;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/da;->b:Ljava/io/File;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/da;->a(Ljava/io/File;)[B

    move-result-object v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/da;->c:LaN/B;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/da;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v1, LaZ/a;

    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/maps/driveabout/app/da;->c:LaN/B;

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/maps/driveabout/app/da;->d:LaN/H;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/maps/driveabout/app/da;->g:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/maps/driveabout/app/da;->h:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/maps/driveabout/app/da;->f:Ljava/lang/String;

    move/from16 v7, p1

    move-object/from16 v13, p2

    invoke-direct/range {v1 .. v13}, LaZ/a;-><init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IILjava/lang/String;LaZ/b;)V

    :goto_1
    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/da;->a:Law/p;

    invoke-interface {v2, v1}, Law/p;->c(Law/g;)V

    goto :goto_0

    :cond_2
    const/4 v12, -0x1

    const/4 v13, -0x1

    new-instance v1, LaZ/a;

    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/maps/driveabout/app/da;->c:LaN/B;

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/maps/driveabout/app/da;->d:LaN/H;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/maps/driveabout/app/da;->g:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/maps/driveabout/app/da;->h:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/maps/driveabout/app/da;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/maps/driveabout/app/da;->f:Ljava/lang/String;

    move/from16 v7, p1

    move-object/from16 v16, p2

    invoke-direct/range {v1 .. v16}, LaZ/a;-><init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IIIILjava/lang/String;Ljava/lang/String;LaZ/b;)V

    goto :goto_1
.end method
