.class public Lcom/google/android/maps/driveabout/app/NavigationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:J


# instance fields
.field private A:Lcom/google/android/maps/driveabout/app/a;

.field private b:Lcom/google/android/maps/driveabout/app/dc;

.field private c:Law/p;

.field private d:LO/t;

.field private e:Lcom/google/android/maps/driveabout/app/dO;

.field private f:Lcom/google/android/maps/driveabout/app/aQ;

.field private g:Lcom/google/android/maps/driveabout/app/cS;

.field private h:Lcom/google/android/maps/driveabout/app/cT;

.field private i:LM/n;

.field private j:LL/j;

.field private k:LL/G;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/maps/driveabout/app/af;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;

.field private final t:Landroid/content/BroadcastReceiver;

.field private final u:Landroid/content/BroadcastReceiver;

.field private final v:Landroid/content/BroadcastReceiver;

.field private w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

.field private x:LO/q;

.field private final y:Landroid/content/ServiceConnection;

.field private final z:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    new-instance v0, Lcom/google/android/maps/driveabout/app/cD;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/cD;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/cw;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cw;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cw;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cx;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cx;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cy;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cy;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->y:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cE;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cE;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->z:Landroid/os/IBinder;

    return-void
.end method

.method private static a(Landroid/content/res/Configuration;)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_1

    move v1, v0

    :goto_0
    and-int/lit8 v2, v1, 0xf

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    and-int/lit8 v0, v1, 0x30

    :cond_0
    return v0

    :cond_1
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->x:LO/q;

    return-object p1
.end method

.method private a(Ljava/lang/String;ZI)Landroid/content/Intent;
    .locals 8

    const/4 v7, 0x0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/google/android/maps/driveabout/app/dr;->a(Z)V

    new-instance v6, LL/O;

    invoke-direct {v6, p0, v1}, LL/O;-><init>(Landroid/content/Context;Ljava/io/File;)V

    invoke-virtual {v6}, LL/O;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    new-instance v0, LL/G;

    invoke-direct {v0}, LL/G;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0, v2}, LM/n;->a(LL/G;)V

    if-eqz p2, :cond_2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    new-instance v2, LL/I;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    invoke-direct {v2, v4, v3}, LL/I;-><init>(Lcom/google/android/maps/driveabout/app/dO;Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v2}, LL/G;->a(LL/P;)V

    new-instance v0, LL/b;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct/range {v0 .. v5}, LL/b;-><init>(Ljava/lang/String;LL/k;Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v1, v0}, LL/G;->a(LL/b;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v1, v7}, LL/G;->a(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    new-instance v2, Lcom/google/android/maps/driveabout/app/cC;

    invoke-direct {v2, p0, v0}, Lcom/google/android/maps/driveabout/app/cC;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;LL/b;)V

    invoke-virtual {v1, v2}, LL/G;->a(LL/H;)V

    :cond_2
    if-ltz p3, :cond_4

    if-nez p3, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int p3, v0

    :cond_3
    new-instance v0, LL/M;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    int-to-long v2, p3

    const/16 v4, 0x1e

    const/16 v5, 0xa

    invoke-direct/range {v0 .. v5}, LL/M;-><init>(Lcom/google/android/maps/driveabout/app/dO;JII)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0, v6}, LL/G;->a(Ll/f;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v6}, LL/O;->g()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    return-object v0

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/aQ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "DriveAbout"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "InterruptedDestination"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "InterruptedChimeBeforeSpeech"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private a(Landroid/content/Intent;II)V
    .locals 10

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->v()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a()V

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "f:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NavigationService:onStartInternal"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "event_log"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    const-string v0, "NavigationService"

    const-string v1, "onStartInternal. Received a \"com.google.android.maps.driveabout.REPLAY_LOG\" intent without an associated \"event_log\" extra."

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    if-nez v0, :cond_7

    if-eqz v1, :cond_7

    const-string v0, "NavigationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartInternal. Ignoring \"event_log\" extra in a \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" intent. Event logs should only be specified in a \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" intent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_log"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_5
    :goto_3
    const/4 v0, 0x1

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Lcom/google/android/maps/driveabout/app/bn;

    invoke-direct {v7, v5}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->e()V

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->i()I

    move-result v6

    const/4 v0, 0x2

    if-ne v6, v0, :cond_8

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->j()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->g()V

    move v0, v6

    :cond_6
    :goto_4
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v1

    if-nez v1, :cond_11

    const/4 v1, 0x3

    if-eq v0, v1, :cond_11

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    goto/16 :goto_0

    :cond_7
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    const-string v0, "is_feature_test"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    const-string v0, "random_ui_seed"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;ZI)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v1, "Unable to replay the event log: "

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    :cond_8
    const/4 v0, 0x3

    if-ne v6, v0, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v1, LL/A;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/j;->a(Ll/j;)V

    move v0, v6

    goto :goto_4

    :cond_9
    const/4 v0, 0x6

    if-ne v6, v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->d()[LO/U;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v2

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->g()[LO/b;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->s:Ljava/lang/String;

    const/4 v0, 0x5

    if-ne v6, v0, :cond_c

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->c()LO/U;

    move-result-object v0

    const/4 v5, 0x2

    new-array v5, v5, [LO/U;

    const/4 v7, 0x0

    aput-object v0, v5, v7

    const/4 v0, 0x1

    array-length v7, v1

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v1, v7

    aput-object v7, v5, v0

    invoke-static {p0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    invoke-virtual {v0, v5}, LO/M;->a([LO/U;)LO/a;

    move-result-object v5

    invoke-virtual {v0}, LO/M;->d()V

    if-eqz v5, :cond_b

    const-string v0, "~"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;LO/a;)V

    :cond_b
    move v0, v6

    goto/16 :goto_4

    :cond_c
    const-string v0, "UserRequestedReroute"

    const/4 v8, 0x0

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/app/cS;->l()Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/app/cT;->b()Landroid/content/Intent;

    move-result-object v8

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {v8}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_d
    invoke-direct {p0, v5}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->k()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/app/bn;->b()LO/U;

    move-result-object v0

    invoke-virtual {v0}, LO/U;->d()LO/V;

    move-result-object v5

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v7

    if-eqz v5, :cond_e

    invoke-virtual {v5}, LO/V;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    if-nez v7, :cond_10

    const/4 v0, 0x0

    :goto_5
    invoke-static {p0, v5, v8, v0}, Lcom/google/googlenav/provider/SearchHistoryProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    :cond_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/a;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/app/aQ;->a([LO/U;I[LO/b;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v1, LL/A;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LL/j;->a(Ll/j;)V

    :cond_f
    move v0, v6

    goto/16 :goto_4

    :cond_10
    new-instance v0, LaN/B;

    invoke-virtual {v7}, Lo/u;->a()I

    move-result v9

    invoke-virtual {v7}, Lo/u;->a()I

    move-result v7

    invoke-direct {v0, v9, v7}, LaN/B;-><init>(II)V

    goto :goto_5

    :cond_11
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->T()V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->d()V

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    move-object v1, v0

    :goto_0
    if-nez p1, :cond_2

    :goto_1
    new-instance v2, Lcom/google/android/maps/driveabout/app/cB;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cB;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/power/a;->a()Lcom/google/android/maps/driveabout/power/f;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/power/f;->a(Lcom/google/android/maps/driveabout/power/i;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/power/f;->a(Landroid/view/Window;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/power/f;->a(Lcom/google/android/maps/driveabout/power/e;)Lcom/google/android/maps/driveabout/power/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/power/f;->a()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PowerManagerEnabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UserPreference"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/maps/driveabout/app/cA;

    invoke-direct {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/cA;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "InterruptedDestination"

    invoke-static {p0, v0, p1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    return p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->c()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationService;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->m:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->m()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->w:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->x:LO/q;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    return-object v0
.end method

.method private m()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->n:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->V()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->U()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->W()V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->Q()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->R()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->S()V

    goto :goto_0
.end method

.method private n()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "InterruptedDestination"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LR/s;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "InterruptedChimeBeforeSpeech"

    invoke-static {p0, v1, v4}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    if-eqz v0, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v0, "UserRequestedReroute"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2, v4}, Lcom/google/android/maps/driveabout/app/NavigationService;->onStart(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->stopSelf()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/res/Configuration;)I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/res/Configuration;)I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->L()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(Z)V

    const-string v0, "InterruptedChimeBeforeSpeech"

    invoke-static {p0, v0, p1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public a([LO/U;I[LO/b;)V
    .locals 3

    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/U;I[LO/b;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    new-instance v2, LL/A;

    invoke-direct {v2, v0}, LL/A;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LL/j;->a(Ll/j;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->l:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    return v0
.end method

.method public c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->b()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    if-nez v0, :cond_0

    const-string v0, "Activity paused, no service. "

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->c()V

    return-void
.end method

.method public g()V
    .locals 1

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->stopSelf()V

    goto :goto_0
.end method

.method public h()Lcom/google/android/maps/driveabout/app/dO;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->r:Z

    return v0
.end method

.method public k()LO/U;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->C()LO/U;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->z:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 11

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/google/android/maps/driveabout/app/af;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->o:Lcom/google/android/maps/driveabout/app/af;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dc;->a(Landroid/content/Context;)Lcom/google/android/maps/driveabout/app/dc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->b:Lcom/google/android/maps/driveabout/app/dc;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    invoke-interface {v0, v10}, Law/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Law/p;->c(I)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/cz;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cz;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    invoke-static {v1, v0, v2, v10}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    invoke-static {v10}, Lcom/google/android/maps/driveabout/app/dr;->a(Z)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dr;->b(Z)V

    invoke-static {v1}, LO/t;->a(Landroid/content/Context;)LO/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    new-instance v0, LM/n;

    invoke-direct {v0, v1}, LM/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    new-instance v0, Lcom/google/android/maps/driveabout/app/dO;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dO;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    new-instance v0, Lcom/google/android/maps/driveabout/app/a;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/a;-><init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    new-instance v3, Lcom/google/android/maps/driveabout/app/bC;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->c:Law/p;

    invoke-direct {v3, v1, v0}, Lcom/google/android/maps/driveabout/app/bC;-><init>(Landroid/content/Context;Law/p;)V

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LL/j;

    invoke-direct {v0, v1}, LL/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "gps"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "network"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    const-string v2, "android_orientation"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-virtual {v4}, LL/j;->f()LM/b;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, LM/n;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    invoke-static {v0}, Ll/f;->a(Ll/f;)V

    :cond_0
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/cT;->a(Landroid/app/Service;)Lcom/google/android/maps/driveabout/app/cT;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    new-instance v0, Lcom/google/android/maps/driveabout/app/aQ;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/aQ;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    new-instance v0, Lcom/google/android/maps/driveabout/app/cS;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    new-instance v5, Lcom/google/android/maps/driveabout/vector/bB;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/maps/driveabout/vector/bB;-><init>(Law/p;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/cS;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/bC;Lcom/google/android/maps/driveabout/app/cT;Lcom/google/android/maps/driveabout/vector/bB;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    new-instance v2, Lcom/google/android/maps/driveabout/app/dx;

    invoke-direct {v2}, Lcom/google/android/maps/driveabout/app/dx;-><init>()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v1, v0, v2, v3, v4}, LQ/p;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/aQ;LO/t;)LQ/p;

    move-result-object v9

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    iget-object v7, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->o:Lcom/google/android/maps/driveabout/app/af;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    move-object v3, p0

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/maps/driveabout/app/aQ;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/app/dO;LO/t;LM/n;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->e:Lcom/google/android/maps/driveabout/app/dO;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->g:Lcom/google/android/maps/driveabout/app/cS;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->j:LL/j;

    move-object v5, v9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/dO;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;Ll/f;LQ/p;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DOCK_EVENT"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->setPriority(I)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->n()V

    :cond_1
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->D()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Lcom/google/android/maps/driveabout/power/BatteryMonitor;->a(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(LO/t;)V

    :cond_2
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    iput-boolean v10, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    return-void
.end method

.method public onDestroy()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    invoke-virtual {v0}, LL/G;->b()V

    iput-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->k:LL/G;

    :cond_0
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->i:LM/n;

    invoke-virtual {v0}, LM/n;->k()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->A:Lcom/google/android/maps/driveabout/app/a;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->b:Lcom/google/android/maps/driveabout/app/dc;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dc;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-virtual {v0}, LO/t;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->d:LO/t;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(LO/t;)V

    const-string v0, "g"

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/maps/driveabout/app/NavigationService;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->ag()F

    move-result v2

    long-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->s:Ljava/lang/String;

    float-to-int v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/ds;->a(ILjava/lang/String;I)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->p:Z

    if-eqz v0, :cond_1

    const-string v0, "Service destroyed and activity paused. "

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->h:Lcom/google/android/maps/driveabout/app/cT;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cT;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->q:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationService;->f:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->a()V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationService;)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Intent;II)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Landroid/content/Intent;II)V

    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationService;->g()V

    return-void
.end method
