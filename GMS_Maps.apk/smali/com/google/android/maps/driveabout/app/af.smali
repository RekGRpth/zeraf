.class public Lcom/google/android/maps/driveabout/app/af;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/maps/driveabout/app/al;

.field private d:Landroid/database/sqlite/SQLiteDatabase;

.field private e:Ljava/util/HashMap;

.field private f:Ljava/util/HashMap;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "da_destination_history"

    new-instance v1, Lcom/google/android/maps/driveabout/app/ag;

    invoke-direct {v1}, Lcom/google/android/maps/driveabout/app/ag;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/maps/driveabout/app/af;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/al;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/driveabout/app/al;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/af;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/af;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    return-void
.end method

.method private a(ILjava/util/List;)F
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ai;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/ai;->f:I

    if-ne v0, p1, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    int-to-float v0, v1

    const/high16 v1, 0x40e00000

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(Lo/u;Ljava/util/List;)F
    .locals 9

    const/high16 v0, 0x3f800000

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v2

    const v1, 0x3a83126f

    invoke-virtual {p1}, Lo/u;->a()I

    move-result v3

    int-to-double v3, v3

    const-wide v5, 0x3eb0c6f7a0b5ed8dL

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Lo/T;->a(D)D

    move-result-wide v3

    double-to-float v3, v3

    div-float v3, v1, v3

    const/high16 v1, 0x40800000

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v1, v4

    const v1, 0x40c90fdb

    mul-float/2addr v1, v4

    invoke-static {v1}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    div-float v5, v0, v1

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ai;

    iget-object v0, v0, Lcom/google/android/maps/driveabout/app/ai;->b:Lo/u;

    invoke-static {v0}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v0

    invoke-virtual {v2, v0}, Lo/T;->d(Lo/T;)F

    move-result v0

    mul-float/2addr v0, v3

    mul-float/2addr v0, v3

    neg-float v0, v0

    const/high16 v7, 0x40000000

    mul-float/2addr v7, v4

    div-float/2addr v0, v7

    float-to-double v7, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->exp(D)D

    move-result-wide v7

    double-to-float v0, v7

    add-float/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    mul-float v0, v1, v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3dcccccd

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method private a(LO/U;ILjava/util/List;Lo/u;II)Lcom/google/android/maps/driveabout/app/ak;
    .locals 2

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {p0, p6, p3}, Lcom/google/android/maps/driveabout/app/af;->a(ILjava/util/List;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, p5, p3}, Lcom/google/android/maps/driveabout/app/af;->b(ILjava/util/List;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, p4, p3}, Lcom/google/android/maps/driveabout/app/af;->a(Lo/u;Ljava/util/List;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, p4, p3}, Lcom/google/android/maps/driveabout/app/af;->b(Lo/u;Ljava/util/List;)F

    move-result v1

    add-float/2addr v0, v1

    new-instance v1, Lcom/google/android/maps/driveabout/app/ak;

    invoke-direct {v1, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/ak;-><init>(LO/U;FI)V

    return-object v1
.end method

.method private a(Lcom/google/android/maps/driveabout/app/aj;)Ljava/util/List;
    .locals 4

    iget-object v0, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/app/aj;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/app/aj;->a(Lcom/google/android/maps/driveabout/app/aj;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/google/android/maps/driveabout/app/aj;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/af;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private a(Lcom/google/android/maps/driveabout/app/ai;)V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/aj;

    iget-object v1, p1, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/aj;-><init>(LO/U;)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/af;->a(Lcom/google/android/maps/driveabout/app/aj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    return-void
.end method

.method private b(ILjava/util/List;)F
    .locals 5

    const/4 v2, 0x1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ai;

    iget v0, v0, Lcom/google/android/maps/driveabout/app/ai;->e:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v2, :cond_0

    const/16 v4, 0x17

    if-lt v0, v4, :cond_2

    :cond_0
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    int-to-float v0, v1

    const/high16 v1, 0x41000000

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private b(Lo/u;Ljava/util/List;)F
    .locals 7

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ai;

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/app/al;->a()J

    move-result-wide v3

    iget-wide v5, v0, Lcom/google/android/maps/driveabout/app/ai;->d:J

    sub-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget v3, v0, Lcom/google/android/maps/driveabout/app/ai;->a:I

    iget v4, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    const-wide/32 v3, 0xdbba00

    cmp-long v3, v1, v3

    if-gez v3, :cond_1

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f800000

    long-to-float v1, v1

    const v2, 0x4b5bba00

    div-float/2addr v1, v2

    sub-float v1, v4, v1

    mul-float/2addr v1, v3

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-static {v0}, Lo/T;->a(Lo/u;)Lo/T;

    move-result-object v0

    const-wide v3, 0x407f400000000000L

    invoke-virtual {v2}, Lo/T;->e()D

    move-result-wide v5

    mul-double/2addr v3, v5

    invoke-virtual {v2, v0}, Lo/T;->d(Lo/T;)F

    move-result v0

    float-to-double v5, v0

    mul-double v2, v3, v3

    cmpl-double v0, v5, v2

    if-lez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 14

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    move v0, v9

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/app/ah;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/af;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/af;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/ah;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/al;->a()J

    move-result-wide v0

    const-wide v2, 0x134fd9000L

    sub-long/2addr v0, v2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "destination_history"

    const-string v3, "time<=?"

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->f:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "destination_history"

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ai;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "time"

    const/16 v8, 0x1f4

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/ai;->a(ILandroid/database/Cursor;)Lcom/google/android/maps/driveabout/app/ai;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/af;->a(Lcom/google/android/maps/driveabout/app/ai;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :goto_2
    :try_start_2
    const-string v2, "DestinationHistory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception opening database: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/af;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->f:Ljava/util/HashMap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v10

    goto/16 :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v12

    move v0, v9

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    move-object v11, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v11

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized a(Lo/u;I)Ljava/util/List;
    .locals 9

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/af;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v7, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ai;

    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/ai;->c:LO/U;

    iget v2, v0, Lcom/google/android/maps/driveabout/app/ai;->a:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/al;->c()I

    move-result v5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/al;->b()I

    move-result v6

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/af;->a(LO/U;ILjava/util/List;Lo/u;II)Lcom/google/android/maps/driveabout/app/ak;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/ak;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ak;->b(Lcom/google/android/maps/driveabout/app/ak;)LO/U;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized a(LO/U;LO/U;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/af;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/app/ai;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/af;->g:I

    invoke-virtual {p1}, LO/U;->c()Lo/u;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/app/al;->a()J

    move-result-wide v4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/app/al;->c()I

    move-result v6

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/af;->c:Lcom/google/android/maps/driveabout/app/al;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/app/al;->b()I

    move-result v7

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/ai;-><init>(ILo/u;LO/U;JII)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/af;->a(Lcom/google/android/maps/driveabout/app/ai;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/af;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "destination_history"

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ai;->a()Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "Error inserting entry"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
