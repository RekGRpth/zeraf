.class public Lcom/google/android/maps/driveabout/app/eG;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/cQ;

.field private final b:Lcom/google/android/maps/driveabout/app/eQ;

.field private c:Lcom/google/android/maps/driveabout/app/eO;

.field private d:Lcom/google/android/maps/driveabout/app/eN;

.field private e:Z

.field private f:Ljava/io/File;

.field private g:Lcom/google/android/maps/driveabout/app/eP;

.field private final h:Landroid/content/DialogInterface$OnClickListener;

.field private final i:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/cQ;)V
    .locals 4

    invoke-static {}, Lcom/google/android/maps/driveabout/app/eG;->b()Lcom/google/android/maps/driveabout/app/eO;

    move-result-object v0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/eG;->c()Lcom/google/android/maps/driveabout/app/eQ;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/eG;-><init>(Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/eO;Lcom/google/android/maps/driveabout/app/eQ;Lcom/google/android/maps/driveabout/app/eN;)V

    const-wide/16 v0, 0x3a98

    const-wide/16 v2, 0x64

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/eG;->a(JJ)Lcom/google/android/maps/driveabout/app/eN;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->d:Lcom/google/android/maps/driveabout/app/eN;

    return-void
.end method

.method constructor <init>(Lcom/google/android/maps/driveabout/app/cQ;Lcom/google/android/maps/driveabout/app/eO;Lcom/google/android/maps/driveabout/app/eQ;Lcom/google/android/maps/driveabout/app/eN;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/eH;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eH;-><init>(Lcom/google/android/maps/driveabout/app/eG;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->h:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/google/android/maps/driveabout/app/eI;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eI;-><init>(Lcom/google/android/maps/driveabout/app/eG;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->i:Landroid/content/DialogInterface$OnCancelListener;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/eG;->a:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/eG;->b:Lcom/google/android/maps/driveabout/app/eQ;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/eG;->d:Lcom/google/android/maps/driveabout/app/eN;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eO;->b(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eO;->c(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eO;->a(I)V

    return-void
.end method

.method private a(JJ)Lcom/google/android/maps/driveabout/app/eN;
    .locals 6

    new-instance v0, Lcom/google/android/maps/driveabout/app/eL;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/app/eL;-><init>(Lcom/google/android/maps/driveabout/app/eG;JJ)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/eG;)Lcom/google/android/maps/driveabout/app/eN;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->d:Lcom/google/android/maps/driveabout/app/eN;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/eG;Lcom/google/android/maps/driveabout/app/eP;)Lcom/google/android/maps/driveabout/app/eP;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/eG;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/eG;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->f()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    return-void
.end method

.method private static b()Lcom/google/android/maps/driveabout/app/eO;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/eJ;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/eJ;-><init>()V

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/eG;)Lcom/google/android/maps/driveabout/app/eP;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    return-object v0
.end method

.method private static c()Lcom/google/android/maps/driveabout/app/eQ;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/eK;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/eK;-><init>()V

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->b:Lcom/google/android/maps/driveabout/app/eQ;

    const/16 v1, 0x1c

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eQ;->a(I)V

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/eG;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->a:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cQ;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/eP;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    :cond_0
    return-void
.end method

.method a(J)V
    .locals 3

    const/high16 v0, 0x3f800000

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/app/eO;->a()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x469c4000

    div-float/2addr v1, v2

    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    :goto_0
    long-to-int v1, p1

    rsub-int v1, v1, 0x3a98

    mul-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    const/high16 v2, 0x447a0000

    div-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/eG;->a:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setRecordingSample(IF)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/io/File;Lcom/google/android/maps/driveabout/app/eP;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/eG;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A recording was already started."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/app/eG;->e:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eO;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/eG;->f:Ljava/io/File;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->a:Lcom/google/android/maps/driveabout/app/cQ;

    const/16 v1, 0x97

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/eG;->h:Landroid/content/DialogInterface$OnClickListener;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/eG;->i:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/cQ;->a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->b:Lcom/google/android/maps/driveabout/app/eQ;

    const/16 v1, 0x18

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/eQ;->a(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->e()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->d:Lcom/google/android/maps/driveabout/app/eN;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eN;->a()V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not record to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->d()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/eO;->c()V

    iput-object v5, p0, Lcom/google/android/maps/driveabout/app/eG;->c:Lcom/google/android/maps/driveabout/app/eO;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->a:Lcom/google/android/maps/driveabout/app/cQ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    invoke-interface {v0, v4}, Lcom/google/android/maps/driveabout/app/eP;->a(Z)V

    iput-object v5, p0, Lcom/google/android/maps/driveabout/app/eG;->g:Lcom/google/android/maps/driveabout/app/eP;

    goto :goto_0
.end method
