.class public Lcom/google/android/maps/driveabout/app/bT;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile l:I


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/aA;

.field private final b:Lo/S;

.field private c:F

.field private d:J

.field private e:LaH/h;

.field private f:Lo/X;

.field private g:I

.field private h:F

.field private i:J

.field private j:D

.field private final k:Lcom/google/googlenav/common/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x7d0

    sput v0, Lcom/google/android/maps/driveabout/app/bT;->l:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/aA;Lcom/google/googlenav/common/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/bT;->k:Lcom/google/googlenav/common/a;

    return-void
.end method

.method private a(Lo/X;Lo/T;)I
    .locals 9

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    new-instance v5, Lo/T;

    invoke-direct {v5}, Lo/T;-><init>()V

    new-instance v6, Lo/T;

    invoke-direct {v6}, Lo/T;-><init>()V

    new-instance v7, Lo/T;

    invoke-direct {v7}, Lo/T;-><init>()V

    const v3, 0x7f7fffff

    const/4 v0, -0x1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p1, v1, v5}, Lo/X;->a(ILo/T;)V

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2, v6}, Lo/X;->a(ILo/T;)V

    invoke-static {v5, v6, p2, v7}, Lo/T;->b(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v2

    cmpg-float v8, v2, v3

    if-gez v8, :cond_1

    move v0, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    :cond_0
    return v0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public static a(I)V
    .locals 0

    sput p0, Lcom/google/android/maps/driveabout/app/bT;->l:I

    return-void
.end method

.method private static b(LaH/h;)Z
    .locals 2

    invoke-virtual {p0}, LaH/h;->j()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lo/X;IFIF)F
    .locals 7

    if-ne p2, p4, :cond_0

    invoke-virtual {p1, p2}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v0

    sub-float v1, p5, p3

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    if-le p2, p4, :cond_3

    const/4 v0, 0x1

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p1, p4}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    add-int/lit8 v2, p4, 0x1

    invoke-virtual {p1, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-virtual {v3, v2}, Lo/T;->c(Lo/T;)F

    move-result v3

    const/high16 v4, 0x3f800000

    sub-float/2addr v4, p5

    mul-float/2addr v3, v4

    add-float/2addr v3, v1

    add-int/lit8 v1, p4, 0x1

    move v4, v3

    :goto_2
    if-ge v1, p2, :cond_1

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    add-float/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    move v4, v2

    move-object v2, v3

    goto :goto_2

    :cond_1
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {v2, v1}, Lo/T;->c(Lo/T;)F

    move-result v1

    mul-float/2addr v1, p3

    add-float/2addr v1, v4

    if-eqz v0, :cond_2

    neg-float v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v5, p5

    move p5, p3

    move p3, v5

    move v6, p4

    move p4, p2

    move p2, v6

    goto :goto_1
.end method

.method public declared-synchronized a()I
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bT;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/bT;->d:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :cond_0
    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bT;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/app/bT;->i:J

    sub-long v2, v0, v2

    long-to-int v2, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/bT;->i:J

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/bT;->b(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaH/h;)V
    .locals 5

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->e:LaH/h;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/bT;->e:LaH/h;

    invoke-virtual {v2}, LaH/h;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    :cond_0
    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v1

    sget v3, Lcom/google/android/maps/driveabout/app/bT;->l:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/maps/driveabout/app/bT;->d:J

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/bT;->a(LaH/h;I)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bT;->e:LaH/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(LaH/h;I)V
    .locals 19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual/range {p1 .. p1}, LaH/h;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->c(I)V

    invoke-virtual/range {p1 .. p1}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Lo/T;->a(D)D

    move-result-wide v12

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    invoke-virtual {v2}, LR/m;->f()I

    move-result v2

    int-to-double v7, v2

    invoke-virtual/range {p1 .. p1}, LaH/h;->n()LO/H;

    move-result-object v2

    check-cast v2, LO/D;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, LaH/h;->o()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, LO/D;->d()D

    move-result-wide v9

    div-double/2addr v9, v12

    cmpg-double v5, v9, v7

    if-gez v5, :cond_2

    invoke-virtual {v2}, LO/D;->a()LO/z;

    move-result-object v3

    invoke-virtual {v3}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v2}, LO/D;->e()I

    move-result v6

    invoke-virtual {v2}, LO/D;->b()Lo/T;

    move-result-object v2

    move-object v11, v2

    :goto_0
    if-eqz v3, :cond_8

    invoke-static/range {p1 .. p1}, Lcom/google/android/maps/driveabout/app/bT;->b(LaH/h;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v3, v6}, Lo/X;->a(I)Lo/T;

    move-result-object v14

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v3, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v15

    invoke-static {v14, v15}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v16

    const-wide/16 v4, 0x0

    invoke-virtual/range {p1 .. p1}, LaH/h;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual/range {p1 .. p1}, LaH/h;->getSpeed()F

    move-result v2

    float-to-double v4, v2

    mul-double/2addr v4, v12

    const-wide v7, 0x408f400000000000L

    div-double/2addr v4, v7

    invoke-virtual/range {p1 .. p1}, LaH/h;->getBearing()F

    move-result v2

    sub-float v2, v16, v2

    float-to-double v7, v2

    const-wide v9, 0x3f91df46a2529d39L

    mul-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double/2addr v4, v7

    move-wide v8, v4

    :goto_1
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    if-ne v3, v2, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    invoke-static {v14, v15, v11}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v7

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/maps/driveabout/app/bT;->a(Lo/X;IFIF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    invoke-virtual/range {p1 .. p1}, LaH/h;->getAccuracy()F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v17, v0

    mul-double v12, v12, v17

    cmpg-double v4, v4, v12

    if-gez v4, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    invoke-virtual {v3, v4}, Lo/X;->d(I)F

    move-result v4

    sub-float v4, v4, v16

    float-to-double v4, v4

    const-wide v12, 0x3f91df46a2529d39L

    mul-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v4, v8

    if-lez p2, :cond_0

    move/from16 v0, p2

    int-to-float v7, v0

    div-float/2addr v2, v7

    float-to-double v7, v2

    add-double/2addr v4, v7

    :cond_0
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    const-wide v12, 0x3f847ae147ae147bL

    cmpl-double v2, v7, v12

    if-ltz v2, :cond_9

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/maps/driveabout/app/bT;->c:F

    move v2, v10

    :goto_2
    if-eqz v2, :cond_1

    invoke-static {v14, v15, v11}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    const-wide v4, 0x3f847ae147ae147bL

    cmpg-double v2, v2, v4

    if-gez v2, :cond_6

    invoke-virtual/range {p1 .. p1}, LaH/h;->getBearing()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/maps/driveabout/app/bT;->i:J

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/maps/driveabout/app/bT;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/maps/driveabout/app/bT;->i:J

    invoke-virtual/range {p1 .. p1}, LaH/h;->e()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/bT;->b(I)I

    :cond_1
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    invoke-virtual {v2, v3}, Lo/S;->a(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual/range {p1 .. p1}, LaH/h;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lo/S;->a(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    return-void

    :cond_2
    invoke-virtual/range {p1 .. p1}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual/range {p1 .. p1}, LaH/h;->l()Lo/af;

    move-result-object v2

    invoke-virtual {v2}, Lo/af;->b()Lo/X;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, LaH/h;->m()Lo/T;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/google/android/maps/driveabout/app/bT;->a(Lo/X;Lo/T;)I

    move-result v6

    invoke-virtual {v4, v6}, Lo/X;->d(I)F

    move-result v2

    invoke-virtual/range {p1 .. p1}, LaH/h;->getBearing()F

    move-result v5

    sub-float v2, v5, v2

    const/high16 v5, -0x3ccc0000

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_3

    const/high16 v5, 0x43b40000

    add-float/2addr v2, v5

    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v5, 0x42b40000

    cmpl-float v2, v2, v5

    if-lez v2, :cond_b

    invoke-virtual {v4}, Lo/X;->g()Lo/X;

    move-result-object v2

    invoke-virtual {v2}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    sub-int v6, v4, v6

    move-object v11, v3

    move-object v3, v2

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x1

    move-wide v4, v8

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x1

    move-wide v4, v8

    goto/16 :goto_2

    :cond_6
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/maps/driveabout/app/bT;->c:F

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v2, v11}, Lo/S;->a(Lo/T;)V

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    invoke-virtual/range {p1 .. p1}, LaH/h;->getBearing()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual/range {p1 .. p1}, LaH/h;->getLatitude()D

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, LaH/h;->getLongitude()D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lo/T;->a(DD)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/S;->a(Lo/T;)V

    goto/16 :goto_4

    :cond_9
    move v2, v10

    goto/16 :goto_2

    :cond_a
    move-wide v8, v4

    goto/16 :goto_1

    :cond_b
    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_0

    :cond_c
    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v0, p1}, Lo/S;->a(Z)V

    return-void
.end method

.method b(I)I
    .locals 12

    const/high16 v11, 0x3f800000

    const/4 v3, 0x0

    if-ltz p1, :cond_0

    sget v0, Lcom/google/android/maps/driveabout/app/bT;->l:I

    if-le p1, v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    iget v2, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    float-to-double v4, v2

    iget-wide v6, p0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    int-to-double v8, p1

    mul-double/2addr v6, v8

    invoke-virtual {v1, v0}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v8, v2

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v2, v4

    iput v2, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    move-object v2, v1

    :goto_1
    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    cmpl-float v1, v1, v11

    if-lez v1, :cond_4

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    invoke-virtual {v4}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v1, v0}, Lo/S;->a(Lo/T;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-static {v2, v0}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v0

    invoke-virtual {v1, v0}, Lo/S;->a(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    sub-float/2addr v1, v11

    iput v1, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bT;->g:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-static {v0, v1}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v4

    iget v5, p0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    sub-float/2addr v5, v4

    float-to-double v5, v5

    const-wide v7, 0x3f91df46a2529d39L

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    iget v7, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    float-to-double v7, v7

    invoke-virtual {v2, v0}, Lo/T;->c(Lo/T;)F

    move-result v2

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v9

    div-float/2addr v2, v9

    float-to-double v9, v2

    mul-double/2addr v9, v5

    mul-double/2addr v7, v9

    double-to-float v2, v7

    iput v2, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    iget-wide v7, p0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    mul-double/2addr v5, v7

    iput-wide v5, p0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    iget-wide v5, p0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    const-wide v7, 0x3f847ae147ae147bL

    cmpl-double v2, v5, v7

    if-ltz v2, :cond_3

    iput v4, p0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    :cond_3
    move-object v2, v0

    move-object v0, v1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    iget v4, p0, Lcom/google/android/maps/driveabout/app/bT;->h:F

    invoke-virtual {v2, v0, v4}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    invoke-virtual {v1, v0}, Lo/S;->a(Lo/T;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/bT;->c:F

    invoke-virtual {v0, v1}, Lo/S;->a(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bT;->b:Lo/S;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/bT;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v3, v0

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2
.end method

.method public b()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->f:Lo/X;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/maps/driveabout/app/bT;->j:D

    const-wide v2, 0x3f847ae147ae147bL

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lo/S;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bT;->a:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aA;->n()Lo/S;

    move-result-object v0

    return-object v0
.end method
