.class public Lcom/google/android/maps/driveabout/app/TopBarView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

.field private b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/ViewGroup;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(LO/N;)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d000b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "<image>"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v2, "<image>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, LO/m;->a(Landroid/content/Context;LO/N;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0038

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    new-instance v4, Lcom/google/android/maps/driveabout/app/dj;

    invoke-direct {v4, v2, v3, v3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;FF)V

    const-string v2, "<image>"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    const/16 v3, 0x21

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040054

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f100154

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    const v0, 0x7f100155

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    const v0, 0x7f100156

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    const v0, 0x7f100157

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    const v0, 0x7f100130

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    const v0, 0x7f100131

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->f:Landroid/widget/TextView;

    const v0, 0x7f100158

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    const v0, 0x7f100159

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    const v0, 0x7f10015a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    const v0, 0x7f10015b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->m()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 5

    const v4, 0x7f050011

    const v3, 0x7f050010

    const/4 v2, 0x0

    const-string v0, "__step_description"

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "__route_overview"

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;IZ)V

    goto :goto_0

    :cond_2
    const-string v0, "__reroute_prompt_overview"

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->e:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;IZ)V

    goto :goto_0

    :cond_3
    const-string v0, "__destination_buttons"

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->d:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;IZ)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    if-eq v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;IZ)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method private a(I)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-ne p1, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0017

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0018

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method static a(Landroid/content/Context;LO/N;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LO/N;->f()I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_2

    invoke-virtual {p1}, LO/N;->e()I

    move-result v1

    const/16 v2, 0x1f4

    if-gt v1, v2, :cond_0

    :cond_2
    invoke-virtual {p1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x11

    if-eq v1, v2, :cond_0

    invoke-static {p1}, LO/m;->b(LO/N;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LO/N;->j()LO/N;

    move-result-object v0

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LO/N;->b()I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-virtual {v1}, LO/N;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->k()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Landroid/content/Context;LO/N;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v1

    const/16 v2, 0x1324

    if-gt v1, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(LO/N;)V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->j()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    goto :goto_1
.end method

.method private j()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    const v1, 0x7f050009

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    const v1, 0x7f050009

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->b(Landroid/view/View;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    const v1, 0x7f05000c

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->o:Z

    :cond_0
    return-void
.end method

.method private m()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%2$s"

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d00a1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%1$s"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/dj;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1080038

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x3fa66666

    invoke-direct {v0, v3, v4}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    add-int/lit8 v3, v1, 0x4

    const/16 v4, 0x21

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    const v1, 0x7f05000c

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/view/View;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->n:Z

    :cond_0
    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setRoutes(LO/z;[LO/z;)V

    const-string v0, "__route_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [LO/z;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setRoutes(LO/z;[LO/z;)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->i()I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->i()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(III)V

    :goto_1
    const-string v0, "__route_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->i()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v2

    invoke-virtual {v2}, LO/z;->p()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v3

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->a(III)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setStep(LO/N;)V

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v1

    if-ne v1, p2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->h()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setDistanceMeters(F)V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v1}, LO/z;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setDistanceUnits(I)V

    :goto_0
    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setUseLongDistanceStepFormat(Z)V

    if-eqz p4, :cond_1

    if-nez p3, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    :goto_1
    const-string v0, "__step_description"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setBackgroundType(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setShowDistance(Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setUseLongDistanceStepFormat(Z)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/TopBarView;->l()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "__reroute_prompt_overview"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->setClickable(Z)V

    const-string v0, "__destination_buttons"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p0, p2, p3}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/android/maps/driveabout/app/RouteSelectorView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->b:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    return-object v0
.end method

.method public e()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->g:Landroid/widget/Button;

    return-object v0
.end method

.method public f()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->i:Landroid/widget/Button;

    return-object v0
.end method

.method public g()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->h:Landroid/widget/Button;

    return-object v0
.end method

.method public h()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->j:Landroid/widget/Button;

    return-object v0
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setButtonVisibility(ZZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->a:Lcom/google/android/maps/driveabout/app/StepDescriptionView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setButtonVisibility(ZZZ)V

    return-void
.end method

.method public setConnectedViews(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 2

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/TopBarView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    return-void
.end method
