.class public Lcom/google/android/maps/driveabout/app/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/aR;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/l;

.field private b:Ljava/lang/String;

.field private c:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    new-instance v0, Lcom/google/android/maps/driveabout/app/l;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a:Lcom/google/android/maps/driveabout/app/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/SettingsActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->c:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/SettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->b:Ljava/lang/String;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/aP;

    invoke-direct {v0, p1, p0}, Lcom/google/googlenav/aP;-><init>(Ljava/lang/String;Lcom/google/googlenav/aR;)V

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v1

    invoke-interface {v1, v0}, Law/p;->c(Law/g;)V

    const-string v1, ""

    const/16 v2, 0x258

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/android/maps/driveabout/app/dp;

    invoke-direct {v5, p0, v0}, Lcom/google/android/maps/driveabout/app/dp;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;Lcom/google/googlenav/aP;)V

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->c:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/SettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/SettingsActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;[B)V
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/app/dq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/maps/driveabout/app/dq;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;Ljava/lang/String;[BLcom/google/android/maps/driveabout/app/dl;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/l;->a(Landroid/app/Activity;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/an;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/an;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v2, "DriveAbout"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const/high16 v0, 0x7f060000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->addPreferencesFromResource(I)V

    const-string v0, "InfoCategory"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string v2, "AboutDriveabout"

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v0, Lcom/google/android/maps/driveabout/app/dl;

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/dl;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;Lcom/google/android/maps/driveabout/app/an;)V

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :goto_1
    const-string v0, "TermsAndConditions"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/dm;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/dm;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "Privacy"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/dn;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/dn;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "Notices"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v2, Lcom/google/android/maps/driveabout/app/do;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/do;-><init>(Lcom/google/android/maps/driveabout/app/SettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "UnitsSetting"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    const-string v0, "DuplicatedGMMSettings"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    const-string v0, "OneButtonRmi"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RmiPreference;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_5
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/RmiPreference;->setEnabled(Z)V

    const v1, 0x7f0d0062

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/RmiPreference;->setSummary(I)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/RmiPreference;->setEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/RmiPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/RmiPreference;->a(Lcom/google/android/maps/driveabout/app/an;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->a()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->c()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/SettingsActivity;->a:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->b()V

    return-void
.end method
