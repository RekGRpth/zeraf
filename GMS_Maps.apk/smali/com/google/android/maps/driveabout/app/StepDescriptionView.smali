.class public Lcom/google/android/maps/driveabout/app/StepDescriptionView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lv/c;


# instance fields
.field protected a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

.field protected b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/TextView;

.field private e:LO/N;

.field private f:LO/N;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/view/View;

.field private n:F

.field private o:I

.field private p:Z

.field private q:I

.field private r:Z

.field private s:Lcom/google/android/maps/driveabout/app/dz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->p:Z

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->p:Z

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f10014f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const v0, 0x7f100150

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const v0, 0x7f10014c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    const v0, 0x7f10014d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    const v0, 0x7f10014e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    const v0, 0x7f100153

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    :cond_0
    const v0, 0x7f1000e5

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->c:Landroid/widget/ImageView;

    const v0, 0x7f100151

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    const v0, 0x7f100152

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    const v0, 0x7f100125

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->d:Landroid/widget/TextView;

    const v0, 0x7f100142

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->m:Landroid/view/View;

    invoke-static {}, Lcom/google/android/maps/driveabout/app/dz;->a()Lcom/google/android/maps/driveabout/app/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->s:Lcom/google/android/maps/driveabout/app/dz;

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/SqueezedLabelView;Ljava/util/Collection;IFZZZI)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v4, Landroid/text/TextPaint;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    const/high16 v0, 0x3f800000

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    invoke-virtual {v4, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->b()I

    move-result v3

    if-eq v3, v1, :cond_0

    if-eqz p7, :cond_1

    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    const v5, 0x7f0d0007

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->s:Lcom/google/android/maps/driveabout/app/dz;

    move-object v1, p2

    move v2, p3

    move v3, p8

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/util/Collection;IILandroid/text/TextPaint;ILv/c;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    invoke-virtual {p1, p4}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setDesiredTextSize(F)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->j()LO/N;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->j()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->e()I

    move-result v0

    int-to-float v0, v0

    const/high16 v5, 0x42480000

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v3}, LO/N;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    if-eqz p7, :cond_5

    const v5, 0x7f0d0008

    goto :goto_1

    :cond_5
    if-eqz p6, :cond_6

    const v5, 0x7f0d0009

    goto :goto_1

    :cond_6
    move v5, v2

    goto :goto_1
.end method

.method private f()V
    .locals 13

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->y()LO/P;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->y()LO/P;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->p()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->q()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->s()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dz;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_3

    move-object v9, v0

    move-object v2, v11

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    move v12, v0

    :goto_1
    if-eqz v12, :cond_7

    const/4 v3, 0x1

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000

    mul-float/2addr v0, v1

    float-to-int v8, v0

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-interface {v9}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    if-eqz v12, :cond_1

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Lcom/google/android/maps/driveabout/app/SqueezedLabelView;Ljava/util/Collection;IFZZZI)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->a()F

    move-result v4

    if-ne v9, v10, :cond_8

    const/4 v5, 0x1

    :goto_3
    if-ne v2, v11, :cond_9

    const/4 v7, 0x1

    :goto_4
    move-object v0, p0

    move-object v2, v9

    move v6, v12

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Lcom/google/android/maps/driveabout/app/SqueezedLabelView;Ljava/util/Collection;IFZZZI)V

    if-eqz v12, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    :cond_2
    :goto_5
    return-void

    :cond_3
    move-object v9, v10

    move-object v2, v11

    goto/16 :goto_0

    :cond_4
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_5

    move-object v9, v10

    move-object v2, v0

    goto/16 :goto_0

    :cond_5
    move-object v9, v0

    move-object v2, v10

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    move v12, v0

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x2

    goto/16 :goto_2

    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    :cond_9
    const/4 v7, 0x0

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_b

    const v0, 0x7f0b001f

    :goto_6
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    if-ne v2, v10, :cond_c

    const/4 v5, 0x1

    :goto_7
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Lcom/google/android/maps/driveabout/app/SqueezedLabelView;Ljava/util/Collection;IFZZZI)V

    if-eqz v12, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    goto :goto_5

    :cond_b
    const v0, 0x7f0b0021

    goto :goto_6

    :cond_c
    const/4 v5, 0x0

    goto :goto_7

    :cond_d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v1}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    if-eqz v12, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    goto/16 :goto_5
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->r:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05000b

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method

.method public a(Lv/a;)V
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/app/du;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/du;-><init>(Lcom/google/android/maps/driveabout/app/StepDescriptionView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected a(Z)V
    .locals 6

    const/16 v3, 0x8

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->f:LO/N;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->c:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-static {v1, v2}, LO/m;->a(Landroid/content/Context;LO/N;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->r:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->k()LO/N;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v0}, LO/N;->k()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->x()LO/P;

    move-result-object v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->s:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    invoke-virtual {v2}, LO/N;->k()LO/N;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/N;Lv/c;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->s:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    iget v3, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->n:F

    float-to-int v3, v3

    iget v4, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->o:I

    invoke-virtual {v1, v2, v3, v4, p0}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/N;IILv/c;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setGravity(I)V

    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->f:LO/N;

    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->p:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->n:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->s:Lcom/google/android/maps/driveabout/app/dz;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->n:F

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->o:I

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/maps/driveabout/app/dz;->a(IIZ)Landroid/text/Spannable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->d:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    if-nez p1, :cond_4

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->f()V

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->b:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->c:Landroid/widget/ImageView;

    const v1, 0x7f020168

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->m:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f05000a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050011

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/bm;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/bm;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method

.method protected e()I
    .locals 1

    const v0, 0x7f040053

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    if-eq p1, p3, :cond_0

    new-instance v0, Lcom/google/android/maps/driveabout/app/dv;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dv;-><init>(Lcom/google/android/maps/driveabout/app/StepDescriptionView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public setBackgroundType(I)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->q:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->q:I

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    const v1, 0x7f020450

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    const v1, 0x7f020425

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->i:Landroid/view/View;

    const v1, 0x7f02044d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    const v1, 0x7f020422

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setButtonVisibility(ZZZ)V
    .locals 8

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v6, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    move v6, v1

    :goto_1
    if-ne p1, v0, :cond_3

    if-ne p2, v6, :cond_3

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v6, v2

    goto :goto_1

    :cond_3
    iget-object v7, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    if-eqz p2, :cond_5

    move v1, v2

    :goto_3
    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    if-eqz p1, :cond_6

    :goto_4
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    if-eqz p3, :cond_0

    if-eq v0, p1, :cond_4

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_7

    move v1, v4

    :goto_5
    if-eqz p1, :cond_8

    move v0, v4

    :goto_6
    invoke-direct {v2, v1, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz p1, :cond_9

    const-wide/16 v0, 0x0

    :goto_7
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    if-eq v6, p2, :cond_0

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    if-eqz v6, :cond_a

    move v0, v4

    :goto_8
    if-eqz p2, :cond_b

    :goto_9
    invoke-direct {v2, v0, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz p2, :cond_c

    const-wide/16 v0, 0x0

    :goto_a
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_5

    :cond_8
    move v0, v5

    goto :goto_6

    :cond_9
    const-wide/16 v0, 0x1f4

    goto :goto_7

    :cond_a
    move v0, v5

    goto :goto_8

    :cond_b
    move v4, v5

    goto :goto_9

    :cond_c
    const-wide/16 v0, 0x1f4

    goto :goto_a
.end method

.method public setDistanceMeters(F)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->n:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->n:F

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Z)V

    :cond_0
    return-void
.end method

.method public setDistanceUnits(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->o:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->o:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Z)V

    :cond_0
    return-void
.end method

.method public setShowDistance(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->p:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->p:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Z)V

    :cond_0
    return-void
.end method

.method public setStep(LO/N;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->e:LO/N;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Z)V

    :cond_0
    return-void
.end method

.method public setUseLongDistanceStepFormat(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->r:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->r:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/StepDescriptionView;->a(Z)V

    :cond_0
    return-void
.end method
