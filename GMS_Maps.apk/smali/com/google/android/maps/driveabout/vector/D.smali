.class public abstract Lcom/google/android/maps/driveabout/vector/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# instance fields
.field private a:Lcom/google/android/maps/driveabout/vector/F;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 5

    const v4, 0xff00

    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v4

    shr-int/lit8 v1, p1, 0x8

    and-int/2addr v1, v4

    and-int v2, p1, v4

    shl-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v4

    invoke-interface {p0, v1, v2, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)V

    return-object v0
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v0
.end method

.method protected varargs a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;
    .locals 1

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aH;-><init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(LD/a;)V
    .locals 0

    return-void
.end method

.method public abstract a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/F;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(FFLC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a_(FFLo/T;LC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(LD/a;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;)V

    return-void
.end method

.method public b(FFLC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(FFLo/T;LC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(LC/a;LD/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/List;)Z
    .locals 1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/D;->v_()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/D;->a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(LD/a;)V
    .locals 0

    return-void
.end method

.method public c(FFLo/T;LC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d(FFLo/T;LC/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/F;

    invoke-interface {v0, p0}, Lcom/google/android/maps/driveabout/vector/F;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    return-void
.end method

.method public i_()V
    .locals 0

    return-void
.end method

.method public j_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public k_()V
    .locals 0

    return-void
.end method

.method public l_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract p()Lcom/google/android/maps/driveabout/vector/E;
.end method

.method protected v_()Lcom/google/android/maps/driveabout/vector/aI;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
