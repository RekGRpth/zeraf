.class Lcom/google/android/maps/driveabout/vector/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/intersectionexplorer/c;

.field b:Lcom/google/googlenav/intersectionexplorer/c;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/maps/driveabout/vector/aw;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/maps/driveabout/vector/aw;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    if-eq v1, v2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v2, p1, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    if-ne v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aw;->a:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aw;->b:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
