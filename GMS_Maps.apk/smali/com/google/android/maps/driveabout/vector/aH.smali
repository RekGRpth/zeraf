.class public Lcom/google/android/maps/driveabout/vector/aH;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/D;

.field private final b:Lcom/google/android/maps/driveabout/vector/aI;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Map;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->d:Ljava/util/Map;

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->e:Z

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->f:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aH;->a:Lcom/google/android/maps/driveabout/vector/D;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/aJ;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/o;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/aJ;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/o;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aH;->d:Ljava/util/Map;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public varargs constructor <init>(Lcom/google/android/maps/driveabout/vector/D;Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->d:Ljava/util/Map;

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->e:Z

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->f:Z

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aH;->a:Lcom/google/android/maps/driveabout/vector/D;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    array-length v2, p3

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p3, v1

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/aJ;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/o;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/aH;)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aI;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/D;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/D;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aJ;

    iget-object v1, p1, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/vector/aJ;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public a()Lcom/google/android/maps/driveabout/vector/D;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->a:Lcom/google/android/maps/driveabout/vector/D;

    return-object v0
.end method

.method public a(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aJ;

    return-object v0
.end method

.method a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aH;->e:Z

    return-void
.end method

.method public b()Lcom/google/android/maps/driveabout/vector/aI;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    return-object v0
.end method

.method public b(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aJ;

    return-object v0
.end method

.method b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aH;->f:Z

    return-void
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->a:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->c:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->h:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->f:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aH;->a(Lcom/google/android/maps/driveabout/vector/aH;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->g:Lcom/google/android/maps/driveabout/vector/aI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->b:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->e:Z

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aH;->f:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "overlay"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "order"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->b:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isFirstPassForOverlay"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->e:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "isLastPassForOverlay"

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->f:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Z)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "overlayRenderTweaks"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    const-string v1, "featureRenderTweaks"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aH;->d:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
