.class Lcom/google/android/maps/driveabout/vector/bn;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private b:F

.field private c:F

.field private d:J


# direct methods
.method public constructor <init>(LC/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    return-void
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .locals 7

    const v6, 0x3e19999a

    const v5, 0x3bc49ba6

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/bn;->d:J

    sub-long v2, v0, v2

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->d:J

    neg-long v0, v2

    long-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x3f800000

    sub-float/2addr v1, v0

    div-float/2addr v1, v5

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    mul-float/2addr v1, v3

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    mul-float/2addr v3, v0

    iput v3, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    mul-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v6

    if-gez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v6

    if-gez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->a:LC/b;

    invoke-static {v0, p1, v2, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->a:LC/b;

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_1

    iget-object p0, p0, Lcom/google/android/maps/driveabout/vector/bn;->a:LC/b;

    :cond_1
    return-object p0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method declared-synchronized a(FF)V
    .locals 2

    const v1, -0x457ced91

    monitor-enter p0

    mul-float v0, p1, v1

    :try_start_0
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    mul-float v0, p2, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bn;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
