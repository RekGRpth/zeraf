.class Lcom/google/android/maps/driveabout/vector/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lg/c;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lg/c;

.field private final c:I

.field private d:Lr/n;

.field private e:Ln/e;

.field private final f:Ln/s;


# direct methods
.method public constructor <init>(Lg/c;ILandroid/content/Context;Ln/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/au;->a:Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/au;->b:Lg/c;

    iput p2, p0, Lcom/google/android/maps/driveabout/vector/au;->c:I

    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/s;

    return-void
.end method


# virtual methods
.method public a(LA/c;IZLA/b;)Lg/a;
    .locals 6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->b:Lg/c;

    invoke-interface {v0, p1, p2, p3, p4}, Lg/c;->a(LA/c;IZLA/b;)Lg/a;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    if-nez v0, :cond_0

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/s;

    invoke-virtual {v0}, Ln/s;->j()Ln/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    :cond_1
    new-instance v0, Lk/a;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/au;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/s;

    invoke-direct/range {v0 .. v5}, Lk/a;-><init>(Lg/a;Ln/e;Lr/n;ILn/s;)V

    return-object v0
.end method

.method public a(LA/c;ZLA/b;)Lk/f;
    .locals 1

    new-instance v0, Lk/f;

    invoke-direct {v0, p1, p3}, Lk/f;-><init>(LA/c;LA/b;)V

    return-object v0
.end method
