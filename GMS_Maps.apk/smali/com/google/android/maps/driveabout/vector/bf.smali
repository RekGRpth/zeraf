.class public Lcom/google/android/maps/driveabout/vector/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[LA/c;

.field public static final b:[LA/c;

.field public static volatile c:I

.field private static d:Z

.field private static e:Z

.field private static volatile f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [LA/c;

    sget-object v1, LA/c;->b:LA/c;

    aput-object v1, v0, v3

    sget-object v1, LA/c;->d:LA/c;

    aput-object v1, v0, v2

    sget-object v1, LA/c;->g:LA/c;

    aput-object v1, v0, v4

    sget-object v1, LA/c;->h:LA/c;

    aput-object v1, v0, v5

    sget-object v1, LA/c;->i:LA/c;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    const/16 v0, 0xc

    new-array v0, v0, [LA/c;

    sget-object v1, LA/c;->a:LA/c;

    aput-object v1, v0, v3

    sget-object v1, LA/c;->c:LA/c;

    aput-object v1, v0, v2

    sget-object v1, LA/c;->d:LA/c;

    aput-object v1, v0, v4

    sget-object v1, LA/c;->f:LA/c;

    aput-object v1, v0, v5

    sget-object v1, LA/c;->e:LA/c;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, LA/c;->j:LA/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LA/c;->l:LA/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LA/c;->k:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LA/c;->m:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LA/c;->n:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LA/c;->o:LA/c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LA/c;->p:LA/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    sput-boolean v3, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    const/16 v0, 0xa

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    const/4 v0, -0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Law/h;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    new-instance v3, Law/j;

    invoke-direct {v3}, Law/j;-><init>()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->Q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->a(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->F()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->b(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-virtual {v3, p3}, Law/j;->c(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/Config;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->d(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->a(Z)Law/j;

    move-result-object v3

    invoke-virtual {v3, v1}, Law/j;->b(Z)Law/j;

    move-result-object v3

    invoke-static {p0}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Law/j;->e(Ljava/lang/String;)Law/j;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/res/Resources;)Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->c(Z)Law/j;

    move-result-object v3

    invoke-virtual {v0, p0}, Lcom/google/googlenav/capabilities/a;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Law/j;->e(Z)Law/j;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/capabilities/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Law/j;->d(Z)Law/j;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v3}, Law/j;->a(I)Law/j;

    move-result-object v3

    const-string v0, "DriveAbout"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v0, "GMM"

    :goto_1
    invoke-virtual {v3, v0}, Law/j;->f(Ljava/lang/String;)Law/j;

    invoke-virtual {v3, v1}, Law/j;->b(I)Law/j;

    invoke-virtual {v3, v1}, Law/j;->f(Z)Law/j;

    invoke-virtual {v3}, Law/j;->a()Law/h;

    move-result-object v0

    const-string v1, "GMM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v3, v1}, Law/j;->b(I)Law/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Law/j;->f(Z)Law/j;

    move-result-object v1

    const-string v2, "DriveAbout"

    invoke-virtual {v1, v2}, Law/j;->f(Ljava/lang/String;)Law/j;

    move-result-object v1

    invoke-virtual {v1}, Law/j;->b()Law/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Law/h;->a(Law/p;)V

    :cond_1
    new-instance v1, Lcom/google/android/maps/driveabout/vector/bh;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/bh;-><init>(Lcom/google/android/maps/driveabout/vector/bg;)V

    invoke-virtual {v0, v1}, Law/h;->a(Law/q;)V

    const-string v1, "1"

    const-string v1, "2"

    const-string v1, "3"

    const-string v1, "3"

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move-object v0, p2

    goto :goto_1
.end method

.method public static declared-synchronized a(LA/c;Landroid/content/Context;Landroid/content/res/Resources;)Lr/z;
    .locals 7

    const-class v6, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v6

    :try_start_0
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorGlobalState.initialize() must be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [LA/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p1}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-static {p0}, Lr/C;->c(LA/c;)Lr/z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit v6

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V
    .locals 11

    const-class v10, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v10

    :try_start_0
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit v10

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    invoke-static {}, LJ/a;->a()V

    invoke-static/range {p5 .. p5}, Lbm/m;->a(Lbm/q;)V

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bf;->b(Landroid/content/Context;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, LJ/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p3, v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Law/h;

    move-result-object v1

    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v2, v3}, Law/h;->a(J)V

    :cond_1
    if-eqz v0, :cond_2

    :cond_2
    invoke-static {v1}, Lcom/google/googlenav/clientparam/f;->a(Law/h;)V

    new-instance v0, LR/l;

    invoke-direct {v0, v1}, LR/l;-><init>(Law/h;)V

    invoke-virtual {v1, v0}, Law/h;->a(Law/q;)V

    :cond_3
    invoke-virtual {v1}, Law/h;->v()V

    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-static {v1, v3}, Lv/d;->a(Law/h;Ljava/io/File;)Lv/d;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    const/4 v0, -0x1

    if-eq p4, v0, :cond_4

    :try_start_2
    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lr/y;->a(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    :goto_1
    :try_start_3
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "GMM"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v0, :cond_5

    if-eqz v4, :cond_9

    :cond_5
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    move-object v4, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    sget-object v4, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v5

    move-object v6, v2

    move-object v7, v3

    move-object v8, p0

    move-object v9, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V

    :cond_6
    :goto_2
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    invoke-static {v1, v3, v2, v0}, Lr/n;->a(Law/h;Ljava/io/File;Ljava/util/Locale;Lcom/google/googlenav/common/a;)Lr/n;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lr/n;->c()V

    invoke-static {v0}, Ln/s;->a(Lr/n;)Ln/s;

    :cond_7
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Ll/a;

    invoke-direct {v0, p0}, Ll/a;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ll/f;->a(Ll/f;)V

    :cond_8
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->e:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z

    const-string v0, "VectorGlobalState.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v4, "Could not load encryption key"

    invoke-static {v4, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_9
    if-eqz p2, :cond_6

    move-object v0, p2

    move-object v4, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public static declared-synchronized a(Ljava/util/Locale;)V
    .locals 4

    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0, p0}, Lr/z;->a(Ljava/util/Locale;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    monitor-exit v1

    return-void
.end method

.method public static a(Z)V
    .locals 1

    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    invoke-virtual {v0, p0}, Lr/I;->a(Z)V

    :cond_0
    return-void
.end method

.method private static declared-synchronized a([LA/c;Law/p;Ljava/util/Locale;Ljava/io/File;Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 11

    const-class v9, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v9

    :try_start_0
    const-string v0, "GMM"

    invoke-interface {p1}, Law/p;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x1

    sput v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    :cond_0
    array-length v10, p0

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_3

    aget-object v0, p0, v8

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_2
    move-object v1, p1

    move-object v2, p4

    move-object/from16 v3, p5

    move-object v4, p2

    move-object v5, p3

    move v7, v6

    invoke-virtual/range {v0 .. v7}, LA/c;->a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lr/z;->g()V

    invoke-static {v0, v1}, Lr/C;->a(LA/c;Lr/z;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    :cond_3
    monitor-exit v9

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->e:Z

    return v0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 7

    const-wide/high16 v5, 0x3fd0000000000000L

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-gtz v1, :cond_0

    iget v1, v2, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v3, v1

    cmpl-double v1, v3, v5

    if-lez v1, :cond_1

    :cond_0
    move v1, v0

    :goto_0
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    div-float v1, v3, v1

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float v0, v2, v0

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x41c80000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget v1, v2, Landroid/util/DisplayMetrics;->xdpi:F

    iget v0, v2, Landroid/util/DisplayMetrics;->ydpi:F

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, LJ/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-static {p0}, LJ/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    return-void
.end method

.method public static b()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .locals 6

    const-class v2, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit v2

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v1

    invoke-interface {v1}, Lr/z;->h()V

    invoke-static {v0}, Lr/C;->a(LA/c;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not stop "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " tile store"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_4
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lr/n;->b()V

    :cond_3
    invoke-static {}, Lv/d;->d()V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Law/h;->u()V

    invoke-static {}, Law/h;->c()V

    :cond_4
    invoke-static {}, Lbm/m;->d()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized d()V
    .locals 4

    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lv/d;->a(Z)V

    :cond_4
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static e()I
    .locals 1

    sget v0, Lcom/google/android/maps/driveabout/vector/bf;->c:I

    return v0
.end method

.method public static declared-synchronized f()V
    .locals 4

    const-class v1, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lv/d;->a(Z)V

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized g()J
    .locals 7

    const-class v3, Lcom/google/android/maps/driveabout/vector/bf;

    monitor-enter v3

    const-wide/16 v0, 0x0

    :try_start_0
    sget-boolean v2, Lcom/google/android/maps/driveabout/vector/bf;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit v3

    return-wide v0

    :cond_0
    :try_start_1
    invoke-static {}, LA/c;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->e()J

    move-result-wide v5

    add-long v0, v1, v5

    :goto_2
    move-wide v1, v0

    goto :goto_1

    :cond_1
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    invoke-virtual {v0}, Lr/n;->g()J

    move-result-wide v4

    add-long/2addr v1, v4

    :cond_2
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    invoke-virtual {v0}, Lv/d;->b()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    add-long v0, v1, v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move-wide v0, v1

    goto :goto_2
.end method

.method public static h()I
    .locals 1

    sget v0, Lcom/google/android/maps/driveabout/vector/bf;->f:I

    return v0
.end method
