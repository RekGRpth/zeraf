.class public Lcom/google/android/maps/driveabout/vector/aK;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/ac;
.implements Lw/c;


# static fields
.field public static volatile a:Z

.field public static final b:Ljava/lang/ThreadLocal;

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final l:Ljava/util/Comparator;


# instance fields
.field private final A:Lcom/google/android/maps/driveabout/vector/ax;

.field private final B:Lcom/google/android/maps/driveabout/vector/n;

.field private final C:Lcom/google/android/maps/driveabout/vector/x;

.field private final D:LS/b;

.field private E:LS/c;

.field private final F:Ljava/util/HashSet;

.field private final G:Ljava/util/HashSet;

.field private final H:[I

.field private final I:Ljava/util/List;

.field private J:J

.field private K:Z

.field private final L:Lcom/google/android/maps/driveabout/vector/bb;

.field private M:Ljava/util/List;

.field private N:Z

.field private O:Landroid/graphics/Bitmap;

.field private P:Z

.field private Q:F

.field private R:J

.field private volatile S:Lcom/google/android/maps/driveabout/vector/q;

.field private final T:Ljava/util/List;

.field private U:Ljava/util/List;

.field private V:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile W:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile X:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile Y:Z

.field private Z:J

.field private aa:I

.field private volatile ab:Lcom/google/android/maps/driveabout/vector/aZ;

.field private ac:Z

.field private volatile ad:F

.field private volatile ae:Z

.field private af:Z

.field private ag:Ljava/lang/Object;

.field private ah:Z

.field private volatile ai:I

.field private aj:Z

.field private ak:I

.field private al:J

.field private am:Z

.field private an:Lz/t;

.field private final ao:Lz/k;

.field private ap:LZ/a;

.field private volatile aq:J

.field private ar:Ljava/lang/Object;

.field protected c:Ljava/util/Map;

.field protected d:Ljava/util/List;

.field protected e:Z

.field private volatile j:LG/a;

.field private volatile k:LC/b;

.field private m:LD/a;

.field private volatile n:I

.field private volatile o:I

.field private final p:Ljava/util/LinkedList;

.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:LC/a;

.field private final u:Lcom/google/android/maps/driveabout/vector/u;

.field private final v:Lx/k;

.field private final w:Landroid/content/res/Resources;

.field private final x:F

.field private y:Ly/d;

.field private final z:Lcom/google/android/maps/driveabout/vector/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->g:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->h:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->i:[I

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aL;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aL;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aM;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aM;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->l:Ljava/util/Comparator;

    return-void

    nop

    :array_0
    .array-data 4
        0xed00
        0xea00
        0xe200
        0x10000
    .end array-data

    :array_1
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_2
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/u;Landroid/content/res/Resources;LC/a;Lcom/google/android/maps/driveabout/vector/aZ;Lz/k;Lz/t;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/aN;-><init>(Lcom/google/android/maps/driveabout/vector/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->U:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    sget-object v0, LG/a;->s:LG/a;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    new-instance v0, Lx/k;

    invoke-direct {v0, p0}, Lx/k;-><init>(Lcom/google/android/maps/driveabout/vector/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {v0}, LF/G;->a(F)V

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {v0}, LF/w;->a(F)V

    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, p2}, Lcom/google/android/maps/driveabout/vector/x;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ax;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/ax;-><init>(ILcom/google/android/maps/driveabout/vector/x;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v3, v1}, Lcom/google/android/maps/driveabout/vector/ax;-><init>(ILcom/google/android/maps/driveabout/vector/x;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    new-instance v0, LS/b;

    invoke-direct {v0}, LS/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/q;I)V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/L;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/L;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LS/c;

    invoke-direct {v0, p2}, LS/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    invoke-virtual {v0, v1}, LS/b;->a(LS/a;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    iput-object p5, p0, Lcom/google/android/maps/driveabout/vector/aK;->ao:Lz/k;

    iput-object p6, p0, Lcom/google/android/maps/driveabout/vector/aK;->an:Lz/t;

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    goto :goto_0
.end method

.method static a(IIF)F
    .locals 2

    add-int v0, p0, p1

    int-to-float v0, v0

    const/high16 v1, 0x43800000

    mul-float/2addr v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    const/high16 v1, 0x3f800000

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000

    invoke-static {v0}, LC/a;->a(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aK;)LS/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    return-object v0
.end method

.method private a(LC/a;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    invoke-virtual {p1}, LC/a;->l()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    invoke-virtual {p1}, LC/a;->A()[F

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/16 v3, 0xc11

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    :cond_0
    return-void
.end method

.method private a(LC/a;IZZ)V
    .locals 11

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->u()Lcom/google/android/maps/driveabout/vector/aP;

    move-result-object v8

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aZ;->j()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {p1}, LC/a;->n()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;)V

    invoke-virtual {p1}, LC/a;->n()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(LC/a;)V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_2

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lcom/google/android/maps/driveabout/vector/D;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_8

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_3

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v1, p1, v3}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v0, :cond_7

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v5, :cond_7

    aget-object v0, v4, v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    invoke-interface {v0}, LF/T;->e()LF/m;

    move-result-object v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v1, v0, p1}, Ly/d;->a(LF/T;LC/a;)LF/m;

    move-result-object v1

    invoke-interface {v0, v1}, LF/T;->a(LF/m;)V

    :cond_5
    move-object v0, v1

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0, p1, v1}, LF/m;->b(LC/a;LD/a;)Z

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/n;->a(Ljava/util/List;)V

    :cond_8
    if-eqz p4, :cond_9

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v2, v0, :cond_9

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v2, v0, :cond_9

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;IZ[Lcom/google/android/maps/driveabout/vector/aZ;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    if-eqz v0, :cond_b

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    const/4 v3, -0x1

    aput v3, v0, v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_5
    if-ge v7, v10, :cond_a

    aget-object v0, v9, v7

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-le v0, v6, :cond_18

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_6
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_5

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1, v3, v6, v4}, LS/c;->a(Ljava/util/HashSet;Ljava/util/HashSet;ILcom/google/android/maps/driveabout/vector/q;)V

    :cond_b
    if-nez p3, :cond_c

    if-eqz p2, :cond_11

    :cond_c
    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p0, v8, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aP;Z)V

    new-instance v3, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v0, 0x0

    invoke-direct {v3, v2, v0}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->m()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->r()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(Lcom/google/android/maps/driveabout/vector/aH;)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v4, v1}, Lcom/google/android/maps/driveabout/vector/aG;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_e
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aI;->a:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v4, :cond_f

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    sget-object v4, LA/c;->a:LA/c;

    if-ne v0, v4, :cond_f

    :cond_f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->A()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v1, v0, p1, v3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->B()V

    instance-of v0, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    if-eqz v0, :cond_12

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->q()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aG;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_8

    :cond_11
    const/4 v0, 0x0

    goto :goto_7

    :cond_12
    const/4 v0, 0x0

    goto :goto_9

    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    if-eqz v0, :cond_15

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x505

    if-ne v0, v2, :cond_14

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->Z:J

    sub-long/2addr v2, v4

    const-string v4, "\nTime in current GL context: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v2

    invoke-virtual {v2}, LB/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    :cond_14
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GL Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawFrameInternal GL ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    if-eqz v1, :cond_16

    :cond_16
    const/16 v1, 0x505

    if-ne v0, v1, :cond_17

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_17

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/D;->i_()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    return-void

    :cond_18
    move v0, v6

    goto/16 :goto_6
.end method

.method private a(LC/a;IZ[Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 11

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-nez p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/aG;->a(Ly/d;Z)V

    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v1, p2}, Ly/d;->a(I)V

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {p1}, LC/a;->C()Lo/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ly/d;->a(Lo/aS;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aG;->a(Ly/d;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    if-nez p3, :cond_5

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Ly/d;->b(I)V

    goto :goto_2

    :cond_5
    new-instance v5, Lcom/google/android/maps/driveabout/vector/aF;

    invoke-direct {v5}, Lcom/google/android/maps/driveabout/vector/aF;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, LC/a;->C()Lo/aQ;

    move-result-object v2

    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :goto_4
    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v7

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v8

    array-length v4, p4

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v4, :cond_9

    aget-object v9, p4, v1

    invoke-virtual {v9}, Lcom/google/android/maps/driveabout/vector/aZ;->k()Z

    move-result v10

    if-nez v10, :cond_8

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto :goto_4

    :cond_8
    invoke-virtual {v9, v0, v5, v6}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I

    move-result v10

    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v9, v7, v8}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Ljava/util/Set;Ljava/util/Map;)V

    goto :goto_6

    :cond_9
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    const/16 v9, 0x14

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v10

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, Ly/d;->a(LC/a;Lo/aS;ILjava/util/Iterator;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILA/c;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x0

    goto :goto_7

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(LD/a;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aK;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    return p1
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/q;)[I
    .locals 2

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aO;->a:[I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->i:[I

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->g:[I

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->h:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(LC/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, LC/a;->z()[F

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    return-void
.end method

.method private c(I)V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/u;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->h()Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v3}, LC/a;->g()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_a

    move v3, v2

    :goto_1
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v0}, LC/a;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    move v0, v2

    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/vector/aK;->d(Z)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->e()V

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->v()V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->W:Lcom/google/android/maps/driveabout/vector/aG;

    iput-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3, p0}, Lcom/google/android/maps/driveabout/vector/aG;->a(Lcom/google/android/maps/driveabout/vector/aK;)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aG;->a()V

    :cond_2
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->X:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->X:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aG;->a()V

    :cond_3
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v3}, LC/a;->o()F

    move-result v3

    const/high16 v4, 0x3f800000

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-direct {p0, v3, p1, v0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;IZZ)V

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->f()V

    :cond_4
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/u;->i()Z

    move-result v3

    if-eqz v3, :cond_b

    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_b

    move v3, v2

    :goto_2
    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v4}, Ly/d;->c()Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v4}, LD/a;->c()Z

    move-result v4

    if-nez v4, :cond_c

    if-nez v3, :cond_c

    :goto_3
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/vector/aG;->b(Z)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aG;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v2, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :cond_5
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->t()Landroid/graphics/Bitmap;

    move-result-object v2

    monitor-enter p0

    :try_start_1
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v2}, Ly/d;->c()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->c()Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :cond_8
    :goto_4
    return-void

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v3, v1

    goto/16 :goto_1

    :cond_b
    move v3, v1

    goto :goto_2

    :cond_c
    move v2, v1

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_d
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_e

    if-nez v0, :cond_e

    if-nez p1, :cond_e

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v0, v4, v6

    if-lez v0, :cond_e

    invoke-static {}, Ljava/lang/System;->gc()V

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    :cond_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_4
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/D;)V
    .locals 5

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/D;

    instance-of v3, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v3, :cond_0

    check-cast v1, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->s()LB/e;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->s()LB/e;

    move-result-object v4

    if-ne v3, v4, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The added tile Overlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " shares the same GLTileCache with "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v0

    aget v2, v0, v6

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClearColorx(IIII)V

    const/16 v0, 0x4000

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x4100

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->h()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glClearStencil(I)V

    or-int/lit16 v0, v0, 0x400

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->j()V

    :cond_1
    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    return-void
.end method

.method private declared-synchronized d(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aQ;

    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/vector/aQ;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->G()Lx/a;

    move-result-object v0

    invoke-virtual {v0}, Lx/a;->a()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {v0}, Lx/o;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->D()V

    :cond_1
    return-void
.end method

.method private s()V
    .locals 4

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    :goto_1
    :try_start_0
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    invoke-static {v1, v0}, Landroid/os/Process;->setThreadPriority(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    goto :goto_1
.end method

.method private t()Landroid/graphics/Bitmap;
    .locals 14

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v2}, LC/a;->k()I

    move-result v3

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v2}, LC/a;->l()I

    move-result v4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-static {v13}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    invoke-static {v13}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    mul-int v2, v3, v4

    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v7

    move v2, v1

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    invoke-virtual {v7}, Ljava/nio/IntBuffer;->array()[I

    move-result-object v6

    move-object v5, v13

    move v7, v1

    move v8, v3

    move v9, v1

    move v10, v1

    move v11, v3

    move v12, v4

    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    return-object v13
.end method

.method private u()Lcom/google/android/maps/driveabout/vector/aP;
    .locals 9

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    :cond_1
    move v6, v1

    move v2, v1

    move v4, v1

    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aO;->b:[I

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->b:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aS;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_2
    move v0, v2

    move v1, v4

    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    move v2, v0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v1, :cond_e

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v1, Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v4

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ge v4, v8, :cond_3

    move v4, v3

    :goto_4
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LA/c;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;)V

    goto :goto_5

    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    move v4, v2

    goto :goto_4

    :cond_4
    move v1, v4

    :goto_6
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/D;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/b;)V

    :cond_5
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V

    move v0, v1

    move v1, v3

    goto :goto_3

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/D;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v1

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ne v1, v8, :cond_c

    move v1, v3

    :goto_7
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_8
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-interface {v2, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2, v8}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/b;)V

    :cond_6
    move v0, v1

    move v1, v4

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_9

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v5

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v5

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ge v5, v6, :cond_b

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    :goto_a
    move-object v1, v0

    goto :goto_9

    :cond_8
    if-eqz v1, :cond_9

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    :cond_9
    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_a

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aK;->l:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_a
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aP;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aP;-><init>(Ljava/util/List;Ljava/util/List;)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    :cond_b
    move-object v0, v1

    goto :goto_a

    :cond_c
    move v1, v2

    goto/16 :goto_7

    :cond_d
    move v1, v2

    goto/16 :goto_8

    :cond_e
    move v1, v2

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private v()V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Ly/d;->a(Z)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/D;->a(Z)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ad:F

    return v0
.end method

.method public a(Lo/T;)F
    .locals 4

    const/high16 v0, 0x41a80000

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Lo/T;)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit v2

    return v1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;
    .locals 2

    new-instance v0, Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/A;-><init>(Lcom/google/android/maps/driveabout/vector/E;Lcom/google/android/maps/driveabout/vector/x;)V

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    invoke-virtual {v0, p1, p2}, LS/c;->a(II)V

    :cond_0
    return-void
.end method

.method public a(LA/c;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(LG/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, p1}, Ly/d;->a(LG/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public a(LZ/a;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, p1}, Ly/d;->a(LZ/a;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/D;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->a:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lcom/google/android/maps/driveabout/vector/aG;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->W:Lcom/google/android/maps/driveabout/vector/aG;

    return-void
.end method

.method a(Lcom/google/android/maps/driveabout/vector/aP;Z)V
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v5

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_1
    if-ge v4, v7, :cond_2

    aget-object v8, v6, v4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    :cond_1
    invoke-virtual {v8, v0}, Lcom/google/android/maps/driveabout/vector/D;->b(Ljava/util/List;)Z

    move-result v0

    or-int/2addr v2, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->a(Z)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->a(Z)V

    goto :goto_3

    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->b(Z)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->b(Z)V

    goto :goto_4

    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->a:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->c:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->b:Lcom/google/android/maps/driveabout/vector/aS;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/x;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/x;->a(Lcom/google/android/maps/driveabout/vector/y;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    if-eqz p1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_1
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7

    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lx/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->J()Z

    move-result v1

    invoke-virtual {v0, v1}, LD/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/q;)V

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ae:Z

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ae:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->af:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->e()V

    :cond_5
    invoke-static {}, LR/o;->e()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->h()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(I)V

    goto :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lx/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-virtual {v0, p2, p3, v1}, LC/a;->a(IIF)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v0}, LC/a;->n()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(IIF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ad:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x1

    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V

    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/E;->a(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->r()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-nez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Z:J

    new-instance v0, LD/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->ao:Lz/k;

    invoke-direct {v0, p1, v1, v2, v3}, LD/a;-><init>(Ljavax/microedition/khronos/opengles/GL10;Lx/k;Lcom/google/android/maps/driveabout/vector/u;Lz/k;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/u;->b(Z)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {v0, v1}, Lx/o;->a(Landroid/content/res/Resources;LD/a;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->F()I

    move-result v0

    invoke-static {v0}, LF/G;->a(I)V

    new-instance v0, Ly/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Ly/d;-><init>(LG/a;LD/a;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    invoke-virtual {v0, v1}, Ly/d;->a(LZ/a;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Ly/d;)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Ly/d;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V

    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/u;->b(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->c()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/aA;
    .locals 3

    new-instance v0, Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/maps/driveabout/vector/aA;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/x;Z)V

    return-object v0
.end method

.method public b()V
    .locals 1

    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/ax;->b(I)V

    return-void
.end method

.method public b(LA/c;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()Lu/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lu/d;->b(LA/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/D;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->b:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/q;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->e()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c(Z)V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    :try_start_0
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->b()V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public g()Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public h()I
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/b;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-interface {v0, v6}, Lcom/google/android/maps/driveabout/vector/b;->a_(LC/a;)I

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()LC/b;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()LC/b;

    move-result-object v0

    :goto_1
    or-int v1, v3, v6

    move v3, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_3

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    invoke-virtual {v1, v0}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/b;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    :cond_2
    invoke-virtual {v0, v4}, LC/a;->a(Z)V

    return v3

    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/b;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public declared-synchronized i()Landroid/graphics/Bitmap;
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    return-object v0
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    return-void
.end method

.method public l()LF/H;
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->e()Lcom/google/android/maps/driveabout/vector/c;

    move-result-object v0

    instance-of v1, v0, LF/H;

    if-eqz v1, :cond_0

    check-cast v0, LF/H;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lcom/google/android/maps/driveabout/vector/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    return-object v0
.end method

.method public n()LS/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    return-object v0
.end method

.method public o()Lcom/google/android/maps/driveabout/vector/aZ;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    return-object v0
.end method

.method public p()LD/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    return-object v0
.end method

.method public q()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    return-void
.end method
