.class public Lcom/google/android/maps/driveabout/vector/av;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Set;

.field private b:Ljava/util/Set;

.field private c:Ljava/util/Set;

.field private d:Lcom/google/googlenav/intersectionexplorer/c;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Lcom/google/android/maps/driveabout/vector/av;)V

    return-void
.end method

.method private static a(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/android/maps/driveabout/vector/z;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    new-instance v2, Lo/Z;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lo/Z;-><init>(I)V

    invoke-virtual {v2, v0}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v2, v1}, Lo/Z;->a(Lo/T;)Z

    new-instance v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v2}, Lo/Z;->d()Lo/X;

    move-result-object v1

    const/high16 v2, 0x3f800000

    const v3, -0xffff01

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    return-object v0
.end method

.method private declared-synchronized b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->c:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/aw;

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aw;-><init>(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)V

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/av;->c:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/av;->a(Lcom/google/googlenav/intersectionexplorer/c;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/android/maps/driveabout/vector/z;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->g()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/av;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/intersectionexplorer/c;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/google/googlenav/intersectionexplorer/c;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Z)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->d:Lcom/google/googlenav/intersectionexplorer/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/c;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/Set;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->e:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/common/collect/dA;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/av;->a:Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/av;->f:Z

    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->r:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
