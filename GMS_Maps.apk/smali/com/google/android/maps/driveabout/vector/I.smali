.class Lcom/google/android/maps/driveabout/vector/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/vector/G;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/G;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/G;Lcom/google/android/maps/driveabout/vector/H;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/I;-><init>(Lcom/google/android/maps/driveabout/vector/G;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->b(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v1, Lm/q;->c:Lm/q;

    invoke-static {v0, v1}, Lm/o;->a(Ljava/util/List;Lm/q;)Lm/v;
    :try_end_1
    .catch Lm/k; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;Lm/v;)Lm/v;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/vector/G;->a(Lcom/google/android/maps/driveabout/vector/G;Z)Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/I;->a:Lcom/google/android/maps/driveabout/vector/G;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->c(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_0
    const/16 v0, 0x7d0

    if-ge v1, v0, :cond_1

    sget-object v0, Lm/q;->d:Lm/q;

    invoke-static {v2, v0}, Lm/o;->a(Ljava/util/List;Lm/q;)Lm/v;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lm/v;->a()Lm/v;
    :try_end_4
    .catch Lm/k; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0
.end method
