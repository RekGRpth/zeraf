.class public Lcom/google/android/maps/driveabout/vector/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/aU;


# static fields
.field static final a:I


# instance fields
.field final b:Lcom/google/android/maps/driveabout/vector/w;

.field protected c:Lcom/google/android/maps/driveabout/vector/v;

.field private d:I

.field private e:J

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    :goto_0
    sput v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    return-void

    :cond_0
    const/16 v0, 0x14

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/w;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z

    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    return-void
.end method

.method private j()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3e6b8520

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    int-to-float v0, v0

    const v1, 0x3f8ccccd

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    :cond_0
    :goto_0
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    return-void

    :cond_1
    const v1, 0x3ebd70a4

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    int-to-float v0, v0

    const v1, 0x3f666666

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/u;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/u;->e:J

    return-void
.end method

.method public a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/v;->a(J)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    return-void
.end method

.method public a(ZZ)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->d()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->e()V

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)Z
    .locals 2

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    if-eqz v1, :cond_0

    const v1, 0x88b8

    if-gt v0, v1, :cond_1

    :cond_0
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/u;->d()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/u;->e:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v1, v0, 0x5

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    if-eqz v2, :cond_0

    add-int/lit16 v0, v0, 0x1f4

    :cond_0
    const/16 v2, 0xf

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->c()I

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->e()V

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->d(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/v;->a(I)V

    :cond_2
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/u;->j()V

    :cond_3
    return-void

    :cond_4
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->f()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->f()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->d()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    return-void
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(I)V
    .locals 1

    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    return-void
.end method

.method public declared-synchronized c(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d(I)I
    .locals 0

    return p1
.end method

.method d()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->a()V

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/w;->q_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/v;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/v;-><init>(Lcom/google/android/maps/driveabout/vector/u;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->start()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/v;->a(Lcom/google/android/maps/driveabout/vector/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
