.class public Lcom/google/android/maps/driveabout/vector/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LC/b;


# direct methods
.method public constructor <init>(LC/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bx;->a:LC/b;

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/maps/driveabout/vector/bx;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "vector.version"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    :cond_0
    const-string v0, "VectorMapSavedState"

    const-string v1, "Couldn\'t load from bundle"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bx;

    invoke-static {p0}, LC/b;->b(Landroid/os/Bundle;)LC/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bx;-><init>(LC/b;)V

    goto :goto_0
.end method


# virtual methods
.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "vector.version"

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bx;->a:LC/b;

    invoke-virtual {v0, p1}, LC/b;->a(Landroid/os/Bundle;)V

    return-void
.end method
