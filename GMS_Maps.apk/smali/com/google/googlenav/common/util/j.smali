.class public final Lcom/google/googlenav/common/util/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x5b

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/common/util/j;->a:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x4t
        0x9t
        0xdt
        0x11t
        0x16t
        0x1at
        0x1et
        0x23t
        0x27t
        0x2bt
        0x30t
        0x34t
        0x38t
        0x3ct
        0x41t
        0x45t
        0x49t
        0x4dt
        0x51t
        0x56t
        0x5at
        0x5et
        0x62t
        0x66t
        0x6at
        0x6et
        0x71t
        0x75t
        0x79t
        0x7dt
        -0x7ft
        -0x7ct
        -0x78t
        -0x74t
        -0x71t
        -0x6dt
        -0x6at
        -0x66t
        -0x63t
        -0x5ft
        -0x5ct
        -0x59t
        -0x56t
        -0x52t
        -0x4ft
        -0x4ct
        -0x49t
        -0x46t
        -0x43t
        -0x40t
        -0x3et
        -0x3bt
        -0x38t
        -0x36t
        -0x33t
        -0x31t
        -0x2et
        -0x2ct
        -0x2at
        -0x27t
        -0x25t
        -0x23t
        -0x21t
        -0x1ft
        -0x1dt
        -0x1ct
        -0x1at
        -0x18t
        -0x17t
        -0x15t
        -0x14t
        -0x12t
        -0x11t
        -0x10t
        -0xft
        -0xdt
        -0xct
        -0xbt
        -0xbt
        -0xat
        -0x9t
        -0x8t
        -0x8t
        -0x7t
        -0x7t
        -0x7t
        -0x6t
        -0x6t
        -0x6t
        -0x6t
    .end array-data
.end method

.method public static a(DD)D
    .locals 2

    invoke-static {p0, p1, p2, p3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(F)F
    .locals 1

    const/high16 v0, 0x43b40000

    rem-float v0, p0, v0

    return v0
.end method

.method public static a(D)I
    .locals 4

    const-wide/high16 v2, 0x3fe0000000000000L

    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    sub-double v0, p0, v2

    :goto_0
    double-to-int v0, v0

    return v0

    :cond_0
    add-double v0, p0, v2

    goto :goto_0
.end method

.method public static a(I)I
    .locals 2

    const/16 v0, 0x168

    invoke-static {p0, v0}, Lcom/google/googlenav/common/util/j;->c(II)I

    move-result v0

    const/16 v1, 0x5a

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/google/googlenav/common/util/j;->a:[B

    rsub-int/lit8 v0, v0, 0x5a

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0xb4

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/google/googlenav/common/util/j;->a:[B

    add-int/lit8 v0, v0, -0x5a

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    neg-int v0, v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x10e

    if-ge v0, v1, :cond_2

    sget-object v1, Lcom/google/googlenav/common/util/j;->a:[B

    rsub-int v0, v0, 0x10e

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    neg-int v0, v0

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/googlenav/common/util/j;->a:[B

    add-int/lit16 v0, v0, -0x10e

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public static a(II)I
    .locals 1

    if-ltz p0, :cond_0

    add-int v0, p0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    div-int v0, p0, p1

    goto :goto_0
.end method

.method public static b(F)F
    .locals 2

    const/high16 v1, 0x43b40000

    cmpl-float v0, p0, v1

    if-ltz v0, :cond_1

    sub-float/2addr p0, v1

    :cond_0
    :goto_0
    return p0

    :cond_1
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    add-float/2addr p0, v1

    goto :goto_0
.end method

.method public static b(I)I
    .locals 4

    const/4 v0, 0x0

    if-gtz p0, :cond_1

    :cond_0
    return v0

    :cond_1
    shr-int/lit8 v1, p0, 0xf

    add-int/lit8 v1, v1, 0x2

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    const/16 v2, 0x20

    if-ge v1, v2, :cond_2

    div-int v2, p0, v0

    add-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_2
    :goto_1
    div-int v1, p0, v0

    if-le v0, v1, :cond_3

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    mul-int v1, v0, v0

    mul-int/lit8 v2, v0, 0x2

    sub-int v2, p0, v2

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static b(II)I
    .locals 1

    div-int/lit8 v0, p1, 0x2

    add-int/2addr v0, p0

    div-int/2addr v0, p1

    return v0
.end method

.method public static c(F)F
    .locals 2

    const/high16 v1, 0x43b40000

    const/high16 v0, 0x43340000

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_1

    sub-float/2addr p0, v1

    :cond_0
    :goto_0
    return p0

    :cond_1
    const/high16 v0, -0x3ccc0000

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    add-float/2addr p0, v1

    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    const/16 v0, 0x168

    if-lt p0, v0, :cond_1

    add-int/lit16 p0, p0, -0x168

    :cond_0
    :goto_0
    return p0

    :cond_1
    if-gez p0, :cond_0

    add-int/lit16 p0, p0, 0x168

    goto :goto_0
.end method

.method private static c(II)I
    .locals 1

    rem-int v0, p0, p1

    if-gez v0, :cond_0

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public static d(I)I
    .locals 1

    const/16 v0, 0xb4

    if-lt p0, v0, :cond_1

    add-int/lit16 p0, p0, -0x168

    :cond_0
    :goto_0
    return p0

    :cond_1
    const/16 v0, -0xb4

    if-ge p0, v0, :cond_0

    add-int/lit16 p0, p0, 0x168

    goto :goto_0
.end method
