.class Lcom/google/googlenav/prefetch/android/c;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/prefetch/android/b;


# direct methods
.method constructor <init>(Lcom/google/googlenav/prefetch/android/b;Las/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-static {v0}, Lcom/google/googlenav/prefetch/android/b;->a(Lcom/google/googlenav/prefetch/android/b;)Ljava/util/List;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iget-object v5, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-static {v5}, Lcom/google/googlenav/prefetch/android/b;->b(Lcom/google/googlenav/prefetch/android/b;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-static {v0}, Lcom/google/googlenav/prefetch/android/b;->c(Lcom/google/googlenav/prefetch/android/b;)Lcom/google/googlenav/prefetch/android/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/prefetch/android/d;->a()V

    :goto_1
    return-void

    :cond_2
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    const/16 v0, 0x4e

    const-string v1, "rg"

    const-string v5, ""

    invoke-static {v0, v1, v5}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-static {v5}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v6

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v7

    invoke-virtual {v7}, Lo/ad;->f()Lo/T;

    move-result-object v7

    new-instance v8, Lcom/google/googlenav/prefetch/android/e;

    invoke-direct {v8, v3}, Lcom/google/googlenav/prefetch/android/e;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    const/16 v9, 0x2710

    new-instance v10, Lcom/google/googlenav/friend/bg;

    invoke-direct {v10}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {v7}, Lo/T;->a()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v10

    invoke-virtual {v7}, Lo/T;->c()I

    move-result v7

    invoke-virtual {v10, v7}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/google/googlenav/friend/bg;->d(I)Lcom/google/googlenav/friend/bg;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/google/googlenav/friend/bg;->e(I)Lcom/google/googlenav/friend/bg;

    move-result-object v7

    invoke-virtual {v7, v8}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v7

    iget-object v9, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-static {v9}, Lcom/google/googlenav/prefetch/android/b;->b(Lcom/google/googlenav/prefetch/android/b;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_3

    invoke-virtual {v7, v2}, Lcom/google/googlenav/friend/be;->a(Z)V

    :goto_3
    invoke-virtual {v6, v7}, Law/h;->c(Law/g;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lcom/google/googlenav/friend/be;->a(Z)V

    goto :goto_3

    :cond_4
    const-wide/16 v0, 0x1e

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/c;->a:Lcom/google/googlenav/prefetch/android/b;

    invoke-static {v0}, Lcom/google/googlenav/prefetch/android/b;->c(Lcom/google/googlenav/prefetch/android/b;)Lcom/google/googlenav/prefetch/android/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/prefetch/android/d;->a()V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    goto :goto_4
.end method
