.class public Lcom/google/googlenav/prefetch/android/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;


# instance fields
.field private a:LaN/B;

.field private b:J

.field private c:J

.field private d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/prefetch/android/g;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    return-void
.end method


# virtual methods
.method public a(ILaH/m;)V
    .locals 0

    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 11

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/googlenav/prefetch/android/g;->c:J

    const-wide/16 v8, 0x7530

    add-long/2addr v6, v8

    cmp-long v2, v6, v4

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-wide v4, p0, Lcom/google/googlenav/prefetch/android/g;->c:J

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v6}, LaH/h;->a()LaN/B;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_0

    invoke-static {v6}, LaH/h;->a(Landroid/location/Location;)I

    move-result v6

    const/16 v7, 0x3e8

    if-ge v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/googlenav/prefetch/android/g;->a:LaN/B;

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/google/googlenav/prefetch/android/g;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v6, v2, v1, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    :goto_2
    if-eqz v0, :cond_0

    iput-object v2, p0, Lcom/google/googlenav/prefetch/android/g;->a:LaN/B;

    iput-wide v4, p0, Lcom/google/googlenav/prefetch/android/g;->b:J

    goto :goto_0

    :cond_2
    move-object v2, v3

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/googlenav/prefetch/android/g;->a:LaN/B;

    invoke-virtual {v6, v2}, LaN/B;->b(LaN/B;)J

    move-result-wide v6

    long-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v6, v6

    iget-wide v7, p0, Lcom/google/googlenav/prefetch/android/g;->b:J

    sub-long v7, v4, v7

    const/16 v9, 0x64

    if-ge v6, v9, :cond_4

    const-wide/32 v9, 0xea60

    cmp-long v6, v7, v9

    if-ltz v6, :cond_5

    :cond_4
    iget-object v6, p0, Lcom/google/googlenav/prefetch/android/g;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v6, v2, v1, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2
.end method
