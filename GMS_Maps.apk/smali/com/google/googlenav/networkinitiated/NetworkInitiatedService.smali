.class public Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private b:Landroid/os/Handler;

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaM/f;->q()V

    :cond_0
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->u()V

    :cond_1
    invoke-static {}, Lcom/google/googlenav/android/c;->f()V

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c()V

    return-void
.end method

.method private a(Landroid/content/Intent;I)V
    .locals 3

    const/4 v1, 0x0

    invoke-static {p0}, Lak/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/googlenav/K;->N()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/M;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/friend/ad;->G()V

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {p0, v1}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    move-result-object v0

    :goto_1
    invoke-static {v1, v1, v1}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/bi;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    invoke-virtual {v0}, LaM/f;->r()V

    invoke-virtual {p0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, LaM/f;->o()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0, v1}, LaM/f;->b(LaM/g;)V

    :cond_3
    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "remote_intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->b()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, LaM/f;->a()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c()V

    return-void
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/networkinitiated/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/networkinitiated/b;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, p1}, Lcom/google/googlenav/networkinitiated/b;->b(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "NetworkInitiatedService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lcom/google/googlenav/networkinitiated/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/networkinitiated/e;-><init>(Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;)V

    iput-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->b:Landroid/os/Handler;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/i;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/i;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/a;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/a;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/h;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/h;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/g;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/g;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/f;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/f;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/networkinitiated/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/networkinitiated/m;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->d:Ljava/util/List;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    invoke-direct {p0}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/networkinitiated/NetworkInitiatedService;->a(Landroid/content/Intent;I)V

    const/4 v0, 0x3

    return v0
.end method
