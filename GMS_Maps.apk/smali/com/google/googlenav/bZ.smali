.class public Lcom/google/googlenav/bZ;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[Lcom/google/googlenav/ab;

.field private static final e:Lcom/google/googlenav/cf;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:J

.field protected final c:Ljava/util/List;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:LaN/B;

.field private i:J

.field private j:Z

.field private final k:Ljava/util/Set;

.field private l:[Lcom/google/googlenav/bZ;

.field private m:[Lcom/google/googlenav/cj;

.field private n:Lcom/google/googlenav/ci;

.field private o:Lcom/google/googlenav/ck;

.field private p:Z

.field private q:Z

.field private r:[Lcom/google/googlenav/ab;

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/googlenav/ab;

    sput-object v0, Lcom/google/googlenav/bZ;->d:[Lcom/google/googlenav/ab;

    new-instance v0, Lcom/google/googlenav/cf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/cf;-><init>(Lcom/google/googlenav/ca;)V

    sput-object v0, Lcom/google/googlenav/bZ;->e:Lcom/google/googlenav/cf;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    sget-object v1, Lcom/google/googlenav/bZ;->e:Lcom/google/googlenav/cf;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/cd;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/cd;)V
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/google/googlenav/bZ;->f:I

    iput-wide v5, p0, Lcom/google/googlenav/bZ;->i:J

    iput-boolean v1, p0, Lcom/google/googlenav/bZ;->j:Z

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/bZ;->k:Ljava/util/Set;

    new-array v2, v1, [Lcom/google/googlenav/cj;

    iput-object v2, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    iput-object v0, p0, Lcom/google/googlenav/bZ;->o:Lcom/google/googlenav/ck;

    sget-object v2, Lcom/google/googlenav/bZ;->d:[Lcom/google/googlenav/ab;

    iput-object v2, p0, Lcom/google/googlenav/bZ;->r:[Lcom/google/googlenav/ab;

    iput v4, p0, Lcom/google/googlenav/bZ;->s:I

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hF;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-ne p2, v2, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bZ;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, p3}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V

    :goto_0
    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->k()I

    move-result v1

    if-ge v0, v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cm;->i()J

    move-result-wide v1

    cmp-long v3, v1, v5

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/bZ;->k:Ljava/util/Set;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hF;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-ne p2, v2, :cond_3

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iput-object v0, p0, Lcom/google/googlenav/bZ;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/bZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ProtoBufType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/bZ;->b:J

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bZ;)Lcom/google/googlenav/ck;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->o:Lcom/google/googlenav/ck;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ck;)Lcom/google/googlenav/ck;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bZ;->o:Lcom/google/googlenav/ck;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/bZ;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bZ;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/bZ;)Lcom/google/googlenav/ci;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->n:Lcom/google/googlenav/ci;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 12

    const/4 v11, 0x4

    const/4 v7, 0x0

    const/4 v0, 0x7

    invoke-static {p1, v0, v7}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    move v8, v7

    :goto_0
    if-ge v8, v9, :cond_2

    new-instance v3, Lcom/google/googlenav/cm;

    invoke-virtual {p1, v11, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, p0, v1}, Lcom/google/googlenav/cm;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/bZ;Lcom/google/googlenav/cd;)V

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move v6, v7

    :goto_1
    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/googlenav/cm;

    invoke-virtual {v4}, Lcom/google/googlenav/cm;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v10, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    new-instance v0, Lcom/google/googlenav/cm;

    invoke-static {v4}, Lcom/google/googlenav/cm;->a(Lcom/google/googlenav/cm;)Lcom/google/googlenav/cl;

    move-result-object v1

    invoke-static {v3}, Lcom/google/googlenav/cm;->b(Lcom/google/googlenav/cm;)[Lcom/google/googlenav/co;

    move-result-object v2

    invoke-static {v3}, Lcom/google/googlenav/cm;->c(Lcom/google/googlenav/cm;)Z

    move-result v3

    invoke-static {v4}, Lcom/google/googlenav/cm;->d(Lcom/google/googlenav/cm;)[I

    move-result-object v5

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/cm;-><init>(Lcom/google/googlenav/cl;[Lcom/google/googlenav/co;ZLcom/google/googlenav/bZ;[I)V

    invoke-interface {v10, v6, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_2
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V
    .locals 9

    const/16 v8, 0x8

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x7

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    new-instance v4, Lcom/google/googlenav/cm;

    invoke-virtual {p1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct {v4, v5, p0, p2}, Lcom/google/googlenav/cm;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/bZ;Lcom/google/googlenav/cd;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/bZ;

    iput-object v0, p0, Lcom/google/googlenav/bZ;->l:[Lcom/google/googlenav/bZ;

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/bZ;->l:[Lcom/google/googlenav/bZ;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/bZ;->l:[Lcom/google/googlenav/bZ;

    new-instance v3, Lcom/google/googlenav/bZ;

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/cj;

    iput-object v0, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    new-instance v3, Lcom/google/googlenav/cj;

    invoke-virtual {p1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/cj;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x6

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/bZ;->i:J

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->c()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/bh;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/cm;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cm;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    move v3, v2

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->k()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/cm;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v1

    :cond_0
    if-eqz v2, :cond_3

    const-string v2, "d"

    move-object v3, v2

    :goto_2
    if-eqz v0, :cond_4

    const-string v2, "a"

    :goto_3
    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->k()I

    move-result v4

    if-ge v4, v1, :cond_5

    if-nez v0, :cond_5

    const-string v0, "e"

    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const-string v2, ""

    move-object v3, v2

    goto :goto_2

    :cond_4
    const-string v2, ""

    goto :goto_3

    :cond_5
    const-string v0, ""

    goto :goto_4
.end method

.method public a(Lcom/google/googlenav/ci;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bZ;->n:Lcom/google/googlenav/ci;

    return-void
.end method

.method public a(Lcom/google/googlenav/cm;)V
    .locals 3

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cm;

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/google/googlenav/bZ;->s:I

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/bZ;->s:I

    goto :goto_1
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/bZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V

    :goto_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/bZ;->b:J

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/bZ;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V
    .locals 5

    const/4 v4, 0x6

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    :cond_0
    if-nez p1, :cond_2

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/bZ;->g:Ljava/lang/String;

    if-nez p1, :cond_3

    move-object v0, v1

    :goto_1
    if-nez v0, :cond_4

    :goto_2
    iput-object v1, p0, Lcom/google/googlenav/bZ;->h:LaN/B;

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lez v1, :cond_1

    new-array v0, v1, [Lcom/google/googlenav/ab;

    iput-object v0, p0, Lcom/google/googlenav/bZ;->r:[Lcom/google/googlenav/ab;

    :cond_1
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_5

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v2

    invoke-interface {p2, v2}, Lcom/google/googlenav/cd;->b(I)Lcom/google/googlenav/ab;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/bZ;->r:[Lcom/google/googlenav/ab;

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {v0}, LaN/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v1

    goto :goto_2

    :cond_5
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bZ;->p:Z

    return-void
.end method

.method public b(I)Lcom/google/googlenav/cj;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/googlenav/bZ;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bZ;->q:Z

    return-void
.end method

.method public c(I)Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->l:[Lcom/google/googlenav/bZ;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c(Z)Lcom/google/googlenav/cm;
    .locals 11

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    const-wide/16 v2, -0x1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cm;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/cm;->b(Z)J

    move-result-wide v4

    const-wide/16 v7, 0x0

    cmp-long v7, v4, v7

    if-eqz v7, :cond_2

    if-eqz v1, :cond_3

    cmp-long v7, v4, v2

    if-gez v7, :cond_4

    :cond_3
    move-wide v1, v4

    :goto_1
    move-wide v9, v1

    move-wide v2, v9

    move-object v1, v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    move-wide v9, v2

    move-wide v1, v9

    goto :goto_1
.end method

.method public c()Ljava/util/Set;
    .locals 7

    const-wide/16 v5, 0x0

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cm;

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->i()J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->i()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/cm;->j()J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/cm;->j()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public d()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->k:Ljava/util/Set;

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/bZ;->p:Z

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/bZ;->q:Z

    return v0
.end method

.method public declared-synchronized g()V
    .locals 7

    const-wide/16 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/googlenav/bZ;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-wide v0, p0, Lcom/google/googlenav/bZ;->i:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bZ;->j:Z

    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ca;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ca;-><init>(Lcom/google/googlenav/bZ;)V

    invoke-direct {v0, v1, v2}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/googlenav/bZ;->b:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x0

    iget-wide v5, p0, Lcom/google/googlenav/bZ;->i:J

    sub-long v1, v5, v1

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j()LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->h:LaN/B;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->m:[Lcom/google/googlenav/cj;

    array-length v0, v0

    return v0
.end method

.method public m()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/googlenav/bZ;->f:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/bZ;->k()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/bZ;->a(I)Lcom/google/googlenav/cm;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/cm;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iput v1, p0, Lcom/google/googlenav/bZ;->f:I

    :goto_1
    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/google/googlenav/bZ;->f:I

    :cond_3
    iget v0, p0, Lcom/google/googlenav/bZ;->f:I

    if-nez v0, :cond_4

    move v1, v2

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public n()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/bZ;->l:[Lcom/google/googlenav/bZ;

    array-length v0, v0

    return v0
.end method
