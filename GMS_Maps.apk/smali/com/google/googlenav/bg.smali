.class public Lcom/google/googlenav/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private B:Ljava/util/Set;

.field private C:I

.field private D:LaW/R;

.field private E:LaN/H;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:LaN/H;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/googlenav/ba;

.field private i:Lcom/google/googlenav/bb;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:LaN/M;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:LaN/B;

.field private q:Z

.field private r:I

.field private s:I

.field private t:Z

.field private u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private v:Ljava/lang/String;

.field private w:Ljava/util/Map;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    iput v4, p0, Lcom/google/googlenav/bg;->b:I

    iput v1, p0, Lcom/google/googlenav/bg;->c:I

    iput v3, p0, Lcom/google/googlenav/bg;->d:I

    iput v3, p0, Lcom/google/googlenav/bg;->e:I

    iput-object v2, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    const-string v0, "100"

    iput-object v0, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    iput-object v2, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/googlenav/bg;->j:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->n:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->o:Z

    iput-object v2, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->q:Z

    iput v1, p0, Lcom/google/googlenav/bg;->r:I

    iput v4, p0, Lcom/google/googlenav/bg;->s:I

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->t:Z

    iput-object v2, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->x:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->y:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->z:Z

    iput-object v2, p0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v2, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    iput v1, p0, Lcom/google/googlenav/bg;->C:I

    iput-object v2, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    iput-object v2, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->F:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->G:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->H:Z

    iput-boolean v1, p0, Lcom/google/googlenav/bg;->I:Z

    iput-object v2, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/bf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    iget v0, p1, Lcom/google/googlenav/bf;->b:I

    iput v0, p0, Lcom/google/googlenav/bg;->b:I

    iget v0, p1, Lcom/google/googlenav/bf;->c:I

    iput v0, p0, Lcom/google/googlenav/bg;->c:I

    iget v0, p1, Lcom/google/googlenav/bf;->d:I

    iput v0, p0, Lcom/google/googlenav/bg;->d:I

    iget v0, p1, Lcom/google/googlenav/bf;->e:I

    iput v0, p0, Lcom/google/googlenav/bg;->e:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->f:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    iget-object v0, p1, Lcom/google/googlenav/bf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/bf;->h:Lcom/google/googlenav/ba;

    iput-object v0, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    iget-object v0, p1, Lcom/google/googlenav/bf;->i:Lcom/google/googlenav/bb;

    iput-object v0, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    iget v0, p1, Lcom/google/googlenav/bf;->j:I

    iput v0, p0, Lcom/google/googlenav/bg;->j:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/bf;->l:LaN/M;

    iput-object v0, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->m:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->n:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->o:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->o:Z

    iget-object v0, p1, Lcom/google/googlenav/bf;->p:LaN/B;

    iput-object v0, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->q:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->q:Z

    iget v0, p1, Lcom/google/googlenav/bf;->r:I

    iput v0, p0, Lcom/google/googlenav/bg;->r:I

    iget v0, p1, Lcom/google/googlenav/bf;->s:I

    iput v0, p0, Lcom/google/googlenav/bg;->s:I

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->t:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->t:Z

    iget-object v0, p1, Lcom/google/googlenav/bf;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p1, Lcom/google/googlenav/bf;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/googlenav/bf;->w:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->z:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->x:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->y:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->y:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->A:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->z:Z

    iget-object v0, p1, Lcom/google/googlenav/bf;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p1, Lcom/google/googlenav/bf;->x:Ljava/util/Set;

    iput-object v0, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    iget v0, p1, Lcom/google/googlenav/bf;->C:I

    iput v0, p0, Lcom/google/googlenav/bg;->C:I

    iget-object v0, p1, Lcom/google/googlenav/bf;->D:LaW/R;

    iput-object v0, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    iget-object v0, p1, Lcom/google/googlenav/bf;->E:LaN/H;

    iput-object v0, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->F:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->F:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->G:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->G:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->H:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->H:Z

    iget-boolean v0, p1, Lcom/google/googlenav/bf;->I:Z

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->I:Z

    iget-object v0, p1, Lcom/google/googlenav/bf;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/bf;
    .locals 38

    new-instance v1, Lcom/google/googlenav/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/bg;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/bg;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/googlenav/bg;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/googlenav/bg;->e:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/bg;->f:LaN/H;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/googlenav/bg;->j:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/bg;->l:LaN/M;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/googlenav/bg;->m:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/googlenav/bg;->n:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->p:LaN/B;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->q:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->r:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->s:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->t:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->x:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->y:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->z:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->A:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/googlenav/bg;->C:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->D:LaW/R;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->E:LaN/H;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->F:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->G:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->H:Z

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/googlenav/bg;->I:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-direct/range {v1 .. v37}, Lcom/google/googlenav/bf;-><init>(Ljava/lang/String;IIIILaN/H;Ljava/lang/String;Lcom/google/googlenav/ba;Lcom/google/googlenav/bb;ILjava/lang/String;LaN/M;ZZZLaN/B;ZIIZ[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/util/Map;ZZZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;ILaW/R;LaN/H;ZZZZLjava/lang/String;)V

    return-object v1
.end method

.method public a(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->b:I

    return-object p0
.end method

.method public a(LaN/B;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->p:LaN/B;

    return-object p0
.end method

.method public a(LaN/H;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->f:LaN/H;

    return-object p0
.end method

.method public a(LaN/M;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->l:LaN/M;

    return-object p0
.end method

.method public a(LaW/R;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->D:LaW/R;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->h:Lcom/google/googlenav/ba;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->i:Lcom/google/googlenav/bb;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->w:Ljava/util/Map;

    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->B:Ljava/util/Set;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->m:Z

    return-object p0
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->u:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->c:I

    return-object p0
.end method

.method public b(LaN/H;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->E:LaN/H;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->g:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->n:Z

    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->d:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->k:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->o:Z

    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->e:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->v:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->q:Z

    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->j:I

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bg;->J:Ljava/lang/String;

    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->t:Z

    return-object p0
.end method

.method public f(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->s:I

    return-object p0
.end method

.method public f(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->y:Z

    return-object p0
.end method

.method public g(I)Lcom/google/googlenav/bg;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/bg;->C:I

    return-object p0
.end method

.method public g(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->x:Z

    return-object p0
.end method

.method public h(Z)Lcom/google/googlenav/bg;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bg;->z:Z

    return-object p0
.end method

.method public i(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->F:Z

    return-object p0
.end method

.method public j(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->G:Z

    return-object p0
.end method

.method public k(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->H:Z

    return-object p0
.end method

.method public l(Z)Lcom/google/googlenav/bg;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/bg;->I:Z

    return-object p0
.end method
