.class public Lcom/google/googlenav/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/F;


# instance fields
.field private a:Lax/y;

.field private b:B

.field private c:I

.field private final d:Ljava/util/List;

.field private e:Lcom/google/googlenav/layer/m;

.field private f:Lcom/google/googlenav/T;

.field private g:Lax/y;

.field private h:Z

.field private i:I

.field private final j:Ljava/util/ArrayList;

.field private k:Ljava/lang/String;

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/Y;->e:Lcom/google/googlenav/layer/m;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/Y;->j:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/Y;->l:I

    return-void
.end method

.method public constructor <init>(Lax/y;Ljava/lang/String;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iput-object v0, p0, Lcom/google/googlenav/Y;->e:Lcom/google/googlenav/layer/m;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/Y;->j:Ljava/util/ArrayList;

    iput v1, p0, Lcom/google/googlenav/Y;->l:I

    iput-object p1, p0, Lcom/google/googlenav/Y;->a:Lax/y;

    iput-object p2, p0, Lcom/google/googlenav/Y;->k:Ljava/lang/String;

    iput v1, p0, Lcom/google/googlenav/Y;->c:I

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/Y;I)I
    .locals 0

    iput p1, p0, Lcom/google/googlenav/Y;->i:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/Y;)Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->g:Lax/y;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/Y;Lax/y;)Lax/y;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/Y;->g:Lax/y;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/Y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-virtual {p2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/Y;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/Y;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/Y;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->j:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/Y;->l:I

    return v0
.end method

.method public a(B)V
    .locals 0

    iput-byte p1, p0, Lcom/google/googlenav/Y;->b:B

    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/Y;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/Y;->l:I

    iput p1, p0, Lcom/google/googlenav/Y;->c:I

    if-gez p1, :cond_1

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/googlenav/Y;->b:B

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-byte v0, p0, Lcom/google/googlenav/Y;->b:B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/google/googlenav/Y;->b:B

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/T;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    return-void
.end method

.method public a(Lcom/google/googlenav/cp;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/google/googlenav/layer/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/Y;->e:Lcom/google/googlenav/layer/m;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/Y;->k:Ljava/lang/String;

    return-void
.end method

.method public b()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->a:Lax/y;

    return-object v0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .locals 3

    const/4 v0, 0x0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/E;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    invoke-virtual {v2}, Lcom/google/googlenav/T;->f()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/T;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/Y;->c:I

    return v0
.end method

.method public c(I)I
    .locals 0

    return p1
.end method

.method public d()B
    .locals 1

    iget-byte v0, p0, Lcom/google/googlenav/Y;->b:B

    return v0
.end method

.method public e()Lcom/google/googlenav/E;
    .locals 2

    iget-byte v0, p0, Lcom/google/googlenav/Y;->b:B

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/Y;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/Y;->c:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/Y;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    invoke-virtual {v1}, Lcom/google/googlenav/T;->f()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public g()Lax/y;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->g:Lax/y;

    return-object v0
.end method

.method public h()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/Y;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/Y;->i:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->k:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public l()Lcom/google/googlenav/layer/m;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->e:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/T;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    return-object v0
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->f:Lcom/google/googlenav/T;

    invoke-virtual {v0}, Lcom/google/googlenav/T;->a()V

    return-void
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/Y;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
