.class public Lcom/google/googlenav/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/m;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/m;)V
    .locals 2

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/l;->a:Lcom/google/googlenav/m;

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aS;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/googlenav/l;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/l;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(II)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aS;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/l;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/l;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/google/googlenav/l;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/l;->a:Lcom/google/googlenav/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/l;->a:Lcom/google/googlenav/m;

    invoke-interface {v0, p1}, Lcom/google/googlenav/m;->a(Ljava/io/DataInput;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x49

    return v0
.end method

.method public s_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "ClientFeatureRestrictionsRequest"

    return-object v0
.end method

.method public u_()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/l;->a:Lcom/google/googlenav/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/l;->a:Lcom/google/googlenav/m;

    invoke-interface {v0}, Lcom/google/googlenav/m;->a()V

    :cond_0
    return-void
.end method

.method public y_()V
    .locals 1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    return-void
.end method
