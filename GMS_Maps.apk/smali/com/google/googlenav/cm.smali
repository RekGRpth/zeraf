.class public Lcom/google/googlenav/cm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/googlenav/co;

.field private final b:Z

.field private final c:Lcom/google/googlenav/bZ;

.field private final d:[Lcom/google/googlenav/bZ;

.field private e:[Lcom/google/googlenav/cb;

.field private final f:[I

.field private final g:Lcom/google/googlenav/cl;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/cl;[Lcom/google/googlenav/co;ZLcom/google/googlenav/bZ;[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    iput-object p2, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    iput-boolean p3, p0, Lcom/google/googlenav/cm;->b:Z

    iput-object p4, p0, Lcom/google/googlenav/cm;->c:Lcom/google/googlenav/bZ;

    iput-object p5, p0, Lcom/google/googlenav/cm;->f:[I

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/googlenav/bZ;

    iput-object v0, p0, Lcom/google/googlenav/cm;->d:[Lcom/google/googlenav/bZ;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/bZ;Lcom/google/googlenav/cd;)V
    .locals 9

    const/4 v8, 0x7

    const/4 v2, 0x6

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    new-instance v2, Lcom/google/googlenav/cl;

    invoke-direct {v2, v0, p3}, Lcom/google/googlenav/cl;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cd;)V

    iput-object v2, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/co;

    iput-object v0, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    move v0, v1

    move v2, v1

    :goto_1
    iget-object v4, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    array-length v4, v4

    if-ge v0, v4, :cond_3

    iget-object v4, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    new-instance v5, Lcom/google/googlenav/co;

    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-direct {v5, v6, p0}, Lcom/google/googlenav/co;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/cm;)V

    aput-object v5, v4, v0

    iget-object v4, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    aget-object v4, v4, v0

    invoke-static {v4}, Lcom/google/googlenav/co;->a(Lcom/google/googlenav/co;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-interface {p3, v0}, Lcom/google/googlenav/cd;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iput-boolean v2, p0, Lcom/google/googlenav/cm;->b:Z

    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/googlenav/cm;->f:[I

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_4

    iget-object v3, p0, Lcom/google/googlenav/cm;->f:[I

    invoke-virtual {p1, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    new-array v0, v1, [Lcom/google/googlenav/bZ;

    iput-object v0, p0, Lcom/google/googlenav/cm;->d:[Lcom/google/googlenav/bZ;

    iput-object p2, p0, Lcom/google/googlenav/cm;->c:Lcom/google/googlenav/bZ;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/cm;)Lcom/google/googlenav/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/cm;)[Lcom/google/googlenav/co;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/cm;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cm;->b:Z

    return v0
.end method

.method static synthetic d(Lcom/google/googlenav/cm;)[I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->f:[I

    return-object v0
.end method


# virtual methods
.method public a(Z)Lcom/google/googlenav/cc;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/googlenav/cm;->b:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    if-eqz p1, :cond_3

    invoke-static {v1}, Lcom/google/googlenav/co;->a(Lcom/google/googlenav/co;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lcom/google/googlenav/co;->a()Lcom/google/googlenav/cc;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object v0, v1

    goto :goto_0
.end method

.method public a(I)Lcom/google/googlenav/co;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/co;
    .locals 5

    iget-object v2, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-virtual {v0}, Lcom/google/googlenav/co;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/cm;->b:Z

    return v0
.end method

.method public b(Z)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/cm;->a(Z)Lcom/google/googlenav/cc;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/cc;->f()J

    move-result-wide v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/googlenav/cm;->e:[Lcom/google/googlenav/cb;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    invoke-virtual {v0}, Lcom/google/googlenav/cb;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ce;

    invoke-virtual {v0}, Lcom/google/googlenav/ce;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b()[Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->d:[Lcom/google/googlenav/bZ;

    return-object v0
.end method

.method public c(Z)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/cm;->a(Z)Lcom/google/googlenav/cc;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/cc;->b(Lcom/google/googlenav/cc;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->a:[Lcom/google/googlenav/co;

    array-length v0, v0

    return v0
.end method

.method public d(Z)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/googlenav/cm;->a(Z)Lcom/google/googlenav/cc;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/cc;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->c:Lcom/google/googlenav/bZ;

    return-object v0
.end method

.method public f()[Lcom/google/googlenav/cb;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->e:[Lcom/google/googlenav/cb;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()J
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public j()J
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->e()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->f()I

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->g()Z

    move-result v0

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cm;->g:Lcom/google/googlenav/cl;

    invoke-virtual {v0}, Lcom/google/googlenav/cl;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
