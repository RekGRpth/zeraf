.class public Lcom/google/googlenav/settings/LegalActivity;
.super Lcom/google/googlenav/settings/GmmPreferenceActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/aR;


# instance fields
.field private b:Landroid/preference/PreferenceScreen;

.field private c:Landroid/preference/Preference;

.field private d:Ljava/lang/String;

.field private e:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/LegalActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/aP;

    invoke-direct {v0, p1, p0}, Lcom/google/googlenav/aP;-><init>(Ljava/lang/String;Lcom/google/googlenav/aR;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    const-string v1, ""

    const/16 v2, 0x258

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/googlenav/settings/D;

    invoke-direct {v5, p0, v0}, Lcom/google/googlenav/settings/D;-><init>(Lcom/google/googlenav/settings/LegalActivity;Lcom/google/googlenav/aP;)V

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->e:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/settings/LegalActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;[B)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/settings/LegalActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/settings/E;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/google/googlenav/settings/E;-><init>(Lcom/google/googlenav/settings/LegalActivity;Ljava/lang/String;[BLcom/google/googlenav/settings/D;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LegalActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x252

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LegalActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f060007

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LegalActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LegalActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "terms"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x5a6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "privacy"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x3ea

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "notices"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/16 v1, 0x253

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "web_history"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->c:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->c:Landroid/preference/Preference;

    const/16 v1, 0x617

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, "terms"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/K;->W()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v2, "privacy"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/K;->U()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v2, "notices"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/K;->V()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/settings/LegalActivity;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v2, "web_history"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->X()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onResume()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/LegalActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/LegalActivity;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/googlenav/settings/LegalActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method
