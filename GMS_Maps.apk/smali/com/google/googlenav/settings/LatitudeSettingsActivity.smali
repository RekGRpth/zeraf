.class public Lcom/google/googlenav/settings/LatitudeSettingsActivity;
.super Lcom/google/googlenav/settings/GmmPreferenceActivity;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/Map;


# instance fields
.field private A:Landroid/preference/CheckBoxPreference;

.field private c:Landroid/preference/CheckBoxPreference;

.field private d:Landroid/preference/ListPreference;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/CheckBoxPreference;

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/CheckBoxPreference;

.field private i:Landroid/preference/CheckBoxPreference;

.field private j:Z

.field private k:Lcom/google/googlenav/friend/j;

.field private l:Lcom/google/googlenav/settings/s;

.field private m:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private n:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private o:Landroid/os/Handler;

.field private p:Lcom/google/googlenav/settings/q;

.field private q:Landroid/widget/Toast;

.field private r:Landroid/widget/Toast;

.field private s:Landroid/widget/Toast;

.field private t:Landroid/preference/Preference;

.field private u:Landroid/preference/Preference;

.field private v:Landroid/preference/Preference;

.field private w:Landroid/preference/PreferenceGroup;

.field private x:Landroid/preference/PreferenceGroup;

.field private y:Landroid/preference/PreferenceGroup;

.field private z:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;
    .locals 6

    const/4 v4, 0x0

    if-ltz p3, :cond_1

    invoke-static {p3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-ltz p4, :cond_0

    invoke-static {p4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/preference/Preference;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v3, v4

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/preference/Preference;
    .locals 2

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    invoke-virtual {v0, p5}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {v0, p3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;I)Landroid/preference/PreferenceGroup;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    if-ltz p2, :cond_0

    invoke-static {p2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/q;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->p:Lcom/google/googlenav/settings/q;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    const-string v0, "off"

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    new-instance v1, Lcom/google/googlenav/settings/k;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/k;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_0
    const-string v0, "automatic"

    goto :goto_1

    :pswitch_1
    const-string v0, "manual"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/settings/p;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    iget v2, v0, Lcom/google/googlenav/settings/p;->a:I

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    iget-object v0, v0, Lcom/google/googlenav/settings/p;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->q:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/settings/s;->a(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/ListPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    return-object v0
.end method

.method private b()V
    .locals 11

    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v5, 0x0

    const-string v0, "location_reporting"

    const/16 v1, 0x56

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;I)Landroid/preference/PreferenceGroup;

    move-result-object v1

    const-string v2, "reporting_type"

    const/16 v0, 0x29a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v10, [Ljava/lang/String;

    const-string v6, ""

    aput-object v6, v3, v5

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    sget-object v0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/settings/p;

    iget v7, v0, Lcom/google/googlenav/settings/p;->a:I

    iget-object v8, v0, Lcom/google/googlenav/settings/p;->b:Ljava/lang/String;

    aput-object v8, v3, v7

    iget v0, v0, Lcom/google/googlenav/settings/p;->a:I

    aput-object v2, v5, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    invoke-virtual {v0, v5}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    const/16 v2, 0x29d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    const-string v7, "enable_reporting"

    const/16 v8, 0x29b

    const/16 v9, 0x29c

    move-object v5, p0

    move-object v6, v1

    invoke-direct/range {v5 .. v10}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/googlenav/settings/o;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/settings/o;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Lcom/google/googlenav/settings/g;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    const-string v1, "automatic"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->r:Landroid/widget/Toast;

    return-object v0
.end method

.method private c()V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "location_sharing"

    const/16 v1, 0x218

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;I)Landroid/preference/PreferenceGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    const-string v2, "enable_location_sharing"

    const/16 v3, 0x29e

    const/16 v4, 0x29f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->i:Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/google/googlenav/settings/g;

    invoke-direct {v0, p0}, Lcom/google/googlenav/settings/g;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->n:Landroid/preference/Preference$OnPreferenceChangeListener;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    const-string v2, "loading_location_sharing"

    const/16 v3, 0x181

    const/4 v4, -0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->v:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    const-string v2, "manage_location_sharing"

    const/16 v3, 0x290

    const/16 v4, 0x291

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->e:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->e:Landroid/preference/Preference;

    new-instance v1, Lcom/google/googlenav/settings/h;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/h;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->q:Landroid/widget/Toast;

    return-object v0
.end method

.method private d()V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "checkins"

    const/16 v1, 0x87

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;I)Landroid/preference/PreferenceGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iget-boolean v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->j:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    const-string v2, "loading_checkins"

    const/16 v3, 0x181

    const/4 v4, -0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->t:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    const-string v2, "automatic_checkins"

    const/16 v3, 0x74

    const/16 v4, 0x75

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    const-string v2, "checkin_notifications"

    const/16 v3, 0x221

    const/16 v4, 0x222

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->z:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    const-string v2, "rating_reminders"

    const/16 v3, 0x223

    const/16 v4, 0x224

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->A:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    const-string v2, "manage_places"

    const/16 v3, 0x82

    const/16 v4, 0x83

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g:Landroid/preference/Preference;

    new-instance v1, Lcom/google/googlenav/settings/i;

    invoke-direct {v1, p0}, Lcom/google/googlenav/settings/i;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->z:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->A:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->i:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private e()V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "location_history"

    const/16 v1, 0x264

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Ljava/lang/String;I)Landroid/preference/PreferenceGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->y:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->y:Landroid/preference/PreferenceGroup;

    const-string v2, "enable_location_history"

    const/16 v3, 0x26b

    const/16 v4, 0x26c

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->h:Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/google/googlenav/settings/j;

    invoke-direct {v0, p0}, Lcom/google/googlenav/settings/j;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)V

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->m:Landroid/preference/Preference$OnPreferenceChangeListener;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->y:Landroid/preference/PreferenceGroup;

    const-string v2, "loading_location_history"

    const/16 v3, 0x181

    const/4 v4, -0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a(Landroid/preference/PreferenceGroup;Ljava/lang/String;IIZ)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->u:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->y:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/friend/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->k:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method private static f()Ljava/util/Map;
    .locals 8

    const/16 v7, 0x17a

    const/16 v6, 0x177

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "automatic"

    new-instance v2, Lcom/google/googlenav/settings/p;

    const/4 v3, 0x0

    const/16 v4, 0x176

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/settings/p;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "manual"

    new-instance v2, Lcom/google/googlenav/settings/p;

    const/4 v3, 0x1

    const/16 v4, 0x17c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x17d

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x17e

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/settings/p;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "off"

    new-instance v2, Lcom/google/googlenav/settings/p;

    const/4 v3, 0x2

    const/16 v4, 0x179

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/settings/p;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Lcom/google/googlenav/settings/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->j:Z

    return v0
.end method

.method static synthetic j(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->t:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->g:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic m(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->z:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic n(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->u:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic o(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->y:Landroid/preference/PreferenceGroup;

    return-object v0
.end method

.method static synthetic p(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->h:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic q(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference$OnPreferenceChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->m:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-object v0
.end method

.method static synthetic r(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->v:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic s(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->w:Landroid/preference/PreferenceGroup;

    return-object v0
.end method

.method static synthetic t(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/preference/Preference$OnPreferenceChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->n:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-object v0
.end method

.method static synthetic u(Lcom/google/googlenav/settings/LatitudeSettingsActivity;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->s:Landroid/widget/Toast;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/16 v2, 0x29d

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    const v1, 0x7f020288

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f060006

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->j:Z

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aw()Lcom/google/googlenav/friend/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->k:Lcom/google/googlenav/friend/j;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->o:Landroid/os/Handler;

    new-instance v0, Lcom/google/googlenav/settings/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/settings/l;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Lcom/google/googlenav/settings/g;)V

    new-instance v1, Lcom/google/googlenav/friend/reporting/f;

    invoke-virtual {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/friend/reporting/u;->a(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/friend/reporting/f;-><init>(Ljavax/crypto/SecretKey;)V

    invoke-static {p0, v1}, Lcom/google/googlenav/friend/reporting/s;->a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/settings/s;

    iget-object v3, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->k:Lcom/google/googlenav/friend/j;

    iget-object v4, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->o:Landroid/os/Handler;

    invoke-direct {v2, v3, v4, v1, v0}, Lcom/google/googlenav/settings/s;-><init>(Lcom/google/googlenav/friend/j;Landroid/os/Handler;Lcom/google/googlenav/friend/reporting/s;Lcom/google/googlenav/settings/B;)V

    iput-object v2, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    new-instance v0, Lcom/google/googlenav/settings/q;

    iget-object v1, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->r()Lcom/google/googlenav/friend/ag;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/settings/q;-><init>(Lcom/google/googlenav/settings/LatitudeSettingsActivity;Lcom/google/googlenav/friend/ag;)V

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->p:Lcom/google/googlenav/settings/q;

    const/16 v0, 0x15a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->q:Landroid/widget/Toast;

    const/16 v0, 0x159

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->r:Landroid/widget/Toast;

    const/16 v0, 0x512

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->s:Landroid/widget/Toast;

    invoke-direct {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->b()V

    invoke-direct {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->c()V

    invoke-direct {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d()V

    invoke-direct {p0}, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->e()V

    iget-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    invoke-virtual {v0}, Lcom/google/googlenav/settings/s;->a()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->a:Lcom/google/googlenav/ui/s;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->r:Landroid/widget/Toast;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->s:Landroid/widget/Toast;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->l:Lcom/google/googlenav/settings/s;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->d:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->k:Lcom/google/googlenav/friend/j;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->h:Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->x:Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/google/googlenav/settings/LatitudeSettingsActivity;->p:Lcom/google/googlenav/settings/q;

    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onDestroy()V

    return-void
.end method
