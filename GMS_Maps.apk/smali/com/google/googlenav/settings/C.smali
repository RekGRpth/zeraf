.class public Lcom/google/googlenav/settings/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/friend/ae;

.field private b:Lcom/google/googlenav/friend/i;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/C;Lcom/google/googlenav/friend/i;)Lcom/google/googlenav/friend/i;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/settings/C;->b:Lcom/google/googlenav/friend/i;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/settings/C;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/settings/C;->c:Ljava/lang/Boolean;

    return-object p1
.end method


# virtual methods
.method public a()Lcom/google/googlenav/friend/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/C;->a:Lcom/google/googlenav/friend/ae;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/friend/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/settings/C;->a:Lcom/google/googlenav/friend/ae;

    return-void
.end method

.method public b()Lcom/google/googlenav/friend/i;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/settings/C;->b:Lcom/google/googlenav/friend/i;

    return-object v0
.end method

.method public c()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/settings/C;->b:Lcom/google/googlenav/friend/i;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/settings/C;->a:Lcom/google/googlenav/friend/ae;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method d()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/settings/C;->a:Lcom/google/googlenav/friend/ae;

    iput-object v0, p0, Lcom/google/googlenav/settings/C;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/googlenav/settings/C;->b:Lcom/google/googlenav/friend/i;

    return-void
.end method
