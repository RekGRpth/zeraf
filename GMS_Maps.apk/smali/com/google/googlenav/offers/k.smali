.class public Lcom/google/googlenav/offers/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaM/h;


# static fields
.field private static a:Lcom/google/googlenav/offers/k;


# instance fields
.field private b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:Lcom/google/googlenav/android/aa;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/android/aa;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/googlenav/offers/k;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lcom/google/googlenav/offers/k;->c:Lcom/google/googlenav/android/aa;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, LaM/j;->a([Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(Lcom/google/googlenav/android/aa;)Lcom/google/googlenav/offers/k;
    .locals 1

    sget-object v0, Lcom/google/googlenav/offers/k;->a:Lcom/google/googlenav/offers/k;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/offers/k;

    invoke-direct {v0, p0}, Lcom/google/googlenav/offers/k;-><init>(Lcom/google/googlenav/android/aa;)V

    sput-object v0, Lcom/google/googlenav/offers/k;->a:Lcom/google/googlenav/offers/k;

    :cond_0
    sget-object v0, Lcom/google/googlenav/offers/k;->a:Lcom/google/googlenav/offers/k;

    return-object v0
.end method

.method public static g()Lcom/google/android/apps/common/offerslib/a;
    .locals 7

    new-instance v0, Lcom/google/android/apps/common/offerslib/a;

    const-string v1, "gmm"

    const-string v2, "6.14.3"

    new-instance v3, Lcom/google/android/apps/common/offerslib/b;

    const/16 v4, 0x2a5

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x302

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x5f1

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/common/offerslib/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->af()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/K;->ah()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v6, 0x1

    :goto_0
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/common/offerslib/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/common/offerslib/b;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static h()Lcom/google/googlenav/offers/k;
    .locals 1

    sget-object v0, Lcom/google/googlenav/offers/k;->a:Lcom/google/googlenav/offers/k;

    return-object v0
.end method


# virtual methods
.method public C_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/offers/k;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/offers/k;->c:Lcom/google/googlenav/android/aa;

    new-instance v1, Lcom/google/googlenav/offers/l;

    invoke-direct {v1, p0}, Lcom/google/googlenav/offers/l;-><init>(Lcom/google/googlenav/offers/k;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 0

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/offers/k;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/offers/k;->g()Lcom/google/android/apps/common/offerslib/a;

    move-result-object v1

    invoke-static {v0}, Lcom/google/googlenav/offers/k;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Context;Lcom/google/android/apps/common/offerslib/a;Landroid/accounts/Account;)V

    invoke-static {v0}, Lcom/google/android/apps/common/offerslib/w;->a(Landroid/content/Context;)Lcom/google/android/apps/common/offerslib/w;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/w;->a(Lcom/google/android/apps/common/offerslib/a;)V

    :cond_0
    return-void
.end method
