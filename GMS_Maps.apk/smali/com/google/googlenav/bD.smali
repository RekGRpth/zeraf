.class Lcom/google/googlenav/bD;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/googlenav/bC;


# direct methods
.method constructor <init>(Lcom/google/googlenav/bC;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    iput p2, p0, Lcom/google/googlenav/bD;->a:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const-string v0, "Stars$StarsCacheUpdater.runNextTaskInBackground$1.run"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->c(Lcom/google/googlenav/bC;)LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    new-instance v2, Lcom/google/common/collect/aw;

    invoke-direct {v2}, Lcom/google/common/collect/aw;-><init>()V

    new-instance v3, Lcom/google/common/collect/ay;

    invoke-direct {v3}, Lcom/google/common/collect/ay;-><init>()V

    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v0}, Lcom/google/googlenav/bz;->a(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    invoke-virtual {v0}, LaR/D;->h()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v0, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    iget v1, p0, Lcom/google/googlenav/bD;->a:I

    new-instance v4, Lcom/google/googlenav/bB;

    invoke-virtual {v2}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lcom/google/googlenav/bB;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ax;)V

    invoke-static {v0, v1, v4}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;ILcom/google/googlenav/bB;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v0, "Stars$StarsCacheUpdater.runNextTaskInBackground$1.run"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1
.end method
