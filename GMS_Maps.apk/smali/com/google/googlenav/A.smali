.class public Lcom/google/googlenav/A;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static j:J


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lax/k;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private final k:LaH/m;

.field private final l:LaN/u;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/google/googlenav/A;->j:J

    return-void
.end method

.method public constructor <init>(LaH/m;LaN/u;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/A;->m:Z

    iput-object p1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    iput-object p2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    iput-object p3, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/A;)Lcom/google/googlenav/J;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/A;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lax/y;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lax/y;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/z;->a(Lax/y;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V
    .locals 7

    new-instance v0, Lcom/google/googlenav/ai;

    const/16 v6, 0xa

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    const/4 v2, -0x1

    if-ne p6, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->a()I

    move-result p6

    :cond_0
    const/4 v2, -0x1

    if-ne p7, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->b()I

    move-result p7

    :cond_1
    invoke-static {v1, v0, p6, p7}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bk;->bN()V

    return-void
.end method

.method private static b(Lax/k;)Z
    .locals 1

    invoke-interface {p0}, Lax/k;->aq()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {p0}, Lax/k;->as()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()LaN/H;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->m()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    goto :goto_1
.end method

.method private f()V
    .locals 5

    const/16 v4, 0x5f3

    iget-object v1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    invoke-virtual {p0}, Lcom/google/googlenav/A;->c()V

    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v2}, LaH/h;->b()Lo/D;

    move-result-object v2

    invoke-static {v0, v2}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    :cond_0
    invoke-interface {v1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v1, v0}, Lax/k;->a(Lax/y;)V

    :cond_2
    :goto_0
    instance-of v0, v1, Lax/b;

    if-nez v0, :cond_7

    new-instance v0, Lax/s;

    invoke-direct {v0, v1}, Lax/s;-><init>(Lax/k;)V

    :goto_1
    invoke-interface {v0}, Lax/k;->aq()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lax/k;->as()Lax/y;

    move-result-object v1

    if-nez v1, :cond_6

    :cond_3
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    check-cast v0, Lax/b;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->c(Lax/b;)V

    :goto_2
    return-void

    :cond_4
    invoke-interface {v1}, Lax/k;->as()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lax/k;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v0, :cond_5

    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :cond_5
    invoke-interface {v1, v0}, Lax/k;->b(Lax/y;)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    check-cast v0, Lax/b;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/b;)V

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Lax/k;)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/A;->c()V

    iput-object p1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/A;->a(Lax/y;)V

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/A;->a(Lax/y;)V

    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/A;->b(Lax/k;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/A;->f()V

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sget-wide v2, Lcom/google/googlenav/A;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x292

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .locals 8

    const/4 v6, -0x1

    iget-object v0, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v5

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/c;IIZZZ)V
    .locals 8

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    const/4 v1, -0x1

    if-ne p5, v1, :cond_0

    const/4 v1, -0x1

    if-eq p6, v1, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1, p5, p6}, LaN/u;->d(II)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p4}, Lcom/google/googlenav/c;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x4f4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    goto :goto_0

    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " loc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Lcom/google/googlenav/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {p4}, Lcom/google/googlenav/c;->b()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/u;->c(LaN/B;)V

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->e()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct {p0}, Lcom/google/googlenav/A;->e()LaN/H;

    move-result-object v7

    :goto_2
    if-eqz v7, :cond_5

    invoke-virtual {v7}, LaN/H;->a()LaN/B;

    move-result-object v1

    if-nez v1, :cond_6

    :cond_5
    iput-object p1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    sget-wide v3, Lcom/google/googlenav/A;->j:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/googlenav/A;->i:J

    new-instance v3, Lcom/google/googlenav/B;

    invoke-direct {v3, p0}, Lcom/google/googlenav/B;-><init>(Lcom/google/googlenav/A;)V

    const/16 v1, 0x4fc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    new-instance v4, Lcom/google/googlenav/ui/wizard/A;

    const/16 v5, 0x12

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    const-wide/16 v5, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    move-object v1, v7

    goto/16 :goto_1

    :cond_6
    move-object v1, v7

    goto/16 :goto_1

    :cond_7
    move-object v7, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a()Z
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/googlenav/A;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 9

    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v8, 0x1

    if-ne p5, v1, :cond_0

    if-eq p6, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1, p5, p6}, LaN/u;->d(II)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/c;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iput-object p2, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    move v0, v8

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/googlenav/c;->b()LaN/B;

    move-result-object v5

    :goto_1
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V

    invoke-virtual {p0}, Lcom/google/googlenav/A;->d()V

    move v0, v8

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v5

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v5

    goto :goto_1

    :cond_3
    iput-object p2, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sget-wide v2, Lcom/google/googlenav/A;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    move v0, v8

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public b()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/googlenav/A;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v4}, LaH/m;->g()Z

    move-result v4

    if-nez v4, :cond_0

    iget-wide v4, p0, Lcom/google/googlenav/A;->i:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v2}, Lcom/google/googlenav/J;->a()V

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    iget-object v3, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    iget-object v3, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    iget-object v3, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0x4f4

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/A;->e()LaN/H;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :cond_3
    :goto_0
    return v0

    :cond_4
    iget-object v1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/googlenav/A;->f()V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public c()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/A;->e:Lax/k;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    return-void
.end method
