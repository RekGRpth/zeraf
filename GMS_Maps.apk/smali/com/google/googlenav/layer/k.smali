.class public Lcom/google/googlenav/layer/k;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:LaN/H;

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/googlenav/layer/l;

.field private f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(LaN/H;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/layer/k;->a:LaN/H;

    iput p2, p0, Lcom/google/googlenav/layer/k;->b:I

    iput p3, p0, Lcom/google/googlenav/layer/k;->c:I

    iput-object p4, p0, Lcom/google/googlenav/layer/k;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/layer/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/layer/k;->e:Lcom/google/googlenav/layer/l;

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 5

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dB;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/f;->e()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/f;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/f;->g()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_2

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget-object v1, p0, Lcom/google/googlenav/layer/k;->a:LaN/H;

    invoke-virtual {v1}, LaN/H;->a()LaN/B;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/layer/k;->b:I

    iget v3, p0, Lcom/google/googlenav/layer/k;->c:I

    iget-object v4, p0, Lcom/google/googlenav/layer/k;->a:LaN/H;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/googlenav/layer/k;->d:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/layer/k;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dB;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/layer/k;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x39

    return v0
.end method

.method public d_()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/layer/k;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/layer/k;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lcom/google/googlenav/layer/k;->e:Lcom/google/googlenav/layer/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/layer/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/layer/l;)V

    goto :goto_0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
