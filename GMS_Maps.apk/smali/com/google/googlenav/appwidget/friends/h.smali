.class final Lcom/google/googlenav/appwidget/friends/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaB/p;
.implements Lbf/f;
.implements Lcom/google/googlenav/bb;


# instance fields
.field final synthetic a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/appwidget/friends/h;-><init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)V

    return-void
.end method

.method private b()V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v5}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)LaB/a;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v5

    if-nez v5, :cond_2

    new-instance v5, LaB/n;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v5, v6, v0}, LaB/n;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v2, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)LaB/a;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, LaB/a;->a(Ljava/util/Vector;LaB/p;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/googlenav/appwidget/friends/h;->Q_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private c(Lcom/google/googlenav/aZ;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v0, Lcom/google/googlenav/friend/aK;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/aK;-><init>(Lcom/google/googlenav/F;)V

    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/b;->a(Lcom/google/googlenav/friend/aK;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/friend/aI;

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, v4, v4}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/friend/aI;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public Q_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    sget-object v1, Lcom/google/googlenav/appwidget/friends/g;->d:Lcom/google/googlenav/appwidget/friends/g;

    invoke-static {v0, v1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/g;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/F;)V
    .locals 1

    check-cast p1, Lcom/google/googlenav/aZ;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/appwidget/friends/h;->a(Lcom/google/googlenav/aZ;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/appwidget/friends/h;->c(Lcom/google/googlenav/aZ;)V

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/h;->b()V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/appwidget/friends/a;->a(Landroid/content/Context;J)V

    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Lbf/a;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2, v2}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/h;->a:Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    sget-object v1, Lcom/google/googlenav/appwidget/friends/g;->e:Lcom/google/googlenav/appwidget/friends/g;

    invoke-static {v0, v1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/g;)V

    return-void
.end method
