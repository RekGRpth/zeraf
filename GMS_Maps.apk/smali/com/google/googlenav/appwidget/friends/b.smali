.class public final Lcom/google/googlenav/appwidget/friends/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I

.field private static final b:[[I

.field private static final c:I


# instance fields
.field private final d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x6

    const/4 v0, 0x3

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    sget-object v0, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    array-length v0, v0

    sput v0, Lcom/google/googlenav/appwidget/friends/b;->a:I

    sget v0, Lcom/google/googlenav/appwidget/friends/b;->a:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/googlenav/appwidget/friends/b;->c:I

    return-void

    :array_0
    .array-data 4
        0x7f1001cc
        0x7f1001cd
        0x7f1001ce
        0x7f1001cf
        0x7f1001d1
        0x7f1001d0
    .end array-data

    :array_1
    .array-data 4
        0x7f1001d2
        0x7f1001d3
        0x7f1001d4
        0x7f1001d5
        0x7f1001d7
        0x7f1001d6
    .end array-data

    :array_2
    .array-data 4
        0x7f1001d8
        0x7f1001d9
        0x7f1001da
        0x7f1001db
        0x7f1001dd
        0x7f1001dc
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    return-void
.end method

.method private a()Landroid/app/PendingIntent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    const-class v2, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_MANUAL"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/friend/aI;)Landroid/net/Uri;
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "latitude"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/friends/location"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/friend/aK;)Ljava/util/ArrayList;
    .locals 7

    const/4 v0, 0x0

    sget-object v1, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    array-length v1, v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aK;->i()Lcom/google/googlenav/common/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/f;->b()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/f;->a(I)J

    move-result-wide v5

    long-to-int v5, v5

    invoke-virtual {p0, v5}, Lcom/google/googlenav/friend/aK;->a(I)Lcom/google/googlenav/friend/aI;

    move-result-object v5

    invoke-static {v5}, Lcom/google/googlenav/appwidget/friends/b;->c(Lcom/google/googlenav/friend/aI;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    sget-object v6, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    array-length v6, v6

    if-ne v5, v6, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-object v1, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    array-length v1, v1

    :goto_1
    if-ge v0, v1, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-object v2
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 5

    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/googlenav/appwidget/friends/b;->b(Lcom/google/googlenav/friend/aI;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_SIGN_IN"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_MANUAL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v1, 0x7f1001df

    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-static {v2, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v0, 0x7f1001e0

    const v1, 0x7f0201d2

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v0, 0x7f1001e1

    const/16 v1, 0x22d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f1001e3

    invoke-virtual {p1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e2

    invoke-virtual {p1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e7

    invoke-virtual {p1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e6

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e5

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/b;->a()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private a(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    sget-object v0, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    array-length v0, v0

    if-ge v6, v0, :cond_0

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->c(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/b;->b()I

    move-result v0

    if-ge v6, v0, :cond_2

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->c(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->c(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    move-object v4, v0

    :goto_1
    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->d(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/b;->b()I

    move-result v0

    if-ge v6, v0, :cond_1

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->c(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->d(Lcom/google/googlenav/appwidget/friends/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v5, v0

    :goto_2
    sget-object v0, Lcom/google/googlenav/appwidget/friends/b;->b:[[I

    aget-object v2, v0, v6

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/appwidget/friends/b;->a(Landroid/widget/RemoteViews;[ILcom/google/googlenav/friend/aI;Lcom/google/googlenav/friend/aI;Landroid/graphics/Bitmap;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move-object v5, v7

    goto :goto_2

    :cond_2
    move-object v4, v7

    goto :goto_1
.end method

.method private a(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V
    .locals 7

    const/4 v6, 0x1

    const v4, 0x7f1001e4

    const v5, 0x7f1001e3

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/appwidget/friends/b;->d(Lcom/google/googlenav/friend/aI;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1001e0

    const v3, 0x7f0201cd

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v0, 0x7f1001e1

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0201cf

    invoke-virtual {p1, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->b(Lcom/google/googlenav/appwidget/friends/c;)Lcom/google/googlenav/friend/aI;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/googlenav/appwidget/friends/b;->a(Lcom/google/googlenav/friend/aI;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-static {v3, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v3, 0x7f1001df

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/googlenav/android/M;->a:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v3, "source"

    const-string v4, "cw"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-static {v3, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p1, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e2

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_1
    const v3, 0x7f1001e7

    if-eqz p3, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e6

    if-eqz p3, :cond_4

    :goto_3
    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e5

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/b;->a()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void

    :cond_0
    const v0, 0x7f0201ce

    invoke-virtual {p1, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    :cond_1
    const v0, 0x7f1001e0

    const v3, 0x7f0201d2

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v0, 0x7f1001e1

    const/16 v3, 0x22f

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0201ce

    invoke-virtual {p1, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p1, v5, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e2

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3
.end method

.method private a(Landroid/widget/RemoteViews;[ILcom/google/googlenav/friend/aI;Lcom/google/googlenav/friend/aI;Landroid/graphics/Bitmap;)V
    .locals 8

    const/4 v0, 0x0

    aget v2, p2, v0

    const/4 v0, 0x1

    aget v0, p2, v0

    const/4 v1, 0x2

    aget v3, p2, v1

    const/4 v1, 0x3

    aget v4, p2, v1

    const/4 v1, 0x4

    aget v5, p2, v1

    const/4 v1, 0x5

    aget v6, p2, v1

    if-nez p4, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-nez p5, :cond_3

    const v1, 0x7f02001f

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    const/4 v0, 0x0

    if-nez p3, :cond_4

    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_2

    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->E()LaN/B;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->E()LaN/B;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {p1, v4, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_3
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p4, v0, v1}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p1, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_4
    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lcom/google/googlenav/appwidget/friends/b;->a(Lcom/google/googlenav/friend/aI;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v0, p5}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v1

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p1, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4
.end method

.method private b()I
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/googlenav/appwidget/friends/b;->c:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/googlenav/appwidget/friends/b;->a:I

    goto :goto_0
.end method

.method private b(Lcom/google/googlenav/friend/aI;)Landroid/net/Uri;
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "latitude"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/friends/profile"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/appwidget/friends/b;->c(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/appwidget/friends/b;->d(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V

    return-void
.end method

.method private c(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->e(Lcom/google/googlenav/appwidget/friends/c;)J

    move-result-wide v1

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/16 v0, 0x22e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/googlenav/appwidget/friends/c;->e(Lcom/google/googlenav/appwidget/friends/c;)J

    move-result-wide v4

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const v1, 0x7f1001c1

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-void
.end method

.method private static c(Lcom/google/googlenav/friend/aI;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/google/googlenav/appwidget/friends/b;->d(Lcom/google/googlenav/friend/aI;)Z

    move-result v0

    goto :goto_0
.end method

.method private d(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/appwidget/friends/c;->c()Lcom/google/googlenav/friend/aI;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/googlenav/appwidget/friends/c;->c()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const v1, 0x7f1001de

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-void
.end method

.method private static d(Lcom/google/googlenav/friend/aI;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->c()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1, v1}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/google/googlenav/appwidget/friends/c;Z)Landroid/widget/RemoteViews;
    .locals 3

    invoke-static {p1}, Lcom/google/googlenav/appwidget/friends/c;->a(Lcom/google/googlenav/appwidget/friends/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040082

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/appwidget/friends/b;->a(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V

    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/appwidget/friends/b;->a(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;)V

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/appwidget/friends/b;->b(Landroid/widget/RemoteViews;Lcom/google/googlenav/appwidget/friends/c;Z)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/googlenav/appwidget/friends/b;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040085

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/b;->a(Landroid/widget/RemoteViews;)V

    goto :goto_0
.end method
