.class public Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static l:Lbm/i;


# instance fields
.field private a:Lak/a;

.field private final b:Ljava/lang/Object;

.field private c:Las/d;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Lbf/a;

.field private h:Ljava/util/List;

.field private i:Lcom/google/googlenav/friend/aI;

.field private j:LaB/a;

.field private k:Lcom/google/googlenav/appwidget/friends/b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lbm/i;

    new-instance v1, Lal/a;

    invoke-direct {v1}, Lal/a;-><init>()V

    const-string v2, "latitude widget running"

    const-string v3, "wu"

    const/16 v4, 0x16

    invoke-direct {v0, v1, v2, v3, v4}, Lbm/i;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->l:Lbm/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Lbf/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->g:Lbf/a;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/appwidget/friends/i;)Lcom/google/googlenav/appwidget/friends/c;
    .locals 9

    const-wide/high16 v5, -0x8000000000000000L

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->a:Lcom/google/googlenav/appwidget/friends/i;

    if-ne p1, v0, :cond_4

    iget-object v3, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->h:Ljava/util/List;

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->i:Lcom/google/googlenav/friend/aI;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    :goto_1
    if-ge v4, v5, :cond_2

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->j:LaB/a;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->j:LaB/a;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v6

    invoke-virtual {v6}, LaB/m;->h()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->j:LaB/a;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v0}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v0

    invoke-virtual {v0}, LaB/m;->d()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move-object v3, v7

    :cond_2
    new-instance v0, Lcom/google/googlenav/appwidget/friends/c;

    const/4 v4, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/appwidget/friends/c;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/aI;Ljava/util/List;ZJ)V

    :cond_3
    :goto_3
    return-object v0

    :cond_4
    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->c:Lcom/google/googlenav/appwidget/friends/i;

    if-ne p1, v0, :cond_5

    new-instance v0, Lcom/google/googlenav/appwidget/friends/c;

    move-object v1, v7

    move-object v2, v7

    move-object v3, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/appwidget/friends/c;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/aI;Ljava/util/List;ZJ)V

    goto :goto_3

    :cond_5
    invoke-static {}, Lcom/google/googlenav/appwidget/friends/c;->a()Lcom/google/googlenav/appwidget/friends/c;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/googlenav/appwidget/friends/c;

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c()Z

    move-result v4

    move-object v1, v7

    move-object v2, v7

    move-object v3, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/appwidget/friends/c;-><init>(Ljava/util/List;Lcom/google/googlenav/friend/aI;Ljava/util/List;ZJ)V

    goto :goto_3

    :cond_6
    move-object v1, v7

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/g;)Lcom/google/googlenav/appwidget/friends/g;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c(Lcom/google/googlenav/appwidget/friends/g;)Lcom/google/googlenav/appwidget/friends/g;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/appwidget/friends/g;)Lcom/google/googlenav/appwidget/friends/g;
    .locals 3

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->g:Lcom/google/googlenav/appwidget/friends/g;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    sget-object p1, Lcom/google/googlenav/appwidget/friends/g;->h:Lcom/google/googlenav/appwidget/friends/g;

    :cond_0
    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Law/h;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/google/googlenav/appwidget/friends/g;->e:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/friend/aI;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->i:Lcom/google/googlenav/friend/aI;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->h:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/google/googlenav/appwidget/friends/c;Z)V
    .locals 3

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->k:Lcom/google/googlenav/appwidget/friends/b;

    invoke-virtual {v2, p1, p2}, Lcom/google/googlenav/appwidget/friends/b;->a(Lcom/google/googlenav/appwidget/friends/c;Z)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method private a()Z
    .locals 3

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Las/d;
    .locals 3

    new-instance v0, Lcom/google/googlenav/appwidget/friends/e;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/appwidget/friends/e;-><init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Las/c;)V

    const-wide/16 v1, 0x7530

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/appwidget/friends/g;)V
    .locals 3

    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/appwidget/friends/d;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/appwidget/friends/d;-><init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/g;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method private c(Lcom/google/googlenav/appwidget/friends/g;)Lcom/google/googlenav/appwidget/friends/g;
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    monitor-enter v1

    :goto_0
    :try_start_0
    invoke-virtual {v1}, Law/h;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Law/h;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/g;)Lcom/google/googlenav/appwidget/friends/g;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object v1, Lcom/google/googlenav/appwidget/friends/f;->a:[I

    invoke-virtual {v0}, Lcom/google/googlenav/appwidget/friends/g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->stopSelf()V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->g:Lcom/google/googlenav/appwidget/friends/g;

    :goto_1
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->c:Lcom/google/googlenav/appwidget/friends/i;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/i;)Lcom/google/googlenav/appwidget/friends/c;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/c;Z)V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->l:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->b:Lcom/google/googlenav/appwidget/friends/i;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/i;)Lcom/google/googlenav/appwidget/friends/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/c;Z)V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->b:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    invoke-static {p0}, Lcom/google/googlenav/appwidget/friends/a;->a(Landroid/content/Context;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_1

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/google/googlenav/appwidget/friends/h;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/appwidget/friends/h;-><init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/d;)V

    invoke-static {v0}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;)V

    :goto_2
    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->c:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->g:Lbf/a;

    const/4 v1, 0x3

    new-instance v2, Lcom/google/googlenav/appwidget/friends/h;

    invoke-direct {v2, p0, v4}, Lcom/google/googlenav/appwidget/friends/h;-><init>(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;Lcom/google/googlenav/appwidget/friends/d;)V

    invoke-virtual {v0, v1, v4, v2}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->c:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :pswitch_4
    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->a:Lcom/google/googlenav/appwidget/friends/i;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/i;)Lcom/google/googlenav/appwidget/friends/c;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/c;Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/appwidget/friends/c;->b()V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :pswitch_5
    sget-object v0, Lcom/google/googlenav/appwidget/friends/i;->b:Lcom/google/googlenav/appwidget/friends/i;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/i;)Lcom/google/googlenav/appwidget/friends/c;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a(Lcom/google/googlenav/appwidget/friends/c;Z)V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    sget-object v0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->l:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    invoke-virtual {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->stopSelf()V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->g:Lcom/google/googlenav/appwidget/friends/g;

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->stopSelf()V

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->g:Lcom/google/googlenav/appwidget/friends/g;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic c(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->h:Ljava/util/List;

    return-object v0
.end method

.method private final c()Z
    .locals 1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;)LaB/a;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->j:LaB/a;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a:Lak/a;

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a:Lak/a;

    invoke-virtual {v0, p0}, Lak/a;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/google/googlenav/appwidget/friends/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/appwidget/friends/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->k:Lcom/google/googlenav/appwidget/friends/b;

    new-instance v0, Lbf/a;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lbf/a;-><init>(Las/c;)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->g:Lbf/a;

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c:Las/d;

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b()Las/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->c:Las/d;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    new-instance v0, LaB/a;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    new-instance v3, LaB/r;

    invoke-direct {v3}, LaB/r;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LaB/a;-><init>(Lcom/google/googlenav/android/aa;Las/c;LaB/q;)V

    iput-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->j:LaB/a;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    invoke-static {}, Lcom/google/googlenav/android/c;->f()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->stopSelf()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->a:Lak/a;

    invoke-virtual {v0}, Lak/a;->a()V

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->a:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->e:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto :goto_0

    :cond_3
    const-string v0, "android.appwidget.action.APPWIDGET_ENABLED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/friend/ad;->n()V

    goto :goto_0

    :cond_4
    const-string v0, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v1, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_MANUAL"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->a:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto :goto_0

    :cond_5
    const-string v0, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.UPDATED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v1, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.UPDATED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->a:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "android.appwidget.action.APPWIDGET_DISABLED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "com.google.googlenav.friend.android.FriendServiceHelper.SIGNED_IN_OUT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->a:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/google/googlenav/appwidget/friends/g;->f:Lcom/google/googlenav/appwidget/friends/g;

    invoke-direct {p0, v0}, Lcom/google/googlenav/appwidget/friends/FriendsAppWidgetUpdateService;->b(Lcom/google/googlenav/appwidget/friends/g;)V

    goto/16 :goto_0
.end method
