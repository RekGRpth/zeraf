.class public Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/IntentFilter;

.field private final b:Landroid/content/IntentFilter;

.field private c:Lcom/google/googlenav/android/R;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.googlenav.friend.android.LatitudeBroadcastReceiver.ACTION_REFRESH_FRIENDS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->a:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.googlenav.friend.android.LatitudeBroadcastReceiver.ACTION_REFRESH_FRIENDS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->b:Landroid/content/IntentFilter;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/MapsActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->c:Lcom/google/googlenav/android/R;

    iget-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->a:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->b:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Lcom/google/android/maps/MapsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public b(Lcom/google/android/maps/MapsActivity;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->c:Lcom/google/googlenav/android/R;

    invoke-virtual {p1, p0}, Lcom/google/android/maps/MapsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->c:Lcom/google/googlenav/android/R;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.googlenav.friend.android.LatitudeBroadcastReceiver.ACTION_REFRESH_FRIENDS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->c:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriends()V

    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.googlenav.friend.android.LatitudeBroadcastReceiver.ACTION_REFRESH_FRIENDS_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/android/LatitudeBroadcastReceiver;->c:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->refreshFriendsSettings()V

    goto :goto_0
.end method
