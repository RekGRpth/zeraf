.class public Lcom/google/googlenav/friend/aI;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Hashtable;


# instance fields
.field private final b:Lcom/google/googlenav/friend/aK;

.field private final c:Z

.field private d:Ljava/lang/Long;

.field private final e:Lcom/google/googlenav/ai;

.field private f:Z

.field private g:I

.field private h:Ljava/lang/Boolean;

.field private i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:Z

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/aI;->a:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, -0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/googlenav/friend/aI;->g:I

    iput-object v3, p0, Lcom/google/googlenav/friend/aI;->h:Ljava/lang/Boolean;

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->i:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->j:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->k:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->l:J

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    iput-object v3, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/aI;->c:Z

    iput-object v3, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/friend/aK;ZLcom/google/googlenav/ai;)V
    .locals 3

    const-wide/16 v1, -0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/googlenav/friend/aI;->g:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/friend/aI;->h:Ljava/lang/Boolean;

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->i:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->j:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->k:J

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->l:J

    iput-object p1, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    iput-boolean p2, p0, Lcom/google/googlenav/friend/aI;->c:Z

    iput-object p3, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/aI;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->n:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/aI;->n:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private J()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->D()Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aI;->c(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/googlenav/a;)Z
    .locals 4

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/a;->e()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/a;->e()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide p1, p0, Lcom/google/googlenav/friend/aI;->l:J

    return-void
.end method

.method public static x()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/friend/aI;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->k()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()[Lcom/google/googlenav/a;
    .locals 7

    const/16 v6, 0x9c

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v3, v2, [Lcom/google/googlenav/a;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/a;

    invoke-direct {v5, v4}, Lcom/google/googlenav/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public C()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->B()[Lcom/google/googlenav/a;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/google/googlenav/friend/aI;->a(Lcom/google/googlenav/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public D()Lcom/google/googlenav/a;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->B()[Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public E()LaN/B;
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->B()[Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public F()J
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->B()[Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/googlenav/a;->b()J

    move-result-wide v3

    sub-long v0, v1, v3

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->p()J

    move-result-wide v0

    goto :goto_0
.end method

.method public G()Z
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/friend/aI;->d(J)Z

    move-result v0

    return v0
.end method

.method public H()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public I()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xaf

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public a(ZZ)Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->D()Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/friend/aI;->J()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x96

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/googlenav/friend/aI;->g(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput p1, p0, Lcom/google/googlenav/friend/aI;->g:I

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->h()V

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->k:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/aI;->k:J

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x79

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide p1, p0, Lcom/google/googlenav/friend/aI;->i:J

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-ltz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    sub-long/2addr v1, p1

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->l:J

    :goto_0
    const/16 v1, 0x82

    iget-wide v2, p0, Lcom/google/googlenav/friend/aI;->l:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void

    :cond_1
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/googlenav/friend/aI;->l:J

    goto :goto_0
.end method

.method public a(LaN/g;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ai;->c(LaN/g;)V

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->h()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/a;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x9c

    invoke-virtual {p1}, Lcom/google/googlenav/a;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertProtoBuf(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ai;->f(Ljava/lang/String;)V

    return-void
.end method

.method public a(Lo/D;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x81

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 12

    const/4 v2, 0x0

    const/16 v11, 0x9

    const-wide/16 v9, 0x0

    const/4 v0, 0x0

    const/high16 v8, -0x80000000

    const/4 v1, 0x5

    invoke-static {p1, v1, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    const/4 v3, 0x6

    invoke-static {p1, v3, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    if-eq v1, v8, :cond_6

    if-eq v3, v8, :cond_6

    const/4 v4, 0x7

    invoke-static {p1, v4}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-gtz v6, :cond_0

    :goto_0
    return v0

    :cond_0
    cmp-long v6, v4, v9

    if-eqz v6, :cond_1

    invoke-virtual {p0, v4, v5}, Lcom/google/googlenav/friend/aI;->a(J)V

    :cond_1
    new-instance v4, LaN/B;

    invoke-direct {v4, v1, v3}, LaN/B;-><init>(II)V

    invoke-virtual {p0, v4}, Lcom/google/googlenav/friend/aI;->a(LaN/g;)V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->t()V

    invoke-virtual {p1, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/friend/aI;->a(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    cmp-long v1, v3, v9

    if-eqz v1, :cond_3

    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/friend/aI;->c(J)V

    :cond_3
    const/16 v1, 0xf

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    cmp-long v1, v3, v9

    if-eqz v1, :cond_4

    invoke-virtual {p0, v3, v4}, Lcom/google/googlenav/friend/aI;->b(J)V

    :cond_4
    const/16 v1, 0xe

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    cmp-long v1, v3, v9

    if-eqz v1, :cond_5

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/friend/aI;->e(J)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->H()Z

    move-result v1

    if-eqz v1, :cond_6

    iput-boolean v0, p0, Lcom/google/googlenav/friend/aI;->m:Z

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/aI;->h(Z)V

    const/4 v0, 0x1

    :cond_6
    const/16 v1, 0x1b

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x1c

    invoke-static {p1, v3, v8}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    if-eqz v1, :cond_8

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_7

    new-instance v2, Lo/D;

    invoke-direct {v2, v1, v3}, Lo/D;-><init>(Lo/r;I)V

    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/aI;->a(Lo/D;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v2}, Lcom/google/googlenav/friend/aI;->a(Lo/D;)V

    goto :goto_0

    :cond_8
    move-object v1, v2

    goto :goto_2
.end method

.method public b(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x83

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-wide p1, p0, Lcom/google/googlenav/friend/aI;->j:J

    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x84

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/aI;->c:Z

    return v0
.end method

.method public c(Z)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c(J)V
    .locals 4

    const/16 v3, 0x7a

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-ltz v1, :cond_0

    invoke-virtual {v0, v3, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    return-void

    :cond_0
    const-wide/16 v1, -0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->d()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()LaN/B;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->m()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->r()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->D()Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/aI;->f:Z

    return-void
.end method

.method public d(J)Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/32 v2, 0xa4cb80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Z)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const v0, 0x600000ff

    :goto_0
    return v0

    :cond_0
    const v0, 0x500000ff

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    const v0, 0x40800080

    goto :goto_0

    :cond_2
    const v0, 0x20800080

    goto :goto_0
.end method

.method public e()Lo/D;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Z)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const v0, 0x180000ff

    :goto_0
    return v0

    :cond_0
    const v0, 0x100000ff

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    const v0, 0x20800080

    goto :goto_0

    :cond_2
    const v0, 0x10800080

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->n:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x7f

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Z)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/aI;->c(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 4

    iget v0, p0, Lcom/google/googlenav/friend/aI;->g:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    const/16 v2, 0x80

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;III)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/friend/aI;->g:I

    :cond_0
    iget v0, p0, Lcom/google/googlenav/friend/aI;->g:I

    return v0
.end method

.method public h(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method public i()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->h()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->h()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v2, 0x6e

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/16 v2, 0x84

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x85

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/friend/aI;->h:Ljava/lang/Boolean;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method public n()Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->d:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/aI;->d:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->d:Ljava/lang/Long;

    return-object v0
.end method

.method public o()J
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->i:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    const/16 v2, 0x79

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/aI;->i:J

    :cond_0
    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->i:J

    return-wide v0
.end method

.method public p()J
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->l:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    const/16 v2, 0x82

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/aI;->l:J

    :cond_0
    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->l:J

    return-wide v0
.end method

.method public q()J
    .locals 4

    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->k:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/friend/aI;->k:J

    :cond_0
    iget-wide v0, p0, Lcom/google/googlenav/friend/aI;->k:J

    return-wide v0
.end method

.method public r()J
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    const/16 v2, 0x7a

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    const/16 v2, 0x70

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public u()Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->c()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/googlenav/friend/aI;->a:Ljava/util/Hashtable;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public w()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v0

    sget-object v1, Lcom/google/googlenav/friend/aI;->a:Ljava/util/Hashtable;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v1

    if-eq v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/friend/aI;->b:Lcom/google/googlenav/friend/aK;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aK;->h()V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/friend/aI;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->c(Z)V

    goto :goto_0
.end method

.method public z()C
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-char v0, Lcom/google/googlenav/ui/bi;->C:C

    :goto_0
    return v0

    :cond_0
    sget-char v0, Lcom/google/googlenav/ui/bi;->B:C

    goto :goto_0
.end method
