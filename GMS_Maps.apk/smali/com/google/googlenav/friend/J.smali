.class public Lcom/google/googlenav/friend/J;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;


# instance fields
.field private final a:Ljava/lang/Object;

.field private volatile b:Lcom/google/googlenav/friend/K;

.field private c:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/J;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    return-void
.end method

.method public static a(LaN/B;JLaN/B;)Z
    .locals 5

    const-wide/16 v0, 0x32

    const/4 v2, 0x0

    if-nez p3, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    if-nez p0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    const-wide/16 v3, 0x0

    cmp-long v3, p1, v3

    if-ltz v3, :cond_2

    const-wide/32 v3, 0x1869f

    cmp-long v3, p1, v3

    if-nez v3, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    cmp-long v3, p1, v0

    if-gez v3, :cond_4

    move-wide p1, v0

    :cond_4
    invoke-virtual {p3, p0}, LaN/B;->b(LaN/B;)J

    move-result-wide v0

    mul-long v3, p1, p1

    cmp-long v0, v0, v3

    if-gtz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/friend/K;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/J;->b:Lcom/google/googlenav/friend/K;

    return-object v0
.end method

.method public a(ILaH/m;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/A;

    invoke-interface {v0, p1, p2}, LaH/A;->a(ILaH/m;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(LaH/A;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/A;

    invoke-interface {v0, p1, p2}, LaH/A;->a(LaN/B;LaH/m;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/friend/J;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-interface {p2}, LaH/m;->r()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Lcom/google/googlenav/friend/K;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v5

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/googlenav/friend/K;-><init>(LaN/B;Ljava/lang/String;ILo/D;)V

    iput-object v2, p0, Lcom/google/googlenav/friend/J;->b:Lcom/google/googlenav/friend/K;

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(LaH/A;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/J;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    return-void
.end method
