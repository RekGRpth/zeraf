.class public Lcom/google/googlenav/friend/history/g;
.super Law/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:Z

.field private final c:I

.field private final d:I

.field private final e:Lcom/google/googlenav/friend/history/h;


# direct methods
.method public constructor <init>(IILcom/google/googlenav/friend/history/h;)V
    .locals 0

    invoke-direct {p0}, Law/b;-><init>()V

    iput p1, p0, Lcom/google/googlenav/friend/history/g;->c:I

    iput p2, p0, Lcom/google/googlenav/friend/history/g;->d:I

    iput-object p3, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/bK;->av:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/aN;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v2, p0, Lcom/google/googlenav/friend/history/g;->c:I

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/googlenav/friend/history/g;->d:I

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bK;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, v1}, LaM/f;->a(I)V

    :goto_0
    return v3

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x8b

    return v0
.end method

.method public d_()V
    .locals 2

    invoke-super {p0}, Law/b;->d_()V

    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/googlenav/friend/history/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/history/g;->e:Lcom/google/googlenav/friend/history/h;

    invoke-interface {v0}, Lcom/google/googlenav/friend/history/h;->a()V

    goto :goto_0
.end method

.method protected synthetic k()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/g;->n()Lcom/google/googlenav/friend/history/i;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lcom/google/googlenav/friend/history/i;
    .locals 3

    new-instance v0, Lcom/google/googlenav/friend/history/i;

    iget-boolean v1, p0, Lcom/google/googlenav/friend/history/g;->b:Z

    iget-object v2, p0, Lcom/google/googlenav/friend/history/g;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/history/i;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public s_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
