.class public Lcom/google/googlenav/friend/history/U;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/b;

.field private final b:Ljava/util/List;

.field private final c:F

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/b;Ljava/lang/String;Ljava/util/List;F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/googlenav/friend/history/U;->a:Lcom/google/googlenav/friend/history/b;

    iput-object p2, p0, Lcom/google/googlenav/friend/history/U;->d:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/friend/history/U;->b:Ljava/util/List;

    iput p4, p0, Lcom/google/googlenav/friend/history/U;->c:F

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/google/googlenav/friend/history/U;->c:F

    float-to-int v0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/googlenav/friend/history/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/U;->a:Lcom/google/googlenav/friend/history/b;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/U;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/history/U;->b:Ljava/util/List;

    return-object v0
.end method

.method public e()Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/U;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->c()Lcom/google/googlenav/friend/history/p;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/friend/history/p;->d:Lcom/google/googlenav/friend/history/p;

    if-eq v0, v3, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
