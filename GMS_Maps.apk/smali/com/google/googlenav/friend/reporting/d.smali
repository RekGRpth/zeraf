.class public Lcom/google/googlenav/friend/reporting/d;
.super Lcom/google/googlenav/friend/reporting/a;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lbm/c;

.field private c:Lcom/google/googlenav/friend/reporting/s;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE TABLE activityDetections (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \'%s\' BLOB,"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "activityType"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \'%s\' INTEGER"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "timeMs"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/friend/reporting/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbm/c;Lcom/google/googlenav/friend/reporting/s;)V
    .locals 8

    const-string v2, "activityDetections"

    const-string v3, "timeMs"

    const/16 v4, 0x3e8

    const-wide/32 v5, 0x5265c00

    sget-object v7, Lcom/google/googlenav/friend/reporting/d;->a:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/friend/reporting/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V

    iput-object p3, p0, Lcom/google/googlenav/friend/reporting/d;->c:Lcom/google/googlenav/friend/reporting/s;

    iput-object p2, p0, Lcom/google/googlenav/friend/reporting/d;->b:Lbm/c;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/location/clientlib/NlpActivity;)Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "activityType"

    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/d;->b:Lbm/c;

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbm/c;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v1, "timeMs"

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Landroid/content/ContentValues;
    .locals 1

    check-cast p1, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/d;->a(Lcom/google/android/location/clientlib/NlpActivity;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/d;->b(Landroid/database/Cursor;)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/d;->c:Lcom/google/googlenav/friend/reporting/s;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/s;->d()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/database/Cursor;)Lcom/google/android/location/clientlib/NlpActivity;
    .locals 5

    const-wide/16 v3, -0x1

    const/4 v0, 0x0

    const-string v1, "activityType"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/d;->b:Lbm/c;

    const-string v1, "activityType"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lbm/c;->c([B)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/friend/reporting/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/reporting/b;-><init>()V

    throw v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->valueOf(Ljava/lang/String;)Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    :cond_1
    const-string v1, "timeMs"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "timeMs"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    :goto_0
    if-eqz v0, :cond_2

    cmp-long v3, v1, v3

    if-lez v3, :cond_2

    new-instance v3, Lcom/google/android/location/clientlib/NlpActivity;

    const/4 v4, -0x1

    invoke-direct {v3, v0, v4, v1, v2}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    return-object v3

    :cond_2
    new-instance v0, Lcom/google/googlenav/friend/reporting/b;

    invoke-direct {v0}, Lcom/google/googlenav/friend/reporting/b;-><init>()V

    throw v0

    :cond_3
    move-wide v1, v3

    goto :goto_0
.end method
