.class public Lcom/google/googlenav/friend/reporting/LocationPrivacyService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private b:Lak/a;

.field private c:Lcom/google/googlenav/friend/history/v;

.field private d:Lcom/google/googlenav/friend/reporting/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "LocationPrivacyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-instance v0, Lcom/google/googlenav/friend/S;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v2, v2, v1}, Lcom/google/googlenav/friend/S;-><init>(ILcom/google/googlenav/friend/aq;Lcom/google/googlenav/android/aa;Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/S;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/T;

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/googlenav/friend/T;->a:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/google/googlenav/friend/ae;

    new-instance v2, Lcom/google/googlenav/friend/U;

    iget-object v0, v0, Lcom/google/googlenav/friend/T;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0}, Lcom/google/googlenav/friend/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/U;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/googlenav/friend/ae;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v1}, Lcom/google/googlenav/friend/ae;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {v1}, Lcom/google/googlenav/friend/ae;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/friend/aH;->b(Z)V

    goto :goto_0

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/friend/aH;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/friend/ad;->B()V

    :goto_2
    invoke-static {}, Lcom/google/googlenav/friend/aH;->f()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    goto :goto_2

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    invoke-static {}, Lcom/google/googlenav/friend/aH;->g()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/google/googlenav/friend/ad;->C()V

    invoke-static {}, Lcom/google/googlenav/friend/aH;->h()V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .locals 7

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/32 v3, 0x36ee80

    add-long v2, v1, v3

    const/4 v1, 0x2

    const-wide/32 v4, 0x2932e00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    invoke-virtual {v0}, Lak/a;->a()V

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->d:Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/reporting/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/reporting/r;->b(Landroid/content/Context;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->c:Lcom/google/googlenav/friend/history/v;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/v;->a()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    :cond_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "LocationPrivacyService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Lak/a;

    invoke-direct {v0}, Lak/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->b:Lak/a;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lak/a;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/friend/history/v;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/friend/history/v;-><init>(Landroid/content/Context;Lbm/c;)V

    iput-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->c:Lcom/google/googlenav/friend/history/v;

    new-instance v0, Lcom/google/googlenav/friend/reporting/e;

    invoke-virtual {p0}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/friend/reporting/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->d:Lcom/google/googlenav/friend/reporting/e;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/LocationPrivacyService;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
