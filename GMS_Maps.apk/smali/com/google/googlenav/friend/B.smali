.class Lcom/google/googlenav/friend/B;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/cA;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/google/googlenav/friend/t;


# direct methods
.method constructor <init>(Lcom/google/googlenav/friend/t;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/B;->b:Lcom/google/googlenav/friend/t;

    iput-object p2, p0, Lcom/google/googlenav/friend/B;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aI;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/aI;->a(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->y()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aI;

    iget-object v2, p0, Lcom/google/googlenav/friend/B;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/B;->a(Lcom/google/googlenav/friend/aI;)V

    iget-object v2, p0, Lcom/google/googlenav/friend/B;->b:Lcom/google/googlenav/friend/t;

    invoke-static {v2}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/t;)Lbf/X;

    move-result-object v2

    invoke-virtual {v2}, Lbf/X;->c()Lcom/google/googlenav/friend/aK;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/friend/aK;->a(Ljava/lang/Long;)Lcom/google/googlenav/friend/aI;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/googlenav/friend/B;->a(Lcom/google/googlenav/friend/aI;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/friend/B;->b:Lcom/google/googlenav/friend/t;

    invoke-static {v0}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/t;)Lbf/X;

    move-result-object v0

    invoke-virtual {v0}, Lbf/X;->J()V

    iget-object v0, p0, Lcom/google/googlenav/friend/B;->b:Lcom/google/googlenav/friend/t;

    invoke-static {v0}, Lcom/google/googlenav/friend/t;->b(Lcom/google/googlenav/friend/t;)Lbf/X;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lbf/X;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/friend/B;->b:Lcom/google/googlenav/friend/t;

    invoke-static {v0}, Lcom/google/googlenav/friend/t;->e(Lcom/google/googlenav/friend/t;)Lcom/google/googlenav/friend/p;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/friend/p;->a(Lcom/google/googlenav/bb;J)V

    return-void
.end method
