.class public Lcom/google/googlenav/friend/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LaN/B;

.field private b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:J

.field private h:I

.field private i:Z

.field private j:J

.field private k:F

.field private l:F

.field private m:D

.field private n:I

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/Boolean;

.field private q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private r:Lcom/google/googlenav/friend/ay;

.field private s:Lcom/google/googlenav/android/aa;

.field private t:Lcom/google/googlenav/friend/aq;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v0, -0x40800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/googlenav/friend/ax;->k:F

    iput v0, p0, Lcom/google/googlenav/friend/ax;->l:F

    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, Lcom/google/googlenav/friend/ax;->m:D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/friend/ax;->n:I

    iput-object v2, p0, Lcom/google/googlenav/friend/ax;->o:Ljava/lang/Boolean;

    iput-object v2, p0, Lcom/google/googlenav/friend/ax;->p:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/ax;)LaN/B;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->a:LaN/B;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/ax;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/friend/ax;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/ax;->d:I

    return v0
.end method

.method static synthetic d(Lcom/google/googlenav/friend/ax;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/friend/ax;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/ax;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/friend/ax;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/ax;->g:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/googlenav/friend/ax;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/ax;->h:I

    return v0
.end method

.method static synthetic h(Lcom/google/googlenav/friend/ax;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/ax;->f:Z

    return v0
.end method

.method static synthetic i(Lcom/google/googlenav/friend/ax;)Lcom/google/googlenav/friend/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->t:Lcom/google/googlenav/friend/aq;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/friend/ax;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->s:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/friend/ax;)Lcom/google/googlenav/friend/ay;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->r:Lcom/google/googlenav/friend/ay;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/friend/ax;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/friend/ax;->i:Z

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/friend/ax;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/ax;->j:J

    return-wide v0
.end method

.method static synthetic n(Lcom/google/googlenav/friend/ax;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/ax;->n:I

    return v0
.end method

.method static synthetic o(Lcom/google/googlenav/friend/ax;)F
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/ax;->k:F

    return v0
.end method

.method static synthetic p(Lcom/google/googlenav/friend/ax;)F
    .locals 1

    iget v0, p0, Lcom/google/googlenav/friend/ax;->l:F

    return v0
.end method

.method static synthetic q(Lcom/google/googlenav/friend/ax;)D
    .locals 2

    iget-wide v0, p0, Lcom/google/googlenav/friend/ax;->m:D

    return-wide v0
.end method

.method static synthetic r(Lcom/google/googlenav/friend/ax;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic s(Lcom/google/googlenav/friend/ax;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->o:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic t(Lcom/google/googlenav/friend/ax;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/friend/ax;->p:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/friend/aw;
    .locals 1

    new-instance v0, Lcom/google/googlenav/friend/aw;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/aw;-><init>(Lcom/google/googlenav/friend/ax;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/ax;->d:I

    return-object p0
.end method

.method public a(J)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-wide p1, p0, Lcom/google/googlenav/friend/ax;->g:J

    return-object p0
.end method

.method public a(LaN/B;)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/ax;->a:LaN/B;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/android/aa;)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/ax;->s:Lcom/google/googlenav/android/aa;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/friend/aq;)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/ax;->t:Lcom/google/googlenav/friend/aq;

    return-object p0
.end method

.method public a(Lcom/google/googlenav/friend/ay;)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/ax;->r:Lcom/google/googlenav/friend/ay;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/friend/ax;->c:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/ax;->e:Z

    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/ax;->h:I

    return-object p0
.end method

.method public b(J)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-wide p1, p0, Lcom/google/googlenav/friend/ax;->j:J

    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/ax;->f:Z

    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput p1, p0, Lcom/google/googlenav/friend/ax;->n:I

    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/friend/ax;
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/friend/ax;->i:Z

    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/friend/ax;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/ax;->o:Ljava/lang/Boolean;

    return-object p0
.end method
