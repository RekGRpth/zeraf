.class Lcom/google/googlenav/intersectionexplorer/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/googlenav/intersectionexplorer/c;

.field final synthetic b:D

.field final synthetic c:Lcom/google/googlenav/intersectionexplorer/d;


# direct methods
.method constructor <init>(Lcom/google/googlenav/intersectionexplorer/d;Lcom/google/googlenav/intersectionexplorer/c;D)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    iput-object p2, p0, Lcom/google/googlenav/intersectionexplorer/e;->a:Lcom/google/googlenav/intersectionexplorer/c;

    iput-wide p3, p0, Lcom/google/googlenav/intersectionexplorer/e;->b:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Lcom/google/googlenav/intersectionexplorer/d;)Lcom/google/googlenav/intersectionexplorer/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/e;->a:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-static {v1}, Lcom/google/googlenav/intersectionexplorer/d;->b(Lcom/google/googlenav/intersectionexplorer/d;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    iput-object v0, v2, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-wide v0, p0, Lcom/google/googlenav/intersectionexplorer/e;->b:D

    const-wide/high16 v2, -0x4010000000000000L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/intersectionexplorer/d;->a(Z)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/d;->b(Lcom/google/googlenav/intersectionexplorer/d;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    const/16 v2, 0x1ef

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/google/googlenav/intersectionexplorer/e;->b:D

    double-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/googlenav/intersectionexplorer/e;->c:Lcom/google/googlenav/intersectionexplorer/d;

    iget-object v5, v5, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
