.class Lcom/google/googlenav/intersectionexplorer/m;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/googlenav/intersectionexplorer/k;

.field final b:Lcom/google/googlenav/intersectionexplorer/c;

.field final c:Lo/T;

.field d:Ljava/util/TreeSet;

.field e:Lcom/google/googlenav/intersectionexplorer/c;

.field f:Lo/ad;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;Lo/T;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iput-object p2, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    iput-object p3, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    return-void
.end method

.method private a(Lq/a;)Ljava/util/TreeSet;
    .locals 12

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-virtual {p1}, Lq/a;->a()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-virtual {p1, v2}, Lq/a;->a(I)Lq/c;

    move-result-object v1

    invoke-virtual {v1}, Lq/c;->b()Lo/af;

    move-result-object v6

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v1}, Lq/c;->c()Lo/T;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lq/c;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->f()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq/b;

    invoke-virtual {v0}, Lq/b;->a()Lq/c;

    move-result-object v8

    invoke-virtual {v1}, Lq/c;->c()Lo/T;

    move-result-object v9

    invoke-virtual {v1}, Lq/c;->d()Lo/T;

    move-result-object v10

    invoke-virtual {v0}, Lq/b;->b()F

    move-result v11

    invoke-virtual {v9, v10, v11}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v9

    invoke-virtual {v8}, Lq/c;->c()Lo/T;

    move-result-object v10

    invoke-virtual {v8}, Lq/c;->d()Lo/T;

    move-result-object v8

    invoke-virtual {v0}, Lq/b;->c()F

    move-result v0

    invoke-virtual {v10, v8, v0}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    invoke-static {v5, v9}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v8

    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    if-eq v8, v0, :cond_0

    invoke-virtual {v8, v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    invoke-virtual {v5, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v5, v8}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lq/c;->d()Lo/T;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v4, v3

    :goto_2
    add-int/lit8 v0, v8, -0x1

    if-ge v4, v0, :cond_2

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    add-int/lit8 v1, v4, 0x1

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, v1, v6}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V

    invoke-virtual {v1, v0, v6}, Lcom/google/googlenav/intersectionexplorer/c;->a(Lcom/google/googlenav/intersectionexplorer/c;Lo/af;)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_3
    invoke-static {v5}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/TreeSet;)Ljava/util/TreeSet;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/T;D)Lo/ad;
    .locals 4

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v0

    mul-double/2addr v0, p2

    const-wide/high16 v2, 0x3fe0000000000000L

    add-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {p1, v0}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/ad;)Lq/d;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->e()Lq/e;

    move-result-object v1

    const/4 v2, 0x1

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, p1, v2, v3, v4}, Lq/e;->a(Lo/ad;ZJ)Lq/d;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->a:Z

    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Lq/d;

    invoke-direct {v2}, Lq/d;-><init>()V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lq/d;->a()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v1, v0}, Lq/d;->a(I)Lo/af;

    move-result-object v3

    invoke-virtual {v3}, Lo/af;->c()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {v1, v0}, Lq/d;->b(I)Lo/X;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lq/d;->a(Lo/af;Lo/X;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    const-wide v1, 0x4082c00000000000L

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lo/T;D)Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    invoke-direct {p0, v0}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lo/ad;)Lq/d;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-wide/high16 v1, 0x4024000000000000L

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    new-instance v2, Lq/a;

    const/4 v3, 0x2

    invoke-direct {v2, v0, v1, v3}, Lq/a;-><init>(Lq/d;II)V

    invoke-direct {p0, v2}, Lcom/google/googlenav/intersectionexplorer/m;->a(Lq/a;)Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->c:Lo/T;

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    invoke-static {v1, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    goto :goto_1
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/d;->h()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->d:Ljava/util/TreeSet;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Ljava/util/TreeSet;)Ljava/util/TreeSet;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->f:Lo/ad;

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Lo/ad;)Lo/ad;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/m;->b:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/m;->e:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-interface {v0, v2}, Lcom/google/googlenav/intersectionexplorer/n;->b(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-static {v2}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/TreeSet;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/googlenav/intersectionexplorer/n;->a(Ljava/util/Set;)V

    goto :goto_1
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/m;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/m;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/m;->a:Lcom/google/googlenav/intersectionexplorer/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z

    return-void
.end method
