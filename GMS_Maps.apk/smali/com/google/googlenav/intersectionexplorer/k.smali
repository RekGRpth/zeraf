.class public Lcom/google/googlenav/intersectionexplorer/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lq/e;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field private final b:Ljava/util/Set;

.field private c:Ljava/util/TreeSet;

.field private d:Lcom/google/googlenav/intersectionexplorer/c;

.field private e:Lo/ad;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/l;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/l;-><init>()V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/k;->g:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    iput-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    return-object p1
.end method

.method static a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 10

    const/4 v5, 0x0

    const-wide v3, 0x7fefffffffffffffL

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    invoke-virtual {v1, p1}, Lo/T;->c(Lo/T;)F

    move-result v1

    float-to-double v1, v1

    cmpg-double v7, v1, v3

    if-gez v7, :cond_1

    move-wide v8, v1

    move-object v2, v0

    move-wide v0, v8

    :goto_1
    move-wide v3, v0

    move-object v5, v2

    goto :goto_0

    :cond_0
    return-object v5

    :cond_1
    move-wide v0, v3

    move-object v2, v5

    goto :goto_1
.end method

.method static a(Ljava/util/Set;Lo/T;Z)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-wide/high16 v1, 0x4000000000000000L

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v3

    mul-double v2, v1, v3

    invoke-static {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v4

    invoke-virtual {v4, p1}, Lo/T;->c(Lo/T;)F

    move-result v4

    float-to-double v4, v4

    cmpl-double v2, v4, v2

    if-lez v2, :cond_2

    :goto_1
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-direct {v0, p1}, Lcom/google/googlenav/intersectionexplorer/c;-><init>(Lo/T;)V

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Ljava/util/TreeSet;)Ljava/util/TreeSet;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object p1
.end method

.method static a(Ljava/util/TreeSet;)Ljava/util/TreeSet;
    .locals 5

    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->b(Lcom/google/googlenav/intersectionexplorer/c;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->f()Z

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/common/collect/dA;->c()Ljava/util/TreeSet;

    move-result-object v2

    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->e(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_2

    :cond_5
    return-object v2
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Lo/ad;)Lo/ad;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/k;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    return p1
.end method

.method static synthetic b(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->c(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/intersectionexplorer/k;)Ljava/util/TreeSet;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object v0
.end method

.method private static c(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;Z)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method private c(Lo/T;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/m;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/intersectionexplorer/m;-><init>(Lcom/google/googlenav/intersectionexplorer/k;Lcom/google/googlenav/intersectionexplorer/c;Lo/T;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic e()Lq/e;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/k;->g()Lq/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->g:Ljava/util/Comparator;

    return-object v0
.end method

.method private static g()Lq/e;
    .locals 2

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    if-nez v0, :cond_0

    new-instance v0, Lq/e;

    sget-object v1, LA/c;->a:LA/c;

    invoke-static {v1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v1

    invoke-direct {v0, v1}, Lq/e;-><init>(Lr/z;)V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    :cond_0
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/k;->a:Lq/e;

    return-object v0
.end method


# virtual methods
.method public a(D)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 2

    const-wide v0, 0x3fd921fb54442d18L

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->b(DD)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;
    .locals 3

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/intersectionexplorer/c;->c(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;

    move-result-object v1

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/n;

    invoke-interface {v0, p1}, Lcom/google/googlenav/intersectionexplorer/n;->b(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lo/T;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->c(Lo/T;)V

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(DD)Lo/af;
    .locals 2

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/googlenav/intersectionexplorer/k;->b(DD)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->f(Lcom/google/googlenav/intersectionexplorer/c;)Lo/af;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/intersectionexplorer/n;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lo/T;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->b(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->c(Lo/T;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    invoke-static {v0, p1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Ljava/util/Set;Lo/T;)Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/c;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/google/googlenav/intersectionexplorer/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    return-object v0
.end method

.method public b(DD)Lcom/google/googlenav/intersectionexplorer/c;
    .locals 10

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v3, p3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/intersectionexplorer/c;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->d:Lcom/google/googlenav/intersectionexplorer/c;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/c;->d(Lcom/google/googlenav/intersectionexplorer/c;)D

    move-result-wide v1

    invoke-static {p1, p2, v1, v2}, Lcom/google/googlenav/intersectionexplorer/c;->a(DD)D

    move-result-wide v1

    cmpg-double v7, v1, v3

    if-gez v7, :cond_1

    move-wide v8, v1

    move-object v2, v0

    move-wide v0, v8

    :goto_1
    move-wide v3, v0

    move-object v5, v2

    goto :goto_0

    :cond_0
    return-object v5

    :cond_1
    move-wide v0, v3

    move-object v2, v5

    goto :goto_1
.end method

.method b(Lo/T;)Z
    .locals 5

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-wide/high16 v1, 0x4079000000000000L

    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {p1, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v1

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v2

    invoke-virtual {v1}, Lo/ad;->e()Lo/T;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v3

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v4

    if-gt v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v3}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    if-gt v3, v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v3

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/k;->e:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    if-lt v2, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->c:Ljava/util/TreeSet;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/intersectionexplorer/k;->f:Z

    return v0
.end method
