.class public Lcom/google/googlenav/intersectionexplorer/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/intersectionexplorer/n;


# static fields
.field public static a:Z

.field public static final b:LaN/Y;

.field private static final d:Lcom/google/googlenav/intersectionexplorer/d;

.field private static e:Z

.field private static final f:[J

.field private static final g:[J

.field private static final h:[J

.field private static final i:[J

.field private static final j:[J

.field private static m:Landroid/speech/tts/TextToSpeech;


# instance fields
.field public c:Ljava/lang/String;

.field private k:LaN/u;

.field private l:Lcom/google/googlenav/ui/s;

.field private n:Landroid/location/LocationManager;

.field private o:Landroid/os/Vibrator;

.field private p:Lcom/google/android/maps/driveabout/vector/av;

.field private final q:Lcom/google/googlenav/intersectionexplorer/k;

.field private r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

.field private s:Lo/T;

.field private t:Ljava/util/Map;

.field private final u:Ljava/lang/Object;

.field private v:Lcom/google/googlenav/intersectionexplorer/j;

.field private w:Ljava/lang/String;

.field private x:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x2

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/d;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/d;-><init>()V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->d:Lcom/google/googlenav/intersectionexplorer/d;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->e:Z

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->f:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->g:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->h:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->i:[J

    new-array v0, v1, [J

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->j:[J

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->a:Z

    const/16 v0, 0x14

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->b:LaN/Y;

    return-void

    :array_0
    .array-data 8
        0x0
        0x64
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x50
    .end array-data

    :array_2
    .array-data 8
        0x0
        0x3c
    .end array-data

    :array_3
    .array-data 8
        0x0
        0x28
    .end array-data

    :array_4
    .array-data 8
        0x0
        0x28
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/k;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/k;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->u:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/intersectionexplorer/d;)Lcom/google/googlenav/intersectionexplorer/k;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    return-object v0
.end method

.method public static a()V
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->d:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/d;->o()V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/s;LaN/u;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    iput-object p2, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    invoke-direct {p0, p3}, Lcom/google/googlenav/intersectionexplorer/d;->b(Z)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/s;Z)V
    .locals 2

    sget-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->e:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->d:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/intersectionexplorer/d;->a(Lcom/google/googlenav/ui/s;LaN/u;Z)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->d:Lcom/google/googlenav/intersectionexplorer/d;

    invoke-direct {v0}, Lcom/google/googlenav/intersectionexplorer/d;->n()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/intersectionexplorer/d;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->u:Ljava/lang/Object;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "St,"

    const-string v1, "Street"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "St."

    const-string v2, "Street"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Rd"

    const-string v2, "Road"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fwy"

    const-string v2, "Freeway"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Pkwy"

    const-string v2, "Parkway"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Blvd"

    const-string v2, "Boulevard"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Expy"

    const-string v2, "Expressway"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ave"

    const-string v2, "Avenue"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Dr"

    const-string v2, "Drive"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ct"

    const-string v2, "Court"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/d;->s()V

    new-instance v0, Lcom/google/android/maps/driveabout/vector/av;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/av;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/av;->e()V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->e()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->o:Landroid/os/Vibrator;

    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->m:Landroid/speech/tts/TextToSpeech;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lcom/google/googlenav/intersectionexplorer/n;)V

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/d;->q()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/intersectionexplorer/d;->e:Z

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/intersectionexplorer/d;)Landroid/os/Vibrator;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->o:Landroid/os/Vibrator;

    return-object v0
.end method

.method public static c()Lcom/google/googlenav/intersectionexplorer/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->d:Lcom/google/googlenav/intersectionexplorer/d;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/intersectionexplorer/d;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->t:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/intersectionexplorer/d;)LaN/u;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    return-object v0
.end method

.method static synthetic i()[J
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->j:[J

    return-object v0
.end method

.method static synthetic j()[J
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->i:[J

    return-object v0
.end method

.method static synthetic k()[J
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->h:[J

    return-object v0
.end method

.method static synthetic l()[J
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->g:[J

    return-object v0
.end method

.method static synthetic m()[J
    .locals 1

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->f:[J

    return-object v0
.end method

.method private n()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/d;->s()V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->f()Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->b()Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lo/T;)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/av;->e()V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->e()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->b()Lo/T;

    move-result-object v0

    goto :goto_0
.end method

.method private o()V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/d;->t()V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->f()Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->e()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_1
    return-void
.end method

.method private p()Lo/T;
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->n:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-string v2, "location"

    invoke-virtual {v0, v2}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->n:Landroid/location/LocationManager;

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->n:Landroid/location/LocationManager;

    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/d;->n:Landroid/location/LocationManager;

    const-string v4, "network"

    invoke-virtual {v2, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Lo/T;->a(DD)Lo/T;

    move-result-object v2

    :goto_1
    iget-object v5, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-static {v5}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v0

    :cond_1
    :goto_2
    return-object v0

    :cond_2
    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    cmp-long v1, v5, v3

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_1

    move-object v0, v2

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    if-nez v0, :cond_1

    if-eqz v2, :cond_5

    move-object v0, v2

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    move-object v2, v1

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method private q()V
    .locals 10

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/google/googlenav/intersectionexplorer/a;

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->b:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->c:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->f:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->i:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/a;->h:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    sget-object v3, Lcom/google/googlenav/intersectionexplorer/a;->g:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/googlenav/intersectionexplorer/a;->d:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/googlenav/intersectionexplorer/a;->a:Lcom/google/googlenav/intersectionexplorer/a;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "north"

    aput-object v3, v2, v0

    const-string v3, "northeast"

    aput-object v3, v2, v4

    const-string v3, "east"

    aput-object v3, v2, v5

    const-string v3, "southeast"

    aput-object v3, v2, v6

    const-string v3, "south"

    aput-object v3, v2, v7

    const/4 v3, 0x5

    const-string v4, "southwest"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "west"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "northwest"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/googlenav/intersectionexplorer/d;->t:Ljava/util/Map;

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/intersectionexplorer/d;->t:Ljava/util/Map;

    aget-object v4, v1, v0

    new-instance v5, Lcom/google/googlenav/intersectionexplorer/h;

    const-wide v6, 0x400921fb54442d18L

    int-to-double v8, v0

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4010000000000000L

    div-double/2addr v6, v8

    aget-object v8, v2, v0

    invoke-direct {v5, v6, v7, v8}, Lcom/google/googlenav/intersectionexplorer/h;-><init>(DLjava/lang/String;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static r()Lcom/google/android/maps/MapsActivity;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->i()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method private s()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aq()Lcom/google/googlenav/ui/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bE;->b()V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbf/am;->b(Z)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-virtual {v0}, LaN/u;->i()V

    return-void
.end method

.method private t()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->l:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aq()Lcom/google/googlenav/ui/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bE;->b()V

    return-void
.end method


# virtual methods
.method public a(LaN/B;Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->u:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lo/T;)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    sget-object v1, Lcom/google/googlenav/intersectionexplorer/d;->b:LaN/Y;

    invoke-virtual {v0, p1, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    if-nez p2, :cond_1

    const/16 v0, 0x1f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    const/16 v0, 0x1f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/av;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/intersectionexplorer/i;

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/intersectionexplorer/i;-><init>(Lcom/google/googlenav/intersectionexplorer/d;Lcom/google/googlenav/intersectionexplorer/e;)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;-><init>(Landroid/content/Context;Lcom/google/googlenav/intersectionexplorer/b;)V

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->f()Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->b()Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->s:Lo/T;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lo/T;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/GestureOverlay;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->f()Lcom/google/googlenav/ui/android/AndroidVectorView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->r:Lcom/google/googlenav/intersectionexplorer/GestureOverlay;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/AndroidVectorView;->addView(Landroid/view/View;)V

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->o:Landroid/os/Vibrator;

    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v0, Lcom/google/googlenav/intersectionexplorer/d;->m:Landroid/speech/tts/TextToSpeech;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->c()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->b()Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->b(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/intersectionexplorer/c;)V
    .locals 5

    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->b()Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo/T;->c(Lo/T;)F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v3

    div-double v0, v1, v3

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/intersectionexplorer/e;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/google/googlenav/intersectionexplorer/e;-><init>(Lcom/google/googlenav/intersectionexplorer/d;Lcom/google/googlenav/intersectionexplorer/c;D)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/MapsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/google/googlenav/intersectionexplorer/d;->w:Ljava/lang/String;

    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->m:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_1

    new-instance v0, Landroid/media/ToneGenerator;

    const/4 v1, 0x5

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Landroid/media/ToneGenerator;-><init>(II)V

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Landroid/media/ToneGenerator;->startTone(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/googlenav/intersectionexplorer/d;->m:Landroid/speech/tts/TextToSpeech;

    invoke-static {p1}, Lcom/google/googlenav/intersectionexplorer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->v:Lcom/google/googlenav/intersectionexplorer/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->v:Lcom/google/googlenav/intersectionexplorer/j;

    invoke-interface {v0, p1}, Lcom/google/googlenav/intersectionexplorer/j;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/av;->a(Ljava/util/Set;)V

    return-void
.end method

.method public a(Z)V
    .locals 5

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->u:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    const/16 v0, 0x1f1

    :try_start_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v2}, Lcom/google/googlenav/intersectionexplorer/k;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    :goto_1
    monitor-exit v1

    return-void

    :cond_0
    const/16 v0, 0x1e8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v4}, Lcom/google/googlenav/intersectionexplorer/k;->b()Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/intersectionexplorer/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Loading failed"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/16 v0, 0x1eb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method b()Lo/T;
    .locals 6

    const-wide v4, 0x412e848000000000L

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->x:Lo/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->x:Lo/T;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/intersectionexplorer/d;->p()Lo/T;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/MapsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v1, "android.intent.extra.INIT_LATLON"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    :cond_2
    if-eqz v1, :cond_3

    const/4 v0, 0x0

    aget v0, v1, v0

    int-to-double v2, v0

    div-double/2addr v2, v4

    const/4 v0, 0x1

    aget v0, v1, v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-static {v0}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/intersectionexplorer/c;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/intersectionexplorer/f;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/intersectionexplorer/f;-><init>(Lcom/google/googlenav/intersectionexplorer/d;LaN/B;)V

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->p:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/av;->a(Lcom/google/googlenav/intersectionexplorer/c;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->b()Lo/T;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/intersectionexplorer/k;->a(Lo/T;)V

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    invoke-virtual {v1, v0}, LaN/u;->b(LaN/B;)V

    const/16 v0, 0x1f0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->u:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->c:Ljava/lang/String;

    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/16 v0, 0x1ce

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/googlenav/ui/android/AndroidVectorView;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    return-object v0
.end method

.method public g()Z
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/intersectionexplorer/d;->q:Lcom/google/googlenav/intersectionexplorer/k;

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/k;->b()Lcom/google/googlenav/intersectionexplorer/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/intersectionexplorer/c;->b()Lo/T;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/intersectionexplorer/d;->k:LaN/u;

    sget-object v2, Lcom/google/googlenav/intersectionexplorer/d;->b:LaN/Y;

    invoke-virtual {v1, v0, v2}, LaN/u;->d(LaN/B;LaN/Y;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/intersectionexplorer/d;->b()Lo/T;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->r()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/intersectionexplorer/g;

    invoke-direct {v1, p0}, Lcom/google/googlenav/intersectionexplorer/g;-><init>(Lcom/google/googlenav/intersectionexplorer/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
