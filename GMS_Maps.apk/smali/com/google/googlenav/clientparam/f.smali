.class public final Lcom/google/googlenav/clientparam/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;

.field private static volatile b:Z

.field private static volatile c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private static volatile d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;

.field private static volatile e:Lcom/google/googlenav/clientparam/j;

.field private static volatile f:Lcom/google/googlenav/clientparam/e;

.field private static volatile g:Lcom/google/googlenav/clientparam/k;

.field private static volatile h:Lcom/google/googlenav/clientparam/d;

.field private static volatile i:Lcom/google/googlenav/clientparam/a;

.field private static volatile j:Lcom/google/googlenav/clientparam/b;

.field private static volatile k:Lcom/google/googlenav/clientparam/c;

.field private static volatile l:Las/d;

.field private static volatile m:Z

.field private static volatile n:Z

.field private static o:Ljava/lang/Object;

.field private static volatile p:Lcom/google/googlenav/clientparam/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/clientparam/f;->b:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    sput-boolean v1, Lcom/google/googlenav/clientparam/f;->m:Z

    sput-boolean v1, Lcom/google/googlenav/clientparam/f;->n:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->o:Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/ax;->i()Lcom/google/common/collect/ay;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/clientparam/f;->a:Ljava/util/Map;

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    invoke-static {}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->getDefaultProto()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    new-instance v0, Lcom/google/googlenav/clientparam/j;

    invoke-static {}, Lcom/google/googlenav/clientparam/j;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->e:Lcom/google/googlenav/clientparam/j;

    new-instance v0, Lcom/google/googlenav/clientparam/e;

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/e;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->f:Lcom/google/googlenav/clientparam/e;

    new-instance v0, Lcom/google/googlenav/clientparam/k;

    invoke-static {}, Lcom/google/googlenav/clientparam/k;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/k;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->g:Lcom/google/googlenav/clientparam/k;

    new-instance v0, Lcom/google/googlenav/clientparam/d;

    invoke-static {}, Lcom/google/googlenav/clientparam/d;->h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/d;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->h:Lcom/google/googlenav/clientparam/d;

    new-instance v0, Lcom/google/googlenav/clientparam/a;

    invoke-static {}, Lcom/google/googlenav/clientparam/a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->i:Lcom/google/googlenav/clientparam/a;

    new-instance v0, Lcom/google/googlenav/clientparam/b;

    invoke-static {}, Lcom/google/googlenav/clientparam/b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->j:Lcom/google/googlenav/clientparam/b;

    new-instance v0, Lcom/google/googlenav/clientparam/c;

    invoke-static {}, Lcom/google/googlenav/clientparam/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/clientparam/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, Lcom/google/googlenav/clientparam/f;->k:Lcom/google/googlenav/clientparam/c;

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Las/d;)Las/d;
    .locals 0

    sput-object p0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    return-object p0
.end method

.method public static declared-synchronized a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;
    .locals 2

    const-class v0, Lcom/google/googlenav/clientparam/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/clientparam/f;->d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Law/h;)V
    .locals 2

    const-class v1, Lcom/google/googlenav/clientparam/f;

    monitor-enter v1

    :try_start_0
    const-string v0, "ServerControlledParametersManager.data"

    invoke-static {p0, v0}, Lcom/google/googlenav/clientparam/f;->a(Law/h;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Law/h;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/googlenav/clientparam/f;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    sget-object v2, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/clientparam/f;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-static {p0, p1, v3}, Lcom/google/googlenav/clientparam/f;->b(Law/h;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Law/h;Ljava/lang/String;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/googlenav/clientparam/f;->b(Law/h;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/clientparam/i;)V
    .locals 0

    sput-object p0, Lcom/google/googlenav/clientparam/f;->p:Lcom/google/googlenav/clientparam/i;

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->am()Z

    move-result v1

    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/clientparam/f;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/google/googlenav/clientparam/f;->n:Z

    return p0
.end method

.method public static declared-synchronized b()Lcom/google/googlenav/clientparam/j;
    .locals 2

    const-class v0, Lcom/google/googlenav/clientparam/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/clientparam/f;->e:Lcom/google/googlenav/clientparam/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized b(Law/h;)V
    .locals 3

    const-class v1, Lcom/google/googlenav/clientparam/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/googlenav/clientparam/f;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    const-string v0, "ServerControlledParametersManager.data"

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/google/googlenav/clientparam/f;->b(Law/h;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Law/h;Ljava/lang/String;Z)V
    .locals 3

    const-class v1, Lcom/google/googlenav/clientparam/f;

    monitor-enter v1

    if-nez p0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/googlenav/clientparam/f;->o:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    :cond_1
    sget-boolean v0, Lcom/google/googlenav/clientparam/f;->n:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/clientparam/f;->m:Z

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Lcom/google/googlenav/clientparam/f;->n:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/googlenav/clientparam/f;->m:Z

    new-instance v0, Lcom/google/googlenav/clientparam/g;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/clientparam/g;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Law/h;->c(Law/g;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/clientparam/f;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()Lcom/google/googlenav/clientparam/e;
    .locals 2

    const-class v0, Lcom/google/googlenav/clientparam/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/clientparam/f;->f:Lcom/google/googlenav/clientparam/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-static {p0}, Lcom/google/googlenav/clientparam/f;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public static declared-synchronized d()Lcom/google/googlenav/clientparam/k;
    .locals 2

    const-class v0, Lcom/google/googlenav/clientparam/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/clientparam/f;->g:Lcom/google/googlenav/clientparam/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    invoke-static {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    sget-object v0, Lcom/google/googlenav/clientparam/f;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/clientparam/f;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/googlenav/clientparam/f;->d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/googlenav/clientparam/f;->d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->mergeEnabledFeatures(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->d:Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    goto :goto_1

    :pswitch_2
    new-instance v1, Lcom/google/googlenav/clientparam/j;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->e:Lcom/google/googlenav/clientparam/j;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->R()V

    move v0, v2

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/google/googlenav/clientparam/e;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/e;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->f:Lcom/google/googlenav/clientparam/e;

    move v0, v2

    goto :goto_0

    :pswitch_4
    new-instance v1, Lcom/google/googlenav/clientparam/k;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/k;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->g:Lcom/google/googlenav/clientparam/k;

    move v0, v2

    goto :goto_0

    :pswitch_5
    new-instance v1, Lcom/google/googlenav/clientparam/d;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/d;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->h:Lcom/google/googlenav/clientparam/d;

    move v0, v2

    goto :goto_0

    :pswitch_6
    new-instance v1, Lcom/google/googlenav/clientparam/a;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->i:Lcom/google/googlenav/clientparam/a;

    move v0, v2

    goto :goto_0

    :pswitch_7
    new-instance v1, Lcom/google/googlenav/clientparam/b;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->j:Lcom/google/googlenav/clientparam/b;

    move v0, v2

    goto :goto_0

    :pswitch_8
    new-instance v1, Lcom/google/googlenav/clientparam/c;

    invoke-direct {v1, v0}, Lcom/google/googlenav/clientparam/c;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v1, Lcom/google/googlenav/clientparam/f;->k:Lcom/google/googlenav/clientparam/c;

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static declared-synchronized e()Lcom/google/googlenav/clientparam/d;
    .locals 2

    const-class v0, Lcom/google/googlenav/clientparam/f;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/googlenav/clientparam/f;->h:Lcom/google/googlenav/clientparam/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    sget-object v3, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-ne v1, v3, :cond_2

    sget-object v1, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    :cond_1
    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static f()Lcom/google/googlenav/clientparam/a;
    .locals 1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->i:Lcom/google/googlenav/clientparam/a;

    return-object v0
.end method

.method static g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 6

    const/4 v5, 0x1

    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sget-object v0, Lcom/google/googlenav/clientparam/f;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->getDefaultProto()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/google/googlenav/clientparam/j;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_3
    invoke-static {}, Lcom/google/googlenav/clientparam/e;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_4
    invoke-static {}, Lcom/google/googlenav/clientparam/k;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_5
    invoke-static {}, Lcom/google/googlenav/clientparam/d;->h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_6
    invoke-static {}, Lcom/google/googlenav/clientparam/a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_7
    invoke-static {}, Lcom/google/googlenav/clientparam/b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :pswitch_8
    invoke-static {}, Lcom/google/googlenav/clientparam/c;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object v0, v2

    goto :goto_1

    :cond_1
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method static synthetic h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic i()Lcom/google/googlenav/clientparam/i;
    .locals 1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->p:Lcom/google/googlenav/clientparam/i;

    return-object v0
.end method

.method static synthetic j()Z
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/clientparam/f;->b:Z

    return v0
.end method

.method static synthetic k()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->o:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic l()Z
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/clientparam/f;->m:Z

    return v0
.end method

.method static synthetic m()Las/d;
    .locals 1

    sget-object v0, Lcom/google/googlenav/clientparam/f;->l:Las/d;

    return-object v0
.end method
