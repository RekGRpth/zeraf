.class public Lcom/google/googlenav/android/M;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/s;


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field private static final l:[Ljava/lang/String;

.field private static final m:Lcom/google/common/collect/ImmutableSet;


# instance fields
.field private final i:Lcom/google/googlenav/android/R;

.field private final j:Lcom/google/googlenav/ui/s;

.field private final k:LaN/u;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/checkin"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->a:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/auto-checkin"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->b:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/location-history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->c:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/location-history/enable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->d:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/list"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->f:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/choose-location"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->g:Landroid/net/Uri;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/manage-places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->h:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "client"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dc"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gl"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hl"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "source"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "tab"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "utm_source"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/android/M;->l:[Ljava/lang/String;

    new-instance v0, Lcom/google/common/collect/aD;

    invoke-direct {v0}, Lcom/google/common/collect/aD;-><init>()V

    sget-object v1, Lcom/google/googlenav/android/M;->l:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aD;->b([Ljava/lang/Object;)Lcom/google/common/collect/aD;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/aD;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->m:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    iput-object p2, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    iput-object p3, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    return-void
.end method

.method private a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;I)I
    .locals 1

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    :cond_0
    :goto_0
    return p3

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/android/M;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v2, -0x1

    const-string v0, "cid:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ne v1, v2, :cond_1

    :goto_1
    return-object p0

    :cond_0
    const-string v0, " cid:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v2, :cond_4

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "cid"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 12

    const/16 v7, 0x2c

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "geo"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri is not geo scheme."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri link contains no body."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ne v2, v4, :cond_2

    move-object v1, v0

    :goto_0
    if-ne v2, v4, :cond_3

    move-object v0, v3

    :goto_1
    invoke-static {v1}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-gez v2, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v1, v7, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    if-ltz v5, :cond_5

    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_5
    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const v1, 0xf4240

    :try_start_1
    invoke-static {v5}, LaN/B;->b(Ljava/lang/String;)I

    move-result v7

    invoke-static {v2}, LaN/B;->b(Ljava/lang/String;)I

    move-result v8

    const/16 v2, 0x2e

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-lez v2, :cond_6

    const-wide/high16 v9, 0x4024000000000000L

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v2, v1

    add-int/lit8 v1, v1, 0x7

    int-to-double v1, v1

    invoke-static {v9, v10, v1, v2}, Ljava/lang/Math;->pow(DD)D
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    double-to-int v1, v1

    :cond_6
    new-instance v9, Lcom/google/googlenav/android/Q;

    invoke-direct {v9}, Lcom/google/googlenav/android/Q;-><init>()V

    if-eqz v0, :cond_c

    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->parseQuery(Ljava/lang/String;)V

    const-string v2, "z"

    invoke-virtual {v9, v2}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v5, 0x16

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {}, LaN/Y;->e()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    :goto_2
    if-nez v7, :cond_7

    if-nez v8, :cond_7

    const/4 v5, 0x1

    :goto_3
    if-eqz v0, :cond_a

    const-string v0, "q"

    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-static {v1}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v5, v6

    :goto_4
    move-object v0, p0

    move v7, v6

    move v8, v6

    move-object v9, v3

    move v10, v6

    move-object v11, v3

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    :goto_5
    return-void

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v5, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    new-instance v10, LaN/B;

    invoke-direct {v10, v7, v8}, LaN/B;-><init>(II)V

    invoke-virtual {v5, v10}, LaN/u;->c(LaN/B;)V

    if-lez v2, :cond_8

    iget-object v1, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, LaN/u;->a(LaN/Y;)V

    move v5, v6

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v2, v1, v1}, LaN/u;->d(II)V

    move v5, v6

    goto :goto_3

    :cond_9
    const-string v0, "cbp"

    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    goto :goto_5

    :cond_b
    move-object v2, v3

    goto :goto_4

    :cond_c
    move v2, v6

    goto/16 :goto_2
.end method

.method private a(Ljava/lang/String;LaN/M;Z)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, v1, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bg;->a(LaN/M;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v2, 0x4f4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V
    .locals 11

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    move-object/from16 v0, p10

    invoke-interface {v1, p1, p2, v0}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    const-string v1, "19"

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x57

    const-string v2, "s"

    invoke-static {v1, v2, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "20"

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v2

    if-nez v2, :cond_1

    check-cast v1, Lbf/bk;

    invoke-virtual {v1, p1, p3, p4}, Lbf/bk;->a(Ljava/lang/String;LaN/H;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->au()V

    if-eqz p5, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/c;

    const/4 v2, 0x2

    invoke-direct {v5, v2}, Lcom/google/googlenav/c;-><init>(I)V

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p13

    move v6, p4

    move v7, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p9

    invoke-virtual/range {v1 .. v10}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/c;IIZZZ)V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    if-eqz p8, :cond_4

    :goto_1
    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->b(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p12

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->k(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const/16 v2, 0x4f4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto/16 :goto_0

    :cond_4
    const-string p8, "100"

    goto :goto_1

    :cond_5
    if-eqz p12, :cond_3

    const/16 v2, 0x4f3

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V
    .locals 14

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    move/from16 v12, p10

    move-object/from16 v13, p11

    invoke-direct/range {v0 .. v13}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V
    .locals 4

    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    invoke-virtual {v1}, Lbb/o;->i()V

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const-string v2, "s"

    invoke-virtual {v1, p0, v2, p2}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    invoke-static {p0}, Lau/b;->i(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lbb/o;->a(III)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    const-string v0, "saddr"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v0

    :goto_0
    const-string v2, "daddr"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v1

    :cond_0
    :goto_1
    new-instance v2, Lbm/b;

    invoke-direct {v2, p3}, Lbm/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lbm/b;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x2

    :goto_2
    new-instance v3, Lax/j;

    invoke-direct {v3, v0, v1}, Lax/j;-><init>(Lax/y;Lax/y;)V

    packed-switch v2, :pswitch_data_0

    new-instance v0, Lax/s;

    invoke-direct {v0, v3}, Lax/s;-><init>(Lax/k;)V

    :goto_3
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    return-void

    :cond_1
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lbm/b;->d()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v2, 0x1

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lbm/b;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x3

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Lbm/b;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v2

    goto :goto_2

    :pswitch_0
    new-instance v0, Lax/x;

    invoke-direct {v0, v3}, Lax/x;-><init>(Lax/k;)V

    goto :goto_3

    :pswitch_1
    new-instance v0, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    goto :goto_3

    :pswitch_2
    new-instance v0, Lax/i;

    invoke-direct {v0, v3}, Lax/i;-><init>(Lax/k;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0xf

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x4f4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .locals 3

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    const-string v0, "19"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "20"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_1
    return-void
.end method

.method private a(Landroid/net/UrlQuerySanitizer;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/net/UrlQuerySanitizer;->getParameterSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Lcom/google/googlenav/android/M;->m:Lcom/google/common/collect/ImmutableSet;

    invoke-virtual {v2, v0}, Lcom/google/common/collect/ImmutableSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LaN/B;->b(Ljava/lang/String;)I

    move-result v5

    aput v5, v1, v4

    const/4 v4, 0x1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaN/B;->b(Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;)I
    .locals 6

    new-instance v2, Law/r;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, p0}, Law/r;-><init>(Ljava/lang/String;Law/s;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    invoke-virtual {v2}, Law/r;->a()V

    const/4 v0, -0x2

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/android/M;)Lcom/google/googlenav/android/R;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    return-object v0
.end method

.method static b(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "cu"

    goto :goto_0
.end method

.method private b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, ";"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    move-object v0, p0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private b()V
    .locals 4

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/android/P;

    invoke-direct {v3, p0, v1}, Lcom/google/googlenav/android/P;-><init>(Lcom/google/googlenav/android/M;Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public static c(Ljava/lang/String;)LaN/B;
    .locals 4

    const/4 v0, 0x0

    const/4 v3, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, ";"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_0

    sget-object v0, Lbb/w;->a:Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_2

    const-string v0, ";"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, ";"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private c()Landroid/net/UrlQuerySanitizer;
    .locals 1

    new-instance v0, Lcom/google/googlenav/android/U;

    invoke-direct {v0}, Lcom/google/googlenav/android/U;-><init>()V

    return-object v0
.end method

.method private static c(Landroid/content/Intent;)Lcom/google/googlenav/ai;
    .locals 3

    const-string v0, "placemark-proto"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-static {v1}, Lcom/google/googlenav/ai;->a(Ljava/io/DataInput;)Lcom/google/googlenav/ai;

    move-result-object v0

    const-string v1, "insitu"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->e(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/net/Uri;)V
    .locals 4

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "output"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "kml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load My Maps with output="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v1, "output"

    const-string v2, "kml"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x9

    const/4 v3, 0x3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public static d(Ljava/lang/String;)I
    .locals 3

    const/4 v0, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lbb/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/lang/Integer;

    sget-object v2, Lbb/w;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private static d(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    const-string v0, "placemark-url"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/net/Uri;)Z
    .locals 10

    const/4 v9, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    if-lt v0, v9, :cond_1

    aget-object v0, v4, v3

    invoke-static {v0}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aget-object v0, v4, v2

    invoke-static {v0}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x0

    const/4 v0, 0x2

    :try_start_1
    aget-object v0, v4, v0

    invoke-static {v0}, LaN/B;->b(Ljava/lang/String;)I

    move-result v7

    const/4 v0, 0x3

    aget-object v0, v4, v0

    invoke-static {v0}, LaN/B;->b(Ljava/lang/String;)I

    move-result v8

    new-instance v0, LaN/B;

    invoke-direct {v0, v7, v8}, LaN/B;-><init>(II)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    const-string v1, ""

    array-length v7, v4

    const/4 v8, 0x5

    if-lt v7, v8, :cond_0

    aget-object v1, v4, v9

    invoke-static {v1}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v6}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_1

    invoke-direct {p0, v5, v6, v0, v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    move v0, v2

    :goto_1
    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "google.layeritemdetails uri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1
.end method

.method private static e(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0xa

    const-string v0, "intent-source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    const/16 v0, 0x35d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/android/O;

    invoke-direct {v2, p0}, Lcom/google/googlenav/android/O;-><init>(Lcom/google/googlenav/android/M;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private f(Landroid/content/Intent;)I
    .locals 6

    const-string v0, "intent_extra_data_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    const-string v3, "app_data"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    const-string v5, "skipSearchWizardOnBack"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v0, "skipSearchWizardOnBack"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :cond_1
    const-string v5, "searchUiSource"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v1, "searchUiSource"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v3, "19"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/android/M;->a(ZLjava/lang/String;)V

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent_extra_data_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_3
    const-string v3, "20"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    invoke-virtual {v3}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lbf/i;->av()I

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4, v3}, Lbf/am;->h(Lbf/i;)V

    :cond_4
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v2}, Lcom/google/googlenav/android/R;->showStarDetails(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->au()V

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v2}, Lcom/google/googlenav/android/R;->showStarOnMap(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static f(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    const-string v1, "cid:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, " cid:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Landroid/content/Intent;)I
    .locals 3

    const-string v0, "intent_extra_data_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v0}, Lcom/google/googlenav/android/R;->showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "intent_extra_data_key"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    const/4 v0, -0x2

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    const-string v1, "by:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, " by:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Landroid/content/Intent;)I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const-string v1, "stars"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent_extra_data_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    const/4 v0, -0x2

    return v0
.end method

.method private i(Landroid/content/Intent;)I
    .locals 18

    const-string v1, "google.star"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->f(Landroid/content/Intent;)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "google.recentplace"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->g(Landroid/content/Intent;)I

    move-result v1

    goto :goto_0

    :cond_1
    const-string v1, "google.myplaces_panel"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->h(Landroid/content/Intent;)I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->j(Landroid/content/Intent;)I

    move-result v1

    goto :goto_0

    :cond_3
    const-string v1, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "intent_extra_data_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v16 .. v16}, Lcom/google/googlenav/android/M;->c(Ljava/lang/String;)LaN/B;

    move-result-object v11

    if-nez v1, :cond_6

    const-string v2, ""

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->t()Z

    move-result v8

    const-string v1, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_7

    :cond_4
    const/4 v1, 0x1

    :goto_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/googlenav/K;->an()Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v1, 0x0

    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    :goto_3
    const/4 v12, 0x0

    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-virtual {v5}, LaN/u;->f()LaN/H;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-virtual {v5, v11}, LaN/u;->c(LaN/B;)V

    :cond_5
    if-nez v1, :cond_10

    const/4 v5, -0x1

    const/4 v7, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v14}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V

    :goto_4
    sget-object v1, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    move-object/from16 v0, v16

    invoke-static {v2, v0, v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v9}, Lcom/google/googlenav/android/M;->a(ZLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    const/16 v1, 0x15

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    if-nez v1, :cond_12

    const-string v9, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    const-string v13, "centerLatitude"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    const-string v13, "centerLongitude"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    const-string v13, "centerLatitude"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    const-string v14, "centerLongitude"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    new-instance v17, LaN/B;

    move-object/from16 v0, v17

    invoke-direct {v0, v13, v14}, LaN/B;-><init>(II)V

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, LaN/u;->c(LaN/B;)V

    :cond_9
    const-string v13, "zoomLevel"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    const-string v14, "zoomLevel"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, LaN/Y;->b(I)LaN/Y;

    move-result-object v14

    invoke-virtual {v13, v14}, LaN/u;->a(LaN/Y;)V

    :cond_a
    :goto_5
    const-string v13, "routePolyline"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    :try_start_0
    new-instance v13, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v14, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v14, "routePolyline"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v14

    if-eqz v14, :cond_b

    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_b

    const/4 v14, 0x3

    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v13

    invoke-static {v13}, LaN/M;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/M;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :cond_b
    :goto_6
    const-string v13, "skipSearchWizardOnBack"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    const-string v5, "skipSearchWizardOnBack"

    invoke-virtual {v9, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    :cond_c
    const-string v13, "searchUiSource"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_d

    const-string v6, "searchUiSource"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :cond_d
    const-string v13, "searchNearBy"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v7}, LaN/u;->f()LaN/H;

    move-result-object v7

    const-string v8, "searchNearBy"

    invoke-virtual {v9, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v8

    invoke-virtual {v7, v8}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v7

    const/4 v8, 0x1

    :cond_e
    const-string v13, "searchIncludeInHistory"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    const-string v10, "searchIncludeInHistory"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_3

    :cond_f
    const-string v13, "latitudeSpan"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    const-string v13, "longitudeSpan"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    const-string v13, "latitudeSpan"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    const-string v14, "longitudeSpan"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v15, v13, v14}, LaN/u;->d(II)V

    goto/16 :goto_5

    :cond_10
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1, v10}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;LaN/M;Z)V

    goto/16 :goto_4

    :catch_0
    move-exception v13

    goto :goto_6

    :cond_11
    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_3

    :cond_12
    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_3
.end method

.method private j(Landroid/content/Intent;)I
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb/o;->c(I)Lbb/w;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    invoke-virtual {v0}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lbb/o;->i()V

    invoke-static {v2}, Lau/b;->i(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3, v4, v4}, Lbb/o;->a(III)V

    const-string v3, "s"

    sget-object v4, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-virtual {v1, v2, v3, v4}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    invoke-virtual {v0}, Lbb/w;->e()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbf/am;->a(Lbb/w;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lbb/w;->e()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_2
    const/16 v0, 0x15

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(Landroid/content/Intent;)I
    .locals 13

    const/4 v12, -0x1

    const/4 v1, -0x2

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->resolveType(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "vnd.android.cursor.item/postal-address"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    const/16 v12, 0x12

    :cond_1
    :goto_0
    return v12

    :cond_2
    const-string v2, "geo"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/Uri;)V

    const/16 v12, 0x14

    goto :goto_0

    :cond_3
    const-string v2, "http"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "https"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    const-string v2, "application/vnd.google-earth.kml+xml"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-direct {p0, v0, v0, v2, v3}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;II)V

    move v0, v1

    :goto_1
    if-ne v0, v1, :cond_5

    const/16 v0, 0x13

    :cond_5
    move v12, v0

    goto :goto_0

    :cond_6
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->o(Landroid/content/Intent;)I

    move-result v0

    goto :goto_1

    :cond_7
    const-string v0, "rideabout"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x61

    const-string v1, "n"

    const-string v2, "n"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->m(Landroid/content/Intent;)I

    move-result v12

    goto :goto_0

    :cond_8
    const-string v0, "latitude"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->n(Landroid/content/Intent;)I

    move-result v12

    goto :goto_0

    :cond_9
    const-string v0, "google.layeritemdetails"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->d(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x18

    :goto_2
    move v12, v0

    goto/16 :goto_0

    :cond_a
    move v0, v12

    goto :goto_2

    :cond_b
    const-string v0, "google.businessdetails"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {p1}, Lcom/google/googlenav/android/M;->c(Landroid/content/Intent;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V

    move v12, v1

    goto/16 :goto_0

    :cond_c
    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    const-string v2, "insitu"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    move v12, v1

    goto/16 :goto_0

    :cond_d
    const-string v0, "google.businessratings"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-static {p1}, Lcom/google/googlenav/android/M;->c(Landroid/content/Intent;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/android/M;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_e

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/android/R;->startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V

    move v12, v1

    goto/16 :goto_0

    :cond_e
    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    const-string v3, "insitu"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v4, v0, v2, v3}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    move v12, v1

    goto/16 :goto_0

    :cond_f
    const-string v0, "google.star"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->f(Landroid/content/Intent;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method private l(Landroid/content/Intent;)V
    .locals 3

    const/16 v0, 0x34

    const-string v1, "x"

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startNextMatchingActivity(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x6d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private m(Landroid/content/Intent;)I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitNavigationLayer()V

    const/16 v0, 0x1b

    return v0
.end method

.method private n(Landroid/content/Intent;)I
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/list"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    :goto_0
    const-string v0, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_SIGN_IN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_SIGN_IN"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/ad;->o()V

    :cond_0
    const/16 v0, 0x1a

    return v0

    :cond_1
    const-string v3, "/map"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    goto :goto_0

    :cond_2
    const-string v3, "/friends/location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsLocation(Lbf/am;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v3, "/friends/profile"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsProfile(Lbf/am;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "/auto-checkin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "notification_fired"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->f(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    goto :goto_0

    :cond_6
    const-string v0, "/checkin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, p1}, Lcom/google/googlenav/android/M;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "/choose-location"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "start_activity_on_complete"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "/location-history/enable"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startLocationHistoryOptIn(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "/location-history"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startFriendsLayerHistorySummary()V

    goto/16 :goto_0

    :cond_a
    const-string v0, "/manage-places"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startManageAutoCheckinPlaces()V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    goto/16 :goto_0
.end method

.method private o(Landroid/content/Intent;)I
    .locals 13

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/u/m/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v4}, Lcom/google/googlenav/android/M;->b(Landroid/net/Uri;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v4}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/latitude"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    iget-object v5, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/googlenav/android/M;->c()Landroid/net/UrlQuerySanitizer;

    move-result-object v7

    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    const-string v0, "lci"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "cbll"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "google.streetview:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "cbll="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "cbp"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v2, "&cbp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v0, "panoid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v2, "&panoid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->c(Ljava/lang/String;)V

    const/4 v0, -0x2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Uri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/my-places"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v0, "offline"

    invoke-virtual {v4}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const-string v1, "offline"

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    :goto_1
    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/offers-list"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startOffersList()V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/transit-entry"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitEntry()V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_a
    const-string v0, "nav"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "daddr"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "sll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    if-eqz v0, :cond_14

    const-string v0, "sll"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const-string v3, "sspn"

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v3

    if-eqz v3, :cond_15

    const-string v3, "sspn"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_3
    const-string v6, "dirflg"

    invoke-virtual {v7, v6}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "entry"

    invoke-virtual {v7, v8}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v0, v3, v6, v8}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    :cond_b
    const-string v0, "t"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "k"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "h"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(I)Z

    move-result v1

    :cond_d
    :goto_4
    const-string v0, "layer"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    const/16 v3, 0x74

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    const/4 v1, 0x1

    :cond_e
    const-string v0, "q"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1a

    const/4 v3, 0x0

    const-string v0, "sll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v8

    if-eqz v8, :cond_f

    new-instance v0, LaN/H;

    new-instance v1, LaN/B;

    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v3, 0x1

    aget v3, v8, v3

    invoke-direct {v1, v2, v3}, LaN/B;-><init>(II)V

    const/16 v2, 0xa

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->b()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->c()Z

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->P()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    move-object v3, v0

    :cond_f
    const/4 v4, -0x1

    const-string v0, "sspn"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v1, 0x0

    aget v4, v0, v1

    :cond_10
    const/4 v0, 0x0

    invoke-static {v6}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, "cid"

    invoke-direct {p0, v7, v1}, Lcom/google/googlenav/android/M;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_11

    const-string v1, "latlng"

    invoke-direct {p0, v7, v1}, Lcom/google/googlenav/android/M;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_11
    if-eqz v1, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " cid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v12, v6

    move-object v6, v0

    move-object v0, v12

    :cond_12
    move-object v2, v0

    move-object v1, v6

    :goto_5
    const/4 v6, 0x0

    const-string v0, "iwd"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "1"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v6, 0x1

    :cond_13
    invoke-static {v1}, Lcom/google/googlenav/android/M;->g(Ljava/lang/String;)Z

    move-result v10

    const-string v0, "hnear"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-nez v8, :cond_18

    invoke-static {v1}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    invoke-direct {p0, v1, v2, v6, v0}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    :goto_6
    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_15
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_16
    const-string v3, "m"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(I)Z

    move-result v1

    goto/16 :goto_4

    :cond_17
    invoke-static {v6}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v6

    goto :goto_5

    :cond_18
    if-nez v8, :cond_19

    if-nez v10, :cond_19

    const/4 v5, 0x1

    :goto_7
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    goto :goto_6

    :cond_19
    const/4 v5, 0x0

    goto :goto_7

    :cond_1a
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v3, "/maps/place"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "cid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "georestrict"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/googlenav/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    const-string v0, "uc"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    const-string v2, "insitu"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v0, 0x1

    :cond_1b
    const-string v1, "openratings"

    const-string v2, "action"

    invoke-virtual {v7, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-static {p1}, Lcom/google/googlenav/android/M;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2, v0}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_8
    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_1c
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v3, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    goto :goto_8

    :cond_1d
    const-string v0, "ftid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1e

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v0}, Lcom/google/googlenav/android/R;->startTransitStationPage(Ljava/lang/String;)V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_1e
    const-string v0, "saddr"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "daddr"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "dirflg"

    invoke-virtual {v7, v5}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "myl"

    invoke-virtual {v7, v6}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v0, :cond_1f

    if-eqz v3, :cond_20

    :cond_1f
    invoke-direct {p0, v0, v3, v5, v6}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_20
    const-string v0, "msid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_21

    invoke-direct {p0, v4}, Lcom/google/googlenav/android/M;->c(Landroid/net/Uri;)V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_21
    const-string v0, "ll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    const-string v3, "spn"

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v3

    const-string v5, "z"

    const/4 v6, -0x1

    invoke-direct {p0, v7, v5, v6}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;I)I

    move-result v5

    if-eqz v0, :cond_29

    new-instance v1, LaN/B;

    const/4 v6, 0x0

    aget v6, v0, v6

    const/4 v8, 0x1

    aget v0, v0, v8

    invoke-direct {v1, v6, v0}, LaN/B;-><init>(II)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    const/4 v0, 0x1

    :goto_9
    if-eqz v3, :cond_22

    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    const/4 v1, 0x0

    aget v1, v3, v1

    const/4 v6, 0x1

    aget v6, v3, v6

    invoke-virtual {v0, v1, v6}, LaN/u;->d(II)V

    const/4 v0, 0x1

    :cond_22
    if-nez v3, :cond_23

    if-ltz v5, :cond_23

    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-static {v5}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->a(LaN/Y;)V

    const/4 v0, 0x1

    :cond_23
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    const-string v1, "cid"

    invoke-virtual {v7, v1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_25

    invoke-static {v1}, Lcom/google/googlenav/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_25

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    const-string v0, "uc"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    const-string v3, "insitu"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    const/4 v0, 0x1

    :cond_24
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_25
    if-nez v0, :cond_26

    if-eqz v2, :cond_28

    :cond_26
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    if-eqz v0, :cond_27

    const/4 v0, -0x2

    goto/16 :goto_0

    :cond_27
    const/4 v0, -0x3

    goto/16 :goto_0

    :cond_28
    const/4 v0, -0x1

    goto/16 :goto_0

    :cond_29
    move v0, v1

    goto :goto_9
.end method


# virtual methods
.method public a()I
    .locals 6

    const/4 v0, -0x2

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v4

    if-eqz v3, :cond_2

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const-string v4, "android.intent.action.SEARCH"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->i(Landroid/content/Intent;)I

    move-result v0

    goto :goto_0

    :cond_4
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/android/M;->a(Landroid/content/Intent;Z)I

    move-result v0

    goto :goto_0

    :cond_5
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_6
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->k(Landroid/content/Intent;)I

    move-result v0

    if-ne v0, v1, :cond_7

    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->l(Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    const/4 v1, -0x3

    if-ne v0, v1, :cond_8

    const/16 v0, 0x13

    goto :goto_0

    :cond_8
    const/16 v1, 0x1b

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    goto :goto_0

    :cond_9
    const-string v4, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->startSettingsActivity()V

    goto :goto_0

    :cond_a
    const-string v4, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->startLatitudeSettingsActivity()V

    goto :goto_0

    :cond_b
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->l(Landroid/content/Intent;)V

    move v0, v1

    goto :goto_0
.end method

.method a(Landroid/content/Intent;Z)I
    .locals 8

    const v7, 0xf4240

    const/16 v4, 0x65

    const/4 v6, 0x0

    const/4 v5, -0x2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0, v1}, Lbm/a;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbm/a;->b(Ljava/lang/String;)[F

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "pw"

    const-string v1, "ng"

    invoke-static {v4, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x371

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->e(Ljava/lang/String;)V

    :goto_0
    return v5

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "application/vnd.google.panorama360+jpg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v0, "pw"

    const-string v1, "wm"

    invoke-static {v4, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x36f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v2, LaN/B;

    aget v3, v0, v6

    float-to-int v3, v3

    mul-int/2addr v3, v7

    const/4 v4, 0x1

    aget v0, v0, v4

    float-to-int v0, v0

    mul-int/2addr v0, v7

    invoke-direct {v2, v3, v0}, LaN/B;-><init>(II)V

    new-instance v0, Lcom/google/googlenav/ai;

    const/16 v3, 0x374

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    const-string v2, "gi"

    new-instance v3, Lcom/google/googlenav/android/N;

    invoke-direct {v3, p0}, Lcom/google/googlenav/android/N;-><init>(Lcom/google/googlenav/android/M;)V

    iget-object v4, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v4, v0, v1, v2, v3}, Lcom/google/googlenav/android/R;->startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method a(Landroid/content/Intent;)V
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/h;

    invoke-direct {v0, v2}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "notification_fired"

    invoke-virtual {p1, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v3, v1, v4

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    sub-long v1, v3, v1

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/h;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/friend/ad;->a(JLjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-static {p1}, Lcom/google/googlenav/android/M;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "optout"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/googlenav/android/R;->startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V

    return-void

    :catch_0
    move-exception v0

    const-string v2, "ANDROID_INTENT"

    invoke-static {v2, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    const/16 v0, 0x34

    const-string v1, "f"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x6d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/android/M;->b()V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/android/M;->b()V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-class v2, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
