.class public Lcom/google/googlenav/android/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/b;


# static fields
.field static final synthetic a:Z

.field private static b:Lcom/google/googlenav/android/c;


# instance fields
.field private final c:Lcom/google/googlenav/android/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/android/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/android/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/googlenav/android/AndroidGmmApplication;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Maps"

    const-string v1, "Build: 6140304"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/android/i;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, Lcom/google/googlenav/android/i;-><init>(Lcom/google/googlenav/android/AndroidGmmApplication;Las/c;Law/h;)V

    iput-object v1, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->L()Z

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/c;->a(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/android/c;->b(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/content/Context;)Law/h;
    .locals 3

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "GMM"

    invoke-static {}, LJ/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Law/h;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/googlenav/clientparam/f;->a(Law/h;)V

    return-object v0
.end method

.method public static a()Lcom/google/googlenav/android/c;
    .locals 1

    sget-boolean v0, Lcom/google/googlenav/android/c;->a:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    return-object v0
.end method

.method public static a(Landroid/app/Application;)Lcom/google/googlenav/android/c;
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    if-nez v0, :cond_0

    const-string v0, "AndroidGmmApplicationDelegate.getInstance"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    check-cast p0, Lcom/google/googlenav/android/AndroidGmmApplication;

    new-instance v0, Lcom/google/googlenav/android/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/c;-><init>(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    sput-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a(Lcom/google/googlenav/android/b;)V

    const-string v0, "AndroidGmmApplicationDelegate.getInstance"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    invoke-static {}, Lbm/m;->b()Lbm/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/android/I;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/android/I;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lbm/m;->a(Lbm/q;)V

    :cond_0
    invoke-static {}, LaL/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, LaL/a;

    invoke-direct {v0}, LaL/a;-><init>()V

    invoke-static {v0}, LaL/d;->a(LaL/e;)V

    :cond_1
    invoke-static {}, LaI/b;->a()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, LaI/a;

    invoke-direct {v0}, LaI/a;-><init>()V

    invoke-static {v0}, LaI/b;->a(LaI/c;)V

    :cond_2
    return-void
.end method

.method private a(Lcom/google/googlenav/android/AndroidGmmApplication;)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->M()Lcom/google/googlenav/O;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/googlenav/O;->c:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/friend/az;->b()V

    invoke-static {}, Lcom/google/googlenav/friend/E;->k()V

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->p()V

    invoke-static {}, Lcom/google/googlenav/friend/ad;->I()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, v0, Lcom/google/googlenav/O;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Maps"

    const-string v2, "Upgrading friends opt in, now split reporting."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, v0, Lcom/google/googlenav/O;->b:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/friend/E;->l()V

    invoke-static {}, Lcom/google/googlenav/friend/as;->l()V

    invoke-static {}, Lcom/google/googlenav/friend/az;->c()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/K;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    invoke-static {p1}, Lcom/google/googlenav/friend/reporting/r;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Law/h;
    .locals 2

    const-string v0, "AndroidGmmApplicationDelegate.startUpDispatcher"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->F()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/googlenav/android/c;->a(Ljava/lang/String;Landroid/content/Context;)Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->v()V

    const-string v1, "AndroidGmmApplicationDelegate.startUpDispatcher"

    invoke-static {v1}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-object v0
.end method

.method private b(Lcom/google/googlenav/android/AndroidGmmApplication;)V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/friend/ac;->e()Lcom/google/googlenav/friend/bi;

    move-result-object v0

    new-instance v1, Lak/b;

    invoke-direct {v1, p1}, Lak/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bi;->a(LaM/h;)V

    new-instance v0, Lcom/google/googlenav/networkinitiated/c;

    invoke-direct {v0, p1}, Lcom/google/googlenav/networkinitiated/c;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/googlenav/friend/ac;->e()Lcom/google/googlenav/friend/bi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bi;->a(LaM/h;)V

    invoke-virtual {v0}, Lcom/google/googlenav/networkinitiated/c;->e()V

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-object v0, Lcom/google/googlenav/android/c;->b:Lcom/google/googlenav/android/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()V
    .locals 0

    invoke-static {}, Lbm/m;->d()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/Config;->a(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/android/i;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public c()Lcom/google/googlenav/android/i;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    return-object v0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->c()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->i(Z)V

    :cond_1
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->u()V

    invoke-static {}, Law/h;->c()V

    :cond_2
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, LaH/o;->n()V

    :cond_3
    return-void
.end method

.method public e()V
    .locals 0

    invoke-static {}, Lcom/google/googlenav/common/k;->a()V

    return-void
.end method

.method public i()Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/android/c;->c:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    return-object v0
.end method
