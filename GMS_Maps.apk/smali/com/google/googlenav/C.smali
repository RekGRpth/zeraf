.class public Lcom/google/googlenav/C;
.super Law/a;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Ljava/util/List;

.field private final c:J

.field private final d:Ljava/util/List;

.field private final e:Lcom/google/googlenav/D;

.field private f:I


# direct methods
.method public constructor <init>(JLjava/util/List;Lcom/google/googlenav/D;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/C;->b:Ljava/util/List;

    iput-wide p1, p0, Lcom/google/googlenav/C;->c:J

    iput-object p3, p0, Lcom/google/googlenav/C;->d:Ljava/util/List;

    iput-object p4, p0, Lcom/google/googlenav/C;->e:Lcom/google/googlenav/D;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 6

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/googlenav/C;->c:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/C;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    const/4 v3, 0x2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/iI;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-nez v3, :cond_0

    iput-boolean v0, p0, Lcom/google/googlenav/C;->a:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/C;->f:I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/googlenav/C;->f:I

    iget v2, p0, Lcom/google/googlenav/C;->f:I

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/googlenav/C;->a:Z

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x78

    return v0
.end method

.method public b_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/C;->e:Lcom/google/googlenav/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/C;->e:Lcom/google/googlenav/D;

    iget-boolean v1, p0, Lcom/google/googlenav/C;->a:Z

    invoke-interface {v0, v1}, Lcom/google/googlenav/D;->a(Z)V

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/C;->e:Lcom/google/googlenav/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/C;->e:Lcom/google/googlenav/D;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/D;->a(Z)V

    :cond_0
    return-void
.end method
