.class Lcom/google/googlenav/ui/bq;
.super Landroid/text/style/ReplacementSpan;
.source "SourceFile"


# instance fields
.field private final a:Lam/g;

.field private final b:Lan/e;


# direct methods
.method public constructor <init>(Lam/g;)V
    .locals 2

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    new-instance v0, Lan/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lan/e;-><init>(Landroid/graphics/Canvas;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/bq;->b:Lan/e;

    iput-object p1, p0, Lcom/google/googlenav/ui/bq;->a:Lam/g;

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5

    add-int/lit8 v0, p4, -0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/bq;->b:Lan/e;

    invoke-virtual {v1, p1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    add-int v1, p6, p8

    iget-object v2, p0, Lcom/google/googlenav/ui/bq;->a:Lam/g;

    invoke-interface {v2, v0}, Lam/g;->b(C)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/ui/bq;->a:Lam/g;

    iget-object v3, p0, Lcom/google/googlenav/ui/bq;->b:Lan/e;

    float-to-int v4, p5

    invoke-interface {v2, v0, v3, v4, v1}, Lam/g;->a(CLam/e;II)Z

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    add-int/lit8 v0, p4, -0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/bq;->a:Lam/g;

    invoke-interface {v1, v0}, Lam/g;->c(C)I

    move-result v0

    return v0
.end method
