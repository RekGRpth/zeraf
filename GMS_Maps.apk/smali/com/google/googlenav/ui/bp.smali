.class Lcom/google/googlenav/ui/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:C

.field final b:J


# direct methods
.method constructor <init>(CJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-char p1, p0, Lcom/google/googlenav/ui/bp;->a:C

    iput-wide p2, p0, Lcom/google/googlenav/ui/bp;->b:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/googlenav/ui/bp;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/googlenav/ui/bp;

    iget-wide v1, p0, Lcom/google/googlenav/ui/bp;->b:J

    iget-wide v3, p1, Lcom/google/googlenav/ui/bp;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-char v1, p0, Lcom/google/googlenav/ui/bp;->a:C

    iget-char v2, p1, Lcom/google/googlenav/ui/bp;->a:C

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-char v2, p0, Lcom/google/googlenav/ui/bp;->a:C

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/googlenav/ui/bp;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
