.class Lcom/google/googlenav/ui/S;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/s;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/s;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/S;->a:Lcom/google/googlenav/ui/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/S;-><init>(Lcom/google/googlenav/ui/s;)V

    return-void
.end method

.method private c(Law/g;)Z
    .locals 1

    invoke-interface {p1}, Law/g;->A_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Law/g;->s_()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(IZLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/S;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Law/g;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/S;->c(Law/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/S;->a:Lcom/google/googlenav/ui/s;

    invoke-static {v1}, Lcom/google/googlenav/ui/s;->g(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/T;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/T;-><init>(Lcom/google/googlenav/ui/S;Law/g;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method public b(Law/g;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/S;->c(Law/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/S;->a:Lcom/google/googlenav/ui/s;

    invoke-static {v1}, Lcom/google/googlenav/ui/s;->g(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/U;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/U;-><init>(Lcom/google/googlenav/ui/S;Law/g;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method
