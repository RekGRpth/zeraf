.class public Lcom/google/googlenav/ui/view/android/bF;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/googlenav/ui/view/android/bF;


# instance fields
.field private b:Lcom/google/googlenav/ui/view/android/bH;

.field private c:Z

.field private d:Lcom/google/android/maps/rideabout/view/j;

.field private e:Lcom/google/android/maps/rideabout/view/i;

.field private f:Lcom/google/android/maps/rideabout/view/h;

.field private g:Lcom/google/android/maps/rideabout/view/c;

.field private h:Lcom/google/googlenav/ui/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/bF;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/bF;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/android/bF;->a:Lcom/google/googlenav/ui/view/android/bF;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/googlenav/ui/view/android/bH;->a:Lcom/google/googlenav/ui/view/android/bH;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    sget-object v0, Lcom/google/android/maps/rideabout/view/g;->a:Lcom/google/android/maps/rideabout/view/j;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    sget-object v0, Lcom/google/android/maps/rideabout/view/f;->a:Lcom/google/android/maps/rideabout/view/i;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    sget-object v0, Lcom/google/android/maps/rideabout/view/e;->a:Lcom/google/android/maps/rideabout/view/h;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->f:Lcom/google/android/maps/rideabout/view/h;

    return-void
.end method

.method public static a()Lcom/google/googlenav/ui/view/android/bF;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/bF;->a:Lcom/google/googlenav/ui/view/android/bF;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/maps/rideabout/view/c;)V
    .locals 1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    invoke-interface {v0, p1}, Lcom/google/android/maps/rideabout/view/j;->a(Lcom/google/android/maps/rideabout/view/c;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    invoke-interface {v0, p1}, Lcom/google/android/maps/rideabout/view/i;->a(Lcom/google/android/maps/rideabout/view/c;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/maps/rideabout/view/c;->f()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/rideabout/view/h;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bF;->f:Lcom/google/android/maps/rideabout/view/h;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/rideabout/view/i;)V
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->a(Lcom/google/android/maps/rideabout/view/c;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/rideabout/view/j;)V
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {p1, v0}, Lcom/google/android/maps/rideabout/view/j;->a(Lcom/google/android/maps/rideabout/view/c;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bF;->h:Lcom/google/googlenav/ui/e;

    return-void
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->f()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->a(Lcom/google/android/maps/rideabout/view/c;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->a(Lcom/google/android/maps/rideabout/view/c;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->e()V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->b(Lcom/google/android/maps/rideabout/view/c;)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/bH;->b:Lcom/google/googlenav/ui/view/android/bH;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->g()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/view/android/bH;->a:Lcom/google/googlenav/ui/view/android/bH;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->f()V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/j;->b(Lcom/google/android/maps/rideabout/view/c;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/i;->d()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    goto :goto_0
.end method

.method public h()Lcom/google/android/maps/rideabout/view/j;
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    sget-object v1, Lcom/google/googlenav/ui/view/android/bH;->b:Lcom/google/googlenav/ui/view/android/bH;

    if-ne v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/maps/rideabout/view/g;->a:Lcom/google/android/maps/rideabout/view/j;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    goto :goto_0
.end method

.method public i()Lcom/google/android/maps/rideabout/view/i;
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/rideabout/view/f;->a:Lcom/google/android/maps/rideabout/view/i;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/bG;->a:[I

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/bH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->h()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public j()Lcom/google/android/maps/rideabout/view/h;
    .locals 2

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bF;->c:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/maps/rideabout/view/e;->a:Lcom/google/android/maps/rideabout/view/h;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/view/android/bG;->a:[I

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->b:Lcom/google/googlenav/ui/view/android/bH;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/bH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/j;->i()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->f:Lcom/google/android/maps/rideabout/view/h;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public k()Lcom/google/android/maps/rideabout/view/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    return-object v0
.end method

.method public l()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bF;->g()V

    sget-object v0, Lcom/google/android/maps/rideabout/view/g;->a:Lcom/google/android/maps/rideabout/view/j;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    sget-object v0, Lcom/google/android/maps/rideabout/view/f;->a:Lcom/google/android/maps/rideabout/view/i;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->e:Lcom/google/android/maps/rideabout/view/i;

    sget-object v0, Lcom/google/android/maps/rideabout/view/e;->a:Lcom/google/android/maps/rideabout/view/h;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->f:Lcom/google/android/maps/rideabout/view/h;

    return-void
.end method

.method public m()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->h:Lcom/google/googlenav/ui/e;

    return-void
.end method

.method public n()Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->h:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method public o()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bF;->g:Lcom/google/android/maps/rideabout/view/c;

    invoke-virtual {v0}, Lcom/google/android/maps/rideabout/view/c;->h()LW/h;

    move-result-object v0

    invoke-virtual {v0}, LW/h;->c()LW/j;

    move-result-object v0

    invoke-virtual {v0}, LW/j;->c()Lbi/a;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bF;->d:Lcom/google/android/maps/rideabout/view/j;

    invoke-interface {v1, v0}, Lcom/google/android/maps/rideabout/view/j;->setLocationMarkerVisibleOnScreen(Lbi/a;)V

    return-void
.end method
