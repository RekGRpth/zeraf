.class Lcom/google/googlenav/ui/view/android/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/bB;


# instance fields
.field A:Landroid/widget/ImageView;

.field B:Lcom/google/googlenav/ui/bD;

.field C:Lcom/google/googlenav/ui/view/android/bw;

.field D:Lcom/google/googlenav/ui/view/android/bw;

.field E:Lcom/google/googlenav/ui/view/android/bw;

.field F:Lcom/google/googlenav/ui/view/android/bu;

.field G:Lcom/google/googlenav/ui/view/android/bx;

.field H:Lcom/google/googlenav/ui/view/android/by;

.field I:Lcom/google/googlenav/ui/view/android/bz;

.field J:Lcom/google/googlenav/ui/view/android/bz;

.field a:Landroid/view/View;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/CheckBox;

.field d:[Landroid/widget/TextView;

.field e:[Landroid/widget/TextView;

.field f:[Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Lcom/google/googlenav/ui/view/android/DistanceView;

.field i:Lcom/google/googlenav/ui/view/android/HeadingView;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/TextView;

.field p:Landroid/view/View;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/TextView;

.field s:Landroid/widget/TextView;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/ImageView;

.field x:Landroid/widget/ImageView;

.field y:Landroid/widget/LinearLayout;

.field z:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bx;->a:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/by;->a:Lcom/google/googlenav/ui/view/a;

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bu;->a:Ljava/lang/String;

    :cond_7
    return-void
.end method
