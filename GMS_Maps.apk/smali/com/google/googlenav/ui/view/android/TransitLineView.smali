.class public Lcom/google/googlenav/ui/view/android/TransitLineView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a()V

    return-void
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/view/android/TransitLineView;->setOrientation(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0401b0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method private a([[Lcom/google/googlenav/bO;Lcom/google/googlenav/ui/view/android/aP;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    new-instance v1, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;-><init>(Landroid/content/Context;)V

    rem-int/lit8 v2, v0, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->setBackgroundColor(I)V

    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {v1, v2, p2}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->setTimeTableRow([Lcom/google/googlenav/bO;Lcom/google/googlenav/ui/view/android/aP;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/android/aS;[Lcom/google/googlenav/ui/view/android/aT;I[[Lcom/google/googlenav/bO;Z)V
    .locals 1

    const v0, 0x7f10042c

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    const v0, 0x7f10042e

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->a:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0, p5}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setDrawLineAfterLastStation(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setDirectionSelectionListener(Lcom/google/googlenav/ui/view/android/aS;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setStations([Lcom/google/googlenav/ui/view/android/aT;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setLineColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a()Lcom/google/googlenav/ui/view/android/aP;

    move-result-object v0

    invoke-direct {p0, p4, v0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a([[Lcom/google/googlenav/bO;Lcom/google/googlenav/ui/view/android/aP;)V

    return-void
.end method

.method public a([Lcom/google/googlenav/ui/view/android/aT;[[Lcom/google/googlenav/bO;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setStations([Lcom/google/googlenav/ui/view/android/aT;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitLineView;->b:Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a()Lcom/google/googlenav/ui/view/android/aP;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/TransitLineView;->a([[Lcom/google/googlenav/bO;Lcom/google/googlenav/ui/view/android/aP;)V

    return-void
.end method
