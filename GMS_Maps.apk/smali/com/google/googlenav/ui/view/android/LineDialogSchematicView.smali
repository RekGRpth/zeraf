.class public Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/RectF;

.field private c:Lcom/google/googlenav/ui/view/android/rideabout/r;

.field private d:I

.field private e:I

.field private f:I

.field private g:F

.field private h:I

.field private i:F

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:[Lcom/google/googlenav/ui/view/android/aT;

.field private t:I

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/GestureDetector;

.field private w:Lcom/google/googlenav/ui/view/android/aP;

.field private x:Lcom/google/googlenav/ui/view/android/aS;

.field private y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b()V

    return-void
.end method

.method private a(FI)I
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aP;->c()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    int-to-float v1, p2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->c()I

    move-result v1

    int-to-float v1, v1

    sub-float v1, p1, v1

    int-to-float v2, p2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v2, v1

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_0

    float-to-int v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;FI)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(FI)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;II)Lcom/google/googlenav/ui/view/android/DirectionTextView;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0401ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DirectionTextView;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2, p3}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->setPosition(II)V

    return-object v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v1, 0x0

    const-wide v7, 0x3ff0c152408e1c81L

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->f:I

    sub-int v4, v0, v2

    const/high16 v0, -0x3d900000

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->m:I

    int-to-float v2, v2

    int-to-float v3, v4

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/aP;->c()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->m:I

    int-to-float v3, v3

    int-to-double v9, v2

    mul-double/2addr v9, v5

    double-to-float v9, v9

    add-float/2addr v3, v9

    int-to-float v9, v4

    int-to-double v10, v2

    mul-double/2addr v10, v7

    double-to-float v2, v10

    add-float/2addr v9, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    iget v10, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->q:I

    int-to-float v10, v10

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/googlenav/ui/view/android/aT;->a:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    aget-object v2, v2, v0

    iget-object v10, v2, Lcom/google/googlenav/ui/view/android/aT;->b:[Lcom/google/googlenav/ui/view/android/aU;

    if-eqz v10, :cond_0

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->n:I

    int-to-float v2, v2

    add-float/2addr v2, v3

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->o:I

    int-to-float v3, v3

    add-float/2addr v9, v3

    move v3, v2

    move v2, v1

    :goto_1
    array-length v11, v10

    if-ge v2, v11, :cond_0

    iget-object v11, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    aget-object v12, v10, v2

    iget v12, v12, Lcom/google/googlenav/ui/view/android/aU;->a:I

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v11, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget v12, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->g:F

    add-float/2addr v12, v3

    iget v13, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->h:I

    int-to-float v13, v13

    add-float/2addr v13, v9

    invoke-virtual {v11, v3, v9, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v11, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget v11, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->g:F

    iget v12, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->i:F

    add-float/2addr v11, v12

    add-float/2addr v3, v11

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v0, p2

    int-to-float v1, p3

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->l:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v0, p2

    int-to-float v1, p3

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->l:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    return v0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->e:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0080

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->n:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->o:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->g:F

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->i:F

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0082

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->h:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->l:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->m:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->f:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->q:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    iget v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    iget v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->l:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->j:I

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/googlenav/ui/view/android/aQ;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/aQ;-><init>(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->v:Landroid/view/GestureDetector;

    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->c:Lcom/google/googlenav/ui/view/android/rideabout/r;

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;)Lcom/google/googlenav/ui/view/android/aS;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->x:Lcom/google/googlenav/ui/view/android/aS;

    return-object v0
.end method

.method private c()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/aP;-><init>(Landroid/content/Context;[Lcom/google/googlenav/ui/view/android/aT;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    return-void
.end method

.method private d()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    iget v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    mul-int/lit8 v1, v1, 0x3

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/view/android/aP;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->p:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v0

    iget-boolean v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->y:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    iget v5, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->e:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v0, v0

    iget v5, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    iget v6, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->e:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v1

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->k:I

    if-lt v0, v2, :cond_2

    iget v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    invoke-direct {p0, p1, v1, v2}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(Landroid/graphics/Canvas;II)V

    :goto_1
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aT;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->p:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->e:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    int-to-float v4, v4

    iget v5, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->e:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v1

    int-to-float v6, v6

    int-to-float v7, v2

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->b:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-direct {p0, p1, v1, v2}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(Landroid/graphics/Canvas;II)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->p:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v2, v1

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DirectionTextView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/googlenav/ui/view/android/DirectionTextView;->layout(IIII)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    const/high16 v2, 0x40000000

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aP;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->j:I

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->setMeasuredDimension(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aP;->d()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->e()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->measureChildren(II)V

    return-void
.end method

.method public setContentView(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->u:Landroid/widget/TextView;

    return-void
.end method

.method public setCurrentStationIndex(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->k:I

    return-void
.end method

.method public setDirectionSelectionListener(Lcom/google/googlenav/ui/view/android/aS;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->x:Lcom/google/googlenav/ui/view/android/aS;

    return-void
.end method

.method public setDrawLineAfterLastStation(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->y:Z

    return-void
.end method

.method public setLineColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->c:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->p:I

    return-void
.end method

.method public setSelectedStationIndex(I)V
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->t:I

    return-void
.end method

.method public setStations([Lcom/google/googlenav/ui/view/android/aT;)V
    .locals 9

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->s:[Lcom/google/googlenav/ui/view/android/aT;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->c()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->removeAllViews()V

    move v1, v2

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aT;->b()Ljava/util/List;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    sub-int v6, v0, v3

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->d()I

    move-result v0

    iget v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->r:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/android/aP;->e()I

    move-result v3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v3, v4

    sub-int/2addr v0, v3

    const/16 v3, 0x5cb

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v6, v0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(Ljava/lang/String;II)Lcom/google/googlenav/ui/view/android/DirectionTextView;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->addView(Landroid/view/View;)V

    move v3, v2

    move v4, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bQ;

    invoke-virtual {v0}, Lcom/google/googlenav/bQ;->b()I

    move-result v7

    iget-object v8, p0, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->w:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v8}, Lcom/google/googlenav/ui/view/android/aP;->e()I

    move-result v8

    add-int/2addr v4, v8

    invoke-virtual {v0}, Lcom/google/googlenav/bQ;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v6, v4}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->a(Ljava/lang/String;II)Lcom/google/googlenav/ui/view/android/DirectionTextView;

    move-result-object v0

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setClickable(Z)V

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    new-instance v8, Lcom/google/googlenav/ui/view/android/aR;

    invoke-direct {v8, p0, v7}, Lcom/google/googlenav/ui/view/android/aR;-><init>(Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;I)V

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/LineDialogSchematicView;->invalidate()V

    return-void
.end method
