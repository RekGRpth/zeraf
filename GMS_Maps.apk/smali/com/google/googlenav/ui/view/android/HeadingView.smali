.class public Lcom/google/googlenav/ui/view/android/HeadingView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaV/i;


# instance fields
.field private a:LaN/B;

.field private b:F

.field private c:LaN/B;

.field private d:J

.field private e:F

.field private f:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:LaH/m;

.field private i:LaV/h;

.field private j:Ljava/lang/Runnable;

.field private k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->c()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, -0x40800000

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->c()V

    return-void
.end method

.method private declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LaN/B;)V
    .locals 4

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->a:LaN/B;

    if-nez v0, :cond_1

    :cond_0
    const/high16 v0, -0x40800000

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(F)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->a:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->a(LaN/B;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->d:J

    iget-wide v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->d:J

    const-wide v2, 0x607bedba4L

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/high16 v0, -0x40800000

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(F)V

    :goto_1
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->c:LaN/B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->a:LaN/B;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/l;->b(LaN/B;LaN/B;)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(F)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized b(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method

.method private c()V
    .locals 1

    const v0, 0x7f0201fd

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->b(I)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/aJ;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/aJ;-><init>(Lcom/google/googlenav/ui/view/android/HeadingView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->j:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/googlenav/ui/view/android/aK;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/aK;-><init>(Lcom/google/googlenav/ui/view/android/HeadingView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->k:Ljava/lang/Runnable;

    return-void
.end method

.method private declared-synchronized d()V
    .locals 5

    const-wide/high16 v3, -0x4010000000000000L

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    float-to-double v0, v0

    cmpl-double v0, v0, v3

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    int-to-double v0, v0

    cmpl-double v0, v0, v3

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->j:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    int-to-double v0, v0

    cmpl-double v0, v0, v3

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->post(Ljava/lang/Runnable;)Z

    :cond_2
    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    iget v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->c(I)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    sub-int v1, v0, v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/j;->d(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_3

    iget v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    int-to-double v1, v1

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    :cond_3
    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->postInvalidate()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 2

    monitor-enter p0

    const/high16 v0, -0x40800000

    :try_start_0
    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->c:LaN/B;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->d:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-static {v0}, LaH/h;->b(Landroid/location/Location;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->h()LaN/B;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(LaN/B;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()LaN/B;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v1}, LaH/m;->s()LaH/h;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    invoke-virtual {v0, p0}, LaV/h;->a(LaV/i;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    :cond_1
    return-void
.end method

.method public declared-synchronized a(FF)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v0}, LaH/m;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/HeadingView;->b(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(ILaH/m;)V
    .locals 1

    monitor-enter p0

    const/high16 v0, -0x40800000

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaN/B;LaH/m;)V
    .locals 5

    const-wide/16 v3, 0x64

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(LaN/B;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p2}, LaH/m;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, LaH/h;->b(Landroid/location/Location;)I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/HeadingView;->b(F)V

    :cond_2
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->c:LaN/B;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->c:LaN/B;

    invoke-virtual {v0, v1}, LaN/B;->a(LaN/B;)J

    move-result-wide v1

    mul-long/2addr v1, v3

    mul-long/2addr v1, v3

    iget-wide v3, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->d:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_4

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a(LaN/B;)V

    :cond_4
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    invoke-virtual {v0, p0}, LaV/h;->b(LaV/i;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->b(LaH/A;)V

    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->a()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->b()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v5, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    float-to-double v0, v0

    const-wide/high16 v2, -0x4010000000000000L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->b:F

    iget v1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->e:F

    sub-float/2addr v0, v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->getHeight()I

    move-result v2

    add-int/lit8 v3, v1, 0x1

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-int/lit8 v4, v2, 0x1

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p1, v0, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized setDestination(LaN/B;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->a:LaN/B;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->e()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->f()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setInitialVisibility(LaN/B;)V
    .locals 5

    const/16 v0, 0x8

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/HeadingView;->h()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, LaN/B;->a(LaN/B;)J

    move-result-wide v1

    const-wide v3, 0x607bedba4L

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->setVisibility(I)V

    return-void
.end method

.method public setLocationProvider(LaH/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->h:LaH/m;

    return-void
.end method

.method public setOrientationProvider(LaV/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/HeadingView;->i:LaV/h;

    return-void
.end method
