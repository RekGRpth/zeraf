.class public Lcom/google/googlenav/ui/view/android/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Lcom/google/googlenav/ui/e;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bj;->f:Lcom/google/googlenav/ui/e;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/bj;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/bj;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/bj;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/googlenav/ui/view/android/bj;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/googlenav/ui/view/android/bj;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 3

    const/4 v2, 0x0

    new-instance v1, Lcom/google/googlenav/ui/view/android/bl;

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/view/android/bl;-><init>(Lcom/google/googlenav/ui/view/android/bk;)V

    iput-object p1, v1, Lcom/google/googlenav/ui/view/android/bl;->a:Landroid/view/View;

    const v0, 0x7f1003e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/bl;->b:Landroid/widget/TextView;

    const v0, 0x7f1003ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/bl;->c:Landroid/widget/TextView;

    const v0, 0x7f1003eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/bl;->d:Landroid/widget/TextView;

    const v0, 0x7f1003ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/bl;->e:Landroid/widget/TextView;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bm;

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/view/android/bm;-><init>(Lcom/google/googlenav/ui/view/android/bk;)V

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/bl;->f:Lcom/google/googlenav/ui/view/android/bm;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lcom/google/googlenav/ui/view/android/bl;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bj;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bj;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bj;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->f:Lcom/google/googlenav/ui/view/android/bm;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bj;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bm;->a:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->f:Lcom/google/googlenav/ui/view/android/bm;

    iput-object p1, v0, Lcom/google/googlenav/ui/view/android/bm;->b:Lcom/google/googlenav/ui/e;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->a:Landroid/view/View;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/bl;->f:Lcom/google/googlenav/ui/view/android/bm;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bl;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bj;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f04016e

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
