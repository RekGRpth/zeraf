.class public Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private b:[Lcom/google/googlenav/bO;

.field private c:Lcom/google/googlenav/ui/view/android/aP;

.field private d:F

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->d()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p2, p1, v0, v1, v2}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 11

    const/4 v4, 0x1

    const v10, -0x777778

    const/high16 v9, -0x1000000

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v0

    iget v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->f:I

    add-int v5, v0, v3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->e:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    aget-object v0, v0, v1

    instance-of v0, v0, Lcom/google/googlenav/bP;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/googlenav/bO;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/googlenav/bO;->a()Ljava/lang/String;

    move-result-object v3

    int-to-float v6, v5

    iget v7, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->d:F

    iget-object v8, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    aget-object v0, v0, v1

    check-cast v0, Lcom/google/googlenav/bP;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    add-int/lit8 v6, v1, 0x1

    aget-object v3, v3, v6

    if-nez v3, :cond_4

    move v3, v4

    :goto_2
    if-eqz v3, :cond_5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setColor(I)V

    :goto_3
    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/view/android/aP;->a()I

    move-result v6

    add-int/2addr v5, v6

    const/16 v6, 0x4ae

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/googlenav/bP;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v6, v7}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v6}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;

    move-result-object v0

    int-to-float v5, v5

    iget v6, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->d:F

    iget-object v7, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    if-nez v3, :cond_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3
.end method

.method private a(Lcom/google/googlenav/ui/view/android/aP;)V
    .locals 4

    const/high16 v3, 0x40000000

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    neg-int v1, v1

    iput v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->f:I

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->e:I

    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/aP;->e()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    iget v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->e:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->d:F

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aP;->a(I)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->f:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->c:Lcom/google/googlenav/ui/view/android/aP;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aP;->e()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setTimeTableRow([Lcom/google/googlenav/bO;Lcom/google/googlenav/ui/view/android/aP;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->b:[Lcom/google/googlenav/bO;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->a(Lcom/google/googlenav/ui/view/android/aP;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/TransitTimeTableRowView;->invalidate()V

    return-void
.end method
