.class public Lcom/google/googlenav/ui/view/android/aB;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:LaA/f;


# direct methods
.method public constructor <init>(ZILjava/lang/String;ILaA/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/aB;->a:Z

    iput p2, p0, Lcom/google/googlenav/ui/view/android/aB;->b:I

    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/aB;->c:Ljava/lang/String;

    iput p4, p0, Lcom/google/googlenav/ui/view/android/aB;->d:I

    iput-object p5, p0, Lcom/google/googlenav/ui/view/android/aB;->e:LaA/f;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aB;->a:Z

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/aB;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aB;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/aB;->d:I

    return v0
.end method

.method public e()LaA/f;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aB;->e:LaA/f;

    return-object v0
.end method
