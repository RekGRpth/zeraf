.class public abstract Lcom/google/googlenav/ui/view/android/CheckableContainer;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private final a:Landroid/widget/CompoundButton;

.field private b:Lcom/google/googlenav/ui/view/android/X;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f100165

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f100165

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f100165

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/CheckableContainer;)Lcom/google/googlenav/ui/view/android/X;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->b:Lcom/google/googlenav/ui/view/android/X;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    new-instance v1, Lcom/google/googlenav/ui/view/android/V;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/V;-><init>(Lcom/google/googlenav/ui/view/android/CheckableContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x1080062

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->setBackgroundResource(I)V

    new-instance v0, Lcom/google/googlenav/ui/view/android/W;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/W;-><init>(Lcom/google/googlenav/ui/view/android/CheckableContainer;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/CheckableContainer;)Landroid/widget/CompoundButton;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method public setCheckedWithoutListener(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->b:Lcom/google/googlenav/ui/view/android/X;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->b:Lcom/google/googlenav/ui/view/android/X;

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/CheckableContainer;->setChecked(Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->b:Lcom/google/googlenav/ui/view/android/X;

    return-void
.end method

.method public setOnCheckedChangeListener(Lcom/google/googlenav/ui/view/android/X;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->b:Lcom/google/googlenav/ui/view/android/X;

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/CheckableContainer;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->toggle()V

    return-void
.end method
