.class public Lcom/google/googlenav/ui/view/android/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field final A:Z

.field final B:Lcom/google/googlenav/ai;

.field C:Z

.field final D:Z

.field final E:Lcom/google/googlenav/ui/e;

.field final F:I

.field final G:Z

.field final H:I

.field final I:Z

.field J:Z

.field final K:Ljava/lang/CharSequence;

.field final L:Ljava/lang/CharSequence;

.field final M:I

.field final N:Z

.field final O:Ljava/lang/CharSequence;

.field final P:Ljava/lang/String;

.field final Q:I

.field public final R:I

.field private final S:Lcom/google/googlenav/ui/br;

.field final b:Ljava/lang/CharSequence;

.field final c:[Ljava/lang/CharSequence;

.field final d:[Ljava/lang/CharSequence;

.field final e:[Ljava/lang/CharSequence;

.field final f:Ljava/lang/CharSequence;

.field final g:Z

.field final h:Ljava/lang/CharSequence;

.field final i:Ljava/lang/String;

.field final j:Ljava/lang/CharSequence;

.field final k:Ljava/lang/CharSequence;

.field final l:Ljava/lang/CharSequence;

.field final m:Ljava/lang/CharSequence;

.field final n:Ljava/lang/CharSequence;

.field final o:Ljava/lang/CharSequence;

.field final p:Ljava/lang/CharSequence;

.field final q:Ljava/lang/String;

.field final r:Ljava/lang/CharSequence;

.field final s:Ljava/lang/CharSequence;

.field final t:Ljava/lang/CharSequence;

.field final u:Lcom/google/googlenav/ui/bs;

.field final v:Lcom/google/googlenav/ui/bs;

.field final w:Lam/f;

.field final x:Lcom/google/googlenav/ui/view/a;

.field final y:Lan/f;

.field final z:LaN/B;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/android/bs;->a:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/view/android/bt;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->E:Lcom/google/googlenav/ui/e;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->b(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->d(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->e(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->d:[Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->f(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->e:[Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->g(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->h(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->g:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->i(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->j(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->k(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->j:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->l(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->k:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->m(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->b(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->n(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->o(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->p(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ac;

    move-result-object v0

    invoke-static {v0}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->aW:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lbf/aS;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->d(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->r(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ap;

    move-result-object v0

    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->s(Lcom/google/googlenav/ui/view/android/bt;)Lam/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->t(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->u(Lcom/google/googlenav/ui/view/android/bt;)Lan/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->y:Lan/f;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->v(Lcom/google/googlenav/ui/view/android/bt;)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->z:LaN/B;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->w(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->A:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->x(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->B:Lcom/google/googlenav/ai;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->y(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->D:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->A(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->B(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->R:I

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->C(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->F:I

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->D(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->G:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->E(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->H:I

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->F(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->I:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->G(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bt;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->K:Ljava/lang/CharSequence;

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    if-eqz v0, :cond_1

    move-object v0, v2

    :goto_1
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->L:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->M:I

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->I(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->N:Z

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    :goto_2
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_2
    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    goto :goto_2
.end method

.method private a(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 2

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3c1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->K(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/bt;Z)Ljava/lang/CharSequence;
    .locals 3

    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->L(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bC;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->F(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v1

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->L(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/bC;->a(ZILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V
    .locals 6

    const/4 v2, 0x0

    if-eqz p1, :cond_5

    array-length v4, p1

    move v3, v2

    move v0, v2

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v5, p1, v3

    if-nez v5, :cond_0

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v0, 0x1

    aget-object v0, p2, v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    :goto_2
    array-length v1, p2

    if-ge v0, v1, :cond_4

    add-int/lit8 v1, v0, 0x1

    aget-object v0, p2, v0

    if-eqz v0, :cond_3

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method private b(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 3

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_3

    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    const/16 v0, 0x3c1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    move-object v1, v0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .locals 7

    const/4 v2, 0x0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bw:Lcom/google/googlenav/ui/aV;

    sget-object v3, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->a()[Lcom/google/googlenav/an;

    move-result-object v4

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v6, v4

    if-ge v0, v6, :cond_1

    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->b:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->a:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->c:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_0

    const-string v6, " "

    invoke-static {v6, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    move-object v0, v1

    :goto_2
    move-object v2, v0

    :cond_3
    :goto_3
    return-object v2

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/al;

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    invoke-static {v0, v4, v3, v3}, Lbf/aS;->a(Lcom/google/googlenav/al;Ljava/util/Vector;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    move-object v2, v1

    goto :goto_3
.end method

.method private d(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/bs;
    .locals 3

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bs;->H:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/android/bv;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/bv;-><init>()V

    invoke-virtual {p0, v0, p1}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bv;Landroid/view/View;)V

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 5

    const/4 v2, 0x0

    const/16 v0, 0x8

    const/4 v1, 0x0

    check-cast p2, Lcom/google/googlenav/ui/view/android/bv;

    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/android/bv;->a()V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iget v3, v3, Lcom/google/googlenav/ui/view/android/bw;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iget v4, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v4, v3, Lcom/google/googlenav/ui/view/android/bw;->b:I

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->a:Landroid/view/View;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->b:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/googlenav/ui/view/android/bs;->A:Z

    if-eqz v3, :cond_b

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iget v4, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v4, v3, Lcom/google/googlenav/ui/view/android/bw;->b:I

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput-object p0, v3, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->B:Lcom/google/googlenav/ai;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bw;->c:Lcom/google/googlenav/ai;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->d:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->e:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bu;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    if-eqz v3, :cond_3

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bx;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v3, :cond_4

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_4
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->l:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->j:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->m:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->k:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->n:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->o:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    if-eqz v3, :cond_5

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    if-nez v4, :cond_c

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->q:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_6
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->u:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    check-cast v0, Lan/f;

    :cond_7
    :goto_2
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    invoke-static {v3, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    invoke-static {v3, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    if-eqz v0, :cond_8

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    iput-object v3, v0, Lcom/google/googlenav/ui/view/android/by;->a:Lcom/google/googlenav/ui/view/a;

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_8
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->h:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->i:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->z:LaN/B;

    invoke-static {v0, v3, v4}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    :goto_3
    iget-object v2, p2, Lcom/google/googlenav/ui/view/android/bv;->w:Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->v:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->y:Lan/f;

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    :cond_9
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_a

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_a

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    iget v2, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    iget-object v2, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    :cond_a
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->B:Lcom/google/googlenav/ui/bD;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->L:Ljava/lang/CharSequence;

    iget v3, p0, Lcom/google/googlenav/ui/view/android/bs;->M:I

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->K:Ljava/lang/CharSequence;

    invoke-static {v0, v2, v3, v4, v1}, Lcom/google/googlenav/ui/bC;->a(Lcom/google/googlenav/ui/bD;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Z)V

    return-void

    :cond_b
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_1

    :cond_d
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    :goto_4
    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->r()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    goto/16 :goto_2

    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    goto/16 :goto_2

    :cond_f
    move-object v0, v2

    goto/16 :goto_3

    :cond_10
    move-object v0, v2

    goto/16 :goto_2

    :cond_11
    move-object v0, v2

    goto :goto_4
.end method

.method protected a(Lcom/google/googlenav/ui/view/android/bv;Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->N:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lbf/aS;->a(Landroid/view/View;)V

    :cond_0
    iput-object p2, p1, Lcom/google/googlenav/ui/view/android/bv;->a:Landroid/view/View;

    const v0, 0x7f10001e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->b:Landroid/widget/TextView;

    const v0, 0x7f100306

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f1001a0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f1001a1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f100309

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v4

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    const v0, 0x7f100307

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    const v0, 0x7f100308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    const v0, 0x7f10030b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    const v0, 0x7f10030c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    const v0, 0x7f1002b4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bu;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bu;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    const v0, 0x7f10019c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->h:Lcom/google/googlenav/ui/view/android/DistanceView;

    const v0, 0x7f10019d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->i:Lcom/google/googlenav/ui/view/android/HeadingView;

    const v0, 0x7f10030d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    const v0, 0x7f10030e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    const v0, 0x7f10030f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->l:Landroid/widget/TextView;

    const v0, 0x7f100310

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->m:Landroid/widget/TextView;

    const v0, 0x7f100263

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->n:Landroid/widget/TextView;

    const v0, 0x7f100311

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->o:Landroid/widget/TextView;

    const v0, 0x7f10007a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    const v0, 0x7f100314

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->q:Landroid/widget/TextView;

    const v0, 0x7f100315

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->r:Landroid/widget/TextView;

    const v0, 0x7f10007d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->s:Landroid/widget/TextView;

    const v0, 0x7f100312

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    const v0, 0x7f10007e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->u:Landroid/widget/TextView;

    const v0, 0x7f100317

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->v:Landroid/widget/TextView;

    const v0, 0x7f100316

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->w:Landroid/widget/ImageView;

    const v0, 0x7f10001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    const v0, 0x7f100271

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    const v0, 0x7f1002a9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    const v0, 0x7f10004e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/bs;->R:I

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    const/16 v1, 0x578

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    :goto_0
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->g:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/googlenav/ui/view/android/bx;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bx;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    :goto_1
    new-instance v0, Lcom/google/googlenav/ui/view/android/by;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/by;-><init>(Lcom/google/googlenav/ui/view/android/bs;Lcom/google/googlenav/ui/view/a;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bz;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bz;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    new-instance v0, Lcom/google/googlenav/ui/view/android/bz;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bz;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    invoke-static {p2}, Lcom/google/googlenav/ui/bC;->a(Landroid/view/View;)Lcom/google/googlenav/ui/bD;

    move-result-object v0

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->B:Lcom/google/googlenav/ui/bD;

    return-void

    :cond_1
    iput-object v5, p1, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    goto :goto_0

    :cond_2
    iput-object v5, p1, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    goto :goto_1
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bs;->F:I

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->G:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    const/16 v7, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    if-eqz v5, :cond_2

    if-lez v0, :cond_1

    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    if-eqz v0, :cond_a

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
