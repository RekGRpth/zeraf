.class public Lcom/google/googlenav/ui/view/dialog/k;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/dialog/o;

.field private b:Lcom/google/googlenav/ui/view/android/J;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/o;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/k;->a:Lcom/google/googlenav/ui/view/dialog/o;

    const/4 v0, 0x1

    const v1, 0x7f020219

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/l;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/l;-><init>(Lcom/google/googlenav/ui/view/dialog/k;)V

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/k;->a(ZILaA/f;[I)V

    const/16 v0, 0x4d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020288

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/k;->a(Ljava/lang/CharSequence;II)V

    return-void

    nop

    :array_0
    .array-data 4
        0xbbe
        0xbbd
    .end array-data
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/k;)Lcom/google/googlenav/ui/view/dialog/o;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/k;->a:Lcom/google/googlenav/ui/view/dialog/o;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lbd/a;

    const/16 v1, 0x4a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/m;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/m;-><init>(Lcom/google/googlenav/ui/view/dialog/k;)V

    invoke-direct {v0, v1, v2, v3}, Lbd/a;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbd/a;

    const/16 v1, 0x4b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/n;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/n;-><init>(Lcom/google/googlenav/ui/view/dialog/k;)V

    invoke-direct {v0, v1, v2, v3}, Lbd/a;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private h()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/dialog/k;->a(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .locals 5

    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/k;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/k;->h()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/k;->b:Lcom/google/googlenav/ui/view/android/J;

    const v0, 0x7f100026

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/k;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/k;->b:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/k;->a:Lcom/google/googlenav/ui/view/dialog/o;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/o;->c()V

    const/4 v0, 0x1

    return v0
.end method

.method protected c()Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/k;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040008

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/k;->a:Lcom/google/googlenav/ui/view/dialog/o;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/o;->c()V

    return-void
.end method
