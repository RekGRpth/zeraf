.class public Lcom/google/googlenav/ui/view/dialog/R;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:LaG/n;

.field private final b:Lcom/google/googlenav/ui/view/dialog/W;

.field private final c:Lcom/google/googlenav/ui/view/dialog/V;

.field private d:Lcom/google/googlenav/ui/view/dialog/F;


# direct methods
.method public constructor <init>(LaG/n;Lcom/google/googlenav/ui/view/dialog/W;)V
    .locals 1

    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/R;->requestWindowFeature(I)Z

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    iput-object p2, p0, Lcom/google/googlenav/ui/view/dialog/R;->b:Lcom/google/googlenav/ui/view/dialog/W;

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/V;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/dialog/V;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/R;)Lcom/google/googlenav/ui/view/dialog/W;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->b:Lcom/google/googlenav/ui/view/dialog/W;

    return-object v0
.end method

.method static a(Landroid/view/View;LaG/f;)V
    .locals 3

    const v0, 0x7f100090

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LaG/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100094

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LaG/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100092

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LaG/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100091

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, LaG/f;->g()Lam/f;

    move-result-object v1

    check-cast v1, Lan/f;

    invoke-virtual {v1}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f10008f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, LaG/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f100093

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LaG/f;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, LaG/f;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f010f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0110

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private h()Ljava/lang/CharSequence;
    .locals 2

    const/16 v1, 0x110

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v0}, LaG/n;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x111

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x112

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private l()I
    .locals 2

    const v0, 0x7f0200b5

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v1}, LaG/n;->f()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x7f0200b6

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200b7

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private m()Ljava/lang/String;
    .locals 4

    const/16 v0, 0x92

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v3}, LaG/n;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 4

    const-string v0, "{0} - {1}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xaf

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v3}, LaG/n;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Ljava/util/List;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->e:[Landroid/view/View;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->e:[Landroid/view/View;

    aget-object v2, v0, v1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/view/dialog/R;->a(Landroid/view/View;LaG/f;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/R;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v0, 0x7f04001d

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iput-object v3, v0, Lcom/google/googlenav/ui/view/dialog/V;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v4, 0x7f10009a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/google/googlenav/ui/view/dialog/V;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v4, 0x7f1000a0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/google/googlenav/ui/view/dialog/V;->c:Landroid/view/View;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f10009e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->d:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f100097

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->l:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f10009b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f10009c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f1000a1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->i:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f10009f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->j:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f1000a2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->k:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f10009d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f100099

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->m:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    const v0, 0x7f100098

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v4, Lcom/google/googlenav/ui/view/dialog/V;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v0}, LaG/n;->e()I

    move-result v0

    const/4 v4, 0x4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v0, v4}, LaG/n;->a(I)Ljava/util/List;

    move-result-object v5

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    new-array v6, v4, [Landroid/view/View;

    iput-object v6, v0, Lcom/google/googlenav/ui/view/dialog/V;->e:[Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/R;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0b00c8

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    new-instance v7, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/R;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v8, 0x7f09000a

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v8, v8, Lcom/google/googlenav/ui/view/dialog/V;->d:Landroid/view/ViewGroup;

    const/4 v9, -0x1

    invoke-virtual {v8, v7, v9, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    const v7, 0x7f04001c

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v8, v8, Lcom/google/googlenav/ui/view/dialog/V;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7, v8, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v8, v8, Lcom/google/googlenav/ui/view/dialog/V;->e:[Landroid/view/View;

    aput-object v7, v8, v0

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v8, v8, Lcom/google/googlenav/ui/view/dialog/V;->d:Landroid/view/ViewGroup;

    invoke-virtual {v8, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/googlenav/ui/view/dialog/R;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    invoke-virtual {v1}, LaG/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->g:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/R;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->i:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/R;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->l:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/R;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->h:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/dialog/R;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->j:Landroid/widget/TextView;

    const/16 v1, 0x535

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->k:Landroid/widget/TextView;

    const/16 v1, 0x24e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->i:Landroid/widget/TextView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/S;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/S;-><init>(Lcom/google/googlenav/ui/view/dialog/R;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->j:Landroid/widget/TextView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/T;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/T;-><init>(Lcom/google/googlenav/ui/view/dialog/R;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->k:Landroid/widget/TextView;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/U;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/U;-><init>(Lcom/google/googlenav/ui/view/dialog/R;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/F;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/R;->a:LaG/n;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/F;-><init>(LaG/n;Lcom/google/googlenav/ui/view/dialog/V;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->d:Lcom/google/googlenav/ui/view/dialog/F;

    return-object v3
.end method

.method protected d()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->lockScreenOrientation()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->c:Lcom/google/googlenav/ui/view/dialog/V;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/V;->a:Landroid/view/View;

    const v1, 0x7f100096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/view/View;)V

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    return-void
.end method

.method protected f()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->f()V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->unlockScreenOrientation()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->b:Lcom/google/googlenav/ui/view/dialog/W;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->b:Lcom/google/googlenav/ui/view/dialog/W;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/W;->c()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/S;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->onStart()V

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/R;->d:Lcom/google/googlenav/ui/view/dialog/F;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/F;->run()V

    return-void
.end method
