.class Lcom/google/googlenav/ui/view/dialog/cp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/co;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/view/dialog/co;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/co;->a(Lcom/google/googlenav/ui/view/dialog/co;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    invoke-static {v1}, Lcom/google/googlenav/ui/view/dialog/co;->b(Lcom/google/googlenav/ui/view/dialog/co;)Lcom/google/googlenav/ui/view/dialog/cs;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/cs;->getCount()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    invoke-static {v0}, Lcom/google/googlenav/ui/view/dialog/co;->b(Lcom/google/googlenav/ui/view/dialog/co;)Lcom/google/googlenav/ui/view/dialog/cs;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/dialog/cs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    move v1, v2

    :goto_2
    if-ge v1, v8, :cond_0

    invoke-virtual {v0, v12, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    invoke-virtual {v9, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/co;->a:Lcom/google/googlenav/ui/wizard/jb;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    iget v1, v1, Lcom/google/googlenav/ui/view/dialog/co;->c:I

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/cp;->a:Lcom/google/googlenav/ui/view/dialog/co;

    iget v2, v2, Lcom/google/googlenav/ui/view/dialog/co;->l:I

    invoke-virtual {v0, v6, v7, v1, v2}, Lcom/google/googlenav/ui/wizard/jb;->a(Ljava/util/List;Ljava/util/List;II)V

    return-void
.end method
