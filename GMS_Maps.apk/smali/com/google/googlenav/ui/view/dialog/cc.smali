.class public abstract Lcom/google/googlenav/ui/view/dialog/cc;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/googlenav/ui/wizard/jb;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jb;)V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/cc;->a:Lcom/google/googlenav/ui/wizard/jb;

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->a(Landroid/app/ActionBar;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    return-void
.end method

.method protected a(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    const v2, 0x7f10004a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0201a7

    invoke-virtual {p0, p2, v2, v1}, Lcom/google/googlenav/ui/view/dialog/cc;->a(Ljava/lang/CharSequence;II)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, p2, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    goto :goto_0
.end method
