.class public Lcom/google/googlenav/ui/view/dialog/ak;
.super Lcom/google/googlenav/ui/view/dialog/cy;
.source "SourceFile"


# instance fields
.field protected a:Landroid/widget/ViewSwitcher;

.field private b:Lcom/google/googlenav/ui/view/dialog/ao;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/e;)V
    .locals 5

    const v0, 0x7f0f0018

    const/16 v1, 0x3dd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x3cb

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020216

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/cy;-><init>(Lcom/google/googlenav/ui/e;ILjava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ak;)Lcom/google/googlenav/ui/view/dialog/ao;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ak;->b:Lcom/google/googlenav/ui/view/dialog/ao;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .locals 0

    invoke-virtual {p1}, Landroid/app/ActionBar;->hide()V

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ak;->v()V

    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/dialog/cy;->a(Landroid/view/View;)V

    const v0, 0x7f1001fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ak;->a:Landroid/widget/ViewSwitcher;

    const v0, 0x7f1001fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x3dc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/ao;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/ak;->b:Lcom/google/googlenav/ui/view/dialog/ao;

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/ap;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/view/dialog/ap;-><init>(Lcom/google/googlenav/ui/view/dialog/ao;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ak;->a(Landroid/webkit/WebViewClient;)V

    return-void
.end method

.method public h()Landroid/widget/ViewSwitcher;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ak;->a:Landroid/widget/ViewSwitcher;

    return-object v0
.end method

.method public l()I
    .locals 1

    const v0, 0x7f04008a

    return v0
.end method

.method public m()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ak;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/al;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/view/dialog/al;-><init>(Lcom/google/googlenav/ui/view/dialog/ak;Landroid/os/Handler;)V

    invoke-direct {v1, v2, v3}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Las/b;->g()V

    return-void
.end method
