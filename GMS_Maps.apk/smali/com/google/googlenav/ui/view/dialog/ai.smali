.class public Lcom/google/googlenav/ui/view/dialog/ai;
.super Lcom/google/googlenav/ui/view/android/aL;
.source "SourceFile"


# static fields
.field private static final g:Ljava/util/List;


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:[Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v0, 0x7f1001f3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f1001f4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1001f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f1001f6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f1001f7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f0f001d

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/aL;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->setCancelable(Z)V

    return-void
.end method

.method public static a()Lcom/google/googlenav/ui/view/dialog/ai;
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ai;

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/ai;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x42c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->a:Ljava/lang/CharSequence;

    const/16 v0, 0x42b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->b:Ljava/lang/CharSequence;

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "26-30"

    aput-object v2, v0, v3

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x42a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "21-25"

    aput-object v2, v0, v4

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x429

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "16-20"

    aput-object v2, v0, v5

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x428

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "11-15"

    aput-object v2, v0, v6

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x427

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "0-10"

    aput-object v2, v0, v7

    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x426

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    const/16 v0, 0x425

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->f:Ljava/lang/CharSequence;

    return-object v1
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v5}, Lcom/google/googlenav/ui/view/dialog/ai;->requestWindowFeature(I)Z

    const v0, 0x7f040089

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->setContentView(I)V

    const v0, 0x7f1001f1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->a:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v0, 0x7f1001f2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->b:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    move v2, v3

    :goto_0
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    if-nez v1, :cond_2

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const v0, 0x7f1001f8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->e:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v0, 0x7f1001f9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aj;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/aj;-><init>(Lcom/google/googlenav/ui/view/dialog/ai;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
