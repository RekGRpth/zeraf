.class public abstract Lcom/google/googlenav/ui/Z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/X;


# instance fields
.field protected a:LaN/V;

.field protected b:Lcom/google/googlenav/ui/aR;

.field protected final c:LaN/R;

.field private d:Z

.field private e:I

.field private final f:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/Z;->d:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    new-instance v0, LaN/R;

    invoke-direct {v0}, LaN/R;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/Z;->c:LaN/R;

    return-void
.end method

.method private a(LaN/I;)I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/Z;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v2

    invoke-virtual {v2}, LaN/P;->i()LaN/P;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LaN/V;->a(LaN/P;Z)LaN/X;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, LaN/X;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, LaN/X;->f()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, LaN/X;->d()J

    move-result-wide v2

    long-to-int v0, v2

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LaN/X;->a(J)V

    :cond_1
    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, Lcom/google/googlenav/ui/Z;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method private a(LaN/I;LaN/X;I)V
    .locals 2

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p1}, LaN/I;->l()I

    move-result v0

    if-nez v0, :cond_4

    if-eqz p2, :cond_1

    invoke-virtual {p2}, LaN/X;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/Z;->a(LaN/I;LaN/X;)Lam/f;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p1, p3}, LaN/I;->a(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p1, v0, p3, v1}, LaN/I;->a(Lam/f;IZ)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, LaN/I;->l()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaN/I;->l()I

    move-result v0

    if-eq v0, p3, :cond_2

    invoke-virtual {p1}, LaN/I;->k()V

    invoke-virtual {p1}, LaN/I;->o()V

    goto :goto_0
.end method

.method private b(LaN/I;Z)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/googlenav/ui/Z;->d:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LaN/P;->e()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    const/16 v3, 0x9

    if-lt v2, v3, :cond_0

    invoke-virtual {v1}, LaN/P;->e()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    const/16 v2, 0x16

    if-gt v1, v2, :cond_0

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v0

    invoke-virtual {v0}, LaN/P;->i()LaN/P;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {v1, v0, p2}, LaN/V;->a(LaN/P;Z)LaN/X;

    move-result-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/Z;->a(LaN/I;)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/Z;->f()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, LaN/I;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {p1}, LaN/I;->m()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, LaN/I;->l()I

    move-result v2

    if-eq v2, v1, :cond_2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/Z;->a(LaN/I;LaN/X;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(LaN/I;LaN/X;)Lam/f;
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aR;->a()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/googlenav/ui/Z;->e:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaN/V;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/Y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/Z;->d:Z

    return-void
.end method

.method public a(LaN/I;Z)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/Z;->b(LaN/I;Z)Z

    move-result v0

    return v0
.end method

.method public a(LaN/p;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, LaN/p;->g()[LaN/P;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    invoke-virtual {p1, v4}, LaN/p;->a(LaN/P;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {v4}, LaN/P;->i()LaN/P;

    move-result-object v4

    invoke-virtual {v5, v4, v0}, LaN/V;->a(LaN/P;Z)LaN/X;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, LaN/X;->b()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {v0}, LaN/V;->b()V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/Y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(LaN/p;)Z
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, LaN/p;->g()[LaN/P;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    invoke-virtual {p1, v5}, LaN/p;->a(LaN/P;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {v5}, LaN/P;->i()LaN/P;

    move-result-object v5

    invoke-virtual {v6, v5, v1}, LaN/V;->a(LaN/P;Z)LaN/X;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, LaN/X;->b()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, LaN/X;->f()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, LaN/X;->d()J

    move-result-wide v5

    const-wide/high16 v7, -0x8000000000000000L

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->a:LaN/V;

    invoke-virtual {v0}, LaN/V;->a()V

    return-void
.end method

.method public d()Lcom/google/googlenav/ui/aR;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    return-object v0
.end method

.method public e()V
    .locals 12

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v8, v0, [[Lcom/google/googlenav/ui/aI;

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v9

    move v7, v5

    move v4, v5

    :goto_0
    if-ge v7, v9, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/Z;->f:Ljava/util/Vector;

    invoke-virtual {v0, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/Y;

    invoke-interface {v0}, Lcom/google/googlenav/ui/Y;->e()[Lcom/google/googlenav/ui/aI;

    move-result-object v3

    if-eqz v3, :cond_9

    array-length v2, v3

    move v1, v5

    move v0, v5

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v6, v3, v1

    if-eqz v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    array-length v1, v3

    if-eq v1, v0, :cond_8

    new-array v2, v0, [Lcom/google/googlenav/ui/aI;

    array-length v10, v3

    move v6, v5

    move v1, v5

    :goto_2
    if-ge v6, v10, :cond_2

    aget-object v0, v3, v6

    if-eqz v0, :cond_7

    add-int/lit8 v0, v1, 0x1

    aget-object v11, v3, v6

    aput-object v11, v2, v1

    :goto_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_2

    :cond_2
    move-object v0, v2

    :goto_4
    aput-object v0, v8, v7

    array-length v0, v0

    add-int/2addr v0, v4

    :goto_5
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v4, v0

    goto :goto_0

    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    :goto_6
    return-void

    :cond_4
    new-array v2, v4, [Lcom/google/googlenav/ui/aI;

    move v0, v5

    move v1, v5

    :goto_7
    array-length v3, v8

    if-ge v0, v3, :cond_6

    aget-object v3, v8, v0

    if-eqz v3, :cond_5

    aget-object v3, v8, v0

    aget-object v4, v8, v0

    array-length v4, v4

    invoke-static {v3, v5, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aget-object v3, v8, v0

    array-length v3, v3

    add-int/2addr v1, v3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_6
    new-instance v0, Lcom/google/googlenav/ui/aR;

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/aR;-><init>([Lcom/google/googlenav/ui/aI;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/Z;->b:Lcom/google/googlenav/ui/aR;

    goto :goto_6

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move-object v0, v3

    goto :goto_4

    :cond_9
    move v0, v4

    goto :goto_5
.end method

.method protected abstract f()Z
.end method
