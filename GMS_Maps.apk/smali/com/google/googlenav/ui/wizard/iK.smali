.class public Lcom/google/googlenav/ui/wizard/iK;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/iH;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/iH;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/wizard/iK;->p()I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 2

    const/16 v1, 0x522

    const-string v0, ""

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iH;->c(Lcom/google/googlenav/ui/wizard/iH;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :pswitch_0
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x517

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x524

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x514

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public P_()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iK;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401a8

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/iK;->a(Landroid/widget/TextView;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/iK;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/iH;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/iJ;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-direct {v2, v3, v5}, Lcom/google/googlenav/ui/wizard/iJ;-><init>(Lcom/google/googlenav/ui/wizard/iH;Lcom/google/googlenav/ui/wizard/iI;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setEnabled(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-object v0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iH;->b(Lcom/google/googlenav/ui/wizard/iH;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iK;->a:Lcom/google/googlenav/ui/wizard/iH;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iH;->b(Lcom/google/googlenav/ui/wizard/iH;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
