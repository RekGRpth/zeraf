.class public Lcom/google/googlenav/ui/wizard/gc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/gh;

.field private final b:Lcom/google/googlenav/ui/wizard/fV;

.field private final c:Lcom/google/googlenav/ui/wizard/gg;

.field private d:Lcom/google/googlenav/ui/wizard/fI;

.field private e:Lcom/google/googlenav/ui/wizard/fM;

.field private f:Z

.field private final g:Lcom/google/googlenav/ui/wizard/jv;

.field private h:Lcom/google/googlenav/aU;

.field private final i:Lcom/google/googlenav/ui/ak;

.field private j:Lcom/google/googlenav/ui/wizard/aL;

.field private final k:LaH/h;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;LaH/h;Lcom/google/googlenav/ui/wizard/gh;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->g:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/gc;->a:Lcom/google/googlenav/ui/wizard/gh;

    new-instance v0, Lcom/google/googlenav/ui/wizard/ge;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ge;-><init>(Lcom/google/googlenav/ui/wizard/gc;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->b:Lcom/google/googlenav/ui/wizard/fV;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    new-instance v0, Lcom/google/googlenav/ui/wizard/gg;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/gg;-><init>(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/ui/wizard/gd;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->c:Lcom/google/googlenav/ui/wizard/gg;

    new-instance v0, Lcom/google/googlenav/ui/wizard/fI;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/fI;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->c:Lcom/google/googlenav/ui/wizard/gg;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fI;->a(Lcom/google/googlenav/ui/wizard/fK;)V

    invoke-virtual {p2}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/aL;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/aL;-><init>(LaH/m;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gc;->j:Lcom/google/googlenav/ui/wizard/aL;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/aU;)Lcom/google/googlenav/aU;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->h:Lcom/google/googlenav/aU;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/gh;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->a:Lcom/google/googlenav/ui/wizard/gh;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/aU;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->h:Lcom/google/googlenav/aU;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method private c()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->j:Lcom/google/googlenav/ui/wizard/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/gi;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/gi;-><init>(Lcom/google/googlenav/ui/wizard/gc;Lcom/google/googlenav/ui/wizard/gd;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/aL;->a(LaH/A;)V

    return-void
.end method

.method private d()LaH/h;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->i:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/fM;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->g:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/gc;)LaH/h;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->d()LaH/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/gc;)Lcom/google/googlenav/ui/wizard/fI;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/fV;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->b:Lcom/google/googlenav/ui/wizard/fV;

    return-object v0
.end method

.method public a(LaH/h;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->c()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/fI;->a(II)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v0, p1, v3}, Lcom/google/googlenav/ui/wizard/fI;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/fM;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->k:LaH/h;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/gc;->d()LaH/h;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    const/16 v1, 0xbd

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/fM;->b(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/fM;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/fI;->a(II)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->d:Lcom/google/googlenav/ui/wizard/fI;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/wizard/fI;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->n()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/gc;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fM;->n()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fM;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gc;->e:Lcom/google/googlenav/ui/wizard/fM;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fM;->m()V

    const/4 v0, 0x1

    goto :goto_0
.end method
