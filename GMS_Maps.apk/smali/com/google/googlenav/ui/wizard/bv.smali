.class public Lcom/google/googlenav/ui/wizard/bv;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static j:Lcom/google/googlenav/common/Config;

.field private static k:Ljava/lang/String;


# instance fields
.field private a:Lax/b;

.field private b:Lax/b;

.field private final c:Lcom/google/googlenav/J;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    const-string v0, ""

    sput-object v0, Lcom/google/googlenav/ui/wizard/bv;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    return-void
.end method

.method private A()I
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(I)I
    .locals 0

    iput p1, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    return p1
.end method

.method public static a(Lcom/google/googlenav/ui/m;)Lax/b;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->L()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "SAVED_DIRECTIONS"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    const-string v2, "SAVED_DIRECTIONS_DB"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    :cond_0
    const-string v2, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-static {v1, v2}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-static {v1, p0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v1

    instance-of v2, v1, Lax/s;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lax/b;->G()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "UI-TDW"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Lax/l;Z)Ljava/lang/String;
    .locals 10

    const/4 v8, 0x5

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {p0}, Lax/l;->c()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    if-ne v5, v8, :cond_0

    if-ne v4, v7, :cond_0

    if-eq v0, v6, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz p1, :cond_2

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    const/16 v0, 0x5bb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v9, [Ljava/lang/String;

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->o()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->s(I)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->e(Lax/b;)Lbf/O;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bv;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->y()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    const-string v1, ""

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v2}, Lax/b;->k()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static b(Lax/l;)Ljava/lang/String;
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lax/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    invoke-virtual {p0}, Lax/l;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lax/l;->d()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_2
    invoke-static {p0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/l;Z)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0xe7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x210

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v1}, Lax/b;->q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v1}, Lax/b;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->e(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->o(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x4

    const-string v1, "sa"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x38

    const-string v1, "ir"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private y()V
    .locals 4

    sget-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v3, v2}, Lax/b;->b(Ljava/io/DataOutput;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const-string v2, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "UI-DL save"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private z()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->h()Lcom/google/googlenav/ui/view/q;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 4

    const/4 v3, 0x3

    const/16 v2, 0x36

    invoke-virtual {p1}, Lat/a;->b()I

    move-result v1

    if-eq v1, v2, :cond_0

    const/16 v0, 0x34

    if-ne v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->ax()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_2

    const-string v0, "l"

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    :cond_1
    return v3

    :cond_2
    const-string v0, "e"

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public a(Lax/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    :cond_1
    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    :cond_2
    invoke-virtual {p1}, Lax/b;->aj()I

    move-result v0

    if-gez v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x30f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {p1}, Lax/b;->aa()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ha"

    invoke-static {v0}, Lbf/O;->c(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lax/b;->C()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "sa"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lax/b;->D()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v0, "ea"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lax/b;->aF()[I

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_7

    const-string v0, "a"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    :cond_7
    new-instance v0, Lcom/google/googlenav/ui/wizard/bw;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/bw;-><init>(Lcom/google/googlenav/ui/wizard/bv;Las/c;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bw;->g()V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x310

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lax/l;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->aM()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->a(Lax/l;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/ah;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v1, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v5

    move v0, p1

    invoke-static/range {v0 .. v6}, Lbf/O;->a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v6

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    goto :goto_0

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const/4 v6, 0x0

    goto :goto_0

    :sswitch_0
    const-string v0, "o"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->c(Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->d(Lax/b;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "t"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v1}, Lax/b;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Lcom/google/googlenav/ui/wizard/bN;->a(Lax/l;Lcom/google/googlenav/ui/wizard/bO;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v0

    invoke-virtual {v0, v7}, Lax/b;->r(I)V

    invoke-direct {p0, p2, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    const-string v0, "e"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->b(I)V

    const-string v0, "l"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    const-string v0, "od"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->z()V

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    goto/16 :goto_0

    :sswitch_7
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-ne v0, v8, :cond_1

    const-string v0, "nc"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    iput-object v9, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_1
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xe8
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0xd7 -> :sswitch_0
        0xd8 -> :sswitch_1
        0xd9 -> :sswitch_2
        0xda -> :sswitch_3
        0xdb -> :sswitch_4
        0xdc -> :sswitch_5
        0xdd -> :sswitch_6
        0x1f4 -> :sswitch_7
    .end sparse-switch
.end method

.method protected b()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-static {v0}, Lbf/O;->b(Lax/b;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0}, Lax/b;->az()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-interface {v2, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lax/b;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    invoke-direct {p0, v0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    instance-of v0, v0, Lax/s;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    const/16 v1, 0x2bd

    invoke-static {v0, v1, p0}, Lcom/google/googlenav/ui/f;->a(Lax/b;ILcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/C;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bN;->a()V

    :cond_4
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_5
    new-instance v0, Lcom/google/googlenav/ui/view/android/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/ah;-><init>(Lcom/google/googlenav/ui/e;Lax/b;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    :cond_0
    return-void
.end method

.method public e()Lax/b;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    return-object v0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->a:Lax/b;

    return-void
.end method

.method public g()V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/wizard/bv;->j:Lcom/google/googlenav/common/Config;

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PROTO_SAVED_DIRECTIONS_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    return-void
.end method

.method public h()V
    .locals 3

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/16 v0, 0x1f4

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bv;->a(IILjava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "b"

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->b(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bv;->A()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/bv;->a(I)I

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->g()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->c:Lcom/google/googlenav/J;

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(IZ)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bv;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bv;->b:Lax/b;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public i()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bv;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Ljava/lang/String;
    .locals 5

    const/16 v4, 0x26

    const/16 v3, 0x3d

    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Directions"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "saddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "daddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "dirflg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
