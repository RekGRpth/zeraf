.class public Lcom/google/googlenav/ui/wizard/fk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private a:Lbf/m;

.field private b:Lcom/google/googlenav/ui/view/dialog/bl;

.field private c:Lcom/google/googlenav/ai;

.field private i:I


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ai;ILbf/m;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lt p2, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    iput p2, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->j()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->a()V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->h()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3f9
        :pswitch_0
    .end packed-switch
.end method

.method protected b()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->h()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected c()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
