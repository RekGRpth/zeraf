.class public Lcom/google/googlenav/ui/wizard/fC;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fq;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/fq;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f001b

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void

    :cond_0
    const v0, 0x7f0f0018

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 7

    const v6, 0x7f10003e

    const v5, 0x7f10002e

    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ay;

    const v1, 0x7f100211

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f100212

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->d(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100213

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x372

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/fC;->b(Landroid/view/View;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->y()Z

    move-result v5

    const v0, 0x7f10003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x387

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/fD;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fD;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    if-eqz v5, :cond_2

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->l(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f100031

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/fE;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fE;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->l(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f100030

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x42f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/fF;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fF;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_4
    return-void

    :cond_1
    const/16 v1, 0x382

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3

    :cond_5
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fC;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fC;->m()V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/16 v0, 0x40c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    const v0, 0x7f1003ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f10048a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x36d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/google/googlenav/K;->S()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f10048b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v1, 0x375

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/fG;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fG;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/16 v0, 0x40d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_3
    const/16 v0, 0x386

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic l()Lcom/google/googlenav/android/BaseMapsActivity;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    return-object v0
.end method

.method private m()V
    .locals 3

    const/16 v0, 0x65

    const-string v1, "wp"

    const-string v2, "rm"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    const-string v1, "http://maps.google.com/help/maps/streetview/contribute/policies.html"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public I_()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x36c

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x387

    goto :goto_0
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/fq;->b(Lcom/google/googlenav/ui/wizard/fq;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cp"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->k(Lcom/google/googlenav/ui/wizard/fq;)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fC;->m()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f100172 -> :sswitch_0
        0x7f1004cd -> :sswitch_2
        0x7f1004ce -> :sswitch_1
    .end sparse-switch
.end method

.method protected c()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ui/wizard/fq;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->h(Lcom/google/googlenav/ui/wizard/fq;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fC;->a(Landroid/view/View;)V

    return-object v0
.end method

.method public h()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->q()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fC;->a(Landroid/view/View;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    const/16 v0, 0x65

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->b(Lcom/google/googlenav/ui/wizard/fq;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cd"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v3, 0x7f11001c

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->y()Z

    move-result v3

    const v0, 0x7f1004cd

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v4, 0x7f100172

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f1004ce

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/16 v6, 0x42f

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v6}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v6

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/16 v0, 0x69

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->l(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/16 v0, 0x388

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->l(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    move v2, v1

    :cond_1
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    move v2, v1

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method
