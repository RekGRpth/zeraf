.class public Lcom/google/googlenav/ui/wizard/bk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/wizard/bq;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    const v0, 0x7f1000b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bk;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bk;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/bk;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 5

    const v1, 0x7f100031

    const v2, 0x7f100030

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/bl;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bl;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/bm;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bm;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/bk;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/bk;->b(Landroid/view/View;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/ui/wizard/bq;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/bq;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/bq;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p2, v0, Lcom/google/googlenav/ui/wizard/bq;->b:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/bq;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-eqz p4, :cond_1

    :goto_1
    iput-object p4, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-boolean p5, v0, Lcom/google/googlenav/ui/wizard/bq;->e:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p6, v0, Lcom/google/googlenav/ui/wizard/bq;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-boolean p7, v0, Lcom/google/googlenav/ui/wizard/bq;->g:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object p8, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    return-void

    :cond_0
    const/16 v1, 0x35c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_1
    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p4

    goto :goto_1
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/wizard/bq;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/bo;->c(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->h()V

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/google/googlenav/ui/wizard/bk;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .locals 9

    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-object v6, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iput-object v4, v0, Lcom/google/googlenav/ui/wizard/bq;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    return-void
.end method

.method protected b()V
    .locals 2

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/bk;->g:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->e()Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/bn;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/bn;-><init>(Lcom/google/googlenav/ui/wizard/bk;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->j()V

    return-void
.end method

.method protected e()Lcom/google/googlenav/ui/view/android/S;
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/wizard/bp;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/bp;-><init>(Lcom/google/googlenav/ui/wizard/bk;Lcom/google/googlenav/ui/e;)V

    return-object v0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v1, v0, Lcom/google/googlenav/ui/wizard/bq;->h:Lcom/google/googlenav/ui/wizard/bo;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bk;->a()V

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/bo;->a(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bk;->a:Lcom/google/googlenav/ui/wizard/bq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/bq;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bk;->g()V

    return-void
.end method
