.class Lcom/google/googlenav/ui/wizard/ha;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filter$FilterListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/gZ;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/gZ;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/gZ;Lcom/google/googlenav/ui/wizard/gV;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/ha;-><init>(Lcom/google/googlenav/ui/wizard/gZ;)V

    return-void
.end method


# virtual methods
.method public onFilterComplete(I)V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/gZ;->a:Lcom/google/googlenav/ui/wizard/gU;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gU;->c(Lcom/google/googlenav/ui/wizard/gU;)Lcom/google/googlenav/ui/wizard/gC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gC;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/gZ;->a:Lcom/google/googlenav/ui/wizard/gU;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gU;->c(Lcom/google/googlenav/ui/wizard/gU;)Lcom/google/googlenav/ui/wizard/gC;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gC;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gA;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/gZ;->a:Lcom/google/googlenav/ui/wizard/gU;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/gU;->d(Lcom/google/googlenav/ui/wizard/gU;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/gZ;->a:Lcom/google/googlenav/ui/wizard/gU;

    invoke-static {v3}, Lcom/google/googlenav/ui/wizard/gU;->c(Lcom/google/googlenav/ui/wizard/gU;)Lcom/google/googlenav/ui/wizard/gC;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->c()Lcom/google/googlenav/friend/aD;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/gC;->a(Lcom/google/googlenav/friend/aD;)Z

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ha;->a:Lcom/google/googlenav/ui/wizard/gZ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/gZ;->a:Lcom/google/googlenav/ui/wizard/gU;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gU;->d(Lcom/google/googlenav/ui/wizard/gU;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    return-void
.end method
