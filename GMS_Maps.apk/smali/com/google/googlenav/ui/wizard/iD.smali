.class Lcom/google/googlenav/ui/wizard/iD;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/o;


# instance fields
.field final synthetic a:Lcom/google/googlenav/android/T;

.field final synthetic b:Lcom/google/googlenav/ui/wizard/iC;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/iC;Lcom/google/googlenav/android/T;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/iD;->a:Lcom/google/googlenav/android/T;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/iC;->b(Lcom/google/googlenav/ui/wizard/iC;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/googlenav/friend/android/InviteActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "inviteType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/iD;->a:Lcom/google/googlenav/android/T;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iD;->a(I)V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/iD;->a(I)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iC;->a()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/ui/wizard/iC;)Lcom/google/googlenav/ui/wizard/iG;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/ui/wizard/iC;)Lcom/google/googlenav/ui/wizard/iG;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/iG;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/iD;->b:Lcom/google/googlenav/ui/wizard/iC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iC;->a()V

    return-void
.end method
