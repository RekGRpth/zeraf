.class Lcom/google/googlenav/ui/wizard/aX;
.super Lcom/google/googlenav/ui/wizard/eI;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aX;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/util/List;I)V
    .locals 1

    invoke-static {p2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aX;->a:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/util/TreeSet;Lcom/google/googlenav/ui/view/android/U;)V
    .locals 5

    invoke-virtual {p1, p2}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->d()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/android/U;->d()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/util/TreeSet;Ljava/util/List;)V
    .locals 4

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->a()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/aX;->a(Ljava/util/TreeSet;Lcom/google/googlenav/ui/view/android/U;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/wizard/aY;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/aY;-><init>(Lcom/google/googlenav/ui/wizard/aX;)V

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aX;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eI;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eI;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/aX;->a(Ljava/util/TreeSet;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
