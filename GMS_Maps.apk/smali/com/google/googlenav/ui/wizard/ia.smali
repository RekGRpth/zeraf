.class public Lcom/google/googlenav/ui/wizard/ia;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:Lbf/m;

.field private i:Lcom/google/googlenav/J;

.field private j:Lbf/am;

.field private k:Lbf/bk;

.field private l:Lcom/google/googlenav/F;

.field private m:B

.field private n:I


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method


# virtual methods
.method public a(Lbf/i;)V
    .locals 2

    instance-of v0, p1, Lbf/cg;

    if-eqz v0, :cond_0

    check-cast p1, Lbf/cg;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v0, v1}, Lbf/cg;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ia;->a()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/aY;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x6f

    const-string v1, "uc"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    new-instance v0, Lcom/google/googlenav/bd;

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/googlenav/aY;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v5, v1, v2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bc;

    const/16 v2, 0x130

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p1, Lcom/google/googlenav/aY;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v7, v2, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x5f8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/bg;->k(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    instance-of v0, v0, Lbf/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    check-cast v0, Lbf/bk;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    iput-byte v0, p0, Lcom/google/googlenav/ui/wizard/ia;->m:B

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ia;->n:I

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    invoke-virtual {v0, v2}, Lbf/am;->h(Lbf/i;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/J;Lbf/am;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ia;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ia;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ia;->j()V

    return-void
.end method

.method public b()V
    .locals 4

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bz;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ia;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/bz;-><init>(Lcom/google/googlenav/ui/wizard/ia;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method public b(Lcom/google/googlenav/aY;)V
    .locals 3

    const/16 v0, 0x6f

    const-string v1, "up"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->l()Lcom/google/googlenav/L;

    move-result-object v0

    iget-object v1, p1, Lcom/google/googlenav/aY;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/L;->a(Ljava/lang/String;Lcom/google/googlenav/android/T;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public d()V
    .locals 6

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ia;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ia;->i:Lcom/google/googlenav/J;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ia;->a()V

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ia;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/J;Lbf/am;)V

    return-void
.end method

.method public h()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->c:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->j:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    invoke-virtual {v0, v1}, Lbf/am;->f(Lbf/i;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    invoke-virtual {v0, v1}, Lbf/bk;->b(Lcom/google/googlenav/F;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    iget-byte v1, p0, Lcom/google/googlenav/ui/wizard/ia;->m:B

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/ia;->n:I

    invoke-virtual {v0, v1}, Lbf/bk;->b(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->y()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/ia;->k:Lbf/bk;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/ia;->l:Lcom/google/googlenav/F;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ia;->a()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
