.class public Lcom/google/googlenav/ui/wizard/cx;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/cA;

.field private b:Ljava/util/List;

.field private c:I

.field private i:Ljava/util/List;

.field private j:Lcom/google/googlenav/common/f;

.field private k:Landroid/app/Activity;

.field private l:Lcom/google/googlenav/ui/friend/D;

.field private m:LaB/a;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaB/a;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->i()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->k:Landroid/app/Activity;

    :cond_0
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cx;->m:LaB/a;

    new-instance v0, Lcom/google/googlenav/ui/friend/D;

    const/4 v1, 0x0

    sget v2, Lcom/google/googlenav/ui/bi;->bp:I

    invoke-direct {v0, p2, v1, v2}, Lcom/google/googlenav/ui/friend/D;-><init>(LaB/a;Lcom/google/googlenav/friend/aK;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->l:Lcom/google/googlenav/ui/friend/D;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cx;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->i()I

    move-result v0

    return v0
.end method

.method private a(Lcom/google/googlenav/friend/aI;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cB;

    invoke-direct {v1, p1, p2}, Lcom/google/googlenav/ui/wizard/cB;-><init>(Lcom/google/googlenav/friend/aI;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->g()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->f()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cx;Lcom/google/googlenav/friend/aI;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/cx;->a(Lcom/google/googlenav/friend/aI;I)V

    return-void
.end method

.method private e()V
    .locals 8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/friend/aI;

    const/16 v0, 0x162

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/googlenav/common/f;->b(J)Z

    move-result v4

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/ar;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cx;->k:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/cx;->m:LaB/a;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/cx;->l:Lcom/google/googlenav/ui/friend/D;

    new-instance v7, Lcom/google/googlenav/ui/wizard/cy;

    invoke-direct {v7, p0}, Lcom/google/googlenav/ui/wizard/cy;-><init>(Lcom/google/googlenav/ui/wizard/cx;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/view/dialog/ar;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/google/googlenav/friend/aI;ZLaB/a;Lcom/google/googlenav/ui/friend/D;Lcom/google/googlenav/ui/view/dialog/av;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->h:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void
.end method

.method private g()V
    .locals 15

    const/4 v12, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v11

    new-instance v2, Lcom/google/googlenav/common/f;

    invoke-direct {v2}, Lcom/google/googlenav/common/f;-><init>()V

    new-instance v5, Lcom/google/googlenav/common/f;

    invoke-direct {v5}, Lcom/google/googlenav/common/f;-><init>()V

    new-instance v6, Lcom/google/googlenav/common/f;

    invoke-direct {v6}, Lcom/google/googlenav/common/f;-><init>()V

    new-instance v7, Lcom/google/googlenav/common/f;

    invoke-direct {v7}, Lcom/google/googlenav/common/f;-><init>()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    move v8, v12

    move v4, v12

    :goto_0
    if-ge v8, v9, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/cB;

    iget-object v10, v0, Lcom/google/googlenav/ui/wizard/cB;->a:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v10}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    iget v10, v0, Lcom/google/googlenav/ui/wizard/cB;->b:I

    packed-switch v10, :pswitch_data_0

    move v0, v4

    :goto_1
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v4, v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v2, v13, v14}, Lcom/google/googlenav/common/f;->a(J)V

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cB;->a:Lcom/google/googlenav/friend/aI;

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v0, v13, v14}, Lcom/google/googlenav/common/f;->d(J)Z

    move v0, v1

    goto :goto_1

    :pswitch_1
    invoke-virtual {v5, v13, v14}, Lcom/google/googlenav/common/f;->a(J)V

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cB;->a:Lcom/google/googlenav/friend/aI;

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v0, v13, v14}, Lcom/google/googlenav/common/f;->d(J)Z

    move v0, v1

    goto :goto_1

    :pswitch_2
    invoke-virtual {v6, v13, v14}, Lcom/google/googlenav/common/f;->a(J)V

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cB;->a:Lcom/google/googlenav/friend/aI;

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v0, v13, v14}, Lcom/google/googlenav/common/f;->b(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v0, v13, v14}, Lcom/google/googlenav/common/f;->a(J)V

    move v0, v1

    goto :goto_1

    :pswitch_3
    invoke-virtual {v7, v13, v14}, Lcom/google/googlenav/common/f;->a(J)V

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cB;->a:Lcom/google/googlenav/friend/aI;

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    if-eqz v4, :cond_2

    iget-object v10, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v13

    new-instance v0, Lcom/google/googlenav/ui/wizard/cz;

    move-object v1, p0

    move-object v4, v3

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/ui/wizard/cz;-><init>(Lcom/google/googlenav/ui/wizard/cx;Lcom/google/googlenav/common/f;Ljava/util/Vector;Ljava/util/Vector;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/common/f;Lcom/google/googlenav/ui/wizard/cA;Ljava/util/List;)V

    invoke-virtual {v13, v0}, Law/h;->c(Law/g;)V

    :goto_2
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    iput v12, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/cA;->a()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private i()I
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    if-lez v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->f()V

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cx;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cx;->j()V

    return-void
.end method

.method protected b()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "FRIENDS_INVITE_REJECTED"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Lcom/google/googlenav/common/f;->a([B)Lcom/google/googlenav/common/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cx;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/googlenav/common/f;

    invoke-direct {v0}, Lcom/google/googlenav/common/f;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/googlenav/common/f;

    invoke-direct {v0}, Lcom/google/googlenav/common/f;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    goto :goto_0
.end method

.method protected c()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    invoke-virtual {v0}, Lcom/google/googlenav/common/f;->c()[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, "FRIENDS_INVITE_REJECTED"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/cx;->j:Lcom/google/googlenav/common/f;

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/cx;->b:Ljava/util/List;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cx;->a()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/cx;->i:Ljava/util/List;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/cx;->a:Lcom/google/googlenav/ui/wizard/cA;

    iput v2, p0, Lcom/google/googlenav/ui/wizard/cx;->c:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/cx;->j()V

    return-void
.end method
