.class Lcom/google/googlenav/ui/wizard/gF;
.super Landroid/widget/AlphabetIndexer;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/gA;)V
    .locals 2

    const/16 v0, 0x8

    invoke-static {p1}, Lcom/google/googlenav/ui/wizard/gF;->a(Lcom/google/googlenav/ui/wizard/gA;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/AlphabetIndexer;-><init>(Landroid/database/Cursor;ILjava/lang/CharSequence;)V

    return-void
.end method

.method static a(Ljava/lang/String;)C
    .locals 3

    const/16 v0, 0x20

    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    goto :goto_0
.end method

.method static a(Lcom/google/googlenav/ui/wizard/gA;)Ljava/lang/CharSequence;
    .locals 3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gA;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/gF;->b(Lcom/google/googlenav/ui/wizard/gA;)C

    move-result v1

    if-eq v1, v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gA;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Lcom/google/googlenav/ui/wizard/gA;)C
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gA;->b()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    if-ne v0, v1, :cond_1

    :cond_0
    const/16 v0, 0x25cb

    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gA;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gF;->a(Ljava/lang/String;)C

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x20

    goto :goto_0
.end method
