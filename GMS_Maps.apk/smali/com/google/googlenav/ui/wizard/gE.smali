.class Lcom/google/googlenav/ui/wizard/gE;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/FilterQueryProvider;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/gC;

.field private b:Ljava/util/Map;

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/gC;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gE;->a:Lcom/google/googlenav/ui/wizard/gC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/gC;Lcom/google/googlenav/ui/wizard/gD;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/gE;-><init>(Lcom/google/googlenav/ui/wizard/gC;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gE;->a:Lcom/google/googlenav/ui/wizard/gC;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gC;->a(Lcom/google/googlenav/ui/wizard/gC;)Lcom/google/googlenav/ui/wizard/gA;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    const-string v2, ""

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    const-string v3, ""

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gA;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gE;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    const-string v2, ""

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/gA;

    :cond_2
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->getCount()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gE;->c:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->moveToFirst()Z

    :cond_4
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/googlenav/ui/wizard/gE;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->c()Lcom/google/googlenav/friend/aD;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gA;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v0, Lcom/google/googlenav/ui/wizard/gA;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/gA;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/wizard/gA;->a(Ljava/util/Collection;Lcom/google/googlenav/ui/wizard/gB;)V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gE;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gE;->c:Ljava/lang/String;

    goto :goto_0
.end method
