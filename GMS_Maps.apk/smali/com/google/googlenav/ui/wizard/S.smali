.class public Lcom/google/googlenav/ui/wizard/S;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/google/googlenav/ui/wizard/ac;

.field private final c:Lcom/google/googlenav/ui/wizard/aC;

.field private final d:Lcom/google/googlenav/ui/wizard/ad;

.field private e:Lcom/google/googlenav/ui/wizard/ah;

.field private final f:Lcom/google/googlenav/ui/wizard/aF;

.field private g:Z

.field private h:Lcom/google/googlenav/ui/wizard/an;

.field private i:Lcom/google/googlenav/h;

.field private j:Ljava/util/List;

.field private k:Z

.field private final l:Lcom/google/googlenav/ui/wizard/jv;

.field private m:Lcom/google/googlenav/aU;

.field private final n:Lcom/google/googlenav/ui/ak;

.field private final o:LaG/h;

.field private p:Lcom/google/googlenav/ui/wizard/aL;

.field private q:LaL/g;

.field private r:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/wizard/S;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;Lbf/a;ZLcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/ac;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/S;->g:Z

    iput-object p6, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/S;->f:Lcom/google/googlenav/ui/wizard/aF;

    new-instance v0, LaG/h;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v1

    invoke-direct {v0, v1}, LaG/h;-><init>(LaB/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->o:LaG/h;

    new-instance v0, Lcom/google/googlenav/ui/wizard/U;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/U;-><init>(Lcom/google/googlenav/ui/wizard/S;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->c:Lcom/google/googlenav/ui/wizard/aC;

    new-instance v0, Lcom/google/googlenav/ui/wizard/ad;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/wizard/ad;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    new-instance v0, Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->H()Lcom/google/googlenav/friend/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-direct {v0, v1, p3, v2}, Lcom/google/googlenav/ui/wizard/ah;-><init>(Lcom/google/googlenav/friend/j;Lbf/a;Lcom/google/googlenav/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/al;Lcom/google/googlenav/ui/wizard/L;)V

    invoke-virtual {p2}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/aL;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/aL;-><init>(LaH/m;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->p:Lcom/google/googlenav/ui/wizard/aL;

    return-void
.end method

.method static a(Z)I
    .locals 1

    if-eqz p0, :cond_0

    const/16 v0, 0xa

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/aU;)Lcom/google/googlenav/aU;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->m:Lcom/google/googlenav/aU;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/List;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->h(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/T;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/T;-><init>(Lcom/google/googlenav/ui/wizard/S;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method static a(Ljava/util/List;I)V
    .locals 1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, p1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;Ljava/util/List;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/S;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/S;)LaG/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->o:LaG/h;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/h;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/h;->f()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/h;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/friend/aD;->j()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private c()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->p:Lcom/google/googlenav/ui/wizard/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/af;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/af;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/aL;->a(LaH/A;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    return-object v0
.end method

.method private d()V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    const-string v0, "wifi"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/S;->g()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/List;

    new-instance v3, LaL/g;

    new-instance v4, Lcom/google/googlenav/ui/wizard/ag;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/googlenav/ui/wizard/ag;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    invoke-direct {v3, v1, v0, v4, v2}, LaL/g;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;LaL/i;I)V

    iput-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    invoke-virtual {v0}, LaL/g;->a()V

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/aF;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->f:Lcom/google/googlenav/ui/wizard/aF;

    return-object v0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    invoke-virtual {v0}, LaL/g;->b()V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->w()Z

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->a(Z)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/util/List;I)V

    :cond_0
    return-void
.end method

.method private static g()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/S;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->i()V

    return-void
.end method

.method private h()LaH/h;
    .locals 5

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/S;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    return-void
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ac;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    return-object v0
.end method

.method private i()V
    .locals 5

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->f()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/an;->z()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(I)V

    return-void
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    return-object v0
.end method

.method private j()V
    .locals 5

    const/16 v1, 0xae

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v0}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v0}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ac;->a()V

    return-void

    :cond_0
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v2}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/aU;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->m:Lcom/google/googlenav/aU;

    return-object v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/wizard/S;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->j()V

    return-void
.end method

.method static synthetic o(Lcom/google/googlenav/ui/wizard/S;)LaH/h;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->h()LaH/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/aC;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->c:Lcom/google/googlenav/ui/wizard/aC;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/h;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->d()V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ah;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ah;->a()V

    goto :goto_0
.end method

.method a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V
    .locals 2

    sget-boolean v0, Lcom/google/googlenav/ui/wizard/S;->a:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/S;->b(Lcom/google/googlenav/h;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/an;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0x606

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(I)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/wizard/an;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->h()LaH/h;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0xbd

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/ah;->a(II)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/wizard/ah;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->o()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->o()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    const/4 v0, 0x1

    goto :goto_0
.end method
