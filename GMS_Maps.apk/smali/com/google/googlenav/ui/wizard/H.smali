.class public Lcom/google/googlenav/ui/wizard/H;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/friend/j;

.field private final b:Lcom/google/googlenav/android/aa;

.field private c:Lcom/google/googlenav/ui/wizard/L;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/j;Lcom/google/googlenav/android/aa;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/H;->a:Lcom/google/googlenav/friend/j;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/H;->b:Lcom/google/googlenav/android/aa;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/H;)Lcom/google/googlenav/friend/j;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/H;->a:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/H;)Lcom/google/googlenav/ui/wizard/L;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/H;->c:Lcom/google/googlenav/ui/wizard/L;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/H;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/H;->b:Lcom/google/googlenav/android/aa;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/H;->a:Lcom/google/googlenav/friend/j;

    new-instance v1, Lcom/google/googlenav/ui/wizard/J;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/J;-><init>(Lcom/google/googlenav/ui/wizard/H;Lcom/google/googlenav/ui/wizard/I;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/j;->a(Lcom/google/googlenav/friend/k;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/H;->a:Lcom/google/googlenav/friend/j;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/j;->k()Lcom/google/googlenav/friend/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/friend/i;->a(Lcom/google/googlenav/ct;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/i;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/I;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/googlenav/ui/wizard/I;-><init>(Lcom/google/googlenav/ui/wizard/H;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ct;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/L;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/H;->c:Lcom/google/googlenav/ui/wizard/L;

    return-void
.end method
