.class public Lcom/google/googlenav/ui/wizard/D;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# static fields
.field private static c:Ljava/util/List;

.field private static i:Ljava/util/List;

.field private static j:Ljava/util/List;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/Set;

.field private k:Lcom/google/googlenav/ui/wizard/E;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->D()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->E()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->F()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/D;->j:Ljava/util/List;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->A()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->B()V

    return-void
.end method

.method private A()V
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->F:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v0, "PLACES_CUSTOM_CATEGORIES"

    invoke-static {v0, v1}, Lbm/e;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/16 v0, 0x57

    const-string v3, "n"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private B()V
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->b:Ljava/util/Set;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->G:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const-string v0, "PLACES_HIDDEN_CATEGORIES"

    invoke-static {v0, v1}, Lbm/e;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/D;->b:Ljava/util/Set;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private C()Ljava/util/List;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sget-object v3, Lcom/google/googlenav/ui/wizard/D;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->j:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private static D()Ljava/util/List;
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x39a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4, v4}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x394

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x391

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x390

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4, v5}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static E()Ljava/util/List;
    .locals 6

    const/4 v5, 0x6

    const/4 v4, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x395

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x38f

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x396

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4, v5}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x397

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x399

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x39d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static F()Ljava/util/List;
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x398

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xb

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x392

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/googlenav/ui/wizard/F;

    const/16 v2, 0x39b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xd

    invoke-direct {v1, v2, v4, v3}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/view/android/aL;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    :cond_0
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method private static a(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    iget-object v2, v0, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-interface {p0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->k:Lcom/google/googlenav/ui/wizard/E;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->k:Lcom/google/googlenav/ui/wizard/E;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/E;->g()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/D;->a()V

    return-void
.end method

.method public static i()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->D()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-static {}, Lcom/google/googlenav/ui/wizard/D;->E()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    return-void
.end method

.method private y()V
    .locals 5

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->F:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const-string v0, "PLACES_CUSTOM_CATEGORIES"

    invoke-static {v0, v2}, Lbm/e;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private z()V
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->G:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_0

    :cond_0
    const-string v0, "PLACES_HIDDEN_CATEGORIES"

    invoke-static {v0, v1}, Lbm/e;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/D;->h()V

    :cond_0
    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/E;)V
    .locals 2

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/D;->k:Lcom/google/googlenav/ui/wizard/E;

    new-instance v0, Lcom/google/googlenav/ui/wizard/a;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->C()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/a;-><init>(Lcom/google/googlenav/ui/wizard/D;Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/F;)V
    .locals 4

    const/16 v3, 0x57

    const/4 v2, 0x1

    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/F;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/D;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->y()V

    const-string v0, "rm"

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->b:Ljava/util/Set;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/D;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->z()V

    const-string v0, "hdn"

    iget v1, p1, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V
    .locals 1

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/D;->k:Lcom/google/googlenav/ui/wizard/E;

    new-instance v0, Lcom/google/googlenav/ui/wizard/hF;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/hF;-><init>(Lcom/google/googlenav/ui/wizard/D;Lcom/google/googlenav/ui/wizard/F;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x57

    const-string v1, "a"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Z)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/D;->y()V

    return-void
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/D;->k:Lcom/google/googlenav/ui/wizard/E;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Z)V

    return-void
.end method

.method public f()Ljava/util/List;
    .locals 7

    new-instance v3, Ljava/util/ArrayList;

    sget-object v0, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v0, Lcom/google/googlenav/ui/wizard/D;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/D;->b:Ljava/util/Set;

    iget v4, v0, Lcom/google/googlenav/ui/wizard/F;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/D;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Lcom/google/googlenav/ui/wizard/F;

    const/4 v6, 0x1

    add-int/lit8 v2, v1, 0x1

    invoke-direct {v5, v0, v6, v1}, Lcom/google/googlenav/ui/wizard/F;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method public g()Ljava/util/List;
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/D;->f()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/F;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/F;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v2, Lcom/google/googlenav/ui/wizard/D;->i:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/D;->a(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method
