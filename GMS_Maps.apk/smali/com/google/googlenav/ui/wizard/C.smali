.class public abstract Lcom/google/googlenav/ui/wizard/C;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/e;


# instance fields
.field protected final d:Lcom/google/googlenav/ui/wizard/jv;

.field protected final e:Lcom/google/googlenav/ui/bi;

.field protected f:Z

.field protected g:I

.field protected h:Lcom/google/googlenav/ui/view/android/aL;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/C;->g:I

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->e:Lcom/google/googlenav/ui/bi;

    return-void
.end method


# virtual methods
.method public a(LaN/B;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/C;->f:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ui/wizard/C;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public af_()V
    .locals 0

    return-void
.end method

.method public ag_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected ah_()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lat/a;)I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    :cond_0
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method public b(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected c()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    return-void
.end method

.method public h()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    return-void
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/C;->f:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/C;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->n()V

    :cond_1
    return-void
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public m()V
    .locals 0

    return-void
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/S;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/C;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->P_()Z

    move-result v0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/C;->f:Z

    return v0
.end method

.method public p()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/googlenav/ui/wizard/C;->g:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/googlenav/ui/wizard/C;->g:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public t()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public u()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
