.class public Lcom/google/googlenav/ui/wizard/eq;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements LaM/h;
.implements LaQ/b;


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private b:LaQ/a;

.field private c:[LaQ/c;

.field private i:[LaQ/c;

.field private j:J

.field private k:Lcom/google/googlenav/ui/wizard/es;

.field private l:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->c:[LaQ/c;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->i:[LaQ/c;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/eq;->j:J

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eq;->a:Lcom/google/googlenav/J;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eq;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->l:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eq;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eq;->l:Landroid/view/LayoutInflater;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eq;Lcom/google/googlenav/ui/wizard/es;)Lcom/google/googlenav/ui/wizard/es;
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eq;->k:Lcom/google/googlenav/ui/wizard/es;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eq;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/eq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->a:Lcom/google/googlenav/J;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a([LaQ/c;Ljava/lang/String;)V
    .locals 4

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->k:Lcom/google/googlenav/ui/wizard/es;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/wizard/es;->add(Ljava/lang/Object;)V

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/eq;->k:Lcom/google/googlenav/ui/wizard/es;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/wizard/es;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eq;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/eq;->g()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/eq;)Lcom/google/googlenav/ui/wizard/es;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->k:Lcom/google/googlenav/ui/wizard/es;

    return-object v0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->c:[LaQ/c;

    const/16 v1, 0x2d4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/eq;->a([LaQ/c;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->i:[LaQ/c;

    const/16 v1, 0x2d5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/eq;->a([LaQ/c;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public C_()V
    .locals 0

    return-void
.end method

.method public D_()V
    .locals 0

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public M_()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->c:[LaQ/c;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->i:[LaQ/c;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/eq;->j:J

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method public a([LaQ/c;[LaQ/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eq;->c:[LaQ/c;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eq;->i:[LaQ/c;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/eq;->j:J

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eq;->j()V

    return-void
.end method

.method protected b()V
    .locals 7

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/eq;->g:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/googlenav/ui/wizard/eq;->j:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eq;->c:[LaQ/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eq;->i:[LaQ/c;

    if-eqz v2, :cond_0

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :cond_0
    new-instance v0, LaQ/a;

    invoke-direct {v0}, LaQ/a;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    invoke-virtual {v0, p0}, LaQ/a;->a(LaQ/b;)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    const/16 v0, 0xc

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x258

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    :cond_1
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eq;->a()V

    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/google/googlenav/ui/wizard/eu;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/eu;-><init>(Lcom/google/googlenav/ui/wizard/eq;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    invoke-virtual {v0}, LaQ/a;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x2da

    :goto_0
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    invoke-virtual {v1}, LaQ/a;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, LaM/f;->a(I)V

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eq;->b:LaQ/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eq;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    return-void

    :cond_2
    const/16 v0, 0x2d6

    goto :goto_0
.end method

.method public h()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eq;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eq;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/eq;->g:I

    return-void
.end method
