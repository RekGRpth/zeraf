.class public Lcom/google/googlenav/ui/wizard/aZ;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/friend/history/o;

.field private b:Lcom/google/googlenav/ui/wizard/ba;

.field private final c:Lcom/google/googlenav/friend/history/z;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/history/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/aZ;->c:Lcom/google/googlenav/friend/history/z;

    return-void
.end method

.method public static a(Lcom/google/googlenav/h;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    const/4 v4, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/aN;->r:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/az;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/h;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/h;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/h;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/googlenav/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :goto_0
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/googlenav/h;->f()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/googlenav/h;->f()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/h;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0

    :cond_1
    const-string v1, ""

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/aZ;)Lcom/google/googlenav/friend/history/o;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->a:Lcom/google/googlenav/friend/history/o;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://maps.google.com/maps?cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/aZ;)Lcom/google/googlenav/ui/wizard/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->b:Lcom/google/googlenav/ui/wizard/ba;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/aZ;)Lcom/google/googlenav/friend/history/z;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->c:Lcom/google/googlenav/friend/history/z;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aZ;->a:Lcom/google/googlenav/friend/history/o;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/aZ;->b:Lcom/google/googlenav/ui/wizard/ba;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/aZ;->j()V

    return-void
.end method

.method protected b()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/X;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/aZ;->a:Lcom/google/googlenav/friend/history/o;

    new-instance v2, Lcom/google/googlenav/ui/wizard/bb;

    invoke-direct {v2, p0, p0}, Lcom/google/googlenav/ui/wizard/bb;-><init>(Lcom/google/googlenav/ui/wizard/aZ;Lcom/google/googlenav/ui/wizard/aZ;)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/X;-><init>(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/view/dialog/ah;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->h:Lcom/google/googlenav/ui/view/android/aL;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->b:Lcom/google/googlenav/ui/wizard/ba;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/aZ;->a:Lcom/google/googlenav/friend/history/o;

    return-void
.end method
