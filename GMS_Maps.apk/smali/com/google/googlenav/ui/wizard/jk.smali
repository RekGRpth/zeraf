.class public Lcom/google/googlenav/ui/wizard/jk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/aR;


# instance fields
.field a:Landroid/widget/Button;

.field b:Landroid/widget/Button;

.field private c:Z

.field private i:Z

.field private j:[B


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/jk;)[B
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->j:[B

    return-object v0
.end method


# virtual methods
.method a(Landroid/view/View;)V
    .locals 5

    const v1, 0x7f100031

    const v2, 0x7f100030

    const/4 v4, 0x0

    const v0, 0x7f10002e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    const/16 v1, 0x1b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    new-instance v1, Lcom/google/googlenav/ui/wizard/jm;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/jm;-><init>(Lcom/google/googlenav/ui/wizard/jk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    const/16 v1, 0xe4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    new-instance v1, Lcom/google/googlenav/ui/wizard/jn;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/jn;-><init>(Lcom/google/googlenav/ui/wizard/jk;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 3

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jk;->j:[B

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jk;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/jl;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/jl;-><init>(Lcom/google/googlenav/ui/wizard/jk;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->i:Z

    goto :goto_0

    :pswitch_1
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/jk;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->i:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->i:Z

    new-instance v0, Lcom/google/googlenav/aP;

    invoke-static {}, Lcom/google/googlenav/K;->W()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/googlenav/aP;-><init>(Ljava/lang/String;Lcom/google/googlenav/aR;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->j:[B

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->c:Z

    return v0
.end method

.method f()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jk;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public k()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jk;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jk;->i:Z

    return v0
.end method
