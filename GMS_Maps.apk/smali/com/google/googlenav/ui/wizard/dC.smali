.class public Lcom/google/googlenav/ui/wizard/dC;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    const v0, 0x7f0f001b

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dC;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dC;)Lcom/google/googlenav/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dC;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dC;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :goto_0
    new-instance v0, Lcom/google/googlenav/ui/wizard/dD;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dD;-><init>(Lcom/google/googlenav/ui/wizard/dC;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dC;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected O_()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dC;->requestWindowFeature(I)Z

    :cond_0
    return-void
.end method

.method public c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/google/googlenav/ui/wizard/dC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ea

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f02022a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dC;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v2, Lcom/google/googlenav/ui/wizard/dE;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dE;-><init>(Lcom/google/googlenav/ui/wizard/dC;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f020228

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const v2, 0x7f020215

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
