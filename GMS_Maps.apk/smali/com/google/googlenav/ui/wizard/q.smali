.class public Lcom/google/googlenav/ui/wizard/q;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/ui/wizard/x;

.field private b:Lcom/google/googlenav/ui/wizard/e;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/q;)Lcom/google/googlenav/ui/wizard/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ai;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Lcom/google/googlenav/aS;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/x;->c:LaN/B;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;III)V

    new-instance v1, Lcom/google/googlenav/ui/wizard/s;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/s;-><init>(Lcom/google/googlenav/ui/wizard/q;Lcom/google/googlenav/ai;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/h;

    invoke-direct {v0, p1}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, p2}, Lcom/google/googlenav/h;->a(Z)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    if-eqz v1, :cond_0

    const-string v1, "x"

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/q;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/y;->a(Lcom/google/googlenav/h;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/q;Lcom/google/googlenav/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/q;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    return-void
.end method


# virtual methods
.method public a(Lat/a;)I
    .locals 2

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->h()V

    iget v0, p0, Lcom/google/googlenav/ui/wizard/q;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/wizard/q;->g:I

    goto :goto_0
.end method

.method public a(Lat/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/wizard/q;->g:I

    return v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/x;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->j()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0x66

    const-string v1, "a"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/e;-><init>(Lcom/google/googlenav/ui/wizard/q;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->show()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->h()V

    return-void
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/y;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->dismiss()V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    :cond_1
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->a()V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/ui/wizard/x;)V

    return-void
.end method

.method public e()V
    .locals 8

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/android/V;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/y;->a()V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    const-string v1, "GO AWAY MONKEY"

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->b:Lcom/google/googlenav/aU;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->b:Lcom/google/googlenav/aU;

    invoke-virtual {v0}, Lcom/google/googlenav/aU;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->b:Lcom/google/googlenav/aU;

    invoke-virtual {v0}, Lcom/google/googlenav/aU;->d()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v7

    new-instance v0, Lcom/google/googlenav/friend/l;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/x;->c:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/x;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/e;->l()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Lcom/google/googlenav/ui/wizard/r;

    invoke-direct {v6, p0}, Lcom/google/googlenav/ui/wizard/r;-><init>(Lcom/google/googlenav/ui/wizard/q;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/friend/l;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/friend/m;)V

    invoke-virtual {v7, v0}, Law/h;->c(Law/g;)V

    :cond_2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    const/16 v1, 0x26

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v4, v5

    goto :goto_1
.end method

.method public f()V
    .locals 2

    const-string v0, "l"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->hide()V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/y;->b()V

    new-instance v0, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    const/16 v1, 0x50f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    new-instance v1, Lcom/google/googlenav/ui/wizard/t;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/t;-><init>(Lcom/google/googlenav/ui/wizard/q;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method public g()V
    .locals 1

    const-string v0, "c"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->a()V

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/googlenav/ui/wizard/w;->a:[I

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/e;->n()Lcom/google/googlenav/ui/wizard/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    const-string v0, "b"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/q;->a()V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->h()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public i()V
    .locals 7

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/e;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x28

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v6, Lcom/google/googlenav/ui/wizard/u;

    invoke-direct {v6, p0}, Lcom/google/googlenav/ui/wizard/u;-><init>(Lcom/google/googlenav/ui/wizard/q;)V

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "a"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/q;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->a:Lcom/google/googlenav/ui/wizard/x;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/x;->c:LaN/B;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/q;->b:Lcom/google/googlenav/ui/wizard/e;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/e;->l()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/v;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/v;-><init>(Lcom/google/googlenav/ui/wizard/q;)V

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/p;->a(LaN/B;Ljava/lang/String;Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/be;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/q;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x2d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    goto :goto_0
.end method
