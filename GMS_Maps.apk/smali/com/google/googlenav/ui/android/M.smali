.class public Lcom/google/googlenav/ui/android/M;
.super Lcom/google/googlenav/ui/view/d;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/googlenav/ui/android/aF;


# instance fields
.field private final b:Lcom/google/googlenav/ui/android/ElevationChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/ui/android/M;->a:Lcom/google/googlenav/ui/android/aF;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V
    .locals 2

    invoke-static {p1, p2}, Lcom/google/googlenav/ui/android/M;->a(Lcom/google/googlenav/ui/android/ButtonContainer;I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->d:Landroid/view/View;

    const v1, 0x7f100193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ElevationChartView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/M;->b:Lcom/google/googlenav/ui/android/ElevationChartView;

    return-void
.end method

.method private static a(Lcom/google/googlenav/ui/android/ButtonContainer;I)Landroid/view/View;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->b:Lcom/google/googlenav/ui/android/ElevationChartView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ElevationChartView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->b:Lcom/google/googlenav/ui/android/ElevationChartView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/android/ElevationChartView;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->d:Landroid/view/View;

    const v1, 0x7f100192

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/M;->b()V

    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 2

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->b:Lcom/google/googlenav/ui/android/ElevationChartView;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/ElevationChartView;->a(Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/M;->b:Lcom/google/googlenav/ui/android/ElevationChartView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ElevationChartView;->setVisibility(I)V

    return-void
.end method

.method public b()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/d;->b()V

    return-void
.end method

.method public c()V
    .locals 0

    invoke-super {p0}, Lcom/google/googlenav/ui/view/d;->c()V

    return-void
.end method
