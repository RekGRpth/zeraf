.class public Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/googlenav/ui/android/ai;


# instance fields
.field a:Landroid/widget/ListAdapter;

.field private b:Lcom/google/googlenav/ui/android/aj;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Landroid/widget/TextView;

.field private f:I

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "ListPopupSpinnerSdk5 should be used with stylable attributes."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/maps/R$styleable;->ListPopupSpinnerSdk5:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->g:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Popup dialog for ListPopupSpinnerSdk5 is not defined."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0400d6

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->e:Landroid/widget/TextView;

    new-instance v0, Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a:Landroid/widget/ListAdapter;

    iget v6, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->f:I

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/android/aj;-><init>(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;Landroid/content/Context;Landroid/view/View;Landroid/widget/ListAdapter;Landroid/widget/AdapterView$OnItemClickListener;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->h:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->h:Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->dismiss()V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->h:Z

    return v0
.end method

.method public c()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->l()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->h()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->l()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->show()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->h:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aj;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->d:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->d()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->e:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->c:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->b:Lcom/google/googlenav/ui/android/aj;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/android/aj;->a(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->d:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->c:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ListPopupSpinnerSdk5;->a:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
