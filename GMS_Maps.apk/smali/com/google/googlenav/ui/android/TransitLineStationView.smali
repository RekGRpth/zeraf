.class public Lcom/google/googlenav/ui/android/TransitLineStationView;
.super Lcom/google/googlenav/ui/android/TransitStationView;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/TransitStationView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/TransitStationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/co;)V
    .locals 6

    const/4 v0, 0x3

    const/4 v1, 0x0

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/co;->d()I

    move-result v3

    if-le v3, v0, :cond_0

    :goto_0
    new-array v3, v0, [Lcom/google/googlenav/cc;

    move v0, v1

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/co;->a(I)Lcom/google/googlenav/cc;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/co;->d()I

    move-result v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/googlenav/co;

    invoke-virtual {p1}, Lcom/google/googlenav/co;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/co;->e()Z

    move-result v5

    invoke-direct {v0, v4, v3, v5}, Lcom/google/googlenav/co;-><init>(Ljava/lang/String;[Lcom/google/googlenav/cc;Z)V

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/ui/android/TransitLineStationView;->a(Landroid/text/SpannableStringBuilder;Lcom/google/googlenav/co;)V

    const v0, 0x7f100432

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/TransitLineStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public setTransitLineStation(Lcom/google/googlenav/bZ;Lcom/google/googlenav/co;Lcom/google/googlenav/ui/e;II)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0, p1, p3}, Lcom/google/googlenav/ui/android/TransitLineStationView;->setTransitStation(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/android/TransitLineStationView;->setClickable(Z)V

    if-ge p4, p5, :cond_0

    const v0, 0x7f0f00e9

    move v1, v0

    :goto_0
    const v0, 0x7f100092

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/TransitLineStationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/TransitLineStationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/android/TransitLineStationView;->setWillNotDraw(Z)V

    return-void

    :cond_0
    if-le p4, p5, :cond_1

    const v0, 0x7f0f00eb

    move v1, v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0f00ea

    if-eqz p2, :cond_2

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/TransitLineStationView;->a(Lcom/google/googlenav/co;)V

    :cond_2
    move v1, v0

    goto :goto_0
.end method
