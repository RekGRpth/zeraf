.class Lcom/google/googlenav/ui/android/l;
.super Lcom/google/android/maps/driveabout/vector/VectorMapView;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private final c:LaO/a;

.field private d:Lcom/google/android/maps/driveabout/vector/aZ;

.field private final e:Lcom/google/android/maps/driveabout/vector/aZ;

.field private f:Lcom/google/android/maps/driveabout/vector/aq;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;)V
    .locals 3

    iput-object p1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/driveabout/vector/VectorMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object p4, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/l;->C()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setDefaultLabelTheme(LG/a;)V

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/l;->D()V

    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(I)V

    invoke-virtual {p4}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LA/c;->c:LA/c;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->r()Lg/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/c;LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->e:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->e:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-virtual {p4}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LA/c;->p:LA/c;

    :goto_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Ln/s;->a()Ln/s;

    move-result-object v1

    invoke-static {p2, v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/Context;Landroid/content/res/Resources;Ln/s;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aq;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->f:Lcom/google/android/maps/driveabout/vector/aq;

    :cond_1
    return-void

    :cond_2
    sget-object v0, LA/c;->o:LA/c;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;Lcom/google/googlenav/ui/android/f;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/android/l;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;)V

    return-void
.end method

.method private C()LG/a;
    .locals 4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LG/a;->u:LG/a;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    sget-object v0, LG/a;->v:LG/a;

    goto :goto_0

    :cond_1
    sget-object v0, LG/a;->t:LG/a;

    goto :goto_0
.end method

.method private D()V
    .locals 5

    sget-object v0, LaE/a;->a:LaE/a;

    invoke-virtual {v0}, LaE/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->x()LG/a;

    move-result-object v0

    const/4 v1, 0x2

    const v2, 0x3e4ccccd

    const/4 v3, 0x0

    sget-object v4, LaE/a;->b:Lcom/google/android/maps/driveabout/vector/aX;

    invoke-static {v0, v1, v2, v3, v4}, LG/a;->a(LG/a;IFFLcom/google/android/maps/driveabout/vector/aX;)LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setLabelTheme(LG/a;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/l;->x()LG/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->setLabelTheme(LG/a;)V

    goto :goto_0
.end method


# virtual methods
.method public A()Lcom/google/android/maps/driveabout/vector/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->f:Lcom/google/android/maps/driveabout/vector/aq;

    return-object v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    invoke-virtual {v0}, LaO/a;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->o:LA/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    sget-object v0, LA/c;->p:LA/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aZ;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->p:LA/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    sget-object v0, LA/c;->o:LA/c;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/l;->b:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v1, v1, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->d:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/l;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_0
.end method

.method public j()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j()V

    invoke-static {}, Lah/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lah/e;->b()Lah/e;

    move-result-object v0

    invoke-virtual {v0}, Lah/e;->d()V

    :cond_0
    return-void
.end method

.method public o_()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o_()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/l;->c:LaO/a;

    invoke-virtual {v0}, LaO/a;->x()V

    return-void
.end method
