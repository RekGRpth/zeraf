.class public Lcom/google/googlenav/ui/android/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Landroid/text/SpannableStringBuilder;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ak;->a:Ljava/util/ArrayList;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    iput-object p1, p0, Lcom/google/googlenav/ui/android/ak;->c:Landroid/content/Context;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ak;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/googlenav/ui/android/ak;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Lcom/google/googlenav/ui/android/ak;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/ak;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/google/googlenav/ui/android/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/ak;->c()V

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Ljava/lang/Object;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/ak;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    return v0
.end method
