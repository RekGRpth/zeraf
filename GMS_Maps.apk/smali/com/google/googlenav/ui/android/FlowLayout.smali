.class public Lcom/google/googlenav/ui/android/FlowLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:I

.field private final c:I

.field private final d:I

.field private e:Z

.field private f:Lcom/google/googlenav/ui/android/aa;

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/android/FlowLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/android/FlowLayout;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->h:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->j:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->d:I

    new-instance v0, Lcom/google/googlenav/ui/android/aa;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/aa;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->h:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->j:I

    const-string v0, "horizontalSpacing"

    invoke-interface {p2, v2, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    const-string v0, "verticalSpacing"

    invoke-interface {p2, v2, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->d:I

    new-instance v0, Lcom/google/googlenav/ui/android/aa;

    invoke-direct {v0, p2}, Lcom/google/googlenav/ui/android/aa;-><init>(Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->h:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->j:I

    const-string v0, "horizontalSpacing"

    invoke-interface {p2, v2, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    const-string v0, "verticalSpacing"

    invoke-interface {p2, v2, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->d:I

    new-instance v0, Lcom/google/googlenav/ui/android/aa;

    invoke-direct {v0, p2}, Lcom/google/googlenav/ui/android/aa;-><init>(Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    return-void
.end method

.method private a(I)I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->e:Z

    if-nez v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :cond_1
    iget v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    add-int/2addr v0, v1

    div-int v1, p1, v0

    mul-int/2addr v0, v1

    sub-int v0, p1, v0

    iget v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iget v2, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->h:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->j:I

    return-void
.end method

.method private a(Landroid/view/View;II)Z
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p2

    if-gt v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->j:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private b()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c()I
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget v0, v0, Lcom/google/googlenav/ui/android/aa;->a:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget v1, v1, Lcom/google/googlenav/ui/android/aa;->b:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget v0, v0, Lcom/google/googlenav/ui/android/aa;->a:I

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v1

    if-le v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget v0, v0, Lcom/google/googlenav/ui/android/aa;->a:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget v0, v0, Lcom/google/googlenav/ui/android/aa;->b:I

    goto :goto_0
.end method

.method private c(I)Landroid/view/View;
    .locals 4

    add-int/lit8 v0, p1, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 12

    sub-int v0, p4, p2

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->b(I)I

    move-result v5

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->c()I

    move-result v6

    invoke-direct {p0, v5}, Lcom/google/googlenav/ui/android/FlowLayout;->a(I)I

    move-result v3

    iget v2, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v8, 0x8

    if-eq v4, v8, :cond_6

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int v4, v3, v8

    if-le v4, v5, :cond_4

    if-lt v1, v6, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    invoke-direct {p0, v5}, Lcom/google/googlenav/ui/android/FlowLayout;->a(I)I

    move-result v3

    iget v4, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    iget v10, p0, Lcom/google/googlenav/ui/android/FlowLayout;->d:I

    add-int/2addr v4, v10

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    :cond_4
    add-int v4, v3, v8

    iget v10, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    add-int/2addr v4, v10

    if-lt v1, v6, :cond_5

    iget-object v10, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    iget-object v10, v10, Lcom/google/googlenav/ui/android/aa;->c:Lcom/google/googlenav/ui/android/Z;

    sget-object v11, Lcom/google/googlenav/ui/android/Z;->a:Lcom/google/googlenav/ui/android/Z;

    if-ne v10, v11, :cond_7

    :cond_5
    add-int/2addr v8, v3

    add-int/2addr v9, v2

    invoke-virtual {v7, v3, v2, v8, v9}, Landroid/view/View;->layout(IIII)V

    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    move v3, v4

    :cond_6
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->c(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_a

    invoke-direct {p0, v10, v4, v5}, Lcom/google/googlenav/ui/android/FlowLayout;->a(Landroid/view/View;II)Z

    move-result v10

    if-eqz v10, :cond_8

    add-int/2addr v8, v3

    add-int/2addr v9, v2

    invoke-virtual {v7, v3, v2, v8, v9}, Landroid/view/View;->layout(IIII)V

    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    move v3, v4

    goto :goto_3

    :cond_8
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v4, v5}, Lcom/google/googlenav/ui/android/FlowLayout;->a(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_9

    add-int v1, v3, v8

    add-int v5, v2, v9

    invoke-virtual {v7, v3, v2, v1, v5}, Landroid/view/View;->layout(IIII)V

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    move v3, v4

    :cond_9
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v3

    add-int/2addr v4, v2

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_a
    add-int v0, v3, v8

    add-int v1, v2, v9

    invoke-virtual {v7, v3, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 12

    const/16 v11, 0x8

    const/high16 v10, -0x80000000

    const/4 v1, 0x0

    sget-boolean v0, Lcom/google/googlenav/ui/android/FlowLayout;->a:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->a()V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->b(I)I

    move-result v5

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->c()I

    move-result v6

    iget v4, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iget v3, p0, Lcom/google/googlenav/ui/android/FlowLayout;->g:I

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v7

    if-ge v0, v7, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eq v8, v11, :cond_1

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/view/View;->measure(II)V

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    const/4 v0, 0x1

    move v2, v3

    move v3, v4

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eq v7, v11, :cond_6

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget v8, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    const/high16 v9, 0x40000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int v7, v3, v4

    if-le v7, v5, :cond_5

    if-lt v0, v6, :cond_4

    :cond_3
    iget v0, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    add-int/2addr v0, v2

    iget v1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->h:I

    add-int/2addr v0, v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/android/FlowLayout;->setMeasuredDimension(II)V

    return-void

    :cond_4
    iget v3, p0, Lcom/google/googlenav/ui/android/FlowLayout;->i:I

    iget v7, p0, Lcom/google/googlenav/ui/android/FlowLayout;->b:I

    iget v8, p0, Lcom/google/googlenav/ui/android/FlowLayout;->d:I

    add-int/2addr v7, v8

    add-int/2addr v2, v7

    add-int/lit8 v0, v0, 0x1

    :cond_5
    iget v7, p0, Lcom/google/googlenav/ui/android/FlowLayout;->c:I

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public setCenterChildren(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->e:Z

    return-void
.end method

.method public setParams(Lcom/google/googlenav/ui/android/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/FlowLayout;->f:Lcom/google/googlenav/ui/android/aa;

    return-void
.end method
