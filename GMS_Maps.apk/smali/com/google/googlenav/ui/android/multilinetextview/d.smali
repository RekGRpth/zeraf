.class Lcom/google/googlenav/ui/android/multilinetextview/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/multilinetextview/b;

.field private b:Lcom/google/googlenav/ui/android/multilinetextview/e;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/multilinetextview/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/android/multilinetextview/b;Lcom/google/googlenav/ui/android/multilinetextview/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/d;-><init>(Lcom/google/googlenav/ui/android/multilinetextview/b;)V

    return-void
.end method

.method private c()Z
    .locals 4

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->b(Lcom/google/googlenav/ui/android/multilinetextview/b;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v0

    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/multilinetextview/b;->b(Lcom/google/googlenav/ui/android/multilinetextview/b;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v3, v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I

    move-result v1

    iget-object v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v3, v2}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v1

    iget v2, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->a()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v3, v2}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->a:Lcom/google/googlenav/ui/android/multilinetextview/b;

    invoke-static {v3, v1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I

    move-result v1

    if-le v2, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()Lcom/google/googlenav/ui/android/multilinetextview/e;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    return-object v0
.end method

.method a(Lcom/google/googlenav/ui/android/multilinetextview/e;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/d;->d(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    :cond_0
    return-void
.end method

.method a(Lcom/google/googlenav/ui/android/multilinetextview/e;Z)Z
    .locals 1

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/d;->c(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/multilinetextview/d;->c()Z

    move-result v0

    goto :goto_0
.end method

.method b()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    return-void
.end method

.method b(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/d;->b:Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/multilinetextview/e;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
