.class Lcom/google/googlenav/ui/android/multilinetextview/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->a:I

    iput p2, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->b:I

    iput p3, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->c:I

    return-void
.end method

.method private a(I)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->b:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(II)I
    .locals 1

    invoke-static {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->b(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/multilinetextview/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->b:I

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/multilinetextview/b;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/android/multilinetextview/b;->a(I)I

    move-result v0

    return v0
.end method

.method private static b(II)I
    .locals 1

    sub-int v0, p1, p0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/multilinetextview/b;)I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->c:I

    return v0
.end method


# virtual methods
.method public a(Ljava/util/ListIterator;)Ljava/util/List;
    .locals 4

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->a:I

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/android/multilinetextview/d;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/d;-><init>(Lcom/google/googlenav/ui/android/multilinetextview/b;Lcom/google/googlenav/ui/android/multilinetextview/c;)V

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lcom/google/googlenav/ui/android/multilinetextview/b;->a:I

    if-ge v0, v3, :cond_2

    invoke-interface {p1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/multilinetextview/e;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/multilinetextview/d;->a(Lcom/google/googlenav/ui/android/multilinetextview/e;)V

    invoke-interface {p1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/ui/android/multilinetextview/d;->a(Lcom/google/googlenav/ui/android/multilinetextview/e;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/multilinetextview/d;->a()Lcom/google/googlenav/ui/android/multilinetextview/e;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/multilinetextview/d;->b(Lcom/google/googlenav/ui/android/multilinetextview/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    :cond_1
    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/multilinetextview/d;->b()V

    goto :goto_0

    :cond_2
    return-object v1
.end method
