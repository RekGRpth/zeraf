.class public Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Lcom/google/googlenav/ui/android/multilinetextview/j;->a()Lcom/google/googlenav/ui/android/multilinetextview/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    if-nez v0, :cond_0

    iput v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    if-nez v0, :cond_1

    iput v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    :cond_1
    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    if-nez v0, :cond_2

    iput v1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    const/4 v1, -0x1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->getLineHeight()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public getMaxLines()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    return v0
.end method

.method public getMinEms()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    return v0
.end method

.method public getMinWidth()I
    .locals 1

    iget v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    return v0
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEms(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setEms(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    return-void
.end method

.method public setHeight(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setHeight(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->getLineHeight()I

    move-result v0

    div-int v0, p1, v0

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    return-void
.end method

.method public setLines(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/TextView;->setLines(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    return-void
.end method

.method public setMaxHeight(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMaxHeight(I)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->getLineHeight()I

    move-result v0

    div-int v0, p1, v0

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->a:I

    return-void
.end method

.method public setMinEms(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMinEms(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    return-void
.end method

.method public setMinWidth(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMinWidth(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    return-void
.end method

.method public setWidth(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setWidth(I)V

    iput p1, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/android/multilinetextview/MultilineTextView;->b:I

    return-void
.end method
