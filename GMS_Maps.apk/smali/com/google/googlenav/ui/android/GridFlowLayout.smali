.class public Lcom/google/googlenav/ui/android/GridFlowLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/ui/android/GridFlowLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/ui/android/GridFlowLayout;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->d:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->f:I

    new-instance v0, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->d:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->f:I

    new-instance v0, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->d:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->f:I

    new-instance v0, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    return-void
.end method

.method private a(I)I
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v1}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v1

    sub-int v1, p1, v1

    if-gez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->d(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v3

    add-int/2addr v2, v3

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->d:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->f:I

    return-void
.end method

.method private b()I
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private b(I)I
    .locals 2

    iget v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->f:I

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10

    const/4 v1, 0x0

    sub-int v0, p4, p2

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->b(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->a(I)I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v2

    mul-int/2addr v2, v5

    add-int/lit8 v3, v5, -0x1

    iget-object v4, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v4}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->d(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    div-int/lit8 v6, v0, 0x2

    iget v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    add-int v2, v0, v6

    iget v0, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    move v3, v0

    move v4, v2

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildCount()I

    move-result v7

    if-ge v0, v7, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_3

    if-lt v2, v5, :cond_2

    iget v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->e:I

    add-int v4, v2, v6

    iget-object v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v2}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->a(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v2

    iget-object v8, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v8}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->b(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v8

    add-int/2addr v2, v8

    add-int/2addr v3, v2

    move v2, v1

    :cond_2
    iget-object v8, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v8}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v8

    add-int/2addr v8, v4

    iget-object v9, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v9}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->a(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v7, v4, v3, v8, v9}, Landroid/view/View;->layout(IIII)V

    iget-object v7, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v7}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v7

    iget-object v8, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v8}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->d(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v4, v7

    add-int/lit8 v2, v2, 0x1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    const/high16 v6, 0x40000000

    sget-boolean v0, Lcom/google/googlenav/ui/android/GridFlowLayout;->a:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->a()V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/android/GridFlowLayout;->b(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->a(I)I

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->b()I

    move-result v0

    div-int/2addr v0, v2

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->b()I

    move-result v3

    mul-int/2addr v2, v0

    if-le v3, v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    iget v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->c:I

    iget-object v3, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->a(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v3

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v3}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->b(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v3

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->d:I

    add-int/2addr v2, v0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/GridFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v4}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->c(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v4

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    invoke-static {v5}, Lcom/google/googlenav/ui/android/GridFlowLayout$Params;->a(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)I

    move-result v5

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/android/GridFlowLayout;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setParams(Lcom/google/googlenav/ui/android/GridFlowLayout$Params;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/ui/android/GridFlowLayout;->b:Lcom/google/googlenav/ui/android/GridFlowLayout$Params;

    return-void
.end method
