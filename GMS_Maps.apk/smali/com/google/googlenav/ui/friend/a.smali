.class public Lcom/google/googlenav/ui/friend/a;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaB/p;


# static fields
.field private static final C:Lcom/google/common/collect/ax;

.field private static final D:[Ljava/lang/CharSequence;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Z

.field private final a:Lcom/google/googlenav/ui/friend/p;

.field private final b:Lcom/google/googlenav/friend/aI;

.field private final c:Lcom/google/googlenav/ui/aa;

.field private final d:Lcom/google/googlenav/ui/friend/n;

.field private l:Lcom/google/googlenav/ui/view/android/DistanceView;

.field private m:Lcom/google/googlenav/ui/view/android/HeadingView;

.field private n:Landroid/view/View;

.field private o:Z

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/QuickContactBadge;

.field private x:Lan/f;

.field private y:Lcom/google/googlenav/ui/friend/E;

.field private z:Lcom/google/googlenav/ui/friend/o;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x134

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x135

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x136

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/friend/a;->C:Lcom/google/common/collect/ax;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/16 v1, 0x1ac

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x19f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/16 v1, 0x1a9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/googlenav/ui/friend/a;->D:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/friend/aI;Lcom/google/googlenav/ui/friend/o;Lcom/google/googlenav/ui/friend/p;Lcom/google/googlenav/ui/friend/n;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "FriendProfileDialog2 must be used only for profiles of friends, not the user"

    invoke-static {v0, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    iput-object p2, p0, Lcom/google/googlenav/ui/friend/a;->z:Lcom/google/googlenav/ui/friend/o;

    invoke-interface {p2}, Lcom/google/googlenav/ui/friend/o;->b()Lcom/google/googlenav/ui/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->c:Lcom/google/googlenav/ui/aa;

    iput-object p3, p0, Lcom/google/googlenav/ui/friend/a;->a:Lcom/google/googlenav/ui/friend/p;

    iput-object p4, p0, Lcom/google/googlenav/ui/friend/a;->d:Lcom/google/googlenav/ui/friend/n;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/friend/a;)Lcom/google/googlenav/ui/friend/p;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->a:Lcom/google/googlenav/ui/friend/p;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f1001bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->w:Landroid/widget/QuickContactBadge;

    invoke-static {v1}, Lcom/google/common/base/aa;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->w:Landroid/widget/QuickContactBadge;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    const v0, 0x7f1001be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/friend/g;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/g;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v0, Lcom/google/googlenav/ui/friend/E;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->n()Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/friend/E;-><init>(Ljava/lang/Long;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->y:Lcom/google/googlenav/ui/friend/E;

    const v0, 0x7f100092

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->u:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1001bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->v:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10019b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->t:Landroid/widget/TextView;

    const v0, 0x7f1001c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->A:Landroid/widget/TextView;

    const v0, 0x7f1001c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->n:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->l:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->l:Lcom/google/googlenav/ui/view/android/DistanceView;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/DistanceView;->setVisibility(I)V

    const v0, 0x7f10019d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->m:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->m:Lcom/google/googlenav/ui/view/android/HeadingView;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/HeadingView;->setVisibility(I)V

    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/CharSequence;ILandroid/view/View$OnClickListener;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100414

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f100022

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz p6, :cond_0

    invoke-virtual {v1, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/friend/a;->k(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/friend/a;)Landroid/widget/QuickContactBadge;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->w:Landroid/widget/QuickContactBadge;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/friend/a;->d(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/friend/a;->e(Landroid/view/View;)V

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/friend/a;->c(Landroid/view/View;)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/friend/a;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/friend/a;->o:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/googlenav/ui/friend/a;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/googlenav/ui/friend/a;->o:Z

    goto :goto_0
.end method

.method private c(Landroid/view/View;)V
    .locals 7

    const v2, 0x7f1001c5

    const/16 v0, 0x59c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200fd

    new-instance v5, Lcom/google/googlenav/ui/friend/h;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/friend/h;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->d:Lcom/google/googlenav/ui/friend/n;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/friend/n;->c(Lcom/google/googlenav/friend/aI;)Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/friend/a;->a(Landroid/view/View;ILjava/lang/CharSequence;ILandroid/view/View$OnClickListener;Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/friend/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->w()V

    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 7

    const v2, 0x7f1001c3

    const/16 v0, 0x50b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0203e7

    new-instance v5, Lcom/google/googlenav/ui/friend/i;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/friend/i;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->d:Lcom/google/googlenav/ui/friend/n;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/friend/n;->a(Lcom/google/googlenav/friend/aI;)Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/friend/a;->a(Landroid/view/View;ILjava/lang/CharSequence;ILandroid/view/View$OnClickListener;Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/friend/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->o()V

    return-void
.end method

.method private e(Landroid/view/View;)V
    .locals 7

    const v2, 0x7f1001c4

    const/16 v0, 0xf2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0203e4

    new-instance v5, Lcom/google/googlenav/ui/friend/j;

    invoke-direct {v5, p0}, Lcom/google/googlenav/ui/friend/j;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->d:Lcom/google/googlenav/ui/friend/n;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/friend/n;->b(Lcom/google/googlenav/friend/aI;)Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/friend/a;->a(Landroid/view/View;ILjava/lang/CharSequence;ILandroid/view/View$OnClickListener;Z)V

    return-void
.end method

.method private f(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f1001c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    const v1, 0x7f1001cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/friend/k;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/k;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private g(Landroid/view/View;)V
    .locals 5

    const v4, 0x7f1001cb

    const/4 v3, 0x0

    const v0, 0x7f1001c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x171

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    const v1, 0x7f100023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->u()Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v1, 0x172

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->k(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x16b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/googlenav/ui/friend/l;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/friend/l;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private h(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f1001c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0203a3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x147

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const v1, 0x7f100023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x148

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const v1, 0x7f1001cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/aI;->I()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->r:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/google/googlenav/ui/friend/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/m;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private i(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f1001c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f10001e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x18b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10001a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f0203a3

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f100023

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->s:Landroid/widget/TextView;

    const v0, 0x7f1001cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/friend/c;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/c;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private j(Landroid/view/View;)V
    .locals 3

    const v0, 0x7f1001ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100023

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f10001e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x1ab

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f10001a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f020371

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f1001cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/friend/d;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/d;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic k()Lcom/google/common/collect/ax;
    .locals 1

    sget-object v0, Lcom/google/googlenav/ui/friend/a;->C:Lcom/google/common/collect/ax;

    return-object v0
.end method

.method private k(Landroid/view/View;)V
    .locals 3

    const v2, 0x3ecccccd

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->z:Lcom/google/googlenav/ui/friend/o;

    invoke-interface {v1}, Lcom/google/googlenav/ui/friend/o;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2, v3, v3}, Lcom/google/googlenav/friend/M;->a(Lcom/google/googlenav/friend/aI;JZZ)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->A:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->v()V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private n()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/a;->Q_()V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-static {v1, v2, v2, v2, v3}, Lcom/google/googlenav/friend/M;->a(Lcom/google/googlenav/friend/aI;ZZZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/friend/a;->b(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->l:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->m:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v2, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v2}, Lcom/google/googlenav/friend/aI;->E()LaN/B;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/friend/a;->b(Z)V

    goto :goto_0
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1a9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x19f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x1ac

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private v()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v0

    const-string v1, "Valid checkin must exist"

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0, v7, v6}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->D()Lcom/google/googlenav/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/a;->b()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const/16 v0, 0xb1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v2, v3, v6, v7}, Lcom/google/googlenav/b;->a(JZZ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    const v3, 0x7f10001e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->p:Landroid/view/View;

    const v1, 0x7f100023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private w()V
    .locals 4

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/a;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x1a8

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget-object v2, Lcom/google/googlenav/ui/friend/a;->D:[Ljava/lang/CharSequence;

    new-instance v3, Lcom/google/googlenav/ui/friend/e;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/friend/e;-><init>(Lcom/google/googlenav/ui/friend/a;I)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->b:Lcom/google/googlenav/friend/aI;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aI;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected K_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->a:Lcom/google/googlenav/ui/friend/p;

    invoke-interface {v0}, Lcom/google/googlenav/ui/friend/p;->i()V

    const/4 v0, 0x1

    return v0
.end method

.method public Q_()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->c:Lcom/google/googlenav/ui/aa;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->y:Lcom/google/googlenav/ui/friend/E;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->x:Lan/f;

    if-eq v1, v0, :cond_0

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/a;->x:Lan/f;

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->w:Landroid/widget/QuickContactBadge;

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/a;->x:Lan/f;

    invoke-virtual {v1}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected c()Landroid/view/View;
    .locals 4

    const/4 v0, 0x1

    const v1, 0x7f020219

    new-instance v2, Lcom/google/googlenav/ui/friend/b;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/friend/b;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/friend/a;->a(ZILaA/f;[I)V

    const/16 v0, 0x215

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020288

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/friend/a;->a(Ljava/lang/CharSequence;II)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/a;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040080

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->a(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->b(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->h(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->f(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->g(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->i(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/friend/a;->j(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/a;->h()V

    return-object v0

    :array_0
    .array-data 4
        0xbbe
        0xbbd
    .end array-data
.end method

.method protected d()V
    .locals 2

    sget-object v0, Lcom/google/googlenav/ui/friend/a;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/friend/a;->B:Z

    const v1, 0x7f1000fc

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/friend/f;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/friend/f;-><init>(Lcom/google/googlenav/ui/friend/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->n()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->l()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->m()V

    invoke-direct {p0}, Lcom/google/googlenav/ui/friend/a;->o()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/googlenav/ui/friend/a;->B:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/friend/a;->dismiss()V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->a:Lcom/google/googlenav/ui/friend/p;

    invoke-interface {v0}, Lcom/google/googlenav/ui/friend/p;->k()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->onBackPressed()V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/a;->a:Lcom/google/googlenav/ui/friend/p;

    invoke-interface {v0}, Lcom/google/googlenav/ui/friend/p;->h()V

    goto :goto_0
.end method
