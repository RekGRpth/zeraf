.class public Lcom/google/googlenav/ui/friend/CircleListItem;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040023

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f100033

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/friend/CircleListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    const v0, 0x7f1000b9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/friend/CircleListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->b:Landroid/widget/TextView;

    const v0, 0x7f1000b8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/friend/CircleListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const v0, 0x7f1000b7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/friend/CircleListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->d:Landroid/widget/CheckBox;

    return-void
.end method

.method private a(LaB/s;Lcom/google/googlenav/friend/aD;)V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/googlenav/friend/aF;

    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/aF;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1, v0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;)Lam/f;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    const v1, 0x7f020358

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/friend/aD;)V
    .locals 6

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->e()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v1, 0x3ce

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, -0x777778

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    iget-object v1, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/friend/aD;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/aD;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setCheckBoxVisible(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->d:Landroid/widget/CheckBox;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public setPostTarget(LaB/s;Lcom/google/googlenav/friend/aD;)V
    .locals 2

    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/friend/CircleListItem;->a(Lcom/google/googlenav/friend/aD;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/google/googlenav/friend/aD;->d()Lcom/google/googlenav/friend/aG;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/friend/CircleListItem;->a(LaB/s;Lcom/google/googlenav/friend/aD;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/friend/CircleListItem;->b(Lcom/google/googlenav/friend/aD;)V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/ui/friend/CircleListItem;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    return-void
.end method
