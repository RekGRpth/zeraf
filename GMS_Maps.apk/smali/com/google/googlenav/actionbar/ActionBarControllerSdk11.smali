.class public Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;
.super Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private h:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->h:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v1, p4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/ActionBar;->setIcon(I)V

    :cond_0
    invoke-virtual {v1, p2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    move p4, v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->a:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->h:Landroid/app/Dialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1, p3}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->a(Lcom/google/android/maps/MapsActivity;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p2}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    new-instance v1, Lcom/google/googlenav/actionbar/c;

    invoke-direct {v1, p0}, Lcom/google/googlenav/actionbar/c;-><init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;)V

    invoke-virtual {p0, v0, v4, v1}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    iget-object v1, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->f:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    return v2
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public n()Landroid/content/Context;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk11;->h:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
