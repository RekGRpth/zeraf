.class Lcom/google/googlenav/actionbar/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/MenuItem$OnActionExpandListener;


# instance fields
.field final synthetic a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;


# direct methods
.method constructor <init>(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    iget-object v0, v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    iget-object v0, v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    invoke-static {v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->a(Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;)Lcom/google/googlenav/actionbar/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/actionbar/b;->b()V

    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->o()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    iget-object v0, v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/actionbar/d;->a:Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;

    iget-object v0, v0, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->f:Landroid/app/ActionBar;

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
