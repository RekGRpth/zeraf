.class abstract Lcom/google/googlenav/cn;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field protected b:I

.field private c:Lcom/google/googlenav/bZ;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/bZ;I)V
    .locals 1

    iget-object v0, p1, Lcom/google/googlenav/bZ;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/google/googlenav/cn;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/google/googlenav/cn;->c:Lcom/google/googlenav/bZ;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Law/a;-><init>()V

    iput-object p1, p0, Lcom/google/googlenav/cn;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/googlenav/cn;->b:I

    return-void
.end method


# virtual methods
.method protected a(Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hF;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/cn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/googlenav/cn;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bh;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_0
    return-object v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 2

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/cn;->c:Lcom/google/googlenav/bZ;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/googlenav/cn;->c:Lcom/google/googlenav/bZ;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    new-instance v1, Lcom/google/googlenav/bZ;

    invoke-direct {v1, v0}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, Lcom/google/googlenav/cn;->c:Lcom/google/googlenav/bZ;

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x70

    return v0
.end method

.method public i()Lcom/google/googlenav/bZ;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/cn;->c:Lcom/google/googlenav/bZ;

    return-object v0
.end method

.method protected k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/cn;->a(Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method
