.class public Lcom/google/googlenav/aV;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/ab;


# static fields
.field private static a:Lcom/google/googlenav/aV;


# instance fields
.field private b:Ljava/util/concurrent/ConcurrentMap;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private volatile d:Ljava/util/concurrent/Future;

.field private volatile e:Ljava/util/concurrent/Future;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, LY/e;->a()LY/e;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, LY/e;->a(J)LY/e;

    move-result-object v0

    invoke-virtual {v0}, LY/e;->o()LY/d;

    move-result-object v0

    invoke-interface {v0}, LY/d;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    iput-object p1, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Lcom/google/googlenav/aV;->e()V

    return-void
.end method

.method public static a()Lcom/google/googlenav/aV;
    .locals 2

    sget-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/aV;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/aV;-><init>(Ljava/util/concurrent/ExecutorService;)V

    sput-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    :cond_0
    sget-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/aV;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/aV;->g()V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/aW;

    invoke-direct {v1, p0}, Lcom/google/googlenav/aW;-><init>(Lcom/google/googlenav/aV;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/aX;

    invoke-direct {v1, p0}, Lcom/google/googlenav/aX;-><init>(Lcom/google/googlenav/aV;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->d:Ljava/util/concurrent/Future;

    return-void
.end method

.method private declared-synchronized g()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v1, Laq/b;

    invoke-direct {v1}, Laq/b;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Laq/b;->writeInt(I)V

    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Laq/b;->writeUTF(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v2, "PLACES_REVIEW_PROMPT_BLACKLIST_SETTING"

    invoke-virtual {v1}, Laq/b;->a()[B

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-virtual {v1}, Laq/b;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public F_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/googlenav/aV;->c()V

    return-void
.end method

.method public a(LaR/P;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/googlenav/aV;->f()V

    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized b()V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PLACES_REVIEW_PROMPT_BLACKLIST_SETTING"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v1, Laq/a;

    invoke-direct {v1, v0}, Laq/a;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Laq/a;->readInt()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1}, LaR/u;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->h()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1}, LaR/u;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aV;->a(Ljava/util/Set;)V

    invoke-direct {p0}, Lcom/google/googlenav/aV;->f()V

    return-void
.end method
