.class Lcom/google/googlenav/wallpaper/b;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/googlenav/wallpaper/i;


# instance fields
.field final synthetic a:Lcom/google/googlenav/wallpaper/MapWallpaper;

.field private b:Lcom/google/googlenav/wallpaper/MapWallpaper;

.field private c:Landroid/view/SurfaceHolder;

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Lcom/google/googlenav/wallpaper/g;

.field private h:Lbn/d;

.field private i:Z

.field private j:Lcom/google/googlenav/wallpaper/c;

.field private k:Lcom/google/googlenav/wallpaper/e;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Lcom/google/googlenav/wallpaper/h;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/wallpaper/MapWallpaper;Lcom/google/googlenav/wallpaper/MapWallpaper;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    const-string v0, "hybrid"

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/googlenav/wallpaper/b;->l:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/googlenav/wallpaper/b;->m:Lcom/google/googlenav/wallpaper/h;

    iput-object p2, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/wallpaper/b;)Ljava/io/File;
    .locals 1

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->f()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/h;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/googlenav/wallpaper/b;->b(Lcom/google/googlenav/wallpaper/h;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(II)V
    .locals 6

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->g:Lcom/google/googlenav/wallpaper/g;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/googlenav/wallpaper/h;

    new-instance v2, Lcom/google/googlenav/wallpaper/d;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/googlenav/wallpaper/d;-><init>(Lcom/google/googlenav/wallpaper/b;II)V

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->c()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/wallpaper/b;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->g:Lcom/google/googlenav/wallpaper/g;

    invoke-virtual {v1}, Lcom/google/googlenav/wallpaper/g;->a()Lcom/google/googlenav/wallpaper/f;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/wallpaper/h;-><init>(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/d;ILjava/lang/String;Lcom/google/googlenav/wallpaper/f;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/wallpaper/b;->a(Lcom/google/googlenav/wallpaper/h;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/SharedPreferences;)V
    .locals 2

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    if-eqz v0, :cond_0

    const-string v0, "weather_type"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    invoke-static {v0}, Lbn/c;->valueOf(Ljava/lang/String;)Lbn/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbn/d;->a(Lbn/c;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 6

    const/4 v1, 0x0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/high16 v0, 0x64000000

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/wallpaper/h;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->c:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->m:Lcom/google/googlenav/wallpaper/h;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/wallpaper/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->l:Landroid/graphics/drawable/Drawable;

    :goto_0
    if-eqz v0, :cond_3

    iget v1, p0, Lcom/google/googlenav/wallpaper/b;->d:I

    iget-object v2, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v2, v2, Lcom/google/googlenav/wallpaper/d;->a:I

    add-int/2addr v2, v1

    iget-object v3, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v3, v3, Lcom/google/googlenav/wallpaper/d;->b:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->c:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/googlenav/wallpaper/b;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    iget-object v2, p0, Lcom/google/googlenav/wallpaper/b;->c:Landroid/view/SurfaceHolder;

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v3, v3, Lcom/google/googlenav/wallpaper/d;->a:I

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->isPreview()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lbn/d;->a(Landroid/graphics/Canvas;IIZ)V

    :cond_0
    iget-object v0, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v0, v0, Lcom/google/googlenav/wallpaper/d;->a:I

    iget-object v2, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v2, v2, Lcom/google/googlenav/wallpaper/d;->b:I

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/wallpaper/b;->a(Landroid/graphics/Canvas;II)V

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->c:Landroid/view/SurfaceHolder;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/wallpaper/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/b;->m:Lcom/google/googlenav/wallpaper/h;

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->k:Lcom/google/googlenav/wallpaper/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->k:Lcom/google/googlenav/wallpaper/e;

    invoke-virtual {v0}, Lcom/google/googlenav/wallpaper/e;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    :cond_4
    new-instance v0, Lcom/google/googlenav/wallpaper/e;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/wallpaper/e;-><init>(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/b;)V

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->k:Lcom/google/googlenav/wallpaper/e;

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->k:Lcom/google/googlenav/wallpaper/e;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/wallpaper/h;

    aput-object p1, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/wallpaper/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method private b(Lcom/google/googlenav/wallpaper/h;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://maps.googleapis.com/maps/api/staticmap?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&sensor=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&scale=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&language="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/wallpaper/b;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->d:Lcom/google/googlenav/wallpaper/f;

    iget-wide v1, v1, Lcom/google/googlenav/wallpaper/f;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->d:Lcom/google/googlenav/wallpaper/f;

    iget-wide v1, v1, Lcom/google/googlenav/wallpaper/f;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->c:Ljava/lang/String;

    const-string v2, "terrain"

    if-ne v1, v2, :cond_0

    iget v1, p1, Lcom/google/googlenav/wallpaper/h;->b:I

    const/16 v2, 0xe

    if-le v1, v2, :cond_0

    const-string v1, "&zoom=14"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string v1, "&size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v1, v1, Lcom/google/googlenav/wallpaper/d;->c:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->a:Lcom/google/googlenav/wallpaper/d;

    iget v1, v1, Lcom/google/googlenav/wallpaper/d;->d:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "&maptype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/google/googlenav/wallpaper/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, "&zoom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/google/googlenav/wallpaper/h;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private b()V
    .locals 7

    const/16 v0, 0x12

    const/4 v1, 0x4

    const/4 v6, 0x0

    const/4 v5, -0x1

    iget-object v2, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v3, "wallpaper_settings"

    invoke-virtual {v2, v3, v6}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "zoom_level_string"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v3, "wallpaper_settings"

    const v4, 0x8000

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "zoom_level"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v5, :cond_0

    add-int/lit8 v2, v2, -0x1

    if-ge v2, v1, :cond_2

    :goto_0
    if-le v1, v0, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v2, "wallpaper_settings"

    invoke-virtual {v1, v2, v6}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "zoom_level_string"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lal/b;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/googlenav/wallpaper/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/googlenav/wallpaper/b;->a(Lcom/google/googlenav/wallpaper/h;)V

    return-void
.end method

.method private c()I
    .locals 5

    const/4 v4, 0x0

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v2, "wallpaper_settings"

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "zoom_level_string"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->a:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v2, "wallpaper_settings"

    invoke-virtual {v1, v2, v4}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "zoom_level_string"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lal/b;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/googlenav/wallpaper/b;)Lcom/google/googlenav/wallpaper/c;
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->j:Lcom/google/googlenav/wallpaper/c;

    return-object v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v1, "wallpaper_settings"

    const v2, 0x8000

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->b()V

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/wallpaper/b;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/wallpaper/b;->a(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/wallpaper/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->g()V

    return-void
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->getDesiredMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->getDesiredMinimumHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/wallpaper/b;->a(II)V

    return-void
.end method

.method private f()Ljava/io/File;
    .locals 3

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v1, "wallpaper"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 3

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->f()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    new-instance v0, Lcom/google/googlenav/wallpaper/c;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/wallpaper/c;-><init>(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/a;)V

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->j:Lcom/google/googlenav/wallpaper/c;

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    invoke-static {v0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    invoke-static {v0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    new-instance v0, Lbn/d;

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->getDesiredMinimumWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->getDesiredMinimumHeight()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, p0}, Lbn/d;-><init>(Landroid/content/Context;IILcom/google/googlenav/wallpaper/i;)V

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v1, Lcom/google/googlenav/wallpaper/g;

    invoke-direct {v1, p0, v4}, Lcom/google/googlenav/wallpaper/g;-><init>(Lcom/google/googlenav/wallpaper/b;Lcom/google/googlenav/wallpaper/a;)V

    iput-object v1, p0, Lcom/google/googlenav/wallpaper/b;->g:Lcom/google/googlenav/wallpaper/g;

    const-string v1, "network"

    const-wide/32 v2, 0x927c0

    const v4, 0x453b8000

    iget-object v5, p0, Lcom/google/googlenav/wallpaper/b;->g:Lcom/google/googlenav/wallpaper/g;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    invoke-virtual {p0}, Lcom/google/googlenav/wallpaper/b;->isPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4c

    const-string v1, "a"

    const-string v2, "p"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->d()V

    return-void
.end method

.method public onDesiredSizeChanged(II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/wallpaper/b;->a(II)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->b:Lcom/google/googlenav/wallpaper/MapWallpaper;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/wallpaper/MapWallpaper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/googlenav/wallpaper/b;->g:Lcom/google/googlenav/wallpaper/g;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    invoke-static {}, Lcom/google/googlenav/android/c;->f()V

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 1

    iput p5, p0, Lcom/google/googlenav/wallpaper/b;->d:I

    iput p6, p0, Lcom/google/googlenav/wallpaper/b;->e:I

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    invoke-virtual {v0, p1}, Lbn/d;->a(F)V

    :cond_0
    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/wallpaper/b;->i:Z

    const-string v0, "weather_on"

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "weather_on"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/wallpaper/b;->i:Z

    :cond_0
    const-string v0, "map_mode"

    const-string v1, "map_mode_satellite"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "hybrid"

    iput-object v1, p0, Lcom/google/googlenav/wallpaper/b;->f:Ljava/lang/String;

    const-string v1, "map_mode_terrain"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "terrain"

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->f:Ljava/lang/String;

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void

    :cond_2
    const-string v1, "map_mode_normal"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "roadmap"

    iput-object v0, p0, Lcom/google/googlenav/wallpaper/b;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/b;->c:Landroid/view/SurfaceHolder;

    invoke-direct {p0}, Lcom/google/googlenav/wallpaper/b;->e()V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/googlenav/wallpaper/b;->h:Lbn/d;

    invoke-virtual {v0, p1}, Lbn/d;->a(Z)V

    :cond_0
    return-void
.end method
