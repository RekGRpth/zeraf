.class Lcom/google/googlenav/wallpaper/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field final synthetic e:Lcom/google/googlenav/wallpaper/b;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/wallpaper/b;II)V
    .locals 1

    iput-object p1, p0, Lcom/google/googlenav/wallpaper/d;->e:Lcom/google/googlenav/wallpaper/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    iput p3, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    const/16 v0, 0x500

    invoke-direct {p0, v0}, Lcom/google/googlenav/wallpaper/d;->a(I)V

    return-void
.end method

.method private a(I)V
    .locals 2

    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    if-gt v0, p1, :cond_0

    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    if-gt v0, p1, :cond_0

    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    iput v0, p0, Lcom/google/googlenav/wallpaper/d;->c:I

    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    iput v0, p0, Lcom/google/googlenav/wallpaper/d;->d:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/wallpaper/d;->d:I

    iput p1, p0, Lcom/google/googlenav/wallpaper/d;->c:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/google/googlenav/wallpaper/d;->c:I

    iput p1, p0, Lcom/google/googlenav/wallpaper/d;->d:I

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/google/googlenav/wallpaper/d;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/googlenav/wallpaper/d;

    iget v2, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    iget v3, p1, Lcom/google/googlenav/wallpaper/d;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    iget v3, p1, Lcom/google/googlenav/wallpaper/d;->b:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[desktopWidth:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", desktopHeight:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",[imageWidth:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageHeight:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/wallpaper/d;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
