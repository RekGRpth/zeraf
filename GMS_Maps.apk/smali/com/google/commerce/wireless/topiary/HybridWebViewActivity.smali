.class public Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "SourceFile"


# static fields
.field private static c:I


# instance fields
.field protected a:Lcom/google/commerce/wireless/topiary/C;

.field protected b:Landroid/widget/FrameLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x64

    sput v0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/k;->a()Lvedroid/support/v4/app/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->a:Lcom/google/commerce/wireless/topiary/C;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/t;->a(Lvedroid/support/v4/app/f;)Lvedroid/support/v4/app/t;

    invoke-virtual {v0}, Lvedroid/support/v4/app/t;->a()I

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->b:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->b:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->b:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/commerce/wireless/topiary/C;

    invoke-direct {v0}, Lcom/google/commerce/wireless/topiary/C;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->a:Lcom/google/commerce/wireless/topiary/C;

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/k;->a()Lvedroid/support/v4/app/t;

    move-result-object v0

    sget v1, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->c:I

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->a:Lcom/google/commerce/wireless/topiary/C;

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/t;->a(ILvedroid/support/v4/app/f;)Lvedroid/support/v4/app/t;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/t;->a()I

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->b:Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebViewActivity;->a:Lcom/google/commerce/wireless/topiary/C;

    return-void
.end method
