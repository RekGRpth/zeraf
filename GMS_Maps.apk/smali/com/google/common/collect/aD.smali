.class public Lcom/google/common/collect/aD;
.super Lcom/google/common/collect/at;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/common/collect/at;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/aD;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/ImmutableSet;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aD;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lcom/google/common/collect/at;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aD;->b(Ljava/lang/Object;)Lcom/google/common/collect/aD;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)Lcom/google/common/collect/at;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aD;->b([Ljava/lang/Object;)Lcom/google/common/collect/aD;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lcom/google/common/collect/aD;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/aD;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public varargs b([Ljava/lang/Object;)Lcom/google/common/collect/aD;
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/aD;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/common/collect/aD;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    array-length v2, p1

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-super {p0, p1}, Lcom/google/common/collect/at;->a([Ljava/lang/Object;)Lcom/google/common/collect/at;

    return-object p0
.end method
