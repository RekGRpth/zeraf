.class public final Lcom/google/common/collect/cY;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/common/collect/dh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/common/collect/cZ;

    invoke-direct {v0}, Lcom/google/common/collect/cZ;-><init>()V

    sput-object v0, Lcom/google/common/collect/cY;->a:Lcom/google/common/collect/dh;

    return-void
.end method

.method static a(Ljava/lang/Iterable;)Lcom/google/common/collect/cW;
    .locals 0

    check-cast p0, Lcom/google/common/collect/cW;

    return-object p0
.end method

.method static a(Lcom/google/common/collect/cW;)Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lcom/google/common/collect/de;

    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/de;-><init>(Lcom/google/common/collect/cW;Ljava/util/Iterator;)V

    return-object v0
.end method

.method static a(Lcom/google/common/collect/cW;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p0}, Lcom/google/common/collect/cW;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/common/collect/cW;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    invoke-interface {v0}, Lcom/google/common/collect/cX;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    if-eq v4, v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method static a(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .locals 3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/common/collect/cY;->a(Ljava/lang/Iterable;)Lcom/google/common/collect/cW;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    invoke-interface {v0}, Lcom/google/common/collect/cX;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lcom/google/common/collect/cW;->a(Ljava/lang/Object;I)I

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/common/collect/aZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Lcom/google/common/collect/cW;)I
    .locals 6

    const-wide/16 v0, 0x0

    invoke-interface {p0}, Lcom/google/common/collect/cW;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cX;

    invoke-interface {v0}, Lcom/google/common/collect/cX;->b()I

    move-result v0

    int-to-long v4, v0

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_0
    invoke-static {v1, v2}, Lac/a;->a(J)I

    move-result v0

    return v0
.end method

.method static b(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .locals 1

    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p1}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object p1

    :cond_0
    invoke-interface {p0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lcom/google/common/collect/cW;Ljava/util/Collection;)Z
    .locals 1

    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p1}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object p1

    :cond_0
    invoke-interface {p0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
