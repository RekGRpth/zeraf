.class public abstract Lcom/google/common/collect/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cV;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final transient b:Lcom/google/common/collect/ax;

.field final transient c:I


# direct methods
.method constructor <init>(Lcom/google/common/collect/ax;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    iput p2, p0, Lcom/google/common/collect/az;->c:I

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Lcom/google/common/collect/ar;
.end method

.method a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->d()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/az;->a(Ljava/lang/Object;)Lcom/google/common/collect/ar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/util/Map;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/az;->c()Lcom/google/common/collect/ax;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/common/collect/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget v0, p0, Lcom/google/common/collect/az;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/common/collect/cV;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/common/collect/cV;

    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-interface {p1}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ax;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
