.class Lcom/google/common/collect/cD;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cs;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:I

.field final c:Lcom/google/common/collect/cs;

.field volatile d:Lcom/google/common/collect/cJ;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/common/collect/cs;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bP;->g()Lcom/google/common/collect/cJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    iput-object p1, p0, Lcom/google/common/collect/cD;->a:Ljava/lang/Object;

    iput p2, p0, Lcom/google/common/collect/cD;->b:I

    iput-object p3, p0, Lcom/google/common/collect/cD;->c:Lcom/google/common/collect/cs;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cJ;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/common/collect/cJ;)V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    iput-object p1, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    invoke-interface {v0, p1}, Lcom/google/common/collect/cJ;->a(Lcom/google/common/collect/cJ;)V

    return-void
.end method

.method public a(Lcom/google/common/collect/cs;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/common/collect/cs;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cD;->c:Lcom/google/common/collect/cs;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/cs;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/common/collect/cD;->b:I

    return v0
.end method

.method public c(Lcom/google/common/collect/cs;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cD;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public d(Lcom/google/common/collect/cs;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/common/collect/cs;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/common/collect/cs;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/google/common/collect/cs;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/common/collect/cs;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
