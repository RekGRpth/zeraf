.class public Lcom/google/common/collect/aK;
.super Lcom/google/common/collect/aQ;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final c:Lcom/google/common/collect/aK;

.field private static final serialVersionUID:J


# instance fields
.field final transient a:Lcom/google/common/collect/ImmutableList;

.field private final transient d:Ljava/util/Comparator;

.field private transient e:Lcom/google/common/collect/ImmutableSet;

.field private transient f:Lcom/google/common/collect/aR;

.field private transient g:Lcom/google/common/collect/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    new-instance v0, Lcom/google/common/collect/aK;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    sget-object v2, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/common/collect/aK;->c:Lcom/google/common/collect/aK;

    return-void
.end method

.method constructor <init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/aQ;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    iput-object p2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    return-void
.end method

.method private a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I
    .locals 3

    invoke-direct {p0}, Lcom/google/common/collect/aK;->l()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->e()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/google/common/collect/dJ;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    return v0
.end method

.method private a(II)Lcom/google/common/collect/aK;
    .locals 3

    if-ge p1, p2, :cond_0

    new-instance v0, Lcom/google/common/collect/aK;

    iget-object v1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, p1, p2}, Lcom/google/common/collect/ImmutableList;->a(II)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/google/common/collect/aK;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aK;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Comparator;)Lcom/google/common/collect/aK;
    .locals 2

    sget-object v0, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/common/collect/aK;->c:Lcom/google/common/collect/aK;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/common/collect/aK;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private j()Lcom/google/common/collect/ImmutableSet;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/common/collect/aO;

    invoke-direct {v0, p0}, Lcom/google/common/collect/aO;-><init>(Lcom/google/common/collect/aK;)V

    goto :goto_0
.end method

.method private k()Lcom/google/common/collect/aR;
    .locals 3

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/common/collect/dw;

    new-instance v1, Lcom/google/common/collect/aL;

    iget-object v2, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-direct {v1, p0, v2}, Lcom/google/common/collect/aL;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/ImmutableList;)V

    iget-object v2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/dw;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private l()Lcom/google/common/collect/ImmutableList;
    .locals 2

    new-instance v0, Lcom/google/common/collect/aN;

    iget-object v1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/aN;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/common/collect/ImmutableSet;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->e:Lcom/google/common/collect/ImmutableSet;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/common/collect/aK;->j()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/aK;->e:Lcom/google/common/collect/ImmutableSet;

    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/common/collect/aK;->a(II)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    goto :goto_0
.end method

.method a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aK;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/common/collect/aK;->a(II)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public c()Lcom/google/common/collect/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->g:Lcom/google/common/collect/ar;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/aP;

    invoke-direct {v0, p0}, Lcom/google/common/collect/aP;-><init>(Lcom/google/common/collect/aK;)V

    iput-object v0, p0, Lcom/google/common/collect/aK;->g:Lcom/google/common/collect/ar;

    :cond_0
    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->g()Lcom/google/common/collect/dY;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->a()Z

    move-result v0

    return v0
.end method

.method e()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/common/collect/aR;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->f:Lcom/google/common/collect/aR;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/common/collect/aK;->k()Lcom/google/common/collect/aR;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/aK;->f:Lcom/google/common/collect/aR;

    :cond_0
    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method g()Lcom/google/common/collect/dY;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/aM;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/aM;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/dY;)V

    return-object v1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    sget-object v1, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v2, Lcom/google/common/collect/dL;->c:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->f()Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->c()Lcom/google/common/collect/ar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x_()Lcom/google/common/collect/ImmutableSet;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->f()Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method
