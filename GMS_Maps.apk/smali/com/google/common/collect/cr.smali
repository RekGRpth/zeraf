.class final enum Lcom/google/common/collect/cr;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cs;


# static fields
.field public static final enum a:Lcom/google/common/collect/cr;

.field private static final synthetic b:[Lcom/google/common/collect/cr;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/common/collect/cr;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/cr;->a:Lcom/google/common/collect/cr;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/common/collect/cr;

    sget-object v1, Lcom/google/common/collect/cr;->a:Lcom/google/common/collect/cr;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/common/collect/cr;->b:[Lcom/google/common/collect/cr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/cr;
    .locals 1

    const-class v0, Lcom/google/common/collect/cr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cr;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/cr;
    .locals 1

    sget-object v0, Lcom/google/common/collect/cr;->b:[Lcom/google/common/collect/cr;

    invoke-virtual {v0}, [Lcom/google/common/collect/cr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/cr;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cJ;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/common/collect/cJ;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/common/collect/cs;)V
    .locals 0

    return-void
.end method

.method public b()Lcom/google/common/collect/cs;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/common/collect/cs;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lcom/google/common/collect/cs;)V
    .locals 0

    return-void
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d(Lcom/google/common/collect/cs;)V
    .locals 0

    return-void
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()Lcom/google/common/collect/cs;
    .locals 0

    return-object p0
.end method

.method public g()Lcom/google/common/collect/cs;
    .locals 0

    return-object p0
.end method

.method public h()Lcom/google/common/collect/cs;
    .locals 0

    return-object p0
.end method

.method public i()Lcom/google/common/collect/cs;
    .locals 0

    return-object p0
.end method
