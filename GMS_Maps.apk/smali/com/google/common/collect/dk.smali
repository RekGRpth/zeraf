.class Lcom/google/common/collect/dk;
.super Lcom/google/common/collect/ap;
.source "SourceFile"


# instance fields
.field final transient a:Lcom/google/common/collect/ax;

.field final transient b:Lcom/google/common/collect/ap;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ax;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/common/collect/ap;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/dk;->a:Lcom/google/common/collect/ax;

    invoke-static {}, Lcom/google/common/collect/ax;->i()Lcom/google/common/collect/ay;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/common/collect/ax;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/dk;

    invoke-direct {v1, v0, p0}, Lcom/google/common/collect/dk;-><init>(Lcom/google/common/collect/ax;Lcom/google/common/collect/ap;)V

    iput-object v1, p0, Lcom/google/common/collect/dk;->b:Lcom/google/common/collect/ap;

    return-void
.end method

.method constructor <init>(Lcom/google/common/collect/ax;Lcom/google/common/collect/ap;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/ap;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/dk;->a:Lcom/google/common/collect/ax;

    iput-object p2, p0, Lcom/google/common/collect/dk;->b:Lcom/google/common/collect/ap;

    return-void
.end method


# virtual methods
.method public synthetic b()Lcom/google/common/collect/Q;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/dk;->f()Lcom/google/common/collect/ap;

    move-result-object v0

    return-object v0
.end method

.method d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dk;->a:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/common/collect/dk;->b:Lcom/google/common/collect/ap;

    invoke-virtual {v0}, Lcom/google/common/collect/ap;->e()Lcom/google/common/collect/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Lcom/google/common/collect/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dk;->a:Lcom/google/common/collect/ax;

    return-object v0
.end method

.method public f()Lcom/google/common/collect/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dk;->b:Lcom/google/common/collect/ap;

    return-object v0
.end method
