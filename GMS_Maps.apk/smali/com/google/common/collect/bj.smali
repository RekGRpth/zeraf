.class public Lcom/google/common/collect/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/bw;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private transient a:Lcom/google/common/collect/bt;

.field private transient b:Lcom/google/common/collect/bt;

.field private transient c:Lcom/google/common/collect/cW;

.field private transient d:Ljava/util/Map;

.field private transient e:Ljava/util/Map;

.field private transient f:Ljava/util/Set;

.field private transient g:Ljava/util/List;

.field private transient h:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/bi;->g()Lcom/google/common/collect/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    return-void
.end method

.method public static a()Lcom/google/common/collect/bj;
    .locals 1

    new-instance v0, Lcom/google/common/collect/bj;

    invoke-direct {v0}, Lcom/google/common/collect/bj;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;
    .locals 2

    new-instance v1, Lcom/google/common/collect/bt;

    invoke-direct {v1, p1, p2}, Lcom/google/common/collect/bt;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v1, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    invoke-interface {v0, p1}, Lcom/google/common/collect/cW;->add(Ljava/lang/Object;)Z

    return-object v1

    :cond_0
    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bt;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    goto :goto_0

    :cond_1
    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    goto :goto_1

    :cond_2
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object p3, v1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object p3, v1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    if-nez v0, :cond_4

    iput-object v1, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    :goto_3
    iput-object v1, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v1, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    goto :goto_0

    :cond_3
    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    goto :goto_2

    :cond_4
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Lcom/google/common/collect/bt;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bt;)V

    return-void
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->f(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/google/common/collect/bt;)V
    .locals 3

    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    :goto_0
    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    :goto_1
    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    :goto_2
    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    :goto_3
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/common/collect/cW;->remove(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method

.method static synthetic b(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method static synthetic c(Lcom/google/common/collect/bj;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    return-object v0
.end method

.method static synthetic e(Lcom/google/common/collect/bj;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)V
    .locals 0

    invoke-static {p0}, Lcom/google/common/collect/bj;->g(Ljava/lang/Object;)V

    return-void
.end method

.method private f(Ljava/lang/Object;)V
    .locals 2

    new-instance v0, Lcom/google/common/collect/bv;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bv;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static g(Ljava/lang/Object;)V
    .locals 1

    if-nez p0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method private h(Ljava/lang/Object;)Ljava/util/List;
    .locals 1

    new-instance v0, Lcom/google/common/collect/bv;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bv;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bj;->d(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->h:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/bo;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bo;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->h:Ljava/util/Map;

    :cond_0
    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    invoke-interface {v0}, Lcom/google/common/collect/cW;->size()I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->h(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->f(Ljava/lang/Object;)V

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/List;
    .locals 1

    new-instance v0, Lcom/google/common/collect/bk;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bk;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->f:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/bl;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bl;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->f:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/common/collect/cV;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/common/collect/cV;

    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/bj;->g:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/bm;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bm;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->g:Ljava/util/List;

    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
