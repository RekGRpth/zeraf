.class public Lx/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;


# instance fields
.field private final b:[LD/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lx/o;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1f

    new-array v0, v0, [LD/b;

    iput-object v0, p0, Lx/o;->b:[LD/b;

    return-void
.end method

.method public static a(LD/a;I)LD/b;
    .locals 1

    sget-object v0, Lx/o;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/o;

    iget-object v0, v0, Lx/o;->b:[LD/b;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private a()V
    .locals 4

    iget-object v1, p0, Lx/o;->b:[LD/b;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, LD/b;->g()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(LD/a;)V
    .locals 1

    sget-object v0, Lx/o;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/o;

    if-eqz v0, :cond_0

    invoke-direct {v0}, Lx/o;->a()V

    sget-object v0, Lx/o;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private a(LD/a;Landroid/content/res/Resources;IIZZZZ)V
    .locals 2

    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    invoke-virtual {v0, p6}, LD/b;->a(Z)V

    invoke-virtual {v0, p7}, LD/b;->b(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    if-eqz p5, :cond_1

    if-eqz p8, :cond_0

    invoke-virtual {v0, p2, p4}, LD/b;->d(Landroid/content/res/Resources;I)V

    :goto_0
    iget-object v1, p0, Lx/o;->b:[LD/b;

    aput-object v0, v1, p3

    return-void

    :cond_0
    invoke-virtual {v0, p2, p4}, LD/b;->c(Landroid/content/res/Resources;I)V

    goto :goto_0

    :cond_1
    if-eqz p8, :cond_2

    invoke-virtual {v0, p2, p4}, LD/b;->b(Landroid/content/res/Resources;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p2, p4}, LD/b;->a(Landroid/content/res/Resources;I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;LD/a;)V
    .locals 2

    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    new-instance v0, Lx/o;

    invoke-direct {v0}, Lx/o;-><init>()V

    invoke-direct {v0, p0, p1}, Lx/o;->b(Landroid/content/res/Resources;LD/a;)V

    sget-object v1, Lx/o;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Landroid/content/res/Resources;LD/a;)V
    .locals 9

    const/4 v3, 0x0

    const v4, 0x7f020175

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x19

    const v4, 0x7f020182

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1a

    const v4, 0x7f020177

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1b

    const v4, 0x7f020178

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1c

    const v4, 0x7f020179

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1d

    const v4, 0x7f02017a

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x1e

    const v4, 0x7f020172

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x1

    const v4, 0x7f02017b

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x2

    const v4, 0x7f02017d

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x3

    const v4, 0x7f02017f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x4

    const v4, 0x7f020181

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x5

    const v4, 0x7f02017c

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x6

    const v4, 0x7f02017e

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x16

    const v4, 0x7f020180

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/4 v3, 0x7

    const v4, 0x7f020184

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x15

    const v4, 0x7f020183

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x8

    const v4, 0x7f020185

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x9

    const v4, 0x7f020186

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xa

    const v4, 0x7f020187

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x14

    const v4, 0x7f02016a

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x17

    const v4, 0x7f020173

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0x18

    const v4, 0x7f02016f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    const/16 v3, 0xb

    const v4, 0x7f020188

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lx/o;->a(LD/a;Landroid/content/res/Resources;IIZZZZ)V

    return-void
.end method
