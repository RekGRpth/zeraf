.class LY/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LY/Z;


# instance fields
.field volatile a:LY/Z;

.field final b:Lae/o;

.field final c:Lcom/google/common/base/Y;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    invoke-direct {p0, v0}, LY/I;-><init>(LY/Z;)V

    return-void
.end method

.method public constructor <init>(LY/Z;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lae/o;->b()Lae/o;

    move-result-object v0

    iput-object v0, p0, LY/I;->b:Lae/o;

    new-instance v0, Lcom/google/common/base/Y;

    invoke-direct {v0}, Lcom/google/common/base/Y;-><init>()V

    iput-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    iput-object p1, p0, LY/I;->a:LY/Z;

    return-void
.end method

.method private static a(Lae/o;Ljava/lang/Throwable;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1}, Lae/o;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Throwable;)Lae/i;
    .locals 1

    invoke-static {}, Lae/o;->b()Lae/o;

    move-result-object v0

    invoke-static {v0, p1}, LY/I;->a(Lae/o;Ljava/lang/Throwable;)Z

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->a()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;LY/L;)LY/Z;
    .locals 0

    return-object p0
.end method

.method public a(Ljava/lang/Object;LY/k;)Lae/i;
    .locals 2

    iget-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    invoke-virtual {v0}, Lcom/google/common/base/Y;->a()Lcom/google/common/base/Y;

    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-virtual {p2, p1}, LY/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LY/I;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, LY/I;->b:Lae/o;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Lae/g;->a(Ljava/lang/Object;)Lae/i;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p2, p1, v0}, LY/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Lae/i;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lae/g;->a(Ljava/lang/Object;)Lae/i;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, LY/I;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, LY/I;->b:Lae/o;

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, LY/I;->b(Ljava/lang/Throwable;)Lae/i;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, LY/I;->b(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    iput-object v0, p0, LY/I;->a:LY/Z;

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-static {v0, p1}, LY/I;->a(Lae/o;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()LY/L;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-virtual {v0, p1}, Lae/o;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->d()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-static {v0}, Lae/q;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/common/base/Y;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()LY/Z;
    .locals 1

    iget-object v0, p0, LY/I;->a:LY/Z;

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
