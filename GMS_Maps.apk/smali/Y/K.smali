.class final enum LY/K;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements LY/L;


# static fields
.field public static final enum a:LY/K;

.field private static final synthetic b:[LY/K;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, LY/K;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LY/K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/K;->a:LY/K;

    const/4 v0, 0x1

    new-array v0, v0, [LY/K;

    sget-object v1, LY/K;->a:LY/K;

    aput-object v1, v0, v2

    sput-object v0, LY/K;->b:[LY/K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LY/K;
    .locals 1

    const-class v0, LY/K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LY/K;

    return-object v0
.end method

.method public static values()[LY/K;
    .locals 1

    sget-object v0, LY/K;->b:[LY/K;

    invoke-virtual {v0}, [LY/K;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LY/K;

    return-object v0
.end method


# virtual methods
.method public a()LY/Z;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(LY/L;)V
    .locals 0

    return-void
.end method

.method public a(LY/Z;)V
    .locals 0

    return-void
.end method

.method public b()LY/L;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method public b(LY/L;)V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(LY/L;)V
    .locals 0

    return-void
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d(LY/L;)V
    .locals 0

    return-void
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()LY/L;
    .locals 0

    return-object p0
.end method

.method public g()LY/L;
    .locals 0

    return-object p0
.end method

.method public h()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public i()LY/L;
    .locals 0

    return-object p0
.end method

.method public j()LY/L;
    .locals 0

    return-object p0
.end method
