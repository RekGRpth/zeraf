.class public Lcom/mediatek/StkSelection/StkSelection;
.super Landroid/app/Activity;
.source "StkSelection.java"


# static fields
.field public static final LOGTAG:Ljava/lang/String; = "StkSelection "

.field private static final REQUEST_TYPE:I = 0x12e

.field public static bSIM1Inserted:Z

.field public static bSIM2Inserted:Z

.field public static bSIM3Inserted:Z

.field public static bSIM4Inserted:Z

.field public static mSlot:I

.field public static strTargetClass:Ljava/lang/String;

.field public static strTargetLoc:Ljava/lang/String;


# instance fields
.field private mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field private serviceComplete:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/StkSelection/StkSelection;->bSIM1Inserted:Z

    sput-boolean v0, Lcom/mediatek/StkSelection/StkSelection;->bSIM2Inserted:Z

    sput-boolean v0, Lcom/mediatek/StkSelection/StkSelection;->bSIM3Inserted:Z

    sput-boolean v0, Lcom/mediatek/StkSelection/StkSelection;->bSIM4Inserted:Z

    sput-object v1, Lcom/mediatek/StkSelection/StkSelection;->strTargetLoc:Ljava/lang/String;

    sput-object v1, Lcom/mediatek/StkSelection/StkSelection;->strTargetClass:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/mediatek/StkSelection/StkSelection;->mSlot:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/StkSelection/StkSelection$1;

    invoke-direct {v0, p0}, Lcom/mediatek/StkSelection/StkSelection$1;-><init>(Lcom/mediatek/StkSelection/StkSelection;)V

    iput-object v0, p0, Lcom/mediatek/StkSelection/StkSelection;->serviceComplete:Ljava/lang/Runnable;

    new-instance v0, Lcom/mediatek/CellConnService/CellConnMgr;

    iget-object v1, p0, Lcom/mediatek/StkSelection/StkSelection;->serviceComplete:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Lcom/mediatek/CellConnService/CellConnMgr;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/StkSelection/StkSelection;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-void
.end method

.method public static IccCardReady(I)Z
    .locals 3
    .param p0    # I

    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mediatek/StkSelection/StkSelection;)Lcom/mediatek/CellConnService/CellConnMgr;
    .locals 1
    .param p0    # Lcom/mediatek/StkSelection/StkSelection;

    iget-object v0, p0, Lcom/mediatek/StkSelection/StkSelection;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-object v0
.end method

.method public static hasIccCard(I)Z
    .locals 6
    .param p0    # I

    const/4 v0, 0x0

    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, p0}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    const-string v3, "StkSelection "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSimInsert: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :catch_0
    move-exception v1

    const-string v3, "StkSelection "

    const-string v4, "isSimInsert: fail"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private launchStk(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    const-string v5, "com.android.stk"

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const-string v5, "com.android.phone"

    const-string v0, "com.mediatek.phone.StkListEntrance"

    :cond_0
    const-string v7, "StkSelection "

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "launchStk, sim_id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", multi: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/mediatek/StkSelection/StkSelection;->startActivity(Landroid/content/Intent;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    packed-switch p1, :pswitch_data_0

    :goto_1
    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v5, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/StkSelection/StkSelection;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "airplane_mode_on"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    const-string v7, "StkSelection "

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0}, Lcom/mediatek/StkSelection/StkSelection;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    if-eqz v4, :cond_3

    const v7, 0x7f040004

    invoke-virtual {p0, v7}, Lcom/mediatek/StkSelection/StkSelection;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/StkSelection/StkSelection;->showTextToast(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    const-string v0, "com.android.stk.StkLauncherActivity"

    goto :goto_1

    :pswitch_1
    const-string v0, "com.android.stk.StkLauncherActivityII"

    goto :goto_1

    :pswitch_2
    const-string v0, "com.android.stk.StkLauncherActivityIII"

    goto :goto_1

    :pswitch_3
    const-string v0, "com.android.stk.StkLauncherActivityIV"

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v7, "StkSelection "

    const-string v8, "fail to get property from Settings"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    invoke-virtual {v6, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_0

    const v7, 0x7f040005

    invoke-virtual {p0, v7}, Lcom/mediatek/StkSelection/StkSelection;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/StkSelection/StkSelection;->showTextToast(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showTextToast(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/mediatek/StkSelection/StkSelection;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "StkSelection "

    const-string v1, "[onCreate]+"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/StkSelection/StkSelection;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v0, p0}, Lcom/mediatek/CellConnService/CellConnMgr;->register(Landroid/content/Context;)V

    const-string v0, "StkSelection "

    const-string v1, "[onCreate]-"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "StkSelection "

    const-string v1, "[onDestroy]+"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/StkSelection/StkSelection;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->unregister()V

    const-string v0, "StkSelection "

    const-string v1, "[onDestroy]-"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v2, "StkSelection "

    const-string v3, "[onResume]+"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/mediatek/StkSelection/StkSelection;->hasIccCard(I)Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM1Inserted:Z

    invoke-static {v4}, Lcom/mediatek/StkSelection/StkSelection;->hasIccCard(I)Z

    move-result v2

    sput-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM2Inserted:Z

    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM1Inserted:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM2Inserted:Z

    if-nez v2, :cond_0

    const v2, 0x7f040003

    invoke-virtual {p0, v2}, Lcom/mediatek/StkSelection/StkSelection;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/StkSelection/StkSelection;->showTextToast(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/StkSelection/StkSelection;->finish()V

    :goto_0
    return-void

    :cond_0
    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM1Inserted:Z

    if-ne v2, v4, :cond_1

    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM2Inserted:Z

    if-nez v2, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/mediatek/StkSelection/StkSelection;->launchStk(IZ)V

    :goto_1
    invoke-virtual {p0}, Lcom/mediatek/StkSelection/StkSelection;->finish()V

    const-string v2, "StkSelection "

    const-string v3, "[onResume]-"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM1Inserted:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/mediatek/StkSelection/StkSelection;->bSIM2Inserted:Z

    if-ne v2, v4, :cond_2

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/mediatek/StkSelection/StkSelection;->launchStk(IZ)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/mediatek/StkSelection/StkSelection;->launchStk(IZ)V

    goto :goto_1
.end method
