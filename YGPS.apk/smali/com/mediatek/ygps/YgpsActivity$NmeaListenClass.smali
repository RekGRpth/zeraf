.class public Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;
.super Ljava/lang/Object;
.source "YgpsActivity.java"

# interfaces
.implements Landroid/location/GpsStatus$NmeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/ygps/YgpsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NmeaListenClass"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/ygps/YgpsActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNmeaReceived(JLjava/lang/String;)V
    .locals 4
    .param p1    # J
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0}, Lcom/mediatek/ygps/YgpsActivity;->access$800(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0}, Lcom/mediatek/ygps/YgpsActivity;->access$900(Lcom/mediatek/ygps/YgpsActivity;)J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0}, Lcom/mediatek/ygps/YgpsActivity;->access$1000(Lcom/mediatek/ygps/YgpsActivity;)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0, p1, p2}, Lcom/mediatek/ygps/YgpsActivity;->access$902(Lcom/mediatek/ygps/YgpsActivity;J)J

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0}, Lcom/mediatek/ygps/YgpsActivity;->access$1100(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0, p3}, Lcom/mediatek/ygps/YgpsActivity;->access$1200(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v0}, Lcom/mediatek/ygps/YgpsActivity;->access$1300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
