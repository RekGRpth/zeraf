.class public abstract Lcom/android/common/widget/EsCompositeCursorAdapter;
.super Landroid/widget/BaseAdapter;
.source "EsCompositeCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;
    }
.end annotation


# instance fields
.field private mCacheValid:Z

.field protected final mContext:Landroid/content/Context;

.field private mCount:I

.field private mNotificationNeeded:Z

.field private mNotificationsEnabled:Z

.field private mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

.field private mSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    iput v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCount:I

    iput-boolean v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    iput-boolean v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationsEnabled:Z

    iput-object p1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mContext:Landroid/content/Context;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    iput-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    return-void
.end method

.method private ensureCacheValid()V
    .locals 5

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCount:I

    const/4 v2, 0x0

    :goto_1
    iget v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v2, v4, :cond_4

    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v2

    iget-object v1, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_2
    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v2

    iget-boolean v4, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v4, :cond_2

    if-nez v0, :cond_1

    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v2

    iget-boolean v4, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->showIfEmpty:Z

    if-eqz v4, :cond_2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v2

    iput v0, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    iget v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCount:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCount:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    goto :goto_0
.end method


# virtual methods
.method public final addPartition(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/4 v4, 0x0

    new-instance v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    invoke-direct {v0, v4, p2}, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;-><init>(ZZ)V

    iget v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    iget v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    :cond_0
    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    iget v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    aput-object v0, v1, v2

    iput-boolean v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected bindHeaderView$3ab248f1(Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I

    return-void
.end method

.method protected abstract bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
.end method

.method public changeCursor(ILandroid/database/Cursor;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, p1

    iget-object v0, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_2

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->isCursorClosingEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, p1

    iput-object p2, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, p1

    const-string v2, "_id"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->idColumnIndex:I

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public final checkPartitions(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    add-int/lit8 v1, v3, -0x1

    :goto_0
    if-ltz v1, :cond_3

    iget-object v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v2, v3, v1

    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    :goto_1
    if-nez v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "partcheck s:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " emptypart:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "partcheck s:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stalepart:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    return-void
.end method

.method public final clearPartitions()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final close()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v2, v2, v1

    iget-object v0, v2, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v2, v2, v1

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iput v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    iput-boolean v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCacheValid:Z

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    iget v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mCount:I

    return v0
.end method

.method public final getCount(I)I
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final getCursor(I)Landroid/database/Cursor;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    iget v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v2, v5, :cond_1

    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v1, v4, v5

    if-lt p1, v4, :cond_3

    if-ge p1, v1, :cond_3

    sub-int v3, p1, v4

    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v2

    iget-boolean v5, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v5, :cond_0

    add-int/lit8 v3, v3, -0x1

    :cond_0
    const/4 v5, -0x1

    if-ne v3, v5, :cond_2

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v2

    iget-object v0, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_3
    move v4, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 9
    .param p1    # I

    const/4 v8, -0x1

    const-wide/16 v5, 0x0

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_0
    iget v7, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v2, v7, :cond_1

    iget-object v7, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v7, v7, v2

    iget v7, v7, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v1, v4, v7

    if-lt p1, v4, :cond_3

    if-ge p1, v1, :cond_3

    sub-int v3, p1, v4

    iget-object v7, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v7, v7, v2

    iget-boolean v7, v7, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v7, :cond_0

    add-int/lit8 v3, v3, -0x1

    :cond_0
    if-ne v3, v8, :cond_2

    :cond_1
    :goto_1
    return-wide v5

    :cond_2
    iget-object v7, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v7, v7, v2

    iget v7, v7, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->idColumnIndex:I

    if-eq v7, v8, :cond_1

    iget-object v7, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v7, v7, v2

    iget-object v0, v7, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->idColumnIndex:I

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    goto :goto_1

    :cond_3
    move v4, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1    # I

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v0, v3, v4

    if-lt p1, v3, :cond_1

    if-ge p1, v0, :cond_1

    sub-int v2, p1, v3

    iget-object v4, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v4, v4, v1

    iget-boolean v4, v4, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v4, :cond_0

    if-nez v2, :cond_0

    const/4 v4, -0x1

    :goto_1
    return v4

    :cond_0
    invoke-virtual {p0, v1, v2}, Lcom/android/common/widget/EsCompositeCursorAdapter;->getItemViewType(II)I

    move-result v4

    goto :goto_1

    :cond_1
    move v3, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v4
.end method

.method protected getItemViewType(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x1

    return v0
.end method

.method public getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final getPartitionForPosition(I)I
    .locals 4
    .param p1    # I

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v3, v3, v1

    iget v3, v3, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v0, v2, v3

    if-lt p1, v2, :cond_0

    if-ge p1, v0, :cond_0

    :goto_1
    return v1

    :cond_0
    move v2, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final getPositionForPartition(I)I
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method protected getView(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/database/Cursor;
    .param p3    # I
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/view/ViewGroup;

    if-eqz p4, :cond_0

    move-object v1, p4

    :goto_0
    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/common/widget/EsCompositeCursorAdapter;->bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mContext:Landroid/content/Context;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/common/widget/EsCompositeCursorAdapter;->newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v7, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, v1

    iget v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v6, v7, v0

    if-lt p1, v7, :cond_4

    if-ge p1, v6, :cond_4

    sub-int v3, p1, v7

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, v1

    iget-boolean v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v0, :cond_0

    add-int/lit8 v3, v3, -0x1

    :cond_0
    const/4 v0, -0x1

    if-ne v3, v0, :cond_2

    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz p2, :cond_1

    move-object v8, p2

    :goto_1
    invoke-virtual {p0, v8, v1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->bindHeaderView$3ab248f1(Landroid/view/View;I)V

    :goto_2
    if-nez v8, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "View should not be null, partition: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " position: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v2, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2, v1, v0, p3}, Lcom/android/common/widget/EsCompositeCursorAdapter;->newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t move cursor to position "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v0, v0, v1

    iget-object v2, v0, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/common/widget/EsCompositeCursorAdapter;->getView(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    goto :goto_2

    :cond_4
    move v7, v6

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    :cond_6
    return-object v8
.end method

.method public getViewTypeCount()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->getItemViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isCursorClosingEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 6
    .param p1    # I

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->ensureCacheValid()V

    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mSize:I

    if-ge v1, v5, :cond_0

    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->count:I

    add-int v0, v3, v5

    if-lt p1, v3, :cond_2

    if-ge p1, v0, :cond_2

    sub-int v2, p1, v3

    iget-object v5, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v5, v5, v1

    iget-boolean v5, v5, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->hasHeader:Z

    if-eqz v5, :cond_1

    if-nez v2, :cond_1

    :cond_0
    :goto_1
    return v4

    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->isEnabled$255f299(I)Z

    move-result v4

    goto :goto_1

    :cond_2
    move v3, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected isEnabled$255f299(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public final isPartitionEmpty(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mPartitions:[Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;

    aget-object v1, v1, p1

    iget-object v0, v1, Lcom/android/common/widget/EsCompositeCursorAdapter$Partition;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public notifyDataSetChanged()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationNeeded:Z

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationNeeded:Z

    goto :goto_0
.end method

.method public final setNotificationsEnabled(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationsEnabled:Z

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/common/widget/EsCompositeCursorAdapter;->mNotificationNeeded:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
