.class public final Lcom/google/protobuf/Internal;
.super Ljava/lang/Object;
.source "Internal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/Internal$EnumLiteMap;
    }
.end annotation


# direct methods
.method public static isValidUtf8(Lcom/google/protobuf/ByteString;)Z
    .locals 12
    .param p0    # Lcom/google/protobuf/ByteString;

    const/16 v11, 0xf4

    const/16 v10, 0xbf

    const/16 v9, 0x80

    const/4 v7, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/ByteString;->size()I

    move-result v6

    move v5, v4

    :cond_0
    :goto_0
    if-ge v5, v6, :cond_d

    add-int/lit8 v4, v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/protobuf/ByteString;->byteAt(I)B

    move-result v8

    and-int/lit16 v0, v8, 0xff

    if-lt v0, v9, :cond_f

    const/16 v8, 0xc2

    if-lt v0, v8, :cond_1

    if-le v0, v11, :cond_2

    :cond_1
    :goto_1
    return v7

    :cond_2
    if-ge v4, v6, :cond_1

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/protobuf/ByteString;->byteAt(I)B

    move-result v8

    and-int/lit16 v1, v8, 0xff

    if-lt v1, v9, :cond_3

    if-le v1, v10, :cond_4

    :cond_3
    move v4, v5

    goto :goto_1

    :cond_4
    const/16 v8, 0xdf

    if-le v0, v8, :cond_0

    if-lt v5, v6, :cond_5

    move v4, v5

    goto :goto_1

    :cond_5
    add-int/lit8 v4, v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/protobuf/ByteString;->byteAt(I)B

    move-result v8

    and-int/lit16 v2, v8, 0xff

    if-lt v2, v9, :cond_1

    if-gt v2, v10, :cond_1

    const/16 v8, 0xef

    if-gt v0, v8, :cond_8

    const/16 v8, 0xe0

    if-ne v0, v8, :cond_6

    const/16 v8, 0xa0

    if-lt v1, v8, :cond_1

    :cond_6
    const/16 v8, 0xed

    if-ne v0, v8, :cond_7

    const/16 v8, 0x9f

    if-gt v1, v8, :cond_1

    :cond_7
    :goto_2
    move v5, v4

    goto :goto_0

    :cond_8
    if-ge v4, v6, :cond_1

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/protobuf/ByteString;->byteAt(I)B

    move-result v8

    and-int/lit16 v3, v8, 0xff

    if-lt v3, v9, :cond_9

    if-le v3, v10, :cond_a

    :cond_9
    move v4, v5

    goto :goto_1

    :cond_a
    const/16 v8, 0xf0

    if-ne v0, v8, :cond_b

    const/16 v8, 0x90

    if-lt v1, v8, :cond_c

    :cond_b
    if-ne v0, v11, :cond_e

    const/16 v8, 0x8f

    if-le v1, v8, :cond_e

    :cond_c
    move v4, v5

    goto :goto_1

    :cond_d
    const/4 v7, 0x1

    move v4, v5

    goto :goto_1

    :cond_e
    move v4, v5

    goto :goto_2

    :cond_f
    move v5, v4

    goto :goto_0
.end method
