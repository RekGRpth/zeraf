.class public final Lcom/google/wireless/webapps/Version$ClientVersion;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Version.java"

# interfaces
.implements Lcom/google/wireless/webapps/Version$ClientVersionOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/webapps/Version;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientVersion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/webapps/Version$ClientVersion$Builder;,
        Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;,
        Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;,
        Lcom/google/wireless/webapps/Version$ClientVersion$App;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private static final serialVersionUID:J


# instance fields
.field private app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

.field private bitField0_:I

.field private buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

.field private dataVersion_:I

.field private deviceHardware_:Ljava/lang/Object;

.field private deviceOs_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

.field private version_:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-direct {v0}, Lcom/google/wireless/webapps/Version$ClientVersion;-><init>()V

    sput-object v0, Lcom/google/wireless/webapps/Version$ClientVersion;->defaultInstance:Lcom/google/wireless/webapps/Version$ClientVersion;

    sget-object v1, Lcom/google/wireless/webapps/Version$ClientVersion$App;->GOOGLE_PLUS:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    iput-object v1, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    sget-object v1, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->DEVELOPER:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    iput-object v1, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    sget-object v1, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->WEB:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iput-object v1, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iput v2, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I

    iput v2, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/webapps/Version$ClientVersion;-><init>(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;)V

    return-void
.end method

.method static synthetic access$1002(Lcom/google/wireless/webapps/Version$ClientVersion;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    return p1
.end method

.method static synthetic access$302(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$App;)Lcom/google/wireless/webapps/Version$ClientVersion$App;
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$App;

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;)Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;)Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/wireless/webapps/Version$ClientVersion;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/wireless/webapps/Version$ClientVersion;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/wireless/webapps/Version$ClientVersion;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/wireless/webapps/Version$ClientVersion;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 1

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion;->defaultInstance:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method private getDeviceHardwareBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getDeviceOsBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->access$100()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->access$100()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getApp()Lcom/google/wireless/webapps/Version$ClientVersion$App;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    return-object v0
.end method

.method public final getBuildType()Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    return-object v0
.end method

.method public final getDataVersion()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I

    return v0
.end method

.method public final getDeviceHardware()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getDeviceOs()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getPlatformType()Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    invoke-virtual {v2}, Lcom/google/wireless/webapps/Version$ClientVersion$App;->getNumber()I

    move-result v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    invoke-virtual {v2}, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->getNumber()I

    move-result v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    invoke-virtual {v3}, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    const/4 v2, 0x5

    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    const/4 v2, 0x6

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceOsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    const/4 v2, 0x7

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceHardwareBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getVersion()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I

    return v0
.end method

.method public final hasApp()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasBuildType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDataVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDeviceHardware()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDeviceOs()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPlatformType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/webapps/Version$ClientVersion;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$App;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    invoke-virtual {v1}, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceOsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceHardwareBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_6
    return-void
.end method
