.class public final Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Version.java"

# interfaces
.implements Lcom/google/wireless/webapps/Version$ClientVersionOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/webapps/Version$ClientVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/webapps/Version$ClientVersion;",
        "Lcom/google/wireless/webapps/Version$ClientVersion$Builder;",
        ">;",
        "Lcom/google/wireless/webapps/Version$ClientVersionOrBuilder;"
    }
.end annotation


# instance fields
.field private app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

.field private bitField0_:I

.field private buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

.field private dataVersion_:I

.field private deviceHardware_:Ljava/lang/Object;

.field private deviceOs_:Ljava/lang/Object;

.field private platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

.field private version_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$App;->GOOGLE_PLUS:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->DEVELOPER:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->WEB:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceOs_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceHardware_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    invoke-direct {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;-><init>()V

    return-object v0
.end method

.method private clear()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$App;->GOOGLE_PLUS:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->DEVELOPER:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->WEB:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->version_:I

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput v1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->dataVersion_:I

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceOs_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceHardware_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    return-object p0
.end method

.method private clone()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    invoke-direct {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$App;->valueOf(I)Lcom/google/wireless/webapps/Version$ClientVersion$App;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->valueOf(I)Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->valueOf(I)Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    goto :goto_0

    :sswitch_4
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->version_:I

    goto :goto_0

    :sswitch_5
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->dataVersion_:I

    goto :goto_0

    :sswitch_6
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceOs_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceHardware_:Ljava/lang/Object;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 5

    new-instance v1, Lcom/google/wireless/webapps/Version$ClientVersion;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;-><init>(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;B)V

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$302(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$App;)Lcom/google/wireless/webapps/Version$ClientVersion$App;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$402(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;)Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$502(Lcom/google/wireless/webapps/Version$ClientVersion;Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;)Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->version_:I

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->version_:I
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$602(Lcom/google/wireless/webapps/Version$ClientVersion;I)I

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->dataVersion_:I

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->dataVersion_:I
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$702(Lcom/google/wireless/webapps/Version$ClientVersion;I)I

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceOs_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->deviceOs_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$802(Lcom/google/wireless/webapps/Version$ClientVersion;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceHardware_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->deviceHardware_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$902(Lcom/google/wireless/webapps/Version$ClientVersion;Ljava/lang/Object;)Ljava/lang/Object;

    # setter for: Lcom/google/wireless/webapps/Version$ClientVersion;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/webapps/Version$ClientVersion;->access$1002(Lcom/google/wireless/webapps/Version$ClientVersion;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->clear()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->clear()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->clone()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->clone()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->clone()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasApp()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getApp()Lcom/google/wireless/webapps/Version$ClientVersion$App;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setApp(Lcom/google/wireless/webapps/Version$ClientVersion$App;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasBuildType()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getBuildType()Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setBuildType(Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasPlatformType()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getPlatformType()Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setPlatformType(Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasVersion()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasDataVersion()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDataVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDataVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasDeviceOs()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceOs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDeviceOs(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->hasDeviceHardware()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDeviceHardware()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDeviceHardware(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    goto :goto_0
.end method

.method public final setApp(Lcom/google/wireless/webapps/Version$ClientVersion$App;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$App;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->app_:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    return-object p0
.end method

.method public final setBuildType(Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildType_:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    return-object p0
.end method

.method public final setDataVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->dataVersion_:I

    return-object p0
.end method

.method public final setDeviceHardware(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceHardware_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setDeviceOs(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->deviceOs_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setPlatformType(Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->platformType_:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    return-object p0
.end method

.method public final setVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->bitField0_:I

    iput p1, p0, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->version_:I

    return-object p0
.end method
