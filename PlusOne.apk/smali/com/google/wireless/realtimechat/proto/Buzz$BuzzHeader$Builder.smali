.class public final Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeaderOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeaderOrBuilder;"
    }
.end annotation


# instance fields
.field private actAsPrimary_:Z

.field private alreadySentToPrimary_:Z

.field private bitField0_:I

.field private countForReliabilityTest_:Z

.field private destinationPayloadsSetSender_:Z

.field private dropIfNoEndpoint_:Z

.field private dropIfNoResource_:Z

.field private individuallyRoutedPayload_:Z

.field private secondaryPayload_:Ljava/lang/Object;

.field private stateUpdate_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$2100()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    goto :goto_0

    :sswitch_3
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    goto :goto_0

    :sswitch_5
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    goto :goto_0

    :sswitch_6
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    goto :goto_0

    :sswitch_7
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    goto :goto_0

    :sswitch_8
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    goto :goto_0

    :sswitch_9
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x32 -> :sswitch_3
        0x38 -> :sswitch_4
        0x40 -> :sswitch_5
        0x48 -> :sswitch_6
        0x50 -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2302(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2402(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2502(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2602(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2702(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2802(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$2902(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$3002(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$3102(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->access$3202(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearActAsPrimary()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    return-object p0
.end method

.method public final clearAlreadySentToPrimary()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    return-object p0
.end method

.method public final clearCountForReliabilityTest()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    return-object p0
.end method

.method public final clearDestinationPayloadsSetSender()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    return-object p0
.end method

.method public final clearDropIfNoEndpoint()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    return-object p0
.end method

.method public final clearDropIfNoResource()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    return-object p0
.end method

.method public final clearIndividuallyRoutedPayload()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    return-object p0
.end method

.method public final clearSecondaryPayload()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getSecondaryPayload()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearStateUpdate()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getActAsPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    return v0
.end method

.method public final getAlreadySentToPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    return v0
.end method

.method public final getCountForReliabilityTest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    return-object v0
.end method

.method public final getDestinationPayloadsSetSender()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    return v0
.end method

.method public final getDropIfNoEndpoint()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    return v0
.end method

.method public final getDropIfNoResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    return v0
.end method

.method public final getIndividuallyRoutedPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    return v0
.end method

.method public final getSecondaryPayload()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getStateUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    return v0
.end method

.method public final hasActAsPrimary()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAlreadySentToPrimary()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCountForReliabilityTest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDestinationPayloadsSetSender()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDropIfNoEndpoint()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDropIfNoResource()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasIndividuallyRoutedPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSecondaryPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStateUpdate()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasStateUpdate()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getStateUpdate()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setStateUpdate(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasAlreadySentToPrimary()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getAlreadySentToPrimary()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setAlreadySentToPrimary(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasSecondaryPayload()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getSecondaryPayload()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setSecondaryPayload(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasDropIfNoEndpoint()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDropIfNoEndpoint()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setDropIfNoEndpoint(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasActAsPrimary()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getActAsPrimary()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setActAsPrimary(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasDropIfNoResource()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDropIfNoResource()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setDropIfNoResource(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasIndividuallyRoutedPayload()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getIndividuallyRoutedPayload()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setIndividuallyRoutedPayload(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasCountForReliabilityTest()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getCountForReliabilityTest()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setCountForReliabilityTest(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->hasDestinationPayloadsSetSender()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDestinationPayloadsSetSender()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->setDestinationPayloadsSetSender(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    goto :goto_0
.end method

.method public final setActAsPrimary(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->actAsPrimary_:Z

    return-object p0
.end method

.method public final setAlreadySentToPrimary(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->alreadySentToPrimary_:Z

    return-object p0
.end method

.method public final setCountForReliabilityTest(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->countForReliabilityTest_:Z

    return-object p0
.end method

.method public final setDestinationPayloadsSetSender(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->destinationPayloadsSetSender_:Z

    return-object p0
.end method

.method public final setDropIfNoEndpoint(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoEndpoint_:Z

    return-object p0
.end method

.method public final setDropIfNoResource(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->dropIfNoResource_:Z

    return-object p0
.end method

.method public final setIndividuallyRoutedPayload(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->individuallyRoutedPayload_:Z

    return-object p0
.end method

.method public final setSecondaryPayload(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->secondaryPayload_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setStateUpdate(Z)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->stateUpdate_:Z

    return-object p0
.end method
