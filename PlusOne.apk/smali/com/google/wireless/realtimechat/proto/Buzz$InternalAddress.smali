.class public final Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddressOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InternalAddress"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;,
        Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;,
        Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$GatewayOrBuilder;,
        Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;,
        Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JIDOrBuilder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

.field private static final serialVersionUID:J


# instance fields
.field private addressable_:Z

.field private bitField0_:I

.field private gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

.field private jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;)V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->access$1400()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->access$1400()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAddressable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public final getGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    return-object v0
.end method

.method public final getJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeGroupSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeGroupSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final hasAddressable()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGateway()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasJID()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->hasJID()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    iput-byte v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->hasGateway()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    iput-byte v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_3
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeGroup(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeGroup(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    return-void
.end method
