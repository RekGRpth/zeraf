.class public final Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

.field private bitField0_:I

.field private device_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

.field private userExists_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->EVERYONE:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    return-void
.end method

.method static synthetic access$54400()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureDeviceIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->hasParticipant()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->getParticipant()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->setParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->addDevice(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final addAllDevice(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addDevice(ILcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addDevice(ILcom/google/wireless/realtimechat/proto/Client$DeviceInfo;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addDevice(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDevice(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$54602(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$54702(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$54802(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->userExists_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$54902(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;Z)Z

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->device_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$55002(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;Ljava/util/List;)Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$55102(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->EVERYONE:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAcl()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->EVERYONE:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    return-object p0
.end method

.method public final clearDevice()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearParticipant()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearStatus()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final clearUserExists()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDevice(I)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    return-object v0
.end method

.method public final getDeviceCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDeviceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipant()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final getUserExists()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    return v0
.end method

.method public final hasAcl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasParticipant()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUserExists()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 3
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->hasParticipant()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getParticipant()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    :goto_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->hasAcl()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->setAcl(Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->hasUserExists()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getUserExists()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->setUserExists(Z)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    :cond_5
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->device_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$55000(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->device_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$55000(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    goto :goto_0

    :cond_6
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->device_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->access$55000(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public final setAcl(Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->acl_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    return-object p0
.end method

.method public final setDevice(ILcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDevice(ILcom/google/wireless/realtimechat/proto/Client$DeviceInfo;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->ensureDeviceIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->device_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->participant_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final setUserExists(Z)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->userExists_:Z

    return-object p0
.end method
