.class public final Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Experiments.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Experiments$ConfigOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Experiments$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Experiments$Config;",
        "Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Experiments$ConfigOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private experimentMapping_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    return-void
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$Config;
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;-><init>(Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;B)V

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->access$2202(Lcom/google/wireless/realtimechat/proto/Experiments$Config;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v2

    if-eq v1, v2, :cond_0

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->access$2200(Lcom/google/wireless/realtimechat/proto/Experiments$Config;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->access$2200(Lcom/google/wireless/realtimechat/proto/Experiments$Config;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    iget v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->access$2200(Lcom/google/wireless/realtimechat/proto/Experiments$Config;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private ensureExperimentMappingIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->newBuilder()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->addExperimentMapping(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final addAllExperimentMapping(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addExperimentMapping(ILcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->build()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addExperimentMapping(ILcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addExperimentMapping(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->build()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addExperimentMapping(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearExperimentMapping()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->bitField0_:I

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$Config;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v0

    return-object v0
.end method

.method public final getExperimentMapping(I)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    return-object v0
.end method

.method public final getExperimentMappingCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getExperimentMappingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setExperimentMapping(ILcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->build()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setExperimentMapping(ILcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->ensureExperimentMappingIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
