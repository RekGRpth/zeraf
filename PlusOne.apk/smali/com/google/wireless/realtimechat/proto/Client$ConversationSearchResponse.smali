.class public final Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationSearchResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private conversation_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;)V

    return-void
.end method

.method static synthetic access$51302(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p1
.end method

.method static synthetic access$51400(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$51402(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$51502(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->access$51100()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->access$51100()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getConversation(I)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    return-object v0
.end method

.method public final getConversationCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getConversationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    return-object v0
.end method

.method public final getConversationOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;

    return-object v0
.end method

.method public final getConversationOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 5

    const/4 v4, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedSerializedSize:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    :goto_0
    return v2

    :cond_0
    const/4 v1, 0x0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v3

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v3

    add-int/lit8 v1, v3, 0x0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    const/4 v4, 0x2

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedSerializedSize:I

    move v2, v1

    goto :goto_0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getSerializedSize()I

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    const/4 v2, 0x2

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->conversation_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
