.class public final Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$EventOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$EventOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

.field private membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

.field private migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

.field private receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

.field private receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

.field private tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

.field private timestamp_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    return-void
.end method

.method static synthetic access$35300()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasChatMessage()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasReceipt()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    :cond_2
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasMembershipChange()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    :cond_3
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasGroupConversationRename()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    :cond_4
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto/16 :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasMigration()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    :cond_5
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto/16 :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->hasTileEvent()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    :cond_6
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;-><init>(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->timestamp_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$35502(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;J)J

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$35602(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$35702(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$35802(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$35902(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$36002(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$36102(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration;

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$36202(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->access$36302(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearChatMessage()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearMigration()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearReceipt()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearReceiverState()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    return-object p0
.end method

.method public final clearTileEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearTimestamp()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    return-object v0
.end method

.method public final getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    return-object v0
.end method

.method public final getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    return-object v0
.end method

.method public final getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    return-object v0
.end method

.method public final getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object v0
.end method

.method public final getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    return-object v0
.end method

.method public final getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    return-object v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    return-wide v0
.end method

.method public final hasChatMessage()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGroupConversationRename()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMembershipChange()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMigration()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceiverState()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTileEvent()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTimestamp()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 3
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasReceiverState()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->setReceiverState(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasChatMessage()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    if-eq v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    :goto_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasReceipt()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v2

    if-eq v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    :goto_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasMembershipChange()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v2

    if-eq v1, v2, :cond_b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    :goto_3
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasGroupConversationRename()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v2

    if-eq v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    :goto_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasMigration()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v2

    if-eq v1, v2, :cond_d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    :goto_5
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasTileEvent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_e

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v2

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    :goto_6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    goto/16 :goto_0

    :cond_9
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    goto/16 :goto_1

    :cond_a
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    goto/16 :goto_2

    :cond_b
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    goto/16 :goto_3

    :cond_c
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    goto :goto_4

    :cond_d
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    goto :goto_5

    :cond_e
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    goto :goto_6
.end method

.method public final setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Migration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setReceiverState(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->receiverState_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    return-object p0
.end method

.method public final setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->timestamp_:J

    return-object p0
.end method
