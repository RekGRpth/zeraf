.class public final Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$ContentOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data$Content;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Data$Content;",
        "Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Data$ContentOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private linkUrl_:Ljava/lang/Object;

.field private location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

.field private photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

.field private photoUrl_:Ljava/lang/Object;

.field private text_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    return-void
.end method

.method static synthetic access$2700()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 5

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v2

    if-eq v1, v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasText()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->setText(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasPhotoUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getPhotoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasLinkUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getLinkUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->setLinkUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Data$Location;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    :goto_0
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasPhotoMetadata()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v1

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_6

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v3

    if-eq v2, v3, :cond_6

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    invoke-static {v2}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    :goto_1
    iget v1, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    :cond_4
    return-object v0

    :cond_5
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    goto :goto_0

    :cond_6
    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    goto :goto_1
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->hasPhotoMetadata()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->getPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->setPhotoMetadata(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Data$Content;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Content;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Data$Content;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;-><init>(Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->text_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$2902(Lcom/google/wireless/realtimechat/proto/Data$Content;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->photoUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$3002(Lcom/google/wireless/realtimechat/proto/Data$Content;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->linkUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$3102(Lcom/google/wireless/realtimechat/proto/Data$Content;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$3202(Lcom/google/wireless/realtimechat/proto/Data$Content;Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$3302(Lcom/google/wireless/realtimechat/proto/Data$Content;Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Content;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Content;->access$3402(Lcom/google/wireless/realtimechat/proto/Data$Content;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearLinkUrl()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getLinkUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearLocation()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearPhotoUrl()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getPhotoUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearText()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Content;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v0

    return-object v0
.end method

.method public final getLinkUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    return-object v0
.end method

.method public final getPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    return-object v0
.end method

.method public final getPhotoUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getText()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final hasLinkUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLocation()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhotoMetadata()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhotoUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasText()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setLinkUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->linkUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setPhotoMetadata(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setPhotoMetadata(Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoMetadata_:Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setPhotoUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->photoUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setText(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Content$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method
