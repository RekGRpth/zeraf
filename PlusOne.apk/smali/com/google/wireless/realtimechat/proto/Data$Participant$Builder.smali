.class public final Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data$Participant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
        "Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private firstName_:Ljava/lang/Object;

.field private fullName_:Ljava/lang/Object;

.field private lastSeenAt_:J

.field private participantId_:Ljava/lang/Object;

.field private profilePhotoUrl_:Ljava/lang/Object;

.field private type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    goto :goto_0

    :sswitch_3
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :sswitch_6
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;-><init>(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->participantId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$302(Lcom/google/wireless/realtimechat/proto/Data$Participant;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->lastSeenAt_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$402(Lcom/google/wireless/realtimechat/proto/Data$Participant;J)J

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->fullName_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$502(Lcom/google/wireless/realtimechat/proto/Data$Participant;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->firstName_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$602(Lcom/google/wireless/realtimechat/proto/Data$Participant;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$702(Lcom/google/wireless/realtimechat/proto/Data$Participant;Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->profilePhotoUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$802(Lcom/google/wireless/realtimechat/proto/Data$Participant;Ljava/lang/Object;)Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$Participant;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->access$902(Lcom/google/wireless/realtimechat/proto/Data$Participant;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearFirstName()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearFullName()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearLastSeenAt()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    return-object p0
.end method

.method public final clearParticipantId()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearProfilePhotoUrl()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearType()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    return-object v0
.end method

.method public final getFirstName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getFullName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getLastSeenAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    return-wide v0
.end method

.method public final getParticipantId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getProfilePhotoUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    return-object v0
.end method

.method public final hasFirstName()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFullName()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLastSeenAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasParticipantId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasProfilePhotoUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasParticipantId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasLastSeenAt()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getLastSeenAt()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setLastSeenAt(J)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasFullName()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasFirstName()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasType()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getType()Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasProfilePhotoUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setProfilePhotoUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    goto :goto_0
.end method

.method public final setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->firstName_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->fullName_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setLastSeenAt(J)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->lastSeenAt_:J

    return-object p0
.end method

.method public final setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->participantId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setProfilePhotoUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->profilePhotoUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setType(Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    return-object p0
.end method
