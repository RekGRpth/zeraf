.class public final Lcom/google/api/services/plusi/model/DataSuggestionActionJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataSuggestionActionJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataSuggestionAction;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataSuggestionActionJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataSuggestionActionJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataSuggestionActionJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataSuggestionActionJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataSuggestionActionJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "accepted"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "actionType"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataCircleIdJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "circleId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataSuggestedEntityIdJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "suggestedEntityId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "suggestionContext"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/DataCircleMemberIdJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "suggestionId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "suggestionUi"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/google/api/services/plusi/model/DataSuggestionActionJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "timeUsec"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataSuggestionActionJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataSuggestionActionJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataSuggestionActionJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->accepted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->actionType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionContext:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionUi:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->timeUsec:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataSuggestionAction;-><init>()V

    return-object v0
.end method
