.class public final Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ViewerSquareSquareActivityStatsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "lastPostAuthorObfuscatedGaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "lastPostTimeUsec"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "lastVisitTimeUsec"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "totalPostCount"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "unreadPostCount"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->lastPostAuthorObfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->lastPostTimeUsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->lastVisitTimeUsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->totalPostCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->unreadPostCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;-><init>()V

    return-object v0
.end method
