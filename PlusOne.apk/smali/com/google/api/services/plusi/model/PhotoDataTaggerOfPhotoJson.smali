.class public final Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotoDataTaggerOfPhotoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/PhotoDataAlbumJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "album"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "isVideo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/CommonPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "ownerperson"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/CommonPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "taggeeperson"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/CommonPersonJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "taggerperson"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhotoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;->album:Lcom/google/api/services/plusi/model/PhotoDataAlbum;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;->isVideo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;->ownerperson:Lcom/google/api/services/plusi/model/CommonPerson;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;->taggeeperson:Lcom/google/api/services/plusi/model/CommonPerson;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;->taggerperson:Lcom/google/api/services/plusi/model/CommonPerson;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotoDataTaggerOfPhoto;-><init>()V

    return-object v0
.end method
