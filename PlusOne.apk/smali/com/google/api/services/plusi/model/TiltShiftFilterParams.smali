.class public final Lcom/google/api/services/plusi/model/TiltShiftFilterParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "TiltShiftFilterParams.java"


# instance fields
.field public blend:Ljava/lang/Float;

.field public blurStyle:Ljava/lang/Float;

.field public center:Lcom/google/api/services/plusi/model/VectorParamsVector2;

.field public diameterX:Ljava/lang/Float;

.field public diameterY:Ljava/lang/Float;

.field public filterStrength:Ljava/lang/Float;

.field public linear:Ljava/lang/Float;

.field public reverseVignette:Ljava/lang/Float;

.field public rotation:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
