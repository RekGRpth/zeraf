.class public final Lcom/google/api/services/plusi/model/PeopleViewPerson;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PeopleViewPerson.java"


# instance fields
.field public commonFriend:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            ">;"
        }
    .end annotation
.end field

.field public friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

.field public member:Lcom/google/api/services/plusi/model/DataCirclePerson;

.field public numberOfCommonFriends:Ljava/lang/Integer;

.field public score:Ljava/lang/Double;

.field public suggestionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
