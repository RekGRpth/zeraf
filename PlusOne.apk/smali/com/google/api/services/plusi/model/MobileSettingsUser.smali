.class public final Lcom/google/api/services/plusi/model/MobileSettingsUser;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MobileSettingsUser.java"


# instance fields
.field public info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

.field public isChild:Ljava/lang/Boolean;

.field public isPlusPage:Ljava/lang/Boolean;

.field public notGooglePlus:Ljava/lang/Boolean;

.field public plusPageInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
