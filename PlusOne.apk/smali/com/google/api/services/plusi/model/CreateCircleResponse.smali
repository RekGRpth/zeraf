.class public final Lcom/google/api/services/plusi/model/CreateCircleResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "CreateCircleResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public category:Ljava/lang/Integer;

.field public circleId:Lcom/google/api/services/plusi/model/DataCircleId;

.field public circlePerson:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            ">;"
        }
    .end annotation
.end field

.field public fbsVersionInfo:Ljava/lang/String;

.field public nameSortKey:Ljava/lang/String;

.field public revertCookie:Lcom/google/api/services/plusi/model/DataRevertCookie;

.field public versionInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
