.class public final Lcom/google/api/services/plusi/model/RenderContextJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "RenderContextJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/RenderContext;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/RenderContextJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RenderContextJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RenderContextJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/RenderContextJson;->INSTANCE:Lcom/google/api/services/plusi/model/RenderContextJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/RenderContext;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "deprecatedIsViewerLoggedIn"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "disableExplanation"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "disableHeader"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "disableReshare"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "disableVisibilityLink"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "isSummaryFormat"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/LayoutLayoutParamsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "layoutParams"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "location"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "moderationViewType"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "renderKey"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "showMuted"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "showSectionHeaders"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "showStreamModerationControls"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "showUnreadIndicator"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "streamId"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "viewerIsModerator"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/RenderContextJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/RenderContextJson;->INSTANCE:Lcom/google/api/services/plusi/model/RenderContextJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/RenderContext;

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->deprecatedIsViewerLoggedIn:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->disableExplanation:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->disableHeader:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->disableReshare:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->disableVisibilityLink:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->isSummaryFormat:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->layoutParams:Lcom/google/api/services/plusi/model/LayoutLayoutParams;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->location:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->moderationViewType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->renderKey:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->showMuted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->showSectionHeaders:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->showStreamModerationControls:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->showUnreadIndicator:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->streamId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderContext;->viewerIsModerator:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RenderContext;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RenderContext;-><init>()V

    return-object v0
.end method
