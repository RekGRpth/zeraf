.class public final Lcom/google/api/services/plusi/model/ExampleObject;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ExampleObject.java"


# instance fields
.field public about:Lcom/google/api/services/plusi/model/ExampleObject;

.field public additionalName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public attendee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ExampleObjectAttendee;",
            ">;"
        }
    .end annotation
.end field

.field public author:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ExampleObject;",
            ">;"
        }
    .end annotation
.end field

.field public booleanExtensionField:Ljava/lang/Boolean;

.field public contributor:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedClientItem;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public forClientOnly:Ljava/lang/String;

.field public forOwnerOnly:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public itemExtensionField:Lcom/google/api/services/plusi/model/ExampleObject;

.field public itemRepeatedExtensionField:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ExampleObject;",
            ">;"
        }
    .end annotation
.end field

.field public location:Lcom/google/api/services/plusi/model/Place;

.field public longExtensionField:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public stringExtensionField:Ljava/lang/String;

.field public stringRepeatedExtensionField:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public thumbnail:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
