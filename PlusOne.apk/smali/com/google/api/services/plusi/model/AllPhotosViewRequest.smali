.class public final Lcom/google/api/services/plusi/model/AllPhotosViewRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AllPhotosViewRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public maxNumberOfAlbums:Ljava/lang/Integer;

.field public resumeToken:Ljava/lang/String;

.field public viewOptions:Lcom/google/api/services/plusi/model/RequestsPhotosViewOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
