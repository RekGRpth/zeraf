.class public final Lcom/google/api/services/plusi/model/DataDiscoverySettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataDiscoverySettings.java"


# instance fields
.field public email:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;",
            ">;"
        }
    .end annotation
.end field

.field public phone:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryPhone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
