.class public final Lcom/google/api/services/plusi/model/SquareResult;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SquareResult.java"


# instance fields
.field public displayName:Ljava/lang/String;

.field public dominant:Ljava/lang/Boolean;

.field public memberCount:Ljava/lang/Long;

.field public peopleInCommon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareResultSquareMember;",
            ">;"
        }
    .end annotation
.end field

.field public photoUrl:Ljava/lang/String;

.field public postCount:Ljava/lang/Long;

.field public privatePosts:Ljava/lang/Boolean;

.field public snippetHtml:Ljava/lang/String;

.field public squareId:Lcom/google/api/services/plusi/model/SquareId;

.field public squareOwner:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareResultSquareMember;",
            ">;"
        }
    .end annotation
.end field

.field public tagline:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
