.class public final Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoadSocialNetworkRequestPersonListOptions.java"


# instance fields
.field public deprecatedCircleIdsToFilter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public includeExtendedProfileInfo:Ljava/lang/Boolean;

.field public includeMobileOidMappings:Ljava/lang/Boolean;

.field public includePeople:Ljava/lang/Boolean;

.field public maxPeople:Ljava/lang/Integer;

.field public profileTypesToFilter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public syncStateToken:Lcom/google/api/services/plusi/model/DataSyncStateToken;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
