.class public final Lcom/google/api/services/plusi/model/Offer;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Offer.java"


# instance fields
.field public availabilityEnds:Ljava/lang/String;

.field public availabilityStarts:Ljava/lang/String;

.field public availableAtOrFrom:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedClientItem;",
            ">;"
        }
    .end annotation
.end field

.field public localizedValidThrough:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public offerType:Ljava/lang/String;

.field public previewUrl:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public seller:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public url:Ljava/lang/String;

.field public validFrom:Ljava/lang/String;

.field public validThrough:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
