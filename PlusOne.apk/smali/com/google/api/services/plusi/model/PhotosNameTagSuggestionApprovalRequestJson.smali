.class public final Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosNameTagSuggestionApprovalRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "approve"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "shapeId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "taggeeId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "taggeeName"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->approve:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->photoId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->shapeId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->taggeeId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->taggeeName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;-><init>()V

    return-object v0
.end method
