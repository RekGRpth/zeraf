.class public final Lcom/google/api/services/plusi/model/AppInvite;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AppInvite.java"


# instance fields
.field public about:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public attribution:Lcom/google/api/services/plusi/model/Attribution;

.field public callToAction:Lcom/google/api/services/plusi/model/DeepLink;

.field public deleted9:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public isPreview:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public proxiedFaviconUrl:Ljava/lang/String;

.field public proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

.field public text:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
