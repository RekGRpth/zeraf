.class public final Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DynamicSkinSoftenerFilterParamsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "advancedColorBlue"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "advancedColorGreen"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "advancedColorRed"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "colorReach"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "detailsLarge"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "detailsMedium"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "detailsSmall"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParamsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->advancedColorBlue:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->advancedColorGreen:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->advancedColorRed:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->colorReach:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->detailsLarge:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->detailsMedium:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;->detailsSmall:Ljava/lang/Float;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DynamicSkinSoftenerFilterParams;-><init>()V

    return-object v0
.end method
