.class public final Lcom/google/api/services/plusi/model/OzDataSettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "OzDataSettings.java"


# instance fields
.field public birthdaySettings:Lcom/google/api/services/plusi/model/DataBirthdaySettings;

.field public chatSettings:Lcom/google/api/services/plusi/model/DataChatSettings;

.field public discoverySettings:Lcom/google/api/services/plusi/model/DataDiscoverySettings;

.field public gadgetsSettings:Lcom/google/api/services/plusi/model/DataGadgetsSettings;

.field public huddleSettings:Lcom/google/api/services/plusi/model/DataHuddleSettings;

.field public mobileSettings:Lcom/google/api/services/plusi/model/DataMobileSettings;

.field public notificationSettings:Lcom/google/api/services/plusi/model/DataNotificationSettings;

.field public plusActionsSettings:Lcom/google/api/services/plusi/model/DataPlusActionsSettings;

.field public socialAds:Lcom/google/api/services/plusi/model/DataSocialAds;

.field public squaresUserSettings:Lcom/google/api/services/plusi/model/SquaresUserSettings;

.field public whoCanCommentSettings:Lcom/google/api/services/plusi/model/DataWhoCanCommentSettings;

.field public whoCanInterruptSettings:Lcom/google/api/services/plusi/model/DataWhoCanInterruptSettings;

.field public whoCanNotifyGamesSettings:Lcom/google/api/services/plusi/model/DataWhoCanNotifySettings;

.field public whoCanNotifySettings:Lcom/google/api/services/plusi/model/DataWhoCanNotifySettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
