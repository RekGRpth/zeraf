.class public final Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoadSocialNetworkResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public personList:Lcom/google/api/services/plusi/model/DataPersonList;

.field public systemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;

.field public viewerCircles:Lcom/google/api/services/plusi/model/DataViewerCircles;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
