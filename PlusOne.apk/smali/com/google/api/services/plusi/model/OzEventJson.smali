.class public final Lcom/google/api/services/plusi/model/OzEventJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OzEventJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/OzEvent;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/OzEventJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OzEventJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OzEventJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/OzEventJson;->INSTANCE:Lcom/google/api/services/plusi/model/OzEventJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/OzEvent;

    const/16 v1, 0x15

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ActionTargetJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "actionTarget"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "background"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "badTiming"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/OutputDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "endViewData"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "favaDiagnostics"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/InputDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "inputData"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/InterstitialRedirectorDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "interstitialRedirectorData"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "isNativePlatformEvent"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "overallUserSegment"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "serverLoggedSuggestions"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/OutputDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "startViewData"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "thirdPartyAppName"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, Lcom/google/api/services/plusi/model/OzEventJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "timestampUsecDelta"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/OzEventJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/OzEventJson;->INSTANCE:Lcom/google/api/services/plusi/model/OzEventJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/OzEvent;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->background:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->badTiming:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->endViewData:Lcom/google/api/services/plusi/model/OutputData;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->inputData:Lcom/google/api/services/plusi/model/InputData;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->interstitialRedirectorData:Lcom/google/api/services/plusi/model/InterstitialRedirectorData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->isNativePlatformEvent:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->overallUserSegment:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->serverLoggedSuggestions:Lcom/google/api/services/plusi/model/LoggedSuggestionSet;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->startViewData:Lcom/google/api/services/plusi/model/OutputData;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->thirdPartyAppName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzEvent;->timestampUsecDelta:Ljava/lang/Long;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OzEvent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OzEvent;-><init>()V

    return-object v0
.end method
