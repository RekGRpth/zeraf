.class public final Lcom/google/api/services/plusi/model/LayoutOptionsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LayoutOptionsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LayoutOptions;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LayoutOptionsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LayoutOptionsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LayoutOptionsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LayoutOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/LayoutOptionsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LayoutOptions;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ElasticityJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "elasticity"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LayoutOptionsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LayoutOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/LayoutOptionsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LayoutOptions;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LayoutOptions;->elasticity:Lcom/google/api/services/plusi/model/Elasticity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LayoutOptions;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LayoutOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LayoutOptions;-><init>()V

    return-object v0
.end method
