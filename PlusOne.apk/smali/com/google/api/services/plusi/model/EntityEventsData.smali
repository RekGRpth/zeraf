.class public final Lcom/google/api/services/plusi/model/EntityEventsData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityEventsData.java"


# instance fields
.field public accessRequest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntityEventsDataAccessRequest;",
            ">;"
        }
    .end annotation
.end field

.field public accessRequestPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field public changedFields:Lcom/google/api/services/plusi/model/EntityEventsDataChangedFields;

.field public deletedPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field public eventActivityId:Ljava/lang/String;

.field public inviteeSummary:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/InviteeSummary;",
            ">;"
        }
    .end annotation
.end field

.field public photoUploaders:Lcom/google/api/services/plusi/model/EntityEventsDataPeopleList;

.field public plusPhotoAlbum:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

.field public renderEventsData:Lcom/google/api/services/plusi/model/EntityEventsDataRenderEventsData;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
