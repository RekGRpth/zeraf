.class public final Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GoogleReviewsRequestProto.java"


# instance fields
.field public commonOptions:Lcom/google/api/services/plusi/model/CommonReviewOptionsProto;

.field public qualityScoreThreshold:Ljava/lang/Double;

.field public requestOwnerResponses:Ljava/lang/Boolean;

.field public requestZagatReviews:Ljava/lang/Boolean;

.field public sortBy:Ljava/lang/String;

.field public suppressRatingOnlyReviews:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
