.class public final Lcom/google/api/services/plusi/model/VolumeControlMap;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "VolumeControlMap.java"


# instance fields
.field public setting:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/VolumeControlPair;",
            ">;"
        }
    .end annotation
.end field

.field public volumePair:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/VolumeControlVolumePair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
