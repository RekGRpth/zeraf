.class public final Lcom/google/api/services/plusi/model/DataImportInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataImportInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataImportInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataImportInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataImportInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataImportInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataImportInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataImportInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataImportInfo;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "groupId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "iconUrl"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataImportInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataImportInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataImportInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataImportInfo;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataImportInfo;->groupId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataImportInfo;->iconUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataImportInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataImportInfo;-><init>()V

    return-object v0
.end method
