.class public final Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "TuneImageFilterParamsTuneImageFilterControlPointParams.java"


# instance fields
.field public brightness:Ljava/lang/Float;

.field public center:Lcom/google/api/services/plusi/model/VectorParamsVector2;

.field public contrast:Ljava/lang/Float;

.field public diameter:Ljava/lang/Float;

.field public inking:Ljava/lang/Float;

.field public saturation:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
