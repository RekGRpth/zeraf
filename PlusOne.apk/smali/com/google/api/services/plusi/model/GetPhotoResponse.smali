.class public final Lcom/google/api/services/plusi/model/GetPhotoResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetPhotoResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public isDownloadable:Ljava/lang/Boolean;

.field public photo:Lcom/google/api/services/plusi/model/DataPhoto;

.field public photoTile:Lcom/google/api/services/plusi/model/Tile;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
