.class public final Lcom/google/api/services/plusi/model/ListResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ListResponse.java"


# instance fields
.field public almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

.field public errorCode:Ljava/lang/Integer;

.field public experimentNames:Ljava/lang/String;

.field public listType:Ljava/lang/String;

.field public people:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
            ">;"
        }
    .end annotation
.end field

.field public retrySeconds:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
