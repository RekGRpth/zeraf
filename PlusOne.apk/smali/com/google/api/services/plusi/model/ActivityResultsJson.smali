.class public final Lcom/google/api/services/plusi/model/ActivityResultsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActivityResultsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ActivityResults;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ActivityResultsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityResultsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityResultsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ActivityResultsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityResultsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ActivityResults;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityCount"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/ActivityRealTimeInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "realTimeInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/RenderContextJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "renderContext"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "shownActivitiesBlob"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/StreamJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "stream"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ActivityResultsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ActivityResultsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityResultsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ActivityResults;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityResults;->activityCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityResults;->realTimeInfo:Lcom/google/api/services/plusi/model/ActivityRealTimeInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityResults;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityResults;->shownActivitiesBlob:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityResults;->stream:Lcom/google/api/services/plusi/model/Stream;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityResults;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityResults;-><init>()V

    return-object v0
.end method
