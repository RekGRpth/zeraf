.class public final Lcom/google/api/services/plusi/model/DetailProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DetailProto.java"


# instance fields
.field public attribution:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public canonicalLabelId:Ljava/lang/String;

.field public displayLabel:Ljava/lang/String;

.field public hoverAttribution:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public value:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DetailProtoValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
