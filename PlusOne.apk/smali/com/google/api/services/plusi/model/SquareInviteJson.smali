.class public final Lcom/google/api/services/plusi/model/SquareInviteJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SquareInviteJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SquareInvite;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SquareInviteJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareInviteJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareInviteJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SquareInviteJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareInviteJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/SquareInvite;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "communityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "deprecated4"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "deprecated7"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "deprecated8"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "imageUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ThumbnailJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "proxiedImage"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "relativeUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "url"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SquareInviteJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/SquareInviteJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareInviteJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/SquareInvite;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->communityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->deprecated4:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->deprecated7:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->deprecated8:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->imageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->relativeUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareInvite;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareInvite;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareInvite;-><init>()V

    return-object v0
.end method
