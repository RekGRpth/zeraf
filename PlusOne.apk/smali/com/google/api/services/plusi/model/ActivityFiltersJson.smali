.class public final Lcom/google/api/services/plusi/model/ActivityFiltersJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActivityFiltersJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ActivityFilters;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ActivityFiltersJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityFiltersJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityFiltersJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ActivityFiltersJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityFiltersJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ActivityFilters;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "fieldRequestOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "maxNumImages"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "skipCommentCollapsing"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/StreamItemFilterJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "streamItemFilter"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/UpdateFilterJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "updateFilter"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ActivityFiltersJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ActivityFiltersJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityFiltersJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ActivityFilters;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityFilters;->maxNumImages:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityFilters;->skipCommentCollapsing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityFilters;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityFilters;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityFilters;-><init>()V

    return-object v0
.end method
