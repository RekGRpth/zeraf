.class public final Lcom/google/api/services/plusi/model/SettingsSetResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SettingsSetResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public success:Ljava/lang/Boolean;

.field public whoCanCommentResponse:Lcom/google/api/services/plusi/model/SetResponseWhoCanCommentResponse;

.field public whoCanNotifyGamesResponse:Lcom/google/api/services/plusi/model/SetResponseWhoCanNotifyGamesResponse;

.field public whoCanNotifyResponse:Lcom/google/api/services/plusi/model/SetResponseWhoCanNotifyResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
