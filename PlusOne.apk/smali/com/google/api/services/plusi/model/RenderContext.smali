.class public final Lcom/google/api/services/plusi/model/RenderContext;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "RenderContext.java"


# instance fields
.field public deprecatedIsViewerLoggedIn:Ljava/lang/Boolean;

.field public disableExplanation:Ljava/lang/Boolean;

.field public disableHeader:Ljava/lang/Boolean;

.field public disableReshare:Ljava/lang/Boolean;

.field public disableVisibilityLink:Ljava/lang/Boolean;

.field public isSummaryFormat:Ljava/lang/Boolean;

.field public layoutParams:Lcom/google/api/services/plusi/model/LayoutLayoutParams;

.field public location:Ljava/lang/String;

.field public moderationViewType:Ljava/lang/String;

.field public renderKey:Ljava/lang/String;

.field public showMuted:Ljava/lang/Boolean;

.field public showSectionHeaders:Ljava/lang/Boolean;

.field public showStreamModerationControls:Ljava/lang/Boolean;

.field public showUnreadIndicator:Ljava/lang/Boolean;

.field public streamId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public viewerIsModerator:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
