.class public final Lcom/google/api/services/plusi/model/SquareInvite;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SquareInvite.java"


# instance fields
.field public communityId:Ljava/lang/String;

.field public deprecated4:Ljava/lang/String;

.field public deprecated7:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deprecated8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

.field public relativeUrl:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
