.class public final Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "TuneImageFilterParamsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/TuneImageFilterParams;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/TuneImageFilterParams;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "ambience"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "brightness"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "contrast"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "controlPoints"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "fillLight"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "hue"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "saturation"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "tint"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "warmth"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->ambience:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->brightness:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->contrast:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->controlPoints:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->fillLight:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->hue:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->saturation:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->tint:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParams;->warmth:Ljava/lang/Float;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/TuneImageFilterParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TuneImageFilterParams;-><init>()V

    return-object v0
.end method
