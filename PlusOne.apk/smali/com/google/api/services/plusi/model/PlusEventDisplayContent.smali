.class public final Lcom/google/api/services/plusi/model/PlusEventDisplayContent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusEventDisplayContent.java"


# instance fields
.field public audience:Lcom/google/api/services/plusi/model/PlusEventAudience;

.field public descriptionHtml:Ljava/lang/String;

.field public descriptionSegments:Lcom/google/api/services/plusi/model/ViewSegments;

.field public eventTimeRange:Ljava/lang/String;

.field public eventTimeRangeShort:Ljava/lang/String;

.field public eventTimeStart:Ljava/lang/String;

.field public isEventOver:Ljava/lang/Boolean;

.field public iso8601EventTimeStart:Ljava/lang/String;

.field public socialSummary:Lcom/google/api/services/plusi/model/PlusEventSocialSummary;

.field public videoEmbedUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
