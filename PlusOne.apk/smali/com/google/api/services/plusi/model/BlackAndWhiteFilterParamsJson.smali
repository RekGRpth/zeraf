.class public final Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "BlackAndWhiteFilterParamsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "brightness"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "contrast"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "grainSliderSoftness"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "grainSliderStrength"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "style"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParamsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;->brightness:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;->contrast:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;->grainSliderSoftness:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;->grainSliderStrength:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;->style:Ljava/lang/Float;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;-><init>()V

    return-object v0
.end method
