.class public final Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusPhotosAddedToCollection.java"


# instance fields
.field public associatedMedia:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusPhotoAlbum;",
            ">;"
        }
    .end annotation
.end field

.field public associatedMediaDisplay:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

.field public collectionId:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ownerObfuscatedId:Ljava/lang/String;

.field public plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
