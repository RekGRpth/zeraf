.class public final Lcom/google/api/services/plusi/model/ChatAclSettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ChatAclSettings.java"


# instance fields
.field public customCircleLevel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/CircleChatLevel;",
            ">;"
        }
    .end annotation
.end field

.field public extendedCirclesLevel:Ljava/lang/String;

.field public inviteNotificationsEnabled:Ljava/lang/Boolean;

.field public publicLevel:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
