.class public final Lcom/google/api/services/plusi/model/Promo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Promo.java"


# instance fields
.field public allowDismiss:Ljava/lang/Boolean;

.field public bulkCircleData:Lcom/google/api/services/plusi/model/LoadBulkCircleDataResponse;

.field public celebritySuggestions:Lcom/google/api/services/plusi/model/CelebritySuggestionsResponse;

.field public disposition:Ljava/lang/String;

.field public findFriendsPromoMessage:Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

.field public isOptional:Ljava/lang/Boolean;

.field public qualitySignals:Lcom/google/api/services/plusi/model/QualitySignals;

.field public response:Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;

.field public results:Lcom/google/api/services/plusi/model/TrendResults;

.field public suggestedCelebritiesPromoData:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;

.field public suggestedCelebritiesPromoMessage:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;

.field public suggestedPeoplePromoData:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;

.field public suggestedPeoplePromoMessage:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
