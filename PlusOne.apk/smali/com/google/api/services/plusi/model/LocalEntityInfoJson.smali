.class public final Lcom/google/api/services/plusi/model/LocalEntityInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LocalEntityInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LocalEntityInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LocalEntityInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LocalEntityInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LocalEntityInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LocalEntityInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/LocalEntityInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/BusinessHoursFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "businessHours"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/LocalCategoriesJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "categories"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "cid"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/LocalFieldsWithDiffJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "localFieldsWithDiff"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/SearchContextJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "localSearchContext"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/FrontendPaperProtoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "paper"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "photoUrlFromGeo"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/PriceRangeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "priceRange"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "verificationStatus"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LocalEntityInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LocalEntityInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/LocalEntityInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->businessHours:Lcom/google/api/services/plusi/model/BusinessHoursField;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->categories:Lcom/google/api/services/plusi/model/LocalCategories;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->cid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->localFieldsWithDiff:Lcom/google/api/services/plusi/model/LocalFieldsWithDiff;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->localSearchContext:Lcom/google/api/services/plusi/model/SearchContext;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->photoUrlFromGeo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->priceRange:Lcom/google/api/services/plusi/model/PriceRangeField;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->verificationStatus:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LocalEntityInfo;-><init>()V

    return-object v0
.end method
