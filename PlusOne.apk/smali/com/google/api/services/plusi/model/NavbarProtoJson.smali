.class public final Lcom/google/api/services/plusi/model/NavbarProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "NavbarProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/NavbarProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/NavbarProtoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/NavbarProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NavbarProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/NavbarProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/NavbarProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/NavbarProto;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "baseNavUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "nextPageUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "numItems"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "numValidNeeded"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numValidResults"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "prevPageUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "start"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/NavbarProtoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/NavbarProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/NavbarProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/NavbarProto;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->baseNavUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->nextPageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->numItems:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->numValidNeeded:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->numValidResults:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->prevPageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NavbarProto;->start:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/NavbarProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NavbarProto;-><init>()V

    return-object v0
.end method
