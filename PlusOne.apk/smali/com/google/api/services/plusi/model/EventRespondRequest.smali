.class public final Lcom/google/api/services/plusi/model/EventRespondRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EventRespondRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public deprecated1:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public eventId:Ljava/lang/String;

.field public eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

.field public fbsVersionInfo:Ljava/lang/String;

.field public invitationToken:Ljava/lang/String;

.field public location:Ljava/lang/String;

.field public numAdditionalGuests:Ljava/lang/Integer;

.field public offNetworkDisplayName:Ljava/lang/String;

.field public response:Ljava/lang/String;

.field public rsvpToken:Ljava/lang/String;

.field public squareId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
