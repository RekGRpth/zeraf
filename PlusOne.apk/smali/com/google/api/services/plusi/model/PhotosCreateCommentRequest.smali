.class public final Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosCreateCommentRequest.java"


# instance fields
.field public authkey:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public commentSegments:Lcom/google/api/services/plusi/model/EditSegments;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public obfuscatedOwnerId:Ljava/lang/String;

.field public photoId:Ljava/lang/Long;

.field public returnAllComments:Ljava/lang/Boolean;

.field public squareId:Ljava/lang/String;

.field public squareStreamId:Ljava/lang/String;

.field public timestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
