.class public final Lcom/google/api/services/plusi/model/AclDetails;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AclDetails.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public community:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedSquare;",
            ">;"
        }
    .end annotation
.end field

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public systemGroup:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
