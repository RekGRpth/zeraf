.class public final Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ProviderBlocksProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ProviderBlocksProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ProviderBlocksProto;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/PlacePageLinkJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "moreReviewsLink"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/NavbarProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "navbar"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ProviderBlockProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "providerBlock"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ResultsRangeProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "resultsRange"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/StoryTitleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "title"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ProviderBlocksProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;->moreReviewsLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;->navbar:Lcom/google/api/services/plusi/model/NavbarProto;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;->providerBlock:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;->resultsRange:Lcom/google/api/services/plusi/model/ResultsRangeProto;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ProviderBlocksProto;->title:Lcom/google/api/services/plusi/model/StoryTitle;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ProviderBlocksProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ProviderBlocksProto;-><init>()V

    return-object v0
.end method
