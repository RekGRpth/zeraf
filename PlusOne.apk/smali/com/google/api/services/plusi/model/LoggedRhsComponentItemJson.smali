.class public final Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedRhsComponentItemJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "col"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "connectSiteId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "gamesLabelId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "localWriteReviewClusterId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "person"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "row"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSquareJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "square"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->col:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->connectSiteId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->gamesLabelId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->localWriteReviewClusterId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->person:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->row:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->square:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;->suggestionInfo:Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;-><init>()V

    return-object v0
.end method
