.class public final Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientLoggedRhsComponent.java"


# instance fields
.field public barType:Ljava/lang/Integer;

.field public componentType:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;

.field public index:Ljava/lang/Integer;

.field public item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field public neighborInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;",
            ">;"
        }
    .end annotation
.end field

.field public promoGroupId:Ljava/lang/String;

.field public suggestionSummaryInfo:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionSummaryInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
