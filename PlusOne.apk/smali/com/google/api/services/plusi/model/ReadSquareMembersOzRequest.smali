.class public final Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReadSquareMembersOzRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public consistentRead:Ljava/lang/Boolean;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public fetchMembersOnly:Ljava/lang/Boolean;

.field public memberListQuery:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberListQuery;",
            ">;"
        }
    .end annotation
.end field

.field public obfuscatedSquareId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
