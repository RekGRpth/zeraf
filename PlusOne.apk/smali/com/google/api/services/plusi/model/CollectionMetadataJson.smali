.class public final Lcom/google/api/services/plusi/model/CollectionMetadataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "CollectionMetadataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/CollectionMetadata;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/CollectionMetadataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CollectionMetadataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CollectionMetadataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/CollectionMetadataJson;->INSTANCE:Lcom/google/api/services/plusi/model/CollectionMetadataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/CollectionMetadata;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/AudienceJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "audience"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "authKey"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "collectionId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/api/services/plusi/model/CollectionMetadataJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "photoCount"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "relativeUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "title"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/CollectionMetadataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/CollectionMetadataJson;->INSTANCE:Lcom/google/api/services/plusi/model/CollectionMetadataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/CollectionMetadata;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->audience:Lcom/google/api/services/plusi/model/Audience;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->authKey:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->collectionId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->photoCount:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->relativeUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CollectionMetadata;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CollectionMetadata;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CollectionMetadata;-><init>()V

    return-object v0
.end method
