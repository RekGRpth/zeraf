.class public final Lcom/google/api/services/plusi/model/GetSquaresOzResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetSquaresOzResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public invitedSquare:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/InvitedSquare;",
            ">;"
        }
    .end annotation
.end field

.field public joinedSquare:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/JoinedSquare;",
            ">;"
        }
    .end annotation
.end field

.field public squaresMembership:Lcom/google/api/services/plusi/model/SquaresMembership;

.field public suggestedSquare:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SuggestedSquare;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
