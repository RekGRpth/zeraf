.class public final Lcom/google/api/services/plusi/model/UploadMediaRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "UploadMediaRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/UploadMediaRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/UploadMediaRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/UploadMediaRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    const/16 v1, 0x16

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "albumId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "albumLabel"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "albumTitle"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "autoResize"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "clientAssignedId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinateJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "coordinate"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "description"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "displayName"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "eventId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/LocalDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "localData"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "offset"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "rotation"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plusi/model/ScottyMediaJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "scottyMedia"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "setProfilePhoto"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/UploadMediaRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/UploadMediaRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->autoResize:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->clientAssignedId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->displayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->eventId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->localData:Lcom/google/api/services/plusi/model/LocalData;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->offset:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->rotation:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->scottyMedia:Lcom/google/api/services/plusi/model/ScottyMedia;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->setProfilePhoto:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UploadMediaRequest;-><init>()V

    return-object v0
.end method
