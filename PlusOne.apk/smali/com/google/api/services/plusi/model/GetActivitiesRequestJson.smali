.class public final Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GetActivitiesRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "continuesToken"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/ClientEmbedOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "embedOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "includeLayout"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "includeSharebox"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "isInitialStreamFetch"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isUserInitiated"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/LayoutConfigJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "layoutConfig"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "perspectiveId"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "skipPopularMixin"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/StreamParamsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "streamParams"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->continuesToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->includeLayout:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->includeSharebox:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->isInitialStreamFetch:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->isUserInitiated:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->layoutConfig:Lcom/google/api/services/plusi/model/LayoutConfig;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->perspectiveId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->skipPopularMixin:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetActivitiesRequest;-><init>()V

    return-object v0
.end method
