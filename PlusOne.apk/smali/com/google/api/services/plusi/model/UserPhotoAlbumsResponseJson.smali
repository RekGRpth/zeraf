.class public final Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "UserPhotoAlbumsResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAlbumJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "aggregateAlbum"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/TileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "albumTile"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/DataAlbumJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "nonAggregateAlbum"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "offset"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/DataUserJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "owner"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "resumeToken"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->aggregateAlbum:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->albumTile:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->nonAggregateAlbum:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->offset:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->owner:Lcom/google/api/services/plusi/model/DataUser;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->resumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;-><init>()V

    return-object v0
.end method
