.class public final Lcom/google/android/gms/internal/ag;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/wallet/Address;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a$80dd846(Lcom/google/android/gms/wallet/Address;Landroid/os/Parcel;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/gms/internal/ac;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/wallet/Address;->mVersionCode:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->name:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->N:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->O:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->P:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->Q:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->R:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->S:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->T:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->U:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/Address;->V:Z

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/wallet/Address;->W:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ac;->c(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    new-instance v0, Lcom/google/android/gms/wallet/Address;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/Address;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/bm;->g(Landroid/os/Parcel;)I

    move-result v1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-ge v2, v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const v3, 0xffff

    and-int/2addr v3, v2

    packed-switch v3, :pswitch_data_0

    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->e(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->h(Landroid/os/Parcel;I)I

    move-result v2

    iput v2, v0, Lcom/google/android/gms/wallet/Address;->mVersionCode:I

    goto :goto_0

    :pswitch_1
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->name:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->N:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->O:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->P:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->Q:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->R:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->S:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->T:Ljava/lang/String;

    goto :goto_0

    :pswitch_9
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->U:Ljava/lang/String;

    goto :goto_0

    :pswitch_a
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->f(Landroid/os/Parcel;I)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/Address;->V:Z

    goto :goto_0

    :pswitch_b
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/wallet/Address;->W:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-eq v2, v1, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/bm$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overread allowed size end="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/internal/bm$a;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/wallet/Address;

    return-object v0
.end method
