.class public final Lcom/google/android/gms/internal/aa$b;
.super Lcom/google/android/gms/internal/aa$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/aa",
        "<TT;>.c<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic ae:Lcom/google/android/gms/internal/aa;

.field public final bd:Landroid/os/Bundle;

.field public final be:Landroid/os/IBinder;

.field public final statusCode:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/aa;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/aa$c;-><init>(Lcom/google/android/gms/internal/aa;Ljava/lang/Object;)V

    iput p2, p0, Lcom/google/android/gms/internal/aa$b;->statusCode:I

    iput-object p3, p0, Lcom/google/android/gms/internal/aa$b;->be:Landroid/os/IBinder;

    iput-object p4, p0, Lcom/google/android/gms/internal/aa$b;->bd:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 4

    const/4 v3, 0x0

    check-cast p1, Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/internal/aa$b;->statusCode:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->bd:Landroid/os/Bundle;

    const-string v1, "pendingIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    iget v3, p0, Lcom/google/android/gms/internal/aa$b;->statusCode:I

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->be:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/aa;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GmsClient"

    const-string v1, "bound to service broker"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    iget-object v1, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    iget-object v2, p0, Lcom/google/android/gms/internal/aa$b;->be:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/aa;->j(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/internal/aa;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-static {v0}, Lcom/google/android/gms/internal/aa;->c(Lcom/google/android/gms/internal/aa;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/aa;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-static {v0}, Lcom/google/android/gms/internal/aa;->e(Lcom/google/android/gms/internal/aa;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-static {v1}, Lcom/google/android/gms/internal/aa;->d(Lcom/google/android/gms/internal/aa;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/internal/aa;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/internal/aa;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/aa$b;->ae:Lcom/google/android/gms/internal/aa;

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
