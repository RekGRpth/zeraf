.class final Lcom/google/android/gms/internal/m$d;
.super Lcom/google/android/gms/internal/aa$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/aa",
        "<",
        "Lcom/google/android/gms/internal/at;",
        ">.c<",
        "Lcom/google/android/gms/plus/PlusClient$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final bQ:Lcom/google/android/gms/common/ConnectionResult;

.field private final bR:Landroid/os/ParcelFileDescriptor;

.field final synthetic x:Lcom/google/android/gms/internal/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$a;Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/m$d;->x:Lcom/google/android/gms/internal/m;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/aa$c;-><init>(Lcom/google/android/gms/internal/aa;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/m$d;->bQ:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/gms/internal/m$d;->bR:Landroid/os/ParcelFileDescriptor;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/gms/plus/PlusClient$a;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/m$d;->bQ:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, Lcom/google/android/gms/internal/m$d;->bR:Landroid/os/ParcelFileDescriptor;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/plus/PlusClient$a;->a(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/m$d;->bR:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlusClientImpl"

    const-string v2, "failed close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final p()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/internal/aa$c;->p()V

    return-void
.end method
