.class Lcom/google/android/gms/internal/ad$c;
.super Landroid/widget/CompoundButton;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic as:Lcom/google/android/gms/internal/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/ad;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ad$c;->as:Lcom/google/android/gms/internal/ad;

    invoke-direct {p0, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public toggle()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$c;->as:Lcom/google/android/gms/internal/ad;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ad;->cz:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/CompoundButton;->toggle()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ad$c;->as:Lcom/google/android/gms/internal/ad;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/internal/ad;->cz:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$c;->as:Lcom/google/android/gms/internal/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ad;->Q()V

    goto :goto_0
.end method
