.class public final Lcom/google/android/gms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept_button:I = 0x7f0801f9

.field public static final accounts_list_fragment:I = 0x7f080053

.field public static final acl_check:I = 0x7f08020d

.field public static final acl_display:I = 0x7f080155

.field public static final acl_icon:I = 0x7f08020b

.field public static final acl_overlay:I = 0x7f08022b

.field public static final acl_text:I = 0x7f08020c

.field public static final action_bar_progress_spinner:I = 0x7f080306

.field public static final action_bar_progress_spinner_view:I = 0x7f08005e

.field public static final action_button:I = 0x7f0802b3

.field public static final action_button_1:I = 0x7f08014d

.field public static final action_button_2:I = 0x7f08014e

.field public static final add_item:I = 0x7f080239

.field public static final add_photo_button:I = 0x7f0801b5

.field public static final add_plus1:I = 0x7f080342

.field public static final add_to_circle_button:I = 0x7f0801c0

.field public static final add_to_circles_button:I = 0x7f0801db

.field public static final added_by_count:I = 0x7f080268

.field public static final address_content:I = 0x7f08024e

.field public static final album_name_layout:I = 0x7f08021f

.field public static final album_title:I = 0x7f080220

.field public static final album_view:I = 0x7f080209

.field public static final alert:I = 0x7f0801dd

.field public static final also_remove_checkbox:I = 0x7f080072

.field public static final also_remove_section:I = 0x7f080071

.field public static final also_report_checkbox:I = 0x7f080074

.field public static final also_report_section:I = 0x7f080073

.field public static final amount_divider:I = 0x7f08008d

.field public static final amount_item:I = 0x7f08008a

.field public static final amount_label:I = 0x7f08008b

.field public static final amount_value:I = 0x7f08008c

.field public static final audience:I = 0x7f08023b

.field public static final audience_button:I = 0x7f08006c

.field public static final audience_circle_name_loader_id:I = 0x7f08002a

.field public static final audience_container:I = 0x7f08022c

.field public static final audience_fragment:I = 0x7f080198

.field public static final audience_names_container:I = 0x7f08006e

.field public static final audience_overlay:I = 0x7f08015c

.field public static final audience_scroll_container:I = 0x7f080068

.field public static final audience_scrollview:I = 0x7f0802d2

.field public static final audience_separator:I = 0x7f080211

.field public static final audience_to_icon:I = 0x7f08006d

.field public static final audience_to_text:I = 0x7f08006a

.field public static final audience_view:I = 0x7f080064

.field public static final audience_view_root:I = 0x7f0802d1

.field public static final authorArrow:I = 0x7f080189

.field public static final authorName:I = 0x7f08018a

.field public static final author_avatar:I = 0x7f08027e

.field public static final author_name:I = 0x7f08027f

.field public static final author_name_and_date:I = 0x7f080174

.field public static final avatar:I = 0x7f08005c

.field public static final avatarFrame:I = 0x7f080185

.field public static final avatarFull:I = 0x7f0800b7

.field public static final avatarLeftFull:I = 0x7f0800b8

.field public static final avatarLowerLeft:I = 0x7f0800ba

.field public static final avatarLowerRight:I = 0x7f0800bc

.field public static final avatarPanel:I = 0x7f0800b6

.field public static final avatarUpperLeft:I = 0x7f0800b9

.field public static final avatarUpperRight:I = 0x7f0800bb

.field public static final avatar_image:I = 0x7f080187

.field public static final avatar_view:I = 0x7f0801b3

.field public static final background:I = 0x7f0801f3

.field public static final ban_user:I = 0x7f08032e

.field public static final bar_graph:I = 0x7f080192

.field public static final block:I = 0x7f08032a

.field public static final blocked:I = 0x7f0800f9

.field public static final blocked_circle:I = 0x7f080331

.field public static final blocking_explanation:I = 0x7f0802ac

.field public static final bottom_action_bar:I = 0x7f0801bf

.field public static final bottom_bar:I = 0x7f08016c

.field public static final bottom_spacer:I = 0x7f08025b

.field public static final brady_container:I = 0x7f080119

.field public static final button:I = 0x7f08020a

.field public static final buttonPanel:I = 0x7f0801a5

.field public static final button_container:I = 0x7f08015b

.field public static final button_disable:I = 0x7f0801af

.field public static final button_divider:I = 0x7f0801ab

.field public static final button_section:I = 0x7f0802a1

.field public static final button_wifi_and_mobile:I = 0x7f0801a8

.field public static final button_wifi_only:I = 0x7f0801ac

.field public static final buttons:I = 0x7f08026f

.field public static final call_button:I = 0x7f080275

.field public static final camera_icon:I = 0x7f0802e1

.field public static final cancel:I = 0x7f080078

.field public static final cancel_button:I = 0x7f080199

.field public static final cancel_send:I = 0x7f08018c

.field public static final caption_bar:I = 0x7f080206

.field public static final category_description:I = 0x7f0801d8

.field public static final category_name:I = 0x7f0801d7

.field public static final center_button:I = 0x7f08029b

.field public static final center_stage_video_container:I = 0x7f080111

.field public static final centered_text:I = 0x7f080215

.field public static final change_photo_button:I = 0x7f0801b4

.field public static final check_indicator:I = 0x7f0802c5

.field public static final check_mark_disable:I = 0x7f0801b1

.field public static final check_mark_wifi_and_mobile:I = 0x7f0801aa

.field public static final check_mark_wifi_only:I = 0x7f0801ae

.field public static final checkin:I = 0x7f0800e0

.field public static final chevron_icon:I = 0x7f080070

.field public static final choose_cover_photo_icon:I = 0x7f080264

.field public static final choose_media:I = 0x7f080224

.field public static final choose_photo_icon:I = 0x7f080266

.field public static final choose_view:I = 0x7f0802dc

.field public static final circle_activity:I = 0x7f080249

.field public static final circle_icon:I = 0x7f0801cc

.field public static final circle_logo:I = 0x7f0800fc

.field public static final circle_member_count:I = 0x7f0802dd

.field public static final circle_name:I = 0x7f0801cd

.field public static final circle_names:I = 0x7f080107

.field public static final circle_row:I = 0x7f0801cb

.field public static final circle_settings:I = 0x7f080330

.field public static final circles:I = 0x7f0800fd

.field public static final circles_button:I = 0x7f0801d9

.field public static final clear:I = 0x7f080340

.field public static final clear_text:I = 0x7f080221

.field public static final compose_bar:I = 0x7f08004b

.field public static final compose_button_bar:I = 0x7f08009b

.field public static final compose_custom:I = 0x7f08009e

.field public static final compose_divider_bar:I = 0x7f0800a0

.field public static final compose_image_bar:I = 0x7f0800a1

.field public static final compose_image_bar_close:I = 0x7f0800a4

.field public static final compose_image_bar_share:I = 0x7f0800a3

.field public static final compose_image_container:I = 0x7f0800a2

.field public static final compose_location:I = 0x7f08009d

.field public static final compose_message_fragment:I = 0x7f0800c6

.field public static final compose_photos:I = 0x7f08009c

.field public static final compose_post:I = 0x7f08009f

.field public static final compose_text:I = 0x7f080213

.field public static final contact:I = 0x7f08024b

.field public static final contacts_stats_sync_checkbox:I = 0x7f0801a1

.field public static final contacts_stats_sync_checkbox_description:I = 0x7f0801a3

.field public static final contacts_stats_sync_checkbox_link:I = 0x7f0801a4

.field public static final contacts_stats_sync_checkbox_title:I = 0x7f0801a2

.field public static final contacts_sync_checkbox:I = 0x7f08019e

.field public static final contacts_sync_checkbox_description:I = 0x7f0801a0

.field public static final contacts_sync_checkbox_title:I = 0x7f08019f

.field public static final contacts_sync_layout:I = 0x7f08019d

.field public static final contacts_sync_view:I = 0x7f0802db

.field public static final content:I = 0x7f080082

.field public static final content_line_1:I = 0x7f0802f1

.field public static final content_line_2:I = 0x7f0802f2

.field public static final content_multiline:I = 0x7f0802f0

.field public static final conversationName:I = 0x7f0800bd

.field public static final conversation_header:I = 0x7f0800c4

.field public static final conversation_rename_input:I = 0x7f0800c3

.field public static final conversation_tile:I = 0x7f0800ad

.field public static final count:I = 0x7f0801e4

.field public static final cover_photo_image:I = 0x7f08025d

.field public static final createButton:I = 0x7f080159

.field public static final createText:I = 0x7f08015a

.field public static final create_acl_button:I = 0x7f08005b

.field public static final current:I = 0x7f080234

.field public static final customize:I = 0x7f08033f

.field public static final date:I = 0x7f080157

.field public static final decline_invitation:I = 0x7f0802a6

.field public static final decline_invitation_divider:I = 0x7f0802a5

.field public static final decline_invitation_label:I = 0x7f0802a7

.field public static final decline_invitation_section:I = 0x7f0802a4

.field public static final default_acl_button:I = 0x7f080057

.field public static final delete:I = 0x7f080347

.field public static final delete_circle:I = 0x7f08032f

.field public static final delete_event:I = 0x7f080337

.field public static final delete_item:I = 0x7f080235

.field public static final delete_photos:I = 0x7f08033b

.field public static final delete_post:I = 0x7f080334

.field public static final desc:I = 0x7f0802d6

.field public static final description:I = 0x7f0800d7

.field public static final details:I = 0x7f08023c

.field public static final device_location:I = 0x7f080253

.field public static final device_location_map:I = 0x7f080255

.field public static final device_location_map_container:I = 0x7f080254

.field public static final device_location_text:I = 0x7f08025a

.field public static final device_location_title:I = 0x7f080259

.field public static final dialog_content:I = 0x7f0800de

.field public static final dialog_frame:I = 0x7f0801bd

.field public static final directions_button:I = 0x7f080273

.field public static final dismiss:I = 0x7f0802b2

.field public static final divider:I = 0x7f08007b

.field public static final divider_1:I = 0x7f0802e3

.field public static final divider_2:I = 0x7f0802e6

.field public static final domain_acl_button:I = 0x7f080055

.field public static final done:I = 0x7f080148

.field public static final done_button:I = 0x7f08022d

.field public static final done_title:I = 0x7f080149

.field public static final download:I = 0x7f080348

.field public static final edit:I = 0x7f080293

.field public static final edit_audience:I = 0x7f080069

.field public static final edit_audience_fragment:I = 0x7f0800c7

.field public static final edit_comment_fragment:I = 0x7f0800c8

.field public static final edit_event:I = 0x7f080336

.field public static final edit_post_fragment:I = 0x7f0800d8

.field public static final education:I = 0x7f080252

.field public static final email_content:I = 0x7f08024c

.field public static final emotishare_container:I = 0x7f080158

.field public static final emotishare_view:I = 0x7f0800d9

.field public static final employer:I = 0x7f08026d

.field public static final empty_conversation_text:I = 0x7f080183

.field public static final empty_conversation_view:I = 0x7f080182

.field public static final empty_emotishare:I = 0x7f08021d

.field public static final empty_link:I = 0x7f08021b

.field public static final empty_media:I = 0x7f08021a

.field public static final empty_media_container:I = 0x7f080219

.field public static final empty_message:I = 0x7f0801c7

.field public static final empty_progress:I = 0x7f080202

.field public static final empty_progress_text:I = 0x7f080203

.field public static final empty_text:I = 0x7f080201

.field public static final empty_view:I = 0x7f080200

.field public static final end:I = 0x7f080233

.field public static final end_date:I = 0x7f0800d0

.field public static final end_time:I = 0x7f0800d1

.field public static final error:I = 0x7f0801d3

.field public static final error_retry_button:I = 0x7f0802bd

.field public static final event_header_view:I = 0x7f08004a

.field public static final event_instant_share_notification:I = 0x7f080046

.field public static final event_instant_share_selection:I = 0x7f080048

.field public static final event_name:I = 0x7f0800cd

.field public static final event_rsvp_section:I = 0x7f080049

.field public static final event_theme_image:I = 0x7f0800c9

.field public static final event_theme_progress_bar:I = 0x7f0800cc

.field public static final expand:I = 0x7f080270

.field public static final explanation:I = 0x7f080076

.field public static final family_name:I = 0x7f08026b

.field public static final feedback:I = 0x7f080305

.field public static final film_strip:I = 0x7f080117

.field public static final filmstrip_container:I = 0x7f080115

.field public static final filmstrip_gradient:I = 0x7f080118

.field public static final filmstrip_scroll_container:I = 0x7f080116

.field public static final find_classmates:I = 0x7f0800ec

.field public static final find_classmates_img:I = 0x7f0800ed

.field public static final find_classmates_text:I = 0x7f0800ee

.field public static final find_coworkers:I = 0x7f0800e9

.field public static final find_coworkers_img:I = 0x7f0800ea

.field public static final find_coworkers_text:I = 0x7f0800eb

.field public static final floating_compose_bar:I = 0x7f0802b8

.field public static final focus_override:I = 0x7f080210

.field public static final footer:I = 0x7f0801f1

.field public static final footer_bar:I = 0x7f080172

.field public static final footer_divider:I = 0x7f0801f2

.field public static final footer_message:I = 0x7f08022a

.field public static final footer_post_button:I = 0x7f0800e8

.field public static final footer_separator:I = 0x7f080229

.field public static final footer_text:I = 0x7f0800e7

.field public static final fragment_container:I = 0x7f080143

.field public static final fragment_sliding_background:I = 0x7f080142

.field public static final frame_container:I = 0x7f08029d

.field public static final from_this_circle:I = 0x7f080088

.field public static final full_name:I = 0x7f080269

.field public static final gallery_image:I = 0x7f0800a5

.field public static final given_name:I = 0x7f08026a

.field public static final gray_spam_bar:I = 0x7f0802c0

.field public static final gray_spam_bar_text:I = 0x7f0802c1

.field public static final green_room_participants_view:I = 0x7f080130

.field public static final grid:I = 0x7f080156

.field public static final hangoutOnAirCheckbox:I = 0x7f080101

.field public static final hangoutOnAirMessage:I = 0x7f080100

.field public static final hangout_audio_muted_status:I = 0x7f080138

.field public static final hangout_audio_toggle_menu_item:I = 0x7f080311

.field public static final hangout_background_logo:I = 0x7f0800f6

.field public static final hangout_camera_error:I = 0x7f08013d

.field public static final hangout_check:I = 0x7f0800d3

.field public static final hangout_exit_menu_item:I = 0x7f08031b

.field public static final hangout_green_room_instructions:I = 0x7f08011a

.field public static final hangout_invite_menu_item:I = 0x7f08031c

.field public static final hangout_launch_join_button:I = 0x7f08011d

.field public static final hangout_launch_join_panel:I = 0x7f08011c

.field public static final hangout_launch_progress_bar:I = 0x7f08011e

.field public static final hangout_launch_progress_text:I = 0x7f08011f

.field public static final hangout_menu_common_exit:I = 0x7f0800f5

.field public static final hangout_menu_common_hangout_switch:I = 0x7f0800f1

.field public static final hangout_menu_common_switch_camera:I = 0x7f0800f4

.field public static final hangout_menu_common_toggle_audio_mute:I = 0x7f0800f2

.field public static final hangout_menu_common_toggle_video_mute:I = 0x7f0800f3

.field public static final hangout_menu_switch_camera:I = 0x7f080317

.field public static final hangout_menu_toggle_audio_mute:I = 0x7f080108

.field public static final hangout_menu_toggle_video_mute:I = 0x7f080109

.field public static final hangout_participants_info:I = 0x7f080129

.field public static final hangout_participants_view:I = 0x7f080131

.field public static final hangout_pinned_status:I = 0x7f08013a

.field public static final hangout_ringing_inviter_profile:I = 0x7f080103

.field public static final hangout_status_bar:I = 0x7f080137

.field public static final hangout_tablet_tile:I = 0x7f08010f

.field public static final hangout_tile_root_view:I = 0x7f080126

.field public static final hangout_toggle_flash_light_button:I = 0x7f08010e

.field public static final hangout_top_menu:I = 0x7f08012c

.field public static final hangout_transfer_menu_item:I = 0x7f080324

.field public static final hangout_video_avatar:I = 0x7f080136

.field public static final hangout_video_blocked:I = 0x7f08013b

.field public static final hangout_video_paused:I = 0x7f08013c

.field public static final hangout_video_snapshot:I = 0x7f080135

.field public static final hangout_video_toggle_menu_item:I = 0x7f080325

.field public static final hangout_video_view:I = 0x7f080134

.field public static final hangout_volume:I = 0x7f080139

.field public static final header:I = 0x7f08023e

.field public static final header_device_location:I = 0x7f080258

.field public static final help:I = 0x7f080236

.field public static final help_details:I = 0x7f080238

.field public static final help_spacer:I = 0x7f0800b3

.field public static final help_title:I = 0x7f080237

.field public static final hint_header:I = 0x7f0802fd

.field public static final hint_ok:I = 0x7f080303

.field public static final hint_table_row1:I = 0x7f0802fe

.field public static final hint_table_row2:I = 0x7f0802ff

.field public static final hint_table_row3:I = 0x7f080300

.field public static final hint_table_row4:I = 0x7f080301

.field public static final hint_table_row5:I = 0x7f080302

.field public static final hint_title:I = 0x7f0802fc

.field public static final home_activity_dialog_new_features:I = 0x7f080035

.field public static final home_activity_dialog_sign_out_confirm:I = 0x7f080034

.field public static final home_activity_dialog_sign_out_pending:I = 0x7f080033

.field public static final home_icon:I = 0x7f0802df

.field public static final homepage:I = 0x7f080245

.field public static final homepage_header:I = 0x7f080246

.field public static final horizontal:I = 0x7f080008

.field public static final host:I = 0x7f08013e

.field public static final hosted_content:I = 0x7f080141

.field public static final huddle_help_text:I = 0x7f0800b5

.field public static final icon:I = 0x7f08012a

.field public static final icon_container:I = 0x7f080147

.field public static final image:I = 0x7f0800a7

.field public static final image_container:I = 0x7f0800a6

.field public static final image_frame:I = 0x7f08018f

.field public static final image_label:I = 0x7f0800dc

.field public static final image_view:I = 0x7f0800db

.field public static final in_circles_button:I = 0x7f0801da

.field public static final incomingCallWidget:I = 0x7f08010a

.field public static final info:I = 0x7f08026c

.field public static final info_desc:I = 0x7f080052

.field public static final info_header:I = 0x7f0801bb

.field public static final info_title:I = 0x7f080051

.field public static final initiate_sharing_bar:I = 0x7f0801f8

.field public static final input:I = 0x7f08029e

.field public static final inset:I = 0x7f08010b

.field public static final inset_container:I = 0x7f080112

.field public static final inset_video_container:I = 0x7f080113

.field public static final intro:I = 0x7f08023f

.field public static final invitation_acl_and_block:I = 0x7f08016f

.field public static final invitation_fragment:I = 0x7f08016a

.field public static final invitation_message_text:I = 0x7f08016b

.field public static final invitation_text:I = 0x7f0802b1

.field public static final invite_button:I = 0x7f0802a3

.field public static final invite_more:I = 0x7f080338

.field public static final invite_participants:I = 0x7f08012f

.field public static final invitee_list_fragment:I = 0x7f0800e1

.field public static final invitees_container:I = 0x7f080120

.field public static final invitees_message:I = 0x7f080123

.field public static final invitees_message_container:I = 0x7f080122

.field public static final invitees_view:I = 0x7f080121

.field public static final inviter_avatar:I = 0x7f080105

.field public static final inviter_name:I = 0x7f080106

.field public static final join_button:I = 0x7f0802a2

.field public static final just_following_checkbox:I = 0x7f08007d

.field public static final just_following_layout:I = 0x7f08007c

.field public static final known_for_terms_row:I = 0x7f080277

.field public static final known_for_terms_value:I = 0x7f080278

.field public static final label:I = 0x7f080170

.field public static final lastMessage:I = 0x7f0800bf

.field public static final layout:I = 0x7f080231

.field public static final leave_square:I = 0x7f080098

.field public static final leave_square_section:I = 0x7f080097

.field public static final left_button:I = 0x7f08029c

.field public static final link:I = 0x7f0800df

.field public static final link_background:I = 0x7f0802ea

.field public static final link_content:I = 0x7f080247

.field public static final link_overlay:I = 0x7f0802eb

.field public static final link_text:I = 0x7f080162

.field public static final link_title:I = 0x7f0802ec

.field public static final links:I = 0x7f080257

.field public static final list:I = 0x7f0801c8

.field public static final list_container:I = 0x7f08017f

.field public static final list_empty_progress:I = 0x7f0800b0

.field public static final list_empty_progress_bar:I = 0x7f0800b1

.field public static final list_empty_progress_text:I = 0x7f0800b2

.field public static final list_empty_text:I = 0x7f0800af

.field public static final list_expander:I = 0x7f0801ef

.field public static final list_header:I = 0x7f080067

.field public static final list_layout_parent:I = 0x7f080066

.field public static final list_parent:I = 0x7f0801ee

.field public static final loading:I = 0x7f0801d1

.field public static final loading_spinner:I = 0x7f0802c2

.field public static final local_acl_button1:I = 0x7f080058

.field public static final local_acl_button2:I = 0x7f080059

.field public static final local_actions:I = 0x7f080240

.field public static final local_details:I = 0x7f080242

.field public static final local_details_content:I = 0x7f080244

.field public static final local_details_header:I = 0x7f080243

.field public static final local_review_icon:I = 0x7f08017a

.field public static final local_review_item:I = 0x7f080176

.field public static final local_reviews:I = 0x7f08024a

.field public static final location:I = 0x7f08026e

.field public static final locationAndDetails:I = 0x7f080276

.field public static final location_container:I = 0x7f0800d4

.field public static final location_marker:I = 0x7f0800d5

.field public static final location_marker_progress_container:I = 0x7f080216

.field public static final location_off:I = 0x7f0802b9

.field public static final location_off_settings:I = 0x7f0802bb

.field public static final location_off_text:I = 0x7f0802ba

.field public static final location_progress:I = 0x7f080217

.field public static final location_text:I = 0x7f0800d6

.field public static final location_view:I = 0x7f080214

.field public static final longDivider:I = 0x7f080290

.field public static final main:I = 0x7f0802b4

.field public static final main_video:I = 0x7f080127

.field public static final map:I = 0x7f080079

.field public static final map_button:I = 0x7f080271

.field public static final map_view:I = 0x7f08017e

.field public static final maybeButton:I = 0x7f0800e3

.field public static final maybeDivider:I = 0x7f0800e4

.field public static final media_buttons_layout:I = 0x7f080223

.field public static final media_count:I = 0x7f080225

.field public static final media_upload_row:I = 0x7f0802d3

.field public static final member_count:I = 0x7f0801ce

.field public static final membership_status:I = 0x7f0802b5

.field public static final mention_scroll_view:I = 0x7f08020f

.field public static final menu:I = 0x7f080154

.field public static final menu_avatar_profile:I = 0x7f08030d

.field public static final menu_discard:I = 0x7f080310

.field public static final menu_hangout_avatar_block:I = 0x7f080316

.field public static final menu_hangout_avatar_pin_video:I = 0x7f080313

.field public static final menu_hangout_avatar_profile:I = 0x7f080312

.field public static final menu_hangout_avatar_remote_mute:I = 0x7f080315

.field public static final menu_hangout_avatar_unpin_video:I = 0x7f080314

.field public static final menu_hangout_debug_simulate_network_error:I = 0x7f080319

.field public static final menu_hangout_debug_stress_join:I = 0x7f08031a

.field public static final menu_hangout_debug_upload_logs:I = 0x7f080318

.field public static final menu_hangout_participant_block:I = 0x7f080322

.field public static final menu_hangout_participant_block_disabled:I = 0x7f080323

.field public static final menu_hangout_participant_pin_video:I = 0x7f08031e

.field public static final menu_hangout_participant_profile:I = 0x7f08031d

.field public static final menu_hangout_participant_remote_mute:I = 0x7f080320

.field public static final menu_hangout_participant_remote_mute_disabled:I = 0x7f080321

.field public static final menu_hangout_participant_unpin_video:I = 0x7f08031f

.field public static final menu_help:I = 0x7f08034a

.field public static final menu_post:I = 0x7f08030f

.field public static final menu_switcher_search:I = 0x7f080047

.field public static final message:I = 0x7f080075

.field public static final messageImage:I = 0x7f080190

.field public static final messageText:I = 0x7f080186

.field public static final message_container:I = 0x7f080124

.field public static final message_list_fragment:I = 0x7f0800c5

.field public static final message_list_item_loading_content:I = 0x7f080184

.field public static final message_status:I = 0x7f08018d

.field public static final message_text:I = 0x7f0800aa

.field public static final middle:I = 0x7f080265

.field public static final minorHangoutDontShow:I = 0x7f0800ff

.field public static final minorHangoutMessage:I = 0x7f0800fe

.field public static final missing_image_view:I = 0x7f0800da

.field public static final more_reviews_bottom_border:I = 0x7f08017b

.field public static final more_reviews_icon:I = 0x7f080177

.field public static final more_reviews_text:I = 0x7f080178

.field public static final more_reviews_top_border:I = 0x7f080175

.field public static final mute:I = 0x7f080328

.field public static final mute_post:I = 0x7f080333

.field public static final mutedFrame:I = 0x7f0800c0

.field public static final mutedIcon:I = 0x7f0800c1

.field public static final name:I = 0x7f08005d

.field public static final name_view:I = 0x7f0801dc

.field public static final navigation_bar:I = 0x7f08013f

.field public static final network_statistics_fragment:I = 0x7f080191

.field public static final network_transactions_fragment:I = 0x7f080197

.field public static final newConversationIcon:I = 0x7f0802c4

.field public static final new_post:I = 0x7f0802e0

.field public static final next_button:I = 0x7f080208

.field public static final next_icon:I = 0x7f0802e7

.field public static final next_progress:I = 0x7f0802e8

.field public static final noButton:I = 0x7f0800e5

.field public static final no_items:I = 0x7f080251

.field public static final none:I = 0x7f080000

.field public static final normal:I = 0x7f080001

.field public static final not_found:I = 0x7f0801d2

.field public static final notification_bar:I = 0x7f080145

.field public static final notification_count:I = 0x7f080152

.field public static final notification_count_overflow:I = 0x7f080153

.field public static final notification_panel:I = 0x7f080144

.field public static final notifications_button:I = 0x7f080151

.field public static final number_of_reviews:I = 0x7f080179

.field public static final ok:I = 0x7f080077

.field public static final one_up_tag_layout:I = 0x7f0801eb

.field public static final one_up_tag_list:I = 0x7f0801ec

.field public static final one_up_tags:I = 0x7f0801ed

.field public static final oob_action_bar:I = 0x7f08019c

.field public static final oob_contacts_sync_fragment:I = 0x7f08019b

.field public static final oob_container:I = 0x7f0801b7

.field public static final oob_instant_upload_fragment:I = 0x7f0801a6

.field public static final oob_item_0:I = 0x7f08000a

.field public static final oob_item_1:I = 0x7f08000b

.field public static final oob_item_10:I = 0x7f080014

.field public static final oob_item_11:I = 0x7f080015

.field public static final oob_item_12:I = 0x7f080016

.field public static final oob_item_13:I = 0x7f080017

.field public static final oob_item_14:I = 0x7f080018

.field public static final oob_item_15:I = 0x7f080019

.field public static final oob_item_16:I = 0x7f08001a

.field public static final oob_item_17:I = 0x7f08001b

.field public static final oob_item_18:I = 0x7f08001c

.field public static final oob_item_19:I = 0x7f08001d

.field public static final oob_item_2:I = 0x7f08000c

.field public static final oob_item_20:I = 0x7f08001e

.field public static final oob_item_21:I = 0x7f08001f

.field public static final oob_item_22:I = 0x7f080020

.field public static final oob_item_23:I = 0x7f080021

.field public static final oob_item_24:I = 0x7f080022

.field public static final oob_item_25:I = 0x7f080023

.field public static final oob_item_26:I = 0x7f080024

.field public static final oob_item_27:I = 0x7f080025

.field public static final oob_item_28:I = 0x7f080026

.field public static final oob_item_29:I = 0x7f080027

.field public static final oob_item_3:I = 0x7f08000d

.field public static final oob_item_30:I = 0x7f080028

.field public static final oob_item_31:I = 0x7f080029

.field public static final oob_item_4:I = 0x7f08000e

.field public static final oob_item_5:I = 0x7f08000f

.field public static final oob_item_6:I = 0x7f080010

.field public static final oob_item_7:I = 0x7f080011

.field public static final oob_item_8:I = 0x7f080012

.field public static final oob_item_9:I = 0x7f080013

.field public static final oob_profile_photo_fragment:I = 0x7f0801b2

.field public static final oob_select_plus_page_fragment:I = 0x7f0801b6

.field public static final open_hours_row:I = 0x7f08027b

.field public static final open_hours_value:I = 0x7f08027c

.field public static final overlay:I = 0x7f0801c3

.field public static final overlay_menu:I = 0x7f08011b

.field public static final panel:I = 0x7f080140

.field public static final participantName:I = 0x7f0802c3

.field public static final participant_list_fragment:I = 0x7f0801be

.field public static final participant_tray_avatars:I = 0x7f0801c6

.field public static final people_array_fragment:I = 0x7f080063

.field public static final people_audience_view_chip_container:I = 0x7f08006b

.field public static final people_list:I = 0x7f080160

.field public static final people_search_circle_id:I = 0x7f08004e

.field public static final people_search_circle_name:I = 0x7f08004f

.field public static final people_search_fragment:I = 0x7f0801cf

.field public static final people_suggestion_data:I = 0x7f08004d

.field public static final people_suggestion_type:I = 0x7f08004c

.field public static final personal:I = 0x7f08024f

.field public static final phone_content:I = 0x7f08024d

.field public static final phone_row:I = 0x7f080279

.field public static final phone_value:I = 0x7f08027a

.field public static final photo:I = 0x7f0801e3

.field public static final photo_1:I = 0x7f08025f

.field public static final photo_2:I = 0x7f080260

.field public static final photo_3:I = 0x7f080261

.field public static final photo_4:I = 0x7f080262

.field public static final photo_5:I = 0x7f080263

.field public static final photo_activity_empty:I = 0x7f0801e7

.field public static final photo_activity_empty_progress:I = 0x7f0801e9

.field public static final photo_activity_empty_progress_text:I = 0x7f0801ea

.field public static final photo_activity_empty_text:I = 0x7f0801e8

.field public static final photo_activity_root_view:I = 0x7f0801e5

.field public static final photo_button:I = 0x7f0800a9

.field public static final photo_caption:I = 0x7f080207

.field public static final photo_group:I = 0x7f080283

.field public static final photo_header_view:I = 0x7f0801f7

.field public static final photo_layout:I = 0x7f0801f6

.field public static final photo_picker_fragment_container:I = 0x7f0801f5

.field public static final photo_progress_bar:I = 0x7f080267

.field public static final photo_view_download_full_failed_dialog:I = 0x7f080032

.field public static final photo_view_download_nonfull_failed_dialog:I = 0x7f080031

.field public static final photo_view_pager:I = 0x7f0801e6

.field public static final photo_view_pending_dialog:I = 0x7f080030

.field public static final photo_view_photo_loader_id:I = 0x7f08002d

.field public static final photo_view_plus_one_loader_id:I = 0x7f08002e

.field public static final photo_view_shape_loader_id:I = 0x7f08002f

.field public static final photos_container:I = 0x7f08021e

.field public static final photos_gallery:I = 0x7f080222

.field public static final places:I = 0x7f080256

.field public static final plus_one:I = 0x7f080205

.field public static final plus_one_animator:I = 0x7f0802be

.field public static final plus_one_glass:I = 0x7f0802bf

.field public static final plus_oned_by:I = 0x7f080339

.field public static final post_container:I = 0x7f08020e

.field public static final post_fragment_audience_loader_id:I = 0x7f08002b

.field public static final post_fragment_media_ref_loader_id:I = 0x7f08002c

.field public static final post_icon:I = 0x7f0802e2

.field public static final post_new:I = 0x7f080326

.field public static final post_photo:I = 0x7f080327

.field public static final posts_section:I = 0x7f080083

.field public static final posts_section_divider:I = 0x7f080084

.field public static final posts_section_gap:I = 0x7f08008e

.field public static final preview_area:I = 0x7f080163

.field public static final preview_chevron:I = 0x7f080165

.field public static final preview_container:I = 0x7f080164

.field public static final preview_icon:I = 0x7f080166

.field public static final preview_link_name:I = 0x7f080167

.field public static final preview_link_url:I = 0x7f080168

.field public static final primary_spinner:I = 0x7f080062

.field public static final primary_spinner_container:I = 0x7f08014a

.field public static final profile:I = 0x7f080230

.field public static final profile_circles_fragment:I = 0x7f08007f

.field public static final progress:I = 0x7f080171

.field public static final progress_bar:I = 0x7f0800e6

.field public static final progress_indicator:I = 0x7f08014f

.field public static final progress_spinner:I = 0x7f0802ce

.field public static final ps_progress:I = 0x7f080295

.field public static final ps_status:I = 0x7f080294

.field public static final public_acl_button:I = 0x7f080054

.field public static final publish_date:I = 0x7f080280

.field public static final quick_actions_buttons:I = 0x7f080296

.field public static final radio:I = 0x7f08029f

.field public static final rating_aspects:I = 0x7f080281

.field public static final rating_item_1:I = 0x7f080286

.field public static final rating_item_2:I = 0x7f080289

.field public static final rating_item_3:I = 0x7f08028a

.field public static final rating_item_4:I = 0x7f08028b

.field public static final rating_label:I = 0x7f080287

.field public static final rating_value:I = 0x7f080288

.field public static final realtimechat_accept_invitation_button:I = 0x7f08016e

.field public static final realtimechat_conversation_edit_name_menu_item:I = 0x7f080309

.field public static final realtimechat_conversation_invite_menu_item:I = 0x7f080308

.field public static final realtimechat_conversation_leave_menu_item:I = 0x7f08030c

.field public static final realtimechat_conversation_mute_menu_item:I = 0x7f08030a

.field public static final realtimechat_conversation_toggle_tile_menu_item:I = 0x7f080307

.field public static final realtimechat_conversation_unmute_menu_item:I = 0x7f08030b

.field public static final realtimechat_reject_invitation_button:I = 0x7f08016d

.field public static final refresh:I = 0x7f080304

.field public static final refresh_button:I = 0x7f080150

.field public static final refresh_icon:I = 0x7f0802e4

.field public static final refresh_progress:I = 0x7f0802e5

.field public static final remove_button:I = 0x7f08017c

.field public static final remove_deleted_media:I = 0x7f08033c

.field public static final remove_image_button:I = 0x7f0800a8

.field public static final remove_location:I = 0x7f080218

.field public static final remove_plus1:I = 0x7f080343

.field public static final remove_post:I = 0x7f08032c

.field public static final remove_preview_button:I = 0x7f080228

.field public static final remove_tag:I = 0x7f080346

.field public static final reportAbuseLink:I = 0x7f0800f0

.field public static final reportAbuseMessage:I = 0x7f0800ef

.field public static final report_abuse:I = 0x7f08032d

.field public static final rescan_media:I = 0x7f08033d

.field public static final reshare:I = 0x7f080349

.field public static final reshare_avatar:I = 0x7f080299

.field public static final reshare_fragment:I = 0x7f080297

.field public static final reshare_info:I = 0x7f08029a

.field public static final reshare_text:I = 0x7f080298

.field public static final resume_hangout_button:I = 0x7f08015f

.field public static final retry_send:I = 0x7f08018b

.field public static final review_container:I = 0x7f080173

.field public static final review_count_and_price:I = 0x7f08028c

.field public static final review_text:I = 0x7f080282

.field public static final right_button:I = 0x7f080099

.field public static final right_highlight_button:I = 0x7f08009a

.field public static final ringing_title:I = 0x7f080104

.field public static final root_layout:I = 0x7f08017d

.field public static final root_view:I = 0x7f0801c4

.field public static final row:I = 0x7f0801ca

.field public static final row_1:I = 0x7f0801d4

.field public static final row_2:I = 0x7f0801d5

.field public static final row_3:I = 0x7f0801d6

.field public static final satellite:I = 0x7f080002

.field public static final save:I = 0x7f080080

.field public static final scaleMarginIndependent:I = 0x7f080005

.field public static final scaleMarginLongEdge:I = 0x7f080006

.field public static final scaleMarginNone:I = 0x7f080004

.field public static final scaleMarginShortEdge:I = 0x7f080007

.field public static final scrapbook_album:I = 0x7f08025e

.field public static final scrapbook_imagery:I = 0x7f08025c

.field public static final scroller:I = 0x7f080081

.field public static final search:I = 0x7f08030e

.field public static final search_go_btn:I = 0x7f080061

.field public static final search_plate:I = 0x7f08005f

.field public static final search_src_text:I = 0x7f080060

.field public static final search_view_container:I = 0x7f08014b

.field public static final second:I = 0x7f0801fc

.field public static final sectionHeader:I = 0x7f0801c1

.field public static final section_wifi_and_mobile:I = 0x7f0801a7

.field public static final select_item:I = 0x7f08033a

.field public static final select_theme_button:I = 0x7f0800cb

.field public static final select_theme_text:I = 0x7f0800ca

.field public static final selector_view:I = 0x7f0800dd

.field public static final self_video_container:I = 0x7f080102

.field public static final self_video_error:I = 0x7f08010d

.field public static final send_button:I = 0x7f0800ab

.field public static final send_text_button:I = 0x7f080291

.field public static final separator:I = 0x7f08006f

.field public static final server_error:I = 0x7f0800ae

.field public static final set_profile_photo:I = 0x7f080344

.field public static final set_wallpaper:I = 0x7f080345

.field public static final settings:I = 0x7f0802aa

.field public static final settings_divider:I = 0x7f0802a9

.field public static final settings_label:I = 0x7f0802ab

.field public static final settings_section:I = 0x7f0802a8

.field public static final shade:I = 0x7f0802d7

.field public static final shadow:I = 0x7f080114

.field public static final share:I = 0x7f080341

.field public static final share_button:I = 0x7f08019a

.field public static final share_link:I = 0x7f080335

.field public static final share_menu_anchor:I = 0x7f08014c

.field public static final share_preview_container:I = 0x7f080227

.field public static final share_preview_wrapper:I = 0x7f080226

.field public static final shim:I = 0x7f0801d0

.field public static final shortDivider:I = 0x7f08028f

.field public static final show_location:I = 0x7f080332

.field public static final show_participant_list_button:I = 0x7f0801c5

.field public static final show_posts:I = 0x7f080086

.field public static final show_posts_divider:I = 0x7f080089

.field public static final show_posts_label:I = 0x7f080087

.field public static final show_posts_section:I = 0x7f080085

.field public static final sign_out:I = 0x7f08033e

.field public static final signup_frame:I = 0x7f0801b9

.field public static final signup_items:I = 0x7f0801bc

.field public static final signup_layout:I = 0x7f0801ba

.field public static final signup_title:I = 0x7f080050

.field public static final single_invitee_view:I = 0x7f0800fb

.field public static final spacer:I = 0x7f080188

.field public static final spacer2:I = 0x7f08018e

.field public static final square:I = 0x7f0802b0

.field public static final square_membership_section:I = 0x7f080095

.field public static final square_membership_section_title:I = 0x7f080096

.field public static final square_name:I = 0x7f0802a0

.field public static final square_photo:I = 0x7f0802ad

.field public static final square_visibility:I = 0x7f0802af

.field public static final squares_acl_button:I = 0x7f08005a

.field public static final stage:I = 0x7f0801e2

.field public static final stage_container:I = 0x7f080110

.field public static final stage_media:I = 0x7f0801e1

.field public static final start:I = 0x7f080232

.field public static final start_arrow:I = 0x7f0800b4

.field public static final start_date:I = 0x7f0800ce

.field public static final start_hangout_button:I = 0x7f08015e

.field public static final start_time:I = 0x7f0800cf

.field public static final state:I = 0x7f0802d5

.field public static final stream_location_layout:I = 0x7f0802b6

.field public static final stream_location_text:I = 0x7f0802b7

.field public static final stream_name:I = 0x7f0802ef

.field public static final subscribe_label:I = 0x7f080093

.field public static final subscribed:I = 0x7f080092

.field public static final subscribed_icon:I = 0x7f080091

.field public static final subscription_disabled:I = 0x7f080094

.field public static final subscription_section:I = 0x7f080090

.field public static final subscription_section_title:I = 0x7f08008f

.field public static final suggested_people_scroll_view:I = 0x7f080065

.field public static final surface_view:I = 0x7f08010c

.field public static final switcher:I = 0x7f0801b8

.field public static final tab_best:I = 0x7f08022e

.field public static final tab_recent:I = 0x7f08022f

.field public static final tag_approval:I = 0x7f0801fa

.field public static final tag_approve:I = 0x7f0801fe

.field public static final tag_buttons:I = 0x7f0801fd

.field public static final tag_deny:I = 0x7f0801ff

.field public static final tag_gaiaid:I = 0x7f080041

.field public static final tag_is_suggestion:I = 0x7f080040

.field public static final tag_media_id:I = 0x7f080043

.field public static final tag_media_url:I = 0x7f080042

.field public static final tag_photo_info_view:I = 0x7f080038

.field public static final tag_photo_layout:I = 0x7f080036

.field public static final tag_photo_view:I = 0x7f080037

.field public static final tag_position:I = 0x7f08003f

.field public static final tag_row_id:I = 0x7f080045

.field public static final tag_scroller_layout:I = 0x7f080039

.field public static final tag_scroller_title:I = 0x7f08003b

.field public static final tag_scroller_view:I = 0x7f08003a

.field public static final tag_shape_id:I = 0x7f08003e

.field public static final tag_shape_name:I = 0x7f08003c

.field public static final tag_shape_rect:I = 0x7f08003d

.field public static final tag_text:I = 0x7f0801fb

.field public static final tag_upload_reason:I = 0x7f080044

.field public static final tagline:I = 0x7f08023d

.field public static final terrain:I = 0x7f080003

.field public static final text:I = 0x7f08007a

.field public static final text_container:I = 0x7f080161

.field public static final text_input:I = 0x7f0802c6

.field public static final text_marker:I = 0x7f080212

.field public static final text_only_content_1:I = 0x7f0802f9

.field public static final text_only_content_2:I = 0x7f0802fa

.field public static final text_only_content_3:I = 0x7f0802fb

.field public static final text_only_stream_name:I = 0x7f0802f8

.field public static final text_only_user_image:I = 0x7f0802f6

.field public static final text_only_user_name:I = 0x7f0802f7

.field public static final text_view:I = 0x7f080169

.field public static final text_view_disable:I = 0x7f0801b0

.field public static final text_view_wifi_and_mobile:I = 0x7f0801a9

.field public static final text_view_wifi_only:I = 0x7f0801ad

.field public static final thumb:I = 0x7f0802d4

.field public static final tile_container:I = 0x7f0800ac

.field public static final timeSince:I = 0x7f0800be

.field public static final time_zone:I = 0x7f0800d2

.field public static final timestamp:I = 0x7f08012b

.field public static final title:I = 0x7f08007e

.field public static final title_bar:I = 0x7f08012d

.field public static final title_button_1:I = 0x7f0802cb

.field public static final title_button_2:I = 0x7f0802cf

.field public static final title_button_3:I = 0x7f0802d0

.field public static final title_layout:I = 0x7f0802c7

.field public static final titlebar_icon:I = 0x7f0802ca

.field public static final titlebar_icon_layout:I = 0x7f0802c8

.field public static final titlebar_label:I = 0x7f0802cc

.field public static final titlebar_label_2:I = 0x7f0802cd

.field public static final titlebar_up:I = 0x7f0802c9

.field public static final toast_icon:I = 0x7f080132

.field public static final toast_text:I = 0x7f080133

.field public static final toasts_view:I = 0x7f080125

.field public static final toggle_hangout_ring_button:I = 0x7f08015d

.field public static final top_border:I = 0x7f08027d

.field public static final touch_handler:I = 0x7f0801f0

.field public static final touch_sensor_view:I = 0x7f080128

.field public static final transaction_bytes:I = 0x7f080195

.field public static final transaction_duration:I = 0x7f080196

.field public static final transaction_name:I = 0x7f080194

.field public static final transaction_time:I = 0x7f080193

.field public static final transient_server_error:I = 0x7f0802bc

.field public static final translucent_layer:I = 0x7f080204

.field public static final type_overlay:I = 0x7f0801c2

.field public static final typing_text:I = 0x7f080180

.field public static final typing_text_view:I = 0x7f080181

.field public static final unblock:I = 0x7f08032b

.field public static final unmute:I = 0x7f080329

.field public static final unreadCount:I = 0x7f0800c2

.field public static final unread_count:I = 0x7f0802ae

.field public static final up:I = 0x7f08012e

.field public static final up_caret:I = 0x7f080146

.field public static final user_activity:I = 0x7f080248

.field public static final user_image:I = 0x7f0802ed

.field public static final user_name:I = 0x7f0802ee

.field public static final user_rated_logo:I = 0x7f080285

.field public static final vertical:I = 0x7f080009

.field public static final vertical_divider:I = 0x7f080292

.field public static final vertical_divider_call:I = 0x7f080274

.field public static final vertical_divider_directions:I = 0x7f080272

.field public static final vertical_separator:I = 0x7f08021c

.field public static final video_avatar:I = 0x7f0800f8

.field public static final video_overlay:I = 0x7f0801f4

.field public static final video_paused:I = 0x7f0800fa

.field public static final video_view:I = 0x7f0800f7

.field public static final video_view_fragment_container:I = 0x7f0802d8

.field public static final videolayout:I = 0x7f0802d9

.field public static final videoplayer:I = 0x7f0802da

.field public static final view_all:I = 0x7f0801de

.field public static final view_all_text:I = 0x7f0801df

.field public static final view_pager:I = 0x7f0801e0

.field public static final visibility:I = 0x7f08023a

.field public static final widget:I = 0x7f0802f3

.field public static final widget_empty_layout:I = 0x7f0802de

.field public static final widget_image_layout:I = 0x7f0802e9

.field public static final widget_main:I = 0x7f0802f4

.field public static final widget_text_layout:I = 0x7f0802f5

.field public static final work_section:I = 0x7f080250

.field public static final wrapper:I = 0x7f0801c9

.field public static final yesButton:I = 0x7f0800e2

.field public static final your_circles_acl_button:I = 0x7f080056

.field public static final zagat:I = 0x7f080241

.field public static final zagat_editorial_text:I = 0x7f08028e

.field public static final zagat_explanation:I = 0x7f08028d

.field public static final zagat_logo:I = 0x7f080284


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
