.class public final Lcom/google/android/gms/wallet/ProxyCard;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/al;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/an;


# instance fields
.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:I

.field public H:I

.field public mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/an;

    invoke-direct {v0}, Lcom/google/android/gms/internal/an;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ProxyCard;->CREATOR:Lcom/google/android/gms/internal/an;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ProxyCard;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/ProxyCard;->CREATOR:Lcom/google/android/gms/internal/an;

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/ProxyCard;->CREATOR:Lcom/google/android/gms/internal/an;

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/an;->a$4f52da30(Lcom/google/android/gms/wallet/ProxyCard;Landroid/os/Parcel;)V

    return-void
.end method
