.class public final Lcom/google/android/gms/wallet/FullWallet;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/al;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/r;

.field public static final DEFAULT_INSTANCE:Lcom/google/android/gms/wallet/FullWallet;


# instance fields
.field public aud:Ljava/lang/String;

.field public billingAddress:Lcom/google/android/gms/wallet/Address;

.field public e:Lcom/google/android/gms/wallet/ProxyCard;

.field public email:Ljava/lang/String;

.field public exp:J

.field public googleTransactionId:Ljava/lang/String;

.field public iat:J

.field public iss:Ljava/lang/String;

.field public mVersionCode:I

.field public merchantTransactionId:Ljava/lang/String;

.field public shippingAddress:Lcom/google/android/gms/wallet/Address;

.field public typ:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/r;

    invoke-direct {v0}, Lcom/google/android/gms/internal/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/FullWallet;->CREATOR:Lcom/google/android/gms/internal/r;

    new-instance v0, Lcom/google/android/gms/wallet/FullWallet;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/FullWallet;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/FullWallet;->DEFAULT_INSTANCE:Lcom/google/android/gms/wallet/FullWallet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/FullWallet;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/FullWallet;->CREATOR:Lcom/google/android/gms/internal/r;

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/FullWallet;->CREATOR:Lcom/google/android/gms/internal/r;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/r;->a(Lcom/google/android/gms/wallet/FullWallet;Landroid/os/Parcel;I)V

    return-void
.end method
