.class public final Lcom/google/android/picasasync/PicasaFacade;
.super Ljava/lang/Object;
.source "PicasaFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/picasasync/PicasaFacade;


# instance fields
.field private mAlbumCoversUri:Landroid/net/Uri;

.field private mAlbumsUri:Landroid/net/Uri;

.field private mAuthority:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

.field private mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

.field private mPhotosUri:Landroid/net/Uri;

.field private mPostAlbumsUri:Landroid/net/Uri;

.field private mPostPhotosUri:Landroid/net/Uri;

.field private mSettingsUri:Landroid/net/Uri;

.field private mSyncRequestUri:Landroid/net/Uri;

.field private mUsersUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/picasasync/PicasaContentProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1, v5, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/picasasync/PicasaSyncService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1, v5, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/picasasync/PicasaFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/picasasync/PicasaFacade;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/picasasync/PicasaFacade;->sInstance:Lcom/google/android/picasasync/PicasaFacade;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasasync/PicasaFacade;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PicasaFacade;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/PicasaFacade;->sInstance:Lcom/google/android/picasasync/PicasaFacade;

    :cond_0
    sget-object v0, Lcom/google/android/picasasync/PicasaFacade;->sInstance:Lcom/google/android/picasasync/PicasaFacade;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized updatePicasaSyncInfo(Z)V
    .locals 13
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.google.android.picasasync.SyncAdapter"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x84

    invoke-virtual {v4, v8, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-boolean v9, v8, Landroid/content/pm/ServiceInfo;->enabled:Z

    if-eqz v9, :cond_1

    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v9, v9, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v9, :cond_5

    :cond_1
    const-string v9, "gp.PicasaFacade"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ignore disabled picasa sync adapter: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_0

    iget-boolean v8, v7, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->enableDownSync:Z

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-eqz v6, :cond_3

    iget v8, v6, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->priority:I

    iget v9, v7, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->priority:I

    if-ge v8, v9, :cond_4

    :cond_3
    move-object v6, v7

    :cond_4
    iget-object v8, v7, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iput-object v7, p0, Lcom/google/android/picasasync/PicasaFacade;->mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    :cond_5
    :try_start_1
    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-nez v9, :cond_6

    const-string v9, "gp.PicasaFacade"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing metadata: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto :goto_1

    :cond_6
    const-string v10, "com.google.android.picasasync.priority"

    const/4 v11, -0x1

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    const-string v11, "com.google.android.picasasync.enable-down-sync"

    const/4 v12, 0x1

    invoke-virtual {v9, v11, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    const-string v12, "com.google.android.picasasync.authority"

    invoke-virtual {v9, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v12, -0x1

    if-eq v10, v12, :cond_7

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_8

    :cond_7
    const-string v9, "gp.PicasaFacade"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing required metadata info: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto :goto_1

    :cond_8
    new-instance v7, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_1

    :cond_9
    iput-boolean v1, v6, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->enableDownSync:Z

    iput-object v6, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    invoke-static {v8}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    invoke-static {v8}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    if-ne v8, v9, :cond_c

    const/4 v8, 0x1

    :goto_2
    invoke-direct {p0, v8}, Lcom/google/android/picasasync/PicasaFacade;->updateSyncableState(Z)V

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v8, v8, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->authority:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_a

    iput-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mAuthority:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "content://"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    const-string v9, "photos"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mPhotosUri:Landroid/net/Uri;

    const-string v9, "albums"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mAlbumsUri:Landroid/net/Uri;

    const-string v9, "posts_album"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mPostAlbumsUri:Landroid/net/Uri;

    const-string v9, "posts"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mPostPhotosUri:Landroid/net/Uri;

    const-string v9, "users"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mUsersUri:Landroid/net/Uri;

    const-string v9, "settings"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mSettingsUri:Landroid/net/Uri;

    const-string v9, "sync_request"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/picasasync/PicasaFacade;->mSyncRequestUri:Landroid/net/Uri;

    const-string v9, "albumcovers"

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mAlbumCoversUri:Landroid/net/Uri;

    :cond_a
    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-boolean v8, v8, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->enableDownSync:Z

    if-nez p1, :cond_b

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaFacade;->isMaster()Z

    move-result v8

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v8

    const-wide/16 v9, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_b
    monitor-exit p0

    return-void

    :cond_c
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private updateSyncableState(Z)V
    .locals 13
    .param p1    # Z

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    if-eqz p1, :cond_3

    move v8, v10

    :goto_0
    new-instance v5, Landroid/content/ComponentName;

    iget-object v11, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    const-class v12, Lcom/google/android/picasasync/ConnectivityReceiver;

    invoke-direct {v5, v11, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v9, v5}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v11

    if-eq v11, v8, :cond_0

    invoke-virtual {v9, v5, v8, v10}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    new-instance v4, Landroid/content/ComponentName;

    iget-object v11, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    const-class v12, Lcom/google/android/picasasync/BatteryReceiver;

    invoke-direct {v4, v11, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v9, v4}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v11

    if-eq v11, v8, :cond_1

    invoke-virtual {v9, v4, v8, v10}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    iget-object v10, p0, Lcom/google/android/picasasync/PicasaFacade;->mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v3, v10, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->authority:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    if-eqz p1, :cond_4

    const-string v10, "com.google"

    invoke-virtual {v1, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v7, v2

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_5

    aget-object v0, v2, v6

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_2

    const/4 v10, -0x1

    invoke-static {v0, v3, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v3, v10}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    const/4 v8, 0x2

    goto :goto_0

    :cond_4
    const-string v10, "com.google"

    invoke-virtual {v1, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v7, v2

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v7, :cond_5

    aget-object v0, v2, v6

    const/4 v10, 0x0

    invoke-static {v0, v3, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public final getAlbumsUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mAlbumsUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getAuthority()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mAuthority:Ljava/lang/String;

    return-object v0
.end method

.method public final getMasterInfo()Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    return-object v0
.end method

.method public final getPhotosUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mPhotosUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getSettingsUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mSettingsUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getSyncRequestUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mSyncRequestUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getUsersUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mUsersUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final isMaster()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mMasterInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaFacade;->mLocalInfo:Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMediaMounted()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaFacade;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    return-void
.end method

.method public final onPackageAdded$552c4e01()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method

.method public final onPackageRemoved$552c4e01()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method
