.class final Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/PicasaStore$DownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/PicasaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlobCacheRegister"
.end annotation


# instance fields
.field private final mAuxKey:[B

.field private mBaos:Ljava/io/ByteArrayOutputStream;

.field private final mFlags:I

.field private final mKey:[B

.field final synthetic this$0:Lcom/google/android/picasastore/PicasaStore;


# direct methods
.method public constructor <init>(Lcom/google/android/picasastore/PicasaStore;JIILjava/lang/String;)V
    .locals 1
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->this$0:Lcom/google/android/picasastore/PicasaStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mBaos:Ljava/io/ByteArrayOutputStream;

    # invokes: Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/picasastore/PicasaStore;->access$100(Lcom/google/android/picasastore/PicasaStore;JI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mKey:[B

    # invokes: Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B
    invoke-static {p1, p6}, Lcom/google/android/picasastore/PicasaStore;->access$200(Lcom/google/android/picasastore/PicasaStore;Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mAuxKey:[B

    iput p5, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mFlags:I

    return-void
.end method


# virtual methods
.method public final onDataAvailable([BII)V
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mBaos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    const/high16 v1, 0x80000

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mBaos:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_0
    return-void
.end method

.method public final onDownloadComplete()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mBaos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    const/high16 v1, 0x80000

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->this$0:Lcom/google/android/picasastore/PicasaStore;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mKey:[B

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mAuxKey:[B

    iget v3, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mFlags:I

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;->mBaos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    # invokes: Lcom/google/android/picasastore/PicasaStore;->putBlobCache([B[BI[B)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/picasastore/PicasaStore;->access$300(Lcom/google/android/picasastore/PicasaStore;[B[BI[B)V

    :cond_0
    return-void
.end method
