.class public final enum Lcom/google/android/apps/plus/analytics/OzViews;
.super Ljava/lang/Enum;
.source "OzViews.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/analytics/OzViews;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HOME:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum OOB_ADD_PEOPLE_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum OOB_ADD_PROFILE_PHOTO_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum OOB_CAMERA_SYNC:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum OOB_IMPROVE_CONTACTS_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SQUARE_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SQUARE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;


# instance fields
.field private final mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

.field private final mViewData:Lcom/google/api/services/plusi/model/OutputData;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v2, 0x4

    const/16 v12, 0xa

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "UNKNOWN"

    const/4 v3, 0x0

    const-string v4, "str"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HOME"

    const-string v3, "str"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v9, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS"

    const-string v3, "str"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v10, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "GENERAL_SETTINGS"

    const-string v3, "Settings"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v11, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_EVERYONE"

    const-string v3, "str"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "LOOP_CIRCLES"

    const/4 v5, 0x5

    const-string v6, "str"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "LOOP_NEARBY"

    const/4 v5, 0x6

    const-string v6, "str"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_MANAGE"

    const/4 v3, 0x7

    const-string v4, "str"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_WHATS_HOT"

    const/16 v3, 0x8

    const-string v4, "xplr"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_USER"

    const/16 v3, 0x9

    const-string v4, "pr"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "COMPOSE"

    const-string v3, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v12, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOCATION_PICKER"

    const/16 v3, 0xb

    const-string v4, "ttn"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CIRCLE_PICKER"

    const/16 v3, 0xc

    const-string v4, "ttn"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_PICKER"

    const/16 v3, 0xd

    const-string v4, "ttn"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "COMMENT"

    const/16 v3, 0xe

    const-string v4, "ttn"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SHARE"

    const/16 v3, 0xf

    const-string v4, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "RESHARE"

    const/16 v3, 0x10

    const-string v4, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ACTIVITY"

    const/16 v3, 0x11

    const-string v4, "pr"

    const/16 v5, 0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PROFILE"

    const/16 v3, 0x12

    const-string v4, "pr"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CIRCLE_SETTINGS"

    const/16 v3, 0x13

    const-string v4, "Settings"

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_IN_CIRCLES"

    const/16 v3, 0x14

    const-string v4, "sg"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_CIRCLE"

    const/16 v3, 0x15

    const-string v4, "sg"

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_TO_CIRCLE"

    const/16 v3, 0x16

    const-string v4, "sg"

    const/16 v5, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_SEARCH"

    const/16 v3, 0x17

    const-string v4, "pr"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SEARCH"

    const/16 v3, 0x18

    const-string v4, "se"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLUSONE"

    const/16 v3, 0x19

    const-string v4, "plusone"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "REMOVE_FROM_CIRCLE"

    const/16 v3, 0x1a

    const-string v4, "sg"

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_PERSON_TO_CIRCLES"

    const/16 v3, 0x1b

    const-string v4, "sg"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_BLOCKED"

    const/16 v3, 0x1c

    const-string v4, "sg"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "WW_SUGGESTIONS"

    const/16 v3, 0x1d

    const-string v4, "getstarted"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTO"

    const/16 v3, 0x1e

    const-string v4, "phst"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTOS_HOME"

    const/16 v3, 0x1f

    const-string v4, "phst"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTOS_LIST"

    const/16 v3, 0x20

    const-string v4, "phst"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTO_PICKER"

    const/16 v3, 0x21

    const-string v4, "ttn"

    const/16 v5, 0x1d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "VIDEO"

    const/16 v3, 0x22

    const-string v4, "phst"

    const/16 v5, 0x1b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ALBUMS_OF_USER"

    const/16 v3, 0x23

    const-string v4, "pr"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "INSTANT_UPLOAD_GALLERY"

    const/16 v3, 0x24

    const-string v4, "phst"

    const/16 v5, 0x1e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATIONS"

    const/16 v3, 0x25

    const-string v4, "messenger"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_GROUP"

    const/16 v3, 0x26

    const-string v4, "messenger"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_ONE_ON_ONE"

    const/16 v3, 0x27

    const-string v4, "messenger"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_START_NEW"

    const/16 v3, 0x28

    const-string v4, "messenger"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_PARTICIPANT_LIST"

    const/16 v3, 0x29

    const-string v4, "messenger"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_INVITE"

    const/16 v3, 0x2a

    const-string v4, "messenger"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT"

    const/16 v3, 0x2b

    const-string v4, "h"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT_START_NEW"

    const/16 v3, 0x2c

    const-string v4, "h"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT_PARTICIPANTS"

    const/16 v3, 0x2d

    const-string v4, "h"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_WIDGET"

    const/16 v3, 0x2e

    const-string v4, "nots"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_CIRCLE"

    const/16 v3, 0x2f

    const-string v4, "nots"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_SYSTEM"

    const/16 v3, 0x30

    const-string v4, "nots"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONTACTS_CIRCLELIST"

    const/16 v3, 0x31

    const-string v4, "sg"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONTACTS_SYNC_CONFIG"

    const/16 v3, 0x32

    const-string v4, "Settings"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLATFORM_PLUS_ONE"

    const/16 v3, 0x33

    const-string v4, "plusone"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLATFORM_THIRD_PARTY_APP"

    const/16 v3, 0x34

    const-string v4, "plusone"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "EVENT"

    const/16 v3, 0x35

    const-string v4, "oevt"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CREATE_EVENT"

    const/16 v3, 0x36

    const-string v4, "oevt"

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "MY_EVENTS"

    const/16 v3, 0x37

    const-string v4, "oevt"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "EVENT_THEMES"

    const/16 v3, 0x38

    const-string v4, "oevt"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SQUARE_LANDING"

    const/16 v3, 0x39

    const-string v4, "sq"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SQUARE_HOME"

    const/16 v3, 0x3a

    const-string v4, "sq"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SQUARE_MEMBERS"

    const/16 v3, 0x3b

    const-string v4, "sq"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SQUARE_SEARCH"

    const/16 v3, 0x3c

    const-string v4, "sq"

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "OOB_CAMERA_SYNC"

    const/16 v3, 0x3d

    const-string v4, "oob"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_CAMERA_SYNC:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "OOB_ADD_PEOPLE_VIEW"

    const/16 v3, 0x3e

    const-string v4, "oob"

    const/16 v5, 0x12

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_ADD_PEOPLE_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "OOB_IMPROVE_CONTACTS_VIEW"

    const/16 v3, 0x3f

    const-string v4, "oob"

    const/16 v5, 0x13

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_IMPROVE_CONTACTS_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "OOB_ADD_PROFILE_PHOTO_VIEW"

    const/16 v3, 0x40

    const-string v4, "oob"

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_ADD_PROFILE_PHOTO_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LEFT_NAV"

    const/16 v3, 0x41

    const-string v4, "natapp"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

    const/16 v0, 0x42

    new-array v0, v0, [Lcom/google/android/apps/plus/analytics/OzViews;

    const/4 v1, 0x0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v3, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v2

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v12

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_CAMERA_SYNC:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_ADD_PEOPLE_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_IMPROVE_CONTACTS_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_ADD_PROFILE_PHOTO_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 6
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p4, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    if-eqz p5, :cond_0

    new-instance v0, Lcom/google/api/services/plusi/model/OutputData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutputData;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    iput-object p5, v0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    goto :goto_0
.end method

.method public static getExtrasForLogging(Landroid/content/Context;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Landroid/content/Context;

    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/analytics/OzViews;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1
    .param p0    # Landroid/content/Context;

    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 2
    .param p0    # I

    invoke-static {}, Lcom/google/android/apps/plus/analytics/OzViews;->values()[Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    aget-object v1, v0, p0

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/analytics/OzViews;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method


# virtual methods
.method public final getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    return-object v0
.end method

.method public final getViewData()Lcom/google/api/services/plusi/model/OutputData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    return-object v0
.end method
