.class public Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
.super Ljava/lang/Object;
.source "OutOfBoxResponseParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_0

    new-array v0, v1, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
