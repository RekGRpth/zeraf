.class public Lcom/google/android/apps/plus/oob/DropDownFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "DropDownFieldLayout.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private mInput:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private dispatchOnInputChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-interface {v0}, Lcom/google/android/apps/plus/oob/ActionCallback;->onInputChanged$7c32a9fe()V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 9
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    const/4 v7, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v6

    iget-object v1, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v2

    iget-object v6, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->style:Lcom/google/api/services/plusi/model/TextStyle;

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/oob/OobTextStyler;->applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Spinner;

    iput-object v6, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    iget-object v8, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;-><init>(Ljava/util/List;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    invoke-virtual {v6, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->indexOf(Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;)I

    move-result v4

    :goto_0
    if-eq v4, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    invoke-virtual {v6, v4}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    if-eqz p3, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    invoke-virtual {v6, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_1
    return-void

    :cond_2
    move v4, v7

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v2

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v2}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->mInput:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iput-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    :cond_0
    return-object v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->dispatchOnInputChanged()V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldLayout;->dispatchOnInputChanged()V

    return-void
.end method
