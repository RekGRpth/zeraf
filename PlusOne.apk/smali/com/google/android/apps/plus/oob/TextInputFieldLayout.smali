.class public Lcom/google/android/apps/plus/oob/TextInputFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "TextInputFieldLayout.java"


# instance fields
.field private mInput:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v2

    iget-object v0, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->style:Lcom/google/api/services/plusi/model/TextStyle;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/oob/OobTextStyler;->applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->getServerStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz p3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    new-instance v3, Lcom/google/android/apps/plus/oob/TextInputFieldLayout$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/oob/TextInputFieldLayout$1;-><init>(Lcom/google/android/apps/plus/oob/TextInputFieldLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_2
    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/TextInputFieldLayout;->mInput:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    return-object v0
.end method
