.class final Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;
.super Landroid/text/style/ClickableSpan;
.source "ActionTagHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/oob/ActionTagHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionClickableSpan"
.end annotation


# instance fields
.field private final mActionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/plus/oob/ActionTagHandler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/oob/ActionTagHandler;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;->this$0:Lcom/google/android/apps/plus/oob/ActionTagHandler;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;->mActionId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;->this$0:Lcom/google/android/apps/plus/oob/ActionTagHandler;

    # getter for: Lcom/google/android/apps/plus/oob/ActionTagHandler;->mCallback:Lcom/google/android/apps/plus/oob/ActionTagHandler$Callback;
    invoke-static {v0}, Lcom/google/android/apps/plus/oob/ActionTagHandler;->access$000(Lcom/google/android/apps/plus/oob/ActionTagHandler;)Lcom/google/android/apps/plus/oob/ActionTagHandler$Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;->mActionId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/oob/ActionTagHandler$Callback;->onActionId(Ljava/lang/String;)V

    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1    # Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ActionTagHandler$ActionClickableSpan;->this$0:Lcom/google/android/apps/plus/oob/ActionTagHandler;

    # getter for: Lcom/google/android/apps/plus/oob/ActionTagHandler;->mLinkColor:I
    invoke-static {v0}, Lcom/google/android/apps/plus/oob/ActionTagHandler;->access$100(Lcom/google/android/apps/plus/oob/ActionTagHandler;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    return-void
.end method
