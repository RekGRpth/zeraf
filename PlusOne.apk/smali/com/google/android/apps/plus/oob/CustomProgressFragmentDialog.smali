.class public Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog;
.super Lvedroid/support/v4/app/DialogFragment;
.source "CustomProgressFragmentDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog$CustomProgressDialogListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const-string v3, "cancelable"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog;->setCancelable(Z)V

    if-eqz v1, :cond_1

    new-instance v3, Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog$1;-><init>(Lcom/google/android/apps/plus/oob/CustomProgressFragmentDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    return-object v2
.end method
