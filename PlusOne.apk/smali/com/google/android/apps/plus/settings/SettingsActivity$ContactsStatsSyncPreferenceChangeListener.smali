.class final Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactsStatsSyncPreferenceChangeListener"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic this$0:Lcom/google/android/apps/plus/settings/SettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/SettingsActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-wide/16 v3, -0x1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordImproveSuggestionsPreferenceChange(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/analytics/OzViews;)V

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/service/EsService;->disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    :goto_0
    const/4 v2, 0x1

    return v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/service/EsService;->enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    goto :goto_0
.end method
