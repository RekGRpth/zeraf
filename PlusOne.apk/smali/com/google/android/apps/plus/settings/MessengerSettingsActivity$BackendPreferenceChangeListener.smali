.class final Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;
.super Ljava/lang/Object;
.source "MessengerSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackendPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mCurrentBackend:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$600(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->logOut(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method
