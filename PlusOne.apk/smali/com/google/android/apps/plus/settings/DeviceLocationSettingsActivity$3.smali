.class final Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;
.super Ljava/lang/Object;
.source "DeviceLocationSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->showDialog(I)V

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$302(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->saveSettings()V
    invoke-static {v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$800(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    const-string v1, "DeviceLocationSettings"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "DeviceLocationSettings"

    const-string v2, "Locating sharing disabled - saveSettings"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->logAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0
.end method
