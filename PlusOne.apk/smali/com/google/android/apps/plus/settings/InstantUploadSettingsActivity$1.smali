.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "InstantUploadSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, -0x1

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "com.google.android.apps.plus.iu.upload_all_progress"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "upload_all_progress"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "upload_all_count"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "upload_all_state"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;

    invoke-direct {v5, p0, v3, v1, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;III)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
