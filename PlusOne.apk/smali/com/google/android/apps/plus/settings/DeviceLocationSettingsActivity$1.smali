.class final Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "DeviceLocationSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$100(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$100(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$102(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$202(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$600(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->dismissDialog(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/AccountSettingsData;->isLocationSharingEnabled()Z

    move-result v1

    # setter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$302(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getBestAvailableLocationSharingAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$402(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # setter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$202(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateLastSavedSettings()V
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$500(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    goto :goto_0
.end method

.method public final onSaveLocationSharingSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->handleSaveComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method
