.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;
.super Ljava/lang/Object;
.source "InstantUploadSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

.field final synthetic val$progress:I

.field final synthetic val$state:I

.field final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;III)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iput p2, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$total:I

    iput p3, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$progress:I

    iput p4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$state:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v7, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$total:I

    iget v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$progress:I

    if-eq v4, v8, :cond_0

    move v4, v5

    :goto_0
    # setter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v7, v4}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->photo_sync_preference_title:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->photo_sync_preference_summary:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->photo_upload_finished:I

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->unregisterUploadProgressReceiver()V
    invoke-static {v4}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$100(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$300()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v7, Lcom/google/android/apps/plus/R$string;->photo_sync_preference_cancel_title:I

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$state:I

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$state:I

    if-ne v4, v5, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v7, Lcom/google/android/apps/plus/R$string;->photo_upload_now_inprogress_summary:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$progress:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    iget v6, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$total:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v8, v5

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget v7, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->val$state:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1$1;->this$1:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v7, Lcom/google/android/apps/plus/R$string;->photo_upload_now_paused_summary:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v4, v7, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
