.class final Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;
.super Ljava/lang/Object;
.source "MessengerSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AclPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    instance-of v4, p2, Ljava/lang/String;

    if-eqz v4, :cond_2

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    const/4 v2, -0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->key_acl_setting_anyone:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_anyone:I

    :cond_0
    :goto_0
    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    # setter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    # setter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$102(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    const-string v4, "MessengerSettings"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "MessengerSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Changing acl to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    iget-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-static {v5, v0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setAcl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    # setter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$202(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->showDialog(ILandroid/os/Bundle;)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    new-instance v5, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$TimeoutRunnable;

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$TimeoutRunnable;-><init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V

    # setter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$302(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    iget-object v4, v4, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->access$300(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x2710

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    :goto_1
    const/4 v4, 0x0

    return v4

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->key_acl_setting_my_circles:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v2, 0x3

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_my_circles:I

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->key_acl_setting_extended_circles:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_extended_circles:I

    goto/16 :goto_0

    :cond_5
    const-string v4, "MessengerSettings"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "MessengerSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid ACL value ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
