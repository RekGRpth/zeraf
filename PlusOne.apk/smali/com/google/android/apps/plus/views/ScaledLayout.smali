.class public Lcom/google/android/apps/plus/views/ScaledLayout;
.super Landroid/widget/RelativeLayout;
.source "ScaledLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ScaledLayout$1;,
        Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;
    }
.end annotation


# instance fields
.field private final mDispSize:Landroid/graphics/Rect;

.field private final mDisplay:Landroid/view/Display;

.field private mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

.field private mScaleHeight:F

.field private mScaleMarginBottom:F

.field private mScaleMarginLeft:F

.field private mScaleMarginRight:F

.field private mScaleMarginTop:F

.field private mScaleWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ScaledLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ScaledLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v4, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    iput v4, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    iput v3, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    iput v3, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    iput v3, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    iput v3, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    sget-object v1, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_NONE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->ScaledLayout:[I

    invoke-virtual {p1, p2, v1, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    :goto_0
    invoke-virtual {v1, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v6, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    :goto_1
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    if-ne v2, v7, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_INDEPENDENT:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    :goto_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScaledLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDisplay:Landroid/view/Display;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDispSize:Landroid/graphics/Rect;

    return-void

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    invoke-virtual {v1, v7, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    goto :goto_0

    :cond_2
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    goto :goto_1

    :cond_3
    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_LONG_EDGE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    goto :goto_2

    :cond_4
    if-ne v2, v6, :cond_5

    sget-object v2, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_SHORT_EDGE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    goto :goto_2

    :cond_5
    sget-object v2, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_NONE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    goto :goto_2
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 23
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v20

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ScaledLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v21, v0

    sget-object v22, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_INDEPENDENT:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v21, v0

    sget-object v22, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_LONG_EDGE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v21, v0

    sget-object v22, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->SCALE_MARGIN_SHORT_EDGE:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v10, 0x1

    :goto_0
    move/from16 v19, v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ScaledLayout;->getMeasuredWidth()I

    move-result v13

    const/high16 v21, -0x80000000

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_5

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v21, v0

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v22, v0

    add-int v11, v21, v22

    if-eqz v10, :cond_4

    add-int v21, v13, v11

    move/from16 v0, v21

    if-ne v7, v0, :cond_4

    move/from16 v19, v13

    :goto_1
    const/high16 v20, 0x40000000

    :cond_1
    :goto_2
    move v4, v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ScaledLayout;->getMeasuredHeight()I

    move-result v12

    const/high16 v21, -0x80000000

    move/from16 v0, v21

    if-ne v5, v0, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_7

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v21, v0

    iget v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v22, v0

    add-int v11, v21, v22

    if-eqz v10, :cond_6

    add-int v21, v12, v11

    move/from16 v0, v21

    if-ne v6, v0, :cond_6

    move v4, v12

    :goto_3
    const/high16 v5, 0x40000000

    :cond_2
    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/views/ScaledLayout;->setMeasuredDimension(II)V

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-super {v0, v3, v2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    if-nez v10, :cond_8

    :goto_5
    return-void

    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    :cond_4
    add-int v21, v7, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v19, v0

    goto :goto_1

    :cond_5
    if-nez v20, :cond_1

    if-gtz v7, :cond_1

    move/from16 v19, v13

    goto :goto_2

    :cond_6
    add-int v21, v6, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v4, v0

    goto :goto_3

    :cond_7
    if-nez v5, :cond_2

    if-gtz v6, :cond_2

    move v4, v12

    goto :goto_4

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ScaledLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    if-eqz v14, :cond_9

    instance-of v0, v14, Landroid/view/View;

    move/from16 v21, v0

    if-eqz v21, :cond_8

    :cond_9
    if-eqz v14, :cond_a

    move-object/from16 v16, v14

    check-cast v16, Landroid/view/View;

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    :goto_6
    move/from16 v0, v17

    if-ge v0, v15, :cond_b

    move/from16 v18, v17

    move v9, v15

    :goto_7
    sget-object v21, Lcom/google/android/apps/plus/views/ScaledLayout$1;->$SwitchMap$com$google$android$apps$plus$views$ScaledLayout$MarginMode:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;->ordinal()I

    move-result v22

    aget v21, v21, v22

    packed-switch v21, :pswitch_data_0

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ScaledLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDisplay:Landroid/view/Display;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDispSize:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDispSize:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mDispSize:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v15

    goto :goto_6

    :cond_b
    move/from16 v18, v15

    move/from16 v9, v17

    goto :goto_7

    :pswitch_0
    int-to-float v0, v15

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v15

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/16 :goto_8

    :pswitch_1
    int-to-float v0, v9

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v9

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    int-to-float v0, v9

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-float v0, v9

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/16 :goto_8

    :pswitch_2
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setScale(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    return-void
.end method

.method public setScaleHeight(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleHeight:F

    return-void
.end method

.method public setScaleMargin(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    return-void
.end method

.method public setScaleMarginBottom(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginBottom:F

    return-void
.end method

.method public setScaleMarginLeft(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginLeft:F

    return-void
.end method

.method public setScaleMarginMode(Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mMarginMode:Lcom/google/android/apps/plus/views/ScaledLayout$MarginMode;

    return-void
.end method

.method public setScaleMarginRight(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginRight:F

    return-void
.end method

.method public setScaleMarginTop(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleMarginTop:F

    return-void
.end method

.method public setScaleWidth(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ScaledLayout;->mScaleWidth:F

    return-void
.end method
