.class public Lcom/google/android/apps/plus/views/SquareListInvitationView;
.super Lcom/google/android/apps/plus/views/SquareListItemView;
.source "SquareListInvitationView.java"


# instance fields
.field private mDismissButtonView:Landroid/widget/ImageView;

.field private mInvitationTextView:Landroid/widget/TextView;

.field private mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field protected mInviterGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/SquareListItemView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/SquareListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/SquareListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final init(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;ZZ)V
    .locals 7
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;
    .param p3    # Z
    .param p4    # Z

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/SquareListItemView;->init(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;ZZ)V

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterGaiaId:Ljava/lang/String;

    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0xa

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareListInvitationView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->square_invitation:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInvitationTextView:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterGaiaId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mDismissButtonView:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareListInvitationView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->square_invitation_no_name:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInvitationTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final initChildViews()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/SquareListItemView;->initChildViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInvitationTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->invitation_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInvitationTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->inviter_avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->dismiss:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListInvitationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mDismissButtonView:Landroid/widget/ImageView;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->inviter_avatar:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterGaiaId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;->onInviterImageClick(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$id;->dismiss:I

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mSquareId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;->onInvitationDismissed(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SquareListItemView;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mInviterAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mDismissButtonView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListInvitationView;->mDismissButtonView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method
