.class public final Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "TypeableAudienceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AudienceInputConnection"
.end annotation


# instance fields
.field private mAudienceTextView:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

.field final synthetic this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 1
    .param p2    # Landroid/view/inputmethod/InputConnection;
    .param p3    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    return-void
.end method


# virtual methods
.method public final deleteSurroundingText(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionEnd()I

    move-result v0

    if-lez p1, :cond_0

    if-gtz p2, :cond_0

    if-gtz v1, :cond_0

    if-gtz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    # getter for: Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->mListener:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->access$100(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->mAudienceTextView:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    # getter for: Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->mListener:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->access$100(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->mAudienceTextView:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;->onDeleteFromBeginning(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->deleteSurroundingText(II)Z

    move-result v2

    goto :goto_0
.end method

.method public final setAudienceTextView(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->mAudienceTextView:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    return-void
.end method
