.class public Lcom/google/android/apps/plus/views/ClientOobActionBar;
.super Landroid/widget/LinearLayout;
.source "ClientOobActionBar.java"


# instance fields
.field private mIsRightButtonEnabled:Z

.field private mIsRightButtonHighlighted:Z

.field private mLeftButton:Landroid/widget/Button;

.field private mRightButton:Landroid/widget/Button;

.field private mRightHighlightButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonEnabled:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private static enableButton(Landroid/widget/Button;IZLandroid/view/View$OnClickListener;)V
    .locals 1
    .param p0    # Landroid/widget/Button;
    .param p1    # I
    .param p2    # Z
    .param p3    # Landroid/view/View$OnClickListener;

    if-eqz p0, :cond_0

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {p0, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const v3, 0x102001a

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->setOrientation(I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->client_oob_action_bar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->addView(Landroid/view/View;)V

    const v1, 0x1020019

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mLeftButton:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/plus/R$id;->right_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightButton:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/plus/R$id;->right_highlight_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightHighlightButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setId(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightHighlightButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setId(I)V

    return-void
.end method


# virtual methods
.method public final enableRightButton(ILandroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View$OnClickListener;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonEnabled:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightButton:Landroid/widget/Button;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v1, p1, v0, p2}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->enableButton(Landroid/widget/Button;IZLandroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightHighlightButton:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->enableButton(Landroid/widget/Button;IZLandroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRightButtonHighlight(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonHighlighted:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mIsRightButtonEnabled:Z

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightButton:Landroid/widget/Button;

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClientOobActionBar;->mRightHighlightButton:Landroid/widget/Button;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method
