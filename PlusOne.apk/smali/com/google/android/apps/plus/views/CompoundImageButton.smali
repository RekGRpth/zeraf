.class public Lcom/google/android/apps/plus/views/CompoundImageButton;
.super Landroid/widget/ImageButton;
.source "CompoundImageButton.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;,
        Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;
    }
.end annotation


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private mBroadcasting:Z

.field private mChecked:Z

.field private mOnCheckedChangeListener:Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/views/CompoundImageButton;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CompoundImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CompoundImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private setChecked(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->refreshDrawableState()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mBroadcasting:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mBroadcasting:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mOnCheckedChangeListener:Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mOnCheckedChangeListener:Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mBroadcasting:Z

    goto :goto_0
.end method


# virtual methods
.method public isChecked()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1    # I

    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/ImageButton;->onCreateDrawableState(I)[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/views/CompoundImageButton;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/CompoundImageButton;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/ImageButton;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;->checked:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CompoundImageButton;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->requestLayout()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/widget/ImageButton;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->isChecked()Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/CompoundImageButton$SavedState;->checked:Z

    return-object v0
.end method

.method public performClick()Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/CompoundImageButton;->setChecked(ZZ)V

    invoke-super {p0}, Landroid/widget/ImageButton;->performClick()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->setChecked(ZZ)V

    return-void
.end method

.method public setOnCheckedChangeListener(Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mOnCheckedChangeListener:Lcom/google/android/apps/plus/views/CompoundImageButton$OnCheckedChangeListener;

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CompoundImageButton;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CompoundImageButton;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
