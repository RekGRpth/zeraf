.class public Lcom/google/android/apps/plus/views/LinksCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "LinksCardViewGroup.java"


# instance fields
.field protected mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field protected mBackgroundSrcRect:Landroid/graphics/Rect;

.field protected mCreationSource:Ljava/lang/String;

.field protected mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

.field protected mDbEmbedContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

.field protected mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

.field protected mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mDefaultTextPadding:I

.field protected mImageBorderRect:Landroid/graphics/Rect;

.field protected mImageDestRect:Landroid/graphics/Rect;

.field protected mImageDimension:I

.field protected mImageResource:Lcom/google/android/apps/plus/service/Resource;

.field protected mImageSrcRect:Landroid/graphics/Rect;

.field protected mLinkDescriptionLayout:Landroid/text/StaticLayout;

.field private mLinkIconRect:Landroid/graphics/Rect;

.field protected mLinkTitleLayout:Landroid/text/StaticLayout;

.field protected mLinkUrl:Ljava/lang/String;

.field private mLinkUrlCorner:Landroid/graphics/Point;

.field protected mLinkUrlLayout:Landroid/text/StaticLayout;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageBorderRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    return-void
.end method

.method private getLinkTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->bindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v2, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    iget v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method protected final createHero(III)I
    .locals 28
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v4, p3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    if-lez v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    add-int/2addr v2, v3

    sub-int/2addr v4, v2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getLinkTitle()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-nez v2, :cond_f

    :cond_1
    const/16 v24, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v2, :cond_10

    const/16 v22, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    const/16 v23, 0x1

    :goto_2
    if-eqz v22, :cond_12

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->interactivePostTitleMaxLines:I

    move/from16 v27, v0

    :goto_3
    const/16 v26, 0x0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/lit8 v26, v2, 0x0

    const/16 v2, 0xe

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v11

    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-static {v11, v0, v4, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int v26, v26, v2

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkDescriptionMaxLines:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    move/from16 v0, v27

    if-ne v0, v2, :cond_3

    const/4 v2, 0x1

    add-int/lit8 v3, v21, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v21

    :cond_3
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v26, v26, v2

    const/16 v2, 0x8

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v11

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-static {v11, v0, v4, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int v26, v26, v2

    :cond_4
    if-nez v22, :cond_5

    if-eqz v23, :cond_7

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_13

    :cond_6
    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int v26, v26, v2

    :cond_7
    :goto_4
    if-eqz v23, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v11

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v2, v2, p1

    const/4 v5, 0x0

    if-eqz v22, :cond_14

    const/4 v6, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v8, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->iconTextPadding:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    const/4 v12, 0x1

    move/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int v26, v26, v2

    :cond_8
    if-eqz v22, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v7, p1, v2

    add-int v8, p2, v26

    move-object/from16 v5, v19

    move v9, v4

    move-object/from16 v10, p0

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createDeepLinkButton(Landroid/content/Context;Ljava/lang/String;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int v26, v26, v2

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_b

    :cond_a
    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v26, v26, v2

    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    if-nez v2, :cond_c

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultLinkCardContentHeight:I

    sget-object v3, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v3, v3, 0x2

    add-int v20, v2, v3

    sub-int v2, v20, v26

    div-int/lit8 v2, v2, 0x2

    sget-object v3, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Point;->offset(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Rect;->offset(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableButton;->offset(II)V

    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int v2, v2, v26

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    sget-object v5, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    new-instance v12, Lcom/google/android/apps/plus/views/ClickableRect;

    move/from16 v13, p1

    move/from16 v14, p2

    move/from16 v15, p3

    move-object/from16 v17, p0

    invoke-direct/range {v12 .. v18}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    if-eqz v22, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    if-lez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    add-int v3, p1, p3

    sget-object v5, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    sub-int/2addr v3, v5

    sget-object v5, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v5, v5, p2

    add-int v6, p1, p3

    sget-object v7, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v6, v7

    sget-object v7, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v7, v7, p2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    add-int/2addr v7, v8

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageBorderRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int v3, v3, v25

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int v5, v5, v25

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int v6, v6, v25

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v7, v25

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    :cond_e
    add-int v2, p2, v16

    return v2

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getDescription()Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_0

    :cond_10
    const/16 v22, 0x0

    goto/16 :goto_1

    :cond_11
    const/16 v23, 0x0

    goto/16 :goto_2

    :cond_12
    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkTitleMaxLines:I

    move/from16 v27, v0

    goto/16 :goto_3

    :cond_13
    sget-object v2, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v26, v26, v2

    goto/16 :goto_4

    :cond_14
    sget-object v3, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v6, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_5
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 14
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-nez v8, :cond_10

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/high16 v10, 0x3f800000

    int-to-float v11, v8

    mul-float/2addr v10, v11

    int-to-float v11, v9

    div-float/2addr v10, v11

    const/high16 v11, 0x3f800000

    iget-object v12, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v11, v12

    iget-object v12, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    cmpl-float v10, v10, v11

    if-lez v10, :cond_11

    int-to-float v10, v9

    mul-float/2addr v10, v11

    float-to-int v10, v10

    sub-int v10, v8, v10

    div-int/lit8 v10, v10, 0x2

    iget-object v11, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    const/4 v12, 0x0

    sub-int/2addr v8, v10

    invoke-virtual {v11, v10, v12, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    :goto_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v10, v10, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v9, v9, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaOverlayPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    if-eqz v1, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v10

    if-le v8, v10, :cond_12

    iget-object v11, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    sub-int v12, v8, v10

    div-int/lit8 v12, v12, 0x2

    const/4 v13, 0x0

    add-int/2addr v8, v10

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v11, v12, v13, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    sget-object v10, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v10, v10, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageBorderRect:Landroid/graphics/Rect;

    sget-object v9, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v9, v9, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, p2, v8

    iget v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    if-nez v8, :cond_13

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    :goto_3
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_4

    add-int/lit8 v7, v6, 0x0

    move/from16 v0, p2

    int-to-float v8, v0

    add-int v9, p3, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move/from16 v0, p2

    neg-int v8, v0

    int-to-float v8, v8

    move/from16 v0, p3

    neg-int v9, v0

    sub-int/2addr v9, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_5

    move/from16 v0, p2

    int-to-float v8, v0

    add-int v9, p3, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move/from16 v0, p2

    neg-int v8, v0

    int-to-float v8, v8

    move/from16 v0, p3

    neg-int v9, v0

    sub-int/2addr v9, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    :cond_5
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v8, :cond_14

    const/4 v3, 0x1

    :goto_4
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_15

    const/4 v4, 0x1

    :goto_5
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v8, :cond_16

    const/4 v2, 0x1

    :goto_6
    if-nez v3, :cond_6

    if-eqz v4, :cond_8

    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_17

    :cond_7
    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v7, v8

    :cond_8
    :goto_7
    if-eqz v4, :cond_a

    if-nez v2, :cond_9

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v9}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Rect;->offset(II)V

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkIcon:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v10, v10, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v11, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v11, v11, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_9
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    add-int/2addr v9, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    neg-int v8, v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    neg-int v9, v9

    sub-int/2addr v9, v7

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/2addr v7, v8

    :cond_a
    if-eqz v3, :cond_b

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v8, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v7, v8

    :cond_b
    if-nez v3, :cond_c

    if-nez v4, :cond_c

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v8, :cond_c

    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_d

    :cond_c
    add-int/2addr v7, v6

    :cond_d
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableRect;->isClicked()Z

    move-result v8

    if-eqz v8, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->isPressed()Z

    move-result v8

    if-nez v8, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->isFocused()Z

    move-result v8

    if-eqz v8, :cond_f

    :cond_e
    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_f
    iget v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    if-nez v8, :cond_18

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultLinkCardContentHeight:I

    :goto_8
    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int v8, v8, p3

    return v8

    :cond_10
    iget-object v8, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    move-object v1, v8

    goto/16 :goto_0

    :cond_11
    int-to-float v10, v8

    div-float/2addr v10, v11

    float-to-int v10, v10

    sub-int v10, v9, v10

    div-int/lit8 v10, v10, 0x2

    iget-object v11, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    const/4 v12, 0x0

    sub-int/2addr v9, v10

    invoke-virtual {v11, v12, v10, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1

    :cond_12
    iget-object v11, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    const/4 v12, 0x0

    sub-int v13, v9, v10

    div-int/lit8 v13, v13, 0x2

    add-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    invoke-virtual {v11, v12, v13, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :cond_13
    iget v6, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    goto/16 :goto_3

    :cond_14
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_17
    add-int/2addr v7, v6

    goto/16 :goto_7

    :cond_18
    iget v5, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    goto :goto_8
.end method

.method public final getOneUpIntent(Z)Landroid/content/Intent;
    .locals 5
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getOneUpIntent(Z)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getLinkTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "link_title"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-nez v3, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "deep_link_label"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "link_url"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    return-object v1

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 13
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSource:Ljava/lang/String;

    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v12, 0x0

    const-wide/16 v0, 0x2000

    and-long/2addr v0, v7

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/16 v0, 0x22

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    const/4 v12, 0x1

    :cond_0
    const-wide/16 v0, 0x800

    and-long/2addr v0, v7

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/16 v0, 0x27

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-static {v10}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    :cond_1
    const/16 v0, 0x1d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-static {v11}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->getWidthForColumns(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    :cond_2
    if-eqz v12, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getProductName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    :cond_3
    return-void

    :cond_4
    const-string v0, "s:updates:esshare"

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSource:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "www."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mActivityId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSource:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;->onAppLinkClicked$5db46bd0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    goto :goto_0
.end method

.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mActivityId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSource:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;->onContentLinkClicked$599fa128(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedMedia;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mCreationSource:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDbEmbedAppInviteDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkTitleLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mBackgroundDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDestRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageBorderRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mLinkUrlCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    iput v2, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageDimension:I

    iput v2, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mDefaultTextPadding:I

    return-void
.end method

.method public unbindResources()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardViewGroup;->mImageSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method
