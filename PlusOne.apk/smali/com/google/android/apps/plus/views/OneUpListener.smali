.class public interface abstract Lcom/google/android/apps/plus/views/OneUpListener;
.super Ljava/lang/Object;
.source "OneUpListener.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# virtual methods
.method public abstract onAppInviteButtonClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V
.end method

.method public abstract onCommentButtonClicked()V
.end method

.method public abstract onCommentClicked(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;)V
.end method

.method public abstract onJoinHangout(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V
.end method

.method public abstract onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
.end method

.method public abstract onMediaClick(Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Z)V
.end method

.method public abstract onPlaceClick(Ljava/lang/String;)V
.end method

.method public abstract onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
.end method

.method public abstract onReshareClicked()V
.end method

.method public abstract onSkyjamBuyClick(Ljava/lang/String;)V
.end method

.method public abstract onSkyjamListenClick(Ljava/lang/String;)V
.end method

.method public abstract onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onSquareClick(Ljava/lang/String;Ljava/lang/String;)V
.end method
