.class final Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DetailsLayout"
.end annotation


# instance fields
.field public addresses:Landroid/view/ViewGroup;

.field public birthday:Landroid/view/View;

.field public contactSection:Landroid/view/ViewGroup;

.field public container:Landroid/view/View;

.field public deviceLocationMap:Lcom/google/android/apps/plus/views/ImageResourceView;

.field public deviceLocationMapContainer:Landroid/view/View;

.field public deviceLocationSection:Landroid/view/ViewGroup;

.field public educationSection:Landroid/view/ViewGroup;

.field public educationSectionLastLocation:Landroid/view/View;

.field public emails:Landroid/view/ViewGroup;

.field public gender:Landroid/view/View;

.field public introduction:Landroid/view/View;

.field public links:Landroid/view/ViewGroup;

.field public linksSection:Landroid/view/ViewGroup;

.field public locations:Landroid/view/ViewGroup;

.field public locationsSection:Landroid/view/ViewGroup;

.field public map:Lcom/google/android/apps/plus/views/ImageResourceView;

.field public personalSection:Landroid/view/ViewGroup;

.field public phoneNumbers:Landroid/view/ViewGroup;

.field public tagLine:Landroid/view/View;

.field public workSection:Landroid/view/ViewGroup;

.field public workSectionLastLocation:Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;-><init>()V

    return-void
.end method
