.class public Lcom/google/android/apps/plus/views/PlacesMapView;
.super Lcom/google/android/apps/plus/views/ImageResourceView;
.source "PlacesMapView.java"


# static fields
.field private static sSizePattern:Ljava/util/regex/Pattern;


# instance fields
.field private final mAspectRatio:I

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private final mMaxWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ".*size=(\\d+)x(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PlacesMapView;->sSizePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PlacesMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PlacesMapView;->setSizeCategory(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PlacesMapView;->setScaleMode(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->places_map_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->places_map_aspect_ratio:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mAspectRatio:I

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlacesMapView;->getPaddingLeft()I

    move-result v4

    sub-int v4, v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlacesMapView;->getPaddingRight()I

    move-result v5

    sub-int v0, v4, v5

    iget v4, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapWidth:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapHeight:I

    if-nez v4, :cond_1

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mMaxWidth:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mAspectRatio:I

    div-int v2, v4, v5

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlacesMapView;->getPaddingTop()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlacesMapView;->getPaddingBottom()I

    move-result v5

    add-int v1, v4, v5

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/plus/views/PlacesMapView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    iget v4, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mMaxWidth:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapHeight:I

    mul-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapWidth:I

    div-int v2, v4, v5

    goto :goto_0
.end method

.method public setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v1, Lcom/google/android/apps/plus/views/PlacesMapView;->sSizePattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapWidth:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PlacesMapView;->mBitmapHeight:I

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method
