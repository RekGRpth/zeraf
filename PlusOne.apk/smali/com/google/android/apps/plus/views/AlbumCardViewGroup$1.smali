.class final Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;
.super Landroid/widget/BaseAdapter;
.source "AlbumCardViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/AlbumCardViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    array-length v1, v1

    if-le v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v1, v1

    if-gt v1, p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v1

    if-le v1, p1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, p1

    goto :goto_0
.end method
