.class public Lcom/google/android/apps/plus/views/EventActionButtonLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventActionButtonLayout.java"


# static fields
.field private static sSpacing:I


# instance fields
.field private mImage:Landroid/widget/ImageView;

.field private mText:Landroid/widget/TextView;

.field private sInitialized:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_Details_rsvp_action_button_internal_spacing:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sSpacing:I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sInitialized:Z

    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setFocusable(Z)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setWillNotDraw(Z)V

    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$style;->EventsRsvpActionButton:I

    invoke-direct {v2, p1, p2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/high16 v5, -0x80000000

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    add-int/lit8 v4, v2, 0x0

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4, v5, v1, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->measure(Landroid/view/View;IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-static {v3, v9, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setCorner(Landroid/view/View;II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sSpacing:I

    add-int/2addr v3, v4

    add-int/lit8 v0, v3, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    sub-int v4, v2, v0

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4, v5, v1, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->measure(Landroid/view/View;IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-static {v3, v0, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setCorner(Landroid/view/View;II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    new-array v3, v6, [Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    aput-object v4, v3, v7

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->verticallyCenter(I[Landroid/view/View;)V

    sub-int v3, v2, v0

    div-int/lit8 v5, v3, 0x2

    new-array v6, v6, [Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    aput-object v3, v6, v9

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    aput-object v3, v6, v7

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v4, v3

    :goto_0
    if-ltz v4, :cond_1

    aget-object v7, v6, v4

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    if-eqz v3, :cond_0

    iget v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    add-int/2addr v8, v5

    iput v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    iget v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    add-int/2addr v8, v9

    iput v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    invoke-virtual {v7, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    add-int/lit8 v3, v4, -0x1

    move v4, v3

    goto :goto_0

    :cond_1
    invoke-static {v2, p1}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->resolveSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->resolveSize(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setMeasuredDimension(II)V

    return-void
.end method
