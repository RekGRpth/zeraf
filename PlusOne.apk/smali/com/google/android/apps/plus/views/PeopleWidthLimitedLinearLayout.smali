.class public final Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PeopleWidthLimitedLinearLayout.java"


# instance fields
.field private mMaxWidth:I

.field private mNumColumns:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x2

    new-instance v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    iget v1, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    if-lt v1, v2, :cond_0

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->mNumColumns:I

    :goto_0
    iget v1, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->mMaxWidth:I

    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->mNumColumns:I

    goto :goto_0
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleWidthLimitedLinearLayout;->mMaxWidth:I

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method
