.class public Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.super Landroid/view/ViewGroup;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;
    }
.end annotation


# static fields
.field private static sAlbumImagePadding:I

.field private static sAvatarMargin:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sFontSpacing:F

.field private static sHangoutBackgroundPaint:Landroid/graphics/Paint;

.field private static sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sIsMuteColor:I

.field private static sLinkedBodyPaint:Landroid/text/TextPaint;

.field private static sLinkedHeaderPaint:Landroid/text/TextPaint;

.field private static sLinkedIconBitmap:Landroid/graphics/Bitmap;

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sMaxHangoutAvatarsToDisplay:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlaceReviewAspectsMarginBottom:I

.field private static sPlaceReviewAspectsMarginTop:I

.field private static sReshareBodyPaint:Landroid/text/TextPaint;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sSideImageSize:I

.field private static sSkyjamIconBitmap:Landroid/graphics/Bitmap;

.field private static sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

.field private static sTextXSpacing:I

.field private static sTitleMarginBottom:I


# instance fields
.field private final mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mAclText:Ljava/lang/String;

.field private mActivityId:Ljava/lang/String;

.field private mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mAlbumViewRect:Landroid/graphics/Rect;

.field private mAnnotation:Landroid/text/Spannable;

.field private mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mAuthorId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mAuthorName:Ljava/lang/String;

.field private mCanComment:Z

.field private mCanPlusOne:Z

.field private mCanReshare:Z

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mCreationSource:Ljava/lang/String;

.field private mCreationSourceId:Ljava/lang/String;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

.field private mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

.field private mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

.field private mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

.field private mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

.field private mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

.field private mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

.field private mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

.field private mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

.field private mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mEdited:Z

.field private mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mHangoutRect:Landroid/graphics/Rect;

.field private mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

.field private mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mLinkBarTop:I

.field private final mLinkImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

.field private mLinkedBody:Landroid/text/Spannable;

.field private mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mLinkedHeader:Landroid/text/Spannable;

.field private mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mLinkedIconRect:Landroid/graphics/Rect;

.field private mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

.field private mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field private mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mLinkedImageSrcRect:Landroid/graphics/Rect;

.field private mLocation:Landroid/text/Spannable;

.field private mLocationBarTop:I

.field private final mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mMediaCount:I

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mMuteState:Ljava/lang/String;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mOriginalSquareName:Ljava/lang/String;

.field private mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

.field private mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mRequestedWidths:[I

.field private mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field protected mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mReshareContentBorder:Landroid/graphics/RectF;

.field private mReshareHeader:Landroid/text/Spannable;

.field private mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

.field private mSkyjamBarTop:I

.field private final mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mSkyjamHeader:Landroid/text/Spannable;

.field private mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mSkyjamIconRect:Landroid/graphics/Rect;

.field private mSkyjamSubheader1:Landroid/text/Spannable;

.field private mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mSkyjamSubheader2:Landroid/text/Spannable;

.field private mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

.field private mSkyjamViewRect:Landroid/graphics/Rect;

.field private mSocialBarTop:I

.field private mSourceAppData:Ljava/lang/String;

.field private final mSourceAppPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mSquareBarTop:I

.field private mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mSquareImage:Lcom/google/android/apps/plus/service/Resource;

.field private final mSquareImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

.field private mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field private mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mSquareImageSrcRect:Landroid/graphics/Rect;

.field private mSquareInvitationLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private final mSquareShareClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mTitle:Landroid/text/Spannable;

.field private mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mTotalComments:I

.field private mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareShareClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$7;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_side_image:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_content_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTextXSpacing:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_place_review_aspects_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_place_review_aspects_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_link_blue:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_music_blue:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_album_image_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAlbumImagePadding:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->card_max_hangout_avatars:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMaxHangoutAvatarsToDisplay:I

    sget v1, Lcom/google/android/apps/plus/R$color;->stream_one_up_muted:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_linked_header:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_linked_header_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_linked_header_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_linked_body:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_linked_body_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_linked_body_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_reshare_body:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_reshare_body_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_reshare_body_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sHangoutBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_media:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sHangoutBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->install(Landroid/view/View;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setWillNotDraw(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbSquareUpdate;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Landroid/text/Spannable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedMedia;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/api/services/plusi/model/PlaceReview;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    return-object v0
.end method

.method private clearLayoutState()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->unbindResources()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageSrcRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareInvitationLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageSrcRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSocialBarTop:I

    return-void
.end method

.method private measureAndLayoutHeroHangout(III)I
    .locals 23
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    if-nez v5, :cond_0

    move/from16 v22, p2

    :goto_0
    return v22

    :cond_0
    move/from16 v19, p2

    add-int/lit8 v17, p3, 0x0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, p2, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    array-length v15, v5

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    mul-int v20, v15, v5

    sub-int v5, p3, v20

    add-int/lit8 v6, v15, 0x1

    div-int v21, v5, v6

    add-int/lit8 v13, v21, 0x0

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v15, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v5, v5, v14

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int/2addr v6, v13

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int v7, v7, p2

    move/from16 v0, p2

    invoke-virtual {v5, v13, v0, v6, v7}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int v5, v5, v21

    add-int/2addr v13, v5

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_1
    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v5, v6

    add-int p2, p2, v5

    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isInProgress()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v18

    sget-object v5, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-object/from16 v0, v18

    if-ne v0, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isJoinable()Z

    move-result v5

    if-eqz v5, :cond_4

    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_enter_greenroom:I

    :goto_2
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    move/from16 v7, p2

    move-object/from16 v8, p0

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButton(Landroid/content/Context;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v5, p3, v5

    div-int/lit8 v5, v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v5, v6

    :cond_3
    :goto_3
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    move/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, p2

    invoke-direct {v5, v6, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutRect:Landroid/graphics/Rect;

    move/from16 v22, p2

    goto/16 :goto_0

    :cond_4
    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_broadcast_view:I

    goto :goto_2

    :cond_5
    new-instance v5, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v10, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    move/from16 v8, p3

    invoke-direct/range {v5 .. v12}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v6

    sub-int v6, p3, v6

    div-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x0

    move/from16 v0, p2

    invoke-virtual {v5, v6, v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getBottom()I

    move-result v5

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v5, v6

    goto :goto_3
.end method

.method private measureAndLayoutHeroImages(III)I
    .locals 25
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-nez v2, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/16 v21, 0x0

    const/high16 v22, -0x80000000

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    const/4 v3, 0x0

    move/from16 v21, p3

    aput p3, v2, v3

    :cond_1
    :goto_1
    const/4 v12, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-ge v12, v2, :cond_12

    new-instance v13, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    aget v3, v3, v12

    invoke-direct {v13, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    invoke-virtual {v2, v13}, Lcom/google/android/apps/plus/views/ImageResourceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getImageRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    const/4 v3, 0x0

    aput p3, v2, v3

    move/from16 v0, p3

    int-to-float v2, v0

    const v3, 0x3fb9999a

    div-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v21, v0

    goto :goto_1

    :cond_3
    const/high16 v20, -0x80000000

    const/4 v12, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-ge v12, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v16, v0

    :goto_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v11

    move/from16 v0, v20

    if-le v11, v0, :cond_4

    move/from16 v20, v11

    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    goto :goto_4

    :cond_6
    new-instance v19, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    iget v2, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v14

    move/from16 v0, v20

    if-le v0, v14, :cond_9

    const/high16 v2, 0x3f800000

    int-to-float v3, v14

    mul-float/2addr v2, v3

    move/from16 v0, v20

    int-to-float v3, v0

    div-float v17, v2, v3

    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v16, v0

    :goto_6
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v2

    mul-int v2, v2, p3

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v3

    div-int/2addr v2, v3

    add-int/lit8 v9, v2, 0x1

    invoke-static {v14, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    const/high16 v2, 0x3f800000

    int-to-float v3, v9

    mul-float/2addr v2, v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v3

    int-to-float v3, v3

    div-float v17, v2, v3

    :cond_7
    const/4 v12, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-ge v12, v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v16, v0

    :goto_8
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v17

    float-to-int v0, v2

    move/from16 v23, v0

    move/from16 v0, v23

    move/from16 v1, v22

    if-le v0, v1, :cond_8

    move/from16 v22, v23

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v17

    float-to-int v0, v2

    move/from16 v21, v0

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v24, v0

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    :goto_9
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    aput-object v2, v24, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    :cond_9
    const/high16 v17, 0x3f800000

    goto/16 :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    goto/16 :goto_6

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    goto :goto_8

    :cond_c
    const/4 v7, 0x0

    goto :goto_9

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_f

    const/high16 v2, 0x3f800000

    :goto_a
    move/from16 v0, p3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v15, v2

    move/from16 v0, v22

    if-le v0, v15, :cond_e

    const/high16 v2, 0x3f800000

    int-to-float v3, v15

    mul-float/2addr v2, v3

    move/from16 v0, v22

    int-to-float v3, v0

    div-float v18, v2, v3

    move/from16 v0, v22

    int-to-float v2, v0

    mul-float v2, v2, v18

    float-to-int v0, v2

    move/from16 v22, v0

    move/from16 v0, v21

    int-to-float v2, v0

    mul-float v2, v2, v18

    float-to-int v0, v2

    move/from16 v21, v0

    :cond_e
    const/4 v12, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-ge v12, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v16, v0

    :goto_c
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v17

    float-to-int v0, v2

    move/from16 v23, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v17

    float-to-int v11, v2

    move/from16 v0, v21

    if-gt v11, v0, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    aput v23, v2, v12

    :goto_d
    add-int/lit8 v12, v12, 0x1

    goto :goto_b

    :cond_f
    const v2, 0x3f666666

    goto :goto_a

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    goto :goto_c

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    const/high16 v3, 0x3f800000

    move/from16 v0, v23

    int-to-float v4, v0

    mul-float/2addr v3, v4

    move/from16 v0, v21

    int-to-float v4, v0

    mul-float/2addr v3, v4

    int-to-float v4, v11

    div-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v2, v12

    goto :goto_d

    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    move/from16 v0, v22

    move/from16 v1, v21

    invoke-direct {v3, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sub-int v2, p3, v22

    div-int/lit8 p1, v2, 0x2

    move/from16 p3, v22

    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    add-int v3, p1, p3

    add-int v4, p2, v21

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    add-int p2, p2, v21

    goto/16 :goto_0

    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    move/from16 v0, p3

    move/from16 v1, v21

    invoke-direct {v3, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_e
.end method

.method private measureAndLayoutLinkedContent(III)I
    .locals 23
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-nez v2, :cond_0

    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkBarTop:I

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v4, p2, v2

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageSrcRect:Landroid/graphics/Rect;

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableRect;

    add-int v3, p1, p3

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sub-int/2addr v3, v5

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    sub-int p3, p3, v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    new-instance v2, Landroid/graphics/Rect;

    add-int v3, p1, v22

    add-int v5, v4, v21

    move/from16 v0, p1

    invoke-direct {v2, v0, v4, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    sub-int v2, p3, v22

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTextXSpacing:I

    sub-int v8, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v10, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    add-int v3, p1, v22

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTextXSpacing:I

    add-int/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v20

    add-int v2, v4, v21

    move/from16 v0, v20

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move/from16 v8, p3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v10, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v20

    move/from16 v4, v20

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v2, v14}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTextXSpacing:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v18, p3, v2

    move/from16 v16, p1

    move/from16 v17, v4

    move-object/from16 v19, p0

    invoke-static/range {v14 .. v19}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createDeepLinkButton(Landroid/content/Context;Ljava/lang/String;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_6

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v2, v2, p2

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    add-int/2addr v2, v3

    move/from16 v0, v20

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v20

    :cond_6
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v20, v2

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    goto/16 :goto_1
.end method

.method private measureAndLayoutLocation(III)I
    .locals 18
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return p2

    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationBarTop:I

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v12, p2, v2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    sub-int v2, p3, v14

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconXPadding:I

    sub-int v5, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v6, 0xf

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v16

    move/from16 v0, v16

    if-le v0, v13, :cond_1

    move/from16 v17, v12

    sub-int v2, v16, v13

    div-int/lit8 v2, v2, 0x2

    add-int v15, v12, v2

    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    add-int v3, p1, v14

    add-int v4, v15, v13

    move/from16 v0, p1

    invoke-direct {v2, v0, v15, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconXPadding:I

    add-int/2addr v2, v14

    add-int v11, p1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v17

    invoke-virtual {v2, v11, v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v2, v3

    goto/16 :goto_0

    :cond_1
    move v15, v12

    sub-int v2, v13, v16

    div-int/lit8 v2, v2, 0x2

    add-int v17, v12, v2

    goto :goto_1
.end method

.method private measureAndLayoutPlaceReviewContent(III)I
    .locals 17
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-nez v1, :cond_0

    :goto_0
    return p2

    :cond_0
    move/from16 v14, p2

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    if-eqz v1, :cond_3

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v16

    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    invoke-interface {v1, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/Rating;

    iget-object v12, v11, Lcom/google/api/services/plusi/model/Rating;->name:Ljava/lang/String;

    iget-object v13, v11, Lcom/google/api/services/plusi/model/Rating;->ratingValue:Ljava/lang/String;

    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    if-eqz v1, :cond_1

    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/RatingClientDisplayData;->renderedRatingText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    iget-object v13, v1, Lcom/google/api/services/plusi/model/RatingClientDisplayData;->renderedRatingText:Ljava/lang/String;

    :cond_1
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$style;->ProfileLocalUserRating_AspectLabel:I

    invoke-direct {v1, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v12, v1}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v1, "\u00a0"

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$style;->ProfileLocalUserRating_AspectValue:I

    invoke-direct {v1, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v13, v1}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    add-int/lit8 v1, v16, -0x1

    if-eq v15, v1, :cond_2

    const-string v1, "  "

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_4

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    add-int v14, p2, v1

    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0xa

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v4, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v1, v0, v14}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    add-int/2addr v1, v3

    add-int/2addr v14, v1

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v3, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v5, 0xa

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v6, p3

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v1, v0, v14}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v14, v1

    :cond_5
    move/from16 p2, v14

    goto/16 :goto_0
.end method

.method private measureAndLayoutSkyjamContent(III)I
    .locals 18
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return p2

    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamBarTop:I

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v12, p1, v2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v13, p2, v2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v12, v15

    add-int v4, v13, v14

    invoke-direct {v2, v12, v13, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v15

    add-int/2addr v12, v2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p3, v2

    sub-int/2addr v2, v15

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int v5, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, v12, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int v16, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v17, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v11, v2, v3

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v15

    sub-int/2addr v12, v2

    add-int v2, v13, v14

    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v13

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v5, p3, v2

    new-instance v2, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, v12, v13}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    if-nez v16, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getRight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    move/from16 v0, v17

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getBottom()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v11, v2, v3

    move v13, v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, v12, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    move/from16 v0, v17

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v11, v2, v3

    move/from16 p2, v11

    goto/16 :goto_0
.end method

.method private measureAndLayoutSquareLinkContent(III)I
    .locals 22
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-nez v2, :cond_0

    :goto_0
    return p2

    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareBarTop:I

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v4, p2, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareName()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageSrcRect:Landroid/graphics/Rect;

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableRect;

    add-int v3, p1, p3

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sub-int/2addr v3, v5

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageClickListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    sub-int p3, p3, v2

    :cond_1
    new-instance v10, Landroid/text/SpannableString;

    invoke-direct {v10, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v21, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    const-string v2, "square"

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v10}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v5, 0x21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0, v2, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v9, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v11, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v14, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareShareClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    move-object/from16 v17, v0

    move/from16 v12, p3

    invoke-direct/range {v9 .. v17}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v2

    if-eqz v2, :cond_3

    sget v20, Lcom/google/android/apps/plus/R$string;->card_square_invite_invitation:I

    :goto_1
    new-instance v11, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v15, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v16, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v14, p3

    invoke-direct/range {v11 .. v18}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getBottom()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v2, v2, p2

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSideImageSize:I

    add-int/2addr v2, v3

    move/from16 v0, v19

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v19

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v19, v2

    goto/16 :goto_0

    :cond_3
    sget v20, Lcom/google/android/apps/plus/R$string;->communities_title:I

    goto :goto_1
.end method

.method private measureAndLayoutTitle(III)I
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, v0, v1

    goto :goto_0
.end method

.method private updateAccessibility()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateParent()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;)V
    .locals 65
    .param p1    # Landroid/database/Cursor;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->unbindResources()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v30

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v56

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->clearLayoutState()V

    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanReshare:Z

    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanPlusOne:Z

    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanComment:Z

    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    const/16 v3, 0x24

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v59

    if-eqz v59, :cond_2

    invoke-static/range {v59 .. v59}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getMarketUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    new-instance v27, Landroid/text/SpannableStringBuilder;

    invoke-direct/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getPreviewUrl()Ljava/lang/String;

    move-result-object v55

    const-string v3, "https://"

    move-object/from16 v0, v55

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v51

    if-gez v51, :cond_0

    const-string v3, "https://"

    move-object/from16 v0, v55

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v51

    :cond_0
    if-ltz v51, :cond_1

    move-object/from16 v0, v55

    move/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v55

    new-instance v44, Landroid/text/SpannableStringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->skyjam_listen:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v45, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "skyjam:listen:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v55

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v45

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual/range {v44 .. v44}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, v44

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    :cond_1
    :goto_3
    new-instance v60, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "skyjam:buy:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v60

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v60

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getArtist()Ljava/lang/String;

    move-result-object v23

    new-instance v3, Landroid/text/SpannableString;

    if-eqz v23, :cond_e

    :goto_4
    move-object/from16 v0, v23

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    :cond_2
    const-wide/16 v3, 0x2804

    and-long v3, v3, v28

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_f

    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v5

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedLink:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_3
    :goto_5
    const/16 v3, 0x28

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    if-eqz v3, :cond_10

    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    :cond_5
    :goto_6
    const/16 v3, 0x27

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v4, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v3, v7, v4}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_6
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    const/16 v3, 0x21

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSourceId:Ljava/lang/String;

    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;

    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    if-nez v3, :cond_7

    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "===> Author name was null for gaia id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v8, Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v14, 0x2

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    move-object/from16 v0, v30

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    const/4 v3, 0x1

    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v3, v4, :cond_11

    const/4 v3, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mEdited:Z

    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_12

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_one_up_is_muted:I

    move-object/from16 v0, v56

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    :goto_8
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v52

    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v53

    invoke-static/range {v52 .. v52}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static/range {v53 .. v53}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOriginalSquareName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_original_author:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v53, v4, v5

    move-object/from16 v0, v56

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v57

    :goto_9
    new-instance v60, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-static/range {v52 .. v52}, Lcom/google/android/apps/plus/phone/Intents;->makeProfileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v60

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v57

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v62

    invoke-virtual/range {v53 .. v53}, Ljava/lang/String;->length()I

    move-result v3

    add-int v61, v62, v3

    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v57

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    const/16 v4, 0x21

    move-object/from16 v0, v60

    move/from16 v1, v62

    move/from16 v2, v61

    invoke-interface {v3, v0, v1, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_8
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static/range {v22 .. v22}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    :cond_9
    const-wide/16 v3, 0x200

    and-long v3, v3, v28

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_26

    const/16 v3, 0x26

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v37

    if-eqz v37, :cond_25

    invoke-static/range {v37 .. v37}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedHangout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    array-length v3, v3

    add-int/lit8 v39, v3, -0x1

    :goto_a
    if-ltz v39, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v4, v4, v39

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v39, v39, -0x1

    goto :goto_a

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getSong()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/SpannableString;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    goto/16 :goto_3

    :cond_e
    const-string v23, ""

    goto/16 :goto_4

    :cond_f
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    goto/16 :goto_5

    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    goto/16 :goto_6

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_7

    :cond_12
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    goto/16 :goto_8

    :cond_13
    sget v3, Lcom/google/android/apps/plus/R$string;->stream_original_author_community:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v53, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOriginalSquareName:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v56

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v57

    goto/16 :goto_9

    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeGaiaIds()Ljava/util/ArrayList;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeNames()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeAvatarUrls()Ljava/util/ArrayList;

    move-result-object v24

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMaxHangoutAvatarsToDisplay:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getNumAttendees()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v49

    move/from16 v0, v49

    new-array v3, v0, [Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    const/16 v39, 0x0

    :goto_b
    move/from16 v0, v39

    move/from16 v1, v49

    if-ge v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    new-instance v12, Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v26

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move-object/from16 v13, p0

    invoke-direct/range {v12 .. v18}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    aput-object v12, v3, v39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v4, v4, v39

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v39, v39, 0x1

    goto :goto_b

    :cond_15
    invoke-static/range {v53 .. v53}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v38, v0

    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isInProgress()Z

    move-result v3

    if-eqz v3, :cond_24

    sget v3, Lcom/google/android/apps/plus/R$string;->card_hangout_state_active:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v38, v4, v5

    move-object/from16 v0, v56

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v64

    :goto_d
    invoke-static/range {v64 .. v64}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    invoke-static/range {v64 .. v64}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    :cond_16
    const/16 v43, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    const-wide/16 v3, 0x800

    and-long v3, v3, v28

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_17

    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v34

    if-eqz v34, :cond_17

    invoke-static/range {v34 .. v34}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppData:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_17

    const/16 v43, 0x1

    :cond_17
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v50

    if-eqz v50, :cond_1a

    invoke-static/range {v50 .. v50}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_19

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_27

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v40

    :goto_e
    if-eqz v40, :cond_18

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v42

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</a>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    :cond_18
    invoke-static/range {v42 .. v42}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    :cond_19
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getDescription()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1a

    invoke-static/range {v41 .. v41}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    :cond_1a
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v54

    if-eqz v54, :cond_1b

    invoke-static/range {v54 .. v54}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_1b
    const-wide/16 v3, 0x1000

    and-long v3, v3, v28

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1c

    const/16 v3, 0x25

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v35

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceReviewJson;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/PlaceReview;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    :cond_1c
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v46

    if-eqz v46, :cond_29

    invoke-static/range {v46 .. v46}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v47

    new-instance v60, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, v60

    move-object/from16 v1, v47

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v47

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-virtual/range {v47 .. v47}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    move-object/from16 v0, v60

    invoke-interface {v3, v0, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_1d
    :goto_f
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v63

    if-eqz v63, :cond_2a

    invoke-static/range {v63 .. v63}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOriginalSquareName:Ljava/lang/String;

    :goto_10
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_1f

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mEdited:Z

    if-eqz v3, :cond_1e

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_one_up_is_edited:I

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    move-object/from16 v0, v58

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setFocusable(Z)V

    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->removeAllViews()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    if-lez v3, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedEmotishare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-eqz v3, :cond_2b

    const/16 v36, 0x1

    :goto_11
    if-nez v36, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v3, :cond_2c

    :cond_20
    const/16 v21, 0x1

    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    new-array v3, v3, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    new-array v3, v3, [Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mRequestedWidths:[I

    const/16 v39, 0x0

    :goto_13
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    move/from16 v0, v39

    if-ge v0, v3, :cond_31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v4, Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, v30

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v39

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_2e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v4, v3, v39

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAlbumImagePadding:I

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAlbumImagePadding:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v39

    if-ne v0, v3, :cond_2d

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAlbumImagePadding:I

    :goto_14
    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAlbumImagePadding:I

    invoke-virtual {v4, v5, v6, v3, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setPadding(IIII)V

    :cond_21
    :goto_15
    if-eqz v21, :cond_22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setImageResourceFlags(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_missing_photo:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceBrokenDrawable(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDefaultIconEnabled(Z)V

    :cond_22
    if-eqz v36, :cond_2f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    :goto_16
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_30

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/high16 v4, 0x3f000000

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setZoomBias(F)V

    :goto_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setFadeIn(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_in_list_count:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v8, v39, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, v56

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setContentDescription(Ljava/lang/CharSequence;)V

    add-int/lit8 v39, v39, 0x1

    goto/16 :goto_13

    :cond_23
    move-object/from16 v38, v53

    goto/16 :goto_c

    :cond_24
    sget v3, Lcom/google/android/apps/plus/R$string;->card_hangout_state_inactive:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v38, v4, v5

    move-object/from16 v0, v56

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v64

    goto/16 :goto_d

    :cond_25
    const/16 v64, 0x0

    goto/16 :goto_d

    :cond_26
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v64

    goto/16 :goto_d

    :cond_27
    if-eqz v43, :cond_28

    const-string v40, ""

    goto/16 :goto_e

    :cond_28
    const/16 v40, 0x0

    goto/16 :goto_e

    :cond_29
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    move-object/from16 v47, v0

    new-instance v60, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, v60

    move-object/from16 v1, v47

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v47

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-virtual/range {v47 .. v47}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    move-object/from16 v0, v60

    invoke-interface {v3, v0, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_f

    :cond_2a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOriginalSquareName:Ljava/lang/String;

    goto/16 :goto_10

    :cond_2b
    const/16 v36, 0x0

    goto/16 :goto_11

    :cond_2c
    const/16 v21, 0x0

    goto/16 :goto_12

    :cond_2d
    const/4 v3, 0x0

    goto/16 :goto_14

    :cond_2e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v5, v6, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setPadding(IIII)V

    goto/16 :goto_15

    :cond_2f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    goto/16 :goto_16

    :cond_30
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v3, v39

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    goto/16 :goto_17

    :cond_31
    new-instance v20, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    new-instance v3, Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, v30

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v4, Lcom/google/android/apps/plus/R$color;->riviera_album_background:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I

    const/4 v5, 0x1

    if-le v3, v5, :cond_34

    sget v3, Lcom/google/android/apps/plus/R$color;->riviera_album_background:I

    :goto_18
    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$9;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$9;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->addView(Landroid/view/View;)V

    :cond_32
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    if-eqz v3, :cond_33

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v3

    if-nez v3, :cond_33

    new-instance v3, Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    move-object/from16 v0, v30

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->addView(Landroid/view/View;)V

    :cond_33
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->bindResources()V

    return-void

    :cond_34
    const/4 v3, 0x0

    goto :goto_18
.end method

.method public bindResources()V
    .locals 4

    const/4 v3, 0x2

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->bindResources()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->updateAccessibility()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    instance-of v5, p1, Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v5, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v5, v5

    if-le v5, v2, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v6, v6, v2

    invoke-interface {v5, v6, v0, v3}, Lcom/google/android/apps/plus/views/OneUpListener;->onMediaClick(Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/plus/views/OneUpListener;->onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$string;->plus_one_removed_confirmation:I

    :goto_1
    const/16 v4, 0x4000

    invoke-static {v4}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, p0, v1}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$string;->plus_one_added_confirmation:I

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/OneUpListener;->onJoinHangout(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V

    goto :goto_2

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/OneUpListener;->onReshareClicked()V

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/OneUpListener;->onCommentButtonClicked()V

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSourceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/OneUpListener;->onAppInviteButtonClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V

    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->uninstall()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSocialBarTop:I

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSocialBarTop:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationBarTop:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationBarTop:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v7, v1, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_c

    :cond_6
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkBarTop:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkBarTop:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_18

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v3, v1, v2

    div-int/lit8 v3, v3, 0x2

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int v4, v2, v1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageSrcRect:Landroid/graphics/Rect;

    sub-int/2addr v1, v3

    sub-int/2addr v2, v4

    invoke-virtual {v5, v3, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_8
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v7, v1, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamBarTop:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamBarTop:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v7, v1, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareBarTop:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareBarTop:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_13

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v3, v1, v2

    div-int/lit8 v3, v3, 0x2

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int v4, v2, v1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageSrcRect:Landroid/graphics/Rect;

    sub-int/2addr v1, v3

    sub-int/2addr v2, v4

    invoke-virtual {v5, v3, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareLinkLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareDescriptionLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sHangoutBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_1a

    move v0, v6

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    array-length v1, v1

    if-ge v0, v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_15

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v1

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_17
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImageDestRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mJoinHangoutButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutUnavailableLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1f
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->updateAccessibility()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v4

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    const/4 v4, 0x1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->isPressed()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_0

    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAlbumViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->layout(IIII)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->layout(IIII)V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 30
    .param p1    # I
    .param p2    # I

    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingLeft()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    add-int v27, v4, v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingTop()I

    move-result v28

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getMeasuredWidth()I

    move-result v26

    sub-int v4, v26, v27

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    sub-int v25, v4, v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int v5, v5, v27

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int v6, v6, v28

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v4, v0, v1, v5, v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int v4, v4, v27

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMargin:I

    add-int v13, v4, v5

    sub-int v7, v25, v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v6, v7

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v4, v5, v6, v8}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v5

    new-instance v4, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v9, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mEdited:Z

    if-eqz v4, :cond_0

    const-string v4, "   "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_is_edited:I

    invoke-virtual {v12, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    const-string v4, "   "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareStreamName()Ljava/lang/String;

    move-result-object v6

    const-string v8, "REJECTED"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareModerationState()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "   "

    invoke-virtual {v5, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    sget v10, Lcom/google/android/apps/plus/R$string;->square_oneup_acl_name_and_stream:I

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v11, v14

    const/4 v4, 0x1

    aput-object v6, v11, v4

    invoke-virtual {v12, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_0
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    new-instance v6, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    const-string v10, "square"

    invoke-direct {v6, v10}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/16 v10, 0x21

    invoke-virtual {v5, v6, v9, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    if-eqz v8, :cond_1

    new-instance v6, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v6}, Landroid/text/style/StrikethroughSpan;-><init>()V

    const/16 v8, 0x21

    invoke-virtual {v5, v6, v9, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "   "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    sget v9, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v9, 0x21

    invoke-virtual {v5, v8, v4, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    const-string v4, " "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/16 v6, 0xf

    invoke-static {v12, v6}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v6

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v9, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int v4, v4, v28

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v5, v13, v4}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v5, v13, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMargin:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    add-int v28, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    :goto_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v26

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutHeroImages(III)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v4

    if-eqz v4, :cond_17

    :cond_3
    :goto_5
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v26

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutHeroHangout(III)I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSocialBarTop:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v5

    add-int v29, v27, v25

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v12, v28, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v4

    if-eqz v4, :cond_19

    const/4 v4, 0x1

    move v9, v4

    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v4, :cond_1a

    const/4 v4, 0x1

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {v4, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v7}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanPlusOne:Z

    if-eqz v4, :cond_1e

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v9, :cond_1b

    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_8
    if-eqz v9, :cond_1c

    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_9
    if-eqz v9, :cond_1d

    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_a
    move-object/from16 v10, p0

    move/from16 v11, v27

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    add-int/2addr v4, v6

    add-int v19, v27, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanReshare:Z

    if-eqz v4, :cond_1f

    new-instance v13, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v15, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBitmap:Landroid/graphics/Bitmap;

    sget-object v16, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v17, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$string;->reshare_button_content_description:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object v14, v5

    move-object/from16 v18, p0

    move/from16 v20, v12

    invoke-direct/range {v13 .. v21}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    if-nez v4, :cond_20

    sget v4, Lcom/google/android/apps/plus/R$string;->riviera_add_comment_button:I

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    :goto_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    if-gtz v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanComment:Z

    if-eqz v4, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    if-lez v4, :cond_21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$plurals;->stream_one_up_comment_count:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    :goto_e
    new-instance v13, Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    if-lez v4, :cond_22

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v15, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commentsBitmap:Landroid/graphics/Bitmap;

    :goto_f
    sget-object v17, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v18, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v19, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanComment:Z

    if-eqz v4, :cond_23

    move-object/from16 v20, p0

    :goto_10
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    const/16 v24, 0x0

    move-object v14, v5

    move/from16 v21, v29

    move/from16 v22, v12

    invoke-direct/range {v13 .. v24}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v29, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5, v4, v12}, Landroid/graphics/Rect;->offsetTo(II)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanComment:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v4, :cond_24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v5

    :goto_11
    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v5

    :cond_6
    if-nez v4, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v5

    :cond_7
    add-int v28, v12, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingBottom()I

    move-result v4

    add-int v4, v4, v28

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setMeasuredDimension(II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    :cond_8
    return-void

    :cond_9
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "   "

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    new-instance v8, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x21

    invoke-virtual {v5, v8, v4, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v7, 0xa

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v6

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v9, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move/from16 v7, v25

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v28, v4, v5

    goto/16 :goto_2

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_d
    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v13, v28, v4

    const/4 v5, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v7, 0xa

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v6

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v9, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move/from16 v7, v25

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v27

    invoke-virtual {v4, v0, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int v5, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v4

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v6

    move v6, v4

    :goto_12
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutTitle(III)I

    move-result v4

    move v6, v4

    :cond_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_f

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLocation(III)I

    move-result v4

    move v6, v4

    :cond_f
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_10

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_11

    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLinkedContent(III)I

    move-result v6

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v6

    :cond_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_12

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSkyjamContent(III)I

    move-result v6

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v6

    :cond_12
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v7, :cond_13

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewContent(III)I

    move-result v6

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v6

    :cond_13
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v7, :cond_14

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v25

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSquareLinkContent(III)I

    move-result v4

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    :cond_14
    new-instance v6, Landroid/graphics/RectF;

    const/4 v7, 0x0

    int-to-float v5, v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getMeasuredWidth()I

    move-result v8

    int-to-float v8, v8

    int-to-float v9, v4

    invoke-direct {v6, v7, v5, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    move/from16 v28, v4

    goto/16 :goto_4

    :cond_15
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v6, v7}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move v6, v13

    goto/16 :goto_12

    :cond_16
    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutTitle(III)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLocation(III)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLinkedContent(III)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSkyjamContent(III)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewContent(III)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSquareLinkContent(III)I

    move-result v28

    goto/16 :goto_4

    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/16 v6, 0x42

    move/from16 v0, v26

    invoke-direct {v5, v0, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSkyjam:Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->init(Lcom/google/android/apps/plus/content/DbEmbedSkyjam;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;->getShouldAutoPlay()Z

    move-result v4

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamView:Lcom/google/android/apps/plus/views/OneUpSkyjamView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->startAutoPlay()V

    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamViewRect:Landroid/graphics/Rect;

    const/4 v5, 0x0

    add-int/lit8 v6, v26, 0x0

    add-int/lit8 v7, v28, 0x42

    move/from16 v0, v28

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    add-int/lit8 v28, v28, 0x42

    goto/16 :goto_5

    :cond_19
    const/4 v4, 0x0

    move v9, v4

    goto/16 :goto_6

    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v4

    goto/16 :goto_7

    :cond_1b
    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_8

    :cond_1c
    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_9

    :cond_1d
    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_a

    :cond_1e
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move/from16 v19, v27

    goto/16 :goto_b

    :cond_1f
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    goto/16 :goto_c

    :cond_20
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTotalComments:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_d

    :cond_21
    move-object/from16 v23, v16

    goto/16 :goto_e

    :cond_22
    const/4 v15, 0x0

    goto/16 :goto_f

    :cond_23
    const/16 v20, 0x0

    goto/16 :goto_10

    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_11
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->clearLayoutState()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanReshare:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanPlusOne:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCanComment:Z

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :goto_1
    return v6

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v4

    if-eqz v4, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_1

    :pswitch_2
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v5, 0x3

    invoke-interface {v4, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-void
.end method

.method public setShouldAutoPlayInformer(Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    return-void
.end method

.method public unbindResources()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedImage:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSquareImage:Lcom/google/android/apps/plus/service/Resource;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mHangoutAttendeeImages:[Lcom/google/android/apps/plus/views/ClickableAvatar;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    return-void
.end method
