.class public Lcom/google/android/apps/plus/views/CirclesButton;
.super Landroid/view/ViewGroup;
.source "CirclesButton.java"


# instance fields
.field private final mCircleCountText:Landroid/widget/TextView;

.field private final mCircleIcon:Landroid/graphics/drawable/Drawable;

.field private final mCircleIconSpacing:I

.field private mCircleNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mCirclesText:Landroid/widget/TextView;

.field private mDefaultTextColor:I

.field private mDefaultTextSize:F

.field private mFixedText:Ljava/lang/String;

.field private final mLabelSpacing:I

.field private mPadding:Landroid/graphics/Rect;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private final mSb:Ljava/lang/StringBuilder;

.field private mShowIcon:Z

.field private mShowProgressIndicator:Z

.field private mStylePaddingLeft:I

.field private mStylePaddingRight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    sget v0, Lcom/google/android/apps/plus/R$style;->CirclesButton:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/16 v7, 0x10

    const/4 v6, -0x1

    const/4 v5, -0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    iput v3, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->CircleButton:[I

    invoke-virtual {p1, p2, v1, v3, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextSize:F

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingLeft:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingRight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextSize:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    iget v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextSize:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    return-void
.end method

.method private appendCirclesText(Ljava/lang/StringBuilder;I)V
    .locals 6
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne p2, v5, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_5

    if-lez v0, :cond_1

    const-string v5, ", "

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p2, :cond_0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_4

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_3

    move v3, v1

    move v4, v0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge p2, v5, :cond_6

    const-string v5, ",\u2026"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 28
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v24, p4, p2

    sub-int v9, p5, p3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v25, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v6, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v25, v9, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    sub-int v5, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v15, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    sub-int v14, v9, v25

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    move/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int/lit8 v21, v25, 0x0

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getVisibility()I

    move-result v25

    if-nez v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int v21, v21, v25

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v25

    add-int v21, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v26, v6, v21

    div-int/lit8 v26, v26, 0x2

    add-int v13, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v13, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v13, v0, Landroid/graphics/Rect;->left:I

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v26, v5, v22

    div-int/lit8 v26, v26, 0x2

    add-int v23, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v23, v0

    :cond_3
    add-int v20, v13, v21

    move/from16 v0, v20

    if-le v0, v15, :cond_4

    move/from16 v20, v15

    :cond_4
    move/from16 v0, v22

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v25

    add-int v7, v23, v25

    if-le v7, v14, :cond_5

    move v7, v14

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    move/from16 v25, v0

    if-eqz v25, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v26, v5, v10

    div-int/lit8 v26, v26, 0x2

    add-int v11, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    add-int v26, v13, v12

    add-int v27, v11, v10

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v13, v11, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v12

    add-int v13, v13, v25

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getVisibility()I

    move-result v25

    if-nez v25, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getVisibility()I

    move-result v25

    if-nez v25, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    sub-int v20, v20, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    add-int v26, v20, v8

    move-object/from16 v0, v25

    move/from16 v1, v20

    move/from16 v2, v23

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    sub-int v20, v20, v25

    :cond_7
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getVisibility()I

    move-result v25

    if-nez v25, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v23

    move/from16 v2, v20

    invoke-virtual {v0, v13, v1, v2, v7}, Landroid/widget/TextView;->layout(IIII)V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    move/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v16

    sub-int v25, v24, v19

    div-int/lit8 v17, v25, 0x2

    sub-int v25, v9, v16

    div-int/lit8 v18, v25, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v25, v0

    add-int v26, v17, v19

    add-int v27, v18, v19

    move-object/from16 v0, v25

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ProgressBar;->layout(IIII)V

    :cond_9
    return-void

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v23

    move/from16 v2, v20

    invoke-virtual {v0, v13, v1, v2, v7}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 29
    .param p1    # I
    .param p2    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v5

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingLeft:I

    move/from16 v23, v0

    if-eqz v23, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingLeft:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/Rect;->left:I

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingRight:I

    move/from16 v23, v0

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mStylePaddingRight:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/Rect;->right:I

    :cond_2
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    if-nez v22, :cond_8

    const v21, 0x7fffffff

    :goto_0
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    if-nez v11, :cond_9

    const v9, 0x7fffffff

    :goto_1
    const/4 v13, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    move/from16 v24, v0

    add-int v13, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    :cond_3
    sub-int v23, v21, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    sub-int v15, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    const/high16 v23, -0x80000000

    move/from16 v0, v23

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/high16 v24, -0x80000000

    move/from16 v0, v24

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    const/high16 v23, 0x40000000

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_4

    add-int v23, v14, v13

    move/from16 v0, v23

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    add-int v21, v23, v24

    :cond_4
    const/high16 v23, 0x40000000

    move/from16 v0, v23

    if-eq v11, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v23

    move/from16 v0, v23

    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    move-result v23

    move/from16 v0, v23

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    add-int v9, v23, v24

    :cond_5
    move/from16 v0, v21

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->resolveSize(II)I

    move-result v21

    move/from16 v0, p2

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->resolveSize(II)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    move-object/from16 v23, v0

    if-nez v23, :cond_6

    sub-int v23, v21, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    sub-int v15, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/views/CirclesButton;->appendCirclesText(Ljava/lang/StringBuilder;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v16

    move/from16 v0, v16

    if-gt v0, v15, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    move/from16 v23, v0

    if-eqz v23, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v23, v0

    sub-int v23, v9, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    const/high16 v24, 0x40000000

    invoke-static/range {v23 .. v24}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v20

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/ProgressBar;->measure(II)V

    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v9}, Lcom/google/android/apps/plus/views/CirclesButton;->setMeasuredDimension(II)V

    return-void

    :cond_8
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v21

    goto/16 :goto_0

    :cond_9
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    goto/16 :goto_1

    :cond_a
    move v14, v15

    goto/16 :goto_2

    :cond_b
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v7, v0, :cond_c

    move v14, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/high16 v24, -0x80000000

    move/from16 v0, v24

    invoke-static {v14, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    const v14, 0x7fffffff

    add-int/lit8 v18, v7, -0x1

    :goto_4
    if-lez v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->appendCirclesText(Ljava/lang/StringBuilder;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v16

    sub-int v17, v7, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    sget v24, Lcom/google/android/apps/plus/R$plurals;->circle_button_more_circles:I

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v17

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v23, v0

    add-int v23, v23, v16

    add-int v14, v23, v8

    if-le v14, v15, :cond_d

    add-int/lit8 v18, v18, -0x1

    goto/16 :goto_4

    :cond_d
    if-le v14, v15, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v24

    sget v25, Lcom/google/android/apps/plus/R$string;->circle_button_circles:I

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/high16 v24, -0x80000000

    move/from16 v0, v24

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_3
.end method

.method public setBlockedMode(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->menu_item_unblock_profile:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->blue_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    goto :goto_0
.end method

.method public setCircles(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    return-void
.end method

.method public setHighlighted(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_by_me_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setShowIcon(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setShowProgressIndicator(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    if-eq v0, p1, :cond_2

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setHighlighted(Z)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->invalidate()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    :cond_0
    return-void
.end method
