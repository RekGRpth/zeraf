.class public Lcom/google/android/apps/plus/views/PeopleSuggestionsView;
.super Landroid/view/ViewGroup;
.source "PeopleSuggestionsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;,
        Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;
    }
.end annotation


# static fields
.field private static sColumnPadding:I

.field private static sIsInitialized:Z

.field private static sRowPadding:I


# instance fields
.field private mCachedSuggestionsLayouts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mCelebritiesView:Landroid/view/View;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mColumnWidth:I

.field private mContext:Landroid/content/Context;

.field private mFriendsAddView:Landroid/view/View;

.field private mGetMoreSuggestionsView:Landroid/view/View;

.field private mHeight:I

.field private mHeightTracker:[I

.field private mHeightTrackerIndex:I

.field private mInUseSuggestionsLayouts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

.field private mNumColumns:I

.field private mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

.field private mOrganizationViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSchoolViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateRowTopCachedHeight:I

.field private mYouMayKnowView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCachedSuggestionsLayouts:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInUseSuggestionsLayouts:Ljava/util/ArrayList;

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInflater:Landroid/view/LayoutInflater;

    sget-boolean v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sIsInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->people_row_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sRowPadding:I

    new-instance v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    sput v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sColumnPadding:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sIsInitialized:Z

    :cond_0
    return-void
.end method

.method private addCardHeight(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    aput p1, v0, v1

    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    if-eq v0, v1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeight:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    aget v1, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    aget v2, v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeight:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    iput v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    aput v3, v1, v4

    aput v3, v0, v3

    :cond_1
    return-void
.end method

.method private addPreviewCard(IIILjava/util/List;ZII)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p5    # Z
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<*>;ZII)",
            "Landroid/view/View;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    if-eqz p2, :cond_0

    invoke-virtual {v8, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    sget v0, Lcom/google/android/apps/plus/R$string;->find_people_view_all:I

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v0, p0

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private addPreviewCard(ILjava/util/List;ZII)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p3    # Z
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<*>;ZII)",
            "Landroid/view/View;"
        }
    .end annotation

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    move v3, v2

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(IIILjava/util/List;ZII)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private addPreviewCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZILjava/lang/Object;)Landroid/view/View;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # I
    .param p7    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<*>;ZI",
            "Ljava/lang/Object;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCachedSuggestionsLayouts:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCachedSuggestionsLayouts:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    move-object v4, v9

    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInUseSuggestionsLayouts:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v9, Lcom/google/android/apps/plus/R$id;->category_name:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v9, Lcom/google/android/apps/plus/R$id;->category_description:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    const/4 v9, 0x3

    new-array v9, v9, [Lcom/google/android/apps/plus/views/PeopleListRowView;

    iput-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v11, 0x0

    sget v9, Lcom/google/android/apps/plus/R$id;->row_1:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v9, v10, v11

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v11, 0x1

    sget v9, Lcom/google/android/apps/plus/R$id;->row_2:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v9, v10, v11

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v11, 0x2

    sget v9, Lcom/google/android/apps/plus/R$id;->row_3:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/PeopleListRowView;

    aput-object v9, v10, v11

    const/4 v6, 0x0

    :goto_3
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    array-length v9, v9

    if-ge v6, v9, :cond_4

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move/from16 v0, p5

    invoke-virtual {v9, p0, v10, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInflater:Landroid/view/LayoutInflater;

    sget v10, Lcom/google/android/apps/plus/R$layout;->people_suggestions:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x3

    if-le v9, v10, :cond_5

    const/4 v3, 0x3

    :goto_4
    const/4 v6, 0x0

    :goto_5
    if-ge v6, v3, :cond_6

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    invoke-virtual {v9, p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    sget v10, Lcom/google/android/apps/plus/R$id;->divider:I

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_5
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_4

    :cond_6
    move v6, v3

    :goto_6
    const/4 v9, 0x3

    if-ge v6, v9, :cond_7

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    aget-object v9, v9, v6

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setVisibility(I)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_7
    sget v9, Lcom/google/android/apps/plus/R$id;->view_all:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v9, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v9, Lcom/google/android/apps/plus/R$id;->people_suggestion_data:I

    move-object/from16 v0, p7

    invoke-virtual {v7, v9, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    if-nez p3, :cond_8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v9, Lcom/google/android/apps/plus/R$string;->find_people_view_all:I

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    :cond_8
    sget v9, Lcom/google/android/apps/plus/R$id;->view_all_text:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private static createYearDescription(Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;)Ljava/lang/String;
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    if-eqz v5, :cond_1

    move v1, v3

    :goto_0
    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    if-eqz v5, :cond_2

    move v0, v3

    :goto_1
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/PeopleListRowView;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v5, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mLayoutRowViews:[Lcom/google/android/apps/plus/views/PeopleListRowView;

    array-length v4, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v3, v1, v2

    if-eqz v3, :cond_0

    invoke-static {v3, p2}, Lcom/google/android/apps/plus/util/ViewUtils;->hasAppearedOnScreen(Landroid/view/View;Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private updateColLeft(I)I
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingLeft()I

    move-result v1

    if-ne p1, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v0, p1

    sget v1, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sColumnPadding:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private updateRowTop(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    add-int v1, p1, p2

    sget v2, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sRowPadding:I

    add-int p1, v1, v2

    :goto_0
    return p1

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mUpdateRowTopCachedHeight:I

    if-nez v1, :cond_1

    iput p2, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mUpdateRowTopCachedHeight:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mUpdateRowTopCachedHeight:I

    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mUpdateRowTopCachedHeight:I

    add-int v1, p1, v0

    sget v2, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sRowPadding:I

    add-int p1, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;)V
    .locals 13
    .param p1    # Lcom/google/api/services/plusi/model/PeopleViewDataResponse;
    .param p2    # Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;
    .param p3    # Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;
    .param p4    # Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;
    .param p5    # Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .param p6    # Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    if-eqz p4, :cond_1

    const/4 v1, 0x2

    :goto_0
    iput v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->removeAllViews()V

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    if-nez v1, :cond_2

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    :goto_1
    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    if-ge v9, v1, :cond_0

    if-nez v9, :cond_4

    move-object/from16 v10, p3

    :goto_2
    iget-object v6, p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    iget v1, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    iget v4, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->index:I

    packed-switch v1, :pswitch_data_0

    const/4 v8, 0x0

    :goto_3
    iget v1, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    packed-switch v1, :pswitch_data_1

    :cond_3
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_4
    move-object/from16 v10, p4

    goto :goto_2

    :pswitch_0
    const-string v1, "FRIEND_ADDS"

    move-object v2, v1

    :goto_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v1, 0x0

    move v5, v1

    :goto_6
    if-ge v5, v7, :cond_6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    if-eqz v3, :cond_9

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_9

    add-int/lit8 v3, v4, -0x1

    if-nez v4, :cond_5

    move-object v8, v1

    goto :goto_3

    :pswitch_1
    const-string v1, "YOU_MAY_KNOW"

    move-object v2, v1

    goto :goto_5

    :pswitch_2
    const-string v1, "SCHOOL"

    move-object v2, v1

    goto :goto_5

    :pswitch_3
    const-string v1, "ORGANIZATION"

    move-object v2, v1

    goto :goto_5

    :cond_5
    move v1, v3

    :goto_7
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v4, v1

    goto :goto_6

    :cond_6
    const/4 v8, 0x0

    goto :goto_3

    :pswitch_4
    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_friends_add:I

    sget v3, Lcom/google/android/apps/plus/R$string;->find_people_friends_add_desc:I

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    const/4 v6, 0x1

    iget v7, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(IIILjava/util/List;ZII)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    goto :goto_4

    :pswitch_5
    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_suggestions:I

    iget-object v3, v8, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    const/4 v4, 0x1

    iget v5, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(ILjava/util/List;ZII)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    goto :goto_4

    :pswitch_6
    iget-object v1, v8, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    iget-object v1, v8, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->createYearDescription(Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    const/4 v6, 0x1

    iget v7, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZILjava/lang/Object;)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    if-nez v1, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :pswitch_7
    iget-object v1, v8, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    iget-object v1, v8, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->createYearDescription(Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    const/4 v6, 0x1

    iget v7, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZILjava/lang/Object;)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    if-nez v1, :cond_8

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :pswitch_8
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/plus/R$layout;->get_more_suggestions:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->find_coworkers:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->find_classmates:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    :pswitch_9
    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_interesting:I

    iget-object v3, p2, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    const/4 v4, 0x0

    iget v5, v10, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;->type:I

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addPreviewCard(ILjava/util/List;ZII)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    goto/16 :goto_4

    :cond_9
    move v1, v4

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final getAppearedOnScreenViews(Landroid/view/View;)Ljava/util/List;
    .locals 4
    .param p1    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/PeopleListRowView;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public final onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v4, Lcom/google/android/apps/plus/R$id;->row_1:I

    if-eq v0, v4, :cond_2

    sget v4, Lcom/google/android/apps/plus/R$id;->row_2:I

    if-eq v0, v4, :cond_2

    sget v4, Lcom/google/android/apps/plus/R$id;->row_3:I

    if-ne v0, v4, :cond_3

    :cond_2
    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getPersonId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getSuggestionId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget v4, Lcom/google/android/apps/plus/R$id;->view_all:I

    if-ne v0, v4, :cond_4

    sget v4, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onFriendsAddViewAll()V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onYouMayKnowViewAll()V

    goto :goto_0

    :pswitch_3
    sget v4, Lcom/google/android/apps/plus/R$id;->people_suggestion_data:I

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onOrganizationViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V

    goto :goto_0

    :pswitch_4
    sget v4, Lcom/google/android/apps/plus/R$id;->people_suggestion_data:I

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onSchoolViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V

    goto :goto_0

    :pswitch_5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onFollowInterestingPeople()V

    goto :goto_0

    :cond_4
    sget v4, Lcom/google/android/apps/plus/R$id;->find_coworkers:I

    if-ne v0, v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onFindCoworkers()V

    goto :goto_0

    :cond_5
    sget v4, Lcom/google/android/apps/plus/R$id;->find_classmates:I

    if-ne v0, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onFindClassmates()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;->onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingTop()I

    move-result v4

    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mUpdateRowTopCachedHeight:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v7, v0

    add-int v8, v4, v2

    invoke-virtual {v6, v0, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    move-result v0

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    move-result v4

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v7, v0

    add-int v8, v4, v2

    invoke-virtual {v6, v0, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    move-result v0

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    move-result v4

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v6, v0

    add-int v7, v4, v2

    invoke-virtual {v5, v0, v4, v6, v7}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    move-result v0

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v6, v0

    add-int v7, v4, v2

    invoke-virtual {v5, v0, v4, v6, v7}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    move-result v0

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v7, v0

    add-int v8, v4, v2

    invoke-virtual {v6, v0, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    move-result v0

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    move-result v4

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    add-int/2addr v7, v0

    add-int v8, v4, v2

    invoke-virtual {v6, v0, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateColLeft(I)I

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->updateRowTop(II)I

    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingLeft()I

    move-result v7

    sub-int v7, v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingRight()I

    move-result v8

    sub-int v0, v7, v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getPaddingBottom()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeight:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTracker:[I

    iput v9, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeightTrackerIndex:I

    aput v9, v8, v10

    aput v9, v7, v9

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mNumColumns:I

    if-ne v7, v10, :cond_2

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mColumnWidth:I

    const/high16 v8, 0x40000000

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v5, v2, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    sget v7, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->sColumnPadding:I

    sub-int v7, v0, v7

    div-int/lit8 v0, v7, 0x2

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v5, v2, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mGetMoreSuggestionsView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    :cond_6
    invoke-direct {p0, v9, v10}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->addCardHeight(IZ)V

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mHeight:I

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mFriendsAddView:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mYouMayKnowView:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOrganizationViews:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mSchoolViews:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCelebritiesView:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mCachedSuggestionsLayouts:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInUseSuggestionsLayouts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->mInUseSuggestionsLayouts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final onUnblockButtonClicked(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    return-void
.end method
