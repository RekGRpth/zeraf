.class public Lcom/google/android/apps/plus/views/OneUpSkyjamView;
.super Landroid/view/View;
.source "OneUpSkyjamView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;
    }
.end annotation


# static fields
.field private static sDefaultTextPaint:Landroid/text/TextPaint;

.field private static sGoogleMusic:Landroid/graphics/Bitmap;

.field private static sInitialized:Z

.field private static sPlayIcon:Landroid/graphics/Bitmap;

.field private static sPreviewPaint:Landroid/graphics/Paint;

.field private static sStopIcon:Landroid/graphics/Bitmap;


# instance fields
.field private mActionIcon:Landroid/graphics/Bitmap;

.field private mActionIconPoint:Landroid/graphics/Point;

.field private mActivityId:Ljava/lang/String;

.field private mGoogleMusicRect:Landroid/graphics/Rect;

.field private mMusicUrl:Ljava/lang/String;

.field private mPreviewBackground:Landroid/graphics/Rect;

.field private mPreviewStatus:Ljava/lang/String;

.field private mPreviewStatusPoint:Landroid/graphics/Point;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-boolean v3, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sInitialized:Z

    sget v1, Lcom/google/android/apps/plus/R$drawable;->google_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_play:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_stop:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final init(Lcom/google/android/apps/plus/content/DbEmbedSkyjam;Ljava/lang/String;)V
    .locals 18
    .param p1    # Lcom/google/android/apps/plus/content/DbEmbedSkyjam;
    .param p2    # Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActivityId:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getMarketUrl()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getPreviewUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v8

    const-string v15, "https://"

    invoke-virtual {v11, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_0

    const-string v15, "https://"

    invoke-virtual {v11, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    :cond_0
    if-ltz v9, :cond_5

    if-eqz v8, :cond_3

    invoke-virtual {v11, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getPlaybackStatus(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getSong()Ljava/lang/String;

    move-result-object v14

    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v15, 0x100

    invoke-direct {v12, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0xa

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mTitle:Ljava/lang/String;

    :cond_1
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mTitle:Ljava/lang/String;

    :cond_2
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v15}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    sget-object v15, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    :goto_2
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    add-int/lit8 v3, v15, 0xd

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v16, v7, 0x42

    div-int/lit8 v16, v16, 0x2

    add-int v4, v15, v16

    new-instance v15, Landroid/graphics/Point;

    invoke-direct {v15, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIconPoint:Landroid/graphics/Point;

    sget-object v15, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v15}, Landroid/text/TextPaint;->descent()F

    move-result v15

    sget-object v16, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v16 .. v16}, Landroid/text/TextPaint;->ascent()F

    move-result v16

    sub-float v15, v15, v16

    float-to-int v13, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    add-int/lit8 v15, v15, 0xd

    add-int/2addr v3, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v16, v13, 0x42

    div-int/lit8 v16, v16, 0x2

    add-int v15, v15, v16

    sget-object v16, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v16 .. v16}, Landroid/text/TextPaint;->ascent()F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    sub-int v4, v15, v16

    new-instance v15, Landroid/graphics/Point;

    invoke-direct {v15, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/Point;

    sget-object v15, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sget-object v15, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    sub-int v3, v15, v6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v16, v5, 0x42

    div-int/lit8 v16, v16, 0x2

    add-int v4, v15, v16

    new-instance v15, Landroid/graphics/Rect;

    add-int v16, v3, v6

    add-int v17, v4, v5

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v15, v3, v4, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    invoke-virtual {v10, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v16, "mode=inline"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v16, "mode=inline"

    const-string v17, "mode=streaming"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "&mode=streaming"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    sget-object v15, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_2
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->registerListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz v2, :cond_1

    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    :goto_1
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "music_account"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "music_url"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "song"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "activity_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActivityId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->unregisterListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewBackground:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIconPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIconPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method public final onPlaybackStatusUpdate(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    if-ne v0, v2, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->invalidate()V

    :cond_2
    return-void

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final startAutoPlay()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/OneUpSkyjamView;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method
