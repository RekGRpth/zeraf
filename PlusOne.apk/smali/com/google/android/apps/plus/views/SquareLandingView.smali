.class public Lcom/google/android/apps/plus/views/SquareLandingView;
.super Lcom/google/android/apps/plus/views/EsScrollView;
.source "SquareLandingView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;,
        Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;
    }
.end annotation


# static fields
.field private static sMemberCountLinkTextColor:I

.field private static sMemberCountTextColor:I

.field private static sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

.field private static sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

.field private static sSelectableItemBackgroundId:I

.field private static sSquareLandingViewInitialized:Z


# instance fields
.field private mAlwaysExpanded:Z

.field private mButtonAction:I

.field private mIsExpanded:Z

.field public mMemberCountBackground:Landroid/graphics/drawable/Drawable;

.field private mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

.field private mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    sget-boolean v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    if-nez v2, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-array v3, v3, [I

    sget v4, Lcom/google/android/apps/plus/R$attr;->buttonSelectableBackground:I

    aput v4, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSelectableItemBackgroundId:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountLinkTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$color;->square_card_members:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_enabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    sget-boolean v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    if-nez v2, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-array v3, v3, [I

    sget v4, Lcom/google/android/apps/plus/R$attr;->buttonSelectableBackground:I

    aput v4, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSelectableItemBackgroundId:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountLinkTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$color;->square_card_members:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_enabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    sget-boolean v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    if-nez v2, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/SquareLandingView;->sSquareLandingViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-array v3, v3, [I

    sget v4, Lcom/google/android/apps/plus/R$attr;->buttonSelectableBackground:I

    aput v4, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sSelectableItemBackgroundId:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountLinkTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$color;->square_card_members:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_enabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/SquareLandingView;)Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/SquareLandingView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    return-object v0
.end method

.method private bindExpandArea()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->expandArea:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_up:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->expandArea:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->collapse_more_info_content_description:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_down:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->expand_more_info_content_description:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initSquareLayout()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    :cond_0
    return-void
.end method

.method private updateButtonSectionVisibility()V
    .locals 4

    const/4 v1, 0x0

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->description:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->inviteButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->shareButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->buttonSection:Landroid/view/View;

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final hideBlockingExplanation()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final init(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/16 v1, 0x8

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->initSquareLayout()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->expandArea:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->requestLayout()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->bindExpandArea()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->expandArea:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    if-eqz v3, :cond_1

    move-object v1, v2

    goto :goto_0

    :cond_1
    move v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->header:I

    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mAlwaysExpanded:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->bindExpandArea()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->requestLayout()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    if-nez v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->header:I

    if-ne v0, v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mIsExpanded:Z

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onExpandClicked(Z)V

    goto :goto_1

    :cond_4
    sget v1, Lcom/google/android/apps/plus/R$id;->join_button:I

    if-ne v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    iget v2, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mButtonAction:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onJoinButtonClicked(I)V

    goto :goto_1

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$id;->member_count:I

    if-ne v0, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onMembersClicked()V

    goto :goto_1

    :cond_6
    sget v1, Lcom/google/android/apps/plus/R$id;->share_button:I

    if-ne v0, v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onShareClicked()V

    goto :goto_1

    :cond_7
    sget v1, Lcom/google/android/apps/plus/R$id;->invite_button:I

    if-ne v0, v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onInviteClicked()V

    goto :goto_1

    :cond_8
    sget v1, Lcom/google/android/apps/plus/R$id;->settings_section:I

    if-ne v0, v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onSettingsClicked()V

    goto :goto_1

    :cond_9
    sget v1, Lcom/google/android/apps/plus/R$id;->decline_invitation_section:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onDeclineInvitationClicked()V

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/EsScrollView;->onFinishInflate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->initSquareLayout()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->setVerticalFadingEdgeEnabled(Z)V

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->setFadingEdgeLength(I)V

    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->inviteButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->shareButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsSection:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->declineInvitationSection:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->setMemberVisibility(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->hideBlockingExplanation()V

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    return-void
.end method

.method public setIsSubscribe(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/SquareLandingView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setMemberVisibility(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mMemberCountBackground:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/SquareLandingView;->sSelectableItemBackgroundId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mMemberCountBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mMemberCountBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountLinkTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/SquareLandingView;->sMemberCountTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setOnClickListener(Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    return-void
.end method

.method public setSquareAboutText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->description:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->description:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->updateButtonSectionVisibility()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->description:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSquareMemberCount(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$plurals;->square_members_count:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSquareName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSquarePhoto(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissing(Z)V

    goto :goto_0
.end method

.method public setSquareVisibility(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareVisibility:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareVisibility:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareVisibility:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v2, 0x1

    sget v1, Lcom/google/android/apps/plus/R$string;->square_public:I

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_public_small:I

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x1

    sget v1, Lcom/google/android/apps/plus/R$string;->square_private:I

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_private_small:I

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareVisibility:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final shouldHighlightOnPress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final showBlockingExplanation()V
    .locals 14

    const/4 v13, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$string;->url_param_help_privacy_block:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v3}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$string;->square_blocking_explanation:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Spanned;->length()I

    move-result v9

    const-class v10, Landroid/text/style/URLSpan;

    invoke-interface {v5, v13, v9, v10}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/text/style/URLSpan;

    array-length v9, v8

    if-lez v9, :cond_0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    aget-object v7, v8, v13

    invoke-interface {v5, v7}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v5, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    new-instance v9, Lcom/google/android/apps/plus/views/SquareLandingView$1;

    invoke-direct {v9, p0, v7}, Lcom/google/android/apps/plus/views/SquareLandingView$1;-><init>(Lcom/google/android/apps/plus/views/SquareLandingView;Landroid/text/style/URLSpan;)V

    const/16 v10, 0x21

    invoke-virtual {v0, v9, v6, v1, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v9, v9, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v9, v9, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v9, v9, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showDeclineInvitation(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->declineInvitationSection:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->declineInvitationSection:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final showSettingsSection(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsSection:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsSection:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final updateActions(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v3, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->inviteButton:Landroid/widget/Button;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->shareButton:Landroid/widget/Button;

    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->inviteButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->shareButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->updateButtonSectionVisibility()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final updateJoinButton(I)V
    .locals 6
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mButtonAction:I

    packed-switch p1, :pswitch_data_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :pswitch_0
    sget v3, Lcom/google/android/apps/plus/R$string;->square_join:I

    const/4 v2, -0x1

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_by_me_button:I

    const/4 v1, 0x1

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v4, v3}, Landroid/widget/Button;->setText(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareLandingView;->mSquareLayout:Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/SquareLandingView;->updateButtonSectionVisibility()V

    goto :goto_0

    :pswitch_1
    sget v3, Lcom/google/android/apps/plus/R$string;->square_accept_invitation:I

    const/4 v2, -0x1

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_by_me_button:I

    const/4 v1, 0x1

    goto :goto_1

    :pswitch_2
    sget v3, Lcom/google/android/apps/plus/R$string;->square_request_to_join:I

    const/4 v2, -0x1

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_by_me_button:I

    const/4 v1, 0x1

    goto :goto_1

    :pswitch_3
    sget v3, Lcom/google/android/apps/plus/R$string;->square_cancel_join_request:I

    const v2, -0xbbbbbc

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    const/4 v1, 0x1

    goto :goto_1

    :pswitch_4
    sget v3, Lcom/google/android/apps/plus/R$string;->square_invitation_required:I

    const v2, -0x333334

    sget v0, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    const/4 v1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
