.class final Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;
.super Ljava/lang/Object;
.source "ColumnGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ColumnGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecycleBin"
.end annotation


# instance fields
.field private mMaxScrap:I

.field private mRecyclerListener:Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTransientStateViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;-><init>()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    return-object p1
.end method


# virtual methods
.method public final addScrap(Landroid/view/View;I)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->viewType:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lvedroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    if-nez v2, :cond_2

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    iget v3, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->position:I

    invoke-virtual {v2, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mMaxScrap:I

    if-le p2, v2, :cond_4

    iput p2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mMaxScrap:I

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    iget v3, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->viewType:I

    aget-object v1, v2, v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mMaxScrap:I

    if-ge v2, v3, :cond_5

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    invoke-interface {v2, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final clear()V
    .locals 3

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mViewTypeCount:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    :cond_1
    return-void
.end method

.method public final clearTransientViews()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    :cond_0
    return-void
.end method

.method public final getScrapView(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v3, p1

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getTransientStateView(I)Landroid/view/View;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0
.end method

.method public final setViewTypeCount(I)V
    .locals 5
    .param p1    # I

    if-gtz p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Must have at least one view type ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " types reported)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mViewTypeCount:I

    if-ne p1, v2, :cond_1

    :goto_0
    return-void

    :cond_1
    new-array v1, p1, [Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mViewTypeCount:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    goto :goto_0
.end method
