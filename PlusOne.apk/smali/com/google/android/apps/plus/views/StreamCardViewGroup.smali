.class public Lcom/google/android/apps/plus/views/StreamCardViewGroup;
.super Landroid/view/ViewGroup;
.source "StreamCardViewGroup.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/ClickableHost;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;
    }
.end annotation


# static fields
.field protected static sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;


# instance fields
.field protected mActivityId:Ljava/lang/String;

.field protected mAvailableWidth:I

.field protected mCardPadding:Landroid/graphics/Rect;

.field private mClickableItemUpTimestamp:J

.field private final mClickableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field protected mHeightDimension:I

.field protected mOnClickListener:Landroid/view/View$OnClickListener;

.field protected mSpan:I

.field protected mWidthDimension:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mSpan:I

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setWillNotDraw(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->getBackgroundPadding()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bindResources()V
    .locals 0

    return-void
.end method

.method protected createCard$3fd9ad2e(IIIII)I
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    return p1
.end method

.method protected drawCard(Landroid/graphics/Canvas;IIIII)I
    .locals 0
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    return p6
.end method

.method protected drawableStateChanged()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->invalidate()V

    return-void
.end method

.method public final getActivityId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method protected final getAvailableWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mAvailableWidth:I

    return v0
.end method

.method protected final getAvailableWidth(Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)I
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p2    # I

    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->getWidthForColumns(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected getBackgroundPadding()Landroid/graphics/Rect;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getBackgrounds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSpan()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mSpan:I

    return v0
.end method

.method public final init(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)Lcom/google/android/apps/plus/views/StreamCardViewGroup;
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V

    return-object p0
.end method

.method protected initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    return-void
.end method

.method protected isVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected layoutCard$54d8973f()V
    .locals 0

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->getBackgrounds()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v8, v8, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    move v1, v8

    :goto_1
    if-ge v1, v10, :cond_3

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;->height:I

    if-lez v11, :cond_2

    sget-object v11, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v11, v11, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->bottom:I

    iget v13, v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;->height:I

    add-int/2addr v12, v13

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    :goto_2
    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;->drawable:Landroid/graphics/drawable/Drawable;

    sget-object v12, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v12, v12, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v11, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v11, v11, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    iput v11, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    sget-object v11, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v11, v11, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v12, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v7, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v4, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v7, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v5, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->drawCard(Landroid/graphics/Canvas;IIIII)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->isPressed()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v9, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItemUpTimestamp:J

    sub-long/2addr v0, v9

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v9

    int-to-long v9, v9

    cmp-long v0, v0, v9

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v7, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->layoutCard$54d8973f()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setMeasuredDimension(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->unbindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItemUpTimestamp:J

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mActivityId:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mSpan:I

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->invalidate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    return v8

    :pswitch_1
    iput-wide v10, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItemUpTimestamp:J

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_1
    if-ltz v1, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v2, v3, v4, v7}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->invalidate()V

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_2

    iput-object v9, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItemUpTimestamp:J

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setPressed(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->invalidate()V

    :cond_2
    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_2
    if-ltz v1, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v2, v3, v4, v8}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    or-int/2addr v0, v5

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_3
    if-nez v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v5, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_3
    iput-wide v10, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItemUpTimestamp:J

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v3, v4, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v9, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->invalidate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final postInit(Lcom/google/android/apps/plus/util/StreamLayoutInfo;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    iget v0, p1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mSpan:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->getWidthForColumns(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v4, v0, v1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mAvailableWidth:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mWidthDimension:I

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iget v5, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mAvailableWidth:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->createCard$3fd9ad2e(IIIII)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mCardPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mHeightDimension:I

    goto :goto_0
.end method

.method public final removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public unbindResources()V
    .locals 0

    return-void
.end method
