.class public final Lcom/google/android/apps/plus/views/EventCardDrawer;
.super Ljava/lang/Object;
.source "EventCardDrawer.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# static fields
.field private static sAuthorBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sEventAttendingBitmap:Landroid/graphics/Bitmap;

.field private static sEventCardPadding:I

.field private static sEventInfoBackgroundPaint:Landroid/graphics/Paint;

.field private static sEventInfoTextPaint:Landroid/text/TextPaint;

.field private static sEventMaybeBitmap:Landroid/graphics/Bitmap;

.field private static sEventNameTextPaint:Landroid/text/TextPaint;

.field private static sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

.field private static sEventTextLineSpacing:I

.field private static sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutTitle:Ljava/lang/String;

.field private static sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sInitialized:Z

.field private static sLocationBitmap:Landroid/graphics/Bitmap;

.field private static sMediaHeight:I

.field private static sMediaWidth:I

.field private static sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sOnAirPaint:Landroid/text/TextPaint;

.field private static sOnAirTitle:Ljava/lang/String;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sRibbonHeightPercentOverlap:F

.field private static sStatusGoingPaint:Landroid/text/TextPaint;

.field private static sStatusInvitedPaint:Landroid/text/TextPaint;

.field private static sStatusMaybePaint:Landroid/text/TextPaint;

.field private static sStatusNotGoingPaint:Landroid/text/TextPaint;


# instance fields
.field private mAttribution:Ljava/lang/CharSequence;

.field private mAttributionLayoutCorner:Landroid/graphics/Point;

.field private mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mBound:Z

.field private mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

.field private mCreatorLayout:Landroid/text/StaticLayout;

.field private mDateLayout:Landroid/text/StaticLayout;

.field private mDateLayoutCorner:Landroid/graphics/Point;

.field mDividerLines:[F

.field private mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mIgnoreHeight:Z

.field private mLocationIcon:Landroid/graphics/Bitmap;

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Landroid/text/StaticLayout;

.field private mLocationLayoutCorner:Landroid/graphics/Point;

.field private mNameLayout:Landroid/text/StaticLayout;

.field private mNameLayoutCorner:Landroid/graphics/Point;

.field private mRsvpBanner:Landroid/graphics/Bitmap;

.field private mRsvpBannerRect:Landroid/graphics/Rect;

.field private mRsvpIcon:Landroid/graphics/Bitmap;

.field private mRsvpIconRect:Landroid/graphics/Rect;

.field private mRsvpLayout:Landroid/text/StaticLayout;

.field private mRsvpLayoutCorner:Landroid/graphics/Point;

.field private mThemeImageRect:Landroid/graphics/Rect;

.field private mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

.field private mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

.field private mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mTimeZoneCorner:Landroid/graphics/Point;

.field private mTimeZoneLayout:Landroid/text/StaticLayout;

.field private mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->initialize(Landroid/content/Context;)V

    sget-boolean v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_avatar_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_ribbon_percent_height_overlap:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sRibbonHeightPercentOverlap:F

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventCardPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_text_line_spacing:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventTextLineSpacing:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAuthorBitmap:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$color;->event_info_background_color:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->event_name_text_color:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_name_text_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_name_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_info_text:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_info_text_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_info_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_status_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_invited:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_status_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_going:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_status_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_not_going:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_status_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_maybe:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_status_text_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$color;->event_detail_on_air:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_details_on_air_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_details_on_air_size:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_check:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventAttendingBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_not_going:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_maybe:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventMaybeBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_ribbon_blue:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_ribbon_green:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_ribbon_grey:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_location_card:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_hangout_taco:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$string;->event_hangout_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    sget v2, Lcom/google/android/apps/plus/R$string;->event_detail_on_air:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->btn_events_on_air:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaWidth(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sMediaWidth:I

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sMediaHeight:I

    sput-boolean v4, Lcom/google/android/apps/plus/views/EventCardDrawer;->sInitialized:Z

    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    const/4 v2, 0x4

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    return-void
.end method

.method private static drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V
    .locals 2
    .param p0    # Landroid/text/StaticLayout;
    .param p1    # Landroid/graphics/Point;
    .param p2    # Landroid/graphics/Canvas;

    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p0, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget v0, p1, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 11
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/graphics/Point;
    .param p5    # Landroid/text/TextPaint;
    .param p6    # Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/CardView;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p3    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->onUnbindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->clear()V

    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    const/4 v6, 0x2

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->bindResources()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->onBindResources()V

    :cond_0
    return-void
.end method

.method public final clear()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    goto :goto_0
.end method

.method public final draw$680d9d43(IILandroid/graphics/Canvas;)I
    .locals 14
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/Canvas;

    move v6, p1

    add-int v4, p1, p2

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v10, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    if-le v10, v4, :cond_1

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_2

    :cond_1
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    sget-object v12, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    if-le v10, v4, :cond_3

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_5

    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAuthorBitmap:Landroid/graphics/Bitmap;

    :cond_4
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_5
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    iget v12, v12, Landroid/graphics/Point;->y:I

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    invoke-virtual {v13}, Landroid/text/StaticLayout;->getHeight()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    if-le v10, v4, :cond_6

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_8

    :cond_6
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_7
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    :cond_8
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    if-le v10, v4, :cond_9

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_a

    :cond_9
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    :cond_a
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    add-int v5, v10, v11

    if-le v5, v4, :cond_b

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_c

    :cond_b
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move v6, v5

    :cond_c
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    add-int v8, v10, v11

    if-le v8, v4, :cond_d

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_e

    :cond_d
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move v6, v8

    :cond_e
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v10, :cond_10

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v10

    iget v9, v10, Landroid/graphics/Rect;->bottom:I

    if-le v9, v4, :cond_f

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_10

    :cond_f
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    move v6, v9

    :cond_10
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    if-eqz v10, :cond_12

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v12, v12, Landroid/graphics/Point;->y:I

    add-int/2addr v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v7

    if-le v7, v4, :cond_11

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_12

    :cond_11
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move v6, v7

    :cond_12
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    if-eqz v10, :cond_14

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    add-int v1, v10, v11

    if-le v1, v4, :cond_13

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v10, :cond_14

    :cond_13
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move v6, v1

    :cond_14
    move p1, v6

    goto/16 :goto_0
.end method

.method public final layout(IIZII)I
    .locals 28
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # I
    .param p5    # I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_1
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    sget v8, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventCardPadding:I

    mul-int/lit8 v22, v8, 0x2

    sget v26, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventTextLineSpacing:I

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v27

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    move/from16 v0, p4

    if-ne v3, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    move/from16 v0, v27

    if-ne v3, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move/from16 v0, p1

    if-eq v3, v0, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    add-int v4, p1, p4

    add-int v5, p2, v27

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    :cond_3
    sget v20, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAvatarSize:I

    add-int v19, p1, v8

    add-int v3, p2, v27

    sub-int v21, v3, v22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    add-int v4, v19, v20

    add-int v5, v21, v20

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    add-int v3, v19, v20

    add-int v2, v3, v8

    add-int v24, p2, v27

    add-int v3, p1, p4

    sub-int/2addr v3, v2

    sub-int v25, v3, v22

    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEventData;->canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v4

    if-nez v4, :cond_a

    const/4 v3, 0x0

    :goto_1
    add-int v3, v3, v24

    add-int v10, v3, v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v12, v3, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x1

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    if-eqz v4, :cond_4

    add-int v10, v3, v26

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x0

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    :cond_4
    add-int v3, v3, v26

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v14

    :cond_5
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/util/EventDateUtils;->shouldShowDay(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v5, v6, v7, v9, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x1

    move v9, v2

    move v10, v3

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEventData;->isEventHangout(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v6, v5, v4}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getDisplayString(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_6

    add-int v10, v3, v26

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x1

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    :cond_6
    if-eqz v4, :cond_7

    add-int v18, v3, v26

    new-instance v9, Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    sget-object v13, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v15, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    const/16 v16, 0x0

    move/from16 v17, v2

    invoke-direct/range {v9 .. v18}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int v3, v3, v18

    :cond_7
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v4, :cond_d

    iget-object v5, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v5, :cond_c

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_2
    sget-object v5, Lcom/google/android/apps/plus/views/EventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object v9, v4

    :cond_8
    :goto_3
    if-eqz v9, :cond_9

    add-int v3, v3, v26

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v12, 0x1

    move/from16 v4, v25

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    sub-int v23, v3, v24

    add-int v3, v24, v23

    sub-int v3, v3, p2

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_4
    sget v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sRibbonHeightPercentOverlap:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v13

    sub-int v3, v24, v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    add-int v5, v2, v25

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v6, v2, v25

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v25, v3

    sub-int v4, v3, v8

    add-int v3, v24, v8

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    const/4 v12, 0x1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v3, v5

    sub-int v5, v3, v24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v3, v24

    :goto_5
    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int v3, v3, v24

    add-int/2addr v3, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x0

    int-to-float v7, v2

    aput v7, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x1

    int-to-float v7, v3

    aput v7, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x2

    add-int/2addr v4, v2

    int-to-float v4, v4

    aput v4, v5, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v5, 0x3

    int-to-float v6, v3

    aput v6, v4, v5

    sub-int v3, v3, v24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v13

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/16 :goto_1

    :pswitch_0
    sget v4, Lcom/google/android/apps/plus/R$string;->card_event_going_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventAttendingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    :pswitch_1
    sget v4, Lcom/google/android/apps/plus/R$string;->card_event_declined_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    :pswitch_2
    sget v4, Lcom/google/android/apps/plus/R$string;->card_event_maybe_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventMaybeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    :pswitch_3
    sget v4, Lcom/google/android/apps/plus/R$string;->card_event_invited_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_c
    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto/16 :goto_2

    :cond_d
    if-eqz v5, :cond_8

    sget-object v9, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onBindResources()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    sget v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sMediaWidth:I

    sget v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sMediaHeight:I

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    return-void
.end method

.method public final onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    :cond_0
    return-void
.end method

.method public final onUnbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    return-void
.end method

.method public final unbindResources()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->onUnbindResources()V

    return-void
.end method
