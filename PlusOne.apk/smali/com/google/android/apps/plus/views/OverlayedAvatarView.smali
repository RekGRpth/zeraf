.class public Lcom/google/android/apps/plus/views/OverlayedAvatarView;
.super Landroid/widget/RelativeLayout;
.source "OverlayedAvatarView.java"


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mOverlay:Landroid/widget/ImageView;

.field private mTypeOverlay:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static create(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .locals 2
    .param p0    # Landroid/view/LayoutInflater;
    .param p1    # Landroid/view/ViewGroup;

    sget v0, Lcom/google/android/apps/plus/R$layout;->participant_tray_avatar_view:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    return-object v0
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar_image:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->type_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mTypeOverlay:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/plus/R$id;->overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mOverlay:Landroid/widget/ImageView;

    return-void
.end method

.method public setBorderResource(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->invalidate()V

    return-void
.end method

.method public setOverlayResource(I)V
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mOverlay:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->invalidate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mOverlay:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mOverlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setParticipantGaiaId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public setParticipantType(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x4

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mTypeOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :pswitch_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mTypeOverlay:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_profile_invited:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->mTypeOverlay:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_profile_sms:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
