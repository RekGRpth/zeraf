.class final Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "ColumnGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ColumnGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/ColumnGridView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/ColumnGridView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;-><init>(Lcom/google/android/apps/plus/views/ColumnGridView;)V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$602(Lcom/google/android/apps/plus/views/ColumnGridView;Z)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$800(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    # setter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$702(Lcom/google/android/apps/plus/views/ColumnGridView;I)I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v4

    if-ge v4, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$900(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$900(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v4

    if-lt v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$900(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/util/SparseBooleanArray;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->deselect(I)V

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1000(Lcom/google/android/apps/plus/views/ColumnGridView;)Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->clearTransientViews()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # invokes: Lcom/google/android/apps/plus/views/ColumnGridView;->clearAllState()V
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1100(Lcom/google/android/apps/plus/views/ColumnGridView;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->requestLayout()V

    return-void

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1200(Lcom/google/android/apps/plus/views/ColumnGridView;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v4

    if-ge v4, v3, :cond_2

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1300(Lcom/google/android/apps/plus/views/ColumnGridView;)Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-result-object v4

    const-string v5, "onchanged - clear"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1400(Lcom/google/android/apps/plus/views/ColumnGridView;)Lvedroid/support/v4/util/SparseArrayCompat;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1500(Lcom/google/android/apps/plus/views/ColumnGridView;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1600(Lcom/google/android/apps/plus/views/ColumnGridView;)[I

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1700(Lcom/google/android/apps/plus/views/ColumnGridView;)I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1800(Lcom/google/android/apps/plus/views/ColumnGridView;)[I

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/ColumnGridView;

    # getter for: Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I
    invoke-static {v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->access$1600(Lcom/google/android/apps/plus/views/ColumnGridView;)[I

    move-result-object v5

    aget v5, v5, v1

    aput v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final onInvalidated()V
    .locals 0

    return-void
.end method
