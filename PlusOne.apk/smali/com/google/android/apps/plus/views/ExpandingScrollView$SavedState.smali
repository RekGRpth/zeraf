.class Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mBigBounce:Z

.field final mExpanded:Z

.field final mExposureLand:I

.field final mExposurePort:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposureLand:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposurePort:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mBigBounce:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;ZIIZ)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Z
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    iput p3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposureLand:I

    iput p4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposurePort:I

    iput-boolean p5, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mBigBounce:Z

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposureLand:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposurePort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mBigBounce:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
