.class public Lcom/google/android/apps/plus/views/ParticipantListItemView;
.super Landroid/widget/RelativeLayout;
.source "ParticipantListItemView.java"


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mNameTextView:Landroid/widget/TextView;

.field private mPersonId:Ljava/lang/String;

.field private mPosition:I

.field private mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mNameTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public final hideSectionHeader()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mNameTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->sectionHeader:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    return-void
.end method

.method public setParticipantName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mNameTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setPersonId(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mPersonId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mPosition:I

    return-void
.end method

.method public final showSectionHeader(C)V
    .locals 2
    .param p1    # C

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
