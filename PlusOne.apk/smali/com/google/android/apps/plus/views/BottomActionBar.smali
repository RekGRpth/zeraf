.class public Lcom/google/android/apps/plus/views/BottomActionBar;
.super Landroid/widget/LinearLayout;
.source "BottomActionBar.java"


# instance fields
.field private mCenterButton:Landroid/widget/Button;

.field private mLeftButton:Landroid/widget/Button;

.field private mNumButtons:I

.field private mRightButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/BottomActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/BottomActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/BottomActionBar;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/plus/R$layout;->server_oob_action_bar:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/BottomActionBar;->addView(Landroid/view/View;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->center_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mCenterButton:Landroid/widget/Button;

    sget v2, Lcom/google/android/apps/plus/R$id;->left_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mLeftButton:Landroid/widget/Button;

    sget v2, Lcom/google/android/apps/plus/R$id;->right_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mRightButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->resetButtons()V

    return-void
.end method

.method private static setButton(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p0    # Landroid/widget/Button;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # Landroid/view/View$OnClickListener;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setId(I)V

    invoke-virtual {p0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p3, :cond_0

    invoke-virtual {p0, p3}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final addButton(IILandroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/view/View$OnClickListener;

    const v0, 0x1020019

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, p3}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final addButton(ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # Landroid/view/View$OnClickListener;

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mCenterButton:Landroid/widget/Button;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/BottomActionBar;->setButton(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mLeftButton:Landroid/widget/Button;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/BottomActionBar;->setButton(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mCenterButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mRightButton:Landroid/widget/Button;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/BottomActionBar;->setButton(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mCenterButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    goto :goto_0
.end method

.method public final getButtons()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/BottomActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v3, v1, Landroid/widget/Button;

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final resetButtons()V
    .locals 2

    const/16 v1, 0x8

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mNumButtons:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mCenterButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BottomActionBar;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method
