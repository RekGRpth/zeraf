.class public Lcom/google/android/apps/plus/views/EventDetailsCardLayout;
.super Lcom/google/android/apps/plus/views/EsScrollView;
.source "EventDetailsCardLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;
    }
.end annotation


# static fields
.field private static sBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sInitialized:Z

.field private static sPadding:I

.field private static sPaddingBottom:I

.field private static sPaddingLeft:I

.field private static sPaddingRight:I

.field private static sPaddingTop:I

.field private static sScrollingSecondaryPadding:I

.field private static sSecondaryPadding:I

.field private static sTwoSpanLayoutDividerPercentage:F


# instance fields
.field private mCardLayout:Z

.field private mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

.field private mExpanded:Z

.field private mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

.field private mLandscape:Z

.field private mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

.field private mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

.field private mUserClick:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x2

    const/4 v5, -0x2

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_secondary_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_secondary_scroll_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sScrollingSecondaryPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_devails_percent_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sTwoSpanLayoutDividerPercentage:F

    sget v2, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingTop:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingRight:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingBottom:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPadding:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sInitialized:Z

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v7, :cond_4

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-ne v2, v3, :cond_1

    move v4, v3

    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    new-instance v2, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    if-eqz v2, :cond_5

    move v2, v5

    :goto_1
    invoke-direct {v8, v6, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->addView(Landroid/view/View;)V

    new-instance v2, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    sget v4, Lcom/google/android/apps/plus/R$id;->event_header_view:I

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setId(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->addView(Landroid/view/View;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    new-instance v2, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->addView(Landroid/view/View;)V

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->addView(Landroid/view/View;)V

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    if-eqz v2, :cond_6

    move v1, v7

    :goto_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    if-eqz v2, :cond_7

    new-instance v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v2, v3, v5, v1, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_3
    return-void

    :cond_4
    move v2, v4

    goto :goto_0

    :cond_5
    move v2, v6

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_2

    :cond_7
    new-instance v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v2, v7, v5, v1, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3
.end method

.method static synthetic access$000()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingBottom:I

    return v0
.end method

.method private toggleExpansion()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setExpandState(Z)V

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setExpandState(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 8
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .param p3    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p4    # Lcom/google/android/apps/plus/views/EventActionListener;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mUserClick:Z

    if-nez v0, :cond_0

    iget-boolean v0, p3, Lcom/google/android/apps/plus/fragments/EventActiveState;->hasUserInteracted:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    if-nez v0, :cond_1

    iget-boolean v0, p3, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    if-nez v0, :cond_1

    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->toggleExpansion()V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mLandscape:Z

    if-eqz v1, :cond_3

    const/4 v4, 0x0

    :goto_2
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/content/DbPlusOneData;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v0, p1, p3, p4}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v0, p1, p3, p4}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    return-void

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    if-nez v7, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->toggleExpansion()V

    goto :goto_1

    :cond_3
    move-object v4, p0

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->toggleExpansion()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mUserClick:Z

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/EsScrollView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x0

    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/views/EsScrollView;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v6, v8, v8, v5, v7}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getMeasuredHeight()I

    move-result v0

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPadding:I

    add-int v1, v0, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    invoke-virtual {v6, v7, v8, v5, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->layout(IIII)V

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingRight:I

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-eqz v6, :cond_0

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->getMeasuredWidth()I

    move-result v7

    add-int v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->getMeasuredHeight()I

    move-result v6

    add-int v3, v1, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    invoke-virtual {v6, v7, v1, v4, v3}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setLayoutType(Z)V

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-virtual {v6, v4, v1}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->setDivider(II)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    add-int/2addr v7, v4

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    add-int/2addr v8, v4

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v1

    invoke-virtual {v6, v7, v1, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->layout(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->clearDivider()V

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sScrollingSecondaryPadding:I

    add-int v2, v3, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    add-int/2addr v7, v8

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v6, v7, v2, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x0

    const/high16 v8, 0x40000000

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/EsScrollView;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-nez v3, :cond_0

    move v3, v0

    :cond_0
    sget v4, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingTop:I

    sget v5, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingRight:I

    add-int/2addr v5, v6

    sub-int/2addr v3, v5

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getMeasuredHeight()I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPadding:I

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mExpanded:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    if-eqz v5, :cond_2

    int-to-float v5, v3

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sTwoSpanLayoutDividerPercentage:F

    mul-float/2addr v5, v6

    float-to-int v1, v5

    sub-int v5, v3, v1

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    mul-int/lit8 v6, v6, 0x2

    sub-int v2, v5, v6

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->measure(II)V

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mCardLayout:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int/2addr v4, v5

    :cond_1
    :goto_1
    sget v5, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingBottom:I

    add-int/2addr v4, v5

    move v0, v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mContainer:Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->measure(II)V

    sget v5, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingLeft:I

    add-int/2addr v5, v3

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingRight:I

    add-int/2addr v5, v6

    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->setMeasuredDimension(II)V

    return-void

    :cond_2
    move v1, v3

    sget v5, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sSecondaryPadding:I

    mul-int/lit8 v5, v5, 0x2

    sub-int v2, v3, v5

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    sget v6, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sScrollingSecondaryPadding:I

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    goto :goto_1
.end method

.method public onRecycle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mHeaderView:Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mMainLayout:Lcom/google/android/apps/plus/views/EventDetailsMainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->mSecondaryLayout:Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->clear()V

    return-void
.end method
