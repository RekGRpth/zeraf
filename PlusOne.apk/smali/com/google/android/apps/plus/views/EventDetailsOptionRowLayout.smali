.class public Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsOptionRowLayout.java"


# static fields
.field private static sDividerHeight:F

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sInitialized:Z

.field private static sMinHeight:I

.field private static sMinSideWidth:I

.field private static sPadding:I


# instance fields
.field private mFirst:Z

.field private mLeftView:Landroid/view/View;

.field private mMeasuredHeight:I

.field private mMeasuredWidth:I

.field private mRightView:Landroid/view/View;

.field private mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->clear()V

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->bind(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final bind(Ljava/lang/String;Ljava/util/List;Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->clear()V

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->bind(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public final clear()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->removeView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->removeView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->clear()V

    return-void
.end method

.method protected init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x0

    sget-boolean v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_option_min_side_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sMinSideWidth:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_option_min_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sMinHeight:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sPadding:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerHeight:F

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sInitialized:Z

    :cond_0
    new-instance v2, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    new-instance v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->addView(Landroid/view/View;)V

    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mFirst:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredWidth:I

    int-to-float v3, v0

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredHeight:I

    int-to-float v0, v0

    sget v2, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerHeight:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-float v2, v6

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredWidth:I

    int-to-float v3, v0

    int-to-float v4, v6

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/high16 v12, -0x80000000

    const/4 v11, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/4 v5, 0x0

    move v6, v7

    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mFirst:Z

    if-eqz v9, :cond_0

    const/4 v9, 0x0

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerHeight:F

    add-float/2addr v9, v10

    float-to-int v5, v9

    :cond_0
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-static {v9, v7, v12, v11, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->measure(Landroid/view/View;IIII)V

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sPadding:I

    mul-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v9, v10

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sMinSideWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-static {v9, v11, v5}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setCorner(Landroid/view/View;II)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    invoke-static {v9, v1, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setCenterBounds(Landroid/view/View;II)V

    add-int/lit8 v8, v1, 0x0

    sub-int v6, v7, v1

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-static {v9, v6, v12, v11, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->measure(Landroid/view/View;IIII)V

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sPadding:I

    mul-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v9, v10

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sMinSideWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    sub-int v10, v7, v3

    invoke-static {v9, v10, v5}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setCorner(Landroid/view/View;II)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    invoke-static {v9, v3, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setCenterBounds(Landroid/view/View;II)V

    sub-int/2addr v6, v3

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-static {v9, v6, v12, v0, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->measure(Landroid/view/View;IIII)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    invoke-static {v9, v8, v5}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setCorner(Landroid/view/View;II)V

    const/4 v9, 0x3

    new-array v2, v9, [Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mRightView:Landroid/view/View;

    aput-object v9, v2, v11

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mLeftView:Landroid/view/View;

    aput-object v10, v2, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mText:Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;

    aput-object v10, v2, v9

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sMinHeight:I

    invoke-static {v2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getMaxHeight([Landroid/view/View;)I

    move-result v10

    sget v11, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sPadding:I

    mul-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4, v2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->verticallyCenter(I[Landroid/view/View;)V

    add-int/2addr v5, v4

    int-to-float v9, v5

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->sDividerHeight:F

    add-float/2addr v9, v10

    float-to-int v5, v9

    iput v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredHeight:I

    iput v7, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredWidth:I

    iget v9, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mMeasuredHeight:I

    invoke-virtual {p0, v7, v9}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public setFirst(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->mFirst:Z

    return-void
.end method
