.class public abstract Lcom/google/android/apps/plus/views/CheckableListItemView;
.super Landroid/view/ViewGroup;
.source "CheckableListItemView.java"

# interfaces
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;
    }
.end annotation


# static fields
.field protected static final sBoldSpan:Landroid/text/style/StyleSpan;

.field private static sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

.field protected static sColorSpan:Landroid/text/style/ForegroundColorSpan;


# instance fields
.field protected mCheckBox:Landroid/widget/CheckBox;

.field protected mCheckBoxVisible:Z

.field protected mChecked:Z

.field private mListener:Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/CheckableListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CheckableListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/google/android/apps/plus/views/CheckableListItemView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CheckableListItemView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    sget v2, Lcom/google/android/apps/plus/R$color;->search_query_highlight_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/CheckableListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    :cond_0
    return-void
.end method


# virtual methods
.method protected drawBackground(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mListener:Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;->onItemCheckedChanged(Lcom/google/android/apps/plus/views/CheckableListItemView;Z)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/CheckableListItemView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->drawBackground(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setCheckBoxVisible(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->invalidate()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    return-void
.end method

.method public setOnItemCheckedChangeListener(Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mListener:Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBoxVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CheckableListItemView;->mChecked:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
