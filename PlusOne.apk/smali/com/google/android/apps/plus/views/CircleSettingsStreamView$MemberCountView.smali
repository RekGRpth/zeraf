.class Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;
.super Landroid/view/ViewGroup;
.source "CircleSettingsStreamView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/CircleSettingsStreamView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemberCountView"
.end annotation


# static fields
.field private static sCountTextColor:I

.field private static sLineHeightReduction:I

.field private static sNumberTextSize:F

.field private static sSelectorBackground:Landroid/graphics/drawable/Drawable;

.field private static sUnitTextSize:F


# instance fields
.field private mCountNumberView:Landroid/widget/TextView;

.field private mCountUnitView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sSelectorBackground:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->circle_member_count_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sSelectorBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_circle_line_height_reduction:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sLineHeightReduction:I

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sCountTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_circle_count_number_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sNumberTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_circle_count_unit_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sUnitTextSize:F

    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sNumberTextSize:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sCountTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    const/16 v2, 0x51

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sUnitTextSize:F

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sCountTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    const/16 v2, 0x31

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->addView(Landroid/view/View;)V

    # getter for: Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sCountBackgroundColor:I
    invoke-static {}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->access$000()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->setBackgroundColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sSelectorBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method final bindMemberCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$plurals;->stream_circle_settings_members_noun:I

    invoke-virtual {v1, v2, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v5, 0x0

    sub-int v2, p4, p2

    sub-int v1, p5, p3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v5, v2, v0}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sLineHeightReduction:I

    sub-int v4, v0, v4

    invoke-virtual {v3, v5, v4, v2, v1}, Landroid/widget/TextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/high16 v9, 0x40000000

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v1}, Landroid/widget/TextView;->measure(II)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v1}, Landroid/widget/TextView;->measure(II)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int v0, v2, v3

    if-le v4, v0, :cond_0

    sub-int v6, v4, v0

    div-int/lit8 v1, v6, 0x2

    :cond_0
    add-int/2addr v2, v1

    sub-int v6, v4, v2

    sget v7, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->sLineHeightReduction:I

    add-int v3, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountNumberView:Landroid/widget/TextView;

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->measure(II)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->mCountUnitView:Landroid/widget/TextView;

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->setMeasuredDimension(II)V

    return-void
.end method
