.class final Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
.super Ljava/lang/Thread;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GLThread"
.end annotation


# instance fields
.field private mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mHasSurface:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/GLTextureView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/GLTextureView;Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    iput v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    iput v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    iput-object p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    return v0
.end method

.method private guardedRun()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v17, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    const/4 v10, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v16, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v15, 0x0

    const/4 v11, 0x0

    const/4 v9, 0x0

    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldExit:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    return-void

    :catchall_0
    move-exception v17

    monitor-exit v18

    throw v17

    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Runnable;

    move-object v9, v0

    :goto_3
    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v9, :cond_12

    :try_start_4
    invoke-interface {v9}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    const/4 v5, 0x1

    :cond_4
    if-eqz v12, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    const/4 v12, 0x0

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mPreserveEGLContextOnPause:Z
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView;->access$900(Lcom/google/android/apps/plus/views/GLTextureView;)Z

    move-result v17

    if-eqz v17, :cond_6

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v17

    if-eqz v17, :cond_7

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    :cond_7
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v17

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->finish()V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    :cond_9
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    :cond_b
    if-eqz v8, :cond_c

    const/16 v16, 0x0

    const/4 v8, 0x0

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->readyToDraw()Z

    move-result v17

    if-eqz v17, :cond_11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v17, v0

    if-nez v17, :cond_d

    if-eqz v5, :cond_f

    const/4 v5, 0x0

    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v17, v0

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_e

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    const/4 v7, 0x1

    const/4 v13, 0x1

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1000(Lcom/google/android/apps/plus/views/GLTextureView;)Z

    move-result v17

    if-eqz v17, :cond_10

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    # setter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1002(Lcom/google/android/apps/plus/views/GLTextureView;Z)Z

    :goto_5
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_3

    :catchall_1
    move-exception v17

    :try_start_6
    monitor-exit v18

    throw v17
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v17

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    :try_start_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    monitor-exit v18
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    throw v17

    :cond_f
    :try_start_8
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->tryAcquireEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v17

    if-eqz v17, :cond_d

    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->start()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    const/16 v17, 0x1

    :try_start_a
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    const/4 v6, 0x1

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_4

    :catch_0
    move-exception v14

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    throw v14

    :cond_10
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    goto :goto_5

    :cond_11
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    :cond_12
    if-eqz v7, :cond_13

    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->createSurface(Landroid/graphics/SurfaceTexture;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v10, v0

    if-eqz v10, :cond_17

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v7, 0x0

    :cond_13
    if-eqz v6, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onSurfaceCreated$4a9c201c()V

    const/4 v6, 0x0

    :cond_14
    if-eqz v13, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    sget-object v20, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v21, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v22, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface/range {v18 .. v22}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v17, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    move-object/from16 v4, v17

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v15, v11}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onSurfaceChanged$4ccda93f(II)V

    const/4 v13, 0x0

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onDrawFrame$62c01aa1()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->swap()Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    move-result v17

    if-nez v17, :cond_16

    const/4 v12, 0x1

    :cond_16
    if-eqz v16, :cond_0

    const/4 v8, 0x1

    goto/16 :goto_0

    :cond_17
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    :try_start_c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    monitor-exit v18
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto/16 :goto_2

    :catchall_3
    move-exception v17

    monitor-exit v18

    throw v17

    :catchall_4
    move-exception v17

    monitor-exit v18

    throw v17
.end method

.method private readyToDraw()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopEglContextLocked()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->finish()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    :cond_0
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$500(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    :cond_0
    return-void
.end method


# virtual methods
.method public final getRenderMode()I
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onPause()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final onResume()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final onWindowResize(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    iput p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    const/4 v4, 0x1

    # setter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1002(Lcom/google/android/apps/plus/views/GLTextureView;Z)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1100(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1100(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    move-result-object v0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v4, :cond_0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v4, :cond_0

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->readyToDraw()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final requestExitAndWait()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldExit:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final requestReleaseEglContextLocked()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method public final requestRender()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final run()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GLThread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->setName(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->guardedRun()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    throw v0
.end method

.method public final setRenderMode(I)V
    .locals 2
    .param p1    # I

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final surfaceCreated()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final surfaceDestroyed()V
    .locals 2

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
