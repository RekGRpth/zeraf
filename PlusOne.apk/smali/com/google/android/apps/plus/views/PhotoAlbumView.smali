.class public Lcom/google/android/apps/plus/views/PhotoAlbumView;
.super Landroid/widget/RelativeLayout;
.source "PhotoAlbumView.java"


# instance fields
.field private mDateDisplay:Landroid/widget/TextView;

.field private mDateVisibilityState:I

.field private mFadeIn:Landroid/view/animation/AlphaAnimation;

.field private mFadeOut:Landroid/view/animation/AlphaAnimation;

.field private mResetToOpaque:Landroid/view/animation/AlphaAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const-wide/16 v4, 0xfa

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateVisibilityState:I

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeIn:Landroid/view/animation/AlphaAnimation;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeIn:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeIn:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lcom/google/android/apps/plus/views/PhotoAlbumView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/PhotoAlbumView$1;-><init>(Lcom/google/android/apps/plus/views/PhotoAlbumView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lcom/google/android/apps/plus/views/PhotoAlbumView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/PhotoAlbumView$2;-><init>(Lcom/google/android/apps/plus/views/PhotoAlbumView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mResetToOpaque:Landroid/view/animation/AlphaAnimation;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/PhotoAlbumView;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/PhotoAlbumView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final enableDateDisplay(Z)V
    .locals 1
    .param p1    # Z

    sget v0, Lcom/google/android/apps/plus/R$id;->date:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v4, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getHeight()I

    move-result v5

    div-int/lit8 v3, v5, 0x2

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v4, v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    :cond_0
    return-void

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getHeight()I

    move-result v6

    sub-int v3, v5, v6

    goto :goto_1
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDateVisibility(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateVisibilityState:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_1
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateVisibilityState:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->cancel()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mResetToOpaque:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mDateDisplay:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoAlbumView;->mFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method
