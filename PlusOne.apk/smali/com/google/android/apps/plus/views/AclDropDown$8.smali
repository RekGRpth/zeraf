.class final Lcom/google/android/apps/plus/views/AclDropDown$8;
.super Ljava/lang/Object;
.source "AclDropDown.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/AclDropDown;->createDefaultAclButton(Lcom/google/android/apps/plus/content/AudienceData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/AclDropDown;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/AclDropDown;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$700(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;->updatePostUI()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$8;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    return-void
.end method
