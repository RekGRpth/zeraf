.class public Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;
.super Landroid/widget/RelativeLayout;
.source "SuggestedPeopleListItemView.java"


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mCheckIndicator:Landroid/view/View;

.field private mNameTextView:Landroid/widget/TextView;

.field private mPersonId:Ljava/lang/String;

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->check_indicator:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mCheckIndicator:Landroid/view/View;

    return-void
.end method

.method public setChecked(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mCheckIndicator:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setParticipantName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setPersonId(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mPersonId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->mPosition:I

    return-void
.end method
