.class public Lcom/google/android/apps/plus/views/ConversationTile;
.super Landroid/widget/RelativeLayout;
.source "ConversationTile.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Tile;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;
    }
.end annotation


# instance fields
.field listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveParticipantIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mConversationRowId:Ljava/lang/Long;

.field private mEditText:Landroid/widget/EditText;

.field private rtcListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ConversationTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    new-instance v1, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/views/ConversationTile;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ConversationTile;->rtcListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mActiveParticipantIds:Ljava/util/HashSet;

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->conversation_tile:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ConversationTile;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->message_text:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ConversationTile;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mEditText:Landroid/widget/EditText;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mActiveParticipantIds:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public final addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->listeners:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->listeners:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getActiveParticipantIds()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mActiveParticipantIds:Ljava/util/HashSet;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->rtcListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->rtcListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public final onStart()V
    .locals 0

    return-void
.end method

.method public final onStop()V
    .locals 0

    return-void
.end method

.method public final onTilePause()V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendPresenceRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v6, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public final onTileResume()V
    .locals 6

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendPresenceRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)I

    :cond_0
    return-void
.end method

.method public final onTileStart()V
    .locals 0

    return-void
.end method

.method public final onTileStop()V
    .locals 0

    return-void
.end method

.method public setConversationRowId(Ljava/lang/Long;)V
    .locals 0
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;

    return-void
.end method
