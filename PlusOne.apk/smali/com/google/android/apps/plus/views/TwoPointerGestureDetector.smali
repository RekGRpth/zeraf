.class public Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TwoPointerGestureDetector.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$1;,
        Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;
    }
.end annotation


# instance fields
.field private mEndTime:J

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

.field private mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

.field private mStartTime:J

.field private mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    new-array v0, v4, [Landroid/view/MotionEvent$PointerCoords;

    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    new-array v0, v4, [Landroid/view/MotionEvent$PointerCoords;

    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method private static verifyPointerDistance([Landroid/view/MotionEvent$PointerCoords;)Z
    .locals 6
    .param p0    # [Landroid/view/MotionEvent$PointerCoords;

    const/4 v2, 0x1

    const/4 v3, 0x0

    aget-object v4, p0, v3

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    aget-object v5, p0, v2

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    aget-object v4, p0, v3

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    aget-object v5, p0, v2

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v4, 0x42c80000

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_0

    const/high16 v4, 0x437a0000

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v12, 0x5

    const/4 v9, 0x2

    const/high16 v11, 0x447a0000

    const/4 v8, 0x1

    const/4 v10, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$1;->$SwitchMap$com$google$android$apps$plus$views$TwoPointerGestureDetector$TwoPointerSwipeState:[I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v6, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    return v6

    :pswitch_0
    if-nez v0, :cond_0

    if-ne v3, v8, :cond_0

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->ONE_DOWN:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :pswitch_1
    if-ne v0, v12, :cond_3

    if-ne v3, v9, :cond_2

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->TWO_DOWN:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v10

    invoke-virtual {p2, v10, v6}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v8

    invoke-virtual {p2, v8, v6}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mStartTime:J

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    invoke-static {v6}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->verifyPointerDistance([Landroid/view/MotionEvent$PointerCoords;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->TWO_DOWN:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :cond_1
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :cond_2
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :cond_3
    if-eq v0, v8, :cond_4

    const/4 v6, 0x6

    if-ne v0, v6, :cond_0

    :cond_4
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :pswitch_2
    if-eq v3, v9, :cond_5

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :cond_5
    const/4 v6, 0x6

    if-ne v0, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v10

    invoke-virtual {p2, v10, v6}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v8

    invoke-virtual {p2, v8, v6}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mEndTime:J

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    invoke-static {v6}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->verifyPointerDistance([Landroid/view/MotionEvent$PointerCoords;)Z

    move-result v6

    if-eqz v6, :cond_6

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->ONE_UP:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :cond_6
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto :goto_0

    :pswitch_3
    if-eq v3, v8, :cond_7

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto/16 :goto_0

    :cond_7
    if-eq v0, v12, :cond_8

    if-nez v0, :cond_9

    :cond_8
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto/16 :goto_0

    :cond_9
    if-ne v0, v8, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mEndTime:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x64

    cmp-long v6, v6, v8

    if-lez v6, :cond_a

    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    goto/16 :goto_0

    :cond_a
    sget-object v6, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;->INITIAL:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mTwoSwipeState:Lcom/google/android/apps/plus/views/TwoPointerGestureDetector$TwoPointerSwipeState;

    iget-wide v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mEndTime:J

    iget-wide v8, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mStartTime:J

    sub-long v1, v6, v8

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-lez v6, :cond_0

    const-wide/16 v6, 0x1f4

    cmp-long v6, v1, v6

    if-gtz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v10

    iget v6, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v7, v7, v10

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v11

    long-to-float v7, v1

    div-float v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v10

    iget v6, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v7, v7, v10

    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v11

    long-to-float v7, v1

    div-float v5, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerStart:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v6, v6, v10

    iget-object v7, p0, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->mPointerEnd:[Landroid/view/MotionEvent$PointerCoords;

    aget-object v7, v7, v10

    invoke-virtual {p0, v6, v7, v4, v5}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent$PointerCoords;
    .param p2    # Landroid/view/MotionEvent$PointerCoords;
    .param p3    # F
    .param p4    # F

    const/4 v0, 0x1

    return v0
.end method
