.class public Lcom/google/android/apps/plus/views/CardLayout;
.super Landroid/widget/RelativeLayout;
.source "CardLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sInitialized:Z

.field private static sPaddingBottom:I

.field private static sPaddingLeft:I

.field private static sPaddingRight:I

.field private static sPaddingTop:I

.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    sget-boolean v1, Lcom/google/android/apps/plus/views/CardLayout;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingBottom:I

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CardLayout;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/CardLayout;->sInitialized:Z

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CardLayout;->setBackgroundResource(I)V

    sget v1, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingLeft:I

    sget v2, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingTop:I

    sget v3, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingRight:I

    sget v4, Lcom/google/android/apps/plus/views/CardLayout;->sPaddingBottom:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/CardLayout;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public onRecycle()V
    .locals 0

    return-void
.end method
