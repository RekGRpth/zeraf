.class public Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "StreamOneUpCommentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sAvatarMarginRight:I

.field private static sAvatarSize:I

.field private static sBackgroundFadePaint:Landroid/graphics/Paint;

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sDividerThickness:I

.field private static sFlaggedCommentFadeArea:Landroid/graphics/Rect;

.field private static sFontSpacing:F

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sMarginTop:I

.field private static sNameMarginRight:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlusOneColor:I

.field private static sPlusOneInverseColor:I

.field private static sPlusOnePaint:Landroid/text/TextPaint;

.field protected static sPressedStateBackground:Landroid/graphics/drawable/Drawable;

.field private static sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAuthorAvatarUrl:Ljava/lang/String;

.field private mAuthorId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mAuthorName:Ljava/lang/String;

.field private mBackgroundPaint:Landroid/graphics/Paint;

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCommentContent:Ljava/lang/String;

.field private mCommentId:Ljava/lang/String;

.field private mContentDescriptionDirty:Z

.field private mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mIsFlagged:Z

.field private mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mPlusOneByMe:Z

.field private mPlusOneCount:I

.field private mPlusOneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setFocusable(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_name_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNameMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_divider_thickness:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sget v1, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_plus_one:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_plus_one_inverse:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneInverseColor:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_body:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_plus_one_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_plus_one_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background_fade:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;Z)V
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setComment(Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setDate(J)V

    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setPlusOne([B)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->requestLayout()V

    return-void
.end method

.method public bindResources()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    return-void
.end method

.method public final getAuthorId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlusOneByMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    return v0
.end method

.method public final getPlusOneCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    return v0
.end method

.method public final getPlusOneId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    return-object v0
.end method

.method public invalidate()V
    .locals 3

    const/16 v2, 0xa

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    iget v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    if-lez v1, :cond_3

    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    :cond_4
    return-void
.end method

.method public final isFlagged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->bindResources()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpListener;->onCommentClicked(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    const/4 v8, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getHeight()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mBackgroundPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    int-to-float v3, v7

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v4

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v1, v6, v1

    invoke-virtual {v0, v8, v8, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    int-to-float v1, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v0, v6, v0

    int-to-float v2, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    sub-int v0, v7, v0

    int-to-float v3, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v0, v6, v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void

    :cond_5
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingLeft()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    add-int v12, v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingTop()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginTop:I

    add-int v13, v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getMeasuredWidth()I

    move-result v11

    sub-int v1, v11, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    sub-int v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->unbindResources()V

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorAvatarUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v2, v12

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v3, v13

    invoke-virtual {v1, v12, v13, v2, v3}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    add-int/2addr v1, v2

    add-int v9, v12, v1

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v4, v1

    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v1, v1

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v3, v1, v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    const/4 v4, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1, v9, v13}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNameMarginRight:I

    add-int/2addr v1, v2

    add-int/2addr v1, v9

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int v14, v1, v2

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v1, v12

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    add-int v15, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    add-int v16, v13, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    if-lez v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    const/4 v6, 0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " &nbsp; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneInverseColor:I

    :goto_0
    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v7, v1, v2, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    const/16 v1, 0x21

    invoke-interface {v7, v3, v2, v4, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    move-object v2, v7

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v4, v14

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v1

    add-int v1, v1, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->bindResources()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginBottom:I

    add-int/2addr v1, v13

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    add-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setMeasuredDimension(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    move-object/from16 v0, p0

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneColor:I

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto :goto_1
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->unbindResources()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :goto_1
    return v4

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    goto :goto_1

    :pswitch_2
    iput-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v5, 0x3

    invoke-interface {v4, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorAvatarUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    const-string v0, "StreamOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===> Author name was null for gaia id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    :cond_2
    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    goto :goto_0
.end method

.method public setComment(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    return-void
.end method

.method public setCustomBackground(Landroid/graphics/Paint;)V
    .locals 0
    .param p1    # Landroid/graphics/Paint;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mBackgroundPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setDate(J)V
    .locals 1
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    return-void
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-void
.end method

.method public setPlusOne([B)V
    .locals 3
    .param p1    # [B

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    goto :goto_0
.end method

.method public setPressed(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isPressed()Z

    move-result v0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->setPressed(Z)V

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    :cond_0
    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    return-void
.end method
