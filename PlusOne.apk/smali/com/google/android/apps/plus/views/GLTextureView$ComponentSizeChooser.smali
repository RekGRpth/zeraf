.class Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;
.super Lcom/google/android/apps/plus/views/GLTextureView$BaseConfigChooser;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComponentSizeChooser"
.end annotation


# instance fields
.field protected mAlphaSize:I

.field protected mBlueSize:I

.field protected mDepthSize:I

.field protected mGreenSize:I

.field protected mRedSize:I

.field protected mStencilSize:I

.field private mValue:[I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/GLTextureView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/GLTextureView;IIIIII)V
    .locals 4
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3024

    aput v2, v0, v1

    aput p2, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3022

    aput v2, v0, v1

    const/4 v1, 0x5

    aput p4, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p5, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p6, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p7, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/GLTextureView$BaseConfigChooser;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;[I)V

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    iput p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mRedSize:I

    iput p3, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mGreenSize:I

    iput p4, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mBlueSize:I

    iput p5, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mAlphaSize:I

    iput p6, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mDepthSize:I

    iput p7, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mStencilSize:I

    return-void
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2
    .param p1    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3    # Ljavax/microedition/khronos/egl/EGLConfig;
    .param p4    # I
    .param p5    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    invoke-interface {p1, p2, p3, p4, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    aget v0, v1, v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 16
    .param p1    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3    # [Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v8, p3

    move-object/from16 v0, p3

    array-length v13, v0

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v13, :cond_1

    aget-object v4, v8, v12

    const/16 v5, 0x3025

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    const/16 v5, 0x3026

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v15

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mDepthSize:I

    if-lt v10, v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mStencilSize:I

    if-lt v15, v1, :cond_0

    const/16 v5, 0x3024

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v14

    const/16 v5, 0x3023

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v11

    const/16 v5, 0x3022

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    const/16 v5, 0x3021

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v7

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mRedSize:I

    if-ne v14, v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mGreenSize:I

    if-ne v11, v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mBlueSize:I

    if-ne v9, v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mAlphaSize:I

    if-ne v7, v1, :cond_0

    :goto_1
    return-object v4

    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
