.class public Lcom/google/android/apps/plus/views/PromoCardViewGroup;
.super Lcom/google/android/apps/plus/views/StreamCardViewGroup;
.source "PromoCardViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;
    }
.end annotation


# static fields
.field protected static sBackgrounds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

.field protected mPromoType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->sBackgrounds:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    sget-object v1, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->roundedCardBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;-><init>(Lcom/google/android/apps/plus/views/StreamCardViewGroup;Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->sBackgrounds:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected final getBackgroundPadding()Landroid/graphics/Rect;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->streamCardBorderPadding:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected final getBackgrounds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->sBackgrounds:Ljava/util/List;

    return-object v0
.end method

.method public final getPromoTypeForLogging()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->mPromoType:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "103"

    goto :goto_0

    :pswitch_1
    const-string v0, "104"

    goto :goto_0

    :pswitch_2
    const-string v0, "105"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->retrievePromoType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->mPromoType:I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V

    return-void
.end method

.method protected final isVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onRecycle()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->onRecycle()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->mPromoType:I

    return-void
.end method

.method public setPromoActionListener(Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    return-void
.end method
