.class final Lcom/google/android/apps/plus/views/AnimatedThemeView$1;
.super Ljava/lang/Object;
.source "AnimatedThemeView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/AnimatedThemeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    # setter for: Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$002(Lcom/google/android/apps/plus/views/AnimatedThemeView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    # getter for: Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$000(Lcom/google/android/apps/plus/views/AnimatedThemeView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    # getter for: Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$000(Lcom/google/android/apps/plus/views/AnimatedThemeView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    # invokes: Lcom/google/android/apps/plus/views/AnimatedThemeView;->openVideo()V
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$200(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V

    :cond_0
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$002(Lcom/google/android/apps/plus/views/AnimatedThemeView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;->this$0:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    # invokes: Lcom/google/android/apps/plus/views/AnimatedThemeView;->release()V
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->access$100(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V

    return-void
.end method
