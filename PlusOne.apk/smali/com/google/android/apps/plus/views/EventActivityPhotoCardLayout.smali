.class public Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;
.super Lcom/google/android/apps/plus/views/CardViewLayout;
.source "EventActivityPhotoCardLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sAvatarMarginBottom:I

.field private static sAvatarMarginLeft:I

.field private static sAvatarSize:I

.field private static sImageMarginBottom:I

.field private static sInitialized:Z

.field private static sTextMarginLeft:I

.field private static sTextMarginRight:I


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

.field private mGaiaId:Ljava/lang/String;

.field private mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPending:Z

.field private mPendingTextView:Landroid/widget/TextView;

.field private mPhotoData:[B

.field private mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BLcom/google/android/apps/plus/views/EventActionListener;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # [B
    .param p7    # Lcom/google/android/apps/plus/views/EventActionListener;
    .param p8    # Ljava/lang/String;

    const/16 v2, 0x8

    const/4 v5, 0x1

    const/4 v3, 0x0

    iput-object p6, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPhotoData:[B

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPhotoData:[B

    invoke-virtual {v1, v4}, Lcom/google/api/services/plusi/model/DataPhotoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataPhoto;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataPhoto;->isPanorama:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {v1, p5, v0}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    if-eqz v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    if-eqz v4, :cond_7

    :goto_2
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p3, p4}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3, v5}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mGaiaId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-interface {v1, v2, v3, p8}, Lcom/google/android/apps/plus/views/EventActionListener;->onPhotoUpdateNeeded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v1, :cond_5

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    const-string v4, "PENDING"

    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_0

    :cond_6
    move v1, v3

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_2
.end method

.method public final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_text_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sTextMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_text_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sTextMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_photo_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sImageMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_photo_avatar_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarMarginBottom:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sInitialized:Z

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_photo_pending_background_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_photo_pending_text_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->card_event_photo_missing_video:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected measureChildren(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    sget v13, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sImageMarginBottom:I

    sub-int v6, v4, v13

    add-int/lit8 v5, v6, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPending:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPendingTextView:Landroid/widget/TextView;

    const/high16 v14, 0x40000000

    invoke-static {v12, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/high16 v15, 0x40000000

    invoke-static {v6, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->measure(II)V

    :goto_0
    sget v13, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarMarginLeft:I

    add-int/lit8 v1, v13, 0x0

    sget v13, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarSize:I

    add-int v2, v1, v13

    add-int/lit8 v13, v4, 0x0

    sget v14, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarMarginBottom:I

    sub-int/2addr v13, v14

    sget v14, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarSize:I

    sub-int v3, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-static {v13, v1, v3}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v14, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarSize:I

    const/high16 v15, 0x40000000

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    sget v15, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sAvatarSize:I

    const/high16 v16, 0x40000000

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    sget v13, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sTextMarginLeft:I

    add-int v9, v13, v2

    sub-int v13, v12, v9

    sget v14, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->sTextMarginRight:I

    sub-int v11, v13, v14

    sub-int v7, v4, v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    const/high16 v14, 0x40000000

    invoke-static {v11, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/high16 v15, -0x80000000

    invoke-static {v7, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->getMeasuredHeight()I

    move-result v8

    const/4 v13, 0x0

    sub-int v14, v7, v8

    div-int/lit8 v14, v14, 0x2

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v13

    add-int v10, v5, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-static {v13, v9, v10}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->setCorner(Landroid/view/View;II)V

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/high16 v14, 0x40000000

    invoke-static {v12, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/high16 v15, 0x40000000

    invoke-static {v6, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/plus/views/ImageResourceView;->measure(II)V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPhotoData:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mDataPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mGaiaId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/EventActionListener;->onPhotoClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mImageResourceView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mPhotoData:[B

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->clear()V

    return-void
.end method
