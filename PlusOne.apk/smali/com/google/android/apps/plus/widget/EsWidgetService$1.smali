.class final Lcom/google/android/apps/plus/widget/EsWidgetService$1;
.super Ljava/lang/Object;
.source "EsWidgetService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/widget/EsWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/widget/EsWidgetService;

.field final synthetic val$authorId:Ljava/lang/String;

.field final synthetic val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/widget/EsWidgetService;Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->this$0:Lcom/google/android/apps/plus/widget/EsWidgetService;

    iput-object p2, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    iput-object p3, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$authorId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    iget-object v0, v0, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;
    invoke-static {}, Lcom/google/android/apps/plus/widget/EsWidgetService;->access$100()Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    iget-object v1, v1, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v2, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    iget v2, v2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->width:I

    iget-object v3, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$content:Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    iget v3, v3, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->height:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/ImageResourceManager;->preloadMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;II)V

    :cond_0
    # getter for: Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;
    invoke-static {}, Lcom/google/android/apps/plus/widget/EsWidgetService;->access$100()Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetService$1;->val$authorId:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/ImageResourceManager;->preloadAvatarByGaiaId(Ljava/lang/String;IZ)V

    return-void
.end method
