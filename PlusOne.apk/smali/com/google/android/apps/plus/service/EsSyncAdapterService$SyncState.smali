.class public final Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
.super Ljava/lang/Object;
.source "EsSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SyncState"
.end annotation


# instance fields
.field private mCanceled:Z

.field private mCurrentCount:I

.field private mCurrentMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

.field private mCurrentOperation:Ljava/lang/String;

.field private mCurrentOperationStart:J

.field private mCurrentSubCount:I

.field private mFullSync:Z

.field private final mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mStartTimestamp:J

.field private mSyncName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mOperations:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z

    return p1
.end method

.method private static declared-synchronized logSyncStats(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 11
    .param p0    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-class v5, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    monitor-enter v5

    :try_start_0
    const-string v4, "EsSyncAdapterService"

    const/4 v6, 0x4

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EsSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mSyncName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " finished. Elapsed time: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mStartTimestamp:J

    sub-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;

    iget v4, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->count:I

    if-nez v4, :cond_0

    iget-wide v0, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->duration:J

    :goto_1
    const-string v4, "EsSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "  ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->operation:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] items: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->count:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", sub-items: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->subCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", duration: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->duration:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms, avg: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->metrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    const-string v6, "EsSyncAdapterService"

    const-string v7, "    "

    invoke-virtual {v4, v6, v7}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :cond_0
    :try_start_1
    iget-wide v6, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->duration:J

    long-to-double v6, v6

    iget v4, v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->count:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-double v8, v4

    div-double/2addr v6, v8

    double-to-long v0, v6

    goto :goto_1

    :cond_1
    monitor-exit v5

    return-void
.end method

.method private declared-synchronized onFinish(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentOperation:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->operation:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentOperationStart:J

    sub-long/2addr v1, v3

    iput-wide v1, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->duration:J

    iput p1, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->count:I

    iput p2, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->subCount:I

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iput-object v1, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;->metrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public final declared-synchronized cancel()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    return-object v0
.end method

.method public final declared-synchronized incrementCount()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized incrementCount(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized incrementSubCount()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentSubCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentSubCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized isCanceled()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onFinish()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I

    iget v1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentSubCount:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onFinish(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onStart(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentOperation:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentOperationStart:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentCount:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentSubCount:I

    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCurrentMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onSyncFinish()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->logSyncStats(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onSyncStart(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    const-string v0, "EsSyncAdapterService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsSyncAdapterService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " started."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mSyncName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mCanceled:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mStartTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final pollAccountSyncRequest()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public final declared-synchronized requestAccountSync(Landroid/os/Bundle;)Z
    .locals 3
    .param p1    # Landroid/os/Bundle;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v1

    if-nez p1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object p1, v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final setFullSync(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z

    return-void
.end method
