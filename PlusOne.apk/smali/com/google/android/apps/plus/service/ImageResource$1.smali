.class final Lcom/google/android/apps/plus/service/ImageResource$1;
.super Ljava/lang/Object;
.source "ImageResource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/ImageResource;->deliverData([BZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/ImageResource;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/ImageResource;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageResource$1;->this$0:Lcom/google/android/apps/plus/service/ImageResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource$1;->this$0:Lcom/google/android/apps/plus/service/ImageResource;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Out of memory while decoding image: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageResource$1;->this$0:Lcom/google/android/apps/plus/service/ImageResource;

    iget-object v2, v2, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource$1;->this$0:Lcom/google/android/apps/plus/service/ImageResource;

    iget-object v0, v0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->dump()V

    return-void
.end method
