.class public final Lcom/google/android/apps/plus/service/ResourceDownloader;
.super Ljava/lang/Object;
.source "ResourceDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;,
        Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;
    }
.end annotation


# static fields
.field private static sRequestFilter:Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;


# instance fields
.field private mNetwork:Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;

.field private mRequestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/service/ResourceDownloader;->sRequestFilter:Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/apps/plus/service/SharedByteArrayPool;->getInstance()Lcom/android/volley/toolbox/ByteArrayPool;

    move-result-object v1

    new-instance v0, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v2, Lcom/android/volley/toolbox/HurlStack;

    invoke-direct {v2}, Lcom/android/volley/toolbox/HurlStack;-><init>()V

    invoke-direct {v0, v2, v1}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;Lcom/android/volley/toolbox/ByteArrayPool;)V

    new-instance v2, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;-><init>(Landroid/content/Context;Lcom/android/volley/Network;Lcom/android/volley/toolbox/ByteArrayPool;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mNetwork:Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;

    new-instance v2, Lcom/android/volley/RequestQueue;

    new-instance v3, Lcom/android/volley/toolbox/NoCache;

    invoke-direct {v3}, Lcom/android/volley/toolbox/NoCache;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mNetwork:Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;

    const/4 v5, 0x2

    new-instance v6, Lcom/android/volley/ExecutorDelivery;

    invoke-direct {v6, p2}, Lcom/android/volley/ExecutorDelivery;-><init>(Landroid/os/Handler;)V

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;ILcom/android/volley/ResponseDelivery;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2}, Lcom/android/volley/RequestQueue;->start()V

    return-void
.end method


# virtual methods
.method public final cancelDownload(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    sget-object v1, Lcom/google/android/apps/plus/service/ResourceDownloader;->sRequestFilter:Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/ResourceDownloader;->sRequestFilter:Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;

    iput-object p1, v0, Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;->resource:Lcom/google/android/apps/plus/service/Resource;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    sget-object v2, Lcom/google/android/apps/plus/service/ResourceDownloader;->sRequestFilter:Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;

    invoke-virtual {v0, v2}, Lcom/android/volley/RequestQueue;->cancelAll(Lcom/android/volley/RequestQueue$RequestFilter;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final downloadResource(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v1, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;-><init>(Lcom/google/android/apps/plus/service/Resource;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method
