.class final Lcom/google/android/apps/plus/service/EsService$19;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/EsService;->processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:Ljava/lang/String;

.field final synthetic val$fingerprint:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$photoId:J

.field final synthetic val$userId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$19;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$eventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$fingerprint:Ljava/lang/String;

    iput-wide p6, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$photoId:J

    iput-object p8, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$userId:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    const/4 v8, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$eventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$fingerprint:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$photoId:J

    iget-object v6, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$userId:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsEventData;->updateDataPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$19;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$intent:Landroid/content/Intent;

    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$19;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$19;->val$intent:Landroid/content/Intent;

    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v8, v7}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v0, v1, v2, v8}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_0
.end method
