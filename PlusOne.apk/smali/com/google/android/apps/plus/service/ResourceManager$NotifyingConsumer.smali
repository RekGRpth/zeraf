.class final Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;
.super Ljava/lang/Object;
.source "ResourceManager.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ResourceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotifyingConsumer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/ResourceManager;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/service/ResourceManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/service/ResourceManager;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;-><init>(Lcom/google/android/apps/plus/service/ResourceManager;)V

    return-void
.end method


# virtual methods
.method public final bindResources()V
    .locals 0

    return-void
.end method

.method public final onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/plus/service/ResourceManager;->access$100(Lcom/google/android/apps/plus/service/ResourceManager;Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final unbindResources()V
    .locals 0

    return-void
.end method
