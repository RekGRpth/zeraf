.class public abstract Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;
.super Ljava/lang/Object;
.source "ResourceManager.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ResourceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "BlockingLoader"
.end annotation


# instance fields
.field private mBoundResource:Lcom/google/android/apps/plus/service/Resource;

.field private final mDone:Landroid/os/ConditionVariable;

.field private mHttpStatus:I

.field private mResource:Ljava/lang/Object;

.field private mResourceId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

.field private mStatus:I

.field final synthetic this$0:Lcom/google/android/apps/plus/service/ResourceManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/ResourceManager;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mDone:Landroid/os/ConditionVariable;

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called on a background thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    # getter for: Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/service/ResourceManager;->access$200(Lcom/google/android/apps/plus/service/ResourceManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    # getter for: Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/plus/service/ResourceManager;->access$200(Lcom/google/android/apps/plus/service/ResourceManager;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mDone:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mStatus:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/service/ResourceUnavailableException;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mResourceId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    iget v2, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mStatus:I

    iget v3, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mHttpStatus:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/ResourceUnavailableException;-><init>(Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;II)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mResource:Ljava/lang/Object;

    return-object v0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mBoundResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mStatus:I

    iget v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mStatus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mResourceId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getHttpStatusCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mHttpStatus:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mResource:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    # getter for: Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/service/ResourceManager;->access$200(Lcom/google/android/apps/plus/service/ResourceManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->this$0:Lcom/google/android/apps/plus/service/ResourceManager;

    # getter for: Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/plus/service/ResourceManager;->access$200(Lcom/google/android/apps/plus/service/ResourceManager;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mDone:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    :cond_0
    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mBoundResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mBoundResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;->mBoundResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
