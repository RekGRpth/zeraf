.class final Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;
.super Ljava/lang/Object;
.source "Resource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/Resource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResourceConsumerHolder"
.end annotation


# instance fields
.field consumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

.field tag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->consumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->tag:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final matches(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->consumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->tag:Ljava/lang/Object;

    if-nez v1, :cond_2

    if-nez p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->tag:Ljava/lang/Object;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
