.class public final Lcom/google/android/apps/plus/service/ImageResourceManager;
.super Lcom/google/android/apps/plus/service/ResourceManager;
.source "ImageResourceManager.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/plus/service/ImageResourceManager;


# instance fields
.field private final mActiveResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/apps/plus/service/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

.field private final mImageCache:Lvedroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;",
            "Lcom/google/android/apps/plus/service/ImageResource;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxCacheableImageSize:I

.field private mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

.field private mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/service/ResourceManager;-><init>(Landroid/content/Context;)V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    iput-object v5, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iput-object v5, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    const/16 v5, 0x30

    if-lt v4, v5, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$integer;->config_image_cache_max_bytes_decoded_large:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    div-int/lit8 v5, v5, 0x4

    mul-int/lit16 v5, v5, 0x400

    mul-int/lit16 v5, v5, 0x400

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_0
    div-int/lit8 v4, v1, 0x3

    iput v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMaxCacheableImageSize:I

    new-instance v4, Lcom/google/android/apps/plus/service/ImageResourceManager$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/plus/service/ImageResourceManager$1;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;I)V

    iput-object v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    :try_start_0
    new-array v0, v1, [B

    const-string v4, "ImageResourceManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "ImageResourceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Grew heap by "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$integer;->config_image_cache_max_bytes_decoded_small:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v4, "ImageResourceManager"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "ImageResourceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not grow heap by "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ImageResourceManager;
    .param p1    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
    .param p2    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method private getAvatar(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
    .param p2    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AvatarResource;

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Lvedroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AvatarResource;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AvatarResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAvatar [CACHED]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/AvatarResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Lvedroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/service/AvatarResource;->register(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-object v0

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/service/AvatarResource;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/service/AvatarResource;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AvatarResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAvatar [NOT CACHED]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/AvatarResource;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AvatarResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAvatar [ACTIVE]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/AvatarResource;->logDebug(Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    goto :goto_1
.end method

.method private getAvatarIdentifier(Ljava/lang/String;Ljava/lang/String;IZI)Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .param p5    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->getNextInPool()Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V

    :goto_0
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->init(Ljava/lang/String;Ljava/lang/String;IZI)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;-><init>()V

    goto :goto_0
.end method

.method private getBlockingAvatar(Ljava/lang/String;Ljava/lang/String;IZI)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    or-int/lit8 v5, p5, 0x10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarIdentifier(Ljava/lang/String;Ljava/lang/String;IZI)Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/service/ImageResourceManager$2;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/plus/service/ImageResourceManager$2;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager$2;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/service/ImageResourceManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/ImageResourceManager;->sInstance:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/ImageResourceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageResourceManager;->sInstance:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/ImageResourceManager;->sInstance:Lcom/google/android/apps/plus/service/ImageResourceManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMedia(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;
    .param p2    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Lvedroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMedia [CACHED]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/MediaResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Lvedroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/service/MediaResource;->register(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-object v0

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/service/MediaResource;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/service/MediaResource;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMedia [NOT CACHED]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/MediaResource;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMedia [ACTIVE]: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/MediaResource;->logDebug(Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    goto :goto_1
.end method

.method private getMediaIdentifier(Lcom/google/android/apps/plus/api/MediaRef;IIII)Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->getNextInPool()Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V

    :goto_0
    move v1, p5

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->init(ILcom/google/android/apps/plus/api/MediaRef;III)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final dump()V
    .locals 5

    const-string v2, "ImageResourceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ImageResourceManager contains "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resources"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/Resource;

    const/4 v2, 0x4

    const-string v3, "ImageResourceManager"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getAvatar(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarIdentifier(Ljava/lang/String;Ljava/lang/String;IZI)Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    move-result-object v6

    invoke-direct {p0, v6, p4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method public final getAvatarByGaiaId(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarIdentifier(Ljava/lang/String;Ljava/lang/String;IZI)Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    move-result-object v6

    invoke-direct {p0, v6, p4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;

    move-result-object v0

    return-object v0
.end method

.method public final getBlockingAvatar(Ljava/lang/String;IZ)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x2

    move-object v0, p0

    move-object v2, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;Ljava/lang/String;IZI)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getBlockingAvatar(Ljava/lang/String;IZI)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/16 v5, 0x28

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;Ljava/lang/String;IZI)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getBlockingAvatarByGaiaId(Ljava/lang/String;IZ)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;Ljava/lang/String;IZI)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/service/ResourceUnavailableException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager$3;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/api/MediaRef;III)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, v3

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaIdentifier(Lcom/google/android/apps/plus/api/MediaRef;IIII)Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    move-result-object v6

    invoke-direct {p0, v6, p4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public final getMedia(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    return-object v0
.end method

.method public final getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaIdentifier(Lcom/google/android/apps/plus/api/MediaRef;IIII)Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    move-result-object v6

    invoke-direct {p0, v6, p5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public final getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    return-object v0
.end method

.method protected final getResourceDownloader()Lcom/google/android/apps/plus/service/ResourceDownloader;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/service/ResourceDownloader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/service/ResourceDownloader;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;

    return-object v0
.end method

.method public final onEnvironmentChanged()V
    .locals 6

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/service/ImageResourceManager;->loadResource(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_0
.end method

.method public final onFirstConsumerRegistered(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Resource is not active: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResource;->getStatus()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal resource state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatusAsString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResource;->unpack()V

    :goto_0
    :pswitch_2
    return-void

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting image load: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->loadResource(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public final onLastConsumerUnregistered(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/service/ImageResource;

    iget-object v0, v1, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Deactivating image resource: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ImageResource;->getStatus()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    :cond_1
    const/16 v3, 0x8

    iput v3, v1, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mResourceDownloader:Lcom/google/android/apps/plus/service/ResourceDownloader;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/service/ResourceDownloader;->cancelDownload(Lcom/google/android/apps/plus/service/Resource;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ImageResource;->isCacheEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ImageResource;->getSizeInBytes()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMaxCacheableImageSize:I

    if-ge v3, v4, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ImageResource;->pack()V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0, v1}, Lvedroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method public final preloadAvatarByGaiaId(Ljava/lang/String;IZ)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarIdentifier(Ljava/lang/String;Ljava/lang/String;IZI)Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v0, v6}, Lvedroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mAvatarIdPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v6, v2}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/AvatarResource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->preloadResource(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_0
.end method

.method public final preloadMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;II)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaIdentifier(Lcom/google/android/apps/plus/api/MediaRef;IIII)Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mImageCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v0, v6}, Lvedroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->setNextInPool(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mMediaIdPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v6, v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;Lcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->preloadResource(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_0
.end method

.method public final verifyEmpty()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager;->mActiveResources:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->dump()V

    :cond_0
    return-void
.end method
