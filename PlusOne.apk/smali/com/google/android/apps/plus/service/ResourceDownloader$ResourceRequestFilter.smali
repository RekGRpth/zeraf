.class final Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;
.super Ljava/lang/Object;
.source "ResourceDownloader.java"

# interfaces
.implements Lcom/android/volley/RequestQueue$RequestFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ResourceDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ResourceRequestFilter"
.end annotation


# instance fields
.field public resource:Lcom/google/android/apps/plus/service/Resource;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/android/volley/Request;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Request",
            "<*>;)Z"
        }
    .end annotation

    check-cast p1, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->getResource()Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$ResourceRequestFilter;->resource:Lcom/google/android/apps/plus/service/Resource;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
