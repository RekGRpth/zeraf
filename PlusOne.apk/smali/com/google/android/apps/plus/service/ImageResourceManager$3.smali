.class final Lcom/google/android/apps/plus/service/ImageResourceManager$3;
.super Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;
.source "ImageResourceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field final synthetic val$flags:I

.field final synthetic val$height:I

.field final synthetic val$mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/api/MediaRef;III)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->this$0:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput p3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$width:I

    iput p4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$height:I

    iput p5, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$flags:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;-><init>(Lcom/google/android/apps/plus/service/ResourceManager;)V

    return-void
.end method


# virtual methods
.method public final bindResources()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->this$0:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v2, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$width:I

    iget v3, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$height:I

    iget v4, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$3;->val$flags:I

    or-int/lit8 v4, v4, 0x10

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    return-void
.end method
