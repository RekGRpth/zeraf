.class public final Lcom/google/android/apps/plus/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AudienceView:[I

.field public static final AudienceView_android_maxLines:I = 0x0

.field public static final CircleButton:[I

.field public static final CircleButton_android_drawableLeft:I = 0x5

.field public static final CircleButton_android_paddingLeft:I = 0x2

.field public static final CircleButton_android_paddingRight:I = 0x3

.field public static final CircleButton_android_text:I = 0x4

.field public static final CircleButton_android_textColor:I = 0x1

.field public static final CircleButton_android_textSize:I = 0x0

.field public static final CircleButton_circle_button_icon_spacing:I = 0x6

.field public static final CircleButton_circle_button_label_spacing:I = 0x7

.field public static final CircleListItemView:[I

.field public static final CircleListItemView_circle_list_item_circle_icon_size:I = 0x8

.field public static final CircleListItemView_circle_list_item_gap_between_count_and_checkbox:I = 0xa

.field public static final CircleListItemView_circle_list_item_gap_between_icon_and_text:I = 0x5

.field public static final CircleListItemView_circle_list_item_gap_between_name_and_count:I = 0x9

.field public static final CircleListItemView_circle_list_item_height:I = 0x0

.field public static final CircleListItemView_circle_list_item_member_count_text_color:I = 0xb

.field public static final CircleListItemView_circle_list_item_name_text_bold:I = 0x7

.field public static final CircleListItemView_circle_list_item_name_text_size:I = 0x6

.field public static final CircleListItemView_circle_list_item_padding_bottom:I = 0x2

.field public static final CircleListItemView_circle_list_item_padding_left:I = 0x3

.field public static final CircleListItemView_circle_list_item_padding_right:I = 0x4

.field public static final CircleListItemView_circle_list_item_padding_top:I = 0x1

.field public static final ColumnGridView_Layout:[I

.field public static final ColumnGridView_Layout_layout_majorSpan:I = 0x2

.field public static final ColumnGridView_Layout_layout_minorSpan:I = 0x1

.field public static final ColumnGridView_Layout_layout_orientation:I = 0x0

.field public static final ConstrainedTextView:[I

.field public static final ConstrainedTextView_android_maxHeight:I = 0x3

.field public static final ConstrainedTextView_android_maxLines:I = 0x5

.field public static final ConstrainedTextView_android_text:I = 0x4

.field public static final ConstrainedTextView_android_textColor:I = 0x2

.field public static final ConstrainedTextView_android_textSize:I = 0x0

.field public static final ConstrainedTextView_android_textStyle:I = 0x1

.field public static final ContactListItemView:[I

.field public static final ContactListItemView_contact_list_item_action_button_background:I = 0xe

.field public static final ContactListItemView_contact_list_item_action_button_width:I = 0xf

.field public static final ContactListItemView_contact_list_item_circle_icon_drawable:I = 0x7

.field public static final ContactListItemView_contact_list_item_circle_icon_size:I = 0x8

.field public static final ContactListItemView_contact_list_item_circles_text_color:I = 0xa

.field public static final ContactListItemView_contact_list_item_circles_text_size:I = 0x9

.field public static final ContactListItemView_contact_list_item_email_icon_padding_left:I = 0x13

.field public static final ContactListItemView_contact_list_item_email_icon_padding_top:I = 0x12

.field public static final ContactListItemView_contact_list_item_gap_between_icon_and_circles:I = 0xc

.field public static final ContactListItemView_contact_list_item_gap_between_image_and_text:I = 0x5

.field public static final ContactListItemView_contact_list_item_gap_between_name_and_circles:I = 0xb

.field public static final ContactListItemView_contact_list_item_gap_between_text_and_button:I = 0xd

.field public static final ContactListItemView_contact_list_item_height:I = 0x0

.field public static final ContactListItemView_contact_list_item_name_text_size:I = 0x6

.field public static final ContactListItemView_contact_list_item_padding_bottom:I = 0x2

.field public static final ContactListItemView_contact_list_item_padding_left:I = 0x3

.field public static final ContactListItemView_contact_list_item_padding_right:I = 0x4

.field public static final ContactListItemView_contact_list_item_padding_top:I = 0x1

.field public static final ContactListItemView_contact_list_item_vertical_divider_padding:I = 0x11

.field public static final ContactListItemView_contact_list_item_vertical_divider_width:I = 0x10

.field public static final EsListPreference:[I

.field public static final EsListPreference_android_entries:I = 0x0

.field public static final EsListPreference_android_entryValues:I = 0x1

.field public static final EsListPreference_entrySummaries:I = 0x2

.field public static final ExactLayout_Layout:[I

.field public static final ExactLayout_Layout_layout_center_horizontal_bound:I = 0x2

.field public static final ExactLayout_Layout_layout_center_vertical_bound:I = 0x3

.field public static final ExactLayout_Layout_layout_x:I = 0x0

.field public static final ExactLayout_Layout_layout_y:I = 0x1

.field public static final ExpandingScrollView:[I

.field public static final ExpandingScrollView_bigBounce:I = 0x2

.field public static final ExpandingScrollView_minExposureLand:I = 0x0

.field public static final ExpandingScrollView_minExposurePort:I = 0x1

.field public static final FullSizeLinearLayout:[I

.field public static final FullSizeLinearLayout_android_maxHeight:I = 0x0

.field public static final FullSizeLinearLayout_maxHeightPercentage:I = 0x1

.field public static final ImageTextButton:[I

.field public static final ImageTextButton_image:I = 0x1

.field public static final ImageTextButton_text:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final MultiWaveView:[I

.field public static final MultiWaveView_bottomChevronDrawable:I = 0x7

.field public static final MultiWaveView_directionDescriptions:I = 0x2

.field public static final MultiWaveView_feedbackCount:I = 0xc

.field public static final MultiWaveView_handleDrawable:I = 0x3

.field public static final MultiWaveView_hitRadius:I = 0x9

.field public static final MultiWaveView_horizontalOffset:I = 0xe

.field public static final MultiWaveView_leftChevronDrawable:I = 0x4

.field public static final MultiWaveView_rightChevronDrawable:I = 0x5

.field public static final MultiWaveView_snapMargin:I = 0xb

.field public static final MultiWaveView_targetDescriptions:I = 0x1

.field public static final MultiWaveView_targetDrawables:I = 0x0

.field public static final MultiWaveView_topChevronDrawable:I = 0x6

.field public static final MultiWaveView_verticalOffset:I = 0xd

.field public static final MultiWaveView_vibrationDuration:I = 0xa

.field public static final MultiWaveView_waveDrawable:I = 0x8

.field public static final ParticipantsGalleryFragment:[I

.field public static final ParticipantsGalleryFragment_backgroundColor:I = 0x0

.field public static final ParticipantsGalleryFragment_emptyMessage:I = 0x1

.field public static final ScaledLayout:[I

.field public static final ScaledLayout_scale:I = 0x0

.field public static final ScaledLayout_scaleHeight:I = 0x1

.field public static final ScaledLayout_scaleMargin:I = 0x3

.field public static final ScaledLayout_scaleMarginBottom:I = 0x5

.field public static final ScaledLayout_scaleMarginLeft:I = 0x6

.field public static final ScaledLayout_scaleMarginMode:I = 0x8

.field public static final ScaledLayout_scaleMarginRight:I = 0x7

.field public static final ScaledLayout_scaleMarginTop:I = 0x4

.field public static final ScaledLayout_scaleWidth:I = 0x2

.field public static final TabButton:[I

.field public static final TabButton_android_icon:I = 0x0

.field public static final TabButton_android_text:I = 0x1

.field public static final Theme:[I

.field public static final Theme_actionDropDownStyle:I = 0x6

.field public static final Theme_buttonBarButtonStyle:I = 0x1

.field public static final Theme_buttonBarStyle:I = 0x0

.field public static final Theme_buttonSelectableBackground:I = 0x5

.field public static final Theme_editAudienceBackground:I = 0x3

.field public static final Theme_editLocationBackground:I = 0x4

.field public static final Theme_listSelector:I = 0x2

.field public static final Theme_navigationItemHeight:I = 0x7

.field public static final Thermometer:[I

.field public static final Thermometer_background:I = 0x0

.field public static final Thermometer_foreground:I = 0x1

.field public static final Thermometer_orientation:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010153

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->AudienceView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->CircleButton:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->CircleListItemView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ColumnGridView_Layout:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ConstrainedTextView:[I

    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ContactListItemView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->EsListPreference:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ExactLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ExpandingScrollView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->FullSizeLinearLayout:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ImageTextButton:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->MapAttrs:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->MultiWaveView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ParticipantsGalleryFragment:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ScaledLayout:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->TabButton:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->Thermometer:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010095
        0x1010098
        0x10100d6
        0x10100d8
        0x101014f
        0x101016f
        0x7f01005b
        0x7f01005c
    .end array-data

    :array_1
    .array-data 4
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
    .end array-data

    :array_2
    .array-data 4
        0x7f010055
        0x7f010056
        0x7f010057
    .end array-data

    :array_3
    .array-data 4
        0x1010095
        0x1010097
        0x1010098
        0x1010120
        0x101014f
        0x1010153
    .end array-data

    :array_4
    .array-data 4
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
    .end array-data

    :array_5
    .array-data 4
        0x10100b2
        0x10101f8
        0x7f010061
    .end array-data

    :array_6
    .array-data 4
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
    .end array-data

    :array_7
    .array-data 4
        0x7f010058
        0x7f010059
        0x7f01005a
    .end array-data

    :array_8
    .array-data 4
        0x1010120
        0x7f010062
    .end array-data

    :array_9
    .array-data 4
        0x7f010018
        0x7f010019
    .end array-data

    :array_a
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
    .end array-data

    :array_b
    .array-data 4
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
    .end array-data

    :array_c
    .array-data 4
        0x7f010016
        0x7f010017
    .end array-data

    :array_d
    .array-data 4
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
    .end array-data

    :array_e
    .array-data 4
        0x1010002
        0x101014f
    .end array-data

    :array_f
    .array-data 4
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
    .end array-data

    :array_10
    .array-data 4
        0x7f010023
        0x7f010024
        0x7f010025
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
