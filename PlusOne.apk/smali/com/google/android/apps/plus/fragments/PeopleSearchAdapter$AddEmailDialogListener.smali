.class public Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
.super Lvedroid/support/v4/app/Fragment;
.source "PeopleSearchAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddEmailDialogListener"
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    return-void
.end method
