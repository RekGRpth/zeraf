.class public Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;
.super Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;
.source "AddedToCircleFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/AddedToCircleFragment$AddedToCircleQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

.field private mDataLoaded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    return-void
.end method


# virtual methods
.method protected final getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    return-object v0
.end method

.method protected final getEmptyText()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$string;->no_added_to_circle_person:I

    return v0
.end method

.method protected final inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;

    sget v0, Lcom/google/android/apps/plus/R$layout;->people_list_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    return v0
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListItemView;
    .param p2    # I

    const/4 v1, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v1}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 8
    .param p1    # Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;-><init>(Landroid/content/Context;IIIIILcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setShowAddButtonIfNeeded(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v4

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "notif_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "circle_actor_data"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v0, v5, v3, v6}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setAlwaysHideLetterSections(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    return-object v0
.end method

.method protected final onInitLoaders(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->updateView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
