.class final Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "BaseSquareMemberFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "SquareMembers"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SquareMembers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onEditSquareMembershipComplete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->access$000(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method
