.class public final Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "HostedEventListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventsLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader$Query;
    }
.end annotation


# static fields
.field private static final EVENT_LIST_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "header_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "event_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->EVENT_LIST_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method private insertEvents(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Landroid/database/Cursor;IIZ[Ljava/lang/Object;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .param p2    # Landroid/database/Cursor;
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v2, 0x1

    if-ltz p3, :cond_0

    if-eqz p5, :cond_1

    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_5

    if-eqz p5, :cond_5

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->EVENT_LIST_PROJECTION:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz p5, :cond_3

    const/4 v1, -0x1

    :goto_0
    add-int/2addr v1, p3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_1
    if-eqz p5, :cond_4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p6, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p6, v2

    const/4 v1, 0x3

    aput-object v0, p6, v1

    invoke-virtual {p1, p6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-static {p6, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_5
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 14

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader$Query;->PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->getMyEvents$13db9565(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->EVENT_LIST_PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->EVENT_LIST_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    new-array v6, v0, [Ljava/lang/Object;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    :cond_1
    const/4 v4, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->insertEvents(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Landroid/database/Cursor;IIZ[Ljava/lang/Object;)V

    add-int/lit8 v10, v3, -0x1

    const/4 v12, 0x0

    move-object v7, p0

    move-object v8, v1

    move-object v9, v2

    move v11, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->insertEvents(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Landroid/database/Cursor;IIZ[Ljava/lang/Object;)V

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
