.class final Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;
.super Landroid/os/Handler;
.source "BaseSearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchStatusHandler"
.end annotation


# instance fields
.field mSearchAdapterRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;->mSearchAdapterRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;->mSearchAdapterRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->showEmptySearchResults()V

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->access$000(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
