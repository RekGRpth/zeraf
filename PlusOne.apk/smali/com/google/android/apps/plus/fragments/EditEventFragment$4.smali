.class final Lcom/google/android/apps/plus/fragments/EditEventFragment$4;
.super Ljava/lang/Object;
.source "EditEventFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/EditEventFragment;->enableEventPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$600(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$700(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$800(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$600(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$900(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/views/EventThemeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$900(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/views/EventThemeView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
