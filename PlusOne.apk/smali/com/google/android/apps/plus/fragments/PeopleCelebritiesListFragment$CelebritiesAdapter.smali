.class final Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;
.super Landroid/widget/BaseAdapter;
.source "PeopleCelebritiesListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CelebritiesAdapter"
.end annotation


# instance fields
.field mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;",
            ">;"
        }
    .end annotation
.end field

.field mCategoryFirstIndex:[I

.field mSingleCategory:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

.field mSingleCategoryMode:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)V

    return-void
.end method

.method private getCategoryIndex(I)I
    .locals 4
    .param p1    # I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aget v1, v1, v0

    if-ge p1, v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request for position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; max="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final bind(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-eqz p2, :cond_2

    move v8, v9

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v8, :cond_3

    move v7, v9

    :goto_1
    add-int/lit8 v8, v7, 0x1

    new-array v8, v8, [I

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    const/4 v4, 0x0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v8, :cond_4

    const v5, 0x7fffffff

    :goto_2
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v8, :cond_5

    move v2, v10

    :goto_3
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aput v10, v8, v10

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v3, 0x1

    :goto_4
    if-gt v3, v6, :cond_1

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v8, :cond_0

    iget-object v8, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->category:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    :cond_0
    iget-object v8, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int v8, v1, v2

    add-int/2addr v4, v8

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v8, :cond_6

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategory:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aput v4, v8, v9

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategories:Ljava/util/ArrayList;

    return-void

    :cond_2
    move v8, v10

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    goto :goto_1

    :cond_4
    const/4 v5, 0x2

    goto :goto_2

    :cond_5
    move v2, v9

    goto :goto_3

    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aput v4, v8, v3

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method public final getCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 5
    .param p1    # I

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategory:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getCategoryIndex(I)I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aget v0, v4, v1

    if-eq p1, v0, :cond_0

    iget-object v3, v2, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    sub-int v4, p1, v0

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4
    .param p1    # I

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getCategoryIndex(I)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    aget v0, v3, v1

    if-ne p1, v0, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getSingleCategoryName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategory:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategory:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->categoryName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    sget v1, Lcom/google/android/apps/plus/R$string;->find_people_all_interesting:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v9, v3, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    if-eqz v9, :cond_2

    if-nez p2, :cond_1

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$layout;->people_list_category_as_card:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    :goto_0
    move-object v1, v3

    check-cast v1, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v5

    sget v9, Lcom/google/android/apps/plus/R$id;->category_name:I

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->categoryName:Ljava/lang/String;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v9, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstRow(I)V

    :cond_0
    :goto_1
    return-object v6

    :cond_1
    move-object/from16 v6, p2

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_0

    :cond_2
    instance-of v9, v3, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    if-eqz v9, :cond_0

    if-nez p2, :cond_3

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$layout;->people_list_row_as_card:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    :goto_2
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PeopleListRowView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    iget-object v10, v10, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v11, 0x0

    invoke-virtual {v5, v9, v10, v11}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    check-cast v3, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Lcom/google/api/services/plusi/model/DataSuggestedPerson;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getCategoryIndex(I)I

    move-result v0

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v9, :cond_4

    if-nez p1, :cond_4

    sget v9, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstRow(I)V

    goto :goto_1

    :cond_3
    move-object/from16 v6, p2

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_2

    :cond_4
    add-int/lit8 v9, p1, 0x1

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    add-int/lit8 v11, v0, 0x1

    aget v10, v10, v11

    if-ne v9, v10, :cond_6

    add-int/lit8 v9, p1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getCount()I

    move-result v10

    if-ne v9, v10, :cond_5

    sget v4, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    :goto_3
    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsLastRow(I)V

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    sget v10, Lcom/google/android/apps/plus/R$string;->find_people_view_all_count:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v1, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    invoke-virtual {v6, v9, v7}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->enableViewAll(ZLjava/lang/String;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v9, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v9, Lcom/google/android/apps/plus/R$id;->people_suggestion_data:I

    iget-object v10, v1, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->category:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsBodyRow()V

    goto/16 :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mSingleCategoryMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->mCategoryFirstIndex:[I

    array-length v1, v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
