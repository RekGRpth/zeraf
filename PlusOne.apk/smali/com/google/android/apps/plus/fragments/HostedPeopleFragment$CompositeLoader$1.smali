.class final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;
.super Ljava/lang/Object;
.source "HostedPeopleFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->setPeopleSuggestions(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/api/services/plusi/model/ListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getSortOrder(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "FRIEND_ADDS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "YOU_MAY_KNOW"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "ORGANIZATION"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-string v0, "SCHOOL"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ListResponse;

    check-cast p2, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;->getSortOrder(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p2, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;->getSortOrder(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
