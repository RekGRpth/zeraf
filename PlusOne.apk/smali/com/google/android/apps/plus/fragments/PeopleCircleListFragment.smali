.class public Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;
.super Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.source "PeopleCircleListFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;
.implements Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$DeleteCircleConfirmationDialog;,
        Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;
    }
.end annotation


# static fields
.field public static final CIRCLE_MEMBERS_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mCircleId:Ljava/lang/String;

.field private final mCircleLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private mCircleMemberAdapter:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

.field private mCircleName:Ljava/lang/String;

.field private final mCircleServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mDeleteCircleRequestId:Ljava/lang/Integer;

.field private mModifyCircleRequestId:Ljava/lang/Integer;

.field private mShownPersonIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUnblockRequestId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "blocked"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "interaction_sort_key"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->CIRCLE_MEMBERS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method private handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->dismissProgressDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->toast_circle_deleted:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public final bindCircleMembers(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleMemberAdapter:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleMemberAdapter:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final doDeleteCircle()V
    .locals 3

    sget v1, Lcom/google/android/apps/plus/R$string;->delete_circle_operation_pending:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->showProgressDialog(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    return-void
.end method

.method protected final getOnEmptyMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->find_people_nobody_blocked:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->empty_circle:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p4}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final onCirclePropertiesChange(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->toast_circle_already_exists:I

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_4
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->invalidateActionBar()V

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_properties_operation_pending:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v3, v4, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->modifyCircleProperties(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v1, "selected_circle_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    const-string v1, "circle_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    const-string v1, "shown_person_ids"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v1, "delete_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "delete_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v1, "unblock_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "unblock_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleMemberAdapter:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v3, v2}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleMemberAdapter:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->delete_circle:I

    if-ne v2, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$DeleteCircleConfirmationDialog;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$DeleteCircleConfirmationDialog;-><init>()V

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$DeleteCircleConfirmationDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "delete_circle_conf"

    invoke-virtual {v3, v5, v6}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$DeleteCircleConfirmationDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v4

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$id;->circle_settings:I

    if-ne v2, v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    invoke-static {v3, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v7, 0x4

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    :goto_1
    if-eq v3, v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    and-int/lit8 v3, v3, 0x40

    if-nez v3, :cond_5

    move v3, v4

    :goto_2
    invoke-static {v6, v7, v3}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->newInstance$50fd8769(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    move-result-object v3

    invoke-virtual {v3, p0, v5}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "circle_settings"

    invoke-virtual {v3, v5, v6}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_4
    move v3, v6

    goto :goto_1

    :cond_5
    move v3, v5

    goto :goto_2

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v2, v3, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$string;->url_param_help_circles:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    move v4, v5

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->delete_circle:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->circle_settings:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->help:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public final onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->handleServiceCallback$b5e9bbb(Lcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->handleServiceCallback$b5e9bbb(Lcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    :cond_2
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_circle_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "shown_person_ids"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mModifyCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "delete_circle_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "unblock_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "circle_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;

    const-string v0, "circle_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleName:Ljava/lang/String;

    return-void
.end method

.method public final onUnblockButtonClicked(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;-><init>(Ljava/lang/String;Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "unblock_person"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final unblockPerson(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mUnblockRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->unblock_person_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->showProgressDialog(I)V

    return-void
.end method
