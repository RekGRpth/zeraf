.class final Lcom/google/android/apps/plus/fragments/PostFragment$16;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShareText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$902(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_REMOVED:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->val$activity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3300(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$16;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    return-void
.end method
