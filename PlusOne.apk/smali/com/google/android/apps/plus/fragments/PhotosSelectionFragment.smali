.class public Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "PhotosSelectionFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

.field private mDateFormat:Ljava/text/DateFormat;

.field private mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mLoaderActive:Z

.field private mMediaRefUserMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMediaRefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mNextButton:Landroid/view/View;

.field private mOwnerId:Ljava/lang/String;

.field private mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

.field private final mSelectedMediaRefs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Ljava/text/DateFormat;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDateFormat:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->updatePostUI()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Z
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->createAudienceData()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private createAudienceData()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 13

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/HashSet;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :goto_0
    return-object v10

    :cond_1
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    if-eqz v9, :cond_2

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-interface {v6, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v10, Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v3, v11, v12}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v10, v7, v12}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->merge(Ljava/lang/Iterable;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v10

    goto/16 :goto_0
.end method

.method private updatePostUI()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->showContent(Landroid/view/View;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->updateSpinner()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_2

    :cond_3
    sget v2, Lcom/google/android/apps/plus/R$string;->no_photos:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "mediarefs"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "mediarefs"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    array-length v5, v3

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefs:Ljava/util/List;

    const/4 v1, 0x0

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefs:Ljava/util/List;

    aget-object v4, v3, v1

    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v4, "owner_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "owner_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mOwnerId:Ljava/lang/String;

    :cond_1
    const-string v4, "mediaref_user_map"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :try_start_0
    const-string v4, "mediaref_user_map"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    :try_start_1
    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    const-string v4, "SELECTED_ITEMS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "SELECTED_ITEMS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    const/4 v1, 0x0

    :goto_3
    array-length v4, v3

    if-ge v1, v4, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    aget-object v4, v3, v1

    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :catch_0
    move-exception v4

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    goto :goto_1

    :catch_1
    move-exception v4

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    goto :goto_2

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefUserMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_5
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotosSelectionLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mMediaRefs:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/PhotosSelectionLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v0, Lcom/google/android/apps/plus/R$layout;->photos_selection_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    sget v0, Lcom/google/android/apps/plus/R$id;->album_view:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/views/PhotoAlbumView;

    sget v0, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_photo_grid_spacing:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v10, v10, v10, v10}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getPhotoColumns(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    const/4 v2, 0x0

    const-string v3, "from_my_phone"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->setSelectedMediaRefs(Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->startSelectionMode()V

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->updateView(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->no_photos:I

    invoke-static {v8, v0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->setupEmptyView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;

    invoke-direct {v2, p0, v8}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;Lcom/google/android/apps/plus/views/PhotoAlbumView;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->registerSelectionListener(Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;)V

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->enableDateDisplay(Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)V

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->next_button:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mNextButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->updatePostUI()V

    return-object v11
.end method

.method public final onDataSourceLoading(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/LoaderManager;->getLoader(I)Lvedroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/Pageable;->isDataSourceLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mLoaderActive:Z

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v1, "SELECTED_ITEMS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method
