.class final Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;
.super Ljava/lang/Object;
.source "PeopleListDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PeopleListItem"
.end annotation


# instance fields
.field public final mContent:Ljava/lang/String;

.field public final mPerson:Lcom/google/android/apps/plus/content/PersonData;

.field public final mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mType:I

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mContent:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mType:I

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mContent:Ljava/lang/String;

    return-void
.end method
