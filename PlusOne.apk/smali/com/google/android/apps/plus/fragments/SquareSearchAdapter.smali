.class public final Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;
.super Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;
.source "SquareSearchAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;
    }
.end annotation


# static fields
.field private static sMinWidth:I


# instance fields
.field protected mListener:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/app/FragmentManager;
    .param p3    # Lvedroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/app/FragmentManager;
    .param p3    # Lvedroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    sget v0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->sMinWidth:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->square_card_min_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->sMinWidth:I

    :cond_0
    return-void
.end method

.method private static setLayoutParams(Landroid/view/View;)V
    .locals 4
    .param p0    # Landroid/view/View;

    const/4 v3, 0x1

    new-instance v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v1, 0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method protected final bindSearchItemView$2e05ad49(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SquareListItemView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p2, p0, v1, v2}, Lcom/google/android/apps/plus/views/SquareListItemView;->init(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;ZZ)V

    invoke-static {p1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->setLayoutParams(Landroid/view/View;)V

    return-void
.end method

.method protected final bindSearchMessageView(Landroid/view/View;I)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/16 v1, 0x8

    const/16 v2, 0x8

    const/16 v0, 0x8

    packed-switch p2, :pswitch_data_0

    :goto_0
    sget v3, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->not_found:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->error:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->setLayoutParams(Landroid/view/View;)V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final createSearchLoader(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
    .locals 7
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mQuery:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$Query;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->getMinQueryLength()I

    move-result v6

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v0
.end method

.method protected final getQueryProjection()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$Query;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method protected final newSearchItemView$3bea8f80(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_list_item_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected final newSearchMessageView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_search_status_card:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final onClick(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;->onSquareSelected(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onInvitationDismissed(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onInviterImageClick(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;

    return-void
.end method
