.class public final Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;
.super Landroid/widget/BaseAdapter;
.source "PeopleListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DataSuggestedPersonAdapter"
.end annotation


# instance fields
.field private mSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final bindPeopleViewPerson(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$layout;->people_list_row_as_card:I

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_1

    sget v2, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstRow(I)V

    :goto_1
    return-object v1

    :cond_0
    move-object v1, p2

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_0

    :cond_1
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    sget v2, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsLastRow(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsBodyRow()V

    goto :goto_1
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->mSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
