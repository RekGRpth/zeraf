.class public Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedPostLinkFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment$CursorLoaderCallbacks;
    }
.end annotation


# instance fields
.field private mClipboardUrl:Ljava/lang/String;

.field private mEditText:Landroid/widget/EditText;

.field private mExtras:Landroid/os/Bundle;

.field private mMainView:Landroid/view/View;

.field private mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .locals 10
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const-string v3, "com.google.android.apps.social"

    const-string v4, "com.google.android.apps.social"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    move-object v4, v1

    move-object v5, v2

    move-object v9, v0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    return-object v3
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mClipboardUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;
    .param p1    # Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->handlePreviewResult(Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    return-void
.end method

.method private static getUrlText(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v5, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-static {v1, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-virtual {v1, v5, v3, v4}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v3, v0

    if-lez v3, :cond_0

    aget-object v2, v0, v5

    invoke-virtual {v2}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private handlePreviewResult(Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/api/ApiaryActivity;

    const/16 v9, 0x8

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    const v8, 0x1020004

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_container:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Lcom/google/android/apps/plus/api/ApiaryActivity;->getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-result-object v3

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    if-eqz v7, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    invoke-direct {v0, v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/WebPage;)V

    :goto_0
    const/4 v2, 0x0

    const/4 v5, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-static {v7}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_link_name:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_link_url:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_chevron:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_icon:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    :goto_3
    return-void

    :cond_0
    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    if-eqz v7, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    invoke-direct {v0, v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/VideoObject;)V

    goto :goto_0

    :cond_1
    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    if-eqz v7, :cond_2

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    invoke-direct {v1, v7}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)V

    goto :goto_0

    :cond_2
    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    if-eqz v7, :cond_3

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    invoke-direct {v1, v7}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicTrack;)V

    goto :goto_0

    :cond_3
    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    if-eqz v7, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    invoke-direct {v0, v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/Thing;)V

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v8, Lcom/google/android/apps/plus/R$id;->preview_area:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v7, 0x6

    const-string v8, "tag"

    const-string v9, "Found an embed we don\'t understand without a THING!"

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getMarketUrl()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getSong()Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method

.method private post(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lvedroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->preview_container:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mClipboardUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->post(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "INTENT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryActivity;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v8, 0x0

    sget v4, Lcom/google/android/apps/plus/R$layout;->hosted_post_link_fragment:I

    invoke-super {p0, p1, p2, p3, v4}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->link_text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "link_url"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-ge v4, v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "clipboard"

    invoke-virtual {v4, v5}, Lvedroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getUrlText(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mClipboardUrl:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mClipboardUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment$CursorLoaderCallbacks;

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;B)V

    invoke-virtual {v4, v5, v6, v7}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->preview_area:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$string;->post_loading_from_clipboard:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/widget/EditText;->setSelection(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mMainView:Landroid/view/View;

    return-object v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "clipboard"

    invoke-virtual {v4, v5}, Lvedroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->getUrlText(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->handlePreviewResult(Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v0, "INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v0, :cond_1

    const-string v0, "preview_result"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method public final post()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->post(Ljava/lang/String;)V

    return-void
.end method
