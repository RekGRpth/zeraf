.class final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;
.super Ljava/lang/Object;
.source "HostedPeopleFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CirclesLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/LoaderManager;->getLoader(I)Lvedroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->setCircles(Landroid/database/Cursor;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->forceLoad()V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lvedroid/support/v4/app/LoaderManager;->getLoader(I)Lvedroid/support/v4/content/Loader;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->setCircles(Landroid/database/Cursor;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->forceLoad()V

    :cond_0
    return-void
.end method
