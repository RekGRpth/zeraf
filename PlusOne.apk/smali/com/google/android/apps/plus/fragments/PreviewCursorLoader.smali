.class final Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PreviewCursorLoader.java"


# instance fields
.field private mCachedData:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;->mCachedData:Z

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final isCachedData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;->mCachedData:Z

    return v0
.end method

.method public final setCachedData(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;->mCachedData:Z

    return-void
.end method
