.class public Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;
.super Lvedroid/support/v4/app/DialogFragment;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimePickerFragmentDialog"
.end annotation


# instance fields
.field private mCancelled:Z

.field private mType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onEndTimeCleared()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mCancelled:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    if-ne v1, v2, :cond_0

    const-string v1, "type"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    const-string v1, "cancelled"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mCancelled:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mCancelled:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "date_time"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "time_zone"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v9}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    new-instance v0, Landroid/app/TimePickerDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v2, 0xc

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    if-nez v1, :cond_1

    const/4 v1, -0x2

    sget v2, Lcom/google/android/apps/plus/R$string;->clear:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/TimePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_1
    return-object v0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "cancelled"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mCancelled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 3
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mCancelled:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onStartTimeSet(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onEndTimeSet(II)V

    goto :goto_0
.end method
