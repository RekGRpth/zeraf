.class public final Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PeopleCircleListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CircleMembersAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object/from16 v15, p1

    check-cast v15, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    const/4 v6, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    const/4 v10, 0x1

    :goto_0
    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    iget-object v7, v7, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, v6, v7, v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_1

    const/4 v11, 0x1

    :goto_1
    if-eqz v11, :cond_2

    const/4 v4, 0x0

    const-string v8, "15"

    :goto_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v14

    if-nez v14, :cond_3

    const/4 v12, 0x1

    :goto_3
    add-int/lit8 v6, v14, 0x1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ne v6, v7, :cond_4

    const/4 v13, 0x1

    :goto_4
    if-eqz v12, :cond_5

    if-eqz v13, :cond_5

    sget v6, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    const/4 v7, 0x0

    invoke-virtual {v15, v6, v7}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstAndLastRow(II)V

    :goto_5
    return-void

    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    :cond_1
    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    const/4 v6, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_3
    const/4 v12, 0x0

    goto :goto_3

    :cond_4
    const/4 v13, 0x0

    goto :goto_4

    :cond_5
    if-eqz v12, :cond_6

    sget v6, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v15, v6}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstRow(I)V

    goto :goto_5

    :cond_6
    add-int/lit8 v6, v14, 0x1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ne v6, v7, :cond_7

    sget v6, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->sTopBottomPadding:I

    invoke-virtual {v15, v6}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsLastRow(I)V

    goto :goto_5

    :cond_7
    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsBodyRow()V

    goto :goto_5
.end method

.method public final isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$CircleMembersAdapter;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->people_list_row_as_card:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
