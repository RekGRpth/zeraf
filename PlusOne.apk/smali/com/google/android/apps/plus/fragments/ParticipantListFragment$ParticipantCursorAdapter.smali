.class final Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "ParticipantListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ParticipantListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ParticipantCursorAdapter"
.end annotation


# instance fields
.field mShowLetterSections:Z

.field final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ParticipantListItemView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/AbsListView;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/widget/AbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mViews:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;)V

    invoke-virtual {p3, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v7, 0x2

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ParticipantListItemView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->clear()V

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->setParticipantName(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->setPersonId(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mShowLetterSections:Z

    if-eqz v6, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->showSectionHeader(C)V

    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v4

    if-eq v4, v1, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->showSectionHeader(C)V

    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->hideSectionHeader()V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->hideSectionHeader()V

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/plus/R$layout;->participant_list_item_view:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ParticipantListItemView;

    return-object v1
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mShowLetterSections:Z

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->mShowLetterSections:Z

    goto :goto_0
.end method
