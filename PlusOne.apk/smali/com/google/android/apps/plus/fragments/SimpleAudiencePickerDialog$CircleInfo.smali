.class public final Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;
.super Ljava/lang/Object;
.source "SimpleAudiencePickerDialog.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CircleInfo"
.end annotation


# instance fields
.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mType:I

    return-void
.end method


# virtual methods
.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->mType:I

    return v0
.end method
