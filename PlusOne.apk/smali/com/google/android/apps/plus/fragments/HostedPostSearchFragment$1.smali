.class final Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedPostSearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSearchActivitiesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mError:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->updateServerErrorView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->loadContent()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
