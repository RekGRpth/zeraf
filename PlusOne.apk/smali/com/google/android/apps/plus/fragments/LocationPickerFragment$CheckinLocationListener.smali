.class final Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;
.super Ljava/lang/Object;
.source "LocationPickerFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckinLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 9
    .param p1    # Landroid/location/Location;

    const/4 v8, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$400(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/phone/LocationController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->removeLocationListener()V
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$500(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$600(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    new-instance v4, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$600(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$702(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;

    const/4 v2, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->invalidateActionBar()V
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$800(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    sget v6, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$900(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z
    invoke-static {v3, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$102(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z
    invoke-static {v3, v2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$202(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v5, v5, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$700(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/api/LocationQuery;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1000(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1100(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1000(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-static {v4, p1, v8}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1300(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/location/Location;Z)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1202(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1100(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1200(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v3, v4, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v4

    # invokes: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->updateView$3c7ec8c3()V
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$1400(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    new-instance v4, Lcom/google/android/apps/plus/api/LocationQuery;

    const/4 v5, 0x0

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$702(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
