.class abstract Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;
.super Ljava/lang/Object;
.source "PeopleSearchResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Profile"
.end annotation


# instance fields
.field avatarUrl:Ljava/lang/String;

.field gaiaId:Ljava/lang/String;

.field name:Ljava/lang/String;

.field personId:Ljava/lang/String;

.field profileType:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->personId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->gaiaId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->name:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->profileType:I

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->avatarUrl:Ljava/lang/String;

    return-void
.end method
