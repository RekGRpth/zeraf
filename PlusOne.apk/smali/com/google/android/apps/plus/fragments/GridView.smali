.class public Lcom/google/android/apps/plus/fragments/GridView;
.super Landroid/widget/GridView;
.source "GridView.java"


# instance fields
.field private final attrsArray:[I

.field private mHorizontalSpacing:I

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mVerticalSpacing:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->attrsArray:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010114
        0x1010115
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mVerticalSpacing:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->attrsArray:[I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/GridView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :array_0
    .array-data 4
        0x1010114
        0x1010115
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->mVerticalSpacing:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/GridView;->attrsArray:[I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/GridView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :array_0
    .array-data 4
        0x1010114
        0x1010115
    .end array-data
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/GridView;->attrsArray:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/GridView;->mVerticalSpacing:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/high16 v12, 0x40000000

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GridView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/R$dimen;->medium_avatar_dimension:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/R$dimen;->medium_avatar_selected_padding:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    add-int v4, v9, v10

    iget v9, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    add-int/2addr v9, v8

    iget v10, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    add-int/2addr v10, v4

    div-int v5, v9, v10

    iget v9, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    add-int/2addr v9, v4

    mul-int/2addr v9, v5

    iget v10, p0, Lcom/google/android/apps/plus/fragments/GridView;->mHorizontalSpacing:I

    sub-int v1, v9, v10

    if-lez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GridView;->getCount()I

    move-result v2

    sget v9, Lcom/google/android/apps/plus/R$dimen;->medium_avatar_selected_dimension:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/R$dimen;->medium_avatar_name_height:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int v3, v9, v10

    add-int v9, v2, v5

    add-int/lit8 v9, v9, -0x1

    div-int v6, v9, v5

    mul-int v9, v3, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GridView;->getPaddingTop()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GridView;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    iget v10, p0, Lcom/google/android/apps/plus/fragments/GridView;->mVerticalSpacing:I

    add-int/lit8 v11, v6, -0x1

    mul-int/2addr v10, v11

    add-int v0, v9, v10

    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-super {p0, v9, v10}, Landroid/widget/GridView;->onMeasure(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-super {p0, v9, p2}, Landroid/widget/GridView;->onMeasure(II)V

    goto :goto_0
.end method
