.class final Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;
.super Ljava/lang/Object;
.source "PhotosSelectionFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCachedFirstVisibleIndex:I

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

.field final synthetic val$albumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;Lcom/google/android/apps/plus/views/PhotoAlbumView;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->val$albumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->mCachedFirstVisibleIndex:I

    return-void
.end method


# virtual methods
.method public final onScroll$1ca47ba7(Lcom/google/android/apps/plus/views/ColumnGridView;IIII)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    if-eqz p4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->access$000(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int v0, p2, p3

    iget v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->mCachedFirstVisibleIndex:I

    if-eq v4, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnCount()I

    move-result v4

    add-int/2addr v4, v0

    add-int/lit8 v5, p5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->access$000(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getTimestampForItem(I)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->val$albumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mDateFormat:Ljava/text/DateFormat;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->setDate(Ljava/lang/String;)V

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->mCachedFirstVisibleIndex:I

    goto :goto_0
.end method

.method public final onScrollStateChanged$6f02efe7(I)V
    .locals 4
    .param p1    # I

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;->access$000(Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment$1;->val$albumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->setDateVisibility(I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    goto :goto_1
.end method
