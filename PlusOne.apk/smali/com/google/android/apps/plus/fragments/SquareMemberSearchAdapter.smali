.class public final Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;
.super Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;
.source "SquareMemberSearchAdapter.java"


# instance fields
.field private mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

.field private final mIsSquareAdmin:Z

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/app/FragmentManager;
    .param p3    # Lvedroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZI)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZI)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/app/FragmentManager;
    .param p3    # Lvedroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mSquareId:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mIsSquareAdmin:Z

    return-void
.end method


# virtual methods
.method protected final bindSearchItemView$2e05ad49(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mIsSquareAdmin:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->init(Landroid/database/Cursor;ZLcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;)V

    return-void
.end method

.method protected final bindSearchMessageView(Landroid/view/View;I)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$string;->no_square_members:I

    :goto_0
    sget v3, Lcom/google/android/apps/plus/R$id;->progress_indicator:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_1
    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/plus/R$string;->loading:I

    goto :goto_0

    :pswitch_2
    sget v2, Lcom/google/android/apps/plus/R$string;->square_search_error:I

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final createSearchLoader(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
    .locals 8
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mQuery:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mSquareId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x3

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v0
.end method

.method protected final getMinQueryLength()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected final getQueryProjection()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method protected final newSearchItemView$3bea8f80(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_item_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected final newSearchMessageView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_message_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final setListener(Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    return-void
.end method
