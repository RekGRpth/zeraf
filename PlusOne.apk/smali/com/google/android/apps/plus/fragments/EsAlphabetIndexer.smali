.class public final Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;
.super Landroid/widget/AlphabetIndexer;
.source "EsAlphabetIndexer.java"


# direct methods
.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->computeAlphabet(Landroid/database/Cursor;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AlphabetIndexer;-><init>(Landroid/database/Cursor;ILjava/lang/CharSequence;)V

    return-void
.end method

.method private static computeAlphabet(Landroid/database/Cursor;I)Ljava/lang/CharSequence;
    .locals 5
    .param p0    # Landroid/database/Cursor;
    .param p1    # I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v1

    if-eq v1, v0, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
