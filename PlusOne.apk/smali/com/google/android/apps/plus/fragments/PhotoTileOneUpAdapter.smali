.class public final Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PhotoTileOneUpAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SettableItemAdapter;


# instance fields
.field private mHeights:Landroid/util/SparseIntArray;

.field private final mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/TileOneUpListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lcom/google/android/apps/plus/views/TileOneUpListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 15
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v12, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v12, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v12, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v12, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object/from16 v11, p1

    check-cast v11, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->setTileOneUpListener(Lcom/google/android/apps/plus/views/TileOneUpListener;)V

    const/4 v12, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->setAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v12, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v11, v5, v12, v13}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->setComment(Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v12, 0x1

    invoke-virtual {v11, v9, v12, v8}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->setPlusOne(Ljava/lang/String;ZI)V

    const/4 v12, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->setDate(J)V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/PhotoTileOneUpCommentView;->requestLayout()V

    goto :goto_0

    :pswitch_1
    const/4 v12, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v12, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v12, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v12, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v11, p1

    check-cast v11, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->setTileOneUpListener(Lcom/google/android/apps/plus/views/TileOneUpListener;)V

    invoke-virtual {v11, v2, v3, v1}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->setOwner(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->setCaption(Ljava/lang/String;)V

    invoke-virtual {v11, v6, v7}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->setDate(J)V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->requestLayout()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$layout;->photo_tile_one_up_comment_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/google/android/apps/plus/R$layout;->photo_tile_one_up_info_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final setItemHeight(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    goto :goto_0
.end method
