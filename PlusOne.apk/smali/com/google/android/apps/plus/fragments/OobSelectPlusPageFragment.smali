.class public Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;
.super Lvedroid/support/v4/app/ListFragment;
.source "OobSelectPlusPageFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;,
        Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;
    }
.end annotation


# static fields
.field private static final DIALOG_IDS:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

.field private final mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mHasGooglePlusProfile:Z

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mSelectedAccountPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activation_progress"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "net_failure"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "server_error"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->DIALOG_IDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lvedroid/support/v4/app/ListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    return v0
.end method

.method private createAccountNames()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getNumPlusPages()I

    move-result v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageName(I)Ljava/lang/String;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$string;->oob_plus_page_name:I

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v3, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    if-nez v5, :cond_2

    sget v5, Lcom/google/android/apps/plus/R$string;->oob_select_plus_page_item_create_profile:I

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 13
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v12, 0x0

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eq v6, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    sget-object v9, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->DIALOG_IDS:[Ljava/lang/String;

    array-length v10, v9

    move v7, v8

    :goto_1
    if-ge v7, v10, :cond_3

    aget-object v6, v9, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11, v6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v6

    check-cast v6, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    instance-of v6, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v6, :cond_5

    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v6

    if-nez v6, :cond_4

    new-instance v6, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    sget v9, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    sget v10, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-direct {v6, v7, v9, v10}, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;-><init>(Landroid/content/Context;II)V

    :cond_4
    iget-object v7, v6, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    sget v9, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v6, v9, v12}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v6

    invoke-virtual {v6, v8}, Lvedroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v6, p0, v8}, Lvedroid/support/v4/app/DialogFragment;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "server_error"

    invoke-virtual {v6, v7, v8}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    sget v6, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v9, Lcom/google/android/apps/plus/R$string;->signup_retry:I

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v7, v9, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v6

    invoke-virtual {v6, v8}, Lvedroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v6, p0, v8}, Lvedroid/support/v4/app/DialogFragment;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "net_failure"

    invoke-virtual {v6, v7, v8}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v3

    if-eqz v3, :cond_7

    const-string v6, "OobSelect+PageFragment"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "OobSelect+PageFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "MobileOutOfBoxResponse: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    if-eqz v3, :cond_8

    iget-object v6, v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "intent"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v6, v5, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHomeOobActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isCreateProfileSelected()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    goto/16 :goto_0
.end method

.method private isCreateProfileSelected()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showProgressDialog()V
    .locals 4

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_signing_in:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "activation_progress"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final activateAccount()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    if-lez v0, :cond_2

    :cond_1
    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    add-int/lit8 v7, v0, -0x1

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageId(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageName(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPagePhotoUrl(I)Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->showProgressDialog()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/content/AccountSettingsData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    iget v7, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserDisplayName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserPhotoUrl()Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method public final isAccountSelected()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "plus_pages"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AccountSettingsData;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AccountSettingsData;->isGooglePlus()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z

    const-string v3, "account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v3, Lcom/google/android/apps/plus/R$layout;->oob_select_plus_page_fragment:I

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$layout;->account_name_list_item_single_choice:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->createAccountNames()Ljava/util/List;

    move-result-object v5

    invoke-direct {v0, p0, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;-><init>(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    if-eqz p3, :cond_0

    const-string v3, "selected_account"

    const/4 v4, -0x1

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    return-object v2
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OOB dialog not cancelable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->activateAccount()V

    :cond_0
    return-void
.end method

.method public final onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-super/range {p0 .. p5}, Lvedroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v0

    iput p3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContinueButtonEnabled(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isCreateProfileSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->showProgressDialog()V

    const-string v1, "DEFAULT"

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->createOutOfBoxRequest(Ljava/lang/String;)Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/service/EsService;->sendOutOfBoxRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/ListFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/ListFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_account"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    return-void
.end method
