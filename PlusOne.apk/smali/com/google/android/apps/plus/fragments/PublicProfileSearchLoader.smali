.class public final Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PublicProfileSearchLoader.java"


# static fields
.field public static final ABORTED:Landroid/database/MatrixCursor;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mIncludePlusPages:Z

.field private final mMinQueryLength:I

.field private volatile mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

.field private final mProjection:[Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Z
    .param p7    # Z
    .param p8    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mMinQueryLength:I

    iput-boolean p6, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    if-eqz p7, :cond_0

    const-string v0, "gaia_id IS NOT NULL"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->setSelection(Ljava/lang/String;)V

    iput-object p8, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private abort()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->abort()V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    return-void
.end method


# virtual methods
.method public final cancelLoad()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->abort()V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->cancelLoad()Z

    move-result v0

    return v0
.end method

.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mMinQueryLength:I

    if-ge v3, v4, :cond_2

    :cond_0
    new-instance v11, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v11, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object v11

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->start()V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->isAborted()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v11, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "PublicProfileSearch"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->logError(Ljava/lang/String;)V

    const/4 v11, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    throw v3

    :cond_4
    new-instance v11, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v11, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->getPeopleSearchResults()Ljava/util/List;

    move-result-object v18

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->getContinuationToken()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    new-array v12, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    aput-object v4, v12, v3

    const/4 v3, 0x1

    aput-object v23, v12, v3

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz v18, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v21

    :goto_1
    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/PeopleResult;

    move-object/from16 v0, v17

    iget-object v15, v0, Lcom/google/api/services/plusi/model/PeopleResult;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/api/services/plusi/model/PeopleResult;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v14, :cond_12

    if-eqz v15, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/4 v13, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    if-ge v13, v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    aget-object v10, v3, v13

    const-string v3, "_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v19, v13

    :cond_5
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_6
    const/16 v21, 0x0

    goto :goto_1

    :cond_7
    const-string v3, "gaia_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v14, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v3, v19, v13

    goto :goto_4

    :cond_8
    const-string v3, "person_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_4

    :cond_9
    const-string v3, "name"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    aput-object v3, v19, v13

    goto :goto_4

    :cond_a
    const-string v3, "profile_type"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->entityInfo:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;

    if-eqz v3, :cond_b

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->entityInfo:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;->type:Ljava/lang/Integer;

    aput-object v3, v19, v13

    goto :goto_4

    :cond_b
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_4

    :cond_c
    const-string v3, "avatar"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_4

    :cond_d
    const-string v3, "snippet"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleResult;->snippetHtml:Ljava/lang/String;

    move-object/from16 v22, v0

    if-nez v22, :cond_e

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    if-eqz v3, :cond_10

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    if-eqz v3, :cond_f

    sget v3, Lcom/google/android/apps/plus/R$string;->people_search_job:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    :cond_e
    :goto_5
    aput-object v22, v19, v13

    goto/16 :goto_4

    :cond_f
    iget-object v0, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_5

    :cond_10
    iget-object v0, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_5

    :cond_11
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_12
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_2
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method public final onAbandon()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->abort()V

    return-void
.end method
