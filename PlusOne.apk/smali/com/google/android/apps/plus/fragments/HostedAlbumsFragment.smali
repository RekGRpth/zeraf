.class public Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedAlbumsFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

.field private mAuthkey:Ljava/lang/String;

.field private mControlPrimarySpinner:Z

.field private mCurrentSpinnerPosition:I

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mExtras:Landroid/os/Bundle;

.field private mGaiaId:Ljava/lang/String;

.field private mIsPhotosHome:Z

.field private mPersonId:Ljava/lang/String;

.field private mPickerMode:I

.field private mShowLocalCameraAlbum:Z

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mControlPrimarySpinner:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_1

    move v1, v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v5, :cond_0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    :goto_1
    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateSpinner()V

    return-void

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->showContent(Landroid/view/View;)V

    goto :goto_2

    :cond_4
    sget v3, Lcom/google/android/apps/plus/R$string;->no_albums:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected final needsAsyncData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lvedroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 17
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v15, v14}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v14, 0x8

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/google/android/apps/plus/R$string;->photos_home_unknown_label:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    :goto_1
    const/4 v14, 0x5

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v1, 0x0

    :goto_2
    const/4 v14, 0x4

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_3

    const/4 v13, 0x0

    :goto_3
    const/4 v14, 0x6

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_4

    const/4 v7, 0x0

    :goto_4
    const/4 v14, 0x7

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_5

    const/4 v10, 0x0

    :goto_5
    const-string v14, "photos_of_me"

    invoke-static {v13, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    const/4 v8, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "photo_picker_mode"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "hide_camera_videos"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "hide_camera_videos"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v14, v15}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAuthkey:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mIsPhotosHome:Z

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mShowLocalCameraAlbum:Z

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideCameraVideos(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    if-eqz v9, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "photo_picker_crop_mode"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "photo_picker_crop_mode"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "photo_picker_title"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v15, "photo_picker_title"

    invoke-virtual {v14, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    :goto_9
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v14}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v6

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v14}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_1
    const/16 v14, 0x8

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    :cond_2
    const/4 v14, 0x5

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_3
    const/4 v14, 0x4

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_3

    :cond_4
    const/4 v14, 0x6

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_4

    :cond_5
    const/4 v14, 0x7

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_5

    :cond_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mGaiaId:Ljava/lang/String;

    goto/16 :goto_6

    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_8
    const/4 v4, 0x0

    goto :goto_8

    :cond_9
    const/4 v12, 0x0

    goto :goto_9

    :cond_a
    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "com.google.android.apps.plus.PhotosHomeFragment.INTENT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string v0, "com.google.android.apps.plus.PhotosHomeFragment.USER_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mUserName:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPersonId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mGaiaId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "photos_home"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mIsPhotosHome:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "photos_show_camera_album"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mShowLocalCameraAlbum:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "photo_picker_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPickerMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAuthkey:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPickerMode:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->invalidateActionBar()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "hide_photos_of_me"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mUserName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mIsPhotosHome:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mShowLocalCameraAlbum:Z

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v8, 0x0

    sget v4, Lcom/google/android/apps/plus/R$layout;->photo_home_view:I

    invoke-super {p0, p1, p2, p3, v4}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getAlbumColumns(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    iget v4, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget v4, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v5, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v6, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v7, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    new-instance v4, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-direct {v4, v0, v8, v1, p0}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v8, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->no_albums:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V

    return-object v3
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->onAsyncData()V

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mControlPrimarySpinner:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPickerMode:I

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ProfileActivity;->createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mCurrentSpinnerPosition:I

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->photo_picker_photos_home_label:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    goto :goto_0
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 6
    .param p1    # I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mControlPrimarySpinner:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mCurrentSpinnerPosition:I

    if-eq v1, p1, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mCurrentSpinnerPosition:I

    :cond_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mPersonId:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.plus.PhotosHomeFragment.INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "com.google.android.apps.plus.PhotosHomeFragment.USER_NAME"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mUserName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final refresh()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mIsPhotosHome:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAuthkey:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getPhotosHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getAlbumList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public final relinquishPrimarySpinner()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mControlPrimarySpinner:Z

    return-void
.end method
