.class final Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "BaseStreamSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$000(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$000(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->dismissProgressDialog()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v1, p3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$100(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$002(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->finishActivity(I)V

    goto :goto_0
.end method

.method public final onSetVolumeControlsRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$000(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$000(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->dismissProgressDialog()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v1, p2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$100(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->access$002(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->finishActivity(I)V

    goto :goto_0
.end method
