.class public Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "LocationPickerFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;,
        Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;"
    }
.end annotation


# static fields
.field private static final ITEM_KEEP_LOCATION:Ljava/lang/Object;


# instance fields
.field private mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

.field private mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mCurrentMapUrl:Ljava/lang/String;

.field private mIsLandscapeMode:Z

.field protected mListView:Landroid/widget/ListView;

.field private mLoadPlacesNeeded:Z

.field private mLoadSearchNeeded:Z

.field private mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field private mLocationListener:Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;

.field private mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

.field private mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

.field private mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

.field private mPrevScrollItemCount:I

.field private mPrevScrollPosition:I

.field private mQuery:Ljava/lang/String;

.field private mScrollOffset:I

.field private mScrollPos:I

.field private mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

.field private mSearchMode:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->ITEM_KEEP_LOCATION:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollPosition:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollItemCount:I

    new-instance v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationListener:Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/views/ImageResourceView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/location/Location;Z)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Landroid/location/Location;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->createStaticMapUrl(Landroid/location/Location;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->updateView$3c7ec8c3()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Lcom/google/android/apps/plus/content/DbLocation;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->sendResult(Lcom/google/android/apps/plus/content/DbLocation;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/phone/LocationController;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Lcom/google/android/apps/plus/api/LocationQuery;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Lcom/google/android/apps/plus/api/LocationQuery;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method private createStaticMapUrl(Landroid/location/Location;Z)Ljava/lang/String;
    .locals 12
    .param p1    # Landroid/location/Location;
    .param p2    # Z

    const/4 v11, 0x2

    const/4 v8, 0x0

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->location_picker_map_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v4

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    const-string v3, "https://maps.googleapis.com/maps/api/staticmap"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "zoom"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "size"

    const-string v5, "%dx%d"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "format"

    const-string v5, "png"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "maptype"

    const-string v5, "roadmap"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "sensor"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "markers"

    const-string v6, "color:%s|%.6f,%.6f"

    const/4 v3, 0x3

    new-array v7, v3, [Ljava/lang/Object;

    if-eqz p2, :cond_1

    const-string v3, "red"

    :goto_0
    aput-object v3, v7, v8

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v7, v10

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->PLUS_STATICMAPS_API_KEY:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "key"

    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_1
    const-string v3, "blue"

    goto :goto_0
.end method

.method private doSearch()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_0
.end method

.method private isSearchWithNoEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeLocationListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    return-void
.end method

.method private sendResult(Lcom/google/android/apps/plus/content/DbLocation;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "location"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private setupAndShowEmptyView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->isSearchWithNoEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->enter_location_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    return-void

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$string;->no_locations:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private showProgress(Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->isSearchWithNoEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateView$3c7ec8c3()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mIsLandscapeMode:Z

    if-nez v1, :cond_0

    const/16 v0, 0x8

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showContent(Landroid/view/View;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setSearchMode(Z)V

    :cond_0
    return-void
.end method

.method public final onBackPressed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setSearchMode(Z)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onBackPressed()Z

    move-result v0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p1, :cond_1

    const-string v3, "location"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/LocationQuery;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    const-string v3, "current_location"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    const-string v3, "search_mode"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    const-string v3, "query"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    const-string v3, "current_map_url"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    const-string v3, "scroll_pos"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollPos:I

    const-string v3, "scroll_off"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v4, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->invalidateActionBar()V

    return-void

    :cond_1
    iput v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollPos:I

    iput v4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    const-string v3, "location"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "location"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/DbLocation;

    new-instance v3, Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbLocation;->getAndroidLocation()Landroid/location/Location;

    move-result-object v4

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    if-nez p1, :cond_0

    new-instance v7, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {v7, v0, v5}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->isSearchWithNoEntry()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v8, "no_location_stream_key"

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/content/EsProvider;->buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "places_only"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v4, "name IS NOT NULL"

    :goto_2
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/PlacesAdapter$LocationQuery;->PROJECTION:[Ljava/lang/String;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_0

    :cond_1
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_2
    move-object v4, v5

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v13, Lcom/google/android/apps/plus/R$layout;->checkin_list:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-super {v0, v1, v2, v3, v13}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_1

    const/4 v13, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mIsLandscapeMode:Z

    const v13, 0x102000a

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    sget v13, Lcom/google/android/apps/plus/R$id;->map:I

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/android/apps/plus/views/ImageResourceView;->setImageResourceFlags(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v13, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    sget v14, Lcom/google/android/apps/plus/R$layout;->location_row_layout:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const v13, 0x1020006

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    sget v13, Lcom/google/android/apps/plus/R$drawable;->ic_location_active:I

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    const v13, 0x1020016

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    const v13, 0x1020005

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbLocation;->isPrecise()Z

    move-result v13

    if-eqz v13, :cond_2

    sget v13, Lcom/google/android/apps/plus/R$string;->my_location:I

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v13, Lcom/google/android/apps/plus/R$id;->remove_button:I

    invoke-virtual {v5, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/view/View;->setVisibility(I)V

    new-instance v13, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V

    invoke-virtual {v8, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    sget-object v14, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->ITEM_KEEP_LOCATION:Ljava/lang/Object;

    const/4 v15, 0x1

    invoke-virtual {v13, v5, v14, v15}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbLocation;->getAndroidLocation()Landroid/location/Location;

    move-result-object v13

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->createStaticMapUrl(Landroid/location/Location;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    const/4 v10, 0x1

    :goto_2
    if-eqz v10, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v14, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    sget-object v16, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v14 .. v16}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mIsLandscapeMode:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    :cond_0
    new-instance v13, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/google/android/apps/plus/phone/PlacesAdapter;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    new-instance v13, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/google/android/apps/plus/phone/PlacesAdapter;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    :goto_4
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v13, v14}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v13, Lcom/google/android/apps/plus/R$string;->no_locations:I

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setupEmptyView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v12

    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbLocation;->isCoarse()Z

    move-result v13

    if-eqz v13, :cond_3

    sget v13, Lcom/google/android/apps/plus/R$string;->my_city:I

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    const/4 v10, 0x1

    goto/16 :goto_2

    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mMapView:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    goto :goto_4
.end method

.method public final onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    :cond_0
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->doSearch()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    sget-object v7, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->ITEM_KEEP_LOCATION:Ljava/lang/Object;

    if-ne v4, v7, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v7, "android.intent.action.PICK"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v7, v5}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "audience"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_2

    const-string v7, "audience"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->startActivity(Landroid/content/Intent;)V

    :cond_3
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->sendResult(Lcom/google/android/apps/plus/content/DbLocation;)V

    goto :goto_0

    :cond_4
    if-ne v4, v2, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v5

    goto :goto_1
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->updateView$3c7ec8c3()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    goto :goto_0
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-static {}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->onPause()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-static {}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->onPause()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->removeLocationListener()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->search_location_hint_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->post_checkin_title:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_search:I

    sget v3, Lcom/google/android/apps/plus/R$string;->menu_search:I

    invoke-virtual {p1, v2, v1, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final onQueryClose()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->doSearch()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public final onResume()V
    .locals 8

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->onResume()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-wide/16 v4, 0xbb8

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v6

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationListener:Lcom/google/android/apps/plus/fragments/LocationPickerFragment$CheckinLocationListener;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x1bfb7a8

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentActivity;->showDialog(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->init()V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollPos:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    :cond_0
    :goto_0
    const-string v0, "scroll_pos"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scroll_off"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_2

    const-string v0, "location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "search_mode"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_3

    const-string v0, "current_location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    const-string v0, "current_map_url"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentMapUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    iput v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    goto :goto_0

    :cond_5
    iput v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mScrollOffset:I

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-lez p4, :cond_1

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollPosition:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollItemCount:I

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollPosition:I

    iput p4, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPrevScrollItemCount:I

    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    return-void
.end method

.method public final onUpButtonClicked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setSearchMode(Z)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onUpButtonClicked()Z

    move-result v0

    goto :goto_0
.end method

.method public final setSearchMode(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mSearchAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mCurrentAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->doSearch()V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->invalidateActionBar()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getView()Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->updateView$3c7ec8c3()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mPlacesAdapter:Lcom/google/android/apps/plus/phone/PlacesAdapter;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_2
.end method
