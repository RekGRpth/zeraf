.class final Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.source "HostedMessengerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RTCServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)V

    return-void
.end method


# virtual methods
.method public final onConnected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$102(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/view/View;)V

    return-void
.end method

.method public final onConversationsLoaded$abe99c5()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v0, v1, v3, v2}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v0, v1, v3, v2}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method public final onDisconnected$13462e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$102(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/view/View;)V

    return-void
.end method
