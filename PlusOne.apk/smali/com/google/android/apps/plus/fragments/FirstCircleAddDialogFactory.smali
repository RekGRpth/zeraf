.class public final Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;
.super Ljava/lang/Object;
.source "FirstCircleAddDialogFactory.java"


# direct methods
.method public static needToShow(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/google/android/apps/plus/R$string;->first_circle_picker_alert_shown_key:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public static show(Lvedroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 11
    .param p0    # Lvedroid/support/v4/app/Fragment;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lvedroid/support/v4/app/Fragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lvedroid/support/v4/app/DialogFragment;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lvedroid/support/v4/app/Fragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->oob_first_circle_picker_alert_title:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->oob_first_circle_picker_alert_message:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v9, Lcom/google/android/apps/plus/R$string;->okay_got_it:I

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v7, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    const/4 v7, 0x0

    invoke-virtual {v0, p0, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0, v3, p1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$string;->first_circle_picker_alert_shown_key:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    return-void
.end method
