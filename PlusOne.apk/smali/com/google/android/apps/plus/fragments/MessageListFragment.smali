.class public Lcom/google/android/apps/plus/fragments/MessageListFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "MessageListFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/MessageClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$MessagesQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;",
        ">;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/views/MessageClickListener;"
    }
.end annotation


# instance fields
.field private defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

.field private mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

.field private mConversationId:Ljava/lang/String;

.field private mConversationRowId:Ljava/lang/Long;

.field private mEarliestEventTimestamp:J

.field private mFirstEventTimestamp:J

.field private mHeaderView:Landroid/view/View;

.field private mInitialLoadFinished:Z

.field private mIsGroup:Z

.field private mIsTypingVisible:Z

.field private mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

.field private mLoadingOlderEvents:Z

.field private mMessagesUri:Landroid/net/Uri;

.field private mParticipantList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private final mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

.field private mRequestId:Ljava/lang/Integer;

.field private mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private mSlideInUpAnimation:Landroid/view/animation/Animation;

.field private mSlideOutDownAnimation:Landroid/view/animation/Animation;

.field private mTotalItemBeforeLoadingOlder:I

.field private mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

.field private mTypingParticipants:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTypingTextView:Landroid/widget/TextView;

.field private mTypingView:Landroid/view/View;

.field private mTypingVisibilityChanged:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/plus/fragments/MessageListFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mEarliestEventTimestamp:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mFirstEventTimestamp:J

    return-wide v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 10
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    iget-wide v6, v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->typingStartTimeMs:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    const-string v1, "MessageListFragment"

    const/4 v6, 0x3

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v6, "MessageListFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Typing status expired for "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->userName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method static synthetic access$2002(Lcom/google/android/apps/plus/fragments/MessageListFragment;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTotalItemBeforeLoadingOlder:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateTypingVisibility()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/MessageListFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    return v0
.end method

.method private declared-synchronized animateTypingVisibility()V
    .locals 14

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    if-nez v1, :cond_2

    :cond_0
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MessageListFragment"

    const-string v1, "Ignoring animation due to null views"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_4

    const/4 v12, 0x1

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    if-eq v1, v12, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v12, :cond_5

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v13

    if-eqz v12, :cond_6

    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v13

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v11

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getWidth()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0, v11, v10}, Landroid/view/animation/TranslateAnimation;->initialize(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v12, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v12, v0

    goto/16 :goto_1

    :cond_5
    const/16 v0, 0x8

    goto/16 :goto_2

    :cond_6
    :try_start_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    neg-int v6, v13

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method private declared-synchronized isTypingAnimationPlaying()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->hasEnded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method private updateHeaderVisibility()V
    .locals 3

    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateHeaderVisibility "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private declared-synchronized updateTypingVisibility()V
    .locals 10

    const/4 v8, 0x3

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v6, 0x3

    :try_start_1
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-array v5, v6, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v2, v1

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    add-int/lit8 v1, v2, 0x1

    iget-object v6, v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->userName:Ljava/lang/String;

    aput-object v6, v5, v2

    if-eq v1, v8, :cond_2

    move v2, v1

    goto :goto_1

    :cond_1
    move v1, v2

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_more_than_three_people_typing_text:I

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->isTypingAnimationPlaying()Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "MessageListFragment"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "MessageListFragment"

    const-string v7, "Animation already playing. Setting typing visibility changed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :pswitch_0
    :try_start_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :pswitch_1
    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_one_person_typing_text:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :pswitch_2
    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_two_people_typing_text:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :pswitch_3
    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_three_people_typing_text:I

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const/4 v9, 0x2

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final displayLeaveConversationDialog()V
    .locals 5

    sget v1, Lcom/google/android/apps/plus/R$string;->realtimechat_leave_conversation_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_leave_conversation_text:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_leave_menu_item_text:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "leave_conversation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final handleFatalError(I)V
    .locals 7
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_error_dialog_general:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_error_dialog_title:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_error_dialog_leave_button:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "conversation_error"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_error_dialog_huddle_too_big:I

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_error_dialog_some_invalid_participants:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final messageLoadFailed()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    const-string v1, "messageLoadFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->realtimechat_failure_loading_messages:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public final messageLoadSucceeded()V
    .locals 3

    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    const-string v1, "messageLoadSucceeded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    const-wide/16 v1, 0x1f4

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/ThreadUtil;->postDelayedOnUiThread(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public final onCancelButtonClicked(J)V
    .locals 2
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v9, 0x15e

    const-wide/16 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p1, :cond_1

    const-string v3, "request_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "request_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    :goto_0
    const-string v3, "loading_older_events"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    const-string v3, "initial_load_finished"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    :goto_1
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "participant"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "is_group"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "conversation_row_id"

    invoke-virtual {v3, v4, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v3, v1, v7

    if-eqz v3, :cond_2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/content/EsProvider;->buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v6, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_2
    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_out_down_self:I

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_in_up_self:I

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    return-void

    :cond_0
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_1
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    goto/16 :goto_1

    :cond_2
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    goto :goto_2
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessagesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "timestamp"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v4

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v8, 0x0

    sget v4, Lcom/google/android/apps/plus/R$layout;->message_list_fragment:I

    invoke-virtual {p1, v4, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    sget v4, Lcom/google/android/apps/plus/R$id;->typing_text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->typing_text_view:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$layout;->message_list_item_loading_older:I

    invoke-virtual {p1, v4, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->message_list_item_loading_content:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;Landroid/widget/AbsListView;Landroid/database/Cursor;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setChoiceMode(I)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/MessageListFragment$7;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v4, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$id;->empty_conversation_view:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v4, Lcom/google/android/apps/plus/R$id;->empty_conversation_text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->new_conversation_description:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lvedroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-object v3
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v0, "conversation_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "leave_conversation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "conversation_error"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :cond_2
    const-string v0, "MessageListFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalidate dialog "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x1

    check-cast p2, Landroid/database/Cursor;

    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->restoreScrollPosition()V

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showContent(Landroid/view/View;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    if-eqz v0, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTotalItemBeforeLoadingOlder:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onAsyncData()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public final onMediaImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoOnly(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_view_messenger_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->removeCallbacksOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->removeCallbacksOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadSucceeded()V

    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->onResume()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadFailed()V

    goto :goto_1
.end method

.method public final onRetryButtonClicked(J)V
    .locals 2
    .param p1    # J

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->retrySendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "loading_older_events"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "initial_load_finished"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public final onUserImageClicked(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final reinitialize()V
    .locals 12

    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "is_group"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "conversation_row_id"

    invoke-virtual {v4, v5, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v4, v1, v10

    if-eqz v4, :cond_1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/content/EsProvider;->buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v7, v9, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "participant"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_conversation_view:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_conversation_text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->new_conversation_description:I

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lvedroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V

    return-void

    :cond_1
    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    goto :goto_0

    :cond_2
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final setConversationInfo(Ljava/lang/String;JJ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J

    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationInfo first "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " earliest local "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mFirstEventTimestamp:J

    iput-wide p4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mEarliestEventTimestamp:J

    return-void
.end method

.method public final setLeaveConversationListener(Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    return-void
.end method

.method public final setParticipantList(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;

    return-void
.end method
