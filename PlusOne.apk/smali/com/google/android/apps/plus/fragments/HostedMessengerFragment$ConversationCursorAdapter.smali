.class final Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "HostedMessengerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConversationCursorAdapter"
.end annotation


# instance fields
.field private mConversationsCursor:Landroid/database/Cursor;

.field private mSuggestionsCursor:Landroid/database/Cursor;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/content/Context;Landroid/widget/AbsListView;)V
    .locals 2
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/widget/AbsListView;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-direct {p0, p2, v1}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->addPartition(ZZ)V

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->addPartition(ZZ)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)V

    invoke-virtual {p3, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v6, 0x7

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ConversationListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->clear()V

    const/16 v2, 0xb

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v2, 0xd

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_5

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setConversationName(Ljava/lang/CharSequence;)V

    const/16 v2, 0xf

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, ""

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_invitation_preview_text:I

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x4

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setUnreadCount(I)V

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_9

    move v2, v3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setMuted(Z)V

    const/16 v2, 0x10

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, "|"

    invoke-direct {v3, v2, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    :cond_4
    :goto_3
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v5, v5, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    const/4 v2, 0x6

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_6
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setConversationName(Ljava/lang/CharSequence;)V

    const/16 v2, 0xc

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v2, 0x8

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v2, 0x9

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-nez v5, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_participant_without_name_text:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_4
    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_name_and_message_image:I

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    if-eqz v7, :cond_3

    if-ne v6, v3, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->realtimechat_name_and_message_text:I

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v2, v8, v4

    aput-object v7, v8, v3

    invoke-virtual {v5, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    const-string v2, "\\<.*?\\>"

    const-string v5, ""

    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    move v2, v4

    goto/16 :goto_2

    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setParticipantsId(Ljava/util/List;Ljava/lang/String;)V

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->updateContentDescription()V

    goto/16 :goto_0

    :cond_b
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setParticipantsId(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_5

    :pswitch_1
    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedParticipantView;

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->setParticipantId(Ljava/lang/String;)V

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->setParticipantName(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_c
    move-object v2, v5

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final getView(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/database/Cursor;
    .param p3    # I
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :cond_1
    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V

    return-object v1

    :pswitch_0
    instance-of v0, p4, Lcom/google/android/apps/plus/views/ConversationListItemView;

    if-eqz v0, :cond_0

    move-object v1, p4

    goto :goto_0

    :pswitch_1
    instance-of v0, p4, Lcom/google/android/apps/plus/views/SuggestedParticipantView;

    if-eqz v0, :cond_0

    move-object v1, p4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->section_header_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$layout;->conversation_list_item_view:I

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/google/android/apps/plus/R$layout;->suggested_participant_view:I

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x3

    const/4 v1, 0x1

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v1, :cond_2

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mConversationsCursor:Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    :cond_0
    :goto_0
    const-string v0, "ConversationList"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished suggestions "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversations "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mConversationsCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v2, :cond_0

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_0
.end method
