.class public Lcom/google/android/apps/plus/fragments/BlockPersonDialog;
.super Lvedroid/support/v4/app/DialogFragment;
.source "BlockPersonDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;-><init>(ZLjava/io/Serializable;)V

    return-void
.end method

.method public constructor <init>(ZLjava/io/Serializable;)V
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/io/Serializable;

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "plus_page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "callback_data"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method

.method private configureExplanationLink(Landroid/widget/TextView;)V
    .locals 7
    .param p1    # Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->what_does_this_mean_link:I

    invoke-virtual {v3, v4}, Lvedroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "plusone_promo_block"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$1;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$1;-><init>(Lcom/google/android/apps/plus/fragments/BlockPersonDialog;Landroid/net/Uri;)V

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-interface {v0, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setClickable(Z)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "callback_data"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;

    invoke-interface {v2, v1}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;->blockPerson(Ljava/io/Serializable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;->blockPerson(Ljava/io/Serializable;)V

    goto :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "plus_page"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz v3, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$string;->block_page_dialog_title:I

    :goto_0
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v5, 0x104000a

    invoke-virtual {v1, v5, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v5, 0x1040000

    invoke-virtual {v1, v5, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$layout;->block_profile_confirm_dialog:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v3, :cond_1

    sget v5, Lcom/google/android/apps/plus/R$string;->block_page_dialog_message:I

    :goto_1
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->explanation:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->configureExplanationLink(Landroid/widget/TextView;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    :cond_0
    sget v5, Lcom/google/android/apps/plus/R$string;->block_person_dialog_title:I

    goto :goto_0

    :cond_1
    sget v5, Lcom/google/android/apps/plus/R$string;->block_person_dialog_message:I

    goto :goto_1
.end method

.method public final onPause()V
    .locals 3

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->explanation:I

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/DialogFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->explanation:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->configureExplanationLink(Landroid/widget/TextView;)V

    :cond_0
    return-void
.end method
