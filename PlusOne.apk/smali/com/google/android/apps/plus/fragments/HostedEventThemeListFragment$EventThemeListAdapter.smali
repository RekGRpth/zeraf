.class final Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "HostedEventThemeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventThemeListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    sget v5, Lcom/google/android/apps/plus/R$id;->progress_bar:I

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnImageListener(Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;)V

    const/4 v5, 0x2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x3

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setResourceDownloadingDrawablePath(Ljava/lang/String;)V

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    return-void
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$layout;->event_theme_list_item:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->progress_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;Landroid/widget/ProgressBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v1
.end method
