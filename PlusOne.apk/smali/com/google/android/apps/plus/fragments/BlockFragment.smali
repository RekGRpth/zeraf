.class public Lcom/google/android/apps/plus/fragments/BlockFragment;
.super Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
.source "BlockFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSetBlockedRequestId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/BlockFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/BlockFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/BlockFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/apps/plus/fragments/BlockFragment;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    if-eqz p5, :cond_1

    if-eqz p4, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->block_page_operation_pending:I

    :goto_0
    new-instance v1, Lcom/google/android/apps/plus/fragments/BlockFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/BlockFragment;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "message"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/fragments/BlockFragment;->setCancelable(Z)V

    iput-object p1, v1, Lcom/google/android/apps/plus/fragments/BlockFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1, p2, p3, p5}, Lcom/google/android/apps/plus/service/EsService;->setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v1, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    return-object v1

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$string;->block_person_operation_pending:I

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->unblock_page_operation_pending:I

    :goto_1
    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$string;->unblock_person_operation_pending:I

    goto :goto_1
.end method


# virtual methods
.method protected final handleSetBlockedCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->dismiss()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;

    :goto_1
    if-eqz v0, :cond_0

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-interface {v0, v3}, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;->onBlockCompleted(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;->onBlockCompleted(Z)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "set_blocked_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "set_blocked_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    const-string v0, "set_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "BlockFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "BlockFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->handleSetBlockedCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "set_blocked_req_id"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mSetBlockedRequestId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "set_account"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BlockFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final show(Lvedroid/support/v4/app/FragmentActivity;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Lvedroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "block_pending"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/BlockFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
