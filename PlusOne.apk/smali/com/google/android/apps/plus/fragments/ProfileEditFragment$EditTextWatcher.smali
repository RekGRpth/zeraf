.class final Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;
.super Ljava/lang/Object;
.source "ProfileEditFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextWatcher"
.end annotation


# instance fields
.field private final mOriginalValue:Ljava/lang/String;

.field private final mView:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/view/View;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->mView:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->mOriginalValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->mOriginalValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->mView:Landroid/view/View;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->mView:Landroid/view/View;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1500(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    goto :goto_0
.end method
