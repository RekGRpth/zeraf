.class public Lcom/google/android/apps/plus/content/CircleData;
.super Ljava/lang/Object;
.source "CircleData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCircleType:I

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/CircleData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/CircleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    iput v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    iget-object v0, p1, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    iput v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    iput-object p3, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/apps/plus/content/CircleData;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    iget v2, v0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    iget v2, v0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    return v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    if-eqz v1, :cond_1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    :cond_1
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    add-int v0, v1, v2

    return v0
.end method

.method public final setSize$13462e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    return-void
.end method

.method public final setType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{CircleData id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mCircleType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/content/CircleData;->mSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
