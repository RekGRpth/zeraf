.class public final Lcom/google/android/apps/plus/content/DeviceLocationSettings;
.super Ljava/lang/Object;
.source "DeviceLocationSettings.java"


# static fields
.field private static sLastSavedSettings:Lcom/google/android/apps/plus/content/DeviceLocationSettings;

.field private static sLastSavedTimestamp:J


# instance fields
.field public final bestAvailableLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field public final isLocationSharingEnabled:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(ZLcom/google/android/apps/plus/content/AudienceData;)V
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->isLocationSharingEnabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->bestAvailableLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method

.method public static getRecentlySavedSettings()Lcom/google/android/apps/plus/content/DeviceLocationSettings;
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedTimestamp:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedTimestamp:J

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedSettings:Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    :cond_0
    const-string v0, "DeviceLocationSettings"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "DeviceLocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Using cached location sharing settings: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedSettings:Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedSettings:Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onSettingsSaved(ZLcom/google/android/apps/plus/content/AudienceData;)V
    .locals 2
    .param p0    # Z
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedTimestamp:J

    new-instance v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/content/DeviceLocationSettings;-><init>(ZLcom/google/android/apps/plus/content/AudienceData;)V

    sput-object v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->sLastSavedSettings:Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    return-void
.end method
