.class public final Lcom/google/android/apps/plus/content/VolumeSettings;
.super Ljava/lang/Object;
.source "VolumeSettings.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5a20bd9d10f8f861L


# instance fields
.field private final mNotificationsEnabled:Z

.field private final mVolume:I


# direct methods
.method public constructor <init>(Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "ON"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;->notificationSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mNotificationsEnabled:Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;->streamVolumeSetting:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getVolumeControlValue(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mVolume:I

    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 0
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mNotificationsEnabled:Z

    iput p2, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mVolume:I

    return-void
.end method


# virtual methods
.method public final getNotificationsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mNotificationsEnabled:Z

    return v0
.end method

.method public final getVolume()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mVolume:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VolumeSettings subscribed="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mNotificationsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/VolumeSettings;->mVolume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
