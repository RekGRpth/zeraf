.class public final Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;
.super Ljava/lang/Object;
.source "PhotoTaggeeData.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/PhotoTaggeeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoTaggee"
.end annotation


# instance fields
.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->mName:Ljava/lang/String;

    return-object v0
.end method
