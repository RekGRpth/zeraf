.class public final Lcom/google/android/apps/plus/content/DbEmbedSkyjam;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedSkyjam.java"


# instance fields
.field protected mAlbum:Ljava/lang/String;

.field protected mArtist:Ljava/lang/String;

.field protected mImageUrl:Ljava/lang/String;

.field protected mMarketUrl:Ljava/lang/String;

.field protected mPreviewUrl:Ljava/lang/String;

.field protected mSong:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->byArtist:Lcom/google/api/services/plusi/model/MusicGroup;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MusicGroup;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mArtist:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mAlbum:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mImageUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->offerUrlWithSessionIndex:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mMarketUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->audioUrlWithSessionIndex:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mPreviewUrl:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/PlayMusicTrack;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/PlayMusicTrack;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mSong:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->byArtist:Lcom/google/api/services/plusi/model/MusicGroup;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MusicGroup;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mArtist:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->inAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mAlbum:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->inAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlayMusicAlbum;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mImageUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->offerUrlWithSessionIndex:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mMarketUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlayMusicTrack;->audioEmbedUrlWithSessionIndex:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mPreviewUrl:Ljava/lang/String;

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSkyjam;
    .locals 3
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mSong:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mArtist:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mAlbum:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mImageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mMarketUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mPreviewUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method private static serialize(Lcom/google/android/apps/plus/content/DbEmbedSkyjam;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedSkyjam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mSong:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mArtist:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mAlbum:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mImageUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mMarketUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mPreviewUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/PlayMusicAlbum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->serialize(Lcom/google/android/apps/plus/content/DbEmbedSkyjam;)[B

    move-result-object v0

    return-object v0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/PlayMusicTrack;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/PlayMusicTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicTrack;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->serialize(Lcom/google/android/apps/plus/content/DbEmbedSkyjam;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAlbum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method public final getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mArtist:Ljava/lang/String;

    return-object v0
.end method

.method public final getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getMarketUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mMarketUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mPreviewUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getSong()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mSong:Ljava/lang/String;

    return-object v0
.end method

.method public final isAlbum()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->mSong:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
