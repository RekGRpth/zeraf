.class public final Lcom/google/android/apps/plus/content/PeopleData$Factory;
.super Ljava/lang/Object;
.source "PeopleData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/PeopleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/PeopleData$Factory;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/PeopleData;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Lcom/google/android/apps/plus/content/PeopleData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/plus/content/PeopleData;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;B)V

    return-object v0
.end method
