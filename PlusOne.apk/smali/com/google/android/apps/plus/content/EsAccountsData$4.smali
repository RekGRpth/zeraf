.class final Lcom/google/android/apps/plus/content/EsAccountsData$4;
.super Ljava/lang/Object;
.source "EsAccountsData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/content/EsAccountsData;->insertExperiments(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    # getter for: Lcom/google/android/apps/plus/content/EsAccountsData;->sExperimentListeners:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/apps/plus/content/EsAccountsData;->access$000()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    # getter for: Lcom/google/android/apps/plus/content/EsAccountsData;->sExperimentListeners:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/apps/plus/content/EsAccountsData;->access$000()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;

    invoke-interface {v2}, Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;->onExperimentsChanged()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
