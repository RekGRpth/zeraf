.class public final Lcom/google/android/apps/plus/content/DbAudienceData;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbAudienceData.java"


# direct methods
.method private static deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 13
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getCircle(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v9

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v10

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-direct {v12, v8, v9, v10, v11}, Lcom/google/android/apps/plus/content/SquareTargetData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    new-instance v8, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v8, v5, v0, v6, v7}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v8
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 2
    .param p0    # [B

    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    return-object v1
.end method

.method public static deserializeList([B)Ljava/util/ArrayList;
    .locals 5
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/AudienceData;)[B
    .locals 11
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v6, v0, v4

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbAudienceData;->putPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/PersonData;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/content/DbAudienceData;->putCircle(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/CircleData;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v5, :cond_2

    aget-object v7, v0, v4

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v10

    add-int v8, v9, v10

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    return-object v9

    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    throw v9
.end method

.method public static serialize(Ljava/util/ArrayList;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->serialize(Lcom/google/android/apps/plus/content/AudienceData;)[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    return-object v5

    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    throw v5
.end method
