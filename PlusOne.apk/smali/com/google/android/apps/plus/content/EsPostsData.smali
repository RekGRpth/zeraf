.class public final Lcom/google/android/apps/plus/content/EsPostsData;
.super Ljava/lang/Object;
.source "EsPostsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsPostsData$UpdateCommentDataQuery;,
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;,
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityWhatsHotQuery;,
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityPromoQuery;,
        Lcom/google/android/apps/plus/content/EsPostsData$CircleNameQuery;,
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityStreamKeyQuery;
    }
.end annotation


# static fields
.field private static final ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

.field private static sEmbedsWhitelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMaxContentLength:Ljava/lang/Integer;

.field private static sMixinsWhitelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMixinsWithPopularWhitelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sShareboxWhitelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sStreamItemTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sStreamItemsWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sStreamNamespaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSyncEnabled:Z

.field private static final sSyncLock:Ljava/lang/Object;

.field private static sWidgetStreamNamespaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncLock:Ljava/lang/Object;

    sput-boolean v3, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activity_id"

    aput-object v2, v0, v1

    const-string v1, "modified"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "data_state"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public static applyPromoCardCirclesChange$79e449f2(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getSuggestedPeople()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0, p1, p4}, Lcom/google/android/apps/plus/content/EsPostsData;->createCircleDataList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->addCircles(Ljava/util/List;)V

    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {v2, p5}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->removeCircleList([Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p3    # Z
    .param p4    # I

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->buildStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v3, 0x0

    const/4 v0, 0x0

    move-object v1, v0

    move-object v2, v0

    move-object v4, p0

    move-object v5, p1

    move v6, v3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->buildStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/16 v2, 0x2c

    const/16 v3, 0x7c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "f."

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->hasCoordinates()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLatitudeE7()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLongitudeE7()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getPrecisionMeters()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_2
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x0

    sget-boolean v2, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v6, v6, v6, v3, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream_key NOT IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "activity_streams"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "deleteNonEssentialStreams deleted streams: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v2, "activities"

    const-string v3, "activity_id NOT IN (SELECT activity_id FROM activity_streams)"

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const-string v2, "EsPostsData"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cleanupData deleted unreferenced activities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getAvailableStorage()J

    move-result-wide v2

    const-wide/32 v4, 0xf42400

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    const-string v2, "activities"

    const-string v3, "activity_id IN (SELECT activity_id FROM activity_streams WHERE sort_index > 50)"

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v2, "EsPostsData"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cleanupData deleted \"all circles\" activities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteUnusedLocations(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0
.end method

.method public static createCircleDataList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    sget-object v5, Lcom/google/android/apps/plus/content/EsPostsData$CircleNameQuery;->PROJECTION:[Ljava/lang/String;

    invoke-static {p0, p1, v6, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_5

    :try_start_0
    array-length v5, p2

    add-int/lit8 v2, v5, -0x1

    :goto_0
    if-ltz v2, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    aget-object v1, p2, v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v1, v6, v3, v7}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v4

    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5
.end method

.method private static createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/Comment;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    const-string v3, "activity_id"

    invoke-virtual {p2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "comment_id"

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "author_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "content"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "created"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :try_start_0
    iget-object v3, p0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B

    move-result-object v2

    :goto_0
    const-string v3, "plus_one_data"

    invoke-virtual {p2, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "plus_one_data"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static createSquareStreamRenderContext(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/api/services/plusi/model/RenderContext;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/RenderContext;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RenderContext;-><init>()V

    const-string v1, "MOBILE_SQUARE_STREAM"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RenderContext;->location:Ljava/lang/String;

    iput-object p1, v0, Lcom/google/api/services/plusi/model/RenderContext;->viewerIsModerator:Ljava/lang/Boolean;

    const-string v1, "FILTERED_POSTMOD"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RenderContext;->moderationViewType:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RenderContext;->streamId:Ljava/util/List;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/RenderContext;->streamId:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "EsPostsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, ">>>>> deleteActivity id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x1

    :try_start_0
    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v3, v6

    const-string v6, "activity_streams"

    const-string v7, "activity_id=?"

    invoke-virtual {v0, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v6, "activities"

    const-string v7, "activity_id=?"

    invoke-virtual {v0, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    :cond_1
    return-void
.end method

.method public static deleteActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream_key IN("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x29

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "activity_streams"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "deleteActivityStream deleted streams: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v4, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v1, v4

    :try_start_0
    const-string v4, "SELECT activity_id FROM activity_comments WHERE comment_id = ?"

    invoke-static {v2, v4, v1}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, ">>>> deleteComment: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for activity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v4, "comment_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v4, "activity_comments"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v4, -0x1

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz v0, :cond_1

    invoke-static {v2, p0, p1, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string v4, "EsPostsData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WARNING: could not find photo for the comment: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method private static deleteUnusedLocations(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string v1, "location_queries"

    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    const-string v7, "_id DESC"

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v11, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "_id IN("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v9, 0x1

    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v9, :cond_2

    const/4 v9, 0x0

    :goto_2
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    const/16 v0, 0x2c

    :try_start_1
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "location_queries"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    .locals 17
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p10    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-static {v0, v1, v4, v5, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v16

    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "doActivityStreamSync starting sync stream: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Lcom/google/android/apps/plus/api/GetActivitiesOperation;

    const/4 v14, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    move/from16 v12, p8

    move-object/from16 v13, p10

    move-object/from16 v15, p9

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->start()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    throw v4

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getErrorCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    return-object v4
.end method

.method public static doNearbyActivitiesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p6    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v2, p2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v9

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "doNearbyActivitiesSync starting sync stream: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    throw v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    return-object v1
.end method

.method private static extractPromoActivityId(Lcom/google/api/services/plusi/model/Promo;)Ljava/lang/String;
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/Promo;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "~promo:~"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v3, "FIND_FRIENDS"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->findFriendsPromoMessage:Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

    if-eqz v2, :cond_2

    const-string v1, "ff:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/Promo;->findFriendsPromoMessage:Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v3, "SUGGESTED_FRIENDS"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoMessage:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;

    if-eqz v2, :cond_3

    const-string v1, "sf:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoMessage:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v3, "SUGGESTED_CELEBRITIES"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoMessage:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;

    if-eqz v2, :cond_4

    const-string v1, "sc:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoMessage:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v2, "EsPostsData"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPostsData"

    const-string v3, "unknown stream type"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getActivityImageData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "activities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "embed_media"

    aput-object v4, v2, v3

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    if-eqz v11, :cond_1

    array-length v1, v11

    if-eqz v1, :cond_1

    invoke-static {v11}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v1, Lcom/google/android/apps/plus/R$dimen;->notification_bigpicture_width:I

    invoke-virtual {v15, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v13, v1

    sget v1, Lcom/google/android/apps/plus/R$dimen;->notification_bigpicture_width:I

    invoke-virtual {v15, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v12, v1

    new-instance v14, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v14, v1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v10, v14, v13, v12, v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;
    :try_end_1
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v1, "EsPostsData"

    const-string v2, "Cannot download media"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static getActivityLastEditedTime(Lcom/google/api/services/plusi/model/Update;)J
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/Update;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getActivityLastModifiedTime(Lcom/google/api/services/plusi/model/Update;)J
    .locals 8
    .param p0    # Lcom/google/api/services/plusi/model/Update;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityLastEditedTime(Lcom/google/api/services/plusi/model/Update;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Comment;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v6, v0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->timeModifiedMs:Ljava/lang/Double;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v4

    double-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :cond_1
    return-wide v2
.end method

.method private static getActivityStatuses(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/HashMap;
    .locals 16
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;",
            ">;"
        }
    .end annotation

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "activity_id IN ("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/api/services/plusi/model/Update;

    const-string v0, "?,"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v15, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v0, ")"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    const-string v1, "activities"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPostsData;->ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;

    const/4 v0, 0x0

    invoke-direct {v13, v0}, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;-><init>(B)V

    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->timestamp:J

    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->dataStatus:I

    invoke-virtual {v14, v8, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-object v14
.end method

.method private static getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "activity_streams"

    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStreamKeyQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "activity_id=?"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p1, v5, v0

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_0

    :goto_0
    return-object v11

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private static getAlbumOrMedia(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)Lcom/google/android/apps/plus/content/DbSerializer;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;-><init>(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V

    :goto_1
    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V

    goto :goto_1
.end method

.method private static getAvailableStorage()J
    .locals 9

    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    new-instance v4, Landroid/os/StatFs;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v7, v7

    mul-long v0, v5, v7

    const-string v5, "EsPostsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "EsPostsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getAvailableStorage: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    const-string v5, "EsPostsData"

    const-string v6, "getAvailableStorage"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 9
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v1, "activity_comments"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "comment_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-object v5

    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v5, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v12, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "activities"

    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData$ActivityPromoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "activity_id=?"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p2, v5, v7

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-nez v11, :cond_0

    :goto_0
    return-object v6

    :cond_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->deserialize([B)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move-object v6, v12

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getDefaultText(J)I
    .locals 5
    .param p0    # J

    const-wide/16 v3, 0x0

    const-wide/16 v1, 0x100

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_event:I

    :goto_0
    return v0

    :cond_0
    const-wide/16 v1, 0x400

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_skyjam:I

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x40

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_album:I

    goto :goto_0

    :cond_2
    const-wide/16 v1, 0x80

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_video:I

    goto :goto_0

    :cond_3
    const-wide/16 v1, 0x804

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_4

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_link:I

    goto :goto_0

    :cond_4
    const-wide/16 v1, 0x20

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_5

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_image:I

    goto :goto_0

    :cond_5
    const-wide/32 v1, 0x40000

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_6

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_emotishare:I

    goto :goto_0

    :cond_6
    const-wide/16 v1, 0x8

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_7

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_location:I

    goto :goto_0

    :cond_7
    const-wide/16 v1, 0x1000

    and-long/2addr v1, p0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_8

    sget v0, Lcom/google/android/apps/plus/R$string;->card_auto_text_review:I

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEmbedsWhitelist()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "SQUARE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "SQUARE_INVITE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "APP_INVITE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "WEB_PAGE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLUS_PHOTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLUS_PHOTO_ALBUM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "VIDEO_OBJECT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "CHECKIN"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLACE_REVIEW"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLUS_PHOTOS_ADDED_TO_COLLECTION"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLUS_EVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLAY_MUSIC_TRACK"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLAY_MUSIC_ALBUM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "HANGOUT_CONSUMER"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "EMOTISHARE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    const-string v1, "THING"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sEmbedsWhitelist:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getMixinsWhitelist(Z)Ljava/util/ArrayList;
    .locals 2
    .param p0    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    const-string v1, "POPULAR_RECOMMENDATIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    const-string v1, "SQUARES"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    const-string v1, "BIRTHDAYS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_MIXIN:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    const-string v1, "THIRD_PARTY_POSTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWithPopularWhitelist:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWhitelist:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWhitelist:Ljava/util/ArrayList;

    const-string v1, "SQUARES"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWhitelist:Ljava/util/ArrayList;

    const-string v1, "BIRTHDAYS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_MIXIN:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWhitelist:Ljava/util/ArrayList;

    const-string v1, "THIRD_PARTY_POSTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sMixinsWhitelist:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private static getMostRecentSortIndex(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 11
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v10, 0x0

    const-string v1, "activity_streams"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "sort_index"

    aput-object v0, v2, v10

    const-string v3, "stream_key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    const-string v7, "sort_index ASC"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-nez v9, :cond_0

    move v0, v10

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 9
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-object v5

    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v5, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getShareboxEmbedsWhitelist()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "APP_INVITE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "WEB_PAGE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "VIDEO_OBJECT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLAY_MUSIC_TRACK"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "PLAY_MUSIC_ALBUM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    const-string v1, "THING"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sShareboxWhitelist:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getStreamItemTypes()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemTypes:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemTypes:Ljava/util/ArrayList;

    const-string v1, "PROMO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemTypes:Ljava/util/ArrayList;

    const-string v1, "UPDATE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getStreamItemsWhiteList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemsWhiteList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemsWhiteList:Ljava/util/ArrayList;

    const-string v1, "FIND_FRIENDS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemsWhiteList:Ljava/util/ArrayList;

    const-string v1, "SUGGESTED_FRIENDS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemsWhiteList:Ljava/util/ArrayList;

    const-string v1, "SUGGESTED_CELEBRITIES"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamItemsWhiteList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getStreamNamespaces(Z)Ljava/util/ArrayList;
    .locals 2
    .param p0    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "STREAM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PHOTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "BIRTHDAY"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "STREAM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "EVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "SEARCH"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PLUSONE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PHOTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "A2A"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "BIRTHDAY"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PHOTOS_ADDED_TO_EVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public static hasStreamChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)Z
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;)Z"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    const/4 v11, 0x0

    :cond_1
    :goto_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v1, "activity_streams"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "activity_id"

    aput-object v4, v2, v3

    const-string v3, "stream_key=? AND activity_id NOT LIKE \'~promo:~%\'"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "sort_index ASC"

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_4

    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasStreamChanged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,server activity id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,local activity id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-nez v9, :cond_7

    if-nez v11, :cond_6

    const/4 v1, 0x0

    :goto_2
    return v1

    :cond_3
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Update;

    iget-object v11, v1, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasStreamChanged received: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    :goto_3
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :cond_5
    const/4 v9, 0x0

    goto :goto_3

    :catchall_1
    move-exception v1

    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    const/4 v1, 0x1

    goto :goto_2

    :cond_7
    if-nez v11, :cond_8

    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    invoke-static {v2, p2, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesIntoStreamInTransaction$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz p5, :cond_1

    if-eqz p2, :cond_2

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_2
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/plusi/model/Update;

    iget-object v0, v7, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-static {v2, p0, p1, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 45
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v43

    if-nez v43, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static/range {p2 .. p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStatuses(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v42

    const-string v2, "DEFAULT"

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    const/4 v14, 0x1

    :goto_0
    new-instance v44, Landroid/content/ContentValues;

    invoke-direct/range {v44 .. v44}, Landroid/content/ContentValues;-><init>()V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_2
    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/plusi/model/Update;

    iget-object v6, v8, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> Activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", author id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", updated: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", read: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityLastEditedTime(Lcom/google/api/services/plusi/model/Update;)J

    move-result-wide v21

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityLastModifiedTime(Lcom/google/api/services/plusi/model/Update;)J

    move-result-wide v30

    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;

    if-nez p5, :cond_5

    if-eqz v41, :cond_4

    move-object/from16 v0, v41

    iget-wide v2, v0, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->timestamp:J

    cmp-long v2, v30, v2

    if-nez v2, :cond_4

    move-object/from16 v0, v41

    iget v2, v0, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->dataStatus:I

    if-eqz v2, :cond_17

    :cond_4
    const/4 v13, 0x1

    :goto_2
    if-eqz v13, :cond_2

    :cond_5
    const-wide/16 v16, 0x0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->authorName:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->photoUrl:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual/range {v44 .. v44}, Landroid/content/ContentValues;->clear()V

    const-string v2, "embed_appinvite"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_checkin"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_deep_link"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_emotishare"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_hangout"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_media"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_photo_album"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_place"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_place_review"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_skyjam"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "embed_square"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "promo"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v2, "activity_id"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "author_id"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "source_id"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->sourceStreamId:Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "source_name"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->sourceStreamName:Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "data_state"

    if-eqz v14, :cond_18

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->place:Lcom/google/api/services/plusi/model/Place;

    if-eqz v2, :cond_19

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v2, :cond_6

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->checkin:Lcom/google/api/services/plusi/model/Checkin;

    if-nez v2, :cond_19

    :cond_6
    const-string v2, "loc"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->place:Lcom/google/api/services/plusi/model/Place;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/api/services/plusi/model/Place;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v16, 0x8

    :goto_4
    sget-object v2, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    if-nez v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$integer;->stream_post_max_length:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    :cond_7
    const-string v2, "annotation"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v2, v3, :cond_8

    sget v2, Lcom/google/android/apps/plus/R$string;->stream_truncated_info:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v9, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v36

    :cond_8
    const-string v2, "annotation_plaintext"

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x2

    or-long v16, v16, v2

    :goto_5
    const-string v2, "title"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v2, v3, :cond_9

    sget v2, Lcom/google/android/apps/plus/R$string;->stream_truncated_info:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v9, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v36

    :cond_9
    const-string v2, "title_plaintext"

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x1

    or-long v16, v16, v2

    :goto_6
    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v32, 0x0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->originalItemId:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->originalItemId:Ljava/lang/String;

    invoke-static {v6, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    invoke-static {v6, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_b
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    if-eqz v2, :cond_1c

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v33, v0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    move-object/from16 v34, v0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->photoUrl:Ljava/lang/String;

    move-object/from16 v32, v0

    :cond_c
    :goto_7
    const-string v2, "original_author_id"

    move-object/from16 v0, v44

    move-object/from16 v1, v33

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "original_author_name"

    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "original_author_avatar_url"

    move-object/from16 v0, v44

    move-object/from16 v1, v32

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "album_id"

    const/4 v3, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "total_comment_count"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->totalCommentCount:Ljava/lang/Integer;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "public"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "spam"

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isSpam:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_1d

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->moderationState:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "NEW"

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->moderationState:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    :cond_d
    const/4 v2, 0x1

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "acl_display"

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_1e

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isRestrictedToDomain:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_1e

    sget v2, Lcom/google/android/apps/plus/R$string;->acl_public:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_9
    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "created"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "modified"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "is_edited"

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v2, v21, v9

    if-eqz v2, :cond_24

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "can_comment"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->canViewerComment:Ljava/lang/Boolean;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "can_reshare"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->canViewerShare:Ljava/lang/Boolean;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "is_plusoneable"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->isPlusoneAble:Ljava/lang/Boolean;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "has_muted"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->isMute:Ljava/lang/Boolean;

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "has_read"

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    if-nez v2, :cond_25

    const/4 v2, -0x1

    :goto_b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v44

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->explanation:Lcom/google/api/services/plusi/model/Explanation;

    if-eqz v2, :cond_29

    const-string v2, "ITEM_POPULAR"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->explanation:Lcom/google/api/services/plusi/model/Explanation;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Explanation;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    const/16 v29, 0x0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->featureHint:Ljava/util/List;

    if-eqz v2, :cond_e

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->featureHint:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v27, v2, -0x1

    :goto_c
    if-ltz v27, :cond_e

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->featureHint:Ljava/util/List;

    move/from16 v0, v27

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/api/services/plusi/model/FeatureHint;

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/FeatureHint;->type:Ljava/lang/String;

    const-string v3, "WHATS_HOT"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_27

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/FeatureHint;->segments:Lcom/google/api/services/plusi/model/CommonSegments;

    if-eqz v2, :cond_27

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/FeatureHint;->segments:Lcom/google/api/services/plusi/model/CommonSegments;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/CommonSegments;->segments:Ljava/util/List;

    if-eqz v2, :cond_27

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/FeatureHint;->segments:Lcom/google/api/services/plusi/model/CommonSegments;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/CommonSegments;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_27

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/FeatureHint;->segments:Lcom/google/api/services/plusi/model/CommonSegments;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/CommonSegments;->segments:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/api/services/plusi/model/CommonSegment;

    move-object/from16 v0, v39

    iget-object v2, v0, Lcom/google/api/services/plusi/model/CommonSegment;->type:Ljava/lang/String;

    const-string v3, "TEXT"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_27

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonSegment;->text:Ljava/lang/String;

    move-object/from16 v29, v0

    :cond_e
    const-string v2, "whats_hot"

    new-instance v3, Lcom/google/android/apps/plus/content/DbWhatsHot;

    const/4 v4, 0x0

    move-object/from16 v0, v29

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/plus/content/DbWhatsHot;-><init>(ZLjava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbWhatsHot;->serialize(Lcom/google/android/apps/plus/content/DbWhatsHot;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_f
    :goto_d
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v2, :cond_2a

    const-string v2, "plus_one_data"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_e
    iget-object v0, v8, Lcom/google/api/services/plusi/model/Update;->squareUpdate:Lcom/google/api/services/plusi/model/SquareUpdate;

    move-object/from16 v40, v0

    if-eqz v40, :cond_10

    const-string v2, "square_update"

    invoke-static/range {v40 .. v40}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->serialize(Lcom/google/api/services/plusi/model/SquareUpdate;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/32 v2, 0x8000

    or-long v16, v16, v2

    :cond_10
    iget-object v0, v8, Lcom/google/api/services/plusi/model/Update;->squareReshareUpdate:Lcom/google/api/services/plusi/model/SquareUpdate;

    move-object/from16 v38, v0

    if-eqz v38, :cond_2b

    const-string v2, "square_reshare_update"

    invoke-static/range {v38 .. v38}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->serialize(Lcom/google/api/services/plusi/model/SquareUpdate;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_f
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v2, :cond_11

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    if-eqz v2, :cond_11

    const-string v2, "embed_deep_link"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v4, v7}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->serialize(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x800

    or-long v16, v16, v2

    :cond_11
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v2, :cond_2d

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    if-eqz v2, :cond_2d

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    if-eqz v2, :cond_2d

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    if-eqz v2, :cond_2d

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/AppInvite;->about:Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-object/from16 v24, v0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/AppInvite;->attribution:Lcom/google/api/services/plusi/model/Attribution;

    if-nez v2, :cond_2c

    const/16 v37, 0x0

    :goto_10
    const-string v2, "embed_appinvite"

    iget-object v3, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v4, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DeepLink;->renderedLabel:Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->serialize(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x2000

    or-long v16, v16, v2

    :goto_11
    if-eqz v24, :cond_14

    const/16 v19, 0x0

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    if-eqz v2, :cond_2e

    new-instance v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/WebPage;)V

    :cond_12
    :goto_12
    if-eqz v19, :cond_14

    const-string v2, "embed_media"

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->serialize(Lcom/google/android/apps/plus/content/DbEmbedMedia;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v2

    if-eqz v2, :cond_40

    const-wide/16 v2, 0x80

    or-long v16, v16, v2

    :cond_13
    :goto_13
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v2

    if-nez v2, :cond_14

    const-wide/16 v2, 0x4

    or-long v16, v16, v2

    :cond_14
    const-string v2, "content_flags"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v15, v8, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    if-eqz v15, :cond_41

    new-instance v18, Lcom/google/android/apps/plus/content/DbStreamComments;

    const/4 v2, 0x3

    move-object/from16 v0, v18

    invoke-direct {v0, v15, v2}, Lcom/google/android/apps/plus/content/DbStreamComments;-><init>(Ljava/util/List;I)V

    const-string v2, "comment"

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/content/DbStreamComments;->serialize(Lcom/google/android/apps/plus/content/DbStreamComments;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_14
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v0, v8, Lcom/google/api/services/plusi/model/Update;->canonicalPermalinkUrl:Ljava/lang/String;

    move-object/from16 v35, v0

    const-string v2, "permalink"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    if-eqz v41, :cond_42

    const-string v2, "activities"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v4, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v44

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_15
    if-nez v14, :cond_2

    if-eqz v15, :cond_44

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_44

    if-nez v41, :cond_43

    const/4 v2, 0x1

    :goto_16
    move-object/from16 v0, p2

    invoke-static {v0, v6, v15, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->updateCommentsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V

    goto/16 :goto_1

    :cond_16
    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_17
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_19
    const-string v2, "loc"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_1a
    const-string v2, "annotation_plaintext"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_1b
    const-string v2, "title_plaintext"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_1c
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    if-eqz v2, :cond_c

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v33, v0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    move-object/from16 v34, v0

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Person;->photoUrl:Ljava/lang/String;

    move-object/from16 v32, v0

    goto/16 :goto_7

    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_1e
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isPrivateToChatContacts:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_1f

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isPrivateToLatitudeFriends:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_20

    :cond_1f
    sget v2, Lcom/google/android/apps/plus/R$string;->acl_private_contacts:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_20
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isSharedWithExtendedNetwork:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_21

    sget v2, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_21
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isRestrictedToDomain:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_23

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_23

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->restrictedDomainData:Lcom/google/api/services/plusi/model/DomainData;

    if-eqz v2, :cond_22

    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->restrictedDomainData:Lcom/google/api/services/plusi/model/DomainData;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DomainData;->name:Ljava/lang/String;

    goto/16 :goto_9

    :cond_22
    const-string v2, ""

    goto/16 :goto_9

    :cond_23
    sget v2, Lcom/google/android/apps/plus/R$string;->acl_limited:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_25
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_26

    const/4 v2, 0x1

    goto/16 :goto_b

    :cond_26
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_27
    add-int/lit8 v27, v27, -0x1

    goto/16 :goto_c

    :cond_28
    const-string v2, "whats_hot"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_29
    if-eqz v14, :cond_f

    const-string v2, "whats_hot"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_2a
    const-string v2, "plus_one_data"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_2b
    const-string v2, "square_reshare_update"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_2c
    iget-object v2, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/AppInvite;->attribution:Lcom/google/api/services/plusi/model/Attribution;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/Attribution;->productName:Ljava/lang/String;

    move-object/from16 v37, v0

    goto/16 :goto_10

    :cond_2d
    iget-object v0, v8, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-object/from16 v24, v0

    goto/16 :goto_11

    :cond_2e
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->emotishare:Lcom/google/api/services/plusi/model/Emotishare;

    if-eqz v2, :cond_2f

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_2f

    new-instance v23, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->emotishare:Lcom/google/api/services/plusi/model/Emotishare;

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;-><init>(Lcom/google/api/services/plusi/model/Emotishare;)V

    const-string v2, "embed_emotishare"

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->serialize(Lcom/google/android/apps/plus/content/DbEmbedEmotishare;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/32 v2, 0x40000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_2f
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    if-eqz v2, :cond_31

    new-instance v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/PlusPhoto;)V

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_30

    const/16 v19, 0x0

    goto/16 :goto_12

    :cond_30
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertMediaPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/PlusPhoto;)V

    goto/16 :goto_12

    :cond_31
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhotoAlbum:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    if-eqz v2, :cond_33

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhotoAlbum:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPostsData;->getAlbumOrMedia(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)Lcom/google/android/apps/plus/content/DbSerializer;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_32

    move-object/from16 v19, v20

    check-cast v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    const-wide/16 v2, 0x4000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_32
    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v2, :cond_12

    const-string v2, "embed_photo_album"

    check-cast v20, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->serialize(Lcom/google/android/apps/plus/content/DbEmbedAlbum;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhotoAlbum:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertMediaAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V

    const-wide/16 v2, 0x40

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_33
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    if-eqz v2, :cond_34

    new-instance v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/VideoObject;)V

    goto/16 :goto_12

    :cond_34
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->checkin:Lcom/google/api/services/plusi/model/Checkin;

    if-eqz v2, :cond_35

    const-string v2, "loc"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->checkin:Lcom/google/api/services/plusi/model/Checkin;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/api/services/plusi/model/Checkin;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x18

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_35
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->placeReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v2, :cond_36

    const-string v2, "embed_place_review"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceReviewJson;

    move-result-object v3

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->placeReview:Lcom/google/api/services/plusi/model/PlaceReview;

    invoke-virtual {v3, v4}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x1000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_36
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhotosAddedToCollection:Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;

    if-eqz v2, :cond_38

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhotosAddedToCollection:Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_12

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->associatedMediaDisplay:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    if-eqz v2, :cond_12

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsEventData;->copyRsvpFromSummary(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v12, p1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction$32ff2bf1(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;ILcom/google/android/apps/plus/content/EsAccount;)Z

    const-string v2, "event_id"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->associatedMediaDisplay:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPostsData;->getAlbumOrMedia(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)Lcom/google/android/apps/plus/content/DbSerializer;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_37

    move-object/from16 v19, v20

    check-cast v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    const-wide/16 v2, 0x4000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_37
    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v2, :cond_12

    const-string v2, "embed_photo_album"

    check-cast v20, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->serialize(Lcom/google/android/apps/plus/content/DbEmbedAlbum;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusPhotosAddedToCollection;->associatedMediaDisplay:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertMediaAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V

    const-wide/16 v2, 0x40

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_38
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_39

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsEventData;->copyRsvpFromSummary(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v12, p1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction$32ff2bf1(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;ILcom/google/android/apps/plus/content/EsAccount;)Z

    const-string v2, "event_id"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x100

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_39
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    if-eqz v2, :cond_3a

    const-string v2, "embed_skyjam"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->serialize(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x400

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_3a
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    if-eqz v2, :cond_3b

    const-string v2, "embed_skyjam"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->serialize(Lcom/google/api/services/plusi/model/PlayMusicTrack;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x400

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_3b
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->hangoutConsumer:Lcom/google/api/services/plusi/model/HangoutConsumer;

    if-eqz v2, :cond_3c

    const-string v2, "embed_hangout"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->hangoutConsumer:Lcom/google/api/services/plusi/model/HangoutConsumer;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->serialize(Lcom/google/api/services/plusi/model/HangoutConsumer;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v2, 0x200

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_3c
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->square:Lcom/google/api/services/plusi/model/EmbedsSquare;

    if-eqz v2, :cond_3d

    const-string v2, "embed_square"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->square:Lcom/google/api/services/plusi/model/EmbedsSquare;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->serialize(Lcom/google/api/services/plusi/model/EmbedsSquare;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/32 v2, 0x10000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_3d
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->squareInvite:Lcom/google/api/services/plusi/model/SquareInvite;

    if-eqz v2, :cond_3e

    const-string v2, "embed_square"

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->squareInvite:Lcom/google/api/services/plusi/model/SquareInvite;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->serialize(Lcom/google/api/services/plusi/model/SquareInvite;)[B

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/32 v2, 0x20000

    or-long v16, v16, v2

    goto/16 :goto_12

    :cond_3e
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    if-eqz v2, :cond_3f

    new-instance v19, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/Thing;)V

    goto/16 :goto_12

    :cond_3f
    const-string v2, "EsPostsData"

    const-string v3, "Found an embed we don\'t understand without a THING!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_12

    :cond_40
    const-wide/16 v2, 0x20

    or-long v16, v16, v2

    goto/16 :goto_13

    :cond_41
    const-string v2, "comment"

    move-object/from16 v0, v44

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_14

    :cond_42
    const-string v2, "activities"

    const-string v3, "activity_id"

    const/4 v4, 0x5

    move-object/from16 v0, p2

    move-object/from16 v1, v44

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_15

    :cond_43
    const/4 v2, 0x0

    goto/16 :goto_16

    :cond_44
    if-eqz v41, :cond_2

    const-string v2, "activity_comments"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v4, v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private static insertActivitiesIntoStreamInTransaction$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;)V"
        }
    .end annotation

    const/4 v7, 0x4

    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "insertActivitiesAndOverwrite in stream: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v7}, Landroid/content/ContentValues;-><init>(I)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPostsData;->getMostRecentSortIndex(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    sub-int v1, v4, v5

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Update;

    const-string v4, "stream_key"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "activity_id"

    iget-object v5, v2, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sort_index"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "last_activity"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "activity_streams"

    const-string v5, "activity_id"

    invoke-virtual {p0, v4, v5, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static insertComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/api/services/plusi/model/Comment;

    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>> insertComment: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p3, p2, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v2, "activity_comments"

    const-string v3, "activity_id"

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const/4 v2, 0x1

    invoke-static {v0, p2, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    iget-object v2, p3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static insertLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/DbLocation;Ljava/util/ArrayList;)V
    .locals 15
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/LocationQuery;
    .param p3    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p4    # Lcom/google/android/apps/plus/content/DbLocation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/api/LocationQuery;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v6

    const-string v11, "location_queries"

    const-string v12, "key=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v6, v13, v14

    invoke-virtual {v1, v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p4, :cond_1

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_3

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v11, "key"

    invoke-virtual {v10, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "location_queries"

    const-string v12, "key"

    invoke-virtual {v1, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-ltz v11, :cond_3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v11, "qrid"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v11, "name"

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "location"

    invoke-static {v5}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v11, "locations"

    const-string v12, "qrid"

    invoke-virtual {v1, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/content/EsProvider;->buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v9, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :catchall_0
    move-exception v11

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v11
.end method

.method public static insertMultiStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/Collection;Ljava/util/List;Ljava/lang/String;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;Z)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v2, v7, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesIntoStreamInTransaction$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {p1, v7}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private static insertPromosInTransaction$40100863(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Promo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x3

    const/4 v8, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Promo;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPostsData;->extractPromoActivityId(Lcom/google/api/services/plusi/model/Promo;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "EsPostsData"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "EsPostsData"

    const-string v6, ">>>>> could not determine activity id"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v5, "EsPostsData"

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "EsPostsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, ">>>>> promo internal activity id: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v5, "activity_id"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "author_id"

    const-string v6, "~promo"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "total_comment_count"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "public"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "spam"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "can_comment"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "can_reshare"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "is_plusoneable"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "has_muted"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "has_read"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "created"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "modified"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "data_state"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v6, "FIND_FRIENDS"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v2, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->findFriendsPromoMessage:Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;-><init>(Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;)V

    const-string v5, "promo"

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->serialize(Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :goto_1
    const-string v5, "activities"

    const-string v6, "activity_id"

    const/4 v7, 0x5

    invoke-virtual {p1, v5, v6, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    goto/16 :goto_0

    :cond_5
    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v6, "SUGGESTED_FRIENDS"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v2, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoMessage:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;

    iget-object v6, v3, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoData:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;

    const/4 v7, 0x2

    invoke-direct {v2, p0, v5, v6, v7}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;-><init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;I)V

    const-string v5, "promo"

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->serialize(Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_1

    :cond_6
    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    const-string v6, "SUGGESTED_CELEBRITIES"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    new-instance v2, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    iget-object v5, v3, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoMessage:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;

    iget-object v6, v3, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoData:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;

    invoke-direct {v2, p0, v5, v6, v9}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;-><init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;I)V

    const-string v5, "promo"

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->serialize(Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_1

    :cond_7
    const-string v5, "promo"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isActivityPlusOnedByViewer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "plus_one_data"

    aput-object v3, v2, v10

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return v1

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v1, v10

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "EsPostsData"

    const-string v7, ">>>>> markActivitiesAsRead activity ids:"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "EsPostsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\t"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "has_read"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v4, Ljava/lang/StringBuffer;

    const/16 v6, 0x100

    invoke-direct {v4, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v6, "activity_id IN("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v2, 0x1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_2
    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    :cond_1
    const/16 v6, 0x2c

    :try_start_1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_2
    const/16 v6, 0x29

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v6, "activities"

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void
.end method

.method public static markDbWhatsHotViewed(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/16 v17, 0x0

    const/4 v4, 0x1

    const-string v5, "activities"

    sget-object v6, Lcom/google/android/apps/plus/content/EsPostsData$ActivityWhatsHotQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "activity_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v3 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    if-nez v14, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/content/DbWhatsHot;->deserialize([B)Lcom/google/android/apps/plus/content/DbWhatsHot;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v17

    :cond_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    if-eqz v17, :cond_0

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/content/DbWhatsHot;->setViewed(Z)V

    const/4 v15, 0x0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    new-instance v16, Landroid/content/ContentValues;

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v4, "whats_hot"

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbWhatsHot;->serialize(Lcom/google/android/apps/plus/content/DbWhatsHot;)[B

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v4, "activities"

    const-string v5, "activity_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v15, 0x1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_1
    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v3, v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v4

    :catch_0
    move-exception v4

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    :catchall_1
    move-exception v4

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public static muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v8, "EsPostsData"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "EsPostsData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ">>>>> muteActivity id: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    const/4 v8, 0x1

    invoke-direct {v5, v8}, Landroid/content/ContentValues;-><init>(I)V

    const-string v8, "has_muted"

    if-eqz p3, :cond_1

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "activities"

    const-string v7, "activity_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :cond_1
    move v6, v7

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-static {p0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> plusOneComment activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", commentId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v1, p4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->updatePlusOnedByMe(Z)V

    invoke-static {v0, p3, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static plusOnePost(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x1

    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsPostsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ">>>>> plusOnePost activity id: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {v2, p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v4

    :cond_1
    if-nez v4, :cond_2

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_2
    invoke-virtual {v4, p3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->updatePlusOnedByMe(Z)V

    invoke-static {p0, v2, p2, v4, v5}, Lcom/google/android/apps/plus/content/EsEventData;->replaceEventPlusOneData(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->replacePostPlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V

    goto :goto_0
.end method

.method private static replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .locals 7
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbPlusOneData;

    const/4 v5, 0x1

    :try_start_0
    invoke-static {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "plus_one_data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "activity_comments"

    const-string v4, "comment_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not serialize DbPlusOneData "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static replacePostPlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .param p5    # Z

    const/4 v5, 0x1

    :try_start_0
    invoke-static {p4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "plus_one_data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "activities"

    const-string v4, "activity_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {p2, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz p5, :cond_0

    invoke-static {p2, p0, p1, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not serialize DbPlusOneData "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static retrievePromoType(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "~promo:~ff:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "~promo:~sf:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "~promo:~sc:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static retrieveStreamItemType(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "~promo:~"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setSyncEnabled(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    return-void
.end method

.method public static syncActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v15, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncLock:Ljava/lang/Object;

    monitor-enter v15

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    if-nez v1, :cond_1

    :cond_0
    monitor-exit v15

    :goto_0
    return-void

    :cond_1
    const-string v1, "Activities:Sync"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static/range {p0 .. p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/apps/plus/widget/EsWidgetProvider;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v5}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v5

    array-length v1, v5

    if-lez v1, :cond_3

    array-length v6, v5

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_3

    aget v7, v5, v1

    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v14, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const-string v1, "v.whatshot"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v3, 0x1

    const/4 v4, 0x0

    :goto_3
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/16 v9, 0x14

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v10, p3

    move-object/from16 v11, p2

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v15

    throw v1

    :cond_4
    :try_start_1
    const-string v1, "v.nearby"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v3, 0x2

    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    const-string v1, "v.all.circles"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v4, 0x0

    :goto_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    move-object v4, v13

    goto :goto_4

    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public static updateActivityIsSpam(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x0

    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> updateActivityIsSpam id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSpam: false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "spam"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "activities"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method public static updateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/api/services/plusi/model/Comment;

    const-string v3, "EsPostsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>> editComment: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for activity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p3, p2, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v3, 0x100

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v3, "comment_id IN("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v3, "activity_comments"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v5, p3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public static updateCommentPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> updateCommentPlusOneId activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", comment id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->setId(Ljava/lang/String;)V

    invoke-static {v0, p3, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    goto :goto_0
.end method

.method private static updateCommentsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Comment;",
            ">;Z)V"
        }
    .end annotation

    const/4 v7, 0x3

    if-nez p3, :cond_0

    const-string v3, "activity_comments"

    const-string v4, "activity_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    return-void

    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Comment;

    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    >>>>> insertComments comment id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", author id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isSpam:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "EsPostsData"

    const-string v4, "    >>>>> skipping! isSpam=true"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    invoke-static {v0, p1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    const-string v3, "activity_comments"

    const-string v4, "activity_id"

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0
.end method

.method public static updateDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "promo"

    invoke-static {p3}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->serialize(Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "activities"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static updatePostPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    const-string v0, "EsPostsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsPostsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ">>>>> update post plusone id: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {v2, p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v4

    :cond_1
    if-nez v4, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v4, p3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->setId(Ljava/lang/String;)V

    invoke-static {p0, v2, p2, v4, v5}, Lcom/google/android/apps/plus/content/EsEventData;->replaceEventPlusOneData(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->replacePostPlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V

    goto :goto_0
.end method

.method public static updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 15
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p3, :cond_0

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct/range {p3 .. p3}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v7

    invoke-static/range {p5 .. p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 p6, 0x0

    :cond_1
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateStreamActivities: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " received activities: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ,new token: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ,old token: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz p7, :cond_3

    move-object/from16 v0, p7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    :cond_3
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v11, 0x0

    const-string v1, "activity_streams"

    const-string v2, "stream_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v3, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    new-instance v14, Landroid/content/ContentValues;

    const/4 v1, 0x5

    invoke-direct {v14, v1}, Landroid/content/ContentValues;-><init>(I)V

    add-int v1, v11, v7

    add-int/lit8 v10, v1, -0x1

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/Update;

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    const/4 v8, -0x1

    :try_start_1
    const-string v1, "SELECT sort_index FROM activity_streams WHERE stream_key=? AND activity_id=?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    const/4 v4, 0x1

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-static {v3, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v1

    long-to-int v8, v1

    :goto_2
    const/4 v1, -0x1

    if-eq v8, v1, :cond_4

    if-eq v8, v11, :cond_4

    :try_start_2
    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Conflict replace incoming! Update id = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v12, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " went from "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v1, "stream_key"

    move-object/from16 v0, p2

    invoke-virtual {v14, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "activity_id"

    iget-object v2, v12, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v14, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sort_index"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "activity_streams"

    const-string v2, "activity_id"

    const/4 v4, 0x5

    invoke-virtual {v3, v1, v2, v14, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    const-string v1, "SELECT count(*) FROM activity_streams WHERE stream_key=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    invoke-static {v3, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v11, v1

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    const-string v1, "token"

    move-object/from16 v0, p6

    invoke-virtual {v14, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "activity_streams"

    const-string v2, "stream_key=? AND sort_index=0"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v3, v1, v14, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    const-string v1, "last_activity"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "activity_streams"

    const-string v2, "stream_key=? AND sort_index=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v1, v14, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_7
    if-lez v7, :cond_9

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateStreamActivities: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " inserting activities:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;Z)V

    :cond_9
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v13, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :catch_0
    move-exception v1

    goto/16 :goto_2
.end method

.method public static updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 17
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/StreamItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p3, :cond_0

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct/range {p3 .. p3}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v10

    invoke-static/range {p5 .. p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 p6, 0x0

    :cond_1
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "updateStreamItems: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " received items: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " ,new token: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " ,old token: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz p7, :cond_3

    move-object/from16 v0, p7

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    :cond_3
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v13, 0x0

    const-string v1, "activity_streams"

    const-string v2, "stream_key=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v3, v1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    new-instance v15, Landroid/content/ContentValues;

    const/4 v1, 0x5

    invoke-direct {v15, v1}, Landroid/content/ContentValues;-><init>(I)V

    add-int v1, v13, v10

    add-int/lit8 v11, v1, -0x1

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/StreamItem;

    if-eqz v9, :cond_8

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->type:Ljava/lang/String;

    const-string v2, "UPDATE"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->updateItem:Lcom/google/api/services/plusi/model/Update;

    if-eqz v1, :cond_6

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->updateItem:Lcom/google/api/services/plusi/model/Update;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    :goto_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    const-string v1, "stream_key"

    move-object/from16 v0, p2

    invoke-virtual {v15, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "activity_id"

    invoke-virtual {v15, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sort_index"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "activity_streams"

    const-string v2, "activity_id"

    const/4 v5, 0x5

    invoke-virtual {v3, v1, v2, v15, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_5
    const-string v1, "SELECT count(*) FROM activity_streams WHERE stream_key=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v2, v5

    invoke-static {v3, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v13, v1

    goto :goto_0

    :cond_6
    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->type:Ljava/lang/String;

    const-string v2, "PROMO"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->promo:Lcom/google/api/services/plusi/model/Promo;

    if-eqz v1, :cond_7

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->promo:Lcom/google/api/services/plusi/model/Promo;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->extractPromoActivityId(Lcom/google/api/services/plusi/model/Promo;)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    :cond_7
    const-string v1, "EsPostsData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "EsPostsData"

    const-string v2, "unknown stream type"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v7, 0x0

    goto :goto_2

    :cond_9
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    const-string v1, "token"

    move-object/from16 v0, p6

    invoke-virtual {v15, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "activity_streams"

    const-string v2, "stream_key=? AND sort_index=0"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v3, v1, v15, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    const-string v1, "last_activity"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "activity_streams"

    const-string v2, "stream_key=? AND sort_index=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v5, v6

    invoke-virtual {v3, v1, v15, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_a
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_b
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/StreamItem;

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->type:Ljava/lang/String;

    const-string v2, "PROMO"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->promo:Lcom/google/api/services/plusi/model/Promo;

    if-eqz v1, :cond_c

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->promo:Lcom/google/api/services/plusi/model/Promo;

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :cond_c
    :try_start_1
    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->type:Ljava/lang/String;

    const-string v2, "UPDATE"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->updateItem:Lcom/google/api/services/plusi/model/Update;

    if-eqz v1, :cond_d

    iget-object v1, v9, Lcom/google/api/services/plusi/model/StreamItem;->updateItem:Lcom/google/api/services/plusi/model/Update;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_d
    const-string v1, "EsPostsData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "EsPostsData"

    const-string v2, "unknown stream type"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_e
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_10

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "updateStreamItems: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " inserting promos:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    move-object/from16 v0, p0

    invoke-static {v0, v3, v12}, Lcom/google/android/apps/plus/content/EsPostsData;->insertPromosInTransaction$40100863(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    :cond_10
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_12

    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "updateStreamItems: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " inserting updates:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v5, p4

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;Z)V

    :cond_12
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v14, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 18
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    const/16 v16, 0x0

    const/4 v2, 0x1

    new-array v15, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v15, v2

    const-string v3, "comments_view"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPostsData$UpdateCommentDataQuery;->PROJECTION:[Ljava/lang/String;

    const-string v5, "activity_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "created DESC"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    const-string v3, "activities"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "total_comment_count"

    aput-object v5, v4, v2

    const-string v5, "activity_id=?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object v6, v15

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    :cond_0
    const-string v2, "total_comment_count"

    add-int v3, v16, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v14, Lcom/google/android/apps/plus/content/DbStreamComments;

    const/4 v2, 0x3

    invoke-direct {v14, v13, v2}, Lcom/google/android/apps/plus/content/DbStreamComments;-><init>(Landroid/database/Cursor;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v14}, Lcom/google/android/apps/plus/content/DbStreamComments;->serialize(Lcom/google/android/apps/plus/content/DbStreamComments;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    :goto_0
    :try_start_2
    const-string v2, "comment"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "activities"

    const-string v3, "activity_id=?"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    return-void

    :catch_0
    move-exception v2

    const/4 v12, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2
.end method
