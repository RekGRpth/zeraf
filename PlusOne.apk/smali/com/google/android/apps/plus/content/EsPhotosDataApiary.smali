.class public final Lcom/google/android/apps/plus/content/EsPhotosDataApiary;
.super Lcom/google/android/apps/plus/content/EsPhotosData;
.source "EsPhotosDataApiary.java"


# static fields
.field private static final PHOTO_COMMENT_ID_COLUMN:[Ljava/lang/String;

.field private static final PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

.field private static sPhotosFromPostsAlbumName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "plusone_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PHOTO_COMMENT_ID_COLUMN:[Ljava/lang/String;

    return-void
.end method

.method private static buildDataPhotoFromPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;Lcom/google/api/services/plusi/model/DataAlbum;)Lcom/google/api/services/plusi/model/DataPhoto;
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/PlusPhoto;
    .param p1    # Lcom/google/api/services/plusi/model/DataAlbum;

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    if-nez v4, :cond_2

    :cond_1
    const-string v4, "EsPhotosData"

    const-string v5, "Invalid photo embed; cannot insert into the database"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_2
    new-instance v2, Lcom/google/api/services/plusi/model/DataUser;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DataUser;-><init>()V

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    iput-object v4, v2, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    if-nez p1, :cond_3

    new-instance p1, Lcom/google/api/services/plusi/model/DataAlbum;

    invoke-direct {p1}, Lcom/google/api/services/plusi/model/DataAlbum;-><init>()V

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    iput-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    const-string v4, "ALL_OTHERS"

    iput-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    :cond_3
    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    new-instance v0, Lcom/google/api/services/plusi/model/DataImage;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataImage;-><init>()V

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->heightPx:Ljava/lang/Integer;

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->widthPx:Ljava/lang/Integer;

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    if-eqz v4, :cond_5

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    :cond_4
    :goto_1
    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getCanonicalUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    new-instance v3, Lcom/google/api/services/plusi/model/DataPhoto;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DataPhoto;-><init>()V

    iput-object p1, v3, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iput-object v2, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->description:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->contentUrl:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->contentUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    goto :goto_1

    :cond_6
    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->imageUrl:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v4, v1, Lcom/google/api/services/plusi/model/ImageObject;->imageUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    goto :goto_1
.end method

.method public static deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v10, 0x4

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v8, 0x1

    new-array v0, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v0, v8

    :try_start_0
    const-string v8, "SELECT photo_id FROM photo_comment WHERE comment_id = ?"

    invoke-static {v1, v8, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const-string v8, "photo_comment"

    const-string v9, "comment_id = ?"

    invoke-virtual {v1, v8, v9, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    neg-int v9, v5

    invoke-static {v1, v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateCommentCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v8, "EsPhotosData"

    invoke-static {v8, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "EsPhotosData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[DELETE_PHOTO_COMMENT], duration: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v8, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v2, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v8

    const-string v8, "EsPhotosData"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "EsPhotosData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "WARNING: could not find photo for the comment: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v8

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v9, "EsPhotosData"

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "EsPhotosData"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[DELETE_PHOTO_COMMENT], duration: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    throw v8
.end method

.method private static deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const-string v0, "photo_plusone"

    const-string v1, "photo_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static deletePhotos$43585934(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 23
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    if-nez v19, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    :try_start_0
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v15, v0, [Ljava/lang/String;

    const-string v19, "photo_id IN("

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    add-int/lit8 v9, v19, -0x1

    :goto_1
    if-ltz v9, :cond_6

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    const-string v19, "EsPhotosData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_2

    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, ">> deletePhoto photo id: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v11, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    aput-object v14, v11, v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v19, "SELECT album_id FROM photo WHERE photo_id = ?"

    move-object/from16 v0, v19

    invoke-static {v6, v0, v11}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    if-nez v12, :cond_3

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    :cond_3
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :goto_2
    :try_start_2
    const-string v19, "?,"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aput-object v14, v15, v9

    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    :catch_0
    move-exception v8

    const-string v19, "EsPhotosData"

    const/16 v20, 0x5

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_4

    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Album not found for photo: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v19

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v20, "EsPhotosData"

    const/16 v21, 0x4

    invoke-static/range {v20 .. v21}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_5

    const-string v20, "EsPhotosData"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "[DELETE_PHOTOS], duration: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v19

    :cond_6
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v19, ")"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v2, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    aput-object v4, v2, v19

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    :try_start_4
    const-string v19, "SELECT photo_count FROM album WHERE photo_count NOT NULL AND album_id = ?"

    move-object/from16 v0, v19

    invoke-static {v6, v0, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v19

    int-to-long v0, v7

    move-wide/from16 v21, v0

    add-long v12, v19, v21

    const-string v19, "photo_count"

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v19, "album"

    const-string v20, "album_id = ?"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v5, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v19

    :try_start_5
    const-string v19, "EsPhotosData"

    const/16 v20, 0x4

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_7

    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Photo count not found; album id: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_8
    const-string v19, "photo"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v19, "EsPhotosData"

    const/16 v20, 0x4

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_9

    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "[DELETE_PHOTOS], duration: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    sget-object v20, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0
.end method

.method private static deletePhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V
    .locals 7
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    if-lez v4, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    const-string v4, "photo_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    const-string v4, "?,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "photo"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {p0, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private static getAlbumAudience(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v1, "AUDIENCE_OWNER_ONLY"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const-string v1, "AUDIENCE_PUBLIC"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "AUDIENCE_LIMITED"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "AUDIENCE_ALL_PERSONAL_CIRCLES"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "AUDIENCE_DOMAIN_PUBLIC"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "AUDIENCE_EXTENDED_CIRCLES"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getAlbumAudienceFromUpdate(Lcom/google/api/services/plusi/model/Update;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/Update;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->isRestrictedToDomain:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUDIENCE_PUBLIC"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "AUDIENCE_DOMAIN_PUBLIC"

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->isPrivateToChatContacts:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->isPrivateToLatitudeFriends:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "AUDIENCE_ALL_PERSONAL_CIRCLES"

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/api/services/plusi/model/Update;->isSharedWithExtendedNetwork:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "AUDIENCE_EXTENDED_CIRCLES"

    goto :goto_0

    :cond_4
    const-string v0, "AUDIENCE_LIMITED"

    goto :goto_0
.end method

.method private static getAlbumContentValues(Lcom/google/api/services/plusi/model/DataAlbum;)Landroid/content/ContentValues;
    .locals 7
    .param p0    # Lcom/google/api/services/plusi/model/DataAlbum;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "album_id"

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "owner_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->audience:Ljava/lang/String;

    if-eqz v3, :cond_6

    const-string v3, "audience"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->audience:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumAudience(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "title"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->timestampSeconds:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->timestampSeconds:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    const-wide v5, 0x408f400000000000L

    mul-double/2addr v3, v5

    double-to-long v0, v3

    const-string v3, "timestamp"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    const-string v3, "album_type"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    if-eqz v3, :cond_2

    const-string v3, "entity_version"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    const-string v3, "ALL_OTHERS"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "UPDATES_ALBUMS"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "stream_id"

    const-string v4, "posts"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sort_order"

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v3, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "title"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    const-string v3, "photo_count"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    if-eqz v3, :cond_5

    const-string v3, "cover_photo_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_5
    return-object v2

    :cond_6
    const-string v3, "audience"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "BUNCH_ALBUMS"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "stream_id"

    const-string v4, "messenger"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sort_order"

    const/16 v4, 0x32

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_8
    const-string v3, "PROFILE_PHOTOS"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "stream_id"

    const-string v4, "profile"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sort_order"

    const/16 v4, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_9
    const-string v3, "stream_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    const-string v3, "sort_order"

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "stream_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    if-eqz v3, :cond_4

    const-string v3, "photo_count"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2
.end method

.method private static getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/DataAlbum;

    :try_start_0
    iget-object v0, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    iget-object v0, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/DataAlbum;
    .param p1    # I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ALBUM [id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", owner: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    if-eqz v3, :cond_1

    const-string v3, ",\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "       type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string v3, ",\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "       title: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v3, :cond_3

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    add-int/lit8 v5, p1, 0x2

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCoverPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 7
    .param p0    # Lcom/google/api/services/plusi/model/DataComment;
    .param p1    # Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "photo_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "comment_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "author_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "content"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->text:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->timestamp:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->timestamp:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    const-wide v5, 0x408f400000000000L

    mul-double/2addr v3, v5

    double-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v3, "create_time"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    const-string v3, "update_time"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_2

    const-string v3, "plusone_data"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataComment;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v2

    :cond_2
    const-string v3, "plusone_data"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataPlusOne;
    .locals 9
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v1, "photo_comment"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PHOTO_COMMENT_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "comment_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getCoverPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;
    .locals 13
    .param p0    # Lcom/google/api/services/plusi/model/DataPhoto;
    .param p1    # I

    const-wide/16 v7, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-lez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    const/16 v9, 0x20

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-nez v9, :cond_2

    move-wide v5, v7

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "COVER PHOTO [id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", owner: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v9, :cond_3

    const-string v9, "N/A"

    :goto_2
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    cmp-long v7, v5, v7

    if-eqz v7, :cond_1

    const-string v2, "MMM dd, yyyy h:mmaa"

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v7, ", date: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    :cond_2
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    const-wide v11, 0x408f400000000000L

    mul-double/2addr v9, v11

    double-to-long v5, v9

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto :goto_2
.end method

.method private static getFingerPrint(Lcom/google/api/services/plusi/model/DataPhoto;)[B
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "cs_01_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->FINGERPRINT_STREAM_PREFIX_LENGTH:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->hexToBytes(Ljava/lang/CharSequence;)[B

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getPhotoCommentPlusOneContentValues(Lcom/google/api/services/plusi/model/DataPlusOne;)Landroid/content/ContentValues;
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/DataPlusOne;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-nez p0, :cond_0

    const-string v1, "plusone_data"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "plusone_data"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    invoke-virtual {v2, p0}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;
    .locals 15
    .param p0    # Lcom/google/api/services/plusi/model/DataPhoto;
    .param p1    # I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v10, p0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-nez v11, :cond_1

    const-wide/16 v11, 0x0

    :goto_0
    double-to-long v11, v11

    const-wide/16 v13, 0x3e8

    mul-long v8, v11, v13

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "PHOTO [id: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", owner: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v11, :cond_2

    const-string v11, "N/A"

    :goto_1
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", version: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-wide/16 v11, 0x0

    cmp-long v11, v8, v11

    if-eqz v11, :cond_0

    const-string v2, "MMM dd, yyyy h:mmaa"

    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v11, ", date: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v11, ", \n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "      title: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    if-nez v11, :cond_3

    const-string v11, "N/A"

    :goto_2
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "video? "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v10, :cond_4

    const/4 v11, 0x1

    :goto_3
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", comments: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    if-nez v11, :cond_5

    const/4 v11, 0x0

    :goto_4
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "+1s: "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_6

    iget-object v11, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    :goto_5
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "by me: "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_7

    iget-object v11, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    :goto_6
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v11, ", \n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "      stream: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_1
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    goto/16 :goto_0

    :cond_2
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    goto/16 :goto_2

    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_3

    :cond_5
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    goto/16 :goto_4

    :cond_6
    const-string v11, "0"

    goto :goto_5

    :cond_7
    const-string v11, "false"

    goto :goto_6

    :cond_8
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-eqz v11, :cond_9

    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    const/4 v13, 0x2

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/DataShape;

    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x2

    invoke-static {v6, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getShapeOutput(Lcom/google/api/services/plusi/model/DataShape;I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_a
    const-string v11, "]"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method private static getShapeContentValues(Lcom/google/api/services/plusi/model/DataShape;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/DataShape;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataShape;->relativeBounds:Lcom/google/api/services/plusi/model/DataRectRelative;

    if-eqz v3, :cond_0

    const-string v3, "bounds"

    invoke-static {}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->getInstance()Lcom/google/api/services/plusi/model/DataRectRelativeJson;

    move-result-object v4

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataShape;->relativeBounds:Lcom/google/api/services/plusi/model/DataRectRelative;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v3, "creator_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataShape;->creator:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "photo_id"

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "shape_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "status"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataShape;->status:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v3, :cond_2

    const-string v3, "subject_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataShape;->suggestion:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataShape;->suggestion:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataShape;->suggestion:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataUser;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "subject_id"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getShapeOutput(Lcom/google/api/services/plusi/model/DataShape;I)Ljava/lang/String;
    .locals 11
    .param p0    # Lcom/google/api/services/plusi/model/DataShape;
    .param p1    # I

    const/4 v10, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-lez p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataShape;->bounds:Lcom/google/api/services/plusi/model/DataRect32;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SHAPE [("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%d, %d, %d, %d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v1, Lcom/google/api/services/plusi/model/DataRect32;->upperLeft:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataPoint32;->x:Ljava/lang/Long;

    aput-object v8, v7, v10

    const/4 v8, 0x1

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->upperLeft:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->y:Ljava/lang/Long;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->lowerRight:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->x:Ljava/lang/Long;

    aput-object v9, v7, v8

    const/4 v8, 0x3

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->lowerRight:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->y:Ljava/lang/Long;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "), "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "subjectId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v5, :cond_6

    const-string v5, "N/A"

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/api/services/plusi/model/DataShape;->status:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/google/api/services/plusi/model/DataShape;->viewerCanEdit:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/google/api/services/plusi/model/DataShape;->viewerCanApprove:Ljava/lang/Boolean;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_7

    const-string v5, ""

    :goto_2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "COMMENT"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_8

    const-string v5, ""

    :goto_3
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "TAG"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_9

    const-string v5, ""

    :goto_4
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "EDIT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_a

    const-string v5, ""

    :goto_5
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "APPROVE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, ", \n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "       state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_6
    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    const-string v5, "|"

    goto/16 :goto_2

    :cond_8
    const-string v5, "|"

    goto :goto_3

    :cond_9
    const-string v5, "|"

    goto :goto_4

    :cond_a
    const-string v5, "|"

    goto :goto_5
.end method

.method private static insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/lang/Long;Ljava/util/List;)V
    .locals 17
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/DataAlbum;
    .param p2    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "EsPhotosData"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "EsPhotosData"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Album not updated; id: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumContentValues(Lcom/google/api/services/plusi/model/DataAlbum;)Landroid/content/ContentValues;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v9, "EsPhotosData"

    const/4 v10, 0x5

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "EsPhotosData"

    const-string v10, "Could not insert album row"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v9, "UPDATES_ALBUMS"

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    const/4 v3, 0x1

    :goto_1
    if-eqz v4, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    if-eqz v9, :cond_4

    iget-object v12, v9, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    if-nez v12, :cond_6

    :cond_4
    :goto_2
    if-eqz p3, :cond_0

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    const-string v12, "album_cover"

    const-string v13, "album_key=?"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v13, "album_key"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v12, v13, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v10, "url"

    iget-object v11, v9, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v12, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "width"

    iget-object v11, v9, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v12, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "height"

    iget-object v11, v9, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v12, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "size"

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v12, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "album_cover"

    const/4 v10, 0x0

    const/4 v11, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v10, v12, v11}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_2
.end method

.method private static insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 7
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p4    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataAlbum;

    const-string v4, "EsPhotosData"

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsPhotosData"

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-static {p0, v0, v3, p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/lang/Long;Ljava/util/List;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static insertAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 19
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Lcom/google/api/services/plusi/model/DataAlbum;
    .param p5    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v11

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-static {v1, v11, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCurrentAlbumMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v15

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    const-string v3, "EsPhotosData"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    :cond_1
    const-string v2, "album"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "entity_version"

    aput-object v5, v3, v4

    const-string v4, "album_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :goto_0
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/lang/Long;Ljava/util/List;)V

    const-string v6, "photos_in_album"

    const/4 v10, 0x1

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p3

    move-object v5, v15

    move-object v7, v13

    move-object v8, v11

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    invoke-static {v1, v15}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v3, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_ALBUM_PHOTOS], album ID: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", num photos: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p4, :cond_5

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/Uri;

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    move-object v3, v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v2, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v4, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_ALBUM_PHOTOS], album ID: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", num photos: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p4, :cond_6

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", duration: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v3

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    :cond_7
    return-void
.end method

.method public static insertAlbums(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;)V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p3

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumEntityMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v2, v14, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-static {v5, v0, v4, v7, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_0
    if-eqz p5, :cond_1

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v2, v14

    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v5, v0, v4, v7, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_1
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lez v9, :cond_4

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v9}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "album_type == \'ALL_OTHERS\' AND album_id IN("

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v14, "?,"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v14

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v15, "EsPhotosData"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_2

    const-string v15, "EsPhotosData"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "[INSERT_ALBUM_LIST], num albums: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", duration: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    throw v14

    :cond_3
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v14, ")"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "album"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/String;

    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v5, v15, v0, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_5

    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "[INSERT_ALBUM_LIST], num albums: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", duration: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v8, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :cond_6
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->notifyAlbumChange$70674dbd(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    if-gtz v2, :cond_8

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v14

    if-lez v14, :cond_9

    :cond_8
    sget-object v14, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    move-object/from16 v0, p3

    invoke-static {v14, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_9
    return-void
.end method

.method public static insertEventPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/DataPhoto;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    :try_start_0
    const-string v0, "EsPhotosData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    const-string v1, "EsPhotosData"

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object/from16 v6, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "EsPhotosData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EsPhotosData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not insert row for event photo; id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "collection_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "photo_id"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v0, 0x2

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v8, v0

    const/4 v0, 0x1

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v1, v8, v0

    const-string v0, "SELECT count(*) FROM photos_in_event WHERE collection_id=? AND photo_id=?"

    invoke-static {p0, v0, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const-string v0, "photos_in_event"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    const-string v0, "EsPhotosData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EsPhotosData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[INSERT_EVENT_PHOTO], event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    :try_start_1
    const-string v0, "photos_in_event"

    const-string v1, "collection_id=? AND photo_id=?"

    invoke-virtual {p0, v0, v9, v1, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_EVENT_PHOTO], event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v0
.end method

.method public static insertMediaAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-string v0, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsPhotosData"

    const-string v3, ">>>>> media album"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->albumId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    if-nez v0, :cond_5

    :cond_1
    const-string v0, "EsPhotosData"

    const-string v3, "Invalid album embed; cannot insert into the database"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    iget-object v7, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->albumId:Ljava/lang/String;

    const-string v5, "photos_in_album"

    const/4 v9, 0x1

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v6, v2

    move-object v8, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    :cond_3
    const-string v0, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_MEDIA_ALBUM], duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_6

    move-object v1, v2

    goto :goto_0

    :cond_6
    new-instance v0, Lcom/google/api/services/plusi/model/DataUser;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataUser;-><init>()V

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->ownerObfuscatedId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    new-instance v5, Lcom/google/api/services/plusi/model/DataAlbum;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataAlbum;-><init>()V

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->albumId:Ljava/lang/String;

    iput-object v3, v5, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->name:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->photoCount:Ljava/lang/Integer;

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->description:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->description:Ljava/lang/String;

    const-string v0, "ALL_OTHERS"

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusPhoto;

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->buildDataPhotoFromPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;Lcom/google/api/services/plusi/model/DataAlbum;)Lcom/google/api/services/plusi/model/DataPhoto;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method public static insertMediaPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/PlusPhoto;)V
    .locals 13
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhoto;

    const/4 v9, 0x1

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    const-string v0, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsPhotosData"

    const-string v3, ">>>>> media photo"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->buildDataPhotoFromPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;Lcom/google/api/services/plusi/model/DataAlbum;)Lcom/google/api/services/plusi/model/DataPhoto;

    move-result-object v10

    if-eqz v10, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v10, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    const-string v5, "photos_in_album"

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v6, v2

    move-object v8, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    :cond_1
    const-string v0, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_MEDIA_PHOTO], duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private static insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Ljava/lang/Long;
    .locals 6
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v2, "album"

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {p0, v2, v3, p2, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v2, "album"

    const-string v3, "album_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {p0, v2, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v7, "SELECT count(*) FROM photo_comment WHERE comment_id=?"

    new-array v8, v5, [Ljava/lang/String;

    aput-object p1, v8, v6

    invoke-static {p0, v7, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v7, 0x0

    cmp-long v7, v0, v7

    if-nez v7, :cond_2

    const-string v7, "photo_comment"

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {p0, v7, v8, p2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v7, -0x1

    cmp-long v7, v2, v7

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    const-string v7, "photo_comment"

    const-string v8, "comment_id=?"

    new-array v9, v5, [Ljava/lang/String;

    aput-object p1, v9, v6

    invoke-virtual {p0, v7, p2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method private static insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/DataPlusOne;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v7, 0x0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v6, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-nez v6, :cond_1

    move v6, v7

    :goto_0
    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-nez v8, :cond_2

    move v8, v7

    :goto_1
    const-string v10, "plusone_data"

    sget-object v11, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    invoke-virtual {v11, p1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "plusone_by_me"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v6, "plusone_count"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "plusone_id"

    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "photo_id"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "SELECT count(*) FROM photo_plusone WHERE photo_id=?"

    new-array v8, v9, [Ljava/lang/String;

    aput-object p2, v8, v7

    invoke-static {p0, v6, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v10, 0x0

    cmp-long v6, v0, v10

    if-nez v6, :cond_3

    const-string v6, "photo_plusone"

    const/4 v8, 0x0

    const/4 v10, 0x4

    invoke-virtual {p0, v6, v8, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v10, -0x1

    cmp-long v6, v2, v10

    if-eqz v6, :cond_0

    move v7, v9

    :cond_0
    :goto_2
    return v7

    :cond_1
    iget-object v6, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_0

    :cond_2
    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_1

    :cond_3
    const-string v6, "photo_plusone"

    const-string v8, "photo_id=?"

    new-array v10, v9, [Ljava/lang/String;

    aput-object p2, v10, v7

    invoke-virtual {p0, v6, v5, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    move v7, v9

    goto :goto_2
.end method

.method public static insertPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;)V
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Lcom/google/api/services/plusi/model/DataPhoto;
    .param p4    # Ljava/lang/Boolean;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    const-string v3, "EsPhotosData"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "EsPhotosData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not insert row for photo of me; id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v13}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_PHOTO], photo ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v10, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v3, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "EsPhotosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_PHOTO], photo ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    throw v2

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Long;
    .locals 35
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Lcom/google/api/services/plusi/model/DataPhoto;
    .param p2    # Ljava/lang/Boolean;
    .param p3    # Z
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            "Ljava/lang/Boolean;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/plusi/model/DataUser;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Long;"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-nez v2, :cond_2

    const-string v2, "EsPhotosData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot add photo that has no album; photo id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v26, 0x0

    :cond_1
    :goto_0
    return-object v26

    :cond_2
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v12

    if-eqz p4, :cond_6

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    move-object/from16 v24, v2

    :goto_1
    if-nez v24, :cond_3

    :try_start_0
    const-string v2, "SELECT entity_version FROM album WHERE album_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v11, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    if-eqz p4, :cond_3

    move-object/from16 v0, p4

    move-object/from16 v1, v24

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    iget-object v0, v11, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    move-object/from16 v23, v0

    if-eqz v24, :cond_4

    if-eqz v23, :cond_8

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_4
    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumContentValues(Lcom/google/api/services/plusi/model/DataAlbum;)Landroid/content/ContentValues;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-static {v0, v12, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Ljava/lang/Long;

    move-result-object v13

    if-nez v13, :cond_7

    const-string v2, "EsPhotosData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not insert album row; album id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/16 v26, 0x0

    goto :goto_0

    :cond_6
    const/16 v24, 0x0

    goto :goto_1

    :catch_0
    move-exception v2

    const/16 v24, 0x0

    if-eqz p4, :cond_3

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_7
    if-eqz p4, :cond_8

    move-object/from16 v0, p4

    move-object/from16 v1, v23

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "photo_id"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "plus_one_key"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "album_id"

    invoke-virtual {v3, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "url"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getCanonicalUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "title"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "description"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->viewerCanComment:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->viewerCanTag:Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    if-eqz v2, :cond_16

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x2

    :goto_3
    or-int/lit8 v7, v2, 0x0

    if-eqz v4, :cond_17

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_17

    const/4 v2, 0x4

    :goto_4
    or-int v4, v7, v2

    if-eqz v5, :cond_18

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x8

    :goto_5
    or-int/2addr v4, v2

    if-eqz v6, :cond_19

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x10

    :goto_6
    or-int/2addr v2, v4

    const-string v4, "action_state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    if-nez v2, :cond_1a

    const/4 v2, 0x0

    :goto_7
    if-eqz v2, :cond_1b

    const-string v4, "comment_count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "EsPhotosData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v2, v4, :cond_a

    const-string v4, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WARN: comment mismatch; total: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", actual: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    :goto_8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v2, :cond_b

    const-string v2, "owner_id"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-eqz v4, :cond_1c

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_c
    :goto_9
    if-eqz v2, :cond_d

    const-string v4, "timestamp"

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_d
    const-string v2, "entity_version"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getFingerPrint(Lcom/google/api/services/plusi/model/DataPhoto;)[B

    move-result-object v2

    if-eqz v2, :cond_e

    const-string v4, "fingerprint"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_e
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v2, :cond_f

    const-string v2, "video_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/DataVideoJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->isPanorama:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->isPanorama:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "is_panorama"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_10
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadStatus:Ljava/lang/String;

    if-eqz v2, :cond_1d

    const-string v2, "upload_status"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadStatus:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_a
    if-eqz p2, :cond_11

    const-string v2, "downloadable"

    move-object/from16 v0, p2

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_11
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    if-eqz v2, :cond_13

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    if-eqz v2, :cond_12

    const-string v2, "width"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_12
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    if-eqz v2, :cond_13

    const-string v2, "height"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_13
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    if-nez v26, :cond_1e

    const-string v2, "photo"

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_14

    const/16 v26, 0x0

    :cond_14
    :goto_b
    if-eqz p5, :cond_15

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_15
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v2, :cond_1f

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    :goto_c
    const/16 v17, 0x0

    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    const-string v3, "photo_comment"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "comment_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "update_time"

    aput-object v5, v4, v2

    const-string v5, "photo_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    :goto_d
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_20

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_d

    :catchall_0
    move-exception v2

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_1a
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto/16 :goto_7

    :cond_1b
    const-string v2, "comment_count"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_8

    :cond_1c
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    if-eqz v4, :cond_c

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_9

    :cond_1d
    const-string v2, "upload_status"

    const-string v4, "ORIGINAL"

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1e
    const-string v2, "photo"

    const-string v4, "photo_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_14

    const/16 v26, 0x0

    goto/16 :goto_b

    :cond_1f
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_20
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    if-eqz v2, :cond_24

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_21
    :goto_e
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/DataComment;

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v14, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v34

    if-eqz v32, :cond_22

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_22
    if-eqz p5, :cond_23

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->user:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_23
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const/16 v17, 0x1

    goto :goto_e

    :cond_24
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_26

    new-instance v27, Ljava/util/ArrayList;

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "comment_id IN("

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_f
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_25

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    const-string v2, "?,"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_25
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v2, ")"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "photo_comment"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_26
    if-nez v17, :cond_27

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_28

    :cond_27
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    if-eqz p6, :cond_28

    move-object/from16 v0, p6

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_28
    const-string v2, "photo_shape"

    const-string v3, "photo_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    if-eqz v2, :cond_30

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v30

    const/16 v21, 0x0

    :goto_10
    move/from16 v0, v21

    move/from16 v1, v30

    if-ge v0, v1, :cond_2f

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    move/from16 v0, v21

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/api/services/plusi/model/DataShape;

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v33, v0

    if-eqz v33, :cond_29

    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_29

    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    if-eqz v2, :cond_29

    const-string v2, "0"

    move-object/from16 v0, v33

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    :cond_29
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataShape;->suggestion:Ljava/util/List;

    if-eqz v2, :cond_2d

    :cond_2a
    const/16 v20, 0x1

    :goto_11
    if-eqz v20, :cond_2c

    if-eqz p5, :cond_2b

    if-eqz v33, :cond_2b

    move-object/from16 v0, p5

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, p7

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getShapeContentValues(Lcom/google/api/services/plusi/model/DataShape;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    if-eqz v2, :cond_2c

    const-string v3, "SELECT count(*) FROM photo_shape WHERE shape_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v29

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_2e

    const-string v3, "photo_shape"

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2c

    :cond_2c
    :goto_12
    add-int/lit8 v21, v21, 0x1

    goto :goto_10

    :cond_2d
    const/16 v20, 0x0

    goto :goto_11

    :cond_2e
    const-string v3, "photo_shape"

    const-string v4, "shape_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, v29

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2c

    goto :goto_12

    :cond_2f
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v31

    if-eqz p6, :cond_30

    move-object/from16 v0, p6

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_30
    if-eqz v26, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    if-eqz p6, :cond_1

    move-object/from16 v0, p6

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V
    .locals 20
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/Boolean;
    .param p3    # Lcom/google/api/services/plusi/model/DataAlbum;
    .param p5    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p9    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Z)V"
        }
    .end annotation

    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "collection_id"

    move-object/from16 v0, p7

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    invoke-interface {v6, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz p1, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v16

    :goto_0
    const/4 v12, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_d

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataPhoto;

    const-string v2, "EsPhotosData"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    const-string v4, "EsPhotosData"

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p8, :cond_2

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementSubCount()V

    :cond_2
    iget-object v2, v3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    if-eqz p4, :cond_6

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    move-object v14, v2

    :goto_2
    iget-object v2, v3, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    if-eqz v2, :cond_7

    iget-object v13, v3, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    :goto_3
    if-eqz v14, :cond_3

    invoke-virtual {v14, v13}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_3
    const/4 v5, 0x1

    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v8, p6

    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    if-nez v18, :cond_9

    const-string v2, "EsPhotosData"

    const/4 v4, 0x5

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "EsPhotosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not insert row for photo of me; id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_5
    const/16 v16, 0x0

    goto :goto_0

    :cond_6
    const/4 v14, 0x0

    goto :goto_2

    :cond_7
    const/4 v13, 0x0

    goto :goto_3

    :cond_8
    const-string v2, "EsPhotosData"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "EsPhotosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Photo not updated; id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    if-eqz p5, :cond_4

    const/4 v2, 0x2

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v2

    const/4 v2, 0x1

    aput-object p7, v10, v2

    if-eqz v14, :cond_a

    if-eqz p9, :cond_a

    const-string v2, "SELECT sort_index FROM photos_in_album WHERE photo_id=? AND collection_id=?"

    move-object/from16 v0, p0

    invoke-static {v0, v2, v10}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v15, v4

    if-eq v15, v12, :cond_4

    :cond_a
    const-string v2, "photo_id"

    move-object/from16 v0, v17

    invoke-virtual {v11, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz p9, :cond_b

    const-string v2, "sort_index"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_b
    if-eqz v14, :cond_c

    const-string v2, "photo_id=? AND collection_id=?"

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1, v11, v2, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_4

    :cond_c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1, v2, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_4

    :cond_d
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    return-void
.end method

.method public static insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p6    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;Z)V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCurrentStreamMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    :try_start_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v7, "photos_in_stream"

    const/4 v11, 0x0

    move-object/from16 v3, p5

    move-object/from16 v9, p3

    move-object/from16 v10, p2

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    if-nez p6, :cond_3

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_3

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    const-string v3, "collection_id=? AND photo_id IN("

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-string v3, "?,"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    move-object v4, v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v3, "EsPhotosData"

    const/4 v5, 0x4

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v5, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "[INSERT_STREAM_PHOTOS], stream: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", num photos: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p5, :cond_6

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", duration: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    throw v4

    :cond_2
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v3, ")"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "photos_in_stream"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v3, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v4, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_STREAM_PHOTOS], stream: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", num photos: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p5, :cond_5

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", duration: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v13, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_7
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p3

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v13, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method public static insertUserPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 17
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-nez p3, :cond_3

    const/4 v11, 0x0

    :goto_0
    if-nez p4, :cond_4

    const/16 v16, 0x0

    :goto_1
    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const-string v2, "photos_of_user"

    const-string v3, "collection_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPhotosData"

    const-string v3, ">>>>> approved photos"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "photos_of_user"

    const/4 v10, 0x0

    move-object/from16 v2, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "EsPhotosData"

    const-string v3, ">>>>> unapproved photos"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "photos_of_user"

    const/4 v10, 0x0

    move-object/from16 v2, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_USER_PHOTOS], userId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", approved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", unapproved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v13, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_2

    :cond_3
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v11

    goto/16 :goto_0

    :cond_4
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v16

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v3, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "EsPhotosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_USER_PHOTOS], userId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", approved: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", unapproved: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v2

    :cond_6
    if-gtz v11, :cond_7

    if-lez v16, :cond_8

    :cond_7
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p5

    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v13, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_8
    return-void
.end method

.method public static setPhotosFromPostsAlbumName(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    return-void
.end method

.method static syncTopLevel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v15, 0x0

    :goto_0
    return v15

    :cond_0
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "EsPhotosData"

    const-string v3, "    #syncTopLevel(); start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    const-string v2, "Photos:TopLevel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    const/4 v15, 0x1

    new-instance v1, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed user photo; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed photo albums; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    :cond_3
    new-instance v1, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    const-string v10, "camerasync"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed camera photos; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    :cond_4
    new-instance v1, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    const-string v10, "posts"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed post photos; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    :cond_5
    if-eqz v15, :cond_6

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "EsPhotosData"

    const-string v3, "    #syncTopLevel(); completed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto/16 :goto_0
.end method

.method public static updateAlbumAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "audience"

    invoke-static {p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumAudience(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Ljava/lang/Long;

    invoke-static {p0, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->notifyAlbumChange$70674dbd(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static updateDownloadPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v8, 0x4

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v4, "downloadable"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v4, "photo"

    const-string v5, "owner_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "EsPhotosData"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[UPDATE_PHOTO_SETTINGS], duration: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :catchall_0
    move-exception v4

    const-string v5, "EsPhotosData"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "EsPhotosData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[UPDATE_PHOTO_SETTINGS], duration: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    throw v4
.end method

.method public static updateInstantUploadCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/DataPhoto;

    const/4 v11, 0x4

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x6

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "type"

    const-string v7, "from_my_phone"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "sort_order"

    const/16 v7, 0x1e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "photo_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v6, "from_my_phone"

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotosHomeRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v6, -0x1

    cmp-long v6, v1, v6

    if-eqz v6, :cond_3

    const-string v6, "photo_home"

    const-string v7, "type=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "from_my_phone"

    aput-object v10, v8, v9

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const-string v6, "photo_home_cover"

    const-string v7, "photo_home_key=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v0, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz p2, :cond_1

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    const-string v6, "photo_home_key"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v6, p2, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "photo_id"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v6, "url"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "width"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "height"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "size"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "photo_home_cover"

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v6, "EsPhotosData"

    invoke-static {v6, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "EsPhotosData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[INSERT_COVER_INSTANT_UPLOAD], duration: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    :try_start_1
    const-string v6, "photo_home"

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v1

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v7, "EsPhotosData"

    invoke-static {v7, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[INSERT_COVER_INSTANT_UPLOAD], duration: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v6
.end method

.method public static updatePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/Comment;)V
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/Comment;

    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v7, 0x1

    new-array v0, v7, [Ljava/lang/String;

    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    aput-object v7, v0, v10

    :try_start_0
    const-string v7, "SELECT photo_id FROM photo_comment WHERE comment_id = ?"

    invoke-static {v1, v7, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    const-string v7, "EsPhotosData"

    invoke-static {v7, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "COMMENT [id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", content: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v11, v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "photo_id"

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "comment_id"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "author_id"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "content"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    if-eqz v7, :cond_1

    const-string v7, "create_time"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    if-eqz v7, :cond_2

    const-string v7, "update_time"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v7, :cond_3

    const-string v7, "plusone_data"

    sget-object v8, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {v8, v9}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v1, v7, v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v7, "EsPhotosData"

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[UPDATE_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", comment ID: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", duration: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v7, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v2, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_5
    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v7, "EsPhotosData"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "WARNING: could not find photo for the comment: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updatePhotoCommentList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataComment;",
            ">;)V"
        }
    .end annotation

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-nez p3, :cond_2

    const/4 v2, 0x0

    :goto_0
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    :try_start_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataComment;

    move-object/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v10

    const-string v11, "EsPhotosData"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v11, 0x3

    const-string v12, "EsPhotosData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "COMMENT [id: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v1, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", content: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v1, Lcom/google/api/services/plusi/model/DataComment;->text:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v11, v1, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-static {v3, v11, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-static {v3, v0, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateCommentCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    :cond_1
    sget-object v11, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v11, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v6, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    goto/16 :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v11, "EsPhotosData"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_4

    const-string v11, "EsPhotosData"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[INSERT_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", num comments: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", duration: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v9, :cond_6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v12, v11, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catchall_0
    move-exception v11

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v12, "EsPhotosData"

    const/4 v13, 0x4

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "EsPhotosData"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[INSERT_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", num comments: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", duration: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v11

    :cond_6
    return-void
.end method

.method public static updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;Z)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/api/services/plusi/model/DataPlusOne;
    .param p5    # Z

    const/4 v4, 0x1

    const-string v6, "EsPhotosData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez p4, :cond_2

    const-string v3, "N/A"

    :goto_0
    const-string v6, "EsPhotosData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, ">>>>> updatePhotoCommentPlusOneId photo id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", commentId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", plusOneId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    if-eqz p5, :cond_4

    if-eqz p4, :cond_4

    invoke-static {v1, p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataPlusOne;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return v4

    :cond_2
    iget-object v3, p4, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    iget-object v7, p4, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p4, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    iput-object v6, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoCommentPlusOneContentValues(Lcom/google/api/services/plusi/model/DataPlusOne;)Landroid/content/ContentValues;

    move-result-object v5

    :goto_2
    invoke-static {v1, p3, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v6, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :cond_4
    invoke-static {p4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoCommentPlusOneContentValues(Lcom/google/api/services/plusi/model/DataPlusOne;)Landroid/content/ContentValues;

    move-result-object v5

    goto :goto_2
.end method

.method public static updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const-string v0, "EsPhotosData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsPhotosData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>>>> updatePhotoCommentPlusOne photo id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", commentId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataPlusOne;

    move-result-object v4

    invoke-static {v4, p4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePlusOne(Lcom/google/api/services/plusi/model/DataPlusOne;Z)Lcom/google/api/services/plusi/model/DataPlusOne;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;Z)Z

    move-result v0

    return v0
.end method

.method public static updatePhotoPlusOne$55b1eb27(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v3, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, ">> updatePlusOne; photo id: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_2

    const-string v2, ""

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " +1\'d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "photo_plusone"

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "plusone_data"

    aput-object v4, v3, v7

    const-string v4, "photo_id=?"

    new-array v5, v5, [Ljava/lang/String;

    aput-object p2, v5, v7

    move-object v7, v6

    move-object v8, v6

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    const/4 v11, 0x0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object v11, v0

    :cond_1
    invoke-static {v11, p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePlusOne(Lcom/google/api/services/plusi/model/DataPlusOne;Z)Lcom/google/api/services/plusi/model/DataPlusOne;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-static {v1, v11, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    :goto_1
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v10, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-void

    :cond_2
    const-string v2, " (un)"

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-static {v1, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public static updatePhotoPlusOne$95d6774(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/api/services/plusi/model/DataPlusOne;

    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">> updatePlusOne; photo id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    if-eqz p3, :cond_1

    invoke-static {v0, p3, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :cond_1
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static updatePhotoShapeApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJZ)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # J
    .param p6    # Z

    const-string v14, "EsPhotosData"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_0

    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, ">> updatePhotoShape photo id: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", approved? "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p6

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    :try_start_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v4, 0x0

    :try_start_1
    const-string v14, "SELECT notification_count FROM photo_home WHERE type = \'photos_of_me\'"

    const/4 v15, 0x0

    invoke-static {v2, v14, v15}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    :cond_1
    :goto_0
    const-wide/16 v14, 0x0

    cmp-long v14, v4, v14

    if-lez v14, :cond_2

    :try_start_2
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    const-string v14, "notification_count"

    const-wide/16 v15, 0x1

    sub-long v15, v4, v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v14, "photo_home"

    const-string v15, "type = \'photos_of_me\'"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v14, v13, v15, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_2
    if-nez p6, :cond_3

    const/4 v14, 0x1

    new-array v10, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v11, 0x0

    :try_start_3
    const-string v14, "SELECT subject_id FROM photo_shape WHERE shape_id = ?"

    invoke-static {v2, v14, v10}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    :goto_1
    if-eqz v11, :cond_3

    const/4 v14, 0x2

    :try_start_4
    new-array v12, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v11, v12, v14

    const/4 v14, 0x1

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v12, v14

    const-string v14, "photos_of_user"

    const-string v15, "collection_id=? AND photo_id=?"

    invoke-virtual {v2, v14, v15, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    const/4 v14, 0x1

    new-array v7, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v7, v14

    if-eqz p6, :cond_6

    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    const-string v14, "status"

    const-string v15, "ACCEPTED"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "photo_shape"

    const-string v15, "shape_id = ?"

    invoke-virtual {v2, v14, v13, v15, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_4

    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "[UPDATE_SHAPE_APPROVAL], duration: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    sget-object v14, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-wide/from16 v0, p2

    invoke-static {v14, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v6, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :catch_0
    move-exception v3

    :try_start_5
    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_1

    const-string v14, "EsPhotosData"

    const-string v15, "Notification count not found; have you sync\'d?"

    invoke-static {v14, v15, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v14

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v15, "EsPhotosData"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_5

    const-string v15, "EsPhotosData"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "[UPDATE_SHAPE_APPROVAL], duration: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v14

    :catch_1
    move-exception v3

    :try_start_6
    const-string v14, "EsPhotosData"

    invoke-static {v14, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_6
    const-string v14, "photo_shape"

    const-string v15, "shape_id = ?"

    invoke-virtual {v2, v14, v15, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2
.end method

.method public static updatePhotosOfYouCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/DataPhoto;

    const/4 v11, 0x4

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x6

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "type"

    const-string v7, "photos_of_me"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "sort_order"

    const/16 v7, 0x14

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "photo_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v6, "photos_of_me"

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotosHomeRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v6, -0x1

    cmp-long v6, v1, v6

    if-eqz v6, :cond_3

    const-string v6, "photo_home"

    const-string v7, "type=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "photos_of_me"

    aput-object v10, v8, v9

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const-string v6, "photo_home_cover"

    const-string v7, "photo_home_key=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v0, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz p2, :cond_1

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    const-string v6, "photo_home_key"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v6, p2, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "photo_id"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v6, "url"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "width"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "height"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "size"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "photo_home_cover"

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v6, "EsPhotosData"

    invoke-static {v6, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "EsPhotosData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[INSERT_COVER_PHOTOS_OF_YOU], duration: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    :try_start_1
    const-string v6, "photo_home"

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v1

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v7, "EsPhotosData"

    invoke-static {v7, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[INSERT_COVER_PHOTOS_OF_YOU], duration: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v6
.end method

.method private static updatePlusOne(Lcom/google/api/services/plusi/model/DataPlusOne;Z)Lcom/google/api/services/plusi/model/DataPlusOne;
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/DataPlusOne;
    .param p1    # Z

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataPlusOne;-><init>()V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    :cond_2
    :goto_1
    iput-object v0, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    goto :goto_1
.end method
