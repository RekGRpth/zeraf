.class public final Lcom/google/android/apps/plus/content/EsAnalyticsData;
.super Ljava/lang/Object;
.source "EsAnalyticsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;,
        Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;
    }
.end annotation


# static fields
.field private static mIsTabletDevice:Ljava/lang/Boolean;

.field private static final runtime:Ljava/lang/Runtime;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    return-void
.end method

.method public static bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ClientOzEvent;

    const-string v5, "event_data"

    invoke-virtual {v0, v2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v5, "analytics_events"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_1
    return-void

    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method private static createActionTarget(Landroid/os/Bundle;)Lcom/google/api/services/plusi/model/ActionTarget;
    .locals 25
    .param p0    # Landroid/os/Bundle;

    const-string v22, "extra_activity_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_comment_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_notification_read"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_notification_types"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_coalescing_codes"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_num_unread_notifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_media_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_has_emotishare"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_external_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_prev_num_unread_noti"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_creation_source_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_search_query"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_notification_volume_change"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const-string v22, "extra_notification_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_1

    const/4 v3, 0x0

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    new-instance v3, Lcom/google/api/services/plusi/model/ActionTarget;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/ActionTarget;-><init>()V

    const-string v22, "extra_notification_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    const-string v22, "extra_notification_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2

    iput-object v11, v3, Lcom/google/api/services/plusi/model/ActionTarget;->notificationId:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_2

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: notificationId: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v22, "extra_search_query"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    const-string v22, "extra_search_query"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_4

    new-instance v22, Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    invoke-direct/range {v22 .. v22}, Lcom/google/api/services/plusi/model/LoggedAutoComplete;-><init>()V

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->query:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->acceptedQuery:Ljava/lang/String;

    const-string v22, "extra_search_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v22, v0

    const-string v23, "extra_search_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->type:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_3

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget.autoComplete.type: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->type:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v22, v0

    const-string v23, "2"

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->personalizationType:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_4

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget.autoComplete.query: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget.autoComplete.acceptedQuery: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget.autoComplete.personalizationType: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LoggedAutoComplete;->personalizationType:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v22, "extra_activity_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    const-string v22, "extra_activity_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_5

    iput-object v4, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityId:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_5

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: activityId: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-string v22, "extra_comment_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_6

    const-string v22, "extra_comment_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_6

    iput-object v7, v3, Lcom/google/api/services/plusi/model/ActionTarget;->commentId:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_6

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: commentId: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v22, "extra_notification_read"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_7

    const-string v22, "extra_notification_read"

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_c

    const/16 v22, 0x1

    :goto_1
    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->isUnreadNotification:Ljava/lang/Boolean;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_7

    const-string v23, "EsAnalyticsData"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v22, "> createActionTarget: isUnreadNotification: "

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v9, :cond_d

    const/16 v22, 0x1

    :goto_2
    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const-string v22, "extra_notification_volume_change"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_8

    const-string v22, "extra_notification_volume_change"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v21

    if-eqz v21, :cond_8

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    if-lez v22, :cond_8

    invoke-static {}, Lcom/google/api/services/plusi/model/VolumeChangeJson;->getInstance()Lcom/google/api/services/plusi/model/VolumeChangeJson;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/VolumeChangeJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/VolumeChange;

    move-object/from16 v0, v20

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->volumeChange:Lcom/google/api/services/plusi/model/VolumeChange;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_8

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget.volumeChange keyType: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeChange;->keyType:Ljava/lang/Integer;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " prevVolume: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeChange;->previousVolume:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " previousNotificationSetting: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeChange;->previousNotificationSetting:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " nextVolume: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeChange;->nextVolume:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " nextNotificationSetting: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeChange;->nextNotificationSetting:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const-string v22, "extra_num_unread_notifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_9

    const-string v22, "extra_num_unread_notifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->numUnreadNotifications:Ljava/lang/Integer;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_9

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: numUnreadNotifications: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const-string v22, "extra_prev_num_unread_noti"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    const-string v22, "extra_prev_num_unread_noti"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->previousNumUnreadNotifications:Ljava/lang/Integer;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_a

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: previousNumUnreadNotifications: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const-string v22, "extra_notification_types"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_11

    const-string v22, "extra_coalescing_codes"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_11

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "extra_notification_types"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v19

    const-string v22, "extra_coalescing_codes"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v19, :cond_11

    if-eqz v6, :cond_11

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_11

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v22

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_11

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    :goto_3
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v8, v0, :cond_f

    new-instance v12, Lcom/google/api/services/plusi/model/NotificationTypes;

    invoke-direct {v12}, Lcom/google/api/services/plusi/model/NotificationTypes;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    const/16 v22, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    if-nez v17, :cond_e

    const/16 v22, 0x0

    :goto_4
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v18

    iput-object v0, v12, Lcom/google/api/services/plusi/model/NotificationTypes;->type:Ljava/util/List;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_b

    iput-object v5, v12, Lcom/google/api/services/plusi/model/NotificationTypes;->coalescingCode:Ljava/lang/String;

    :cond_b
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "("

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ") "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_c
    const/16 v22, 0x0

    goto/16 :goto_1

    :cond_d
    const/16 v22, 0x0

    goto/16 :goto_2

    :cond_e
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v22

    goto :goto_4

    :cond_f
    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_10

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: notificationTypes: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iput-object v13, v3, Lcom/google/api/services/plusi/model/ActionTarget;->notificationTypes:Ljava/util/List;

    :cond_11
    const-string v22, "extra_external_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_12

    const-string v22, "extra_external_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->externalUrl:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_12

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: externalUrl: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->externalUrl:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    const-string v22, "extra_has_emotishare"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_13

    const-string v22, "extra_media_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_13

    const-string v22, "extra_creation_source_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_14

    :cond_13
    new-instance v22, Lcom/google/api/services/plusi/model/ActivityDetails;

    invoke-direct/range {v22 .. v22}, Lcom/google/api/services/plusi/model/ActivityDetails;-><init>()V

    move-object/from16 v0, v22

    iput-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    :cond_14
    const-string v22, "extra_has_emotishare"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_15

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v22, v0

    const/16 v23, 0x14e

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActivityDetails;->embedType:Ljava/lang/Integer;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_15

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: embedType: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityDetails;->embedType:Ljava/lang/Integer;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    const-string v22, "extra_media_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_16

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v22, v0

    const-string v23, "extra_media_url"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActivityDetails;->mediaUrl:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_16

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: mediaUrl: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityDetails;->mediaUrl:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    const-string v22, "extra_creation_source_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v22, v0

    const-string v23, "extra_creation_source_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActivityDetails;->sourceStreamId:Ljava/lang/String;

    const-string v22, "EsAnalyticsData"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_0

    const-string v22, "EsAnalyticsData"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "> createActionTarget: sourceStreamId: "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityDetails;->sourceStreamId:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private static createClientActionData(Landroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientActionData;
    .locals 21
    .param p0    # Landroid/os/Bundle;

    const-string v18, "extra_gaia_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_participant_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_circle_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_square_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_promo_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_promo_group_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "extra_posting_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    new-instance v2, Lcom/google/api/services/plusi/model/ClientActionData;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ClientActionData;-><init>()V

    const-string v18, "extra_gaia_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    const-string v18, "extra_gaia_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    new-instance v17, Ljava/util/ArrayList;

    const/16 v18, 0x1

    invoke-direct/range {v17 .. v18}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/google/api/services/plusi/model/ClientActionData;->obfuscatedGaiaId:Ljava/util/List;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_2

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: target gaiaId: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v18, "extra_participant_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    const-string v18, "extra_participant_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    if-eqz v13, :cond_4

    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_4

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    new-instance v10, Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;-><init>()V

    iput-object v12, v10, Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iput-object v11, v2, Lcom/google/api/services/plusi/model/ClientActionData;->circleMember:Ljava/util/List;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_4

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: participants: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v18, "extra_circle_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_6

    const-string v18, "extra_circle_ids"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_6

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v3, Lcom/google/api/services/plusi/model/ClientLoggedCircle;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/ClientLoggedCircle;-><init>()V

    iput-object v4, v3, Lcom/google/api/services/plusi/model/ClientLoggedCircle;->circleId:Ljava/lang/String;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientActionData;->circle:Ljava/util/List;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_6

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: circleIds: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v18, "extra_square_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "extra_square_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_7

    new-instance v18, Lcom/google/api/services/plusi/model/ClientLoggedSquare;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/ClientLoggedSquare;-><init>()V

    move-object/from16 v0, v18

    iput-object v0, v2, Lcom/google/api/services/plusi/model/ClientActionData;->square:Lcom/google/api/services/plusi/model/ClientLoggedSquare;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/ClientActionData;->square:Lcom/google/api/services/plusi/model/ClientLoggedSquare;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ClientLoggedSquare;->obfuscatedGaiaId:Ljava/lang/String;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: target squareId: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const-string v18, "extra_posting_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    const-string v18, "extra_posting_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_8

    new-instance v18, Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;-><init>()V

    move-object/from16 v0, v18

    iput-object v0, v2, Lcom/google/api/services/plusi/model/ClientActionData;->shareboxInfo:Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/ClientActionData;->shareboxInfo:Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v14, v0, Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;->postingMode:Ljava/lang/String;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_8

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: postingMode: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const-string v18, "extra_promo_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_9

    const-string v18, "extra_promo_group_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    :cond_9
    new-instance v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;

    invoke-direct {v15}, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;-><init>()V

    const-string v18, "extra_promo_group_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    const-string v18, "extra_promo_group_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;->promoGroupId:Ljava/lang/String;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_a

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: promoGroupId: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;->promoGroupId:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const-string v18, "extra_promo_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    new-instance v18, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;-><init>()V

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;->componentType:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;->componentType:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;

    move-object/from16 v18, v0

    const-string v19, "extra_promo_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;->promoType:Ljava/lang/String;

    const-string v18, "EsAnalyticsData"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_b

    const-string v18, "EsAnalyticsData"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "> createClientActionData: promoType: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;->componentType:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentType;->promoType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iput-object v15, v2, Lcom/google/api/services/plusi/model/ClientActionData;->rhsComponent:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;

    goto/16 :goto_0
.end method

.method private static createClientOutData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/ClientOutputData;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/google/api/services/plusi/model/ClientOutputData;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ClientOutputData;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v2, Lcom/google/api/services/plusi/model/ClientUserInfo;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ClientUserInfo;-><init>()V

    iput-object p0, v2, Lcom/google/api/services/plusi/model/ClientUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v1, Lcom/google/api/services/plusi/model/ClientOutputData;->userInfo:Ljava/util/List;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ClientOutputData;->userInfo:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedCircle;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedCircle;-><init>()V

    iput-object p1, v0, Lcom/google/api/services/plusi/model/ClientLoggedCircle;->circleId:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ClientOutputData;->streamFilterCircle:Lcom/google/api/services/plusi/model/ClientLoggedCircle;

    :cond_1
    return-object v1
.end method

.method public static createClientOzEvent(Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientOzEvent;
    .locals 23
    .param p0    # Lcom/google/android/apps/plus/analytics/OzActions;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p3    # J
    .param p5    # J
    .param p7    # Landroid/os/Bundle;

    new-instance v5, Lcom/google/api/services/plusi/model/ClientOzEvent;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/ClientOzEvent;-><init>()V

    const/4 v14, 0x0

    const/4 v3, 0x0

    const/16 v18, 0x0

    const/4 v8, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/google/api/services/plusi/model/ClientOzEvent;->clientTimeMsec:Ljava/lang/Long;

    new-instance v15, Lcom/google/api/services/plusi/model/OzEvent;

    invoke-direct {v15}, Lcom/google/api/services/plusi/model/OzEvent;-><init>()V

    new-instance v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/FavaDiagnostics;-><init>()V

    const-wide/16 v19, 0x0

    cmp-long v19, p3, v19

    if-lez v19, :cond_f

    cmp-long v19, p5, p3

    if-ltz v19, :cond_f

    sub-long v19, p5, p3

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/analytics/OzActions;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_0

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Action name: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " namespace: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " typeNum: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/analytics/OzActions;->name()Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewData()Lcom/google/api/services/plusi/model/OutputData;

    move-result-object v16

    if-eqz v16, :cond_1

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/api/services/plusi/model/OzEvent;->startViewData:Lcom/google/api/services/plusi/model/OutputData;

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_2

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "StartView name: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " namespace: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " typeNum: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " filterType: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " tab: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/analytics/OzViews;->name()Ljava/lang/String;

    move-result-object v18

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewData()Lcom/google/api/services/plusi/model/OutputData;

    move-result-object v6

    if-eqz v6, :cond_3

    iput-object v6, v15, Lcom/google/api/services/plusi/model/OzEvent;->endViewData:Lcom/google/api/services/plusi/model/OutputData;

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v14, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_4

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "EndView name: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " namespace: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " typeNum: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " filterType: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " tab: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/analytics/OzViews;->name()Ljava/lang/String;

    move-result-object v8

    :cond_4
    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-nez v19, :cond_5

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    :cond_5
    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    if-nez v19, :cond_6

    if-nez v14, :cond_10

    sget-object v19, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    sget-object v19, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/analytics/OzViews;->name()Ljava/lang/String;

    move-result-object v18

    :goto_1
    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_6

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "StartView name: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " namespace: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " typeNum: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    if-eqz p7, :cond_b

    const-string v19, "extra_start_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_8

    const-string v19, "extra_start_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v12

    if-nez v17, :cond_11

    const/4 v4, 0x0

    :goto_2
    invoke-static {v12, v4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOutData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/ClientOutputData;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/google/api/services/plusi/model/ClientOzEvent;->startViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_7

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "createClientOzEvent: start view target gaiaId: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "createClientOzEvent: start view streamFilterCircleId: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const-string v19, "extra_end_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_9

    const-string v19, "extra_end_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_9

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-static {v12, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOutData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/ClientOutputData;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/google/api/services/plusi/model/ClientOzEvent;->endViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_9

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "createClientOzEvent: end view target gaiaId: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const-string v19, "extra_platform_event"

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_a

    const-string v19, "extra_platform_event"

    const/16 v20, 0x0

    move-object/from16 v0, p7

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v15, Lcom/google/api/services/plusi/model/OzEvent;->isNativePlatformEvent:Ljava/lang/Boolean;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_a

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "createClientOzEvent: isPlatform: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OzEvent;->isNativePlatformEvent:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientActionData(Landroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientActionData;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/google/api/services/plusi/model/ClientOzEvent;->actionData:Lcom/google/api/services/plusi/model/ClientActionData;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createActionTarget(Landroid/os/Bundle;)Lcom/google/api/services/plusi/model/ActionTarget;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v15, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    :cond_b
    sget-boolean v19, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v19, :cond_d

    new-instance v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

    invoke-direct {v13}, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;-><init>()V

    sget-object v19, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->jsHeapSizeLimit:Ljava/lang/Long;

    sget-object v19, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    sget-object v19, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v10

    iget-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    sub-long v19, v19, v10

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->usedJsHeapSize:Ljava/lang/Long;

    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_c

    const-string v19, "EsAnalyticsData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "MemoryStats Max: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->jsHeapSizeLimit:Ljava/lang/Long;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Total: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Used: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-object v0, v13, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->usedJsHeapSize:Ljava/lang/Long;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Free: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iput-object v13, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

    :cond_d
    const-string v19, "EsAnalyticsData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_e

    if-nez v3, :cond_12

    const-string v19, "EsAnalyticsData"

    const-string v20, "EVENT SUMMARY: %s -> %s"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v18, v21, v22

    const/16 v22, 0x1

    aput-object v8, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_3
    iput-object v9, v15, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    iput-object v15, v5, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    return-object v5

    :cond_f
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_10
    new-instance v19, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct/range {v19 .. v19}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    move-object/from16 v0, v19

    iput-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v14, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    iget-object v0, v9, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "UNKNOWN:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_1

    :cond_11
    const-string v19, "extra_circle_id"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    :cond_12
    if-nez v8, :cond_13

    const-string v19, "EsAnalyticsData"

    const-string v20, "EVENT SUMMARY: %s in %s"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v3, v21, v22

    const/16 v22, 0x1

    aput-object v18, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_13
    const-string v19, "EsAnalyticsData"

    const-string v20, "EVENT SUMMARY: %s in %s (unexpected endView: %s)"

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v3, v21, v22

    const/16 v22, 0x1

    aput-object v18, v21, v22

    const/16 v22, 0x2

    aput-object p2, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method public static createClientOzExtension(Landroid/content/Context;)Lcom/google/api/services/plusi/model/ClientOzExtension;
    .locals 13
    .param p0    # Landroid/content/Context;

    const-wide/16 v11, 0x0

    const/4 v7, 0x0

    new-instance v2, Lcom/google/api/services/plusi/model/ClientOzExtension;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ClientOzExtension;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->sendTimeMsec:Ljava/lang/Long;

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    if-eqz v6, :cond_1

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    :goto_0
    if-eqz v6, :cond_8

    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_TABLET:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->value()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientId:Ljava/lang/String;

    const-string v6, "EsAnalyticsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "EsAnalyticsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Set the client id to "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->value()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->callingApplication:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-object v2

    :cond_1
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xb

    if-ge v6, v8, :cond_2

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_0

    :cond_2
    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    if-nez v6, :cond_3

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_0

    :cond_3
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    if-nez v6, :cond_4

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v6, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v6, v8, Landroid/util/DisplayMetrics;->xdpi:F

    float-to-double v9, v6

    cmpl-double v6, v9, v11

    if-eqz v6, :cond_5

    iget v6, v8, Landroid/util/DisplayMetrics;->ydpi:F

    float-to-double v9, v6

    cmpl-double v6, v9, v11

    if-nez v6, :cond_6

    :cond_5
    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto/16 :goto_0

    :cond_6
    iget v6, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v6, v6

    iget v9, v8, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v6, v9

    float-to-double v9, v6

    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    iget v8, v8, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v6, v8

    float-to-double v11, v6

    mul-double v8, v9, v9

    mul-double v10, v11, v11

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x4014000000000000L

    cmpl-double v6, v8, v10

    if-ltz v6, :cond_7

    const/4 v6, 0x1

    :goto_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->mIsTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto/16 :goto_0

    :cond_7
    move v6, v7

    goto :goto_3

    :cond_8
    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_OS:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public static createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static createVolumeChange(IIZIZ)Lcom/google/api/services/plusi/model/VolumeChange;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # Z
    .param p3    # I
    .param p4    # Z

    new-instance v0, Lcom/google/api/services/plusi/model/VolumeChange;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/VolumeChange;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeChange;->keyType:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeChange;->previousVolume:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getNotificationSetting(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeChange;->previousNotificationSetting:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeChange;->nextVolume:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getNotificationSetting(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeChange;->nextNotificationSetting:Ljava/lang/String;

    return-object v0
.end method

.method private static getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/OutputData;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private static getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "extra_gaia_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getNotificationSetting(Z)Ljava/lang/String;
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "2"

    goto :goto_0
.end method

.method private static getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/OutputData;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->tab:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->tab:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [B

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "event_data"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "analytics_events"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static queryLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    :try_start_0
    const-string v1, "SELECT last_analytics_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public static removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v1, 0x1

    :try_start_1
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "event_data"

    aput-object v3, v2, v1

    const-string v1, "analytics_events"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    if-nez v9, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_0
    return-object v12

    :cond_0
    :try_start_2
    const-string v1, "event_data"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    const/4 v13, 0x0

    :goto_1
    invoke-interface {v9, v13}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/ClientOzEvent;

    if-eqz v10, :cond_1

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_2
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const-string v1, "analytics_events"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "last_analytics_sync_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
