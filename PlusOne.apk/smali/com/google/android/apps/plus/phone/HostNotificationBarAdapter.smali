.class public final Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "HostNotificationBarAdapter.java"


# static fields
.field private static sEmptyCursor:Landroid/database/MatrixCursor;


# instance fields
.field private mColorRead:I

.field private mColorUnread:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mUnreadNotificationCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->addPartition(ZZ)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$color;->notifications_text_color_read:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mColorRead:I

    sget v1, Lcom/google/android/apps/plus/R$color;->notifications_text_color_unread:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mColorUnread:I

    invoke-static {}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->getEmptyCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    return-void
.end method

.method private static bindNotificationUserAvatar(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/AvatarView;Landroid/widget/ImageView;Z)V
    .locals 9
    .param p0    # Landroid/database/Cursor;
    .param p1    # Lcom/google/android/apps/plus/views/AvatarView;
    .param p2    # Landroid/widget/ImageView;
    .param p3    # Z

    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x6

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActionList([B)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataAction;

    if-eqz v0, :cond_0

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/DataItem;

    if-eqz v5, :cond_0

    iget-object v7, v5, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v7, :cond_0

    iget-object v2, v5, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    iget-object v6, v2, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    invoke-virtual {p1, v6, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v7, 0x8

    invoke-virtual {p2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {p1, p3}, Lcom/google/android/apps/plus/views/AvatarView;->setDimmed(Z)V

    return-void
.end method

.method private static getEmptyCursor()Landroid/database/Cursor;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    if-nez v0, :cond_0

    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "empty"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "empty"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    return-object v0
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/16 v7, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    const/16 v0, 0xc

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_1

    move v2, v3

    :goto_0
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez v2, :cond_2

    move v5, v3

    :goto_1
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-virtual {v1, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    const/16 v5, 0xd

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v5, v3, :cond_3

    :goto_2
    const/4 v5, 0x3

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/16 v6, 0x10

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    packed-switch v5, :pswitch_data_0

    :goto_3
    :pswitch_0
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_8

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mColorRead:I

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v5, v4

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2

    :pswitch_1
    if-nez v3, :cond_4

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsNotificationData;->isEventNotificationType(I)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_event:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_5
    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsNotificationData;->isCommentNotificationType(I)Z

    move-result v1

    if-eqz v1, :cond_6

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_comment:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_6
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_post:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_2
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_post:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_3
    invoke-static {p3, v1, v0, v2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->bindNotificationUserAvatar(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/AvatarView;Landroid/widget/ImageView;Z)V

    goto :goto_3

    :pswitch_4
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_photo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_5
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_games:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_6
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_event:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_7
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_notification_alert:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_8
    const/16 v3, 0x30

    if-ne v6, v3, :cond_7

    invoke-static {p3, v1, v0, v2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->bindNotificationUserAvatar(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/AvatarView;Landroid/widget/ImageView;Z)V

    goto :goto_3

    :cond_7
    const/16 v3, 0x15

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x17

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setDimmed(Z)V

    goto/16 :goto_3

    :cond_8
    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mColorUnread:I

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method protected final getItemViewType(II)I
    .locals 0
    .param p1    # I
    .param p2    # I

    return p1
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final getUnreadNotificationCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mUnreadNotificationCount:I

    return v0
.end method

.method protected final isEnabled$255f299(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->loading_notifications:I

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->notification_row_view:I

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->no_notifications:I

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final setNotifications(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->getEmptyCursor()Landroid/database/Cursor;

    move-result-object v1

    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput v2, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mUnreadNotificationCount:I

    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/16 v1, 0xc

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eq v1, v3, :cond_3

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mUnreadNotificationCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->mUnreadNotificationCount:I

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_0
.end method
