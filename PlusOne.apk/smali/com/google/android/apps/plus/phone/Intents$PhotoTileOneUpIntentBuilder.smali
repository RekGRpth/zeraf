.class public final Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;
.super Ljava/lang/Object;
.source "Intents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/Intents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoTileOneUpIntentBuilder"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mIntent:Landroid/content/Intent;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mTileId:Ljava/lang/String;

.field private mViewId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/plus/content/EsAccount;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Class;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Account must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "account"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mTileId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "tile_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mTileId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mViewId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "view_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mViewId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "photo_ref"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public final setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object p0
.end method

.method public final setTileId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mTileId:Ljava/lang/String;

    return-object p0
.end method

.method public final setViewId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->mViewId:Ljava/lang/String;

    return-object p0
.end method
