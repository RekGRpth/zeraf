.class final Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter$1;
.super Ljava/lang/Object;
.source "PhotoAlbumsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMovedToScrapHeap(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->photo:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->access$000(Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
