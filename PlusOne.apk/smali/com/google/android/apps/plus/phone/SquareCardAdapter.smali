.class public final Lcom/google/android/apps/plus/phone/SquareCardAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "SquareCardAdapter.java"


# static fields
.field private static sInitialized:Z

.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mInflater:Landroid/view/LayoutInflater;

.field protected mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;
    .param p4    # Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    sget-boolean v2, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sInitialized:Z

    if-nez v2, :cond_0

    sput-boolean v0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sInitialized:Z

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p4, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    sget-object v2, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v2, :cond_1

    :goto_0
    invoke-virtual {p4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v1, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v2, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v3, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p4, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    new-instance v0, Lcom/google/android/apps/plus/phone/SquareCardAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/SquareCardAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/SquareCardAdapter;)V

    invoke-virtual {p4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v2, 0xe

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->square_invited_item_text:I

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->square_member_item_text:I

    goto :goto_1

    :pswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->square_suggested_item_text:I

    goto :goto_1

    :pswitch_4
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    const/16 v2, 0x10

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    check-cast p1, Lcom/google/android/apps/plus/views/SquareListItemView;

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    invoke-virtual {p1, p3, v2, v1, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->init(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;ZZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/16 v2, 0xe

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_1
    const/16 v2, 0x10

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, -0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v3, 0xe

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-object v2

    :pswitch_0
    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    const/16 v3, 0x9

    invoke-static {p1, v2, v5, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextView(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v3, -0x3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnCount()I

    move-result v4

    invoke-direct {v1, v8, v3, v4, v6}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    iput v7, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x10

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/google/android/apps/plus/R$layout;->square_list_item_view:I

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_1
    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v1, v8, v7, v6, v6}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/google/android/apps/plus/R$layout;->square_list_invitation_view:I

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method
