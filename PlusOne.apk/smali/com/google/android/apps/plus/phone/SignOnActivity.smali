.class public Lcom/google/android/apps/plus/phone/SignOnActivity;
.super Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.source "SignOnActivity.java"


# instance fields
.field private mCallingActivity:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;-><init>()V

    return-void
.end method

.method public static finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 6
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "from_signup"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v4, "no_account"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "intent"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private fireIntent(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {p0, v2, v1}, Lcom/google/android/apps/plus/phone/Intents;->getTargetIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_1

    :cond_2
    const-string v1, "from_signup"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x2000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method private recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v2, :cond_1

    move-object v3, v1

    :goto_0
    if-eqz v3, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method protected final getUpgradeOrigin()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getTargetIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "DEFAULT"

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/plus/phone/PlusOneActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PLUS_ONE"

    goto :goto_0

    :cond_2
    const-string v2, "DEFAULT"

    goto :goto_0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getUpgradeOrigin()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p2, p1, p3, v1}, Lcom/google/android/apps/plus/phone/Intents;->getOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V

    if-eqz p1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_0
    if-eqz v0, :cond_1

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-eqz p3, :cond_3

    const-string v1, "no_account"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->showAccountList()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v1, "SignOnActivity#callingActivity"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    :goto_0
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    goto :goto_0

    :cond_3
    if-nez p1, :cond_1

    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "SignOnActivity#callingActivity"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
