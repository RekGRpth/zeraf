.class final Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;
.super Ljava/lang/Object;
.source "UrlGatewayLoaderActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$000(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v2, v2, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v3, v3, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v4, v4, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mGaiaId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    check-cast p1, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->isConnectionError()Z

    move-result v1

    # setter for: Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$102(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;Z)Z

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mActivityId:Ljava/lang/String;

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1$1;-><init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
