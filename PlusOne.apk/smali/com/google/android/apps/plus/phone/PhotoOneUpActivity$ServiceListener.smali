.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "PhotoOneUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V

    return-void
.end method


# virtual methods
.method public final onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1602(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # invokes: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->hideProgressDialog()V
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1700(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    sget v2, Lcom/google/android/apps/plus/R$string;->remove_photo_error:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mDeleteMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1800(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1602(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-static {v2, p1, p3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->onPhotoRemoved$1349ef()V

    goto :goto_0

    :cond_0
    return-void
.end method
