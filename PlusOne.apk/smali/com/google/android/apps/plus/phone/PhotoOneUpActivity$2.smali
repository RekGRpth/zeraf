.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;
.super Ljava/lang/Object;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int v0, v3, v4

    const/16 v3, 0x64

    if-le v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mKeyboardIsVisible:Z
    invoke-static {v3, v6}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    invoke-interface {v2, v5}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->enableImageTransforms(Z)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mKeyboardIsVisible:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    invoke-interface {v2, v6}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->enableImageTransforms(Z)V

    goto :goto_1

    :cond_1
    return-void
.end method
