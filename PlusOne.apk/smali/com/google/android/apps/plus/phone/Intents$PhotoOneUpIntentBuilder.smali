.class public final Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
.super Ljava/lang/Object;
.source "Intents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/Intents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoOneUpIntentBuilder"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAllowPlusOne:Ljava/lang/Boolean;

.field private mAuthkey:Ljava/lang/String;

.field private mDisableComments:Ljava/lang/Boolean;

.field private mDisplayName:Ljava/lang/String;

.field private mEventId:Ljava/lang/String;

.field private mForceLoadId:Ljava/lang/Long;

.field private mGaiaId:Ljava/lang/String;

.field private final mIntent:Landroid/content/Intent;

.field private mIsPlaceholder:Ljava/lang/Boolean;

.field private mIsStreamPost:Ljava/lang/Boolean;

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mMediaType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

.field private mNotificationId:Ljava/lang/String;

.field private mPageHint:Ljava/lang/Integer;

.field private mPhotoId:Ljava/lang/Long;

.field private mPhotoIndex:Ljava/lang/Integer;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPhotoOnly:Ljava/lang/Boolean;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoUrl:Ljava/lang/String;

.field private mRefreshAlbumId:Ljava/lang/String;

.field private mStreamId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Class;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/content/Intent;
    .locals 10

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Account must be set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "account"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "album_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "album_name"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "allow_plusone"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "display_name"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "event_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mEventId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "force_load_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "refresh_album_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "mediarefs"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mNotificationId:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "notif_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "owner_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPageHint:Ljava/lang/Integer;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "page_hint"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPageHint:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoId:Ljava/lang/Long;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "photo_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "photo_index"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoIndex:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "photos_of_user_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_f

    :cond_e
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_10

    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "show_photo_only"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_19

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_ref"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_11
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_url"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mStreamId:Ljava/lang/String;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "stream_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAuthkey:Ljava/lang/String;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "auth_key"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAuthkey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisableComments:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "disable_photo_comments"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisableComments:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsPlaceholder:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "is_placeholder"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsPlaceholder:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsStreamPost:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "is_stream_post"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsStreamPost:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_17
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v1

    :cond_18
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "page_hint"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_19
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoId:Ljava/lang/Long;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    :goto_2
    invoke-static {v7}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1b

    move-object v5, v7

    :goto_3
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mGaiaId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-eqz v6, :cond_1c

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_4
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_ref"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_1a
    move-object v7, v1

    goto :goto_2

    :cond_1b
    move-object v5, v1

    goto :goto_3

    :cond_1c
    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_4
.end method

.method public final setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object p0
.end method

.method public final setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumId:Ljava/lang/String;

    return-object p0
.end method

.method public final setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAlbumName:Ljava/lang/String;

    return-object p0
.end method

.method public final setAllowPlusOne(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mAuthkey:Ljava/lang/String;

    return-object p0
.end method

.method public final setDisableComments(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisableComments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setDisplayName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mDisplayName:Ljava/lang/String;

    return-object p0
.end method

.method public final setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mEventId:Ljava/lang/String;

    return-object p0
.end method

.method public final setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    return-object p0
.end method

.method public final setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mGaiaId:Ljava/lang/String;

    return-object p0
.end method

.method public final setIsPlaceholder(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsPlaceholder:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setIsStreamPost(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mIsStreamPost:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # [Lcom/google/android/apps/plus/api/MediaRef;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    return-object p0
.end method

.method public final setMediaType(Lcom/google/android/apps/plus/api/MediaRef$MediaType;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mMediaType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    return-object p0
.end method

.method public final setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mNotificationId:Ljava/lang/String;

    return-object p0
.end method

.method public final setPageHint(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPageHint:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoId:Ljava/lang/Long;

    return-object p0
.end method

.method public final setPhotoIndex(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoIndex:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    return-object p0
.end method

.method public final setPhotoOnly(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object p0
.end method

.method public final setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    return-object p0
.end method

.method public final setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    return-object p0
.end method

.method public final setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->mStreamId:Ljava/lang/String;

    return-object p0
.end method
