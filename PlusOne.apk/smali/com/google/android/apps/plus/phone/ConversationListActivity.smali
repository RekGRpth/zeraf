.class public Lcom/google/android/apps/plus/phone/ConversationListActivity;
.super Landroid/app/Activity;
.source "ConversationListActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getMessengerActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x12010000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/plus/phone/ConversationActivity;->hasInstance()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->hasInstance()Z

    move-result v1

    if-nez v1, :cond_0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationListActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationListActivity;->finish()V

    return-void
.end method
