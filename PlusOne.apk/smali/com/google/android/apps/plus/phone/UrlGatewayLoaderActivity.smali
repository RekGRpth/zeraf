.class public Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
.super Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;
.source "UrlGatewayLoaderActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$NoConnectionDialog;,
        Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;
    }
.end annotation


# instance fields
.field private mConnectionError:Z

.field private final mDesktopActivityIdLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final mProfileLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$1;-><init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityIdLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;-><init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->isReadyToRedirect()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->redirect()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->showConnectionErrorMessage()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->redirectToBrowser()V

    goto :goto_0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileIdValidated:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityIdLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->isReadyToRedirect()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->redirect()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->showConnectionErrorMessage()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->redirectToBrowser()V

    goto :goto_0
.end method

.method private showConnectionErrorMessage()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$NoConnectionDialog;

    invoke-direct {v0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$NoConnectionDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "noconnection"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$NoConnectionDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$layout;->url_gateway_loader_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->setContentView(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityId:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mActivityId:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v3, v4, v1}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mGaiaId:Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mDesktopActivityIdLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v4, v2}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v3, v4, v1}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
