.class public Lcom/google/android/apps/plus/phone/HomeActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "HomeActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;
.implements Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/analytics/InstrumentedActivity;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;",
        "Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;"
    }
.end annotation


# static fields
.field private static final REMOVE:Landroid/net/Uri;


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mDestination:Landroid/os/Bundle;

.field private mDestinationState:[Landroid/os/Parcelable;

.field private mDialogTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFirstLoad:Z

.field private mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

.field private mNavigationBar:Landroid/widget/ListView;

.field private mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

.field private mNotificationBar:Landroid/widget/ListView;

.field private mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

.field private mRequestId:Ljava/lang/Integer;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

.field private mUnreadNotificationCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://plus.google.com/downgrade/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/HomeActivity;->REMOVE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    const/16 v0, 0xb

    new-array v0, v0, [Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mFirstLoad:Z

    new-instance v0, Lcom/google/android/apps/plus/phone/HomeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/HomeActivity$1;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/SignOnManager;-><init>(Lvedroid/support/v4/app/FragmentActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/HomeActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/HomeActivity;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->handleServiceCallback$b5e9bbb(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/HomeActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/HomeActivity;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/HomeActivity;
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    return-object v0
.end method

.method private buildDestinationBundleForIntent()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "destination"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "account"

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string v3, "destination"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "destination"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private configureDestinations()V
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->removeAllDestinations()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_nav_home:I

    sget v6, Lcom/google/android/apps/plus/R$string;->home_stream_label:I

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x5

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_circles:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_people_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v4

    if-eqz v4, :cond_5

    move v1, v2

    :goto_0
    if-eqz v1, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_nav_profile:I

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v2, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x7

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_myphotos:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_photos_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    sget-object v4, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/16 v5, 0x8

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_communities:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_squares_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x3

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_hangouts:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_hangout_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x2

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_events:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_events_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x4

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_nav_messenger:I

    sget v7, Lcom/google/android/apps/plus/R$string;->home_screen_huddle_label:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/util/MapUtils;->getPlacesActivityIntent$7ec49240()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/high16 v6, 0x10000

    invoke-virtual {v5, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    :goto_2
    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v4, 0x6

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_nav_local:I

    sget v6, Lcom/google/android/apps/plus/R$string;->home_screen_local_label:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    :cond_3
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/AppsUtils;->showApps(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/16 v4, 0x9

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_nav_apps:I

    sget v6, Lcom/google/android/apps/plus/R$string;->apps_preference_category:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/16 v4, 0xa

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_search_24_white:I

    sget v6, Lcom/google/android/apps/plus/R$string;->search_activity_label:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->showDestinations()V

    return-void

    :cond_5
    move v1, v3

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_nav_profile:I

    sget v6, Lcom/google/android/apps/plus/R$string;->home_screen_profile_label:I

    invoke-virtual {v4, v2, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    goto/16 :goto_1

    :cond_7
    move v2, v3

    goto :goto_2
.end method

.method private handleServiceCallback$b5e9bbb(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    goto :goto_0
.end method

.method private static isLauncherIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private navigateToDestination(ILandroid/os/Bundle;Lvedroid/support/v4/app/Fragment$SavedState;Z)V
    .locals 11
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Lvedroid/support/v4/app/Fragment$SavedState;
    .param p4    # Z

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    :goto_0
    const/4 v5, 0x4

    if-ne p1, v5, :cond_0

    if-eqz v4, :cond_0

    const/4 p1, 0x0

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :pswitch_1
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto :goto_1

    :pswitch_2
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;-><init>()V

    const-string v5, "person_id"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "person_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto :goto_1

    :pswitch_3
    sget-object v5, Lcom/google/android/apps/plus/util/Property;->ENABLE_TILES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;-><init>()V

    :goto_2
    const-string v5, "person_id"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "person_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v5, "photos_home"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "photos_home"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_5
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/phone/HostedFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto :goto_1

    :cond_6
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;-><init>()V

    goto :goto_2

    :pswitch_4
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;-><init>()V

    const-string v5, "refresh"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v0, :cond_7

    :try_start_0
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;-><init>()V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    :cond_7
    if-eqz v0, :cond_1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v5, v6, :cond_1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommService;->getNotificationIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v5

    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_native_lib_error:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "showError: message=%s finishOnOk=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const v10, 0x1080027

    invoke-static {v7, v5, v8, v9, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v7, Lcom/google/android/apps/plus/phone/HomeActivity$2;

    invoke-direct {v7, p0, v6}, Lcom/google/android/apps/plus/phone/HomeActivity$2;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;Z)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "error"

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_6
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;-><init>(Z)V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto/16 :goto_1

    :pswitch_7
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;-><init>()V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto/16 :goto_1

    :pswitch_8
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;-><init>()V

    const-string v5, "refresh"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method private refreshNotifications()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    return-void
.end method

.method private saveDestinationState()V
    .locals 4

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->saveHostedFragmentState()Lvedroid/support/v4/app/Fragment$SavedState;

    move-result-object v2

    aput-object v2, v1, v0

    :cond_0
    return-void
.end method

.method private showCurrentDestination()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->dismissPopupMenus()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v4, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->navigateToDestination(ILandroid/os/Bundle;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lvedroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    goto :goto_1

    :cond_4
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private updateNotificationsSpinner()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showNotificationsProgressIndicator()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideNotificationsProgressIndicator()V

    goto :goto_0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_1
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/SignOnManager;->onActivityResult$6eb84b56(II)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/HostLayout;->onAttachFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V

    :cond_0
    instance-of v0, p1, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lvedroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNotificationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNotificationBar()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isTaskRoot()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "destination"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->isLauncherIntent(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v1, p1, v4}, Lcom/google/android/apps/plus/phone/SignOnManager;->onCreate(Landroid/os/Bundle;Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$layout;->home_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->setContentView(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->host:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/HostLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/HostLayout;->setListener(Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showNotificationsButton()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnNotificationButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    sget v4, Lcom/google/android/apps/plus/R$string;->main_menu_content_description:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->setUpButtonContentDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->getNavigationBar()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->getNotificationBar()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBar:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBar:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBar:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->registerExperimentListener(Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->configureDestinations()V

    if-eqz p1, :cond_7

    const-string v1, "reqId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "reqId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    const-string v2, "notificationMode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationsMode(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->attachActionBar()V

    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mUnreadNotificationCount:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationCount(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "show_notifications"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->showNotificationBarDelayed()V

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-eqz v1, :cond_9

    move v4, v2

    :goto_2
    if-nez v4, :cond_b

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncPreferenceSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_3
    if-eqz v1, :cond_b

    new-instance v1, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;-><init>(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "new_features"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move v4, v3

    goto :goto_2

    :cond_a
    move v1, v3

    goto :goto_3

    :cond_b
    if-nez v4, :cond_5

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v1, v4, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncPreferenceSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_c

    move v1, v2

    :goto_4
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getContactsSyncConfigActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_c
    move v1, v3

    goto :goto_4
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v4

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "timestamp DESC"

    move-object v1, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->host_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onDestroy()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->verifyEmpty()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->unregisterExperimentListener(Lcom/google/android/apps/plus/content/EsAccountsData$ExperimentListener;)V

    return-void
.end method

.method public final onExperimentsChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->configureDestinations()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getDestinationId(I)I

    move-result v1

    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x6

    if-ne v1, v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/util/MapUtils;->getPlacesActivityIntent$7ec49240()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xa

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x9

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AppsUtils;->getManageAppsIntent$3458983a(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->saveDestinationState()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    aget-object v0, v0, v1

    check-cast v0, Lvedroid/support/v4/app/Fragment$SavedState;

    invoke-direct {p0, v1, v2, v0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->navigateToDestination(ILandroid/os/Bundle;Lvedroid/support/v4/app/Fragment$SavedState;Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBar:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eq v0, v4, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_6
    const-string v0, "com.google.plus.analytics.intent.extra.START_VIEW"

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->setNotifications(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->getUnreadNotificationCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mUnreadNotificationCount:I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mUnreadNotificationCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationCount(I)V

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mFirstLoad:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->refreshNotifications()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mFirstLoad:Z

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onNavigationBarVisibilityChange(Z)V
    .locals 11
    .param p1    # Z

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    if-eqz p1, :cond_0

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->SHOW_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    invoke-static {p0, v1, v8, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object v0, p0

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_0
    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->HIDE_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object v0, p0

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->isLauncherIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    const-string v0, "show_notifications"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->showNotificationBarDelayed()V

    :cond_0
    return-void
.end method

.method public final onNotificationBarVisibilityChange(Z)V
    .locals 2
    .param p1    # Z

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationBarAdapter:Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostNotificationBarAdapter;->getUnreadNotificationCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->tellServerNotificationsWereRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationsMode(Z)V

    return-void
.end method

.method public final onNotificationRefreshButtonClick()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->refreshNotifications()V

    return-void
.end method

.method public final onNotificationsButtonClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->toggleNotificationBarVisibility()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, p1}, Lcom/google/android/apps/plus/views/HostLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v5, Lcom/google/android/apps/plus/R$id;->feedback:I

    if-ne v3, v5, :cond_1

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HomeActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    sget v5, Lcom/google/android/apps/plus/R$id;->settings:I

    if-ne v3, v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/apps/plus/phone/Intents;->getSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v3, v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->url_param_help_stream:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HomeActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    sget v5, Lcom/google/android/apps/plus/R$id;->sign_out:I

    if-ne v3, v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/SignOnManager;->signOut(Z)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v5, 0x1

    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/R$id;->feedback:I

    if-eq v2, v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->settings:I

    if-eq v2, v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->help:I

    if-eq v2, v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->sign_out:I

    if-ne v2, v4, :cond_1

    :cond_0
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/views/HostLayout;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    return v5
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->onResume()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->handleServiceCallback$b5e9bbb(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "sign_out"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->signOut(Z)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v3, Lcom/google/android/apps/plus/phone/HomeActivity;->REMOVE:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startExternalActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v4, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->configureDestinations()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "reqId"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "notificationMode"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->isNotificationsMode()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->saveDestinationState()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onStart()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->setSyncEnabled(Z)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onStop()V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->setSyncEnabled(Z)V

    return-void
.end method

.method public final onUpButtonClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onUpButtonClicked()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNotificationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNotificationBar()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->toggleNavigationBarVisibility()V

    goto :goto_0
.end method
