.class public Lcom/google/android/apps/plus/phone/OobIntents;
.super Ljava/lang/Object;
.source "OobIntents.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/phone/OobIntents;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mInitial:Z

.field private final mStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/phone/OobIntents$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/phone/OobIntents$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/OobIntents;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInitialIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;
    .param p4    # Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    invoke-static {p0, p1, v1, p2, p4}, Lcom/google/android/apps/plus/phone/Intents;->getOutOfBoxActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/OobIntents;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, p1, p3, v2}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    invoke-static {p0, p1, p3, v1}, Lcom/google/android/apps/plus/phone/OobIntents;->getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method private static getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/AccountSettingsData;
    .param p3    # Lcom/google/android/apps/plus/phone/OobIntents;

    iget v0, p3, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/Intents;->getOobSelectPlusPageActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/phone/Intents;->getOobProfilePhotoActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/phone/Intents;->getOobContactsSyncIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/phone/Intents;->getOobInstantUploadIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/AccountSettingsData;
    .param p3    # I

    const/4 v0, 0x5

    :goto_0
    packed-switch p3, :pswitch_data_0

    :goto_1
    :pswitch_0
    return v0

    :pswitch_1
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 p3, 0x1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->hasProfilePhoto()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenWarmWelcome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 p3, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_3
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->needContactSyncOob(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v0, 0x4

    goto :goto_1

    :cond_3
    const/4 p3, 0x4

    goto :goto_0

    :pswitch_4
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->needInstantUploadOob(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    const/4 p3, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getNextIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)Landroid/content/Intent;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget v2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-static {p1, p2, p3, v2}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    invoke-static {p1, p2, p3, v2}, Lcom/google/android/apps/plus/phone/OobIntents;->getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final isInitialIntent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    return v0
.end method

.method public final isLastIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
