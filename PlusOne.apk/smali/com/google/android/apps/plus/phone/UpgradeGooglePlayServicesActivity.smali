.class public Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "UpgradeGooglePlayServicesActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity$GooglePlayServicesErrorDialog;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity$GooglePlayServicesErrorDialog;

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity$GooglePlayServicesErrorDialog;-><init>(I)V

    const-string v3, "errorDialog"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/plus/phone/UpgradeGooglePlayServicesActivity$GooglePlayServicesErrorDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
