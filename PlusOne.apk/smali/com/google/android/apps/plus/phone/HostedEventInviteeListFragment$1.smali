.class final Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedEventInviteeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    # invokes: Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->access$000(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleAddRemoveGuestCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onGetEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    # invokes: Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleGetEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->access$100(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onGetEventInviteesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleGetEventInviteesCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;->this$0:Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleAddRemoveGuestCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method
