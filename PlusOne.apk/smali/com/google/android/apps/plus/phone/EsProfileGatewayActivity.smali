.class public abstract Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "EsProfileGatewayActivity.java"


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mHandler:Landroid/os/Handler;

.field protected mPendingRequestId:Ljava/lang/Integer;

.field protected mPersonId:Ljava/lang/String;

.field protected mPersonName:Ljava/lang/String;

.field protected mRedirected:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity$1;-><init>(Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method


# virtual methods
.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->finish()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->add_to_circle_confirmation_toast:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->finish()V

    :cond_1
    return-void

    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    const-string v2, "selected_circle_ids"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity$2;-><init>(Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_3

    const-string v2, "account"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "person_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonId:Ljava/lang/String;

    const-string v2, "person_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonName:Ljava/lang/String;

    const-string v2, "pending_req_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "pending_req_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "redirected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mRedirected:Z

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mRedirected:Z

    if-eqz v2, :cond_4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->finish()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->finish()V

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "person_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "person_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "pending_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "redirected"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mRedirected:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected final setCircleMembership(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonName:Ljava/lang/String;

    new-array v0, v8, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPendingRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->add_to_circle_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0, v8}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v6

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "EsProfileGatewayActivity"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsProfileGatewayActivity"

    const-string v1, "Cannot show dialog"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected final showCirclePicker()V
    .locals 6

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
