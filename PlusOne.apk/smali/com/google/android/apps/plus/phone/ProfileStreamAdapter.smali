.class public final Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
.super Lcom/google/android/apps/plus/phone/StreamAdapter;
.source "ProfileStreamAdapter.java"


# instance fields
.field private mBlockRequestPending:Z

.field mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field mCircleNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

.field private mErrorText:Ljava/lang/String;

.field mFamilyName:Ljava/lang/String;

.field mFullName:Ljava/lang/String;

.field mGender:Ljava/lang/String;

.field mGivenName:Ljava/lang/String;

.field mHasCoverPhotoUpgrade:Z

.field mHasFullBleedUpgrade:Z

.field mHasProfile:Z

.field mIsBlocked:Z

.field private mIsEditEnabled:Z

.field private mIsLocalPlusPage:Z

.field mIsMuted:Z

.field mIsMyProfile:Z

.field private mIsPlusPage:Z

.field mIsSmsIntentRegistered:Z

.field private mIsUnclaimedLocalPlusPage:Z

.field private mPackedCircleIds:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field mPlusOneByMe:Z

.field mPlusOnes:I

.field private mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

.field mProfileLoadFailed:Z

.field private mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

.field mScrapbookAlbumId:Ljava/lang/String;

.field mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

.field mScrapbookCoverPhotoId:Ljava/lang/String;

.field mScrapbookCoverPhotoOffset:I

.field mScrapbookCoverPhotoOwnerType:Ljava/lang/String;

.field mScrapbookCoverPhotoUrl:Ljava/lang/String;

.field mScrapbookFullBleedCoordinates:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

.field mScrapbookLayout:Ljava/lang/String;

.field mShowAddToCircles:Z

.field mShowBlocked:Z

.field mShowCircles:Z

.field mShowProgress:Z

.field private mViewIsExpanded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    return-void
.end method

.method private addDateInfo(Lcom/google/api/services/plusi/model/DateInfo;Ljava/lang/StringBuilder;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/DateInfo;
    .param p2    # Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v5, :cond_2

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v5

    if-eqz v5, :cond_2

    move v2, v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v5, :cond_3

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v5

    if-eqz v5, :cond_3

    :goto_2
    if-nez v2, :cond_4

    if-nez v1, :cond_4

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_5

    const-string v4, ", "

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v4, p1, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v2, :cond_8

    if-nez v1, :cond_6

    if-eqz v3, :cond_8

    :cond_6
    iget-object v4, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " - "

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_7

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_end_date_for_current:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_7
    iget-object v4, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    if-eqz v3, :cond_9

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_end_date_for_current:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_9
    if-eqz v2, :cond_a

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_a
    if-eqz v1, :cond_0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private bindProfileAboutView(Lcom/google/android/apps/plus/views/ProfileAboutView;)V
    .locals 62
    .param p1    # Lcom/google/android/apps/plus/views/ProfileAboutView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasCoverPhotoUpgrade:Z

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoLayout(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isCoverPhotoFromFeaturedAlbum()Z

    move-result v5

    if-nez v5, :cond_d

    const/4 v5, 0x1

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoUrl(Ljava/lang/String;ZZ)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v12, v5, Lcom/google/api/services/plusi/model/CommonContent;->photoUrl:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_13

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarUrl(Ljava/lang/String;Z)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFamilyName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/IntField;->value:Ljava/lang/Integer;

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/IntField;->value:Ljava/lang/Integer;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAddedByCount(Ljava/lang/Integer;)V

    :goto_4
    const/16 v42, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->address:Lcom/google/api/services/plusi/model/PlacePageAddressProto;

    if-nez v7, :cond_15

    const/16 v42, 0x0

    :goto_5
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocation(Ljava/lang/String;Z)V

    :cond_1
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_1d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    const/4 v7, -0x1

    if-eq v5, v7, :cond_1d

    sget v5, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    const/4 v10, 0x1

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setPlusOneData(Ljava/lang/String;Z)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->tagLine:Lcom/google/api/services/plusi/model/StringField;

    if-eqz v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->tagLine:Lcom/google/api/services/plusi/model/StringField;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/StringField;->value:Ljava/lang/String;

    move-object/from16 v56, v0

    :goto_8
    invoke-static/range {v56 .. v56}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1f

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setTagLine(Ljava/lang/String;)V

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasProfile:Z

    if-nez v5, :cond_2

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_not_on_google_plus:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    aput-object v9, v7, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v39

    :cond_2
    if-nez v39, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->introduction:Lcom/google/api/services/plusi/model/StringField;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->introduction:Lcom/google/api/services/plusi/model/StringField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/StringField;->value:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v39

    :cond_3
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setIntroduction(Ljava/lang/String;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    if-eqz v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    if-eqz v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_20

    const/16 v31, 0x1

    :goto_a
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v5, :cond_30

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-nez v7, :cond_21

    const/4 v12, 0x0

    :goto_b
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarUrl(Ljava/lang/String;Z)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    if-nez v7, :cond_22

    const/16 v49, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->placeInfo:Lcom/google/api/services/plusi/model/PlaceInfo;

    if-nez v7, :cond_24

    const/16 v46, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    move-object/from16 v2, v46

    move-object/from16 v3, v42

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalActions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v6, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    if-eqz v6, :cond_25

    const/4 v5, 0x1

    :goto_e
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalEditorialReviewsSection(Z)V

    if-eqz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatEditorialReview:Lcom/google/api/services/plusi/model/Description;

    if-nez v7, :cond_26

    const/4 v7, 0x0

    :goto_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceLabel(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceValue(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v10, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    if-eqz v10, :cond_27

    iget-object v10, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;->aggregatedReviews:Lcom/google/api/services/plusi/model/AggregatedReviewsProto;

    if-eqz v10, :cond_27

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;->aggregatedReviews:Lcom/google/api/services/plusi/model/AggregatedReviewsProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/AggregatedReviewsProto;->numReviews:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    :goto_10
    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalEditorialReviews(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    if-eqz v7, :cond_7

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/KnownForTermsProto;->term:Ljava/util/List;

    if-nez v7, :cond_28

    :cond_7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v40

    :goto_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getOpeningHoursSummary(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getOpeningHoursFull(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v47

    invoke-static/range {v48 .. v48}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-static/range {v47 .. v47}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_29

    :cond_8
    const/16 v34, 0x1

    :goto_12
    invoke-static/range {v49 .. v49}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface/range {v40 .. v40}, Ljava/util/List;->size()I

    move-result v5

    if-gtz v5, :cond_9

    if-eqz v34, :cond_2a

    :cond_9
    const/16 v32, 0x1

    :goto_13
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalDetailsSection(Z)V

    if-eqz v32, :cond_a

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move-object/from16 v2, v49

    move-object/from16 v3, v48

    move-object/from16 v4, v47

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalDetails(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->authorityPage:Lcom/google/api/services/plusi/model/AuthorityPageProto;

    if-nez v5, :cond_2b

    const/16 v41, 0x0

    :goto_14
    if-eqz v41, :cond_2c

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2c

    const/16 v30, 0x1

    :goto_15
    if-eqz v30, :cond_2d

    if-nez v31, :cond_2d

    const/16 v54, 0x1

    :goto_16
    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableHompageSection(Z)V

    if-eqz v54, :cond_b

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    move-object/from16 v0, v41

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "https://www.google.com/s2/u/0/favicons?domain="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    iget-object v9, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setHomepage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v36

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearAllReviews()V

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalYourActivitySection(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-result-object v61

    if-eqz v61, :cond_c

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addYourReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalYourCirclesActivitySection(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_17
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2e

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v51

    check-cast v51, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addCircleReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    goto :goto_17

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasFullBleedUpgrade:Z

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoLayout(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOffset:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoFullBleedOffset(I)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isCoverPhotoFromFeaturedAlbum()Z

    move-result v5

    if-nez v5, :cond_f

    const/4 v5, 0x1

    :goto_18
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoUrl(Ljava/lang/String;ZZ)V

    goto/16 :goto_2

    :cond_f
    const/4 v5, 0x0

    goto :goto_18

    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v55

    move/from16 v0, v55

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v59, v0

    const/16 v37, 0x0

    :goto_19
    move/from16 v0, v37

    move/from16 v1, v55

    if-ge v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    move/from16 v0, v37

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    aput-object v5, v59, v37

    add-int/lit8 v37, v37, 0x1

    goto :goto_19

    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-virtual {v0, v5, v1, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setScrapbookAlbumUrls(Ljava/lang/Long;[Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_12
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoToDefault(Z)V

    goto/16 :goto_2

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarToDefault(Z)V

    goto/16 :goto_3

    :cond_14
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAddedByCount(Ljava/lang/Integer;)V

    goto/16 :goto_4

    :cond_15
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->address:Lcom/google/api/services/plusi/model/PlacePageAddressProto;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/PlacePageAddressProto;->addressLine:Ljava/util/List;

    if-eqz v7, :cond_16

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_17

    :cond_16
    const/16 v42, 0x0

    goto/16 :goto_5

    :cond_17
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v5, 0x0

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    const/4 v9, 0x1

    if-le v5, v9, :cond_18

    const-string v5, "\n"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const/4 v5, 0x1

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_18
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v42

    goto/16 :goto_5

    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_1

    const/16 v20, 0x0

    const/16 v43, 0x0

    const/16 v53, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/api/services/plusi/model/Employment;

    if-eqz v21, :cond_1a

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    move-object/from16 v20, v0

    :cond_1a
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEmployer(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-eqz v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    move-object/from16 v43, v0

    :cond_1b
    const/4 v5, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocation(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/Education;

    if-eqz v18, :cond_1c

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    move-object/from16 v53, v0

    :cond_1c
    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEducation(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_1d
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setPlusOneData(Ljava/lang/String;Z)V

    goto/16 :goto_7

    :cond_1e
    const/16 v56, 0x0

    goto/16 :goto_8

    :cond_1f
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setTagLine(Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_20
    const/16 v31, 0x0

    goto/16 :goto_a

    :cond_21
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v12, v5, Lcom/google/api/services/plusi/model/CommonContent;->photoUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_22
    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PhoneProto;->phoneNumber:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_23

    const/16 v49, 0x0

    goto/16 :goto_c

    :cond_23
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PhoneProto;->phoneNumber:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/PlacePagePhoneNumber;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/PlacePagePhoneNumber;->formattedPhone:Ljava/lang/String;

    move-object/from16 v49, v0

    goto/16 :goto_c

    :cond_24
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->placeInfo:Lcom/google/api/services/plusi/model/PlaceInfo;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/PlaceInfo;->clusterId:Ljava/lang/String;

    move-object/from16 v46, v0

    goto/16 :goto_d

    :cond_25
    const/4 v5, 0x0

    goto/16 :goto_e

    :cond_26
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatEditorialReview:Lcom/google/api/services/plusi/model/Description;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/Description;->text:Ljava/lang/String;

    goto/16 :goto_f

    :cond_27
    const/4 v10, 0x0

    goto/16 :goto_10

    :cond_28
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/KnownForTermsProto;->term:Ljava/util/List;

    move-object/from16 v40, v0

    goto/16 :goto_11

    :cond_29
    const/16 v34, 0x0

    goto/16 :goto_12

    :cond_2a
    const/16 v32, 0x0

    goto/16 :goto_13

    :cond_2b
    iget-object v0, v5, Lcom/google/api/services/plusi/model/AuthorityPageProto;->authorityLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    move-object/from16 v41, v0

    goto/16 :goto_14

    :cond_2c
    const/16 v30, 0x0

    goto/16 :goto_15

    :cond_2d
    const/16 v54, 0x0

    goto/16 :goto_16

    :cond_2e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v52

    invoke-interface/range {v52 .. v52}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2f

    const/4 v5, 0x1

    :goto_1a
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalReviewsSection(Z)V

    invoke-interface/range {v52 .. v52}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_1b
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_30

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v51

    check-cast v51, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocalReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    goto :goto_1b

    :cond_2f
    const/4 v5, 0x0

    goto :goto_1a

    :cond_30
    const/16 v27, 0x0

    const/16 v35, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-nez v5, :cond_35

    const/4 v13, 0x0

    :goto_1c
    if-eqz v13, :cond_31

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    if-eqz v5, :cond_36

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_36

    const/16 v27, 0x1

    :goto_1d
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    if-eqz v5, :cond_37

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_37

    const/16 v35, 0x1

    :goto_1e
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    if-eqz v5, :cond_38

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_38

    const/16 v22, 0x1

    :cond_31
    :goto_1f
    if-nez v27, :cond_32

    if-nez v35, :cond_32

    if-eqz v22, :cond_39

    :cond_32
    const/16 v25, 0x1

    :goto_20
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableContactSection(Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEmails()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearPhoneNumbers()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearAddresses()V

    if-eqz v27, :cond_3a

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_21
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3a

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/TaggedEmail;

    const/16 v58, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v5, :cond_33

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForEmailType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v58

    :cond_33
    if-nez v58, :cond_34

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_email:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v58

    :cond_34
    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addEmail(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_21

    :cond_35
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v13, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    goto/16 :goto_1c

    :cond_36
    const/16 v27, 0x0

    goto :goto_1d

    :cond_37
    const/16 v35, 0x0

    goto :goto_1e

    :cond_38
    const/16 v22, 0x0

    goto :goto_1f

    :cond_39
    const/16 v25, 0x0

    goto :goto_20

    :cond_3a
    if-eqz v35, :cond_3d

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_22
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3d

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lcom/google/api/services/plusi/model/TaggedPhone;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_3c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v49

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPlusPagePhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v58

    :goto_23
    if-nez v58, :cond_3b

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_phone:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v58

    :cond_3b
    move-object/from16 v0, v49

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsSmsIntentRegistered:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v5, v1, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addPhoneNumber(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_22

    :cond_3c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v49

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v58

    goto :goto_23

    :cond_3d
    if-eqz v22, :cond_40

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_24
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_40

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/TaggedAddress;

    const/16 v58, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v5, :cond_3e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    iget-object v7, v11, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForAddress(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v58

    :cond_3e
    if-nez v58, :cond_3f

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_address:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v58

    :cond_3f
    iget-object v5, v11, Lcom/google/api/services/plusi/model/TaggedAddress;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addAddress(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_24

    :cond_40
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateContactSectionDividers()V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v5, :cond_43

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    if-eqz v5, :cond_45

    const-string v5, "UNKNOWN"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_45

    const-string v5, "OTHER"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_45

    const/16 v29, 0x1

    :goto_25
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_46

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    if-eqz v5, :cond_46

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/BirthdayField;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_46

    const/16 v23, 0x1

    :goto_26
    if-nez v29, :cond_41

    if-eqz v23, :cond_47

    :cond_41
    const/4 v5, 0x1

    :goto_27
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enablePersonalSection(Z)V

    if-eqz v29, :cond_49

    const-string v60, ""

    const-string v5, "MALE"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_48

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_gender_male:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v60

    :cond_42
    :goto_28
    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setGender(Ljava/lang/String;)V

    :goto_29
    if-eqz v23, :cond_4a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/BirthdayField;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setBirthday(Ljava/lang/String;)V

    :goto_2a
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updatePersonalSectionDividers()V

    :cond_43
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_4b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    if-eqz v5, :cond_4b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v5, :cond_4b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4b

    const/16 v28, 0x1

    :goto_2b
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEmploymentLocations()V

    if-eqz v28, :cond_4c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    move-object/from16 v45, v0

    invoke-interface/range {v45 .. v45}, Ljava/util/List;->size()I

    move-result v55

    const/16 v37, 0x0

    :goto_2c
    move/from16 v0, v37

    move/from16 v1, v55

    if-ge v0, v1, :cond_4d

    move-object/from16 v0, v45

    move/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/google/api/services/plusi/model/Employment;

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->title:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_44

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->title:Ljava/lang/String;

    move-object/from16 v0, v57

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_44
    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->addDateInfo(Lcom/google/api/services/plusi/model/DateInfo;Ljava/lang/StringBuilder;)V

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addEmploymentLocation(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v37, v37, 0x1

    goto :goto_2c

    :cond_45
    const/16 v29, 0x0

    goto/16 :goto_25

    :cond_46
    const/16 v23, 0x0

    goto/16 :goto_26

    :cond_47
    const/4 v5, 0x0

    goto/16 :goto_27

    :cond_48
    const-string v5, "FEMALE"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_42

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_gender_female:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v60

    goto/16 :goto_28

    :cond_49
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setGender(Ljava/lang/String;)V

    goto/16 :goto_29

    :cond_4a
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setBirthday(Ljava/lang/String;)V

    goto/16 :goto_2a

    :cond_4b
    const/16 v28, 0x0

    goto/16 :goto_2b

    :cond_4c
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v5, :cond_4d

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setNoEmploymentLocations()V

    :cond_4d
    if-nez v28, :cond_4e

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    if-eqz v5, :cond_50

    :cond_4e
    const/4 v5, 0x1

    :goto_2d
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableWorkSection(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_51

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    if-eqz v5, :cond_51

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v5, :cond_51

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_51

    const/16 v26, 0x1

    :goto_2e
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEducationLocations()V

    if-eqz v26, :cond_52

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    move-object/from16 v44, v0

    invoke-interface/range {v44 .. v44}, Ljava/util/List;->size()I

    move-result v55

    const/16 v37, 0x0

    :goto_2f
    move/from16 v0, v37

    move/from16 v1, v55

    if-ge v0, v1, :cond_53

    move-object/from16 v0, v44

    move/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/google/api/services/plusi/model/Education;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Education;->majorConcentration:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4f

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Education;->majorConcentration:Ljava/lang/String;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v14}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->addDateInfo(Lcom/google/api/services/plusi/model/DateInfo;Ljava/lang/StringBuilder;)V

    :cond_4f
    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addEducationLocation(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v37, v37, 0x1

    goto :goto_2f

    :cond_50
    const/4 v5, 0x0

    goto/16 :goto_2d

    :cond_51
    const/16 v26, 0x0

    goto :goto_2e

    :cond_52
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v5, :cond_53

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setNoEducationLocations()V

    :cond_53
    if-nez v26, :cond_54

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    if-eqz v5, :cond_58

    :cond_54
    const/4 v5, 0x1

    :goto_30
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableEducationSection(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_59

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-eqz v5, :cond_59

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_55

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-eqz v5, :cond_59

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_59

    :cond_55
    const/16 v33, 0x1

    :goto_31
    if-eqz v33, :cond_5a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->locationMapUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocationUrl(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearLocations()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    if-eqz v5, :cond_56

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_56

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocation(Ljava/lang/String;Z)V

    :cond_56
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-eqz v5, :cond_5b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :cond_57
    :goto_32
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5b

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_57

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocation(Ljava/lang/String;Z)V

    goto :goto_32

    :cond_58
    const/4 v5, 0x0

    goto/16 :goto_30

    :cond_59
    const/16 v33, 0x0

    goto :goto_31

    :cond_5a
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v5, :cond_5b

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocationUrl(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setNoLocations()V

    :cond_5b
    if-nez v33, :cond_5c

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    if-eqz v5, :cond_5f

    :cond_5c
    const/4 v5, 0x1

    :goto_33
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocationsSection(Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateLocationsSectionDividers()V

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLinksSection(Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearLinks()V

    if-eqz v31, :cond_60

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :cond_5d
    :goto_34
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_60

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/google/api/services/plusi/model/ProfilesLink;

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->url:Ljava/lang/String;

    if-eqz v5, :cond_5d

    const/16 v58, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_5e

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_website:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v58

    :cond_5e
    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->url:Ljava/lang/String;

    move-object/from16 v0, v41

    iget-object v7, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->label:Ljava/lang/String;

    move-object/from16 v0, v41

    iget-object v8, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->faviconImgUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v5, v7, v8, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_34

    :cond_5f
    const/4 v5, 0x0

    goto :goto_33

    :cond_60
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateLinksSectionDividers()V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    if-eqz v5, :cond_63

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNames:Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCircles(Ljava/util/ArrayList;)V

    :goto_35
    sget-object v5, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v16

    if-eqz v16, :cond_68

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    if-eqz v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DeviceLocations;->deviceLocation:Ljava/util/List;

    if-eqz v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DeviceLocations;->deviceLocation:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_67

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DeviceLocations;->deviceLocation:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/DeviceLocation;

    if-eqz v5, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/DeviceLocation;->lat:Ljava/lang/Double;

    if-eqz v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/DeviceLocation;->lng:Ljava/lang/Double;

    if-eqz v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/DeviceLocation;->mapImageUrlMobile:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_67

    iget-object v7, v5, Lcom/google/api/services/plusi/model/DeviceLocation;->displayName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_67

    move-object v15, v5

    :goto_36
    if-eqz v15, :cond_69

    iget-object v0, v15, Lcom/google/api/services/plusi/model/DeviceLocation;->mapImageUrlMobile:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_37
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearDeviceLocationContent()V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v5, :cond_61

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v7, :cond_6a

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    if-eqz v7, :cond_6a

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DeviceLocations;->sharingEnabled:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v5

    if-eqz v5, :cond_6a

    const/4 v5, 0x1

    :goto_38
    if-nez v5, :cond_6b

    const/4 v15, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDeviceLocationNotShared()V

    :cond_61
    :goto_39
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDeviceLocationInHeader(Lcom/google/api/services/plusi/model/DeviceLocation;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v15}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDeviceLocationUrl(Ljava/lang/String;Lcom/google/api/services/plusi/model/DeviceLocation;)V

    if-eqz v16, :cond_6c

    if-nez v15, :cond_62

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    if-eqz v5, :cond_6c

    :cond_62
    const/4 v5, 0x1

    :goto_3a
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDeviceLocationSection(Z)V

    goto/16 :goto_0

    :cond_63
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    if-eqz v5, :cond_64

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showAddToCircles(Z)V

    goto/16 :goto_35

    :cond_64
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    if-eqz v5, :cond_65

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showBlocked()V

    goto/16 :goto_35

    :cond_65
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    if-eqz v5, :cond_66

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showProgress()V

    goto/16 :goto_35

    :cond_66
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showNone()V

    goto/16 :goto_35

    :cond_67
    const/4 v15, 0x0

    goto :goto_36

    :cond_68
    const/4 v15, 0x0

    goto :goto_36

    :cond_69
    const/16 v17, 0x0

    goto :goto_37

    :cond_6a
    const/4 v5, 0x0

    goto :goto_38

    :cond_6b
    if-nez v15, :cond_61

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDeviceLocationNotAvailable()V

    goto :goto_39

    :cond_6c
    const/4 v5, 0x0

    goto :goto_3a
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isCoverPhotoFromFeaturedAlbum()Z
    .locals 2

    const-string v0, "GALLERY"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOwnerType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final beginBlockInProgress()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "ProfileAdapter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ProfileAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindView(); "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ProfileAboutView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->init(ZZ)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileLoadFailed:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mErrorText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showError(ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    invoke-direct {v1}, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;-><init>()V

    sget-object v2, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-eqz v2, :cond_1

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDisplayPolicies(Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->bindProfileAboutView(Lcom/google/android/apps/plus/views/ProfileAboutView;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V

    :cond_2
    :goto_1
    return-void

    :pswitch_0
    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showExpandButtonText:Z

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final endBlockInProgress(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public final getEducationList()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "{}"

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/EducationsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getEmploymentList()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "{}"

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/EmploymentsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getFullName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGender()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    return-object v0
.end method

.method public final getGivenName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getPlacesLivedList()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "{}"

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/api/services/plusi/model/LocationsJson;->getInstance()Lcom/google/api/services/plusi/model/LocationsJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/LocationsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getScrapbookAlbumId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getScrapbookCoverPhotoCoordinates()Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookFullBleedCoordinates:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    return-object v0
.end method

.method public final getScrapbookCoverPhotoId()Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getScrapbookCoverPhotoOffset()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOffset:I

    return v0
.end method

.method public final getScrapbookCoverPhotoOwnerId()Ljava/lang/String;
    .locals 2

    const-string v0, "GALLERY"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOwnerType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "115239603441691718952"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getScrapbookCoverPhotoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getScrapbookLayout()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    return-object v0
.end method

.method public final getScrapbookPhotoId(I)Ljava/lang/Long;
    .locals 5
    .param p1    # I

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v2, v3, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-nez p1, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v1, v3, Lcom/google/api/services/plusi/model/ScrapBookEntry;->photoId:Ljava/lang/String;

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v4, p1, -0x1

    if-le v3, v4, :cond_0

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v1, v3, Lcom/google/api/services/plusi/model/ScrapBookEntry;->photoId:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final getSharingRosterData()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->rosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "{}"

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->getInstance()Lcom/google/api/services/plusi/model/SharingRosterDataJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->rosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getStreamItemViewType(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/16 v0, 0xd

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getStreamItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getViewIsExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final hasCoverPhotoUpgrade()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasCoverPhotoUpgrade:Z

    return v0
.end method

.method public final hasFullBleedPhotoUpgrade()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasFullBleedUpgrade:Z

    return v0
.end method

.method public final init(Ljava/lang/String;ZZZLcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    iput-boolean p3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasProfile:Z

    iput-boolean p4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsSmsIntentRegistered:Z

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-void
.end method

.method public final isBlocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    return v0
.end method

.method public final isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMuted:Z

    return v0
.end method

.method public final isPlusOnedByMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    return v0
.end method

.method public final isPlusPage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    return v0
.end method

.method public final newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x2

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_about_fragment:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ProfileAboutView;

    new-instance v3, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    if-lt v4, v2, :cond_2

    :goto_0
    iput v2, v3, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_0
    const-string v2, "ProfileAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "ProfileAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "newView() -> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_1
.end method

.method public final setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    return-void
.end method

.method public final setProfileData(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    iget-object v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    const-string v7, "USER"

    iput-object v7, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    const-string v7, "e:"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    new-instance v7, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v7, Lcom/google/api/services/plusi/model/Contacts;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/Contacts;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/TaggedEmail;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TaggedEmail;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    if-eqz v4, :cond_4

    const-string v4, "USER"

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Name;->given:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Name;->family:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFamilyName:Ljava/lang/String;

    :cond_3
    iput-boolean v5, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    :cond_4
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_13

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    :goto_3
    iget-boolean v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->blocked:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    iget-object v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->packedCircleIds:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SocialGraphData;->muted:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_15

    move v4, v6

    :goto_4
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMuted:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-eqz v3, :cond_6

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookAlbumId:Ljava/lang/String;

    :cond_5
    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    if-eqz v4, :cond_6

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;->photoId:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoId:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoUrl:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;->cropUrl:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

    if-nez v4, :cond_6

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    if-eqz v4, :cond_6

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_6

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoUrl:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapBookEntry;->cropUrl:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoCropUrl:Ljava/lang/String;

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->layout:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    const-string v4, "COVER"

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasCoverPhotoUpgrade:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasCoverPhotoUpgrade:Z

    if-nez v4, :cond_7

    const-string v4, "FULL_BLEED"

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookLayout:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    :cond_7
    move v4, v6

    :goto_5
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasFullBleedUpgrade:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookFullBleedCoordinates:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->offset:Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->offset:Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;->top:Ljava/lang/Integer;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOffset:I

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->photoOwnerType:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mScrapbookCoverPhotoOwnerType:Ljava/lang/String;

    :cond_9
    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v4, :cond_17

    const-string v4, "OTHER"

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    :goto_6
    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v4, :cond_19

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v4, :cond_19

    :goto_7
    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsEditEnabled:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    const-string v7, "p:"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    new-instance v7, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v7, Lcom/google/api/services/plusi/model/Contacts;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/Contacts;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v4, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    new-instance v1, Lcom/google/api/services/plusi/model/TaggedPhone;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/TaggedPhone;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_b
    const-string v4, "PLUSPAGE"

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v4, :cond_d

    move v4, v5

    :goto_8
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    const-string v7, "UNCLAIMED"

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/LocalEntityInfo;->type:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    :cond_c
    iget-object v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/Page;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    iget-object v4, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    goto/16 :goto_2

    :cond_d
    const-string v7, "PLUSPAGE"

    iget-object v8, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_e

    move v4, v5

    goto :goto_8

    :cond_e
    iget-object v7, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    if-nez v7, :cond_f

    move v4, v5

    goto :goto_8

    :cond_f
    const-string v7, "LOCAL"

    iget-object v8, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Page;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    move v4, v5

    goto :goto_8

    :cond_10
    iget-object v7, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    if-eqz v7, :cond_11

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    if-nez v4, :cond_12

    :cond_11
    move v4, v5

    goto :goto_8

    :cond_12
    move v4, v6

    goto :goto_8

    :cond_13
    iget-object v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_14

    iget-object v4, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    goto/16 :goto_3

    :cond_14
    sget v4, Lcom/google/android/apps/plus/R$string;->profile_unknown_name:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    goto/16 :goto_3

    :cond_15
    move v4, v5

    goto/16 :goto_4

    :cond_16
    move v4, v5

    goto/16 :goto_5

    :cond_17
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Gender;->value:Ljava/lang/String;

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Gender;->value:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    goto/16 :goto_6

    :cond_18
    const-string v4, "UNKNOWN"

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    goto/16 :goto_6

    :cond_19
    move v6, v5

    goto/16 :goto_7
.end method

.method public final setViewIsExpanded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    return-void
.end method

.method public final showError(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileLoadFailed:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mErrorText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final updateCircleList()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    goto :goto_0

    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNameListForPackedIds(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNames:Ljava/util/ArrayList;

    goto :goto_0
.end method
