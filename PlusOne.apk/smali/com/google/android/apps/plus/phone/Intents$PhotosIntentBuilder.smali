.class public final Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
.super Ljava/lang/Object;
.source "Intents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/Intents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotosIntentBuilder"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumType:Ljava/lang/String;

.field private mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mAuthkey:Ljava/lang/String;

.field private mCropMode:Ljava/lang/Integer;

.field private mEventId:Ljava/lang/String;

.field private mGaiaId:Ljava/lang/String;

.field private mHideAlbumSpinner:Ljava/lang/Boolean;

.field private mHideCameraVideos:Ljava/lang/Boolean;

.field private mHideShareAction:Ljava/lang/Boolean;

.field private final mIntent:Landroid/content/Intent;

.field private mIsExternal:Ljava/lang/Boolean;

.field private mMediaRefUserMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mNotificationId:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPhotoPickerMediaRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoPickerMode:Ljava/lang/Integer;

.field private mPhotoPickerTitleResourceId:Ljava/lang/Integer;

.field private mPhotosHome:Ljava/lang/Boolean;

.field private mShowCameraAlbum:Ljava/lang/Boolean;

.field private mStreamId:Ljava/lang/String;

.field private mTakePhoto:Ljava/lang/Boolean;

.field private mTakeVideo:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Class;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/content/Intent;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Account must be set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIsExternal:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIsExternal:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    if-eqz v2, :cond_2

    const-string v2, "album_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string v2, "album_name"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, "album_type"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_5

    const-string v2, "event_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mEventId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_6

    const-string v2, "mediarefs"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    if-eqz v2, :cond_7

    const-string v2, "notif_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_8

    const-string v2, "owner_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    if-eqz v2, :cond_9

    const-string v2, "person_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v2, :cond_a

    const-string v2, "photos_of_user_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_b

    const-string v2, "show_photo_only"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    const-string v2, "photos_home"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    const-string v2, "photos_show_camera_album"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    if-eqz v2, :cond_e

    const-string v2, "stream_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    const-string v2, "photo_picker_mode"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    const-string v2, "photo_picker_title"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v2, "photo_picker_selected"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mCropMode:Ljava/lang/Integer;

    if-eqz v2, :cond_12

    const-string v2, "photo_picker_crop_mode"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mCropMode:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideCameraVideos:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    const-string v2, "hide_camera_videos"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideCameraVideos:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAuthkey:Ljava/lang/String;

    if-eqz v2, :cond_14

    const-string v2, "auth_key"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAuthkey:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakePhoto:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_15

    const-string v2, "take_photo"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakePhoto:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakeVideo:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v2, "take_video"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakeVideo:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_16
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefUserMap:Ljava/util/Map;

    if-eqz v2, :cond_17

    const-string v3, "mediaref_user_map"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefUserMap:Ljava/util/Map;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_17
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v2, :cond_18

    const-string v2, "audience"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_18
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideShareAction:Ljava/lang/Boolean;

    if-eqz v2, :cond_19

    const-string v2, "hide_share_action"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideShareAction:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_19
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideAlbumSpinner:Ljava/lang/Boolean;

    if-eqz v2, :cond_1a

    const-string v2, "hide_album_spinner"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideAlbumSpinner:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1a
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v2
.end method

.method public final setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object p0
.end method

.method public final setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    return-object p0
.end method

.method public final setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    return-object p0
.end method

.method public final setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    return-object p0
.end method

.method public final setAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-object p0
.end method

.method public final setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAuthkey:Ljava/lang/String;

    return-object p0
.end method

.method public final setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mCropMode:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mEventId:Ljava/lang/String;

    return-object p0
.end method

.method public final setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIsExternal:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    return-object p0
.end method

.method public final setHideAlbumSpinner(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideAlbumSpinner:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setHideCameraVideos(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideCameraVideos:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setHideShareAction(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mHideShareAction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setMediaRefUserMap(Ljava/util/Map;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;",
            ">;>;)",
            "Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;"
        }
    .end annotation

    instance-of v0, p1, Ljava/io/Serializable;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefUserMap:Ljava/util/Map;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mediaRefUserMap must be serializable!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # [Lcom/google/android/apps/plus/api/MediaRef;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    return-object p0
.end method

.method public final setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    return-object p0
.end method

.method public final setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    return-object p0
.end method

.method public final setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    return-object p0
.end method

.method public final setPhotoPickerInitiallySelected(Ljava/util/ArrayList;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)",
            "Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    return-object p0
.end method

.method public final setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    return-object p0
.end method

.method public final setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    return-object p0
.end method

.method public final setTakePhoto(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakePhoto:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final setTakeVideo(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mTakeVideo:Ljava/lang/Boolean;

    return-object p0
.end method
