.class public Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;
.super Lvedroid/support/v4/app/FragmentStatePagerAdapter;
.source "PhotoComposePagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mContext:Landroid/content/Context;

.field private final mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

.field private final mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/app/FragmentManager;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p4    # Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;
    .param p5    # Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    invoke-direct {p0, p2}, Lvedroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Lvedroid/support/v4/app/FragmentManager;)V

    if-nez p5, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MediaRefProvider was null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;->getCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Lvedroid/support/v4/app/Fragment;
    .locals 5
    .param p1    # I

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoComposeFragmentIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    if-ltz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    invoke-interface {v4}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;->getCount()I

    move-result v4

    if-lt p1, v4, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    invoke-interface {v4, p1}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;->getItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;

    invoke-direct {v3}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->setRemoveImageListener(Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;)V

    goto :goto_0
.end method

.method public final getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->mMediaRefProvider:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;->getItemPosition(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
