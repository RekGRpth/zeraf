.class public Lcom/google/android/apps/plus/phone/LicenseActivity;
.super Landroid/app/Activity;
.source "LicenseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->license_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/LicenseActivity;->setContentView(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/LicenseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/licenses.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
