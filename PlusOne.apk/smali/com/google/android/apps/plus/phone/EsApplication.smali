.class public Lcom/google/android/apps/plus/phone/EsApplication;
.super Landroid/app/Application;
.source "EsApplication.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field public static sMemoryClass:I


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private sSystemUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/EsApplication;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsApplication;->sSystemUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsApplication;->sSystemUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->loadExperiments(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/plus/phone/EsApplication$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/phone/EsApplication$1;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsApplication$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_2

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->init(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_view_default_title:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->setPhotosFromPostsAlbumName(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->IS_AUTOMATION_BUILD:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-class v2, Lcom/google/android/apps/plus/R;

    invoke-static {v2}, Lcom/google/android/picasasync/R;->init(Ljava/lang/Class;)V

    const-class v2, Lcom/google/android/apps/plus/service/PicasaNetworkReceiver;

    invoke-static {v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->setNetworkReceiver(Ljava/lang/Class;)V

    const-class v2, Lcom/google/android/apps/plus/service/PicasaNetworkReceiver;

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->setNetworkReceiver(Ljava/lang/Class;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    goto :goto_0
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5
    .param p1    # Ljava/lang/Thread;
    .param p2    # Ljava/lang/Throwable;

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v1, p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    const-string v1, "EsApplication"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EsApplication"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Uncaught exception in background thread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    invoke-static {}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->isDatabaseRecentlyDeleted()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "EsApplication"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "EsApplication"

    const-string v2, "An account has just been deactivated, which put background threads at a risk of failure. Letting this thread live."

    invoke-static {v1, v2, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsApplication;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_4

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsApplication;->mHandler:Landroid/os/Handler;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsApplication;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/phone/EsApplication$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/plus/phone/EsApplication$2;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;Ljava/lang/Thread;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsApplication;->sSystemUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v1, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
