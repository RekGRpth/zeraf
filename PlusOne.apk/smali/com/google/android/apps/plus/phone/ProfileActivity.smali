.class public Lcom/google/android/apps/plus/phone/ProfileActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "ProfileActivity.java"


# instance fields
.field private mCurrentSpinnerIndex:I

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    return-void
.end method

.method public static createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 2
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v1, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_posts_tab_text:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_photos_tab_text:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static getFragmentForPosition(I)Lcom/google/android/apps/plus/phone/HostedFragment;
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;-><init>()V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;-><init>()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getFragmentForPosition(I)Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    return-object v0
.end method

.method protected final getExtrasForLogging()Landroid/os/Bundle;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "person_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "extra_gaia_id"

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    return-void
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/app/Fragment;

    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    if-eqz v2, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->relinquishPrimarySpinner()V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Lvedroid/support/v4/app/Fragment;)V

    return-void

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->relinquishPrimarySpinner()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "profile_view_type"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "notif_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    return-void

    :pswitch_0
    iput v4, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onPrimarySpinnerSelectionChange(I)V

    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getFragmentForPosition(I)Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->replaceFragment(Lvedroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "spinnerIndex"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "spinnerIndex"

    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
