.class public final Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "AlbumGridViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;,
        Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;
    }
.end annotation


# static fields
.field private static sDefaultFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;


# instance fields
.field private final mAlbumType:Ljava/lang/String;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

.field private final mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mHasDisabledPhotos:Ljava/lang/Boolean;

.field private final mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mSelectedMediaRefs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->sDefaultFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Landroid/view/View$OnLongClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    sget-object v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->sDefaultFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    iput-object p7, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mAlbumType:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$2;-><init>(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;)V

    invoke-virtual {p4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    return-object v0
.end method

.method private createMediaRef(Ljava/lang/String;JLcom/google/android/apps/plus/api/MediaRef$MediaType;Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mAlbumType:Ljava/lang/String;

    const-string v1, "camera_photos"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object v1, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v5, 0x0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto :goto_0
.end method

.method private static getMediaTypeForRow(Landroid/database/Cursor;)Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    .locals 1
    .param p0    # Landroid/database/Cursor;

    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xc

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_0
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object/from16 v7, p1

    check-cast v7, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    const/16 v1, 0xe

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    invoke-interface {v1, v13}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;->getState(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    sget v1, Lcom/google/android/apps/plus/R$string;->photo_in_list_count:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v15, v9, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v5, v14

    const/4 v14, 0x1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v5, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->tag_position:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v1, v5}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(ILjava/lang/Object;)V

    const/16 v1, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v1, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/16 v1, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaTypeForRow(Landroid/database/Cursor;)Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v5

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->createMediaRef(Ljava/lang/String;JLcom/google/android/apps/plus/api/MediaRef$MediaType;Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {v7, v11}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(Ljava/lang/Object;)V

    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v12, 0x0

    :goto_1
    if-lez v12, :cond_2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setPlusOneCount(Ljava/lang/Integer;)V

    :goto_2
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v8, 0x0

    :goto_3
    if-lez v8, :cond_4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setCommentCount(Ljava/lang/Integer;)V

    :goto_4
    const/16 v1, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setNotification(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;->select(I)V

    :goto_6
    new-instance v10, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v1, 0x2

    const/4 v5, -0x3

    invoke-direct {v10, v1, v5}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCount()I

    move-result v1

    if-ge v9, v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v9}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$3;-><init>(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;I)V

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    goto/16 :goto_1

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setPlusOneCount(Ljava/lang/Integer;)V

    goto/16 :goto_2

    :cond_3
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    goto/16 :goto_3

    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setCommentCount(Ljava/lang/Integer;)V

    goto/16 :goto_4

    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;->deselect(I)V

    goto :goto_6

    :cond_7
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 7
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x8

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaTypeForRow(Landroid/database/Cursor;)Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v4

    const/16 v0, 0x9

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->createMediaRef(Ljava/lang/String;JLcom/google/android/apps/plus/api/MediaRef$MediaType;Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTimestampForItem(I)J
    .locals 3
    .param p1    # I

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isAnyPhotoDisabled()Z
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mHasDisabledPhotos:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mHasDisabledPhotos:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v2, 0xe

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;->getState(I)I

    move-result v1

    if-ne v1, v3, :cond_2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mHasDisabledPhotos:Ljava/lang/Boolean;

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mHasDisabledPhotos:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->album_column_grid_view_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v3, :cond_1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onResume()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->onResume()V

    :cond_1
    return-void
.end method

.method public final onStop()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onStop()V

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onStop()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final setSelectedMediaRefs(Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    return-void
.end method

.method public final setStateFilter(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->sDefaultFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mFilter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;

    goto :goto_0
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mHasDisabledPhotos:Ljava/lang/Boolean;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
