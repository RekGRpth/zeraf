.class public final Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;
.super Landroid/widget/BaseAdapter;
.source "TimeZoneSpinnerAdapter.java"


# static fields
.field private static stimeZoneFormat:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

.field private mTimeZones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->stimeZoneFormat:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->time_zone_format:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->stimeZoneFormat:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private prepareRow(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Z

    if-nez p2, :cond_0

    if-eqz p4, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v6, Lcom/google/android/apps/plus/R$layout;->timezone_spinner_dropdown_item:I

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v6, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    :goto_0
    move-object/from16 v0, p2

    instance-of v6, v0, Landroid/widget/TextView;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZones:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getOffset()J

    move-result-wide v2

    move-object/from16 v6, p2

    check-cast v6, Landroid/widget/TextView;

    sget-object v7, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->stimeZoneFormat:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v4}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-wide/32 v10, 0x36ee80

    div-long v10, v2, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-wide/32 v10, 0x36ee80

    div-long v10, v2, v10

    const-wide/32 v12, 0x36ee80

    mul-long/2addr v10, v12

    sub-long v10, v2, v10

    const-wide/32 v12, 0xea60

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2

    :cond_2
    new-instance p2, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-direct {v0, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZones:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->prepareRow(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZones:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->prepareRow(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final setTimeZoneHelper(Lcom/google/android/apps/plus/util/TimeZoneHelper;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/util/TimeZoneHelper;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZoneInfos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->mTimeZones:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->notifyDataSetChanged()V

    return-void
.end method
