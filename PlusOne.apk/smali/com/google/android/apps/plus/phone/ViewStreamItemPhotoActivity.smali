.class public Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "ViewStreamItemPhotoActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/FragmentActivity;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTIVITY_RESULT_PROJECTION:[Ljava/lang/String;

.field private static final STREAM_ITEM_PHOTO_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "raw_contact_source_id"

    aput-object v1, v0, v2

    const-string v1, "stream_item_photo_sync1"

    aput-object v1, v0, v3

    const-string v1, "stream_item_photo_sync2"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->STREAM_ITEM_PHOTO_PROJECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v2

    const-string v1, "activity_id"

    aput-object v1, v0, v3

    const-string v1, "embed_media"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->ACTIVITY_RESULT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->STREAM_ITEM_PHOTO_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->ACTIVITY_RESULT_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->finish()V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "account"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "stream_item_uri"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const-string v2, "account"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "stream_item_uri"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    new-instance v2, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V

    return-object v2
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 10
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    if-eqz v2, :cond_3

    array-length v3, v2

    if-eqz v3, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v2

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v7}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    :goto_0
    invoke-virtual {v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->finish()V

    return-void

    :cond_1
    invoke-virtual {v9, v7}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoOnly(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    goto :goto_0

    :cond_2
    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v7, v1, v0}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    invoke-static {p0, v7, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    invoke-static {p0, v7, v1, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_5
    sget v0, Lcom/google/android/apps/plus/R$string;->profile_does_not_exist:I

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
