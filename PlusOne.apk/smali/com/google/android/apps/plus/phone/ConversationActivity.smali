.class public Lcom/google/android/apps/plus/phone/ConversationActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "ConversationActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;
.implements Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;
.implements Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationQuery;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsQuery;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragmentActivity;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;",
        "Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;",
        "Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sInstanceCount:I


# instance fields
.field private final conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdvancedHangoutsEnabled:Z

.field private mCheckExtraTile:Z

.field private mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

.field private mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

.field private mConversationId:Ljava/lang/String;

.field private mConversationName:Ljava/lang/String;

.field private mConversationRowId:Ljava/lang/Long;

.field private mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

.field private mCreateConversationRequestId:I

.field private mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

.field private mEarliestEventTimestamp:J

.field private mFirstEventTimestamp:J

.field private mFirstHangoutMenuItemIndex:I

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mIsConversationLoaded:Z

.field private mIsGroup:Z

.field private mIsMuted:Z

.field private mLastHangoutMenuItemIndex:I

.field private mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

.field private mNeedToInviteParticipants:Z

.field private mParticipantCount:I

.field private mParticipantList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private final mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mRootView:Landroid/view/View;

.field private mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private mTileContainer:Landroid/widget/LinearLayout;

.field private mTileSelectorMenuItem:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/ConversationActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCreateConversationRequestId:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/phone/ConversationActivity;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCreateConversationRequestId:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/ConversationActivity;I)V
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;
    .param p1    # I

    const/4 v3, -0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    const-wide v5, 0x3fe999999999999aL

    mul-double/2addr v3, v5

    cmpg-double v0, v1, v3

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getHeight()I

    move-result v2

    invoke-virtual {v1, v7, v2, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    return-object v0
.end method

.method public static hasInstance()Z
    .locals 1

    sget v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initHangoutTile()V
    .locals 12

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const-string v3, "c:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v10, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    if-eqz v1, :cond_5

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreate(Landroid/os/Bundle;)V

    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v11, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v11}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileContainer:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStart()V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    :cond_2
    :goto_2
    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const-string v3, "messenger"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Messenger:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v6, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v5, v0

    move v8, v7

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setHangoutInfo(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;Ljava/util/ArrayList;ZZ)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    if-eqz v1, :cond_0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->shouldShowHangoutTile()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-eq v1, v2, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v1, v2, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    goto :goto_0

    :catch_0
    move-exception v9

    const-string v1, "Could not load hangout native library"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/LinkageError;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setInnerActionBarEnabled(Z)Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    goto :goto_1

    :cond_6
    sget v1, Lcom/google/android/apps/plus/R$menu;->conversation_activity_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    goto :goto_2
.end method

.method private initialize()V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v3, "initialize"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "is_group"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    sget-boolean v3, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    const-string v3, "conversation_row_id"

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v3, v0, v8

    if-eqz v3, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    :cond_1
    const-string v3, "participant"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v3, :cond_4

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setConversationLabel(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    new-instance v4, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getParticipantsGalleryView()Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v7, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ConversationTile;->setConversationRowId(Ljava/lang/Long;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    goto :goto_0
.end method

.method private inviteMoreParticipants()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v6, :cond_1

    move v0, v1

    :goto_1
    invoke-static {p0, v3, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ParticipantHelper;->inviteMoreParticipants(Landroid/app/Activity;Ljava/util/Collection;ZLcom/google/android/apps/plus/content/EsAccount;Z)V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    :goto_2
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    goto :goto_2
.end method

.method private prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;
    .locals 8
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_toggle_tile_menu_item:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v6, v7, :cond_0

    move v2, v4

    :goto_0
    if-eqz v2, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_speech_bubble:I

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    if-nez v2, :cond_3

    iget v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstHangoutMenuItemIndex:I

    :goto_2
    iget v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mLastHangoutMenuItemIndex:I

    if-ge v1, v4, :cond_3

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    move v2, v5

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_menu_hangout:I

    goto :goto_1

    :cond_2
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4

    :goto_3
    return-object v3

    :cond_4
    const/4 v3, 0x0

    goto :goto_3
.end method

.method private setConversationLabel(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setTitlebarTitle(Ljava/lang/String;)V

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private shouldShowHangoutTile()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tile"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "tile"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSubtitle()V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->participant_count:I

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final blockPerson(Ljava/io/Serializable;)V
    .locals 1
    .param p1    # Ljava/io/Serializable;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->blockPerson(Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method

.method public final displayParticipantsInTray()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-ne v1, v2, :cond_2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ConversationTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ConversationTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V

    goto :goto_0
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getGreenRoomParticipantListActivityIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getParticipantListActivityIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getHangoutNotificationIntent()Landroid/content/Intent;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getConversationActivityHangoutTileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipantListActivityIntent()Landroid/content/Intent;
    .locals 7

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/Intents;->getParticipantListActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method

.method public final hideInsertCameraPhotoDialog()V
    .locals 1

    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->dismissDialog(I)V

    return-void
.end method

.method public final leaveConversation()V
    .locals 9

    const/4 v2, 0x0

    sget-boolean v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const-string v3, "messenger"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Messenger:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v7, 0x0

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v8}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    :cond_1
    return-void
.end method

.method protected final needsAsyncData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 6
    .param p1    # Lvedroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    new-instance v1, Lcom/google/android/apps/plus/phone/ConversationActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ConversationActivity$2;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setListener(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setLeaveConversationListener(Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setConversationInfo(Ljava/lang/String;JJ)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setParticipantList(Ljava/util/HashMap;)V

    goto :goto_1

    :cond_4
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    sget-boolean v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final onBlockCompleted(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_1:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/Hangout;->isAdvancedUiSupported(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    new-instance v1, Lcom/google/android/apps/plus/phone/ConversationActivity$1;

    invoke-direct {v1, p0, p0}, Lcom/google/android/apps/plus/phone/ConversationActivity$1;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$layout;->conversation_activity:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setContentView(Landroid/view/View;)V

    const-string v1, "ConversationActivity.onCreate"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->tile_container:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileContainer:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/apps/plus/R$id;->conversation_tile:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ConversationTile;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ConversationTile;->addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    sget v1, Lcom/google/android/apps/plus/R$menu;->conversation_activity_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initialize()V

    sget v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-le v1, v4, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConversationActivity onCreate instanceCount out of sync: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v0, 0x7f0a003e

    if-ne p1, v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConversationActivity.onCreateLoader: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    if-ne p1, v5, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "participant_id!=? AND active=1"

    new-array v5, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    const-string v6, "first_name ASC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$menu;->conversation_activity_menu:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_toggle_tile_menu_item:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstHangoutMenuItemIndex:I

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mLastHangoutMenuItemIndex:I

    const/4 v2, 0x1

    return v2
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    sget v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConversationActivity onDestroy instanceCount out of sync: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v2, :cond_8

    if-eqz p2, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    const/4 v1, 0x6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setConversationLabel(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->updateSubtitle()V

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/phone/ConversationActivity$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity$3;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;I)V

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->inviteMoreParticipants()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setConversationInfo(Ljava/lang/String;JJ)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initHangoutTile()V

    :cond_5
    :goto_2
    return-void

    :cond_6
    move v1, v3

    goto :goto_0

    :cond_7
    move v1, v3

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->updateSubtitle()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    :goto_3
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_9
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initHangoutTile()V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v5, v6, :cond_d

    :goto_4
    invoke-static {p0, v0, v1, v4, v2}, Lcom/google/android/apps/plus/phone/ParticipantHelper;->inviteMoreParticipants(Landroid/app/Activity;Ljava/util/Collection;ZLcom/google/android/apps/plus/content/EsAccount;Z)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setParticipantList(Ljava/util/HashMap;)V

    goto/16 :goto_2

    :cond_d
    move v2, v3

    goto :goto_4
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onMeetingMediaStarted()V
    .locals 7

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "AUTHOR_PROFILE_ID"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const-string v3, "com.google.hangouts"

    const/4 v4, 0x0

    const-string v5, "JOIN_HANGOUT"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendTileEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)I

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setIntent(Landroid/content/Intent;)V

    const-string v0, "ConversationActivity.onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initialize()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->reinitialize()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    move v1, v2

    :goto_0
    return v1

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_mute_menu_item:I

    if-ne v0, v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v1, v4, v5, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "conversation_is_muted"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    :goto_2
    move v1, v3

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_unmute_menu_item:I

    if-ne v0, v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v1, v4, v5, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "conversation_is_muted"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_3

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_leave_menu_item:I

    if-ne v0, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->displayLeaveConversationDialog()V

    goto :goto_2

    :cond_6
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_toggle_tile_menu_item:I

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    goto :goto_2

    :cond_7
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_edit_name_menu_item:I

    if-ne v0, v1, :cond_8

    new-instance v1, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "rename_conversation"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    sget v1, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    if-ne v0, v1, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->inviteMoreParticipants()V

    goto :goto_2

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 1

    const-string v0, "ConversationActivity.onPause"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTilePause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPause()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_mute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_unmute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_edit_name_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_leave_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_toggle_tile_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_mute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_unmute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_edit_name_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_leave_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileSelectorMenuItem:Landroid/view/MenuItem;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_toggle_tile_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_invite_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_mute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_unmute_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_edit_name_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->realtimechat_conversation_leave_menu_item:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    const-string v0, "ConversationActivity.onResume"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->isIntentAccountActive()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setAllowSendMessage(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->connectAndStayConnected(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->finish()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    const-string v0, "ConversationActivity.onStart"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStart()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->requestFocus()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStop()V

    const-string v0, "ConversationActivity.onStop"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileStop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStop()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setCurrentConversationRowId(Ljava/lang/Long;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->shouldShowHangoutTile()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->markConversationRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setCurrentConversationRowId(Ljava/lang/Long;)V

    goto :goto_0
.end method

.method public final showInsertCameraPhotoDialog()V
    .locals 1

    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showDialog(I)V

    return-void
.end method

.method public final stopHangoutTile()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    :cond_0
    return-void
.end method

.method public final toggleTiles()V
    .locals 5

    sget-boolean v3, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v3, v4, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Lcom/google/android/apps/plus/views/Tile;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/Tile;->onTilePause()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/Tile;->onTileStop()V

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/Tile;->setVisibility(I)V

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/Tile;->onTileStart()V

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/Tile;->onTileResume()V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileSelectorMenuItem:Landroid/view/MenuItem;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v3, v4, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_speech_bubble:I

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileSelectorMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    goto :goto_1

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_hangout:I

    goto :goto_2

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$menu;->conversation_activity_menu:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    goto :goto_0
.end method
