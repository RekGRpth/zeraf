.class public Lcom/google/android/apps/plus/phone/LocationPickerActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "LocationPickerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mLocationPickerFragment:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;-><init>()V

    return-object v0
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->location_picker_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Lvedroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->mLocationPickerFragment:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->mLocationPickerFragment:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->setSearchMode(Z)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->mLocationPickerFragment:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->mLocationPickerFragment:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onBackPressed()V

    :cond_1
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->getHostActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->initForDarkTheme()V

    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->location_provider_disabled:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1bfb7a8
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final onUpButtonClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PICK"

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/LocationPickerActivity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onUpButtonClick()V

    goto :goto_0
.end method
