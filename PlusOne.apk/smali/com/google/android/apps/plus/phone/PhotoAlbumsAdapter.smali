.class public final Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PhotoAlbumsAdapter.java"


# instance fields
.field private final mImageViews:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/views/ImageResourceView;",
            ">;"
        }
    .end annotation
.end field

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p4    # Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;)V

    invoke-virtual {p3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 15
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    sget v10, Lcom/google/android/apps/plus/R$id;->photo:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v10, Lcom/google/android/apps/plus/R$id;->title:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    sget v10, Lcom/google/android/apps/plus/R$id;->count:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDefaultIconEnabled(Z)V

    const/16 v10, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_0

    const/16 v10, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v11, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v10, v6, v11}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    const/16 v10, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-eqz v10, :cond_1

    sget v10, Lcom/google/android/apps/plus/R$string;->photos_home_unknown_label:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/google/android/apps/plus/R$plurals;->album_photo_count:I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v5, v13, v14

    invoke-virtual {v10, v11, v12, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    new-instance v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v10, 0x2

    const/4 v11, -0x3

    invoke-direct {v4, v10, v11}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    const/16 v10, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    :cond_2
    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$layout;->photo_home_view_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onResume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onStop()V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoAlbumsAdapter;->mImageViews:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onStop()V

    goto :goto_0

    :cond_0
    return-void
.end method
