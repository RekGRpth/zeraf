.class public final Lcom/google/android/apps/plus/phone/SquareStreamAdapter;
.super Lcom/google/android/apps/plus/phone/StreamAdapter;
.source "SquareStreamAdapter.java"


# instance fields
.field private mCanInvite:Z

.field private mCanJoin:Z

.field private mCanRequestToJoin:Z

.field private mCanSeeMembers:Z

.field private mCanShare:Z

.field private mDisableSubscription:Z

.field private mIsMember:Z

.field private mJoinability:I

.field private mMemberCount:I

.field private mMembershipStatus:I

.field private mNotificationsEnabled:Z

.field private mSquareAboutText:Ljava/lang/String;

.field private mSquareDetailsViewOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

.field private mSquareName:Ljava/lang/String;

.field private mSquarePhotoUrl:Ljava/lang/String;

.field private mSquareTagline:Ljava/lang/String;

.field private mViewIsExpanded:Ljava/lang/Boolean;

.field private mVisibility:I

.field private mVolume:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    invoke-direct/range {p0 .. p13}, Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibleIndex:I

    return-void
.end method

.method private getLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;
    .locals 3

    const/4 v1, 0x2

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    if-lt v2, v1, :cond_0

    :goto_0
    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    return-object v0

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bindStreamHeaderView$23b74339(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;

    const/4 v1, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x5

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    if-nez v6, :cond_a

    const-string v6, "SquareStreamAdapter"

    invoke-static {v6, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "SquareStreamAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bindView(); "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SquareLandingView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mViewIsExpanded:Ljava/lang/Boolean;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v6

    invoke-virtual {v0, v6, v5}, Lcom/google/android/apps/plus/views/SquareLandingView;->init(ZZ)V

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setSquareName(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquarePhotoUrl:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setSquarePhoto(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMemberCount:I

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setSquareMemberCount(I)V

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareAboutText:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setSquareAboutText(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibility:I

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setSquareVisibility(I)V

    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanSeeMembers:Z

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/SquareLandingView;->setMemberVisibility(Z)V

    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-eqz v6, :cond_5

    move v1, v2

    :cond_1
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SquareLandingView;->updateJoinButton(I)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanInvite:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanShare:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/views/SquareLandingView;->updateActions(ZZ)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SquareLandingView;->showSettingsSection(Z)V

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    if-ne v1, v2, :cond_2

    move v5, v3

    :cond_2
    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/SquareLandingView;->showDeclineInvitation(Z)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mDisableSubscription:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mNotificationsEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SquareLandingView;->setIsSubscribe(Z)V

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-nez v1, :cond_9

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibility:I

    if-ne v1, v3, :cond_9

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mJoinability:I

    if-ne v1, v3, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->showBlockingExplanation()V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareDetailsViewOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SquareLandingView;->setOnClickListener(Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    iget v6, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    if-eq v6, v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    if-ne v1, v2, :cond_6

    const/4 v1, 0x2

    goto :goto_0

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanJoin:Z

    if-eqz v1, :cond_7

    move v1, v3

    goto :goto_0

    :cond_7
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanRequestToJoin:Z

    if-eqz v1, :cond_8

    move v1, v4

    goto :goto_0

    :cond_8
    move v1, v5

    goto :goto_0

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->hideBlockingExplanation()V

    goto :goto_1

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public final bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->isSquareAdmin()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setSquareMode(ZZ)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public final getJoinability()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mJoinability:I

    return v0
.end method

.method public final getMembershipStatus()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    return v0
.end method

.method public final getPhotoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquarePhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getSettings()Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVolume:I

    if-eqz v4, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVolume:I

    :goto_1
    new-instance v4, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mDisableSubscription:Z

    if-nez v5, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mNotificationsEnabled:Z

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;-><init>(ZIZZ)V

    return-object v4

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public final getStreamHeaderViewType(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/16 v0, 0xe

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0xd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final getTagline()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareTagline:Ljava/lang/String;

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final getVisibility()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibility:I

    return v0
.end method

.method public final hasSquareData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->hasSquareData()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSquareAdmin()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newStreamHeaderView$4b8874c5(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    const/4 v3, 0x0

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    if-nez v2, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$layout;->square_landing_view:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SquareLandingView;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SquareLandingView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_0
    const-string v2, "SquareStreamAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "SquareStreamAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "newView() -> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$layout;->square_cant_see_posts:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public final resetAnimationState()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibleIndex:I

    return-void
.end method

.method public final setOnClickListener(Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareDetailsViewOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    return-void
.end method

.method public final setSquareData(Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareName:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquarePhotoUrl:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareTagline:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mSquareAboutText:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMemberCount:I

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mMembershipStatus:I

    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVisibility:I

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mJoinability:I

    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanJoin:Z

    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanRequestToJoin:Z

    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanSeeMembers:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanInvite:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mCanShare:Z

    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mNotificationsEnabled:Z

    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mDisableSubscription:Z

    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mVolume:I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mViewIsExpanded:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mIsMember:Z

    if-nez v0, :cond_9

    :goto_8
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mViewIsExpanded:Ljava/lang/Boolean;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->notifyDataSetChanged()V

    return-void

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v0, v2

    goto :goto_7

    :cond_9
    move v1, v2

    goto :goto_8
.end method

.method public final setViewIsExpanded(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->mViewIsExpanded:Ljava/lang/Boolean;

    return-void
.end method
