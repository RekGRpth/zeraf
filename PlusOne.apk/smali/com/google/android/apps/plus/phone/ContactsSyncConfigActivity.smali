.class public Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "ContactsSyncConfigActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Lvedroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020019

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->commit()Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->oob_contacts_sync_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->setContentView(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->bottom_bar:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/BottomActionBar;

    const v1, 0x1020019

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_done:I

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(IILandroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->showTitlebar(Z)V

    sget v1, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;->setTitlebarTitle(Ljava/lang/String;)V

    return-void
.end method
