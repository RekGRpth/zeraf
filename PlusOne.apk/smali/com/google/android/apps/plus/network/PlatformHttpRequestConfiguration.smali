.class public final Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;
.super Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;
.source "PlatformHttpRequestConfiguration.java"


# instance fields
.field private final mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    return-void
.end method


# virtual methods
.method public final addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 2
    .param p1    # Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V

    const-string v0, "X-Container-Url"

    iget-object v1, p0, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getContainerUrl(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected final getUserAgentHeader(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "; G+ SDK/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "1.0.0"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
