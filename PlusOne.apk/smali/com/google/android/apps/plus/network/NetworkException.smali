.class public final Lcom/google/android/apps/plus/network/NetworkException;
.super Ljava/io/IOException;
.source "NetworkException.java"


# static fields
.field private static final serialVersionUID:J = -0x238301abc50f5ac7L


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/network/NetworkException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    return-void
.end method
