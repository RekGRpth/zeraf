.class final Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;
.super Ljava/lang/Object;
.source "UploadsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/UploadsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UploadTaskProvider"
.end annotation


# static fields
.field private static final MEDIA_RECORD_TABLE_NAME:Ljava/lang/String;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->MEDIA_RECORD_TABLE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mAccountManager:Landroid/accounts/AccountManager;

    return-void
.end method

.method public static onSyncStart()V
    .locals 2

    const-string v0, "iu.UploadTaskProvider"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.UploadTaskProvider"

    const-string v1, "onSyncStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$200(Lcom/google/android/apps/plus/iu/UploadsManager;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method


# virtual methods
.method public final getNextTask(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;
    .locals 13
    .param p1    # Ljava/lang/String;

    const/16 v12, 0x21

    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    :goto_0
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getNextMediaRecord(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mContext:Landroid/content/Context;

    const/4 v7, -0x1

    invoke-static {v4, p1, v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->enqueueMediaRecords(Landroid/content/Context;Ljava/lang/String;I)Z

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getNextMediaRecord(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_2

    const/4 v1, 0x0

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getUploadAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "iu.UploadTaskProvider"

    invoke-static {v4, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "iu.UploadTaskProvider"

    const-string v7, "-- SYNC skip; no upload account"

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/16 v4, 0x12c

    invoke-virtual {v0, v4, v12}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v4, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v7

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v7}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v4, v7, v0}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v7, "com.google"

    invoke-virtual {v4, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    array-length v8, v7

    move v4, v6

    :goto_2
    if-ge v4, v8, :cond_7

    aget-object v9, v7, v4

    iget-object v9, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    move v4, v5

    :goto_3
    if-nez v4, :cond_8

    const-string v4, "iu.UploadTaskProvider"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "iu.UploadTaskProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "invalid account, remove all uploads in DB: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->MEDIA_RECORD_TABLE_NAME:Ljava/lang/String;

    const-string v8, "upload_account = ?"

    new-array v9, v5, [Ljava/lang/String;

    aput-object v2, v9, v6

    invoke-virtual {v4, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    move v4, v6

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getUploadReason()I

    move-result v3

    if-nez v3, :cond_a

    const-string v4, "iu.UploadTaskProvider"

    invoke-static {v4, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "iu.UploadTaskProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "-- SYNC skip; bad upload reason; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const/16 v4, 0x12c

    invoke-virtual {v0, v4, v12}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v4, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v7

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v7}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v4, v7, v0}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto/16 :goto_0

    :cond_a
    new-instance v1, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->isImage()Z

    move-result v5

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;-><init>(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;IZ)V

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->isExternalStorageMounted()Z
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$500()Z

    move-result v4

    if-eqz v4, :cond_b

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$600(Lcom/google/android/apps/plus/iu/UploadsManager;)Z

    move-result v4

    if-nez v4, :cond_d

    :cond_b
    const-string v4, "iu.UploadTaskProvider"

    invoke-static {v4, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "iu.UploadTaskProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "--- SYNC skip: no storage; task: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    goto/16 :goto_1

    :cond_d
    const-string v4, "iu.UploadTaskProvider"

    invoke-static {v4, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "iu.UploadTaskProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "--- SYNC upload; task: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", account: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
