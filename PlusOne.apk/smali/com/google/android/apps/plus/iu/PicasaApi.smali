.class public final Lcom/google/android/apps/plus/iu/PicasaApi;
.super Ljava/lang/Object;
.source "PicasaApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;
    }
.end annotation


# instance fields
.field private final mBaseUrl:Ljava/lang/String;

.field private final mClient:Lcom/google/android/apps/plus/iu/GDataClient;

.field private final mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/GDataClient$Operation;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    new-instance v1, Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/iu/GDataClient;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "picasa_gdata_base_url"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "https://picasaweb.google.com/data/feed/api/"

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mBaseUrl:Ljava/lang/String;

    return-void
.end method

.method private getUploadedPhotos$67a1bd15(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/iu/PhotoCollectorJson;)I
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/iu/PhotoCollectorJson;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x3

    const-string v7, "PicasaApi.getUploadedPhotos"

    invoke-static {v7}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    const-string v7, "iu.PicasaAPI"

    invoke-static {v7, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "iu.PicasaAPI"

    const-string v8, "get uploaded photos for %s etag %s"

    new-array v9, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v3

    aget-object v10, p2, v3

    aput-object v10, v9, v4

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    const/4 v7, 0x0

    aget-object v7, p2, v7

    iput-object v7, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-virtual {v7, p3, v1}, Lcom/google/android/apps/plus/iu/GDataClient;->get(Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataClient$Operation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v7, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outStatus:I

    sparse-switch v7, :sswitch_data_0

    const-string v3, "iu.PicasaAPI"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "iu.PicasaAPI"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getUploadedPhotos failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :try_start_2
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const-string v3, "iu.PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "iu.PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v6

    :goto_0
    return v3

    :sswitch_0
    const/4 v4, 0x0

    :try_start_3
    iget-object v5, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    aput-object v5, p2, v4

    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-virtual {p4, v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->parse(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const-string v4, "iu.PicasaAPI"

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "iu.PicasaAPI"

    const-string v5, "   done"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    goto :goto_0

    :sswitch_1
    :try_start_5
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const-string v3, "iu.PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "iu.PicasaAPI"

    const-string v5, "   done"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v4

    goto :goto_0

    :sswitch_2
    :try_start_6
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    const-string v3, "iu.PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "iu.PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v5

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_7
    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v3
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catch_0
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->handleInterrruptedException(Ljava/lang/Throwable;)Z

    const-string v3, "iu.PicasaAPI"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "iu.PicasaAPI"

    const-string v4, "getUploadedPhotos failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_6
    const-string v3, "iu.PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "iu.PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v6

    goto/16 :goto_0

    :catchall_1
    move-exception v3

    const-string v4, "iu.PicasaAPI"

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "iu.PicasaAPI"

    const-string v5, "   done"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v3

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x130 -> :sswitch_1
        0x191 -> :sswitch_2
        0x193 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final getUploadedPhotos(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)I
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;

    const/4 v6, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "user/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v8, "@gmail."

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "@googlemail."

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    const/16 v8, 0x40

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    invoke-virtual {v5, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :cond_1
    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "?max-results=1000&imgmax=d&thumbsize=640u&visibility=visible&v=4&alt=json&kind=photo"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "&streamid=camera_sync_created"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;

    invoke-direct {v1, p3}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;-><init>(Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)V

    const/4 v3, 0x1

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "&start-index="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, p2, v4, v1}, Lcom/google/android/apps/plus/iu/PicasaApi;->getUploadedPhotos$67a1bd15(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/iu/PhotoCollectorJson;)I

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v2

    :cond_2
    iget v5, v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->entryCount:I

    if-eqz v5, :cond_3

    iget v5, v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->entryCount:I

    add-int/2addr v3, v5

    const/4 v5, 0x0

    aput-object v5, p2, v6

    goto :goto_0

    :cond_3
    const-string v5, "iu.PicasaAPI"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "iu.PicasaAPI"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "total entry count="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v2, v6

    goto :goto_1
.end method

.method public final setAuthToken(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/iu/GDataClient;->setAuthToken(Ljava/lang/String;)V

    return-void
.end method
