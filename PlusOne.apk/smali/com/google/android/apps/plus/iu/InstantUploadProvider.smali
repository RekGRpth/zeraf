.class public Lcom/google/android/apps/plus/iu/InstantUploadProvider;
.super Landroid/content/ContentProvider;
.source "InstantUploadProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;
    }
.end annotation


# static fields
.field private static final MEDIA_RECORD_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MEDIA_RECORD_TABLE:Ljava/lang/String;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_ID:[Ljava/lang/String;

.field private static final SETTINGS_LOCK:Ljava/lang/Object;

.field private static final SETTING_DEFAULTS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTING_DEPRECATED:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final UPLOAD_TASK_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final UPLOAD_TASK_TABLE:Ljava/lang/String;

.field private static sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_ID:[Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "upload_account"

    const-string v2, "account"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "bytes_total"

    const-string v2, "bytes_total"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "bytes_uploaded"

    const-string v2, "bytes_uploaded"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "media_url"

    const-string v2, "content_uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "fingerprint"

    const-string v2, "fingerprint"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "upload_state"

    const-string v2, "state"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    const-string v1, "media_record_id"

    const-string v2, "media_record_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "upload_account"

    const-string v2, "upload_account"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "bytes_total"

    const-string v2, "bytes_total"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "bytes_uploaded"

    const-string v2, "bytes_uploaded"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "media_url"

    const-string v2, "media_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "fingerprint"

    const-string v2, "fingerprint"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "upload_state"

    const-string v2, "upload_state"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v1, "media_id"

    const-string v2, "media_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_name"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_type"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_enabled"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "video_upload_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_roaming"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_battery"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_eventid"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_starttime"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_endtime"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "upload_full_resolution"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_upload_state"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_state"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "upload_all_state"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "manual_upload_state"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "quota_limit"

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "quota_used"

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "full_size_disabled"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "sync_photo_on_mobile"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;)Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    return-object v0
.end method

.method private cancelUploads(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 13
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v12

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_ID:[Ljava/lang/String;

    const/4 v6, 0x0

    const-string v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    const/4 v8, 0x0

    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v12, v10, v11}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTask(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return v8

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static disableInstantShare(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    const-string v1, "com.google.android.picasasync.instant_share_eventid"

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private queryPhotos(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 29
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v20

    const-string v4, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v28

    new-instance v26, Landroid/content/SyncResult;

    invoke-direct/range {v26 .. v26}, Landroid/content/SyncResult;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->syncUploadedPhotos(Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;Lcom/google/android/apps/plus/iu/UserEntry;)V

    new-instance v24, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "uri"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "photoid"

    aput-object v6, v4, v5

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v0, v4, [J

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    invoke-static {v14}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    move-object/from16 v12, p2

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    aget-object v23, v12, v21

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    const/4 v15, 0x0

    :try_start_0
    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v19

    sget-object v4, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_ID:[Ljava/lang/String;

    const-string v6, "fingerprint_hash=? AND user_id=?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    move-object/from16 v0, v28

    iget-wide v9, v0, Lcom/google/android/apps/plus/iu/UserEntry;->id:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v23, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v15, :cond_1

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    :catch_0
    move-exception v17

    :try_start_1
    const-string v4, "iu.IUProvider"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "iu.IUProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "problem retrieving photo id for uri:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    if-eqz v15, :cond_1

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v4

    if-eqz v15, :cond_3

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v4

    :cond_4
    return-object v24
.end method

.method private querySettings(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 18
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;

    const-string v11, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    sget-object v12, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v12

    :try_start_0
    new-instance v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p2

    array-length v11, v0

    new-array v2, v11, [Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v5, 0x0

    move-object/from16 v0, p2

    array-length v8, v0

    :goto_0
    if-ge v5, v8, :cond_9

    aget-object v7, p2, v5

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v2, v5

    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    new-instance v11, Ljava/lang/IllegalArgumentException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "unknown column: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v11

    monitor-exit v12

    throw v11

    :cond_2
    :try_start_1
    const-string v11, "quota_limit"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    const-string v11, "quota_used"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    const-string v11, "full_size_disabled"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_3
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_5

    if-nez v1, :cond_5

    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v13, "Must specify account for quota details"

    invoke-direct {v11, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    :cond_5
    if-nez v4, :cond_6

    if-eqz v6, :cond_7

    :cond_6
    const/4 v4, 0x1

    :goto_3
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "com.google.android.picasasync."

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v6, :cond_8

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "."

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_4
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v2, v5

    aget-object v11, v2, v5

    if-nez v11, :cond_0

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v2, v5

    goto/16 :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    :cond_8
    const-string v11, ""

    goto :goto_4

    :cond_9
    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz v4, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    sget-object v13, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v14, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    if-eqz v14, :cond_a

    sget-object v14, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    invoke-virtual {v14}, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->getAccount()Ljava/lang/String;

    move-result-object v14

    invoke-static {v1, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_b

    :cond_a
    new-instance v14, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    invoke-direct {v14, v11, v1}, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v14, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0xb

    if-ge v11, v14, :cond_d

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Void;

    const/4 v15, 0x0

    const/16 v16, 0x0

    aput-object v16, v14, v15

    invoke-virtual {v11, v14}, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_b
    :goto_5
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_c
    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v3

    :cond_d
    :try_start_4
    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    sget-object v14, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Void;

    const/16 v16, 0x0

    const/16 v17, 0x0

    aput-object v17, v15, v16

    invoke-virtual {v11, v14, v15}, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception v11

    :try_start_5
    monitor-exit v13

    throw v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private queryUploads(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private removeMedia$5508efd6(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v14

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_MAP:Ljava/util/Map;

    sget-object v5, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_ID:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "media_record_id IN ( SELECT _id FROM "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " WHERE "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " )"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-virtual {v14, v12, v13}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTask(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v11, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    if-lez v15, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->MEDIA_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    return v15
.end method

.method static updateQuotaSettings(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    const-wide/16 v6, -0x1

    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    iget-wide v3, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaLimit:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_0

    const-string v3, "quota_limit"

    iget-wide v4, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaLimit:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-wide v3, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaUsed:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_1

    const-string v3, "quota_used"

    iget-wide v4, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaUsed:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v4, "full_size_disabled"

    iget-boolean v3, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->disableFullRes:Z

    if-eqz v3, :cond_4

    const-string v3, "1"

    :goto_0
    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "iu.IUProvider"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "iu.IUProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Update quota settings; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {p0, p1, v2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateSettings(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.apps.plus.iu.QUOTA_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "quota_limit"

    iget-wide v4, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaLimit:J

    long-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "quota_used"

    iget-wide v4, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaUsed:J

    long-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "full_size_disabled"

    iget-boolean v4, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->disableFullRes:Z

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_3
    return-void

    :cond_4
    const-string v3, "0"

    goto :goto_0
.end method

.method private static updateSettings(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v1, 0x0

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v10

    sget-object v12, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v12

    :try_start_0
    const-string v11, "instant_share_starttime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    const-string v13, "instant_share_endtime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    if-eqz v11, :cond_3

    if-eqz v13, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-wide/32 v18, 0x1b77400

    add-long v16, v16, v18

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    cmp-long v11, v18, v14

    if-lez v11, :cond_2

    const-string v11, "instant_share_starttime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v11, "instant_share_endtime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v11, "instant_share_eventid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v11, "iu.IUProvider"

    const/4 v13, 0x5

    invoke-static {v11, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v11, "iu.IUProvider"

    const-string v13, "Start time occurs in the future; don\'t enable IS"

    invoke-static {v11, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v11, "unknown setting: "

    invoke-direct {v14, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v13, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v11

    monitor-exit v12

    throw v11

    :cond_2
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    cmp-long v11, v13, v16

    if-lez v11, :cond_0

    const-string v11, "instant_share_endtime"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v11, "iu.IUProvider"

    const/4 v13, 0x5

    invoke-static {v11, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v11, "iu.IUProvider"

    const-string v13, "End time longer than max allowed; adjusting"

    invoke-static {v11, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    if-nez v11, :cond_4

    if-eqz v13, :cond_0

    :cond_4
    const-string v13, "instant_share_starttime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v13, "instant_share_endtime"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v13, "instant_share_eventid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v13, "iu.IUProvider"

    const/4 v14, 0x5

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    if-nez v11, :cond_5

    const-string v11, "iu.IUProvider"

    const-string v13, "Event end time specified without a start time; don\'t enable IS"

    invoke-static {v11, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-string v11, "iu.IUProvider"

    const-string v13, "Event start time specified without an end time; don\'t enable IS"

    invoke-static {v11, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v11, "quota_limit"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    const-string v11, "quota_used"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    const-string v11, "full_size_disabled"

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    :cond_7
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_8

    if-eqz p1, :cond_1

    :cond_8
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "com.google.android.picasasync."

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_a

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "."

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_3
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_b

    const/4 v6, 0x0

    :goto_4
    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static {v8, v9, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v4, 0x0

    goto :goto_2

    :cond_a
    const-string v11, ""

    goto :goto_3

    :cond_b
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_c
    if-eqz v1, :cond_d

    invoke-virtual {v10}, Lcom/google/android/apps/plus/iu/UploadsManager;->reloadSystemSettings()V

    :cond_d
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_e

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v11

    const-wide/16 v12, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_e
    return v1
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/pm/ProviderInfo;

    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "uploads"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "upload_all"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "iu"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "settings"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "media"

    const/16 v3, 0x13

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->cancelUploads(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :sswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->removeMedia$5508efd6(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :sswitch_2
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelUploadExistingPhotos(Ljava/lang/String;)V

    :cond_0
    move v0, v2

    goto :goto_0

    :sswitch_3
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateSettings(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_2
        0xb -> :sswitch_3
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.upload"

    :goto_0
    return-object v0

    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.upload_all"

    goto :goto_0

    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.iu"

    goto :goto_0

    :sswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.iu.photos"

    goto :goto_0

    :sswitch_4
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.iu.media"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x12 -> :sswitch_3
        0x13 -> :sswitch_4
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "INSERT "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v3

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unsupported uri:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v4

    :sswitch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/iu/UploadsManager;->addUpload(Landroid/content/ContentValues;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->getUploadUri(J)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    :goto_0
    return-object v4

    :sswitch_1
    :try_start_2
    const-string v4, "account"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->uploadExistingPhotos(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOAD_ALL_URI:Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "QUERY "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v0

    :sswitch_0
    :try_start_1
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_MAP:Ljava/util/Map;

    const-string v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementQueryResultCount(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v9}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    return-object v8

    :sswitch_1
    :try_start_2
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :sswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->querySettings(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :sswitch_3
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getUploadAllStatus(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstantUploadStatus()Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :sswitch_5
    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryPhotos(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "projection must include list of local uris"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_3
        0xb -> :sswitch_2
        0x11 -> :sswitch_4
        0x12 -> :sswitch_5
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unsupported uri:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v1, "account"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateSettings(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method
