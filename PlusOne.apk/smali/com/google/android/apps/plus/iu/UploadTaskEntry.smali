.class Lcom/google/android/apps/plus/iu/UploadTaskEntry;
.super Lcom/android/gallery3d/common/Entry;
.source "UploadTaskEntry.java"


# annotations
.annotation runtime Lcom/android/gallery3d/common/Entry$Table;
    value = "upload_tasks"
.end annotation


# static fields
.field private static final REQUIRED_COLUMNS:[Ljava/lang/String;

.field public static final SCHEMA:Lcom/android/gallery3d/common/EntrySchema;


# instance fields
.field private mAccount:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "account"
    .end annotation
.end field

.field private mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "album_id"
    .end annotation
.end field

.field private mAlbumTitle:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "album_title"
    .end annotation
.end field

.field private mAuthTokenType:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "auth_token_type"
    .end annotation
.end field

.field private mBytesTotal:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "bytes_total"
    .end annotation
.end field

.field private mBytesUploaded:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "bytes_uploaded"
    .end annotation
.end field

.field private mComponentName:Landroid/content/ComponentName;

.field private mContentUri:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "content_uri"
    .end annotation
.end field

.field private mDisplayName:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "display_name"
    .end annotation
.end field

.field private mError:Ljava/lang/Throwable;

.field private mEventId:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "event_id"
    .end annotation
.end field

.field private mFingerprint:[B
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "fingerprint"
    .end annotation
.end field

.field private mMediaRecordId:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "media_record_id"
    .end annotation
.end field

.field private mMimeType:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "mime_type"
    .end annotation
.end field

.field private mPriority:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "priority"
    .end annotation
.end field

.field private mRawComponentName:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "component_name"
    .end annotation
.end field

.field private mRequestTemplate:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "request_template"
    .end annotation
.end field

.field private mResizedFilename:Ljava/lang/String;

.field private mState:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "state"
    .end annotation
.end field

.field private mUploadUrl:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_url"
    .end annotation
.end field

.field private mUploadedTime:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "uploaded_time"
    .end annotation
.end field

.field private mUrl:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "url"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/android/gallery3d/common/EntrySchema;

    const-class v1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "content_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media_record_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->REQUIRED_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    return-void
.end method

.method static createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .locals 6
    .param p0    # Landroid/content/ContentValues;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->REQUIRED_COLUMNS:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {p0, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "missing fields in upload request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/common/EntrySchema;->valuesToObject(Landroid/content/ContentValues;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    return-object v0
.end method

.method public static fromDb(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .locals 2
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J

    new-instance v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-direct {v0}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1, p0, p1, p2, v0}, Lcom/android/gallery3d/common/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/android/gallery3d/common/Entry;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public final getAlbumId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getBytesTotal()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesTotal:J

    return-wide v0
.end method

.method public final getBytesUploaded()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesUploaded:J

    return-wide v0
.end method

.method final getComponentName()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mComponentName:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mRawComponentName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mRawComponentName:Ljava/lang/String;

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mComponentName:Landroid/content/ComponentName;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mComponentName:Landroid/content/ComponentName;

    return-object v0
.end method

.method public final getContentUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mContentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final getEventId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method public final getFingerprint()Lcom/android/gallery3d/common/Fingerprint;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mFingerprint:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/gallery3d/common/Fingerprint;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mFingerprint:[B

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V

    goto :goto_0
.end method

.method public final getMediaRecordId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mMediaRecordId:J

    return-wide v0
.end method

.method final getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public final getPercentageUploaded()I
    .locals 6

    const-wide/16 v4, 0x0

    const/16 v1, 0x64

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesTotal:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesUploaded:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesUploaded:J

    long-to-float v2, v2

    iget-wide v3, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesTotal:J

    long-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v4, 0x4059000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    if-le v0, v1, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method final getRequestTemplate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mRequestTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public final getResizedFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mResizedFilename:Ljava/lang/String;

    return-object v0
.end method

.method public final getState()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    return v0
.end method

.method public final getUploadContentUri()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mResizedFilename:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mResizedFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mContentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method final getUploadUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUploadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final hasFingerprint()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mFingerprint:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPriority()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mPriority:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mPriority:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isCancellable()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isReadyForUpload()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStartedYet()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesUploaded:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isUploading()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setAlbumId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mAlbumId:Ljava/lang/String;

    return-void
.end method

.method final setAuthTokenType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mAuthTokenType:Ljava/lang/String;

    return-void
.end method

.method public final setBytesTotal(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesTotal:J

    return-void
.end method

.method public final setBytesUploaded(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mBytesUploaded:J

    return-void
.end method

.method public final setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/common/Fingerprint;

    invoke-virtual {p1}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mFingerprint:[B

    return-void
.end method

.method final setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mMimeType:Ljava/lang/String;

    return-void
.end method

.method final setPriority(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mPriority:I

    return-void
.end method

.method final setRequestTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mRequestTemplate:Ljava/lang/String;

    return-void
.end method

.method public final setResizedFilename(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mResizedFilename:Ljava/lang/String;

    return-void
.end method

.method public final setState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    return-void
.end method

.method public final setState(ILjava/lang/Throwable;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Throwable;

    iput p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    iput-object p2, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mError:Ljava/lang/Throwable;

    return-void
.end method

.method final setUploadUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUploadUrl:Ljava/lang/String;

    return-void
.end method

.method final setUploadedTime()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUploadedTime:J

    return-void
.end method

.method final setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mUrl:Ljava/lang/String;

    return-void
.end method

.method public final shouldRetry()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "content_uri"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "media_record_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "album_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "event_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "mime_type"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "state"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "bytes_total"

    aput-object v4, v2, v3

    invoke-virtual {v1, p0, v2}, Lcom/android/gallery3d/common/EntrySchema;->toDebugString(Lcom/android/gallery3d/common/Entry;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getPercentageUploaded()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
