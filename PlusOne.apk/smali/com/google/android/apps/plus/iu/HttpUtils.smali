.class final Lcom/google/android/apps/plus/iu/HttpUtils;
.super Ljava/lang/Object;
.source "HttpUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/HttpUtils$MetricsTrackingConnectionManager;
    }
.end annotation


# static fields
.field private static sConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

.field private static sHttpParams:Lorg/apache/http/params/HttpParams;


# direct methods
.method public static createHttpClient(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/client/HttpClient;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/HttpUtils;->getHttpParams(Landroid/content/Context;)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/params/HttpParams;->copy()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/HttpUtils;->getConnectionManager(Landroid/content/Context;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    return-object v1
.end method

.method private static declared-synchronized getConnectionManager(Landroid/content/Context;)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/HttpUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/HttpUtils;->sConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/HttpUtils;->init(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/HttpUtils;->sConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized getHttpParams(Landroid/content/Context;)Lorg/apache/http/params/HttpParams;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/HttpUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/HttpUtils;->sHttpParams:Lorg/apache/http/params/HttpParams;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/HttpUtils;->init(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/HttpUtils;->sHttpParams:Lorg/apache/http/params/HttpParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static init(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    const/16 v6, 0x4e20

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-wide/16 v2, 0x4e20

    invoke-static {v0, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v4

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v2, Lcom/google/android/apps/plus/iu/HttpUtils$MetricsTrackingConnectionManager;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/plus/iu/HttpUtils$MetricsTrackingConnectionManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    sput-object v2, Lcom/google/android/apps/plus/iu/HttpUtils;->sConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " InstantUploadSync/1.0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/HttpUtils;->sHttpParams:Lorg/apache/http/params/HttpParams;

    return-void
.end method
