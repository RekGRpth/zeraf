.class public final Lcom/google/android/apps/plus/iu/InstantUploadSyncService;
.super Landroid/app/Service;
.source "InstantUploadSyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/InstantUploadSyncService$CarryOverDummyReceiver;,
        Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;
    }
.end annotation


# static fields
.field private static sSyncAdapter:Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static activateAccount(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->getAccounts(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    iget-object v3, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v1, v3}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->deactivateAccount(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v1, p2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    if-eqz p2, :cond_2

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->onAccountActivated(Ljava/lang/String;)V

    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->onAccountDeactivated(Ljava/lang/String;)V

    return-void
.end method

.method private static declared-synchronized getSyncAdapter(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->sSyncAdapter:Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->sSyncAdapter:Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->sSyncAdapter:Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->getSyncAdapter(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
