.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;
.super Ljava/lang/Object;
.source "InstantUploadSyncManager.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 3
    .param p1    # [Landroid/accounts/Account;

    const/4 v2, 0x4

    const-string v0, "iu.SyncManager"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "account change detect - update database"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$000(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
