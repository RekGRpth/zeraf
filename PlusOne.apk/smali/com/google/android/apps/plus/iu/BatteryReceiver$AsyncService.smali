.class public Lcom/google/android/apps/plus/iu/BatteryReceiver$AsyncService;
.super Landroid/app/IntentService;
.source "BatteryReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/BatteryReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AsyncService"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "InstantUploadSyncBatteryReceiverAsync"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->onBatteryStateChanged(Z)V

    return-void
.end method
