.class public final Lcom/google/android/apps/plus/iu/UploadSettings;
.super Ljava/lang/Object;
.source "UploadSettings.java"


# static fields
.field private static final PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

.field private static sInstance:Lcom/google/android/apps/plus/iu/UploadSettings;


# instance fields
.field private mAutoUploadEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private mEventEndTime:J

.field private mEventId:Ljava/lang/String;

.field private mEventStartTime:J

.field private mSettingsValid:Z

.field private mSyncAccount:Ljava/lang/String;

.field private mSyncOnBattery:Z

.field private mSyncOnRoaming:Z

.field private mUploadFullRes:Z

.field private mWifiOnlyPhoto:Z

.field private mWifiOnlyVideo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto_upload_account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sync_on_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "video_upload_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sync_on_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "instant_share_eventid"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "instant_share_starttime"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "instant_share_endtime"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "upload_full_resolution"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadSettings;->PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadSettings;
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/UploadSettings;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadSettings;->sInstance:Lcom/google/android/apps/plus/iu/UploadSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/UploadSettings;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadSettings;->sInstance:Lcom/google/android/apps/plus/iu/UploadSettings;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings(Landroid/database/Cursor;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadSettings;->sInstance:Lcom/google/android/apps/plus/iu/UploadSettings;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final getAutoUploadEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mAutoUploadEnabled:Z

    return v0
.end method

.method public final getEventEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventEndTime:J

    return-wide v0
.end method

.method public final getEventId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventStartTime:J

    return-wide v0
.end method

.method public final getSyncAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncAccount:Ljava/lang/String;

    return-object v0
.end method

.method public final getSyncOnBattery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnBattery:Z

    return v0
.end method

.method public final getSyncOnRoaming()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnRoaming:Z

    return v0
.end method

.method final getSystemSettingsCursor()Landroid/database/Cursor;
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/iu/UploadSettings;->PROJECTION_ENABLE_ACCOUNT_WIFI:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final getUploadFullRes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mUploadFullRes:Z

    return v0
.end method

.method public final getWifiOnlyPhoto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyPhoto:Z

    return v0
.end method

.method public final getWifiOnlyVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyVideo:Z

    return v0
.end method

.method public final reloadSettings()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings(Landroid/database/Cursor;)V

    return-void
.end method

.method final reloadSettings(Landroid/database/Cursor;)V
    .locals 21
    .param p1    # Landroid/database/Cursor;

    if-nez p1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSystemSettingsCursor()Landroid/database/Cursor;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v18

    if-nez v18, :cond_3

    :cond_1
    const-string v18, "iu.UploadsManager"

    const/16 v19, 0x5

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_2

    const-string v18, "iu.UploadsManager"

    const-string v19, "failed to query system settings"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_3
    const/16 v18, 0x0

    :try_start_1
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_f

    const/4 v2, 0x1

    :goto_1
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_10

    const/16 v16, 0x1

    :goto_2
    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_11

    const/16 v17, 0x1

    :goto_3
    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_12

    const/4 v14, 0x1

    :goto_4
    const/16 v18, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-eqz v18, :cond_13

    const/4 v13, 0x1

    :goto_5
    const/16 v18, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v18, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const/16 v18, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/16 v18, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    if-eqz v18, :cond_14

    const/4 v15, 0x1

    :goto_6
    invoke-static/range {p1 .. p1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    const/4 v2, 0x0

    const/4 v8, 0x0

    :cond_4
    const-string v18, "iu.UploadsManager"

    const/16 v19, 0x4

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_6

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v18, "#reloadSettings()"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "; account: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; IU: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    if-eqz v2, :cond_15

    const-string v18, "enabled"

    :goto_7
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; IS: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    if-eqz v8, :cond_16

    move-object/from16 v18, v8

    :goto_8
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v8, :cond_5

    const-string v11, "MMM dd, yyyy h:mmaa"

    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v18

    invoke-static {v11, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    new-instance v18, Ljava/util/Date;

    move-object/from16 v0, v18

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v18

    invoke-static {v11, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v18, " (start: "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, ", end: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    const-string v18, "; photoWiFi: "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; videoWiFi: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; roam: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; battery: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "; size: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    if-eqz v15, :cond_17

    const-string v18, "FULL"

    :goto_9
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v18, "iu.UploadsManager"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v18, "iu.UploadsManager"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncAccount:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v12, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_7

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   account changed from: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " --> "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v12}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mAutoUploadEnabled:Z

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v2, v0, :cond_8

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   auto upload changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_9

    const-string v19, "iu.UploadsManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v18, "   event ID changed from "

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_18

    const-string v18, "<< NULL >>"

    :goto_a
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " --> "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    if-nez v8, :cond_19

    const-string v18, "<< NULL >>"

    :goto_b
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyPhoto:Z

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_a

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   wifiOnlyPhoto changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyVideo:Z

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_b

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   wifiOnlyVideo changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnRoaming:Z

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v14, v0, :cond_c

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   syncOnRoaming changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnBattery:Z

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v13, v0, :cond_d

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   syncOnBattery changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mUploadFullRes:Z

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v15, v0, :cond_e

    const-string v18, "iu.UploadsManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "   uploadFullRes changed to "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mAutoUploadEnabled:Z

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncAccount:Ljava/lang/String;

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyPhoto:Z

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/iu/UploadSettings;->mWifiOnlyVideo:Z

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnRoaming:Z

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mSyncOnBattery:Z

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventId:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventStartTime:J

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventEndTime:J

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mUploadFullRes:Z

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/iu/UploadSettings;->mSettingsValid:Z

    goto/16 :goto_0

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_10
    const/16 v16, 0x0

    goto/16 :goto_2

    :cond_11
    const/16 v17, 0x0

    goto/16 :goto_3

    :cond_12
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_13
    const/4 v13, 0x0

    goto/16 :goto_5

    :cond_14
    const/4 v15, 0x0

    goto/16 :goto_6

    :catchall_0
    move-exception v18

    invoke-static/range {p1 .. p1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v18

    :cond_15
    const-string v18, "disabled"

    goto/16 :goto_7

    :cond_16
    const-string v18, "disabled"

    goto/16 :goto_8

    :cond_17
    const-string v18, "STANDARD"

    goto/16 :goto_9

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/UploadSettings;->mEventId:Ljava/lang/String;

    move-object/from16 v18, v0

    goto/16 :goto_a

    :cond_19
    move-object/from16 v18, v8

    goto/16 :goto_b
.end method
