.class final Lcom/google/android/apps/plus/iu/UploadUtils$MediaDetectionListener;
.super Ljava/lang/Object;
.source "UploadUtils.java"

# interfaces
.implements Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/UploadUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaDetectionListener"
.end annotation


# instance fields
.field public mMediaType:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadUtils$MediaDetectionListener;->mMediaType:I

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadUtils$MediaDetectionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMediaTypeDetected(I)V
    .locals 2
    .param p1    # I

    const-class v1, Lcom/google/android/apps/plus/iu/UploadUtils;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/plus/iu/UploadUtils$MediaDetectionListener;->mMediaType:I

    const-class v0, Lcom/google/android/apps/plus/iu/UploadUtils;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
