.class final Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;
.super Lcom/google/android/apps/plus/iu/SyncTask;
.source "UploadsManager.java"

# interfaces
.implements Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/UploadsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UploadTask"
.end annotation


# instance fields
.field protected mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

.field private mFingerprintSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/gallery3d/common/Fingerprint;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsPhoto:Z

.field protected mLogName:Ljava/lang/String;

.field protected volatile mRunning:Z

.field private final mStateSetting:Ljava/lang/String;

.field protected mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

.field protected final mUploadType:I

.field private final mValues:Landroid/content/ContentValues;

.field final synthetic this$0:Lcom/google/android/apps/plus/iu/UploadsManager;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;IZ)V
    .locals 3
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/iu/SyncTask;-><init>(Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mValues:Landroid/content/ContentValues;

    iput p3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    iput-boolean p4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mIsPhoto:Z

    shl-int/lit8 v1, p3, 0x1

    if-eqz p4, :cond_0

    const/4 v0, 0x0

    :cond_0
    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mPriority:I

    sparse-switch p3, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown upload type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "instant_upload_state"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mStateSetting:Ljava/lang/String;

    const-string v0, "InstantUpload"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    :goto_0
    return-void

    :sswitch_1
    const-string v0, "instant_share_state"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mStateSetting:Ljava/lang/String;

    const-string v0, "InstantShare"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const-string v0, "upload_all_state"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mStateSetting:Ljava/lang/String;

    const-string v0, "UploadAll"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const-string v0, "manual_upload_state"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mStateSetting:Ljava/lang/String;

    const-string v0, "Manual"

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_1
        0x1e -> :sswitch_0
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method private getNextUpload()Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getNextUpload(Ljava/util/HashSet;Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;)Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private isOutOfQuota(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_QUOTA:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1800()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const/4 v8, -0x1

    const/4 v9, -0x1

    if-eqz v7, :cond_1

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {v8, v9}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->isOutOfQuota(II)Z

    move-result v0

    return v0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private isUploadedBefore(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z
    .locals 14
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v10

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v13

    if-nez v13, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2100(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2200()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2300()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "fingerprint_hash=? AND user_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, v13, Lcom/google/android/apps/plus/iu/UserEntry;->id:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v12

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v1

    invoke-virtual {v1, v12}, Lcom/android/gallery3d/common/Fingerprint;->equals([B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    invoke-virtual {v1, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method private onIncompleteUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)Z
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Z

    const/4 v4, 0x5

    const/4 v1, 0x1

    const/16 v6, 0xa

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v1

    if-eq v1, v4, :cond_8

    const-string v1, "iu.UploadsManager"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- STOP wrong state after upload; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "--- STOP wrong state; "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v3, 0x5

    invoke-virtual {v1, p1, v3, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V

    :goto_1
    const/4 v1, 0x0

    monitor-exit v2

    :goto_2
    return v1

    :pswitch_1
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--- QUEUE stalled "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v4, 0x3

    invoke-virtual {v3, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStalled(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :pswitch_2
    :try_start_1
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--- QUEUE unauthorized "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v4, 0x3

    invoke-virtual {v3, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    const/16 v3, 0xa

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    iget v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    if-ne v3, v6, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/16 v4, 0xa

    invoke-static {v3, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_4
    monitor-exit v2

    goto :goto_2

    :pswitch_3
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--- QUEUE quota exceeded "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v4, 0x3

    invoke-virtual {v3, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    iget v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    if-ne v3, v6, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/16 v4, 0x9

    invoke-static {v3, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_6
    monitor-exit v2

    goto/16 :goto_2

    :pswitch_4
    const-string v1, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- STOP cancelled "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/16 v3, 0x8

    invoke-virtual {v1, p1, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    goto/16 :goto_1

    :cond_8
    const-string v1, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- STOP failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onStalled(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Z

    const/16 v1, 0xf

    const/16 v2, 0xe

    const/16 v4, 0xd

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2000(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    :goto_1
    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v3, 0xa

    if-ne v0, v3, :cond_0

    if-eqz p2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2000(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-static {v0, p1, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    goto :goto_2
.end method

.method private onTaskDone(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1100(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v2, 0xa

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v2, 0x1

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_1
    :goto_1
    const-string v0, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   task done: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v2, 0x28

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1200(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private onUploadsDone()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v2, 0x28

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1200(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private queueTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Landroid/content/SyncStats;
    .param p3    # Ljava/lang/Throwable;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V

    iget-wide v0, p2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p2, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStalled(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)V

    return-void
.end method

.method private skipTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Landroid/content/SyncStats;
    .param p3    # Ljava/lang/Throwable;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V

    iget-wide v1, p2, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p2, Landroid/content/SyncStats;->numSkippedEntries:J

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->id:J

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->removeTaskFromDb(J)Z
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1000(Lcom/google/android/apps/plus/iu/UploadsManager;J)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x12c

    const/16 v2, 0x26

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1100(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onTaskDone(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    return-void
.end method

.method private syncCameraSyncStream(Landroid/content/SyncResult;Ljava/lang/String;)Z
    .locals 9
    .param p1    # Landroid/content/SyncResult;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    invoke-static {v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$700(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;
    invoke-static {v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$200(Lcom/google/android/apps/plus/iu/UploadsManager;)Ljava/util/HashSet;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v3, 0x1

    monitor-exit v7

    :goto_0
    return v3

    :cond_0
    iget-boolean v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-nez v6, :cond_1

    const/4 v3, 0x0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_1
    monitor-exit v7

    const-string v6, "iu.UploadsManager"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sync camera-sync stream for dedup: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v6, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    invoke-virtual {v4, p2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v5

    if-nez v5, :cond_4

    const-string v6, "iu.UploadsManager"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "no userEntry for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->cancelSync()V

    new-instance v6, Ljava/io/IOException;

    const-string v7, "no user entry"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_4
    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->syncUploadedPhotos(Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;Lcom/google/android/apps/plus/iu/UserEntry;)V

    iget-object v6, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    cmp-long v6, v0, v6

    if-gez v6, :cond_6

    const-string v6, "iu.UploadsManager"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "need picasa authorization for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->cancelSync()V

    new-instance v6, Ljava/io/IOException;

    const-string v7, "need picasa authorization"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_6
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v7

    :try_start_1
    iget-boolean v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-eqz v3, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;
    invoke-static {v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$200(Lcom/google/android/apps/plus/iu/UploadsManager;)Ljava/util/HashSet;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_7
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v6

    monitor-exit v7

    throw v6
.end method


# virtual methods
.method public final cancelIfCurrentTaskMatches(J)Z
    .locals 4
    .param p1    # J

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-wide v2, v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->id:J

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->stopCurrentTask(I)V

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final cancelSync()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->stopCurrentTask(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->stopSync()V

    :cond_0
    const-string v0, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "--- CANCEL sync "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isBackgroundSync()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSyncOnBattery()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncOnBattery()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSyncOnRoaming()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncOnRoaming()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSyncOnWifiOnly()Z
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v3, 0x14

    if-ne v2, v3, :cond_1

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mIsPhoto:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_0

    :goto_1
    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getWifiOnlyPhoto()Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getWifiOnlyVideo()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final onFileChanged(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/FingerprintHelper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->invalidate(Ljava/lang/String;)V

    return-void
.end method

.method public final onProgress(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-eqz v0, :cond_1

    const-string v0, "iu.UploadsManager"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  progress: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1600(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v2, 0xa

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v2, 0x1

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onRejected(I)V
    .locals 3
    .param p1    # I

    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.UploadsManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REJECT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " due to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0x28

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1200(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    :cond_1
    return-void
.end method

.method protected final onStateChanged$fa872aa(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mValues:Landroid/content/ContentValues;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mStateSetting:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v1, 0x28

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1200(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1100(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    return-void
.end method

.method public final performSync(Landroid/content/SyncResult;)V
    .locals 11
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mRunning:Z

    if-nez v2, :cond_0

    monitor-exit v3

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$700(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    invoke-static {v2, p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_1
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- START syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onStateChanged$fa872aa(I)V

    iget v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    const/16 v5, 0x1e

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    const/16 v5, 0x14

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    const/16 v5, 0x28

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--- CANCEL sync; type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "iu.upload"

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->endWithReport(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    :try_start_3
    monitor-exit v3

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncCameraSyncStream(Landroid/content/SyncResult;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "iu.UploadsManager"

    const-string v3, "--- STOP IU provider; picasa sync canceled"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "iu.upload"

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->endWithReport(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v2

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v4, 0x0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "iu.upload"

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/plus/iu/MetricsUtils;->endWithReport(Landroid/content/Context;ILjava/lang/String;)V

    throw v2

    :cond_6
    :try_start_5
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->retrieveAllFingerprints(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    :cond_7
    iget-object v6, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->getNextUpload()Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-nez v9, :cond_b

    :try_start_7
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ STOP syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; no more tasks"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onUploadsDone()V

    :cond_9
    :goto_2
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- DONE syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_a
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mSyncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mFingerprintSet:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "iu.upload"

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->endWithReport(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    :try_start_8
    monitor-exit v3

    throw v2

    :cond_b
    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ SKIP task "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; account changed; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    const/4 v2, 0x0

    invoke-direct {p0, v9, v6, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->skipTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v10

    if-nez v10, :cond_f

    const-wide/16 v2, -0x1

    move-wide v4, v2

    :goto_3
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_11

    cmp-long v2, v7, v4

    if-lez v2, :cond_11

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-lez v2, :cond_10

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ SKIP task "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; exceed retry time; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_4
    const/4 v2, 0x0

    invoke-direct {p0, v9, v6, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->skipTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v10}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getRetryEndTime()J

    move-result-wide v2

    move-wide v4, v2

    goto :goto_3

    :cond_10
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ SKIP task "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; couldn\'t find media record; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings()V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->isOutOfQuota(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    const/16 v7, 0xa

    if-ne v3, v7, :cond_12

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/UploadSettings;->getAutoUploadEnabled()Z

    move-result v3

    if-eqz v3, :cond_15

    :cond_12
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/UploadSettings;->getUploadFullRes()Z

    move-result v3

    if-eqz v3, :cond_15

    if-nez v2, :cond_15

    const/4 v2, 0x1

    move v3, v2

    :goto_5
    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isStartedYet()Z

    move-result v2

    if-nez v2, :cond_17

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadedTime()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v9, v3}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->fillRequest(Landroid/content/Context;Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_13

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ QUEUE task "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J
    invoke-static {v2, v10, v3, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1400(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    const/4 v2, 0x0

    invoke-direct {p0, v9, v6, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->queueTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto/16 :goto_2

    :catch_0
    move-exception v2

    :try_start_a
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ SKIP task "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; file removed; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    const/4 v2, 0x0

    invoke-direct {p0, v9, v6, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->skipTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_15
    const/4 v2, 0x0

    move v3, v2

    goto :goto_5

    :catch_1
    move-exception v2

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_16

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ QUEUE task "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; fill failed; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_16
    invoke-direct {p0, v9, v6, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->queueTask(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_17
    const-string v2, "camera-sync"

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncCameraSyncStream(Landroid/content/SyncResult;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_18

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ STOP syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; picasa sync canceled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_18
    const/4 v2, 0x0

    if-eqz v7, :cond_1d

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->isUploadedBefore(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z

    move-result v7

    if-eqz v7, :cond_1d

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_19

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP task "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; duplicate; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/16 v4, 0xc

    invoke-virtual {v3, v9, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    :cond_1a
    :goto_6
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-wide v4, v9, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->id:J

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->removeTaskFromDb(J)Z
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1000(Lcom/google/android/apps/plus/iu/UploadsManager;J)Z

    if-eqz v2, :cond_1c

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v10}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getUploadTime()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v10, v4, v5}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadTime(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    :cond_1b
    sget-object v4, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v4, v3, v2}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    :cond_1c
    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onTaskDone(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    goto/16 :goto_2

    :cond_1d
    const-string v2, "iu.UploadsManager"

    const/4 v7, 0x4

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1e

    const-string v2, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+++ START "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " retry end: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    iget-wide v4, v6, Landroid/content/SyncStats;->numIoExceptions:J

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->doUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Landroid/content/SyncResult;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    invoke-static {v2, v9, p0, p1, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1500(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Landroid/content/SyncResult;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v2

    if-nez v2, :cond_20

    iget-wide v6, v6, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v3, v6, v4

    if-lez v3, :cond_1f

    const/4 v3, 0x1

    :goto_7
    invoke-direct {p0, v9, v3}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->onIncompleteUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Z)Z

    move-result v3

    if-eqz v3, :cond_1a

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J
    invoke-static {v2, v10, v3, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1400(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    invoke-static {v2, v9}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1600(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    goto/16 :goto_2

    :cond_1f
    const/4 v3, 0x0

    goto :goto_7

    :cond_20
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    const/4 v4, 0x4

    invoke-virtual {v3, v9, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    iget-object v3, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numEntries:J

    iget-object v3, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numInserts:J

    const-string v3, "camera-sync"

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->writeToPhotoTable$595d6497(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/MediaRecordEntry;)Z
    invoke-static {v3, v9, v2, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1700(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/MediaRecordEntry;Landroid/content/SyncResult;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_21

    const-string v3, "iu.UploadsManager"

    const-string v4, "sync album now: %s, %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncAccount:Ljava/lang/String;

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->syncCameraSyncStream(Landroid/content/SyncResult;Ljava/lang/String;)Z

    :cond_22
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_23

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ DONE "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mLogName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_23
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1a

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ DONE; upload finished; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto/16 :goto_6
.end method

.method protected final stopCurrentTask(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mCurrentTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopCurrentTask: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v2

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isCancellable()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->this$0:Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
