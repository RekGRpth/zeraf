.class public final Lcom/google/android/apps/plus/api/EditModerationStateOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EditModerationStateOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EditModerationStateRequest;",
        "Lcom/google/api/services/plusi/model/EditModerationStateResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mModerationState:Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "editmoderationstate"

    invoke-static {}, Lcom/google/api/services/plusi/model/EditModerationStateRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EditModerationStateRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EditModerationStateResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EditModerationStateResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mSquareId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mActivityId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mModerationState:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "REJECTED"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mModerationState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "APPROVED"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mModerationState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mActivityId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->updateActivityIsSpam(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/EditModerationStateRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mSquareId:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->createSquareStreamRenderContext(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/api/services/plusi/model/RenderContext;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditModerationStateRequest;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    new-instance v0, Lcom/google/api/services/plusi/model/EntityModerationState;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityModerationState;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mModerationState:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EntityModerationState;->state:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->mActivityId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EntityModerationState;->updateId:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/EditModerationStateRequest;->entityState:Ljava/util/List;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/EditModerationStateRequest;->entityState:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
