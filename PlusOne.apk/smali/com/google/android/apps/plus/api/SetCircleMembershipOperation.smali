.class public Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SetCircleMembershipOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;",
        "Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mCirclesToAdd:[Ljava/lang/String;

.field protected final mCirclesToRemove:[Ljava/lang/String;

.field protected final mFireAndForget:Z

.field protected final mPersonId:Ljava/lang/String;

.field protected final mPersonName:Ljava/lang/String;

.field protected final mUpdateNotification:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # Z
    .param p8    # Z
    .param p9    # Landroid/content/Intent;
    .param p10    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v4, "modifymemberships"

    invoke-static {}, Lcom/google/api/services/plusi/model/ModifyMembershipsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ModifyMembershipsRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/ModifyMembershipsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ModifyMembershipsResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mFireAndForget:Z

    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mUpdateNotification:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    new-instance v5, Lcom/google/api/services/plusi/model/DataMembership;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataMembership;-><init>()V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;

    move-result-object v4

    iput-object v4, v5, Lcom/google/api/services/plusi/model/DataMembership;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move-object v3, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;[Ljava/lang/String;[Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mUpdateNotification:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mFireAndForget:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->setCircleMembershipResult(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->showToastIfNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_2
    return-void

    :cond_3
    move-object v3, v0

    goto :goto_1
.end method

.method public final onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .locals 5
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # [Lorg/apache/http/Header;
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->setCircleMembershipResult(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/api/PlusiOperation;->onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V

    return-void
.end method

.method protected bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->populateRequest(Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;)V

    return-void
.end method

.method protected populateRequest(Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;)V
    .locals 7
    .param p1    # Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;

    new-instance v5, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;-><init>()V

    iput-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v5, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;->person:Ljava/util/List;

    new-instance v4, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleMemberId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleMemberId;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;->displayName:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;->person:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToAdd:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToAdd:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToRemove:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToRemove:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
