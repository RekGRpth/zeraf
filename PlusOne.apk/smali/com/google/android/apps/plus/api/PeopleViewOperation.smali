.class public final Lcom/google/android/apps/plus/api/PeopleViewOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PeopleViewOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PeopleViewDataRequest;",
        "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/google/api/services/plusi/model/PeopleViewDataRequest;

.field mRequestParameters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataRequestParameter;",
            ">;"
        }
    .end annotation
.end field

.field private mResponse:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataRequestParameter;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    const-string v3, "loadpeopleviewdata"

    invoke-static {}, Lcom/google/api/services/plusi/model/PeopleViewDataRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PeopleViewDataRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mRequestParameters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getRequest()Lcom/google/api/services/plusi/model/PeopleViewDataRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mRequest:Lcom/google/api/services/plusi/model/PeopleViewDataRequest;

    return-object v0
.end method

.method public final getResponse()Lcom/google/api/services/plusi/model/PeopleViewDataResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mResponse:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mResponse:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PeopleViewDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mRequestParameters:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewDataRequest;->parameter:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->getVersionInfo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewDataRequest;->fbsVersionInfo:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/PeopleViewOperation;->mRequest:Lcom/google/api/services/plusi/model/PeopleViewDataRequest;

    return-void
.end method
