.class public final Lcom/google/android/apps/plus/api/SnapToPlaceOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SnapToPlaceOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SnapToPlaceRequest;",
        "Lcom/google/api/services/plusi/model/SnapToPlaceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mIsPlaceSearch:Z

.field private final mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

.field private final mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mStoreResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/api/LocationQuery;
    .param p6    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p7    # Z

    const-string v3, "snaptoplace"

    invoke-static {}, Lcom/google/api/services/plusi/model/SnapToPlaceRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SnapToPlaceRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SnapToPlaceResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SnapToPlaceResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mStoreResult:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->hasQueryString()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mIsPlaceSearch:Z

    return-void
.end method


# virtual methods
.method public final getCoarseLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method public final getPlaceLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method public final getPreciseLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->preciseLocation:Lcom/google/api/services/plusi/model/LocationResult;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x1

    iget-object v4, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->preciseLocation:Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->cityLocation:Lcom/google/api/services/plusi/model/LocationResult;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x2

    iget-object v4, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->cityLocation:Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_1
    iget-object v4, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->localPlace:Ljava/util/List;

    if-nez v4, :cond_4

    move v1, v2

    :goto_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->userIsAtFirstPlace:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    new-instance v5, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v5, v7, v0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mStoreResult:Z

    if-eqz v0, :cond_7

    if-lez v1, :cond_5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    if-ge v2, v1, :cond_6

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    new-instance v6, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-direct {v6, v7, v0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/content/DbLocation;->isSamePlace(Lcom/google/android/apps/plus/content/DbLocation;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_5
    move-object v5, v3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mIsPlaceSearch:Z

    if-eqz v1, :cond_8

    move-object v4, v3

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/DbLocation;Ljava/util/ArrayList;)V

    :cond_7
    return-void

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/content/DbLocation;->isSamePlace(Lcom/google/android/apps/plus/content/DbLocation;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object v0, v3

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mOmitLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/content/DbLocation;->isSamePlace(Lcom/google/android/apps/plus/content/DbLocation;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object v4, v3

    move-object v3, v0

    goto :goto_2

    :cond_a
    move-object v3, v0

    goto :goto_2
.end method

.method public final hasCoarseLocation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPlaceLocation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPreciseLocation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const-wide v3, 0x416312d000000000L

    check-cast p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->latitudeE7:Ljava/lang/Integer;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->longitudeE7:Ljava/lang/Integer;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->precisionMeters:Ljava/lang/Double;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->hasQueryString()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->searchQuery:Ljava/lang/String;

    :cond_1
    return-void
.end method
