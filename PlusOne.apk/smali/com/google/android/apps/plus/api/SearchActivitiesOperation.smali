.class public final Lcom/google/android/apps/plus/api/SearchActivitiesOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SearchActivitiesOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SearchQueryRequest;",
        "Lcom/google/api/services/plusi/model/SearchQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mSearchMode:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "searchquery"

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mSearchMode:I

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mSearchMode:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/SearchUtils;->getSearchKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ActivityResults;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    const-string v4, "DEFAULT"

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ActivityResults;->shownActivitiesBlob:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPostsData;->updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mSearchMode:I

    iget-object v4, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ActivityResults;->shownActivitiesBlob:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/SearchUtils;->insertSearchResults(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mSearchMode:I

    invoke-static {v0, v1, v7, v2, v7}, Lcom/google/android/apps/plus/util/SearchUtils;->insertSearchResults(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/SearchQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchQuery;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->queryText:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mSearchMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "TACOS"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->filter:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityRequestData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityRequestData;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->shownActivitiesBlob:Ljava/lang/String;

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    new-instance v1, Lcom/google/api/services/plusi/model/ActivityFilters;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ActivityFilters;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeEmbedsData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->skipCommentCollapsing:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    return-void

    :pswitch_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "BEST"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->sort:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "RECENT"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->sort:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
