.class public final Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetVolumeControlsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetVolumeControlsRequest;",
        "Lcom/google/api/services/plusi/model/GetVolumeControlsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mId:Ljava/lang/String;

.field private mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

.field private final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "getvolumecontrols"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetVolumeControlsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetVolumeControlsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetVolumeControlsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetVolumeControlsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v6, 0x0

    const-string v3, "getvolumecontrols"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetVolumeControlsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetVolumeControlsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetVolumeControlsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetVolumeControlsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getVolumeControlMap()Lcom/google/api/services/plusi/model/VolumeControlMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetVolumeControlsResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetVolumeControlsResponse;->map:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeControlMap;->volumePair:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mMap:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeControlMap;->volumePair:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    if-eqz v2, :cond_0

    iget-object v3, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->volumeSettings:Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/android/apps/plus/content/VolumeSettings;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->volumeSettings:Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/content/VolumeSettings;-><init>(Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;)V

    const-string v0, "SQUARES"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/VolumeControlKey;->type:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/VolumeControlKey;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    invoke-static {v0, v2, v4, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->setSquareVolume(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V

    goto :goto_0

    :cond_1
    const-string v0, "CIRCLE"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/VolumeControlKey;->type:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/VolumeControlKey;->focusGroupId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    invoke-static {v0, v2, v4, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->setCircleVolume(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetVolumeControlsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/api/services/plusi/model/VolumeControlKey;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/VolumeControlKey;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlKey;->type:Ljava/lang/String;

    const-string v1, "CIRCLE"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlKey;->focusGroupId:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/api/services/plusi/model/VolumeControlKey;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetVolumeControlsRequest;->filterKey:Ljava/util/List;

    :cond_0
    return-void

    :cond_1
    const-string v1, "SQUARES"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlKey;->obfuscatedGaiaId:Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid volume control type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
