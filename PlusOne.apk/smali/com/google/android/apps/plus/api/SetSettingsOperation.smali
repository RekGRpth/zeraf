.class public final Lcom/google/android/apps/plus/api/SetSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SetSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;",
        "Lcom/google/api/services/plusi/model/SetMobileSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final mWarmWelcomeTimestamp:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "setmobilesettings"

    invoke-static {}, Lcom/google/api/services/plusi/model/SetMobileSettingsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SetMobileSettingsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SetMobileSettingsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SetMobileSettingsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/api/SetSettingsOperation;->mWarmWelcomeTimestamp:J

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/MobilePreference;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobilePreference;-><init>()V

    iget-wide v1, p0, Lcom/google/android/apps/plus/api/SetSettingsOperation;->mWarmWelcomeTimestamp:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MobilePreference;->wwMainFlowAckTimestampMsec:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    return-void
.end method
