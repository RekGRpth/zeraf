.class public final Lcom/google/android/apps/plus/api/SendEventRsvpOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SendEventRsvpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventRespondRequest;",
        "Lcom/google/api/services/plusi/model/EventRespondResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthKey:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;

.field private final mRollbackRsvpType:Ljava/lang/String;

.field private final mRsvpType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "eventrespond"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventRespondRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventRespondRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventRespondResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventRespondResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAuthKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRollbackRsvpType:Ljava/lang/String;

    return-void
.end method

.method private rollback()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRollbackRsvpType:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->refreshEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EventRespondResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondResponse;->result:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondResponse;->result:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;->SUCCESS:Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->rollback()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->rollback()V

    :cond_1
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/EventRespondRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->eventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->response:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAuthKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    return-void
.end method
