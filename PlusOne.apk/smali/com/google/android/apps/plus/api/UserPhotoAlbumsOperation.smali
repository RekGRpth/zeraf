.class public final Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "UserPhotoAlbumsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;",
        "Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mOwnerId:Ljava/lang/String;

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "userphotoalbums"

    invoke-static {}, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->onStartResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->aggregateAlbum:Ljava/util/List;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsResponse;->nonAggregateAlbum:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbums(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v1, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;->sharedAlbumsOnly:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;->ownerId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;->maxPreviewCount:Ljava/lang/Integer;

    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;->maxResults:Ljava/lang/Integer;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;->mOwnerId:Ljava/lang/String;

    goto :goto_1
.end method
