.class public final Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;
.super Lcom/google/android/apps/plus/api/PlusOneOperation;
.source "CommentOptimisticPlusOneOperation.java"


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mPhotoId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;JLjava/lang/String;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # Ljava/lang/String;
    .param p9    # Z

    const-string v5, "TACO_COMMENT"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v6, p8

    move/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iput-wide p6, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    return-void
.end method


# virtual methods
.method protected final onFailure()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v5, v6, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_0
    iget-wide v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    if-nez v6, :cond_3

    :goto_1
    invoke-static {v0, v3, v4, v5, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected final onPopulateRequest()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Z

    :cond_1
    return-void
.end method

.method protected final onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->updateCommentPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;Z)Z

    :cond_1
    return-void
.end method
