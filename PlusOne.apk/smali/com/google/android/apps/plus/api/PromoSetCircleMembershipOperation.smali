.class public final Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;
.super Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;
.source "PromoSetCircleMembershipOperation.java"


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private mAddedCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field

.field private mOriginalPromoPeopleMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

.field private mRemovedCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # [Ljava/lang/String;
    .param p8    # [Ljava/lang/String;
    .param p9    # Z
    .param p10    # Z
    .param p11    # Landroid/content/Intent;
    .param p12    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mRequestId:I

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCircleDataList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAddedCircles:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCircleDataList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mRemovedCircles:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final getAddedCircles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAddedCircles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRemovedCircles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mRemovedCircles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mOriginalPromoPeopleMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->updateDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->populateRequest(Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;)V

    return-void
.end method

.method protected final populateRequest(Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->populateRequest(Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mOriginalPromoPeopleMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mRequestId:I

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->applyPromoCardCirclesChange$79e449f2(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->updateDbPromoPeopleMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)V

    return-void
.end method
