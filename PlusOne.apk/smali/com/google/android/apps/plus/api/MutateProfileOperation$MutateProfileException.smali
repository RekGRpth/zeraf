.class public final Lcom/google/android/apps/plus/api/MutateProfileOperation$MutateProfileException;
.super Lcom/google/android/apps/plus/api/ProtocolException;
.source "MutateProfileOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/api/MutateProfileOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MutateProfileException"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/api/services/plusi/model/MutateProfileResponse;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/MutateProfileResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MutateProfileResponse;->errorCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/api/MutateProfileOperation;->access$000(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/MutateProfileResponse;->errorMessage:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(ILjava/lang/String;)V

    return-void
.end method
