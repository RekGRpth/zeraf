.class public final Lcom/google/android/apps/plus/api/RegisterDeviceOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "RegisterDeviceOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/RegisterDeviceRequest;",
        "Lcom/google/api/services/plusi/model/RegisterDeviceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "registerdevice"

    invoke-static {}, Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;->getInstance()Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/RegisterDeviceResponseJson;->getInstance()Lcom/google/api/services/plusi/model/RegisterDeviceResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/RegisterDeviceOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RegisterDeviceOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->setNotificationPushEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/PushAddress;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PushAddress;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->pushAddress:Lcom/google/api/services/plusi/model/PushAddress;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->pushAddress:Lcom/google/api/services/plusi/model/PushAddress;

    new-instance v1, Lcom/google/api/services/plusi/model/PushAddressAndroidPushAddress;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/PushAddressAndroidPushAddress;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PushAddress;->androidPushAddress:Lcom/google/api/services/plusi/model/PushAddressAndroidPushAddress;

    return-void
.end method
