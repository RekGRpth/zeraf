.class public final Lcom/google/android/apps/plus/api/SubOperationFailedException;
.super Ljava/io/IOException;
.source "SubOperationFailedException.java"


# static fields
.field private static final serialVersionUID:J = -0xd6d430cae400283L


# instance fields
.field private final mFailedOperations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/network/HttpOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/network/HttpOperation;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/api/SubOperationFailedException;->mFailedOperations:Ljava/util/List;

    return-void
.end method
