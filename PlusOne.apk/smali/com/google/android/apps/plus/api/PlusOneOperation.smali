.class public Lcom/google/android/apps/plus/api/PlusOneOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PlusOneOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PlusOneRequest;",
        "Lcom/google/api/services/plusi/model/PlusOneResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mIsPlusOne:Z

.field protected final mItemId:Ljava/lang/String;

.field protected final mItemType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    const-string v3, "plusone"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusOneRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PlusOneRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusOneResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PlusOneResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemId:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mIsPlusOne:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PlusOneResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneResponse;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onFailure()V

    goto :goto_0
.end method

.method protected onFailure()V
    .locals 0

    return-void
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onFailure()V

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/api/PlusiOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected onPopulateRequest()V
    .locals 0

    return-void
.end method

.method protected onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/DataPlusOne;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PlusOneRequest;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onPopulateRequest()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->type:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->itemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->isPlusone:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    const-string v1, "native:android_app"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ApiaryFields;->sourceInfo:Ljava/lang/String;

    return-void
.end method
