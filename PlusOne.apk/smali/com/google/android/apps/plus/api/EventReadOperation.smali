.class public final Lcom/google/android/apps/plus/api/EventReadOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EventReadOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventReadRequest;",
        "Lcom/google/api/services/plusi/model/EventLeafResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENT_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAuthKey:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;

.field private final mFetchNewer:Z

.field private final mInvitationToken:Ljava/lang/String;

.field private mPermissionErrorEncountered:Z

.field private mPollingToken:Ljava/lang/String;

.field private final mResolveTokens:Z

.field private mResumeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "polling_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resume_token"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/EventReadOperation;->EVENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .param p9    # Landroid/content/Intent;
    .param p10    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v4, "eventread"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventReadRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/EventLeafResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventLeafResponseJson;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPermissionErrorEncountered:Z

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Event ID must not be empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAuthKey:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "eventread"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventLeafResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventLeafResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPermissionErrorEncountered:Z

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAuthKey:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 22
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EventLeafResponse;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->update:Lcom/google/api/services/plusi/model/Update;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->activityId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v6}, Lcom/google/android/apps/plus/content/EsEventData;->getDisplayTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v12

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->status:Ljava/lang/String;

    const-string v4, "INSUFFICIENT_PERMISSION"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPermissionErrorEncountered:Z

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v3, :cond_a

    :cond_1
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->pollingToken:Ljava/lang/String;

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-eqz v3, :cond_b

    :cond_2
    const/4 v11, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-nez v3, :cond_c

    const/4 v3, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    if-eqz v4, :cond_1b

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {v3, v4, v10}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    const/4 v3, 0x1

    move v4, v3

    :goto_5
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->photosData:Ljava/util/List;

    if-eqz v3, :cond_d

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->photosData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;->photos:Ljava/util/List;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;->photos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_4
    :goto_6
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataPhoto;

    new-instance v17, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const/16 v18, 0x64

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    const-wide v20, 0x408f400000000000L

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_5
    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    new-instance v18, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->profilePhotoUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    if-eqz v4, :cond_7

    move-object/from16 v0, v17

    iget-wide v12, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_7
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/api/services/plusi/model/DataPhotoJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    goto/16 :goto_2

    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_3

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_d
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->comments:Ljava/util/List;

    if-eqz v3, :cond_12

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->comments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_e
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Comment;

    new-instance v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct {v15}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const/16 v16, 0x5

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    move-object/from16 v16, v0

    if-eqz v16, :cond_f

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_f
    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    new-instance v16, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct/range {v16 .. v16}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    iget-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v16, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsEventData$EventComment;-><init>()V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->commentId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    if-eqz v17, :cond_10

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->ownedByViewer:Z

    :cond_10
    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v17, v0

    if-eqz v17, :cond_11

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    move-object/from16 v17, v0

    if-eqz v17, :cond_11

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->totalPlusOnes:J

    :cond_11
    iget-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    if-eqz v3, :cond_e

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_12
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->frames:Ljava/util/List;

    if-eqz v3, :cond_1a

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->frames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_13
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/EventFrame;

    new-instance v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct {v15}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const-string v16, "INVITED"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_16

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    :goto_9
    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->lastTimeMillis:Ljava/lang/Long;

    move-object/from16 v16, v0

    if-eqz v16, :cond_14

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->lastTimeMillis:Ljava/lang/Long;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_14
    new-instance v16, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->invitee:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_19

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventFrame;->invitee:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_15
    :goto_a
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    move-object/from16 v18, v0

    if-eqz v18, :cond_15

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_15

    new-instance v19, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;-><init>()V

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->numAdditionalGuests:I

    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_16
    const-string v16, "RSVP_NO"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_17

    const/16 v16, 0x3

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto/16 :goto_9

    :cond_17
    const-string v16, "RSVP_YES"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_18

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto/16 :goto_9

    :cond_18
    const-string v16, "CHECKIN"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_13

    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto/16 :goto_9

    :cond_19
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_13

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static/range {v3 .. v14}, Lcom/google/android/apps/plus/content/EsEventData;->updateEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V

    goto/16 :goto_0

    :cond_1b
    move v4, v3

    goto/16 :goto_5
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0x194

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x190

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HttpTransaction"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "HttpTransaction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[EVENT_READ] received error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; disable IS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v7, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/EventReadRequest;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/api/EventReadOperation;->EVENT_PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;-><init>()V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeActivityId:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeUpdate:Ljava/lang/Boolean;

    new-instance v1, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;-><init>()V

    const/16 v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;->maxFrames:Ljava/lang/Integer;

    new-instance v2, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;-><init>()V

    const/16 v3, 0x1f4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;->maxComments:Ljava/lang/Integer;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;-><init>()V

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;->maxPhotos:Ljava/lang/Integer;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAuthKey:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/ReadOptions;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/ReadOptions;-><init>()V

    iput-object v3, v6, Lcom/google/api/services/plusi/model/ReadOptions;->photosOptions:Ljava/util/List;

    iput-object v1, v6, Lcom/google/api/services/plusi/model/ReadOptions;->framesOptions:Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    iput-object v2, v6, Lcom/google/api/services/plusi/model/ReadOptions;->commentsOptions:Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->eventUpdateOptions:Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    const-string v0, "LIST"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->responseFormat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->includePlusEvent:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->resolvePersons:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v5, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->readOptions:Ljava/util/List;

    iput-object v4, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->invitationToken:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->pollingToken:Ljava/lang/String;

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->resumeToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public final setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPermissionErrorEncountered:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x193

    const-string v1, "INSUFFICIENT_PERMISSION"

    const/4 v2, 0x0

    invoke-super {p0, v0, v1, v2}, Lcom/google/android/apps/plus/api/PlusiOperation;->setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/api/PlusiOperation;->setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
