.class public final Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SetVolumeControlsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SetVolumeControlsRequest;",
        "Lcom/google/api/services/plusi/model/SetVolumeControlsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mCircleToVolumeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/VolumeSettings;",
            ">;"
        }
    .end annotation
.end field

.field private mId:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mVolumeSettings:Lcom/google/android/apps/plus/content/VolumeSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/content/VolumeSettings;

    const-string v3, "setvolumecontrols"

    invoke-static {}, Lcom/google/api/services/plusi/model/SetVolumeControlsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SetVolumeControlsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SetVolumeControlsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SetVolumeControlsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mVolumeSettings:Lcom/google/android/apps/plus/content/VolumeSettings;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/HashMap;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/VolumeSettings;",
            ">;)V"
        }
    .end annotation

    const-string v3, "setvolumecontrols"

    invoke-static {}, Lcom/google/api/services/plusi/model/SetVolumeControlsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SetVolumeControlsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SetVolumeControlsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SetVolumeControlsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mCircleToVolumeMap:Ljava/util/HashMap;

    return-void
.end method

.method private static createVolumeSettings(Lcom/google/android/apps/plus/content/VolumeSettings;)Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/VolumeSettings;

    new-instance v0, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/VolumeSettings;->getVolume()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->volumeIntToString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;->streamVolumeSetting:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/VolumeSettings;->getNotificationsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ON"

    :goto_0
    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;->notificationSetting:Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v1, "OFF"

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/SetVolumeControlsResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SetVolumeControlsResponse;->value:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SetVolumeControls: unexpected server failure."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mCircleToVolumeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mCircleToVolumeMap:Ljava/util/HashMap;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->setCircleVolumes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "CIRCLE"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mVolumeSettings:Lcom/google/android/apps/plus/content/VolumeSettings;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->setCircleVolume(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V

    goto :goto_0

    :cond_3
    const-string v0, "SQUARES"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mVolumeSettings:Lcom/google/android/apps/plus/content/VolumeSettings;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->setSquareVolume(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SetVolumeControlsRequest;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mCircleToVolumeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mCircleToVolumeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/VolumeSettings;

    new-instance v4, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;-><init>()V

    new-instance v5, Lcom/google/api/services/plusi/model/VolumeControlKey;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/VolumeControlKey;-><init>()V

    iput-object v5, v4, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    iget-object v5, v4, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getFocusCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/google/api/services/plusi/model/VolumeControlKey;->focusGroupId:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    const-string v5, "CIRCLE"

    iput-object v5, v1, Lcom/google/api/services/plusi/model/VolumeControlKey;->type:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->createVolumeSettings(Lcom/google/android/apps/plus/content/VolumeSettings;)Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    move-result-object v0

    iput-object v0, v4, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->volumeSettings:Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;-><init>()V

    new-instance v1, Lcom/google/api/services/plusi/model/VolumeControlKey;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/VolumeControlKey;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    iput-object v3, v1, Lcom/google/api/services/plusi/model/VolumeControlKey;->type:Ljava/lang/String;

    const-string v1, "CIRCLE"

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getFocusCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/api/services/plusi/model/VolumeControlKey;->focusGroupId:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mVolumeSettings:Lcom/google/android/apps/plus/content/VolumeSettings;

    invoke-static {v1}, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->createVolumeSettings(Lcom/google/android/apps/plus/content/VolumeSettings;)Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->volumeSettings:Lcom/google/api/services/plusi/model/VolumeControlVolumeSettings;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Lcom/google/api/services/plusi/model/VolumeControlMap;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/VolumeControlMap;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetVolumeControlsRequest;->values:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SetVolumeControlsRequest;->values:Lcom/google/api/services/plusi/model/VolumeControlMap;

    iput-object v2, v0, Lcom/google/api/services/plusi/model/VolumeControlMap;->volumePair:Ljava/util/List;

    return-void

    :cond_2
    const-string v1, "SQUARES"

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/api/services/plusi/model/VolumeControlVolumePair;->key:Lcom/google/api/services/plusi/model/VolumeControlKey;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mId:Ljava/lang/String;

    iput-object v3, v1, Lcom/google/api/services/plusi/model/VolumeControlKey;->obfuscatedGaiaId:Ljava/lang/String;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid volume control type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
