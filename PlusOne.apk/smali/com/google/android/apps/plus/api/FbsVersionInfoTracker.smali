.class public final Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;
.super Ljava/lang/Object;
.source "FbsVersionInfoTracker.java"


# static fields
.field private static sTimestamp:J

.field private static sVersionInfo:Ljava/lang/String;


# direct methods
.method public static getVersionInfo()Ljava/lang/String;
    .locals 6

    sget-object v2, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sVersionInfo:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sTimestamp:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0xdbba0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sVersionInfo:Ljava/lang/String;

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sVersionInfo:Ljava/lang/String;

    return-object v2
.end method

.method public static setVersionInfo(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sVersionInfo:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    sput-wide v0, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->sTimestamp:J

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
