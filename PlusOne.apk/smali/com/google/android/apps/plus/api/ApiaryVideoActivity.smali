.class public Lcom/google/android/apps/plus/api/ApiaryVideoActivity;
.super Lcom/google/android/apps/plus/api/ApiaryActivity;
.source "ApiaryVideoActivity.java"


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mImage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/MediaLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;->mDisplayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;->mImage:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "empty media item"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    if-nez v2, :cond_2

    new-instance v1, Ljava/io/IOException;

    const-string v2, "missing image object"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;->mImage:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;->mDisplayName:Ljava/lang/String;

    return-void
.end method
