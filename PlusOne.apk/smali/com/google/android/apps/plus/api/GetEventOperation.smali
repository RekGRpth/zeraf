.class public final Lcom/google/android/apps/plus/api/GetEventOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/GetEventOperation$EventQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventReadRequest;",
        "Lcom/google/api/services/plusi/model/EventLeafResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthKey:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "eventread"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventLeafResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventLeafResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mAuthKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/EventLeafResponse;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventLeafResponse;->activityId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/EventLeafResponse;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const-wide/16 v9, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/api/GetEventOperation$EventQuery;->PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v9

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/EventLeafResponse;->update:Lcom/google/api/services/plusi/model/Update;

    move-object v11, v7

    invoke-static/range {v0 .. v11}, Lcom/google/android/apps/plus/content/EsEventData;->updateEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-object v6, v7

    move-object v5, v7

    goto :goto_0
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0x194

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x190

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mEventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HttpTransaction"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "HttpTransaction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GET_EVENT] received error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; disable IS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v4, 0x0

    const/4 v7, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/EventReadRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;-><init>()V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;->maxFrames:Ljava/lang/Integer;

    new-instance v1, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;-><init>()V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;->maxComments:Ljava/lang/Integer;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;-><init>()V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;->maxPhotos:Ljava/lang/Integer;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;-><init>()V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeActivityId:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeUpdate:Ljava/lang/Boolean;

    new-instance v4, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mEventId:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetEventOperation;->mAuthKey:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/ReadOptions;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/ReadOptions;-><init>()V

    iput-object v2, v6, Lcom/google/api/services/plusi/model/ReadOptions;->photosOptions:Ljava/util/List;

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->framesOptions:Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    iput-object v1, v6, Lcom/google/api/services/plusi/model/ReadOptions;->commentsOptions:Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    const-string v0, "LIST"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->responseFormat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->includePlusEvent:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->resolvePersons:Ljava/lang/Boolean;

    iput-object v3, v6, Lcom/google/api/services/plusi/model/ReadOptions;->eventUpdateOptions:Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v5, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->readOptions:Ljava/util/List;

    iput-object v4, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    return-void
.end method
