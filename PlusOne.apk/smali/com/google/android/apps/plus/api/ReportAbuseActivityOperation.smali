.class public final Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "ReportAbuseActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;",
        "Lcom/google/api/services/plusi/model/ReportAbuseActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAbuseType:Ljava/lang/String;

.field private final mActivityId:Ljava/lang/String;

.field private final mCommentId:Ljava/lang/String;

.field private final mDeleteComment:Z

.field private final mIsUndo:Z

.field private final mSourceStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .param p9    # Ljava/lang/String;
    .param p10    # Z

    const-string v4, "reportabuseactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbuseActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbuseActivityResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mSourceStreamId:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAbuseType:Ljava/lang/String;

    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mDeleteComment:Z

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mIsUndo:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Z

    const/4 v6, 0x0

    const-string v9, "SPAM"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mDeleteComment:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->itemId:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mIsUndo:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->isUndo:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/DataAbuseReport;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataAbuseReport;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAbuseType:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAbuseReport;->abuseType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mSourceStreamId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAbuseReport;->destinationStreamId:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
