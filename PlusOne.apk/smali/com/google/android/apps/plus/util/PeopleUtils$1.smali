.class final Lcom/google/android/apps/plus/util/PeopleUtils$1;
.super Ljava/lang/Object;
.source "PeopleUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/plus/content/PersonData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/content/PersonData;

    check-cast p2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/StringUtils;->safeStringCompareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
