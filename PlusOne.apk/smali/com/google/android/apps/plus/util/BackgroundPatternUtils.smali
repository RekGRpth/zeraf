.class public final Lcom/google/android/apps/plus/util/BackgroundPatternUtils;
.super Ljava/lang/Object;
.source "BackgroundPatternUtils.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

.field private static sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBackgroundPattern(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/BackgroundPatternUtils;
    .locals 6
    .param p0    # Landroid/content/Context;

    sget-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    if-nez v3, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v3, 0x4

    new-array v4, v3, [Landroid/graphics/drawable/BitmapDrawable;

    const/4 v5, 0x0

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_blue_tile:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    aput-object v3, v4, v5

    const/4 v5, 0x1

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_green_tile:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    aput-object v3, v4, v5

    const/4 v5, 0x2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_red_tile:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    aput-object v3, v4, v5

    const/4 v5, 0x3

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_yellow_tile:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    aput-object v3, v4, v5

    sput-object v4, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    const/4 v0, 0x0

    sget-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    array-length v2, v3

    :goto_0
    if-ge v0, v2, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v3, v3, v0

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    sget-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sTiledPlusStageDrawables:[Landroid/graphics/drawable/BitmapDrawable;

    aget-object v3, v3, v0

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeY(Landroid/graphics/Shader$TileMode;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    invoke-direct {v3}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sInstance:Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    :cond_1
    sget-object v3, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->sInstance:Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    return-object v3
.end method
