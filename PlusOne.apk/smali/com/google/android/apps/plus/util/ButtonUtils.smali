.class public final Lcom/google/android/apps/plus/util/ButtonUtils;
.super Ljava/lang/Object;
.source "ButtonUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;
    }
.end annotation


# static fields
.field private static final sButtonInfos:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/ButtonUtils;->sButtonInfos:Landroid/util/SparseArray;

    return-void
.end method

.method public static getButton(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Ljava/lang/CharSequence;I)Lcom/google/android/apps/plus/views/ClickableButton;
    .locals 15
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p7    # Ljava/lang/CharSequence;
    .param p8    # I

    move/from16 v0, p5

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButtonInfo(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;

    move-result-object v14

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v5, v14, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    iget-object v6, v14, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v7, v14, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v12, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v8, p6

    move/from16 v9, p3

    move/from16 v10, p4

    move-object/from16 v11, p7

    move/from16 v13, p8

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;ZI)V

    return-object v1
.end method

.method public static getButton(Landroid/content/Context;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    invoke-static {p0, p2}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButtonInfo(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    const/4 v2, 0x0

    iget-object v4, v1, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    iget-object v5, v1, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v6, v1, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v11, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object/from16 v7, p5

    move v8, p3

    move/from16 v9, p4

    move-object v10, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    return-object v0
.end method

.method private static getButtonInfo(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v3, 0x3

    sget-object v1, Lcom/google/android/apps/plus/util/ButtonUtils;->sButtonInfos:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;-><init>(B)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    packed-switch p1, :pswitch_data_0

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/util/ButtonUtils;->sButtonInfos:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white_pressed:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white_pressed:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v1, 0x4

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_2
    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_default_gray:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_default_gray_pressed:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_3
    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_blue:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->defaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_blue_pressed:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->pressedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v1, 0x5

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/util/ButtonUtils$ButtonInfo;->textPaint:Landroid/text/TextPaint;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
