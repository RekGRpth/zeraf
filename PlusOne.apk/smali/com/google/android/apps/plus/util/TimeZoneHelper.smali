.class public final Lcom/google/android/apps/plus/util/TimeZoneHelper;
.super Ljava/lang/Object;
.source "TimeZoneHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;,
        Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;
    }
.end annotation


# static fields
.field private static sGenericTimeZonePrefix:Ljava/lang/String;

.field private static sTimeZoneFormat:Ljava/lang/String;


# instance fields
.field private mCalendar:Ljava/util/Calendar;

.field private mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/LongSparseArray",
            "<",
            "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mOrderedTimeZoneInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method public static areTimeZoneIdsEquivalent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static buildMapping([Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Lvedroid/support/v4/util/LongSparseArray;
    .locals 12
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Calendar;",
            ")",
            "Lvedroid/support/v4/util/LongSparseArray",
            "<",
            "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;",
            ">;"
        }
    .end annotation

    new-instance v8, Lvedroid/support/v4/util/LongSparseArray;

    invoke-direct {v8}, Lvedroid/support/v4/util/LongSparseArray;-><init>()V

    array-length v9, p0

    add-int/lit8 v1, v9, -0x1

    :goto_0
    if-ltz v1, :cond_3

    aget-object v9, p0, v1

    invoke-static {v9}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-static {v6, p2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v4

    invoke-virtual {v8, v4, v5}, Lvedroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;-><init>()V

    invoke-virtual {v8, v4, v5, v0}, Lvedroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    :cond_0
    new-instance v9, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-direct {v9, v6}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;-><init>(Ljava/util/TimeZone;)V

    invoke-virtual {v9, v4, v5}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->setOffset(J)V

    invoke-virtual {v6}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mSeenDisplayNames:Ljava/util/HashSet;

    invoke-virtual {v11, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    if-eqz p1, :cond_2

    invoke-virtual {v10, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    iget-object v11, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mExcludedTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v9, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mSeenDisplayNames:Ljava/util/HashSet;

    invoke-virtual {v9, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    iget-object v11, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Lvedroid/support/v4/util/LongSparseArray;->size()I

    move-result v9

    add-int/lit8 v1, v9, -0x1

    :goto_2
    if-ltz v1, :cond_5

    invoke-virtual {v8, v1}, Lvedroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, Lvedroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;

    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mExcludedTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mExcludedTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    iget-object v10, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mExcludedTimeZoneInfoList:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mExcludedTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    iget-object v9, v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_5
    return-object v8
.end method

.method public static getDisplayString(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/util/Calendar;
    .param p2    # Z

    invoke-static {p0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v1

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->areTimeZoneIdsEquivalent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez p2, :cond_0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J
    .locals 11
    .param p0    # Ljava/util/TimeZone;
    .param p1    # Ljava/util/Calendar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v5

    mul-int/lit8 v0, v7, 0x3c

    add-int/2addr v0, v8

    const v9, 0xea60

    mul-int v6, v0, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v0

    int-to-long v9, v0

    return-wide v9
.end method

.method public static getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method private getTimeZoneInfo(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v13}, Lvedroid/support/v4/util/LongSparseArray;->size()I

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    if-nez p2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v11

    :cond_0
    :goto_0
    return-object v11

    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    const/4 v3, 0x0

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->areTimeZoneIdsEquivalent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-virtual {v10}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mCalendar:Ljava/util/Calendar;

    invoke-static {v10, v13}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :cond_2
    if-lez v8, :cond_9

    const/4 v6, 0x0

    if-eqz p2, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lvedroid/support/v4/util/LongSparseArray;->indexOfKey(J)I

    move-result v6

    if-gez v6, :cond_3

    const/4 v6, 0x0

    :cond_3
    :goto_1
    if-ge v6, v8, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v13, v6}, Lvedroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v13, v1, v2}, Lvedroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;

    iget-object v12, v13, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v7, :cond_8

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    cmp-long v13, v1, v13

    if-nez v13, :cond_4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    :cond_4
    if-nez v4, :cond_5

    if-nez v5, :cond_6

    :cond_5
    move-object v9, v11

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_7
    if-nez p2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v11

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_9
    move-object v11, v9

    goto/16 :goto_0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->sTimeZoneFormat:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->time_zone_utc_format:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->sTimeZoneFormat:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->time_zone_generic_system_prefix:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->sGenericTimeZonePrefix:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public final configure$622086bc(Ljava/util/Calendar;)V
    .locals 13
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mCalendar:Ljava/util/Calendar;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOrderedTimeZoneInfoList:Ljava/util/List;

    invoke-static {}, Ljava/util/TimeZone;->getAvailableIDs()[Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/plus/util/TimeZoneHelper;->sGenericTimeZonePrefix:Ljava/lang/String;

    invoke-static {v11, v12, p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->buildMapping([Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Lvedroid/support/v4/util/LongSparseArray;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    iget-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11}, Lvedroid/support/v4/util/LongSparseArray;->size()I

    move-result v2

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11, v0}, Lvedroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    iget-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOffsetToTimeZonesMapping:Lvedroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11, v6, v7}, Lvedroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;

    iget-object v10, v9, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneGroup;->mTimeZoneInfoList:Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    const/4 v1, 0x0

    move v4, v3

    :goto_1
    if-ge v1, v5, :cond_0

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    add-int/lit8 v3, v4, 0x1

    invoke-virtual {v8, v4}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->setPosition(I)V

    iget-object v11, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOrderedTimeZoneInfoList:Ljava/util/List;

    invoke-interface {v11, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    move v4, v3

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    move v3, v4

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mCalendar:Ljava/util/Calendar;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZoneInfo(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v1

    return-object v1
.end method

.method public final getOffset(Ljava/util/TimeZone;)J
    .locals 2
    .param p1    # Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mCalendar:Ljava/util/Calendar;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;Ljava/util/Calendar;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getTimeZone(Ljava/lang/String;Ljava/lang/Long;)Ljava/util/TimeZone;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZoneInfo(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public final getTimeZoneInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper;->mOrderedTimeZoneInfoList:Ljava/util/List;

    return-object v0
.end method

.method public final getTimeZonePos(Ljava/lang/String;Ljava/lang/Long;)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZoneInfo(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getPosition()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
