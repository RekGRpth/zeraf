.class public final enum Lcom/google/android/apps/plus/util/Property;
.super Ljava/lang/Enum;
.source "Property.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/Property$OverrideType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/util/Property;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/util/Property;

.field public static final enum ACTIVE_HANGOUT_MODE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ADD_PHONE_ON_PROFILE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ASPEN_MAX_INDIVIDUAL_ACLS:Lcom/google/android/apps/plus/util/Property;

.field public static final enum AUTH_EMAIL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum AUTH_PASSWORD:Lcom/google/android/apps/plus/util/Property;

.field public static final enum AUTH_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum EMOTISHARE_GEN1_DATE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum EMOTISHARE_GEN2_DATE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum EMOTISHARE_GEN3_DATE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_ADVANCED_HANGOUTS:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_ASPEN_MIXIN:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_ASPEN_SETTINGS_V2:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HANGOUT_FILMSTRIP_STATUS:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HANGOUT_RECORD_ABUSE_INTERSTITIAL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HANGOUT_STAGE_STATUS:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HANGOUT_SWITCH:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_HOST_ACTIVITY:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_INSTANT_SHARE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_INSTANT_SHARE_VIDEO:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_LAB1:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_LOCAL_PAGE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_LOCAL_SWITCHER:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_PLUS_PAGES:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_REWIEWS:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_SHAKE_GLOBAL_ACTION:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_SKINNY_PLUS_PAGE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_STREAM_GIF_ANIMATION:Lcom/google/android/apps/plus/util/Property;

.field public static final enum ENABLE_TILES:Lcom/google/android/apps/plus/util/Property;

.field public static final enum FORCE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum HANGOUT_CAMERA_MIRRORED:Lcom/google/android/apps/plus/util/Property;

.field public static final enum HANGOUT_CAMERA_ORIENTATION:Lcom/google/android/apps/plus/util/Property;

.field public static final enum HANGOUT_STRESS_MODE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum IS_AUTOMATION_BUILD:Lcom/google/android/apps/plus/util/Property;

.field public static final enum LOCATION_DEBUGGING:Lcom/google/android/apps/plus/util/Property;

.field public static final enum NATIVE_HANGOUT_LOG:Lcom/google/android/apps/plus/util/Property;

.field public static final enum NATIVE_WRAPPER_HANGOUT_LOG_LEVEL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_APIARY_AUTH_TOKEN:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_STATICMAPS_API_KEY:Lcom/google/android/apps/plus/util/Property;

.field public static final enum PLUS_TRACE:Lcom/google/android/apps/plus/util/Property;

.field public static final enum POS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum POS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

.field public static final enum POS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum SOCIAL_ADS_URL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum TRACING_LEVEL:Lcom/google/android/apps/plus/util/Property;

.field public static final enum TRACING_PATH:Lcom/google/android/apps/plus/util/Property;

.field public static final enum TRACING_TOKEN:Lcom/google/android/apps/plus/util/Property;

.field public static final enum TRACING_TOKEN_2:Lcom/google/android/apps/plus/util/Property;

.field public static final enum WARM_WELCOME_ON_LOGIN:Lcom/google/android/apps/plus/util/Property;

.field private static sProperties:Ljava/util/Properties;


# instance fields
.field private final mDefaultValue:Ljava/lang/String;

.field private final mExperimentId:Ljava/lang/String;

.field private final mKey:Ljava/lang/String;

.field private final mOverride:Lcom/google/android/apps/plus/util/Property$OverrideType;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v13, 0x2

    const/4 v2, 0x0

    const/16 v12, 0x1b

    const/16 v11, 0xb

    const/4 v10, 0x1

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_DOGFOOD_FEATURES"

    const-string v3, "debug.plus.dogfood"

    const-string v4, "false"

    sget-object v5, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "AUTH_URL"

    const-string v3, "debug.plus.auth.url"

    invoke-direct {v0, v1, v10, v3}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->AUTH_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "AUTH_EMAIL"

    const-string v3, "debug.plus.auth.email"

    invoke-direct {v0, v1, v13, v3}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->AUTH_EMAIL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "AUTH_PASSWORD"

    const/4 v3, 0x3

    const-string v4, "debug.plus.auth.password"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->AUTH_PASSWORD:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "SOCIAL_ADS_URL"

    const/4 v3, 0x4

    const-string v4, "debug.plus.safe.url"

    const-string v5, "https://googleads.g.doubleclick.net/pagead/drt/m"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->SOCIAL_ADS_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "NATIVE_HANGOUT_LOG"

    const/4 v3, 0x5

    const-string v4, "debug.plus.hangout.native"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_HANGOUT_LOG:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "NATIVE_WRAPPER_HANGOUT_LOG_LEVEL"

    const/4 v3, 0x6

    const-string v4, "debug.plus.hangout.tag.wrapper"

    const-string v5, "WARNING"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_WRAPPER_HANGOUT_LOG_LEVEL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "HANGOUT_CAMERA_ORIENTATION"

    const/4 v3, 0x7

    const-string v4, "debug.plus.camera.orientation"

    const-string v5, ""

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_ORIENTATION:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "HANGOUT_CAMERA_MIRRORED"

    const/16 v3, 0x8

    const-string v4, "debug.plus.camera.mirrored"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_MIRRORED:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "HANGOUT_STRESS_MODE"

    const/16 v3, 0x9

    const-string v4, "debug.plus.hangout.stress"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->HANGOUT_STRESS_MODE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HANGOUT_SWITCH"

    const/16 v3, 0xa

    const-string v4, "debug.plus.hangout.switch"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_SWITCH:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_ADVANCED_HANGOUTS"

    const-string v3, "debug.plus.hangout.enable_adv"

    const-string v4, "TRUE"

    invoke-direct {v0, v1, v11, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_ADVANCED_HANGOUTS:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HANGOUT_STAGE_STATUS"

    const/16 v3, 0xc

    const-string v4, "debug.plus.hangout.stage_icon"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_STAGE_STATUS:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HANGOUT_FILMSTRIP_STATUS"

    const/16 v3, 0xd

    const-string v4, "debug.plus.hangout.strip_icon"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_FILMSTRIP_STATUS:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HANGOUT_RECORD_ABUSE"

    const/16 v3, 0xe

    const-string v4, "debug.plus.enable.rec_abuse"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HANGOUT_RECORD_ABUSE_INTERSTITIAL"

    const/16 v3, 0xf

    const-string v4, "debug.plus.rec_abuse.warning"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE_INTERSTITIAL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "FORCE_HANGOUT_RECORD_ABUSE"

    const/16 v3, 0x10

    const-string v4, "debug.plus.force.rec_abuse"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->FORCE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ACTIVE_HANGOUT_MODE"

    const/16 v3, 0x11

    const-string v4, "debug.plus.hangout.active_mode"

    const-string v5, "DISABLE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ACTIVE_HANGOUT_MODE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_SKINNY_PLUS_PAGE"

    const/16 v3, 0x12

    const-string v4, "debug.plus.enable.skinny"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_SKINNY_PLUS_PAGE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_TRACE"

    const/16 v3, 0x13

    const-string v4, "debug.plus.trace"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_TRACE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_CLIENTID"

    const/16 v3, 0x14

    const-string v4, "debug.plus.clientid"

    const-string v5, "862067606707.apps.googleusercontent.com"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "PLUS_STATICMAPS_API_KEY"

    const/16 v5, 0x15

    const-string v6, "debug.plus.staticmaps.api_key"

    const-string v7, "AIzaSyAYfoSs86LzFMXNWJhyeGtZp0ijdZb_uGU"

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->PLUS_STATICMAPS_API_KEY:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_FRONTEND_PATH"

    const/16 v3, 0x16

    const-string v4, "debug.plus.frontend.path"

    const-string v5, "/plusi/v2/ozInternal/"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_FRONTEND_URL"

    const/16 v3, 0x17

    const-string v4, "debug.plus.frontend.url"

    const-string v5, "www.googleapis.com"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_APIARY_AUTH_TOKEN"

    const/16 v3, 0x18

    const-string v4, "debug.plus.apiary_token"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_APIARY_AUTH_TOKEN:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "PLUS_BACKEND_URL"

    const/16 v3, 0x19

    const-string v4, "debug.plus.backend.url"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "POS_FRONTEND_URL"

    const/16 v3, 0x1a

    const-string v4, "debug.pos.frontend.url"

    const-string v5, "www.googleapis.com"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "POS_FRONTEND_PATH"

    const-string v3, "debug.pos.frontend.path"

    const-string v4, "/pos/v1/"

    invoke-direct {v0, v1, v12, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "POS_BACKEND_URL"

    const/16 v3, 0x1c

    const-string v4, "debug.pos.backend.url"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->POS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "WARM_WELCOME_ON_LOGIN"

    const/16 v3, 0x1d

    const-string v4, "debug.plus.warm.welcome"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->WARM_WELCOME_ON_LOGIN:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "LOCATION_DEBUGGING"

    const/16 v3, 0x1e

    const-string v4, "debug.plus.location.toast"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->LOCATION_DEBUGGING:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ADD_PHONE_ON_PROFILE"

    const/16 v3, 0x1f

    const-string v4, "debug.plus.add.phone.number"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ADD_PHONE_ON_PROFILE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_PLUS_PAGES"

    const/16 v3, 0x20

    const-string v4, "debug.plus.enable_plus_pages"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_PLUS_PAGES:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "TRACING_TOKEN"

    const/16 v3, 0x21

    const-string v4, "debug.plus.tracing_token"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->TRACING_TOKEN:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "TRACING_TOKEN_2"

    const/16 v3, 0x22

    const-string v4, "debug.plus.tracing_token2"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->TRACING_TOKEN_2:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "TRACING_PATH"

    const/16 v3, 0x23

    const-string v4, "debug.plus.tracing_path"

    const-string v5, ".*"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->TRACING_PATH:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "TRACING_LEVEL"

    const/16 v3, 0x24

    const-string v4, "debug.plus.tracing_level"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->TRACING_LEVEL:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_SHAKE_GLOBAL_ACTION"

    const/16 v3, 0x25

    const-string v4, "debug.plus.enable_shake_action"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_SHAKE_GLOBAL_ACTION:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_LOCAL_SWITCHER"

    const/16 v3, 0x26

    const-string v4, "debug.plus.enable_local"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCAL_SWITCHER:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_LOCAL_PAGE"

    const/16 v3, 0x27

    const-string v4, "debug.plus.enable_local_page"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCAL_PAGE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_REWIEWS"

    const/16 v3, 0x28

    const-string v4, "debug.plus.enable_reviews"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_REWIEWS:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_HOST_ACTIVITY"

    const/16 v3, 0x29

    const-string v4, "debug.plus.enable_host_activity"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HOST_ACTIVITY:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_INSTANT_SHARE"

    const/16 v3, 0x2a

    const-string v4, "debug.plus.enable_instant_share"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_INSTANT_SHARE_VIDEO"

    const/16 v3, 0x2b

    const-string v4, "debug.plus.enable_instant_share_video"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE_VIDEO:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_EMOTISHARE"

    const/16 v5, 0x2c

    const-string v6, "debug.plus.enable.emotishare"

    const-string v7, "FALSE"

    const-string v8, "2dab999b"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "EMOTISHARE_GEN1_DATE"

    const/16 v5, 0x2d

    const-string v6, "debug.plus.emotishare.gen1"

    const-string v7, "0"

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN1_DATE:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "EMOTISHARE_GEN2_DATE"

    const/16 v5, 0x2e

    const-string v6, "debug.plus.emotishare.gen2"

    new-instance v0, Ljava/util/GregorianCalendar;

    const/16 v1, 0x7dc

    invoke-direct {v0, v1, v11, v12}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN2_DATE:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "EMOTISHARE_GEN3_DATE"

    const/16 v5, 0x2f

    const-string v6, "debug.plus.emotishare.gen3"

    new-instance v0, Ljava/util/GregorianCalendar;

    const/16 v1, 0x7dd

    invoke-direct {v0, v1, v10, v10}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN3_DATE:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_STREAM_GIF_ANIMATION"

    const/16 v3, 0x30

    const-string v4, "debug.plus.enable.streamanim"

    const-string v5, "TRUE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_STREAM_GIF_ANIMATION:Lcom/google/android/apps/plus/util/Property;

    new-instance v0, Lcom/google/android/apps/plus/util/Property;

    const-string v1, "ENABLE_LAB1"

    const/16 v3, 0x31

    const-string v4, "debug.plus.enable.lab1"

    const-string v5, "FALSE"

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_LAB1:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_SQUARES"

    const/16 v5, 0x32

    const-string v6, "debug.plus.enable_squares"

    const-string v7, "FALSE"

    const-string v8, "1128676a"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_ASPEN_SETTINGS_V2"

    const/16 v5, 0x33

    const-string v6, "debug.plus.enable.aspen.settings.v2"

    const-string v7, "FALSE"

    const-string v8, "54100b94"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_SETTINGS_V2:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_ASPEN_MIXIN"

    const/16 v5, 0x34

    const-string v6, "debug.plus.enable.mixin"

    const-string v7, "FALSE"

    const-string v8, "cc08ed5d"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_MIXIN:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ASPEN_MAX_INDIVIDUAL_ACLS"

    const/16 v5, 0x35

    const-string v6, "debug.plus.post_max_individual_acls"

    const-string v7, "10"

    const-string v8, "83b5aecc"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ASPEN_MAX_INDIVIDUAL_ACLS:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_TILES"

    const/16 v5, 0x36

    const-string v6, "debug.plus.enable_tiles"

    const-string v7, "false"

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_DOGFOOD:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_TILES:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "ENABLE_LOCATION_SHARING"

    const/16 v5, 0x37

    const-string v6, "debug.plus.enable_location_sharing"

    const-string v7, "false"

    const-string v8, "8d04e73"

    sget-object v9, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_NEVER:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    new-instance v3, Lcom/google/android/apps/plus/util/Property;

    const-string v4, "IS_AUTOMATION_BUILD"

    const/16 v5, 0x38

    const-string v6, "debug.plus.testing.automation"

    const-string v7, "FALSE"

    sget-object v8, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_DOGFOOD:Lcom/google/android/apps/plus/util/Property$OverrideType;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    sput-object v3, Lcom/google/android/apps/plus/util/Property;->IS_AUTOMATION_BUILD:Lcom/google/android/apps/plus/util/Property;

    const/16 v0, 0x39

    new-array v0, v0, [Lcom/google/android/apps/plus/util/Property;

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->AUTH_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->AUTH_EMAIL:Lcom/google/android/apps/plus/util/Property;

    aput-object v1, v0, v13

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->AUTH_PASSWORD:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->SOCIAL_ADS_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->NATIVE_HANGOUT_LOG:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->NATIVE_WRAPPER_HANGOUT_LOG_LEVEL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_ORIENTATION:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_MIRRORED:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->HANGOUT_STRESS_MODE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_SWITCH:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_ADVANCED_HANGOUTS:Lcom/google/android/apps/plus/util/Property;

    aput-object v1, v0, v11

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_STAGE_STATUS:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_FILMSTRIP_STATUS:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE_INTERSTITIAL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->FORCE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ACTIVE_HANGOUT_MODE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_SKINNY_PLUS_PAGE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_TRACE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_STATICMAPS_API_KEY:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_APIARY_AUTH_TOKEN:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    aput-object v1, v0, v12

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->POS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->WARM_WELCOME_ON_LOGIN:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->LOCATION_DEBUGGING:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ADD_PHONE_ON_PROFILE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_PLUS_PAGES:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->TRACING_TOKEN:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->TRACING_TOKEN_2:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->TRACING_PATH:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->TRACING_LEVEL:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_SHAKE_GLOBAL_ACTION:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCAL_SWITCHER:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCAL_PAGE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_REWIEWS:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HOST_ACTIVITY:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE_VIDEO:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN1_DATE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN2_DATE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN3_DATE:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_STREAM_GIF_ANIMATION:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_LAB1:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_SETTINGS_V2:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_ASPEN_MIXIN:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ASPEN_MAX_INDIVIDUAL_ACLS:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_TILES:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->IS_AUTOMATION_BUILD:Lcom/google/android/apps/plus/util/Property;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/util/Property;->$VALUES:[Lcom/google/android/apps/plus/util/Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_ALWAYS:Lcom/google/android/apps/plus/util/Property$OverrideType;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    sget-object v5, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_ALWAYS:Lcom/google/android/apps/plus/util/Property$OverrideType;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V
    .locals 7
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/util/Property$OverrideType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/util/Property$OverrideType;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/util/Property;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/util/Property$OverrideType;)V
    .locals 1
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/util/Property$OverrideType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/util/Property$OverrideType;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/plus/util/Property;->mKey:Ljava/lang/String;

    invoke-static {p3, p4}, Lcom/google/android/apps/plus/util/Property;->getDefaultProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/Property;->mDefaultValue:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/util/Property;->mExperimentId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/util/Property;->mOverride:Lcom/google/android/apps/plus/util/Property$OverrideType;

    return-void
.end method

.method private static getDefaultProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->sProperties:Ljava/util/Properties;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/Property;->sProperties:Ljava/util/Properties;

    const-class v1, Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const-string v2, "com/google/android/apps/plusone/debug.prop"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->sProperties:Ljava/util/Properties;

    invoke-virtual {v1, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->sProperties:Ljava/util/Properties;

    invoke-virtual {v1, p0}, Ljava/util/Properties;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->sProperties:Ljava/util/Properties;

    invoke-virtual {v1, p0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1

    :catch_0
    move-exception v1

    const-string v1, "EsProperty"

    const-string v2, "Cannot load debug.prop"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getExperimentIds()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/android/apps/plus/util/Property;->values()[Lcom/google/android/apps/plus/util/Property;

    move-result-object v4

    move-object v0, v4

    array-length v3, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v5, v0, v1

    iget-object v6, v5, Lcom/google/android/apps/plus/util/Property;->mExperimentId:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v5, Lcom/google/android/apps/plus/util/Property;->mExperimentId:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/util/Property;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/util/Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/util/Property;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->$VALUES:[Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/util/Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/util/Property;

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/String;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/util/Property;->mExperimentId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/util/Property;->mExperimentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/util/Property;->mDefaultValue:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->getExperiment(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    if-ne p0, v2, :cond_1

    move-object v1, v0

    :goto_1
    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/util/Property;->mDefaultValue:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/util/Property;->mOverride:Lcom/google/android/apps/plus/util/Property$OverrideType;

    sget-object v3, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_ALWAYS:Lcom/google/android/apps/plus/util/Property$OverrideType;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/util/Property;->mOverride:Lcom/google/android/apps/plus/util/Property$OverrideType;

    sget-object v3, Lcom/google/android/apps/plus/util/Property$OverrideType;->OVERRIDE_DOGFOOD:Lcom/google/android/apps/plus/util/Property$OverrideType;

    if-ne v2, v3, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/util/Property;->mKey:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/util/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public final getAsLong()Ljava/lang/Long;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getBoolean()Z
    .locals 2

    const-string v0, "TRUE"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
