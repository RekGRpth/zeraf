.class final Lcom/google/android/apps/plus/hangout/VideoView$Renderer;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Renderer"
.end annotation


# instance fields
.field private disabled:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/VideoView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/VideoView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;-><init>(Lcom/google/android/apps/plus/hangout/VideoView;)V

    return-void
.end method


# virtual methods
.method public final onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->reinitializeRenderer:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$100(Lcom/google/android/apps/plus/hangout/VideoView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/VideoView;->access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initializeIncomingVideoRenderer(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_0

    const-string v0, "initializeIncomingVideoRenderer failed. Rendering disabled"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/VideoView;->access$102(Lcom/google/android/apps/plus/hangout/VideoView;Z)Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_3

    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/VideoView;->access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->renderIncomingVideoFrame(I)Z

    goto :goto_1
.end method

.method public final onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->reinitializeRenderer:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$100(Lcom/google/android/apps/plus/hangout/VideoView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/VideoView;->access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initializeIncomingVideoRenderer(I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_0

    const-string v0, "initializeIncomingVideoRenderer failed. Rendering disabled"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/hangout/VideoView;->access$102(Lcom/google/android/apps/plus/hangout/VideoView;Z)Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_4

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/VideoView;->access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I

    move-result v3

    invoke-virtual {v0, v3, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoRendererSurfaceSize(III)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_2

    const-string v0, "setIncomingVideoRendererSurfaceSize failed. Rendering disabled"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/VideoView;->access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initializeIncomingVideoRenderer(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;->disabled:Z

    if-eqz v0, :cond_0

    const-string v0, "initializeIncomingVideoRenderer failed. Rendering disabled"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
