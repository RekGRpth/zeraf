.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;
.super Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;
.source "HangoutTabletTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CenterStageTouchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHeight()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    cmpl-float v4, p4, v5

    if-lez v4, :cond_1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, p4, v4

    if-lez v4, :cond_1

    if-lt v1, v2, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideChild(Landroid/view/View;)V

    :goto_0
    return v3

    :cond_0
    if-gt v1, v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    goto :goto_0

    :cond_1
    cmpg-float v4, p4, v5

    if-gez v4, :cond_3

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    neg-float v4, v4

    cmpg-float v4, p4, v4

    if-gez v4, :cond_3

    if-lt v1, v2, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showChild(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    if-gt v1, v2, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    goto :goto_0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent$PointerCoords;
    .param p2    # Landroid/view/MotionEvent$PointerCoords;
    .param p3    # F
    .param p4    # F

    const/4 v3, 0x1

    iget v4, p1, Landroid/view/MotionEvent$PointerCoords;->y:F

    iget v5, p2, Landroid/view/MotionEvent$PointerCoords;->y:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x437a0000

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getWidth()I

    move-result v0

    iget v4, p1, Landroid/view/MotionEvent$PointerCoords;->x:F

    float-to-int v2, v4

    iget v4, p2, Landroid/view/MotionEvent$PointerCoords;->x:F

    float-to-int v1, v4

    if-ge v2, v1, :cond_0

    div-int/lit8 v4, v0, 0x2

    if-ge v2, v4, :cond_1

    mul-int/lit8 v4, v0, 0x7

    div-int/lit8 v4, v4, 0x8

    if-le v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V

    :goto_0
    return v3

    :cond_0
    div-int/lit8 v4, v0, 0x2

    if-le v2, v4, :cond_1

    div-int/lit8 v4, v0, 0x8

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z

    move-result v3

    goto :goto_0
.end method
