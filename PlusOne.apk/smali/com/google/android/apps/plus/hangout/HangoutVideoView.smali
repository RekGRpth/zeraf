.class public abstract Lcom/google/android/apps/plus/hangout/HangoutVideoView;
.super Landroid/widget/RelativeLayout;
.source "HangoutVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutVideoView$1;,
        Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;
    }
.end annotation


# instance fields
.field private final mAudiodMutedStatusView:Landroid/view/View;

.field private final mAvatarView:Landroid/widget/ImageView;

.field private final mBackgoundLogo:Landroid/widget/ImageView;

.field private final mBlockedView:Landroid/view/View;

.field private final mCameraErrorView:Landroid/view/View;

.field private final mDispSize:Landroid/graphics/Rect;

.field private final mDisplay:Landroid/view/Display;

.field private final mFlashToggleButton:Landroid/widget/ImageButton;

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

.field private final mPausedView:Landroid/view/View;

.field private final mPinnedStatusView:Landroid/view/View;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private final mSnapshotView:Landroid/widget/ImageView;

.field private mVideoSurface:Landroid/view/View;

.field private final mVolumeBar:Lcom/google/android/apps/plus/views/Thermometer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, -0x2

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDisplay:Landroid/view/Display;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDispSize:Landroid/graphics/Rect;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$layout;->hangout_video_view:I

    const/4 v4, 0x1

    invoke-virtual {v0, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_video_avatar:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAvatarView:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_video_blocked:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mBlockedView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_video_paused:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPausedView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_camera_error:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mCameraErrorView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_toggle_flash_light_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mFlashToggleButton:Landroid/widget/ImageButton;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_background_logo:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mBackgoundLogo:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_video_snapshot:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mSnapshotView:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_video_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mRootView:Landroid/widget/RelativeLayout;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_audio_muted_status:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAudiodMutedStatusView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_pinned_status:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPinnedStatusView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_volume:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/Thermometer;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVolumeBar:Lcom/google/android/apps/plus/views/Thermometer;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public final getAvatarView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAvatarView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getFlashToggleButton()Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mFlashToggleButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public final getSnapshotView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mSnapshotView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final hideAudioMutedStatus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAudiodMutedStatusView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final hideAvatar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAvatarView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final hideBlocked()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mBlockedView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final hideLogo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mBackgoundLogo:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final hidePaused()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPausedView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final hidePinnedStatus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPinnedStatusView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final hideVideoSurface()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final hideVolumeBar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVolumeBar:Lcom/google/android/apps/plus/views/Thermometer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/Thermometer;->setVisibility(I)V

    return-void
.end method

.method public final isAudioMuteStatusShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAudiodMutedStatusView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isHangoutTileStarted()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->isTileStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoShowing()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final layoutVideo(IIII)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutVideoView$1;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutVideoView$LayoutMode:[I

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown layout mode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    new-instance v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v2, p3, p4}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v4, v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v4, "HangoutVideo.layout: mode=%s  video=%d,%d  parent=%d,%d   new dimensions=%d,%d  self=%s"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget v7, v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    iget v7, v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    if-nez p2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v2, p3, p4}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    goto :goto_1

    :cond_0
    int-to-double v4, p1

    int-to-double v6, p2

    div-double v0, v4, v6

    invoke-static {v0, v1, p3, p4}, Lcom/google/android/apps/plus/hangout/Utils;->fitContentInContainer(DII)Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getPaddingRight()I

    move-result v5

    add-int v3, v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getPaddingBottom()I

    move-result v5

    add-int v2, v4, v5

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    sub-int v1, v4, v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    sub-int v0, v4, v2

    if-lez v1, :cond_0

    if-gtz v0, :cond_2

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDisplay:Landroid/view/Display;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDispSize:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    if-gtz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDispSize:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v1, v4, v3

    :cond_1
    if-gtz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mDispSize:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int v0, v4, v2

    :cond_2
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->onMeasure$3b4dfe4b(II)V

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method

.method public onMeasure$3b4dfe4b(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method public setBackgroundViewColor(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    return-void
.end method

.method public final setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method

.method public final setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mLayoutMode:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->requestLayout()V

    return-void
.end method

.method public final setVideoSurface(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mRootView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mRootView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->requestLayout()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVolume(I)V
    .locals 5
    .param p1    # I

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v0, 0x9

    if-le p1, v0, :cond_1

    const/16 p1, 0x9

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVolumeBar:Lcom/google/android/apps/plus/views/Thermometer;

    int-to-double v1, p1

    const-wide/high16 v3, 0x4022000000000000L

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/Thermometer;->setFillLevel(D)V

    return-void
.end method

.method public final showAudioMutedStatus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAudiodMutedStatusView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showAvatar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mSnapshotView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mAvatarView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showBlocked()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mBlockedView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showCameraError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mCameraErrorView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showPaused()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPausedView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showPinnedStatus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mPinnedStatusView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showVideoSurface()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVideoSurface:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final showVolumeBar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->mVolumeBar:Lcom/google/android/apps/plus/views/Thermometer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/Thermometer;->setVisibility(I)V

    return-void
.end method
