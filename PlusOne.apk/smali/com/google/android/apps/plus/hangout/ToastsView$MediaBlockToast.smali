.class final Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;
.super Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;
.source "ToastsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/ToastsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaBlockToast"
.end annotation


# instance fields
.field private final mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final mIsRecording:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/ToastsView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;B)V

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->FORCE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p4, 0x1

    :cond_0
    iput-boolean p4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mIsRecording:Z

    return-void
.end method


# virtual methods
.method final populateView(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 8
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/widget/TextView;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mIsRecording:Z

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_recording_abuse:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/hangout/Avatars;->renderAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/MeetingMember;Landroid/widget/ImageView;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v4

    if-eqz v4, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_media_block_by_self:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v4

    if-eqz v4, :cond_2

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_media_block_to_self:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_media_block:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
