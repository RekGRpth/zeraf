.class public Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
.super Lcom/google/android/apps/plus/hangout/RemoteVideoView;
.source "RemoteVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/RemoteVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CenterStageVideoView"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected final startVideo()V
    .locals 7

    const/16 v6, 0xf

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->incomingVideoContainerWidth:I

    iget v5, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->incomingVideoContainerHeight:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->startIncomingVideoForSpeakerIndex(IIII)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->requestID:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->incomingVideoContainerWidth:I

    iget v5, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->incomingVideoContainerHeight:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->startIncomingVideoForUser(Ljava/lang/String;III)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->requestID:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0
.end method

.method public final updateVideoStreaming()V
    .locals 5

    iget v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->requestID:I

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne v3, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    goto :goto_1

    :cond_3
    if-nez v2, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    invoke-interface {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;->onVideoSourceAboutToChange$1385ff()V

    :cond_4
    sget-object v3, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    iget v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->requestID:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoSourceToSpeakerIndex(II)V

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    iget v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->requestID:I

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoSourceToUser(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    invoke-interface {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;->onVideoSourceAboutToChange$1385ff()V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onPause()V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onResume()V

    goto :goto_0
.end method
