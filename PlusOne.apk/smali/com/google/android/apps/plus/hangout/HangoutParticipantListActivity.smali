.class public Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HangoutParticipantListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mParticipantList:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private canInviteMoreParticipants()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inviteMoreParticipants()V
    .locals 13

    const/4 v7, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_invite_menu_item_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {v11}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->makePersonFromParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v0, 0x0

    invoke-direct {v3, v12, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x5

    move-object v0, p0

    move v6, v5

    move v8, v7

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {p0, v10, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    if-nez p1, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    if-eqz p3, :cond_0

    const-string v1, "audience"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    const-string v2, "HANGOUT"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_invites_sent:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_1:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->inviteMoreParticipants()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->participant_list_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setContentView(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->toasts_view:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHangoutInfo()Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    sget v1, Lcom/google/android/apps/plus/R$menu;->hangout_participant_list_activity_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->createTitlebarButtons(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "hangout_participants"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_participants_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setTitlebarTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->participant_count:I

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->hangout_invite_menu_item:I

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->inviteMoreParticipants()V

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    return v0
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    sget v1, Lcom/google/android/apps/plus/R$id;->hangout_invite_menu_item:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->canInviteMoreParticipants()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
