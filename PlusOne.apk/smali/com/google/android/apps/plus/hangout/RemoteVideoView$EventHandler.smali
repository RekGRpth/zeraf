.class final Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "RemoteVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/RemoteVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/RemoteVideoView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)V

    return-void
.end method


# virtual methods
.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUDIO_VIDEO_SESSION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    sget-object v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->NONE:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    :cond_0
    return-void
.end method

.method public final onIncomingVideoFrameDimensionsChanged(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameDimensionsChanged(III)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    if-eq p3, v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iput p2, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iput p3, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestLayout()V

    goto :goto_0
.end method

.method public final onIncomingVideoFrameReceived(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameReceived(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$000(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->requestRender()V

    :cond_0
    return-void
.end method

.method public final onIncomingVideoStarted(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoStarted(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isHangoutTileStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$000(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setRequestID(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$000(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/VideoTextureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->onResume()V

    :cond_0
    return-void
.end method

.method public final onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method

.method public final onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$200(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$100(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VCard;->getAvatarData()[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getAvatarView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/hangout/Avatars;->renderAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/MeetingMember;Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$102(Lcom/google/android/apps/plus/hangout/RemoteVideoView;Z)Z

    :cond_0
    return-void
.end method

.method public final onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method

.method public final onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-eq v0, v1, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    sget-object v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->NONE:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eq v0, p2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;->onVideoSourceAboutToChange$1385ff()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iput-object p2, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->onResume()V

    :cond_4
    const-string v0, "Now showing %s on video activity"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$200(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_1

    :cond_7
    if-eqz p3, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    sget-object v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getAvatarView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/plus/hangout/Avatars;->renderAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/MeetingMember;Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    sget-object v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VCard;->getAvatarData()[B

    move-result-object v0

    if-nez v0, :cond_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    # setter for: Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->access$102(Lcom/google/android/apps/plus/hangout/RemoteVideoView;Z)Z

    goto :goto_1
.end method
