.class public Lcom/google/android/apps/plus/hangout/GCommApp;
.super Ljava/lang/Object;
.source "GCommApp.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Lcom/google/android/apps/plus/service/Hangout$ApplicationEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/GCommApp$5;,
        Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;,
        Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;,
        Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;,
        Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;,
        Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static crashReported:Z

.field private static gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;


# instance fields
.field private account:Lcom/google/android/apps/plus/content/EsAccount;

.field private final appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

.field private audioFocus:Z

.field private audioManager:Landroid/media/AudioManager;

.field private final connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

.field private connectivityManager:Landroid/net/ConnectivityManager;

.field private cpuWakeLock:Landroid/os/PowerManager$WakeLock;

.field private currentNetworkSubtype:I

.field private currentNetworkType:I

.field private currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

.field private eventHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private exitMeetingCleanupDone:Z

.field private gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

.field private volatile gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

.field private final gcommServiceConnection:Landroid/content/ServiceConnection;

.field private greenRoomParticipantIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private hangoutSigninRefCount:I

.field private hangoutStartTime:J

.field private headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

.field private incomingAudioLevelBeforeAudioFocusLoss:I

.field private isBound:Z

.field private isExitingHangout:Z

.field private lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private mSavedAudioMode:I

.field private muteMicOnAudioFocusGain:Z

.field private outgoingVideoMute:Z

.field private final plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

.field private screenWakeLock:Landroid/os/PowerManager$WakeLock;

.field screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

.field private selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

.field private signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

.field private wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/plus/phone/EsApplication;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/phone/EsApplication;

    const/4 v2, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$1;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;

    invoke-direct {v0, p0, v9}, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$3;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    const-string v0, "Constructing GCommApp"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->resetSelfMediaState()V

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    invoke-direct {v0, p0, v9}, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->initialize(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsApplication;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_HANGOUT_LOG:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v4

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_WRAPPER_HANGOUT_LOG_LEVEL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v6

    const-string v2, "Google_Plus_Android"

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Utils;->getVersion()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$2;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    new-array v2, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const-class v3, Lcom/google/android/apps/plus/hangout/GCommService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v10}, Lcom/google/android/apps/plus/phone/EsApplication;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-nez v0, :cond_1

    const-string v0, "Unable to bind to GCommService"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityManager:Landroid/net/ConnectivityManager;

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    const-string v0, "gcomm"

    invoke-virtual {v7, v10, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    const/16 v0, 0xa

    const-string v2, "gcomm"

    invoke-virtual {v7, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    const-string v0, "gcomm"

    invoke-virtual {v8, v10, v0}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    iput v9, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    sput-object p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v2}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/GCommApp;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/GCommApp;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/phone/EsApplication;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setMode(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x2

    invoke-virtual {v2, p0, v1, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "requestAudioFocus returned "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "INCOMING_AUDIO_VOLUME"

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setupAudio()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/hangout/GCommApp;J)J
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    return-wide p1
.end method

.method static synthetic access$1702(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    return v0
.end method

.method static synthetic access$200(Landroid/net/NetworkInfo;)Z
    .locals 2
    .param p0    # Landroid/net/NetworkInfo;

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommApp;->getCaptureSessionType()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommService;)Lcom/google/android/apps/plus/hangout/GCommService;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommService;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/GCommApp;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;)Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/service/Hangout$Info;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommApp;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    const/16 v1, 0x36

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->dispatchMessage(ILjava/lang/Object;)V

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method private exitMeetingAndDisconnect()V
    .locals 1

    const-string v0, "GCommApp.exitMeetingAndDisconnect"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    return-void
.end method

.method private getAllEventHandlers()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommEventHandler;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private static getCaptureSessionType()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;
    .locals 16

    const/4 v15, 0x1

    const-string v12, ""

    const/4 v13, 0x2

    :try_start_0
    new-array v0, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "/system/bin/cat"

    aput-object v14, v0, v13

    const/4 v13, 0x1

    const-string v14, "/proc/cpuinfo"

    aput-object v14, v0, v13

    new-instance v3, Ljava/lang/ProcessBuilder;

    invoke-direct {v3, v0}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    const/16 v13, 0x400

    new-array v11, v13, [B

    :goto_0
    invoke-virtual {v6, v11}, Ljava/io/InputStream;->read([B)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v11}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    :cond_0
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v2, 0x0

    const/4 v9, 0x0

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v7, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v7, :cond_2

    aget-object v8, v1, v5

    const-string v13, "BogoMIPS.*"

    invoke-virtual {v8, v13}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    const-string v13, "[^.0-9]"

    const-string v14, ""

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    add-float/2addr v2, v13

    add-int/lit8 v9, v9, 0x1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/high16 v13, 0x41200000

    cmpl-float v13, v2, v13

    if-lez v13, :cond_3

    const/high16 v13, 0x43480000

    cmpg-float v13, v2, v13

    if-ltz v13, :cond_4

    :cond_3
    const/high16 v13, 0x447a0000

    cmpl-float v13, v2, v13

    if-gtz v13, :cond_4

    if-le v9, v15, :cond_5

    :cond_4
    sget-object v13, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;->MEDIUM_RESOLUTION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    :goto_3
    return-object v13

    :cond_5
    sget-object v13, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;->LOW_RESOLUTION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    goto :goto_3
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;)V

    sput-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    return-object v0
.end method

.method public static isDebuggable(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInstantiated()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static reportCrash(Ljava/lang/String;Z)V
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/GCommApp;->crashReported:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/hangout/GCommApp;->crashReported:Z

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const-class v2, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    if-eqz p0, :cond_2

    const-string v1, "com.google.android.apps.plus.hangout.java_crash_signature"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->startActivity(Landroid/content/Intent;)V

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->finish()V

    :cond_3
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-boolean v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsApplication;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    goto :goto_0
.end method

.method static reportJavaCrashFromNativeCode(Ljava/lang/Throwable;)V
    .locals 2
    .param p0    # Ljava/lang/Throwable;

    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->computeJavaCrashSignature(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportCrash(Ljava/lang/String;Z)V

    return-void
.end method

.method static reportNativeCrash()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportCrash(Ljava/lang/String;Z)V

    return-void
.end method

.method private resetSelfMediaState()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_0
.end method

.method public static sendEmptyMessage(Landroid/content/Context;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method public static sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/plus/hangout/GCommApp;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/hangout/GCommApp$4;

    invoke-direct {v2, v0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommApp$4;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private setupAudio()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->isHeadsetPluggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    :cond_0
    return-void
.end method

.method private updateWakeLockState(Z)V
    .locals 3
    .param p1    # Z

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->NONE:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->SCREEN:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp$5;->$SwitchMap$com$google$android$apps$plus$hangout$GCommApp$WakeLockType:[I

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->NONE:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "Released CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "Released screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v1, "Acquired CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "Released screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v1, "Acquired screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "Released CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final createHangout(Z)V
    .locals 1
    .param p1    # Z

    const-string v0, "GCommApp.createHangout"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->createHangout(Z)V

    return-void
.end method

.method public final disconnect()V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GCommApp.disconnect: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    const-string v0, "Released wifi lock"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    if-eq v0, v2, :cond_1

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->signoutAndDisconnect()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->resetSelfMediaState()V

    return-void
.end method

.method public final dispatchMessage(ILjava/lang/Object;)V
    .locals 13
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v11, 0x1

    sparse-switch p1, :sswitch_data_0

    new-instance v10, Ljava/lang/IllegalStateException;

    invoke-direct {v10}, Ljava/lang/IllegalStateException;-><init>()V

    throw v10

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSigninTimeOutError()V

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    goto :goto_2

    :sswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedOut()V

    goto :goto_3

    :sswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_4

    :sswitch_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_5

    :sswitch_6
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/hangout/GCommApp;->createHangout(Z)V

    :cond_0
    return-void

    :sswitch_7
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V

    goto :goto_6

    :sswitch_8
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_7

    :sswitch_9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMediaStarted()V

    goto :goto_8

    :sswitch_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz p2, :cond_1

    move v10, v11

    :goto_a
    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingExited(Z)V

    goto :goto_9

    :cond_1
    const/4 v10, 0x0

    goto :goto_a

    :sswitch_b
    move-object v4, p2

    check-cast v4, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    iget-object v10, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCallgrokLogUploadCompleted$4f708078()V

    goto :goto_b

    :sswitch_c
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto :goto_c

    :sswitch_d
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto :goto_d

    :sswitch_e
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_e

    :sswitch_f
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_f

    :sswitch_10
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_10
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_10

    :sswitch_11
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_11
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_11

    :sswitch_12
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_12
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameReceived(I)V

    goto :goto_12

    :sswitch_13
    move-object v6, p2

    check-cast v6, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_13
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getRequestID()I

    move-result v10

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getDimensions()Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getDimensions()Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v12

    iget v12, v12, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameDimensionsChanged(III)V

    goto :goto_13

    :sswitch_14
    move-object v2, p2

    check-cast v2, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_14
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_14

    :sswitch_15
    move-object v2, p2

    check-cast v2, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_15
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_15

    :sswitch_16
    move-object v3, p2

    check-cast v3, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_16
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVolumeChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;I)V

    goto :goto_16

    :sswitch_17
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_17
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCurrentSpeakerChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_17

    :sswitch_18
    move-object v9, p2

    check-cast v9, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_18
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->getRequestID()I

    move-result v10

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->getSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v11

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->isVideoAvailable()Z

    move-result v12

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_18

    :sswitch_19
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_19
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoStarted(I)V

    goto :goto_19

    :sswitch_1a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onOutgoingVideoStarted()V

    goto :goto_1a

    :sswitch_1b
    move-object v1, p2

    check-cast v1, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_1b

    :sswitch_1c
    move-object v0, p2

    check-cast v0, Landroid/util/Pair;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget-object v10, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Landroid/util/Pair;

    iget-object v10, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Landroid/util/Pair;

    iget-object v11, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v12, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_1c

    :sswitch_1d
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCameraSwitchRequested()V

    goto :goto_1d

    :sswitch_1e
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoMuteToggleRequested()V

    goto :goto_1e

    :sswitch_1f
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    move-object v10, p2

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoMuteChanged(Z)V

    goto :goto_1f

    :sswitch_20
    move-object v5, p2

    check-cast v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_20
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    iget v10, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    iget v11, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCameraPreviewFrameDimensionsChanged(II)V

    goto :goto_20

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3 -> :sswitch_7
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x32 -> :sswitch_c
        0x33 -> :sswitch_d
        0x34 -> :sswitch_8
        0x35 -> :sswitch_9
        0x36 -> :sswitch_a
        0x37 -> :sswitch_e
        0x38 -> :sswitch_f
        0x39 -> :sswitch_10
        0x3b -> :sswitch_11
        0x3c -> :sswitch_b
        0x65 -> :sswitch_14
        0x66 -> :sswitch_17
        0x67 -> :sswitch_18
        0x68 -> :sswitch_19
        0x69 -> :sswitch_1a
        0x6a -> :sswitch_12
        0x6b -> :sswitch_13
        0x6d -> :sswitch_1b
        0x6e -> :sswitch_1c
        0x6f -> :sswitch_15
        0x70 -> :sswitch_16
        0xc9 -> :sswitch_1d
        0xca -> :sswitch_1e
        0xcb -> :sswitch_1f
        0xcc -> :sswitch_20
    .end sparse-switch
.end method

.method public final enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;
    .param p2    # Z
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/service/Hangout$Info;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;Z)V"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v2, "GCommApp.enterHangout: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    iput-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v2, p1, p2, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->enterMeeting(Lcom/google/android/apps/plus/service/Hangout$Info;ZZ)V

    if-nez p3, :cond_1

    iput-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    if-nez v2, :cond_2

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    goto :goto_0
.end method

.method public final exitMeeting()V
    .locals 1

    const-string v0, "GCommApp.exitMeeting"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanup()V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->exitMeeting()V

    return-void
.end method

.method public final exitMeetingCleanup()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "INCOMING_AUDIO_VOLUME"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->onAudioFocusChange(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    :goto_1
    iput v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    :cond_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_1
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getApp()Lcom/google/android/apps/plus/phone/EsApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    return-object v0
.end method

.method public final getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method public final getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

    return-object v0
.end method

.method public final getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-object v0
.end method

.method public final getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final hasAudioFocus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    return v0
.end method

.method public final isAudioMute()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v0

    return v0
.end method

.method public final isExitingHangout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    return v0
.end method

.method public final isInAHangout()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITHOUT_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final isInAHangoutWithMedia()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v0

    goto :goto_0
.end method

.method public final isOutgoingVideoMute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    return v0
.end method

.method public onAudioFocusChange(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "onAudioFocusChange: %d (meeting=%s)"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    add-int/lit8 v1, v1, 0x0

    div-int/lit8 v1, v1, 0x5

    add-int/lit8 v0, v1, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setupAudio()V

    const-string v1, "AUDIOFOCUS_GAIN: speakerphone=%s volume=%d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    const-string v1, "AUDIOFOCUS_LOSS: speakerphone=%s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method final raiseNetworkError()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const/4 v1, -0x1

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->NETWORK:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method public final registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .param p3    # Z

    const-string v0, "Registering for events: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->isOnMainThread(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-eqz p3, :cond_3

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final setAudioMute(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setAudioMute(Z)V

    return-void
.end method

.method public final setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-void
.end method

.method public final setOutgoingVideoMute(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    return-void
.end method

.method public final setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-void
.end method

.method public final shouldShowToastForMember(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_3

    iget-wide v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x1388

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    move v0, v4

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    move v1, v4

    :goto_2
    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    move v5, v4

    goto :goto_0

    :cond_3
    move v0, v5

    goto :goto_1

    :cond_4
    move v1, v5

    goto :goto_2
.end method

.method public final signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GCommApp.signinUser: signinTask="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v0, "startUsingNetwork: info is null"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->raiseNetworkError()V

    move v0, v1

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    const-string v2, "Current network type: %d subtype: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    const-string v2, "Acquired wifi lock"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting HangoutActivity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    return-void
.end method

.method public final stoppingHangoutActivity()V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Stopping HangoutActivity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    goto :goto_0
.end method

.method public final unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .param p3    # Z

    const-string v0, "Unregistering for events: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->isOnMainThread(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-eqz p3, :cond_3

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eq p2, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    :goto_0
    return-void

    :cond_3
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
