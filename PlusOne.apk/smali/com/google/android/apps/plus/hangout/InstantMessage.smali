.class final Lcom/google/android/apps/plus/hangout/InstantMessage;
.super Ljava/lang/Object;
.source "InstantMessage.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final from:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final fromMucJid:Ljava/lang/String;

.field private final message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/InstantMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/InstantMessage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/InstantMessage;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/InstantMessage;->from:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/InstantMessage;->fromMucJid:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/InstantMessage;->message:Ljava/lang/String;

    return-void
.end method
