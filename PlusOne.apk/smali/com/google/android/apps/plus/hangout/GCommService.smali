.class public Lcom/google/android/apps/plus/hangout/GCommService;
.super Landroid/app/Service;
.source "GCommService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;,
        Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;
    }
.end annotation


# instance fields
.field private callTimeoutRunnable:Ljava/lang/Runnable;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;

.field private handler:Landroid/os/Handler;

.field private final localBinder:Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;

.field private notificationIntent:Landroid/content/Intent;

.field private playbackMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;-><init>(Lcom/google/android/apps/plus/hangout/GCommService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->localBinder:Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/GCommService;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->eventHandler:Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/GCommService;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommService;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->callTimeoutRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/hangout/GCommService;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommService;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->callTimeoutRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/GCommService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/GCommService;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final getNotificationIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->notificationIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/apps/plus/hangout/GCommService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->localBinder:Lcom/google/android/apps/plus/hangout/GCommService$LocalBinder;

    return-object v1
.end method

.method public onCreate()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "GCommService.onCreate called"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->eventHandler:Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/GCommService;->stopForeground(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const-string v0, "GCommService.onDestroy called"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->eventHandler:Lcom/google/android/apps/plus/hangout/GCommService$EventHandler;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    return v0
.end method

.method public final showHangoutNotification(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->notificationIntent:Landroid/content/Intent;

    const-string v2, "GCommService.showHangoutNotification"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, p1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    sget v2, Lcom/google/android/apps/plus/R$drawable;->hangout_notification_icon:I

    iput v2, v0, Landroid/app/Notification;->icon:I

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/app/Notification;->flags:I

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Landroid/app/Notification;->flags:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_ongoing_notify_title:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_ongoing_notify_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method public final startRingback()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommService;->stopRingback()V

    const-string v1, "GCommService.startRingback"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :try_start_0
    sget v1, Lcom/google/android/apps/plus/R$raw;->hangout_ringback:I

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not create MediaPlayer for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/R$raw;->hangout_ringback:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error playing media: "

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_1
.end method

.method public final stopRingback()V
    .locals 1

    const-string v0, "GCommService.stopRingback"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommService;->playbackMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    return-void
.end method
