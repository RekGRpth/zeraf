.class public Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HangoutRingingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    }
.end annotation


# static fields
.field private static final INVITER_PROJECTION:[Ljava/lang/String;

.field private static isCurrentlyRinging:Z

.field private static mRingtone:Landroid/media/Ringtone;

.field private static sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

.field private final mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

.field private mCallTimeoutRunnable:Ljava/lang/Runnable;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field volatile mContinueVibrating:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private final mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

.field private mHasActed:Z

.field private mInviteId:Ljava/lang/String;

.field private mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mInviterCircleNamesTextView:Landroid/widget/TextView;

.field private mInviterId:Ljava/lang/String;

.field private mInviterName:Ljava/lang/String;

.field private mIsHangoutLite:Z

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPackedCircleIds:Ljava/lang/String;

.field private mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

.field private final mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

.field private mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

.field private mSelfVideoVerticalGravity:F

.field private mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

.field private mSelfVideoViewContainer:Landroid/widget/FrameLayout;

.field mVibrator:Landroid/os/Vibrator;

.field mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

.field private toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

.field private toggleVideoMuteMenuButton:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    sput-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->INVITER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    const v0, -0x414ccccd

    iput v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    const-string v0, "Accepted invitation"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v3, v2}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const-string v0, "Not yet signed in. Will send finish once signed in."

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    goto :goto_0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    const-string v0, "Rejected invitation"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const-string v0, "Not yet signed in. Will send finish once signed in."

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    goto :goto_0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->createMissedHangoutNotification()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->INVITER_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":notifications:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createMissedHangoutNotification()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_missed_notification_title:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    new-instance v7, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v7, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-static {p0, v5, v6, v7}, Lcom/google/android/apps/plus/phone/Intents;->getMissedHangoutCallbackIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    invoke-static {p0, v8, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/app/Notification;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v2, v5, v1, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v5, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v2, Landroid/app/Notification;->flags:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_missed_notification_content:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, p0, v1, v5, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v3, v5, v6, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v0, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const/4 v3, 0x0

    const/4 v2, 0x0

    sput-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    sput-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getApp()Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    if-eq p1, v0, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->removeStatusBarNotification()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->finish()V

    return-void
.end method

.method public static onC2DMReceive(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 30
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;

    const-string v3, "Hangout Invitation Receiver got invitation tickle"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v3, v4, :cond_1

    const-string v3, "Ignoring hangout invitation since this device doesn\'t support hangouts"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_notify_setting_key:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$bool;->hangout_notify_setting_default_value:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "Ignoring hangout invitation because of setting"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v3, "notification"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-eqz v21, :cond_3

    if-nez v23, :cond_4

    :cond_3
    const-string v3, "Incorrect tickle: inviteId = %s, hangoutInviteProtoBase64 = %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v21, v4, v5

    const/4 v5, 0x1

    aput-object v23, v4, v5

    const-string v5, "GoogleMeeting"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "GoogleMeeting"

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v24

    const/16 v22, 0x0

    :try_start_0
    invoke-static/range {v24 .. v24}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->parseFrom([B)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    :goto_1
    if-nez v22, :cond_5

    const-string v3, "Could not decode invite: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v23, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v3, "Invalid BatchCommand message received"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasCommand()Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "Ignoring hangoutInviteNotification without any command"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v25

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->DISMISSED:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    if-ne v3, v4, :cond_a

    const-string v3, "Got hangoutInviteNotification:\nCommand: %s\nHangoutId: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v3, :cond_9

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-boolean v4, v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-eqz v4, :cond_7

    const-string v3, "Ignoring hangout ring cancellation since user already acted on it"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutId()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "Cancelling hangout ringing."

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    goto/16 :goto_0

    :cond_8
    const-string v3, "Ignoring hangout ring cancellation since hangout ids don\'t match"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v3, "Ignoring dismiss command since ring activity is not running."

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v26

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-object/from16 v0, v26

    if-eq v0, v3, :cond_b

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->LITE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-object/from16 v0, v26

    if-eq v0, v3, :cond_b

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->ONAIR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-object/from16 v0, v26

    if-eq v0, v3, :cond_b

    const-string v3, "Ignoring Hangout ring for unsupported hangout type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasInvitation()Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "Ignoring hangoutStartContext without invitation"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getInvitation()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasInviterGaiaId()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasTimestamp()Z

    move-result v3

    if-nez v3, :cond_e

    :cond_d
    const-string v3, "Ignoring hangoutStartContext without invitation data"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getTimestamp()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x493e0

    cmp-long v3, v3, v5

    if-lez v3, :cond_f

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring expired hangout invitation tickle. Tickle age = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getTimestamp()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInvitationType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-result-object v28

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v28

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v28

    if-eq v0, v3, :cond_10

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v28

    if-eq v0, v3, :cond_10

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring unsupported invitation type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInvitationType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v27 .. v27}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterProfileName()Ljava/lang/String;

    move-result-object v29

    const-string v3, "Got hangoutInviteNotification:\nCommand: %s\nInviterGaiaId: %s\nInviterName: %s\nHangoutId: %s\nHangoutType: %s\nNotificationType: %s"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v11, v4, v5

    const/4 v5, 0x2

    aput-object v29, v4, v5

    const/4 v5, 0x3

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v28

    if-ne v0, v3, :cond_11

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Transfer:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    :cond_11
    new-instance v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNick()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    const-string v3, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-eqz v3, :cond_13

    const/16 v18, 0x1

    :goto_2
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_1
    .catch Ljava/lang/LinkageError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->ENTERING_MEETING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_12

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITHOUT_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_12

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-ne v0, v3, :cond_14

    :cond_12
    const/16 v20, 0x1

    :goto_3
    if-eqz v20, :cond_15

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v3

    if-eqz v3, :cond_15

    const-string v3, "Ignoring the ring/ding since user is already in same hangout"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    const/16 v18, 0x0

    goto :goto_2

    :catch_1
    move-exception v19

    const-string v3, "Hangout native lib is missing or misconfigured"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/lang/LinkageError;->printStackTrace()V

    goto/16 :goto_0

    :cond_14
    const/16 v20, 0x0

    goto :goto_3

    :cond_15
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->NOTIFICATION_DING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    if-eq v3, v4, :cond_16

    if-nez v18, :cond_16

    if-nez v20, :cond_16

    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    if-eqz v3, :cond_19

    :cond_16
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Creating ding notification. AppState: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". callInProgress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". hangoutInProgress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". isCurrentlyRinging: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNick()Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ding:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/16 v16, 0x0

    move-object v9, v2

    invoke-direct/range {v9 .. v16}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldNotify(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    iput v3, v4, Landroid/app/Notification;->icon:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, v4, Landroid/app/Notification;->when:J

    iget v3, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v4, Landroid/app/Notification;->flags:I

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v4, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :goto_4
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldVibrate(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_17

    iget v3, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v4, Landroid/app/Notification;->defaults:I

    :cond_17
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_dinging_ticker:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v29, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->hangout_dinging_content_title:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v29, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/google/android/apps/plus/R$string;->hangout_dinging_content:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v29, v9, v10

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v5, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v6, v7, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const-string v3, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v3, v5, v6, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_18
    iget v3, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v4, Landroid/app/Notification;->defaults:I

    goto :goto_4

    :cond_19
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->LITE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    if-ne v3, v4, :cond_1c

    const/4 v14, 0x1

    :goto_5
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNING_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-eq v4, v5, :cond_1a

    sget-object v5, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v4, v5, :cond_1b

    :cond_1a
    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    :cond_1b
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v12, v29

    move-object v13, v2

    invoke-static/range {v9 .. v14}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutRingingActivityIntent$55105fd9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$Info;Z)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1c
    const/4 v14, 0x0

    goto :goto_5
.end method

.method private removeStatusBarNotification()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sending hangout finish request. Status: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->sendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static stopRingActivity()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    invoke-direct {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->createMissedHangoutNotification()V

    :cond_0
    return-void
.end method

.method private stopRingTone()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    sput-object v1, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mContinueVibrating:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0xf

    const/4 v7, 0x4

    if-ne v3, v7, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_4

    move v7, v4

    :goto_0
    if-eqz v7, :cond_5

    const v3, -0x41bd70a4

    :goto_1
    iput v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    if-eqz v7, :cond_6

    move v3, v4

    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setRequestedOrientation(I)V

    :goto_3
    sget v3, Lcom/google/android/apps/plus/R$layout;->hangout_ringing_activity:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v7, 0x680080

    invoke-virtual {v3, v7}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "hangout_info"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/Hangout$Info;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    const-string v3, "hangout_invite_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    const-string v3, "hangout_inviter_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    const-string v3, "hangout_inviter_name"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v7, Lcom/google/android/apps/plus/R$string;->hangout_anonymous_person:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    :cond_0
    const-string v3, "hangout_is_lite"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    sget v3, Lcom/google/android/apps/plus/R$id;->inviter_name:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->circle_names:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->inviter_avatar:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    :goto_4
    sget v3, Lcom/google/android/apps/plus/R$id;->self_video_container:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v3, p0, v7, v8}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    new-instance v7, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->incomingCallWidget:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    new-instance v7, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$3;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$3;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setOnTriggerListener(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->clearAnimation()V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    sget v7, Lcom/google/android/apps/plus/R$array;->incoming_hangout_widget_2way_targets:I

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetResources(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    sget v7, Lcom/google/android/apps/plus/R$array;->incoming_hangout_widget_2way_target_descriptions:I

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetDescriptionsResourceId(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    sget v7, Lcom/google/android/apps/plus/R$array;->incoming_hangout_widget_2way_direction_descriptions:I

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setDirectionDescriptionsResourceId(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->reset(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v3, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    invoke-virtual {v3, v5, v6, v7}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_1
    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibrator:Landroid/os/Vibrator;

    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$4;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x7530

    invoke-virtual {v3, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-nez v3, :cond_a

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_ringtone_setting_key:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->hangout_ringtone_setting_default_value:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, v6, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    :goto_5
    sget-object v6, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-nez v6, :cond_9

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Cannot get a ringtone for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_vibrate_setting_key:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$bool;->hangout_vibrate_setting_default_value:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, v6, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    if-nez v3, :cond_2

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mContinueVibrating:Z

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    invoke-direct {v3, p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;->start()V

    :cond_2
    new-instance v1, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.hangout.NOTIFICATION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-virtual {v3, p0, v4, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    sput-object p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    invoke-direct {v3, p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getApp()Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.PHONE_STATE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_toggle_audio_mute:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$5;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_toggle_video_mute:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$6;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$6;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_4
    move v7, v5

    goto/16 :goto_0

    :cond_5
    const v3, -0x42333333

    goto/16 :goto_1

    :cond_6
    move v3, v5

    goto/16 :goto_2

    :cond_7
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setRequestedOrientation(I)V

    goto/16 :goto_3

    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_9
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v3}, Landroid/media/Ringtone;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Landroid/media/Ringtone;->setStreamType(I)V

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v3}, Landroid/media/Ringtone;->play()V

    goto/16 :goto_6

    :cond_a
    move-object v3, v6

    goto/16 :goto_5
.end method

.method protected onPause()V
    .locals 11

    const/4 v10, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v0, :cond_0

    new-instance v6, Landroid/app/Notification;

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_ringing_incoming:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_ringing_incoming:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutRingingActivityIntent$55105fd9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$Info;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const/16 v1, 0x10

    iput v1, v6, Landroid/app/Notification;->flags:I

    invoke-virtual {v6, v7, v8, v9, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->removeStatusBarNotification()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 4

    const/4 v3, -0x1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    new-instance v1, Lcom/google/android/apps/plus/hangout/SelfVideoView;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->turnOffFlashLightSupport()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    sget-object v2, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setVerticalGravity(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method
