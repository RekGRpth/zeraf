.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;
.super Ljava/lang/Object;
.source "HangoutTabletTile.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
