.class final Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "GCommApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppEventHandler"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/GCommApp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    return-void
.end method


# virtual methods
.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordErrorExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$902(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/service/Hangout$Info;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # invokes: Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$300(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    return-void
.end method

.method public final onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    return-void
.end method

.method public final onMeetingExited(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1100(Lcom/google/android/apps/plus/hangout/GCommApp;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanup()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1702(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordNormalExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$902(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/service/Hangout$Info;

    :cond_1
    return-void
.end method

.method public final onMeetingMediaStarted()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1102(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1200(Lcom/google/android/apps/plus/hangout/GCommApp;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    new-instance v1, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V

    # setter for: Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1302(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1500(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1300(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    move-result-object v1

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    # setter for: Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$1602(Lcom/google/android/apps/plus/hangout/GCommApp;J)J

    return-void
.end method

.method public final onSigninTimeOutError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # invokes: Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$300(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    return-void
.end method
