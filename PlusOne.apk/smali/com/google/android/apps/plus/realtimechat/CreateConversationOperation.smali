.class public final Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "CreateConversationOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$CheckIfFailedRunnable;,
        Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;
    }
.end annotation


# instance fields
.field mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field mConversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

.field mConversationRowId:Ljava/lang/Long;

.field mMessageRowId:J

.field mMessageText:Ljava/lang/String;

.field mResultCode:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-object p4, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mMessageText:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mResultCode:I

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 15

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/ConnectivityManager;

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v9}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v4, 0x0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_3

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mResultCode:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v6

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v6, v12}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->addParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto :goto_1

    :cond_4
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_5

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->GROUP:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v6, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "c:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mConversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mConversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mMessageText:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->createConversationLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/String;ZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;

    move-result-object v14

    const-string v0, "conversation_row_id"

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mConversationRowId:Ljava/lang/Long;

    const-string v0, "message_row_id"

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mMessageRowId:J

    if-eqz v4, :cond_2

    new-instance v7, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$CheckIfFailedRunnable;

    const/4 v0, 0x0

    invoke-direct {v7, p0, v0}, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$CheckIfFailedRunnable;-><init>(Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;B)V

    new-instance v10, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v10, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-wide/16 v0, 0x2710

    invoke-virtual {v10, v7, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v6, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto :goto_2
.end method

.method public final getResultCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mResultCode:I

    return v0
.end method

.method public final getResultValue()Ljava/lang/Object;
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mConversationRowId:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;->mConversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;-><init>(Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;Ljava/lang/Long;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)V

    return-object v0
.end method
