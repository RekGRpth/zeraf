.class final Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;
.super Ljava/lang/Thread;
.source "RealTimeChatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceThread"
.end annotation


# instance fields
.field private final mMainHandler:Landroid/os/Handler;

.field private final mProcessQueueRunnable:Ljava/lang/Runnable;

.field private final mQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread$1;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mMainHandler:Landroid/os/Handler;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->setName(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mQueue:Ljava/util/Queue;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;Landroid/content/Intent;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;)Ljava/util/Queue;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final quit()V
    .locals 3

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    const-string v2, "worker thread quit"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_3

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "RealTimeChatService"

    const-string v2, "thread quit with items in queue"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;
    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$1500(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Lcom/google/android/apps/plus/realtimechat/BunchClient;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "RealTimeChatService"

    const-string v2, "bunchClient not destroyed in quit"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$1502(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Lcom/google/android/apps/plus/realtimechat/BunchClient;)Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_5
    return-void
.end method

.method public final run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mThreadHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread$2;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
