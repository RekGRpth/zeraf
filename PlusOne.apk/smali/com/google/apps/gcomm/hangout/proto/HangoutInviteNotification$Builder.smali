.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HangoutInviteNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

.field private context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

.field private dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

.field private hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

.field private notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

.field private status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->RINGING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->NOTIFICATION_RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->UNKNOWN:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .locals 2
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
    .locals 1

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;-><init>()V

    return-object v0
.end method

.method private buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .locals 5

    new-instance v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;B)V

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$302(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$402(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$502(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$602(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$702(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$802(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->access$902(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;I)I

    return-object v1
.end method

.method private clear()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->RINGING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->NOTIFICATION_RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->UNKNOWN:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    return-object p0
.end method

.method private clone()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
    .locals 5

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v2

    if-eq v1, v2, :cond_b

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasContext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v2

    iget v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v4

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-static {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    :goto_0
    iget v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getStatus()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    goto :goto_0

    :cond_2
    iget v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    :cond_3
    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasCommand()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v2

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    :cond_5
    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasNotificationType()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v2

    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    :cond_7
    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasHangoutType()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    move-result-object v2

    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    :cond_9
    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasDismissReason()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getDismissReason()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    move-result-object v1

    if-nez v1, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    :cond_b
    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v1

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_2

    move v4, v5

    :goto_1
    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-virtual {v1, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    return-object v0
.end method
