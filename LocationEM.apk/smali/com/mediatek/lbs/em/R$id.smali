.class public final Lcom/mediatek/lbs/em/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ButtonMenu:I = 0x7f0500ca

.field public static final Button_AGPS:I = 0x7f0500d1

.field public static final Button_APN:I = 0x7f0500d0

.field public static final Button_AgpsTest:I = 0x7f050001

.field public static final Button_AreaMaxNum:I = 0x7f05004a

.field public static final Button_AreaMinInter:I = 0x7f050048

.field public static final Button_AreaStartTime:I = 0x7f05004c

.field public static final Button_AreaStopTime:I = 0x7f05004e

.field public static final Button_AreaType:I = 0x7f050046

.field public static final Button_AutoOff:I = 0x7f050004

.field public static final Button_AutoOn:I = 0x7f050003

.field public static final Button_CdmaTemplate:I = 0x7f05005f

.field public static final Button_ChangeRefLatLng:I = 0x7f0500a8

.field public static final Button_Delay:I = 0x7f050029

.field public static final Button_EditButtonCancel:I = 0x7f05008f

.field public static final Button_EditButtonOK:I = 0x7f05008e

.field public static final Button_EpoStress:I = 0x7f050093

.field public static final Button_GPS:I = 0x7f0500cd

.field public static final Button_GeoLat:I = 0x7f050054

.field public static final Button_GeoLong:I = 0x7f050056

.field public static final Button_GeoRadius:I = 0x7f050052

.field public static final Button_Geographic:I = 0x7f05004f

.field public static final Button_GpsReset:I = 0x7f050007

.field public static final Button_HAcc:I = 0x7f050023

.field public static final Button_LoadProfile:I = 0x7f050005

.field public static final Button_LocationAge:I = 0x7f050027

.field public static final Button_MSISDNEdit:I = 0x7f050033

.field public static final Button_Manual:I = 0x7f050092

.field public static final Button_McpAddr:I = 0x7f050062

.field public static final Button_McpPort:I = 0x7f050064

.field public static final Button_MnlController:I = 0x7f05008a

.field public static final Button_MnlVerifier:I = 0x7f050088

.field public static final Button_NiDialogTest:I = 0x7f050086

.field public static final Button_Once:I = 0x7f0500a7

.field public static final Button_PdeIp4Addr:I = 0x7f050068

.field public static final Button_PdeIp6Addr:I = 0x7f05006a

.field public static final Button_PdePort:I = 0x7f05006c

.field public static final Button_PdeUrl:I = 0x7f05006f

.field public static final Button_PeriodicInter:I = 0x7f050041

.field public static final Button_PeriodicNFix:I = 0x7f05003f

.field public static final Button_PeriodicSTime:I = 0x7f050043

.field public static final Button_PosMethod:I = 0x7f05003c

.field public static final Button_Reset2Default:I = 0x7f050085

.field public static final Button_ResetAgpsd:I = 0x7f050087

.field public static final Button_SILR:I = 0x7f050030

.field public static final Button_SetProfile:I = 0x7f050006

.field public static final Button_SlpAddr:I = 0x7f050016

.field public static final Button_SlpPort:I = 0x7f050018

.field public static final Button_SlpTemplate:I = 0x7f050012

.field public static final Button_SlpTest:I = 0x7f050013

.field public static final Button_StartCpAutoTest:I = 0x7f05008b

.field public static final Button_StartGPS:I = 0x7f0500a6

.field public static final Button_Stress:I = 0x7f0500a9

.field public static final Button_TIME:I = 0x7f0500cf

.field public static final Button_Test:I = 0x7f050002

.field public static final Button_ThirdMSISDNEdit:I = 0x7f050036

.field public static final Button_TriggerAbort:I = 0x7f050038

.field public static final Button_TriggerStart:I = 0x7f050037

.field public static final Button_UpdatePeriod:I = 0x7f050097

.field public static final Button_VAcc:I = 0x7f050025

.field public static final Button_WIFI:I = 0x7f0500d2

.field public static final Button_YGPS:I = 0x7f0500ce

.field public static final Button_apply:I = 0x7f0500c8

.field public static final Button_cancel:I = 0x7f0500c7

.field public static final Button_ok:I = 0x7f0500c6

.field public static final Button_serverConnect:I = 0x7f0500d7

.field public static final CheckBox_CerVerify:I = 0x7f05002a

.field public static final CheckBox_EcidEnable:I = 0x7f05002c

.field public static final CheckBox_EmAllow:I = 0x7f05007e

.field public static final CheckBox_EnableAgps:I = 0x7f050009

.field public static final CheckBox_EnableAuto:I = 0x7f050091

.field public static final CheckBox_EnableEpo:I = 0x7f050090

.field public static final CheckBox_EnableIot:I = 0x7f05002b

.field public static final CheckBox_EnableNiTimer:I = 0x7f050082

.field public static final CheckBox_ExternalAddr:I = 0x7f050073

.field public static final CheckBox_LabPerformance:I = 0x7f050077

.field public static final CheckBox_LogNmea:I = 0x7f0500ac

.field public static final CheckBox_LogToSdcard:I = 0x7f0500ad

.field public static final CheckBox_McpEnable:I = 0x7f050060

.field public static final CheckBox_MlcNumber:I = 0x7f050075

.field public static final CheckBox_NiAllow:I = 0x7f05007d

.field public static final CheckBox_PdeAddrVaild:I = 0x7f050065

.field public static final CheckBox_PdeIpType:I = 0x7f050066

.field public static final CheckBox_PdeUrlVaild:I = 0x7f05006d

.field public static final CheckBox_RoamingAllow:I = 0x7f05007f

.field public static final CheckBox_SUPLTwo:I = 0x7f05002e

.field public static final CheckBox_Supl2File:I = 0x7f050080

.field public static final CheckBox_ThirdMSISDN:I = 0x7f050034

.field public static final CheckBox_Tls:I = 0x7f050019

.field public static final EditText_Delay1:I = 0x7f0500bc

.field public static final EditText_Delay2:I = 0x7f0500be

.field public static final EditText_Delay3:I = 0x7f0500c0

.field public static final EditText_Delay4:I = 0x7f0500c2

.field public static final EditText_EditButton:I = 0x7f05008d

.field public static final EditText_EpoDelay1:I = 0x7f05009b

.field public static final EditText_EpoDelay2:I = 0x7f05009d

.field public static final EditText_EpoNumOfLoop:I = 0x7f050099

.field public static final EditText_NumOfLoop:I = 0x7f0500ba

.field public static final EditText_UpdatePeriod:I = 0x7f050096

.field public static final EditText_addr:I = 0x7f0500cb

.field public static final EditText_lat:I = 0x7f0500c4

.field public static final EditText_lng:I = 0x7f0500c5

.field public static final EditText_serverIp:I = 0x7f0500d5

.field public static final EditText_serverPort:I = 0x7f0500d6

.field public static final ImageView_fileRow:I = 0x7f0500a3

.field public static final LinearLayout_AreaSettings:I = 0x7f050044

.field public static final LinearLayout_CdmaPage:I = 0x7f050059

.field public static final LinearLayout_CdmaSettings:I = 0x7f05005e

.field public static final LinearLayout_CpSettings:I = 0x7f050070

.field public static final LinearLayout_NiDialog:I = 0x7f050081

.field public static final LinearLayout_PayloadType:I = 0x7f05001a

.field public static final LinearLayout_PeriodicSettings:I = 0x7f05003d

.field public static final LinearLayout_SUPLTwoDotZero:I = 0x7f05002f

.field public static final LinearLayout_SUPLTwoDotZeroTitle:I = 0x7f05002d

.field public static final LinearLayout_Test:I = 0x7f050000

.field public static final LinearLayout_UpSettings:I = 0x7f05000c

.field public static final RadioButton_Area:I = 0x7f05003a

.field public static final RadioButton_AssistanceData:I = 0x7f050072

.field public static final RadioButton_CDMA_Force:I = 0x7f05005d

.field public static final RadioButton_CDMA_Prefer:I = 0x7f05005c

.field public static final RadioButton_Cp:I = 0x7f05000a

.field public static final RadioButton_GeoRadiusSignNorth:I = 0x7f050057

.field public static final RadioButton_GeoRadiusSignSouth:I = 0x7f050058

.field public static final RadioButton_Imsi:I = 0x7f05001f

.field public static final RadioButton_Ipv4:I = 0x7f05001e

.field public static final RadioButton_KValue:I = 0x7f050020

.field public static final RadioButton_LocationEstimate:I = 0x7f050071

.field public static final RadioButton_Meter:I = 0x7f050021

.field public static final RadioButton_Msa:I = 0x7f050011

.field public static final RadioButton_Msb:I = 0x7f050010

.field public static final RadioButton_Periodic:I = 0x7f050039

.field public static final RadioButton_PreferSim1:I = 0x7f050079

.field public static final RadioButton_PreferSim2:I = 0x7f05007a

.field public static final RadioButton_PreferSim3:I = 0x7f05007b

.field public static final RadioButton_PreferSim4:I = 0x7f05007c

.field public static final RadioButton_Rrc:I = 0x7f05001c

.field public static final RadioButton_Rrlp:I = 0x7f05001b

.field public static final RadioButton_RrlpRrc:I = 0x7f05001d

.field public static final RadioButton_Standalone:I = 0x7f05000f

.field public static final RadioButton_Up:I = 0x7f05000b

.field public static final RadioButton_WCDMA_Prefer:I = 0x7f05005b

.field public static final RadioButton_cold:I = 0x7f0500b1

.field public static final RadioButton_full:I = 0x7f0500b2

.field public static final RadioButton_hot:I = 0x7f0500af

.field public static final RadioButton_warm:I = 0x7f0500b0

.field public static final RadioGroup_CDMA_Prefer:I = 0x7f05005a

.field public static final RadioGroup_SimPrefer:I = 0x7f050078

.field public static final Spinner_NotificationTimeout:I = 0x7f050083

.field public static final Spinner_ServerPrfoile:I = 0x7f0500d4

.field public static final Spinner_VerificationTimeout:I = 0x7f050084

.field public static final TextView_0:I = 0x7f0500b3

.field public static final TextView_1:I = 0x7f0500b4

.field public static final TextView_2:I = 0x7f0500b5

.field public static final TextView_3:I = 0x7f0500b6

.field public static final TextView_4:I = 0x7f0500b7

.field public static final TextView_5:I = 0x7f0500b8

.field public static final TextView_AgpsMode:I = 0x7f05000e

.field public static final TextView_AreaMaxNum:I = 0x7f050049

.field public static final TextView_AreaMinInter:I = 0x7f050047

.field public static final TextView_AreaStartTime:I = 0x7f05004b

.field public static final TextView_AreaStopTime:I = 0x7f05004d

.field public static final TextView_AreaType:I = 0x7f050045

.field public static final TextView_AutoTestMessage:I = 0x7f05008c

.field public static final TextView_AutoTestResult:I = 0x7f050089

.field public static final TextView_CellInfo:I = 0x7f0500d3

.field public static final TextView_Delay:I = 0x7f050028

.field public static final TextView_Delay1:I = 0x7f0500bb

.field public static final TextView_Delay2:I = 0x7f0500bd

.field public static final TextView_Delay3:I = 0x7f0500bf

.field public static final TextView_Delay4:I = 0x7f0500c1

.field public static final TextView_EpoDelay1:I = 0x7f05009a

.field public static final TextView_EpoDelay2:I = 0x7f05009c

.field public static final TextView_EpoNumOfLoop:I = 0x7f050098

.field public static final TextView_ExternalAddr:I = 0x7f050074

.field public static final TextView_FileInfo:I = 0x7f050095

.field public static final TextView_FilePath:I = 0x7f0500a2

.field public static final TextView_GeoLat:I = 0x7f050053

.field public static final TextView_GeoLong:I = 0x7f050055

.field public static final TextView_GeoRadius:I = 0x7f050051

.field public static final TextView_GeographicResult:I = 0x7f050050

.field public static final TextView_HAcc:I = 0x7f050022

.field public static final TextView_LocationAge:I = 0x7f050026

.field public static final TextView_Loop:I = 0x7f0500aa

.field public static final TextView_MSISDN:I = 0x7f050032

.field public static final TextView_McpAddr:I = 0x7f050061

.field public static final TextView_McpPort:I = 0x7f050063

.field public static final TextView_MlcNumber:I = 0x7f050076

.field public static final TextView_NumOfLoop:I = 0x7f0500b9

.field public static final TextView_PdeIp4Addr:I = 0x7f050067

.field public static final TextView_PdeIp6Addr:I = 0x7f050069

.field public static final TextView_PdePort:I = 0x7f05006b

.field public static final TextView_PdeUrl:I = 0x7f05006e

.field public static final TextView_PeriodicInter:I = 0x7f050040

.field public static final TextView_PeriodicNFix:I = 0x7f05003e

.field public static final TextView_PeriodicSTime:I = 0x7f050042

.field public static final TextView_Porgress:I = 0x7f050094

.field public static final TextView_PosMethod:I = 0x7f05003b

.field public static final TextView_Property:I = 0x7f050008

.field public static final TextView_SILRResult:I = 0x7f050031

.field public static final TextView_ServerResult:I = 0x7f0500d8

.field public static final TextView_SlpAddr:I = 0x7f050015

.field public static final TextView_SlpPort:I = 0x7f050017

.field public static final TextView_SlpTestResult:I = 0x7f050014

.field public static final TextView_ThirdMSISDN:I = 0x7f050035

.field public static final TextView_VAcc:I = 0x7f050024

.field public static final TextView_fileRow:I = 0x7f0500a4

.field public static final TextView_fileRow2:I = 0x7f0500a5

.field public static final View_AgpsMode:I = 0x7f05000d

.field public static final button_LocationEm:I = 0x7f0500a0

.field public static final button_Sdcard:I = 0x7f05009f

.field public static final checkBox_switchDeleteToFirst:I = 0x7f0500c3

.field public static final linearLayout1:I = 0x7f0500a1

.field public static final linearLayout2:I = 0x7f05009e

.field public static final linearLayout3:I = 0x7f0500ab

.field public static final linearLayout4:I = 0x7f0500ae

.field public static final spinner_locList:I = 0x7f0500cc

.field public static final viewgps:I = 0x7f0500d9

.field public static final webview2:I = 0x7f0500c9


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
