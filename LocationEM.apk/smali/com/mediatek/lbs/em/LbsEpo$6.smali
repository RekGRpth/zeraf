.class Lcom/mediatek/lbs/em/LbsEpo$6;
.super Ljava/lang/Object;
.source "LbsEpo.java"

# interfaces
.implements Lcom/mediatek/common/epo/MtkEpoStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsEpo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsEpo;


# direct methods
.method constructor <init>(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChanged(I)V
    .locals 3
    .param p1    # I

    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Progress="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "EPO starting"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0xc9

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "EPO update success"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->updateEpoFileInfo()V
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsEpo;->access$1000(Lcom/mediatek/lbs/em/LbsEpo;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0xca

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "EPO update failure"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/16 v0, 0xcb

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "EPO update canceled"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x12c

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "EPO is idle now"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # getter for: Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsEpo;->access$1100(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->sessionDone()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->enableEpoButtons()V
    invoke-static {v0}, Lcom/mediatek/lbs/em/LbsEpo;->access$1200(Lcom/mediatek/lbs/em/LbsEpo;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo$6;->this$0:Lcom/mediatek/lbs/em/LbsEpo;

    const-string v1, "WARNING: EPO unknown status recv"

    # invokes: Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V

    goto :goto_0
.end method
