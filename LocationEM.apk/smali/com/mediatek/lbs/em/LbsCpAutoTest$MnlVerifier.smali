.class public Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;
.super Ljava/lang/Object;
.source "LbsCpAutoTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsCpAutoTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MnlVerifier"
.end annotation


# instance fields
.field private mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMnlHandler:Landroid/os/Handler;

.field private mMnlResultListener:Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

.field private mStatusListener:Landroid/location/GpsStatus$Listener;

.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;


# direct methods
.method public constructor <init>(Lcom/mediatek/lbs/em/LbsCpAutoTest;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlResultListener:Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    new-instance v0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$1;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$1;-><init>(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mStatusListener:Landroid/location/GpsStatus$Listener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$2;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$2;-><init>(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationListener:Landroid/location/LocationListener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$3;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier$3;-><init>(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlHandler:Landroid/os/Handler;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    const-string v0, "mtk-agps"

    invoke-virtual {p1, v0}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/agps/MtkAgpsManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlResultListener:Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Lcom/mediatek/common/agps/MtkAgpsManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Landroid/location/LocationListener;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;)Landroid/location/GpsStatus$Listener;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mStatusListener:Landroid/location/GpsStatus$Listener;

    return-object v0
.end method

.method private sendAgpsExtraCmd(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "CMD"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v2, "EXTRA_CMD"

    invoke-interface {v1, v2, v0}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    return-void
.end method

.method private setGpsMode(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    const-string v2, "Hot Start"

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    const-string v1, "rti"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    const-string v3, "delete_aiding_data"

    invoke-virtual {v1, v2, v3, v0}, Landroid/location/LocationManager;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    :goto_1
    return-void

    :cond_0
    if-ne p1, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    const-string v2, "Warm Start"

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    const-string v1, "ephemeris"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    const-string v2, "Cold Start"

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    const-string v1, "ephemeris"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "position"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "time"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "iono"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "utc"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "health"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    const-string v2, "Full Start"

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    const-string v1, "all"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    const-string v2, "WARNING: unknown reset type"

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public controlMnl(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_OPEN"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_CLOSE"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_START_485"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_START_486"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_STOP_485"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_STOP_486"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public enableAutoTestV2(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERR unknown enableAutoTestV2 type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/mediatek/lbs/em/LbsCpAutoTest;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->access$200(Lcom/mediatek/lbs/em/LbsCpAutoTest;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "AUTO_TEST_ON_V2_GEMINI"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "AUTO_TEST_ON_V2_DT"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "AUTO_TEST_OFF"

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public resetAgpsd()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "RESET_AGPSD"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    return-void
.end method

.method public startCpAutoTest(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "TC"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v2, "AUTO_TEST_RUN"

    invoke-interface {v1, v2, v0}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    return-void
.end method

.method public startCpAutoTestV2(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "TC"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v2, "AUTO_TEST_RUN_V2"

    invoke-interface {v1, v2, v0}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    return-void
.end method

.method public startNITest(ILcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    iput-object p2, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlResultListener:Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_NI"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    return-void
.end method

.method public startSITest(ILcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    iput-object p2, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlResultListener:Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlResultListener;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->this$0:Lcom/mediatek/lbs/em/LbsCpAutoTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsCpAutoTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    const-string v1, "MNL_TEST_SI"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/mediatek/common/agps/MtkAgpsManager;->extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->setGpsMode(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mMnlHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsCpAutoTest$MnlVerifier;->mStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    return-void
.end method
