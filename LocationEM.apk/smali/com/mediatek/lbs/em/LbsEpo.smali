.class public Lcom/mediatek/lbs/em/LbsEpo;
.super Landroid/app/Activity;
.source "LbsEpo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/lbs/em/LbsEpo$StressTest;
    }
.end annotation


# static fields
.field private static final STRESS_END:I = 0x1

.field private static final UPDATE_STATUS:I


# instance fields
.field private mButtonEpoStress:Landroid/widget/ToggleButton;

.field private mButtonManual:Landroid/widget/ToggleButton;

.field private mButtonUpdatePeriod:Landroid/widget/Button;

.field private mCheckBoxEnableAuto:Landroid/widget/CheckBox;

.field private mCheckBoxEnableEpo:Landroid/widget/CheckBox;

.field private mDelay1:I

.field private mDelay2:I

.field private mEditTextEpoDelay1:Landroid/widget/EditText;

.field private mEditTextEpoDelay2:Landroid/widget/EditText;

.field private mEditTextNumOfLoop:Landroid/widget/EditText;

.field private mEditTextUpdatePeroid:Landroid/widget/EditText;

.field private mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

.field private mEpoStatusListener:Lcom/mediatek/common/epo/MtkEpoStatusListener;

.field private mHandler:Landroid/os/Handler;

.field private mNumOfLoop:I

.field private mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

.field private mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

.field private mTextViewEpoFileInfo:Landroid/widget/TextView;

.field private mTextViewProgress:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mNumOfLoop:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay1:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay2:I

    new-instance v0, Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mNumOfLoop:I

    iget v2, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay1:I

    iget v3, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay2:I

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsEpo;III)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    new-instance v0, Lcom/mediatek/lbs/em/UtilityStringList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/mediatek/lbs/em/UtilityStringList;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    new-instance v0, Lcom/mediatek/lbs/em/LbsEpo$6;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsEpo$6;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoStatusListener:Lcom/mediatek/common/epo/MtkEpoStatusListener;

    new-instance v0, Lcom/mediatek/lbs/em/LbsEpo$7;

    invoke-direct {v0, p0}, Lcom/mediatek/lbs/em/LbsEpo$7;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/em/LbsEpo;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/lbs/em/LbsEpo;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->epoEnable(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->updateEpoFileInfo()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/lbs/em/LbsEpo$StressTest;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->enableEpoButtons()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/lbs/em/LbsEpo;)Lcom/mediatek/common/epo/MtkEpoClientManager;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/mediatek/lbs/em/LbsEpo;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->getStartResultString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->enableWidgetAfterStress()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/lbs/em/LbsEpo;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/lbs/em/LbsEpo;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->epoEnableAuto(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/lbs/em/LbsEpo;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/lbs/em/LbsEpo;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->epoManual(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/lbs/em/LbsEpo;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/lbs/em/LbsEpo;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->epoStressTest(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/lbs/em/LbsEpo;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->epoUpdatePeriod()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/lbs/em/LbsEpo;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/em/LbsEpo;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V

    return-void
.end method

.method private enableEpoButtons()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    return-void
.end method

.method private enableWidgetAfterStress()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->stopStress()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay1:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay2:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    return-void
.end method

.method private epoEnable(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v0}, Lcom/mediatek/common/epo/MtkEpoClientManager;->enable()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v0}, Lcom/mediatek/common/epo/MtkEpoClientManager;->disable()V

    goto :goto_0
.end method

.method private epoEnableAuto(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->enableAutoDownload(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->enableAutoDownload(Z)V

    goto :goto_0
.end method

.method private epoManual(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->startDownload()I

    move-result v0

    const-string v1, "============================"

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startDownload result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->getStartResultString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/lbs/em/LbsEpo;->showMsgToLog(Ljava/lang/String;)V

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->stopDownload()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->enableEpoButtons()V

    goto :goto_0
.end method

.method private epoStressTest(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay1:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay2:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsEpo;III)V

    iput-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->startStress()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v5}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextNumOfLoop:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay1:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay2:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ERR: Parameters error!!"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v5}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->enableWidgetAfterStress()V

    goto :goto_0
.end method

.method private epoUpdatePeriod()V
    .locals 8

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextUpdatePeroid:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v1, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update peroid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V

    const-wide/16 v3, 0xa

    cmp-long v3, v1, v3

    if-gez v3, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "ERR: The update peroid should be larger than 10!!"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const-wide/32 v3, 0x37a4a9

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    const-wide/16 v4, 0x0

    invoke-interface {v3, v4, v5}, Lcom/mediatek/common/epo/MtkEpoClientManager;->setUpdatePeriod(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsEpo;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "ERR: Parameters error!!"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    const-wide/16 v4, 0x3c

    mul-long/2addr v4, v1

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-interface {v3, v4, v5}, Lcom/mediatek/common/epo/MtkEpoClientManager;->setUpdatePeriod(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private getStartResultString(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "Success"

    goto :goto_0

    :pswitch_1
    const-string v0, "EPO is already started"

    goto :goto_0

    :pswitch_2
    const-string v0, "Network is unavailable"

    goto :goto_0

    :pswitch_3
    const-string v0, "EPO is disabled"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getTimeString()Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    const-string v2, "[%02d:%02d:%02d] "

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/util/Date;->getMinutes()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/util/Date;->getSeconds()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initWidget()V
    .locals 2

    const v0, 0x7f050090

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    const v0, 0x7f050091

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    const v0, 0x7f050092

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    const v0, 0x7f050093

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    const v0, 0x7f050096

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextUpdatePeroid:Landroid/widget/EditText;

    const v0, 0x7f050097

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonUpdatePeriod:Landroid/widget/Button;

    const v0, 0x7f050094

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mTextViewProgress:Landroid/widget/TextView;

    const v0, 0x7f050095

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mTextViewEpoFileInfo:Landroid/widget/TextView;

    const v0, 0x7f050099

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextNumOfLoop:Landroid/widget/EditText;

    const v0, 0x7f05009b

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay1:Landroid/widget/EditText;

    const v0, 0x7f05009d

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay2:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mTextViewProgress:Landroid/widget/TextView;

    const/high16 v1, 0x41300000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextNumOfLoop:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mNumOfLoop:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay1:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay1:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextEpoDelay2:Landroid/widget/EditText;

    iget v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mDelay2:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$1;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsEpo$1;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$2;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsEpo$2;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonManual:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$3;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsEpo$3;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonEpoStress:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$4;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsEpo$4;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mButtonUpdatePeriod:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/lbs/em/LbsEpo$5;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/em/LbsEpo$5;-><init>(Lcom/mediatek/lbs/em/LbsEpo;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private showMsgToLog(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->getTimeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/lbs/em/UtilityStringList;->add(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mTextViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStringList:Lcom/mediatek/lbs/em/UtilityStringList;

    invoke-virtual {v1}, Lcom/mediatek/lbs/em/UtilityStringList;->get()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateEpoFileInfo()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->getEpoFileInfo()Lcom/mediatek/common/epo/MtkEpoFileInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mTextViewEpoFileInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/common/epo/MtkEpoFileInfo;->getDownloadTimeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [Downoad Time]\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mediatek/common/epo/MtkEpoFileInfo;->getStartTimeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [Start Time]\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mediatek/common/epo/MtkEpoFileInfo;->getExpireTimeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [Expire Time]\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->setContentView(I)V

    const-string v0, "mtk-epo-client"

    invoke-virtual {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/epo/MtkEpoClientManager;

    iput-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    if-nez v0, :cond_0

    const-string v0, "ERR: cannot get EPO client service"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/em/LbsEpo;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoStatusListener:Lcom/mediatek/common/epo/MtkEpoStatusListener;

    invoke-interface {v0, v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->addStatusListener(Lcom/mediatek/common/epo/MtkEpoStatusListener;)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->initWidget()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mStressTest:Lcom/mediatek/lbs/em/LbsEpo$StressTest;

    invoke-virtual {v0}, Lcom/mediatek/lbs/em/LbsEpo$StressTest;->stopStress()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoStatusListener:Lcom/mediatek/common/epo/MtkEpoStatusListener;

    invoke-interface {v0, v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->removeStatusListener(Lcom/mediatek/common/epo/MtkEpoStatusListener;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableEpo:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->getStatus()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mCheckBoxEnableAuto:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->getAutoDownloadStatus()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEditTextUpdatePeroid:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsEpo;->mEpoMgr:Lcom/mediatek/common/epo/MtkEpoClientManager;

    invoke-interface {v1}, Lcom/mediatek/common/epo/MtkEpoClientManager;->getUpdatePeriod()J

    move-result-wide v1

    const-wide/32 v3, 0xea60

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsEpo;->updateEpoFileInfo()V

    return-void
.end method

.method public sendMessage(IILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "text"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    iput p1, v1, Landroid/os/Message;->what:I

    iput p2, v1, Landroid/os/Message;->arg1:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/mediatek/lbs/em/LbsEpo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
