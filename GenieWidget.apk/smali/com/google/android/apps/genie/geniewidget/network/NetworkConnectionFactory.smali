.class public Lcom/google/android/apps/genie/geniewidget/network/NetworkConnectionFactory;
.super Ljava/lang/Object;
.source "NetworkConnectionFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/network/NetworkConnectionFactory$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/network/NetworkConnectionFactory$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$GConfig$NetworkImplementation:[I

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/GConfig;->NETWORK:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized network type in GConfig: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/GConfig;->NETWORK:Lcom/google/android/apps/genie/geniewidget/GConfig$NetworkImplementation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;-><init>()V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;-><init>(ZZ)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;-><init>(ZZ)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {v0, v3, v3}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;-><init>(ZZ)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;

    invoke-direct {v0, v2, v2}, Lcom/google/android/apps/genie/geniewidget/network/AlwaysFailConnection;-><init>(ZZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
