.class public Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
.super Ljava/lang/Object;
.source "PersistentCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;
    }
.end annotation


# instance fields
.field private cache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;",
            "Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

.field private context:Landroid/content/Context;

.field private fileName:Ljava/lang/String;

.field private maxSize:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/utils/Clock;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->maxSize:I

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->fileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;

    const/high16 v1, 0x3f800000

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;IFZ)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->load()V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->maxSize:I

    return v0
.end method

.method private getCacheFile(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized get([BJ)Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;
    .locals 5
    .param p1    # [B
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-interface {v3}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    new-instance v4, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    invoke-direct {v4, p1, p2, p3}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([BJ)V

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    if-eqz v0, :cond_1

    const-wide/16 v3, 0x0

    cmp-long v3, p2, v3

    if-ltz v3, :cond_0

    iget-wide v3, v0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->timeStamp:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v3, v1, v3

    cmp-long v3, v3, p2

    if-gtz v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized load()V
    .locals 14

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v11, Ljava/io/FileInputStream;

    iget-object v12, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->fileName:Ljava/lang/String;

    invoke-direct {p0, v12}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v11}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x0

    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v3

    if-gez v3, :cond_2

    :cond_0
    :goto_1
    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object v0, v1

    :cond_1
    :goto_2
    monitor-exit p0

    return-void

    :cond_2
    :try_start_3
    new-array v5, v3, [B

    invoke-virtual {v1, v5}, Ljava/io/DataInputStream;->read([B)I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    if-ltz v3, :cond_0

    new-array v6, v3, [B

    invoke-virtual {v1, v6}, Ljava/io/DataInputStream;->read([B)I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v9

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    new-instance v12, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    invoke-direct {v12, v5}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([B)V

    new-instance v13, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    invoke-direct {v13, v6, v9, v10}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([BJ)V

    invoke-virtual {v11, v12, v13}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_4
    const-string v11, "Genie"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IOException reading cache: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catch_2
    move-exception v2

    move-object v0, v1

    :goto_3
    :try_start_5
    const-string v11, "Genie"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Cache file not found: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_1

    :try_start_6
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :catch_3
    move-exception v11

    goto :goto_2

    :catch_4
    move-exception v11

    move-object v0, v1

    goto :goto_2

    :catchall_0
    move-exception v11

    :goto_4
    if-eqz v0, :cond_3

    :try_start_7
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_3
    :goto_5
    :try_start_8
    throw v11
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v11

    :goto_6
    monitor-exit p0

    throw v11

    :catch_5
    move-exception v12

    goto :goto_5

    :catchall_2
    move-exception v11

    move-object v0, v1

    goto :goto_4

    :catch_6
    move-exception v2

    goto :goto_3

    :catchall_3
    move-exception v11

    move-object v0, v1

    goto :goto_6

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public declared-synchronized put([B[B)V
    .locals 2
    .param p1    # [B
    .param p2    # [B

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    invoke-interface {v0}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->put([B[BJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put([B[BJ)V
    .locals 3
    .param p1    # [B
    .param p2    # [B
    .param p3    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    invoke-direct {v1, p1}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([B)V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    invoke-direct {v2, p2, p3, p4}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;-><init>([BJ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->save()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized save()V
    .locals 13

    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->fileName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v10, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->toArray()[Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    const/4 v1, 0x0

    :try_start_2
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v10, Ljava/io/FileOutputStream;

    iget-object v11, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->fileName:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v10}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v10, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->cache:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->size()I

    move-result v10

    add-int/lit8 v5, v10, -0x1

    :goto_1
    if-ltz v5, :cond_2

    aget-object v4, v0, v5

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    iget-object v6, v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    iget-object v7, v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->data:[B

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;

    iget-wide v8, v10, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;->timeStamp:J

    array-length v10, v6

    invoke-virtual {v2, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->write([B)V

    array-length v10, v7

    invoke-virtual {v2, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v2, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    move-object v1, v2

    goto :goto_0

    :catch_0
    move-exception v10

    move-object v1, v2

    goto :goto_0

    :catch_1
    move-exception v3

    :goto_2
    :try_start_5
    const-string v10, "Genie"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cache file not found: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    :catch_2
    move-exception v10

    goto/16 :goto_0

    :catch_3
    move-exception v3

    :goto_3
    :try_start_7
    const-string v10, "Genie"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException writing cache: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_0

    :try_start_8
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    :catch_4
    move-exception v10

    goto/16 :goto_0

    :catchall_0
    move-exception v10

    :goto_4
    if-eqz v1, :cond_4

    :try_start_9
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_4
    :goto_5
    :try_start_a
    throw v10
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception v10

    monitor-exit p0

    throw v10

    :catch_5
    move-exception v11

    goto :goto_5

    :catchall_2
    move-exception v10

    move-object v1, v2

    goto :goto_4

    :catch_6
    move-exception v3

    move-object v1, v2

    goto :goto_3

    :catch_7
    move-exception v3

    move-object v1, v2

    goto :goto_2
.end method
