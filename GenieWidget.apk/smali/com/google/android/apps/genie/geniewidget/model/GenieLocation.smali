.class public Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;
.super Ljava/lang/Object;
.source "GenieLocation.java"


# instance fields
.field private final accuracy:F

.field private geocoder:Landroid/location/Geocoder;

.field private final lat:D

.field private final lng:D


# direct methods
.method public constructor <init>(DDF)V
    .locals 3
    .param p1    # D
    .param p3    # D
    .param p5    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lat:D

    iput-wide p3, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lng:D

    iput p5, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->accuracy:F

    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->geocoder:Landroid/location/Geocoder;

    :cond_0
    return-void
.end method


# virtual methods
.method public getAccuracy()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->accuracy:F

    return v0
.end method

.method public getGeocodedCity()Ljava/lang/String;
    .locals 11

    const/4 v10, 0x0

    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->geocoder:Landroid/location/Geocoder;

    if-nez v0, :cond_2

    :cond_0
    move-object v8, v10

    :cond_1
    :goto_0
    return-object v8

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->geocoder:Landroid/location/Geocoder;

    iget-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lat:D

    iget-wide v3, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lng:D

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move-object v8, v10

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    invoke-virtual {v6}, Landroid/location/Address;->getLocality()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    if-nez v8, :cond_1

    move-object v8, v10

    goto :goto_0

    :catch_0
    move-exception v9

    move-object v8, v10

    goto :goto_0
.end method

.method public getLat()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lat:D

    return-wide v0
.end method

.method public getLng()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lng:D

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lat:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->lng:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " +/-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/model/GenieLocation;->accuracy:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
