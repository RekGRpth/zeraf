.class public Lcom/google/android/apps/genie/geniewidget/ui/PageView;
.super Landroid/widget/ScrollView;
.source "PageView.java"


# instance fields
.field private child:Landroid/view/View;

.field private dimensionChanged:Z

.field private pageHeight:I

.field private pageNumber:I

.field private pageWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public getPageNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageNumber:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/high16 v5, 0x40000000

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->dimensionChanged:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->child:Landroid/view/View;

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->child:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v2, v3, :cond_2

    :cond_1
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageWidth:I

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageHeight:I

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/widget/ScrollView;->onMeasure(II)V

    iput-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->dimensionChanged:Z

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->child:Landroid/view/View;

    :cond_2
    :goto_0
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageWidth:I

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->setMeasuredDimension(II)V

    return-void

    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->child:Landroid/view/View;

    goto :goto_0
.end method

.method public setPageDimension(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageWidth:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageHeight:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageWidth:I

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->dimensionChanged:Z

    :cond_1
    return-void
.end method

.method public setPageNumber(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/PageView;->pageNumber:I

    return-void
.end method
