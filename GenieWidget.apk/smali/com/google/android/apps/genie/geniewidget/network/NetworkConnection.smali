.class public interface abstract Lcom/google/android/apps/genie/geniewidget/network/NetworkConnection;
.super Ljava/lang/Object;
.source "NetworkConnection.java"


# virtual methods
.method public abstract getRawFeed(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;JJLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "JJ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;",
            "Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getUrl(Ljava/lang/String;JZLcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JZ",
            "Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation
.end method

.method public abstract logClick(Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;)V
.end method
