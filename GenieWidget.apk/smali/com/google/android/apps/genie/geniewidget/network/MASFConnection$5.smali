.class Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;
.super Ljava/io/InputStream;
.source "MASFConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->parseGenieResponse(Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field bytesSoFar:I

.field onePercentThreshold:I

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

.field tmpBytes:I

.field final synthetic val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

.field final synthetic val$response:Ljava/io/InputStream;

.field final synthetic val$size:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Ljava/io/InputStream;)V
    .locals 4

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$size:I

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$response:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->bytesSoFar:I

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->tmpBytes:I

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$size:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->onePercentThreshold:I

    return-void
.end method


# virtual methods
.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->bytesSoFar:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->bytesSoFar:I

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->tmpBytes:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->tmpBytes:I

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$size:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->tmpBytes:I

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->onePercentThreshold:I

    if-lt v2, v3, :cond_0

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->tmpBytes:I

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->bytesSoFar:I

    int-to-double v2, v2

    iget v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$size:I

    int-to-double v4, v4

    div-double v0, v2, v4

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$callback:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    const-wide/high16 v3, 0x4059000000000000L

    mul-double/2addr v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->progress(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$5;->val$response:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    return v2
.end method
