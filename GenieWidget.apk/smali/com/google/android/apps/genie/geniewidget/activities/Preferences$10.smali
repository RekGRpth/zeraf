.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->setUpDownloadingPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

.field final synthetic val$prefetchArticles:Landroid/preference/CheckBoxPreference;

.field final synthetic val$prefetchImages:Landroid/preference/CheckBoxPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchImages:Landroid/preference/CheckBoxPreference;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchArticles:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchImages:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchArticles:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->showDialog(I)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchImages:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$10;->val$prefetchImages:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method
