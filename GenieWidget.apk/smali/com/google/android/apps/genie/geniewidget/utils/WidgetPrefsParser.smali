.class public Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;
.super Ljava/lang/Object;
.source "WidgetPrefsParser.java"


# instance fields
.field private data:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser$1;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser$1;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->setData(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public hasFlag(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public serialize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public set(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v11, 0x1

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->clear()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x3

    if-ge v9, v10, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p1, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, ","

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    const-string v9, "="

    invoke-virtual {v2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    array-length v9, v6

    const/4 v10, 0x2

    if-ge v9, v10, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    :try_start_0
    aget-object v9, v6, v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v9, 0x1

    aget-object v9, v6, v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v9

    goto :goto_1
.end method

.method public setPrefs(IZZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const/4 v2, 0x2

    :goto_0
    add-int/2addr v0, v2

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->set(II)V

    return-void

    :cond_1
    move v2, v1

    goto :goto_0
.end method

.method public showNews(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->hasFlag(II)Z

    move-result v0

    return v0
.end method

.method public showWeather(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->hasFlag(II)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->data:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
