.class public Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;
.super Ljava/lang/Object;
.source "ContentRetriever.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;
    }
.end annotation


# instance fields
.field private cacheName:Ljava/lang/String;

.field private final downloader:Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

.field private result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;

    invoke-direct {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/ApacheResourceDownloader;-><init>()V

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/DefaultResourceConverter;

    invoke-direct {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DefaultResourceConverter;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/ResourceDownloader;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceConverter;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->downloader:Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

    return-void
.end method


# virtual methods
.method public getCacheName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->cacheName:Ljava/lang/String;

    return-object v0
.end method

.method public retrieve(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->cacheName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;->ABORTED:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    const/4 v8, 0x0

    :try_start_0
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    const-string v1, "url.txt"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v0, "utf-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/io/FileOutputStream;->write([B)V

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->downloader:Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;

    const-string v3, "index"

    if-eqz p3, :cond_0

    const/16 v4, 0x64

    :cond_0
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/genie/geniewidget/persistance/RecursiveDownloader;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->cacheName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;->SUCCEESSFUL:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v9

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    return-object v0

    :catch_0
    move-exception v0

    move-object v8, v9

    goto :goto_0

    :catch_1
    move-exception v7

    :goto_1
    :try_start_3
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;->FAILED:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v7

    :goto_2
    :try_start_5
    sget-object v0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;->FAILED:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever;->result:Lcom/google/android/apps/genie/geniewidget/services/ContentRetriever$RetrievalResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    :try_start_7
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :goto_4
    throw v0

    :catch_5
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v8, v9

    goto :goto_3

    :catch_6
    move-exception v7

    move-object v8, v9

    goto :goto_2

    :catch_7
    move-exception v7

    move-object v8, v9

    goto :goto_1
.end method
