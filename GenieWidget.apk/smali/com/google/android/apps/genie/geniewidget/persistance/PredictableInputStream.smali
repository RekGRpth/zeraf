.class Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
.super Ljava/io/InputStream;
.source "PredictableInputStream.java"


# instance fields
.field private input:Ljava/io/InputStream;

.field private nextChar:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->input:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public peek()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, -0x1

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->nextChar:I

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0
.end method
