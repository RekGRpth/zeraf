.class Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;
.super Ljava/lang/Object;
.source "MASFConnection.java"

# interfaces
.implements Lcom/google/android/news/masf/protocol/Request$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->makeNetworkRequest(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/news/masf/protocol/PlainRequest;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

.field final synthetic val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

.field final synthetic val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

.field final synthetic val$requestCacheBytes:[B


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;[B)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$requestCacheBytes:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public requestCompleted(Lcom/google/android/news/masf/protocol/Request;Lcom/google/android/news/masf/protocol/Response;)V
    .locals 8
    .param p1    # Lcom/google/android/news/masf/protocol/Request;
    .param p2    # Lcom/google/android/news/masf/protocol/Response;

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getStatusCode()I

    move-result v4

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getStreamLength()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    # invokes: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->parseGenieResponse(Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$100(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;Ljava/io/InputStream;ILcom/google/android/apps/genie/geniewidget/network/NetworkCallback;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    # getter for: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->protoCodec:Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;
    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$200(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;)Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/genie/geniewidget/GenieProtoCodec;->parseGenieResponse(Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->this$0:Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;

    # getter for: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->clock:Lcom/google/android/apps/genie/geniewidget/utils/Clock;
    invoke-static {v5}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$300(Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;)Lcom/google/android/apps/genie/geniewidget/utils/Clock;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/genie/geniewidget/utils/Clock;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;-><init>(Ljava/util/List;ZJ)V

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    invoke-interface {v4, v5, v3}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->success(Ljava/lang/Object;Ljava/lang/Object;)V

    # getter for: Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->genieCache:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection;->access$400()Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$requestCacheBytes:[B

    invoke-virtual {v2}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->put([B[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-interface {v4, v1}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->failure(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Masf error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/news/masf/protocol/Response;->getStatusCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->failure(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public requestFailed(Lcom/google/android/news/masf/protocol/Request;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Lcom/google/android/news/masf/protocol/Request;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/MASFConnection$2;->val$continuation:Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;

    invoke-interface {v0, p2}, Lcom/google/android/apps/genie/geniewidget/network/NetworkCallback;->failure(Ljava/lang/Exception;)V

    return-void
.end method
