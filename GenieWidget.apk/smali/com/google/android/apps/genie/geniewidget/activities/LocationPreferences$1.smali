.class Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;
.super Ljava/lang/Object;
.source "LocationPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationSuccess(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

.field final synthetic val$location:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->val$location:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->searching:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$100(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v3, 0x7f0c002d

    invoke-virtual {v2, v3}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v2, 0x7f0c0003

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    const v2, 0x7f0c0004

    invoke-virtual {v1, v2}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->val$location:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocation:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Landroid/preference/EditTextPreference;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$1;->val$location:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
