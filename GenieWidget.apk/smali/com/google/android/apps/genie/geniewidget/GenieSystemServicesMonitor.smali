.class public Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;
.super Ljava/lang/Object;
.source "GenieSystemServicesMonitor.java"


# instance fields
.field private final ctx:Landroid/content/Context;

.field private mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->ctx:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public declared-synchronized registerConnectivityIntent(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor$1;-><init>(Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;I)V

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregisterConnectivityIntent()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->ctx:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/GenieSystemServicesMonitor;->mConnectivityActionReceiver:Landroid/content/BroadcastReceiver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
