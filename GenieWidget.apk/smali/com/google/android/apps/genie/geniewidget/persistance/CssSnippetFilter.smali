.class Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;
.super Ljava/lang/Object;
.source "CssSnippetFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$1;,
        Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;
    }
.end annotation


# instance fields
.field private charsetName:Ljava/lang/String;

.field private endCharacter:I

.field private pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

.field private resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->url:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->endCharacter:I

    return-void
.end method

.method private appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p1    # Ljava/io/ByteArrayOutputStream;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # I

    if-lez p3, :cond_0

    invoke-virtual {p1, p3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    neg-int v0, p3

    invoke-static {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/io/ByteArrayOutputStream;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "ISO-8859-1"

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, ""

    goto :goto_0
.end method

.method private downloadTransformed(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->url:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface {v2, v3, p1, v4, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/CssEscapeEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->charsetName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->writeEscaped(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public process()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, -0x1

    const/16 v8, 0x2f

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    if-eq v0, v9, :cond_1

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$1;->$SwitchMap$com$google$android$apps$genie$geniewidget$persistance$CssSnippetFilter$ParsingState:[I

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v0

    goto :goto_0

    :pswitch_0
    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->endCharacter:I

    if-ne v0, v6, :cond_2

    if-eq v0, v9, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    :cond_1
    :goto_2
    return-void

    :cond_2
    sparse-switch v0, :sswitch_data_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_0
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->AFTER_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_1
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_1

    :pswitch_1
    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->endCharacter:I

    if-ne v0, v6, :cond_3

    if-eq v0, v9, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_2

    :cond_3
    sparse-switch v0, :sswitch_data_1

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_2
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_3
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_4
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_5
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto :goto_1

    :sswitch_6
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto :goto_1

    :sswitch_7
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_U:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :pswitch_2
    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->endCharacter:I

    if-ne v0, v6, :cond_4

    if-eq v0, v9, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_2

    :cond_4
    sparse-switch v0, :sswitch_data_2

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_8
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->BEFORE_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_9
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_a
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    goto/16 :goto_1

    :sswitch_b
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->AFTER_COLUMN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :pswitch_3
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    sparse-switch v0, :sswitch_data_3

    goto/16 :goto_1

    :sswitch_c
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :sswitch_d
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_4
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_5
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    sparse-switch v0, :sswitch_data_4

    goto/16 :goto_1

    :sswitch_e
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :sswitch_f
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_6
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_7
    sparse-switch v0, :sswitch_data_5

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_10
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_R:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_11
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    :cond_5
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_8
    sparse-switch v0, :sswitch_data_6

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_12
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->TOKEN_L:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_13
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    :cond_6
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_9
    sparse-switch v0, :sswitch_data_7

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_14
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :sswitch_15
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_16
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-static {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssCommentParser;->bypassComment(Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->write(I)V

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_1

    :pswitch_a
    sparse-switch v0, :sswitch_data_8

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_EXPECTING_CLOSE_BRACKET:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1

    :sswitch_17
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_18
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->downloadTransformed(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :sswitch_19
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_1a
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :pswitch_b
    sparse-switch v0, :sswitch_data_9

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_1b
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_3

    :sswitch_1c
    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_3

    :pswitch_c
    sparse-switch v0, :sswitch_data_a

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    :goto_4
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_1d
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_4

    :sswitch_1e
    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_4

    :pswitch_d
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    sparse-switch v0, :sswitch_data_b

    if-lez v4, :cond_7

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_13

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_5
    const/4 v6, 0x6

    if-ne v4, v6, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_14

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_6
    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->peek()I

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    goto/16 :goto_1

    :sswitch_1f
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, -0x30

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_20
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, 0xa

    add-int/lit8 v6, v6, -0x41

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_21
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v0, 0xa

    add-int/lit8 v6, v6, -0x61

    add-int/2addr v3, v6

    goto :goto_5

    :sswitch_22
    if-lez v4, :cond_8

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_9

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_8
    goto :goto_5

    :cond_8
    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_9
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_8

    :sswitch_23
    if-nez v4, :cond_b

    const-string v6, "\""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_a

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_9
    goto :goto_5

    :cond_a
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_9

    :cond_b
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_c

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_5

    :cond_c
    const-string v6, "\""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_d

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_a
    goto/16 :goto_5

    :cond_d
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_a

    :sswitch_24
    if-nez v4, :cond_f

    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_e

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_b
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_5

    :cond_e
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_b

    :cond_f
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_10

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_FUNCTION:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_b

    :cond_10
    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_ESCAPE_IN_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    if-ne v5, v6, :cond_11

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_DOUBLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    :goto_c
    goto :goto_b

    :cond_11
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto :goto_c

    :sswitch_25
    if-lez v4, :cond_12

    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_5

    :cond_13
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_5

    :cond_14
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->URL_INSIDE_SINGLE_QUOTES:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    goto/16 :goto_6

    :pswitch_e
    packed-switch v0, :pswitch_data_1

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->read()I

    move-result v6

    invoke-direct {p0, v1, v2, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->appendBuffer(Ljava/io/ByteArrayOutputStream;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1

    :pswitch_f
    sget-object v5, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;->REGULAR_TOKEN:Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter$ParsingState;

    invoke-direct {p0, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->downloadTransformed(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->pipe:Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;

    invoke-interface {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;->pipe()I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2f -> :sswitch_1
        0x3a -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_3
        0x27 -> :sswitch_4
        0x2f -> :sswitch_5
        0x3b -> :sswitch_2
        0x75 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_b
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_9
        0x2f -> :sswitch_a
        0x3b -> :sswitch_8
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x22 -> :sswitch_c
        0x5c -> :sswitch_d
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x27 -> :sswitch_e
        0x5c -> :sswitch_f
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x2f -> :sswitch_11
        0x72 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x2f -> :sswitch_13
        0x6c -> :sswitch_12
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x9 -> :sswitch_15
        0xa -> :sswitch_15
        0xd -> :sswitch_15
        0x20 -> :sswitch_15
        0x28 -> :sswitch_14
        0x2f -> :sswitch_16
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0x9 -> :sswitch_17
        0xa -> :sswitch_17
        0xd -> :sswitch_17
        0x20 -> :sswitch_17
        0x22 -> :sswitch_19
        0x27 -> :sswitch_1a
        0x29 -> :sswitch_18
    .end sparse-switch

    :sswitch_data_9
    .sparse-switch
        0x22 -> :sswitch_1b
        0x5c -> :sswitch_1c
    .end sparse-switch

    :sswitch_data_a
    .sparse-switch
        0x27 -> :sswitch_1d
        0x5c -> :sswitch_1e
    .end sparse-switch

    :sswitch_data_b
    .sparse-switch
        0x20 -> :sswitch_22
        0x22 -> :sswitch_23
        0x27 -> :sswitch_24
        0x30 -> :sswitch_1f
        0x31 -> :sswitch_1f
        0x32 -> :sswitch_1f
        0x33 -> :sswitch_1f
        0x34 -> :sswitch_1f
        0x35 -> :sswitch_1f
        0x36 -> :sswitch_1f
        0x37 -> :sswitch_1f
        0x38 -> :sswitch_1f
        0x39 -> :sswitch_1f
        0x41 -> :sswitch_20
        0x42 -> :sswitch_20
        0x43 -> :sswitch_20
        0x44 -> :sswitch_20
        0x45 -> :sswitch_20
        0x46 -> :sswitch_20
        0x5c -> :sswitch_25
        0x61 -> :sswitch_21
        0x62 -> :sswitch_21
        0x63 -> :sswitch_21
        0x64 -> :sswitch_21
        0x65 -> :sswitch_21
        0x66 -> :sswitch_21
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x29
        :pswitch_f
    .end packed-switch
.end method
