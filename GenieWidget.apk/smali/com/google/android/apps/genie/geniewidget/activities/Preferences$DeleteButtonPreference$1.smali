.class Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;
.super Ljava/lang/Object;
.source "Preferences.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->onBindView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v1, v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getCustomTopicPreference()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->topic:Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->access$300(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)Lcom/google/android/apps/genie/geniewidget/model/NewsTopic;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->parent:Landroid/preference/PreferenceCategory;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->access$400(Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;)Landroid/preference/PreferenceCategory;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference$1;->this$1:Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;

    iget-object v1, v1, Lcom/google/android/apps/genie/geniewidget/activities/Preferences$DeleteButtonPreference;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/Preferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/Preferences;->access$200(Lcom/google/android/apps/genie/geniewidget/activities/Preferences;)Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getGenieContext()Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->setCustomTopicPreference(Ljava/lang/Iterable;)V

    return-void
.end method
