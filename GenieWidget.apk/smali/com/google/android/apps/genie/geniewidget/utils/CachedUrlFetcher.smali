.class public Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;
.super Ljava/lang/Object;
.source "CachedUrlFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;,
        Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$UrlInputStreamSupplier;,
        Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;
    }
.end annotation


# instance fields
.field protected final cache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;",
            ">;"
        }
    .end annotation
.end field

.field private inputStreamSupplier:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$UrlInputStreamSupplier;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$UrlInputStreamSupplier;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->inputStreamSupplier:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$1;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;I)V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->cache:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private makeRequest(Ljava/lang/String;)[B
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->inputStreamSupplier:Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;

    invoke-interface {v4, p1}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$InputStreamSupplier;->getInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v4, 0x1000

    new-array v1, v4, [B

    const/4 v3, -0x1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v4

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_2
    return-object v4
.end method


# virtual methods
.method public fetch(Ljava/lang/String;)[B
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->cache:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->cache:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;

    iget-object v0, v1, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;->data:[B

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->makeRequest(Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;->cache:Ljava/util/LinkedHashMap;

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;

    invoke-direct {v2, v0}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$Payload;-><init>([B)V

    invoke-virtual {v1, p1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public fetchAsync(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/genie/geniewidget/utils/Callback",
            "<[B>;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;-><init>(Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher;Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/utils/Callback;)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/utils/CachedUrlFetcher$3;->start()V

    return-void
.end method
