.class Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;
.super Landroid/os/AsyncTask;
.source "GenieApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/GenieApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateDatabaseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final now:J

.field private final payload:Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

.field private final req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/genie/geniewidget/GenieApplication;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;J)V
    .locals 0
    .param p2    # Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;
    .param p3    # Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;
    .param p4    # J

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->payload:Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    iput-wide p4, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->now:J

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->payload:Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/network/GeniePayload;->getPayload()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem;->getType()Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;->WEATHER_FORECAST:Lcom/google/android/apps/genie/geniewidget/items/DashboardItem$ItemType;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->this$0:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    # getter for: Lcom/google/android/apps/genie/geniewidget/GenieApplication;->genieContext:Lcom/google/android/apps/genie/geniewidget/GenieContext;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->access$100(Lcom/google/android/apps/genie/geniewidget/GenieApplication;)Lcom/google/android/apps/genie/geniewidget/GenieContext;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->req:Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;

    iget-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/GenieApplication$UpdateDatabaseTask;->now:J

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/apps/genie/geniewidget/providers/WeatherProvider;->updateContent(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/network/GenieRequest;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;J)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method
