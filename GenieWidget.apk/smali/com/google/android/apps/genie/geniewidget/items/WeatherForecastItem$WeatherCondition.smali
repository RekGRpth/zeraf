.class public Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
.super Ljava/lang/Object;
.source "WeatherForecastItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WeatherCondition"
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private final endTimestamp:J

.field private highTemp:I

.field private final humidity:Ljava/lang/String;

.field private imageUrl:Ljava/lang/String;

.field private lowTemp:I

.field private final pointInTime:Ljava/lang/String;

.field private probPrecipitation:I

.field private sunrise:Ljava/lang/String;

.field private sunset:Ljava/lang/String;

.field private final temperature:I

.field private final timestamp:J

.field private final wind:Ljava/lang/String;


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # Ljava/lang/String;
    .param p12    # Ljava/lang/String;
    .param p13    # Ljava/lang/String;
    .param p14    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->timestamp:J

    iput-wide p3, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->endTimestamp:J

    iput-object p5, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->pointInTime:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->imageUrl:Ljava/lang/String;

    iput p8, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->temperature:I

    iput p9, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->highTemp:I

    iput p10, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->lowTemp:I

    iput-object p11, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->wind:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->humidity:Ljava/lang/String;

    iput-object p13, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunrise:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunset:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->endTimestamp:J

    return-wide v0
.end method

.method public getHighTemp()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->highTemp:I

    return v0
.end method

.method public getHumidity()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->humidity:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLowTemp()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->lowTemp:I

    return v0
.end method

.method public getPointInTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->pointInTime:Ljava/lang/String;

    return-object v0
.end method

.method public getProbPrecipitation()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->probPrecipitation:I

    return v0
.end method

.method public getSunrise()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunrise:Ljava/lang/String;

    return-object v0
.end method

.method public getSunset()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunset:Ljava/lang/String;

    return-object v0
.end method

.method public getTemperature()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->temperature:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->timestamp:J

    return-wide v0
.end method

.method public getWind()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->wind:Ljava/lang/String;

    return-object v0
.end method

.method public hasHighTemp()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->highTemp:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLowTemp()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->lowTemp:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTemperature()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->temperature:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->description:Ljava/lang/String;

    return-void
.end method

.method public setHighTemp(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->highTemp:I

    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->imageUrl:Ljava/lang/String;

    return-void
.end method

.method public setLowTemp(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->lowTemp:I

    return-void
.end method

.method public setProbPrecipitation(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->probPrecipitation:I

    return-void
.end method

.method public setSunrise(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunrise:Ljava/lang/String;

    return-void
.end method

.method public setSunset(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->sunset:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
