.class Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;
.super Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;
.source "HtmlParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->getInterestingAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

.field final synthetic val$download:Z

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/lang/String;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->val$tag:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->val$download:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$AttributeListener;-><init>(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$1;)V

    return-void
.end method


# virtual methods
.method public onAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;Ljava/lang/String;)Z
    .locals 11
    .param p1    # Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v1, 0x20

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    const/16 v1, 0x3d

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    const-string v1, "style"

    invoke-virtual {p3, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v7

    const/16 v1, 0x27

    if-ne v7, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, "\'"

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    :goto_0
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$400(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;

    invoke-direct {v2, v10, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;
    invoke-static {v3}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$500(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$300(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;-><init>(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/persistance/DataPipe;Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/persistance/CssSnippetFilter;->process()V

    invoke-virtual {v10}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v7

    const/4 v1, -0x1

    if-eq v7, v1, :cond_3

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    :cond_0
    :goto_1
    const/4 v1, 0x1

    :goto_2
    return v1

    :cond_1
    const/16 v1, 0x22

    if-ne v7, v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, "\""

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, " \t\r\n>"

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->val$tag:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->val$tag:Ljava/lang/String;

    invoke-virtual {p3, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->peek()I

    move-result v7

    const/16 v1, 0x27

    if-ne v7, v1, :cond_5

    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, "\'"

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    :goto_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v6, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;

    invoke-direct {v6, v10, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->read()I

    move-result v7

    :goto_4
    const/4 v1, -0x1

    if-eq v7, v1, :cond_8

    if-lez v7, :cond_7

    invoke-virtual {v8, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :goto_5
    invoke-virtual {v6}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityDecodingDataPipe;->read()I

    move-result v7

    goto :goto_4

    :cond_5
    const/16 v1, 0x22

    if-ne v7, v1, :cond_6

    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, "\""

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;->read()I

    goto :goto_3

    :cond_6
    new-instance v10, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;

    const-string v1, " \t\r\n>"

    invoke-direct {v10, p1, v1}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->reset()V

    neg-int v1, v7

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/UnicodeUtils;->decodeUtf32(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # invokes: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->bytesToString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$200(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0x22

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->resolver:Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$500(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->baseUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$400(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$300(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->val$download:Z

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/apps/genie/geniewidget/persistance/ResourceResolver;->convertResource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlEntityEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;

    # getter for: Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->charsetName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;->access$300(Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    const/16 v1, 0x22

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_9
    invoke-virtual {v10}, Lcom/google/android/apps/genie/geniewidget/persistance/SpecialEndCharacterInputStream;->getEndCharacter()I

    move-result v7

    const/16 v1, 0x3e

    if-ne v7, v1, :cond_0

    invoke-virtual {p2, v7}, Ljava/io/OutputStream;->write(I)V

    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_a
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/genie/geniewidget/persistance/HtmlParser$2;->bypassAttribute(Lcom/google/android/apps/genie/geniewidget/persistance/PredictableInputStream;Ljava/io/OutputStream;)V

    goto/16 :goto_1
.end method
