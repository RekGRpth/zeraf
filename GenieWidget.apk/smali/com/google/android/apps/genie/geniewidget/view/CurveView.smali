.class public Lcom/google/android/apps/genie/geniewidget/view/CurveView;
.super Landroid/view/View;
.source "CurveView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;
    }
.end annotation


# static fields
.field private static final CURVE_DASH_INTERVALS:[F

.field private static final RAIN_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

.field private static final RAIN_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

.field private static final TEMP_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

.field private static final TEMP_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

.field private static final sDashedCurve:Landroid/graphics/DashPathEffect;


# instance fields
.field private context:Landroid/content/Context;

.field private currentTime:J

.field private endTime:J

.field private height:I

.field private knewMotionIntent:Z

.field listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

.field private final mCurve:Landroid/graphics/Paint;

.field private final mEnd:Ljava/util/Date;

.field private final mLine:Landroid/graphics/Paint;

.field private final mScale:Landroid/graphics/Paint;

.field private final mStart:Ljava/util/Date;

.field private final mStartDate:Ljava/util/Date;

.field private motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

.field private precipitation:Landroid/graphics/Path;

.field precipitationData:[F

.field private startTime:J

.field private sunriseHour:I

.field private sunriseMin:I

.field private sunsetHour:I

.field private sunsetMin:I

.field private tempHigh:I

.field private tempLow:I

.field private temperature:Landroid/graphics/Path;

.field temperatureData:[F

.field private topPrecipitation:F

.field private topTemp:F

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->TEMP_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->TEMP_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->RAIN_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->RAIN_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

    new-instance v0, Landroid/graphics/DashPathEffect;

    sget-object v1, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->CURVE_DASH_INTERVALS:[F

    array-length v2, v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    sput-object v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sDashedCurve:Landroid/graphics/DashPathEffect;

    return-void

    :array_0
    .array-data 4
        0x40800000
        0x40800000
    .end array-data

    :array_1
    .array-data 4
        -0x1a0000fe
        -0x1a859dd8
    .end array-data

    :array_2
    .array-data 4
        -0x1a0636ff
        -0x1ac2ceec
    .end array-data

    :array_3
    .array-data 4
        -0x1a330301
        -0x1ab99754
    .end array-data

    :array_4
    .array-data 4
        -0x1a996907
        -0x1ad3d1c1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;[F[FJJLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;
    .param p3    # [F
    .param p4    # [F
    .param p5    # J
    .param p7    # J
    .param p9    # Ljava/lang/String;
    .param p10    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->context:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setClickable(Z)V

    iput-wide p5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    iput-wide p7, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

    iput-object p3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    iput-object p4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    const/16 v2, 0x3a

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseHour:I

    add-int/lit8 v2, v1, 0x1

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseMin:I

    const/16 v2, 0x3a

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, 0x0

    move-object/from16 v0, p10

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetHour:I

    add-int/lit8 v2, v1, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetMin:I

    new-instance v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-direct {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    const/16 v3, 0xff

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    const/high16 v3, 0x41400000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    const-string v3, "Droid Sans"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    const/16 v3, 0x5e

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    const/high16 v3, 0x41400000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    const-string v3, "Droid Sans"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    const/16 v3, 0xff

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p5, p6}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStartDate:Ljava/util/Date;

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStartDate:Ljava/util/Date;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/Date;->setMinutes(I)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStartDate:Ljava/util/Date;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/Date;->setSeconds(I)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p5, p6}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStart:Ljava/util/Date;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p7, p8}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mEnd:Ljava/util/Date;

    return-void
.end method

.method private drawGradient(Landroid/graphics/Canvas;FLandroid/graphics/drawable/GradientDrawable;Landroid/graphics/drawable/GradientDrawable;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # F
    .param p3    # Landroid/graphics/drawable/GradientDrawable;
    .param p4    # Landroid/graphics/drawable/GradientDrawable;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStart:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getHours()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3c

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStart:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getMinutes()I

    move-result v9

    add-int v5, v8, v9

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mEnd:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getHours()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3c

    iget-object v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mEnd:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getMinutes()I

    move-result v9

    add-int v1, v8, v9

    if-ge v1, v5, :cond_0

    add-int/lit16 v1, v1, 0x5a0

    :cond_0
    sub-int v4, v1, v5

    iget v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseHour:I

    mul-int/lit8 v8, v8, 0x3c

    add-int/lit16 v8, v8, 0x5a0

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunriseMin:I

    add-int v6, v8, v9

    iget v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetHour:I

    mul-int/lit8 v8, v8, 0x3c

    add-int/lit16 v8, v8, 0x5a0

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sunsetMin:I

    add-int v7, v8, v9

    :goto_0
    if-le v6, v5, :cond_1

    add-int/lit16 v6, v6, -0x5a0

    goto :goto_0

    :cond_1
    :goto_1
    if-le v7, v5, :cond_2

    add-int/lit16 v7, v7, -0x5a0

    goto :goto_1

    :cond_2
    :goto_2
    if-ge v6, v1, :cond_4

    if-ge v7, v1, :cond_4

    if-ge v6, v7, :cond_3

    move-object/from16 v0, p4

    sub-int v8, v7, v5

    int-to-float v8, v8

    int-to-float v9, v4

    div-float/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v9, v9

    mul-float v2, v8, v9

    add-int/lit16 v6, v6, 0x5a0

    sub-int v8, v6, v5

    int-to-float v8, v8

    int-to-float v9, v4

    div-float/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v9, v9

    mul-float v3, v8, v9

    :goto_3
    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v8, v8

    const/high16 v9, 0x41f00000

    add-float/2addr v9, p2

    float-to-int v9, v9

    float-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    iget v11, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v11, v11, 0x1e

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    :cond_3
    move-object v0, p3

    sub-int v8, v6, v5

    int-to-float v8, v8

    int-to-float v9, v4

    div-float/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v9, v9

    mul-float v2, v8, v9

    add-int/lit16 v7, v7, 0x5a0

    sub-int v8, v7, v5

    int-to-float v8, v8

    int-to-float v9, v4

    div-float/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v9, v9

    mul-float v3, v8, v9

    goto :goto_3

    :cond_4
    return-void
.end method

.method private formatPrecipitation([F)[F
    .locals 5
    .param p1    # [F

    const/high16 v4, 0x42c80000

    array-length v2, p1

    new-array v1, v2, [F

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget v2, p1, v0

    sub-float v2, v4, v2

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private formatTemperature([F)[F
    .locals 8
    .param p1    # [F

    const/4 v6, 0x0

    const/high16 v7, 0x40a00000

    array-length v5, p1

    new-array v4, v5, [F

    aget v1, p1, v6

    aget v2, p1, v6

    const/4 v0, 0x1

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    aget v5, p1, v0

    cmpl-float v5, v5, v1

    if-lez v5, :cond_0

    aget v1, p1, v0

    :cond_0
    aget v5, p1, v0

    cmpg-float v5, v5, v2

    if-gez v5, :cond_1

    aget v2, p1, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    div-float v5, v1, v7

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/lit8 v5, v5, 0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    div-float v5, v2, v7

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/lit8 v5, v5, 0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    if-ne v5, v6, :cond_3

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    add-int/lit8 v5, v5, -0x5

    iput v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    :cond_3
    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    sub-int/2addr v5, v6

    int-to-float v3, v5

    const/4 v0, 0x0

    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_4

    aget v5, p1, v0

    sub-float v5, v3, v5

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v3

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    return-object v4
.end method

.method private getCurve([F[F[F[F)Landroid/graphics/Path;
    .locals 11
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F
    .param p4    # [F

    const/high16 v10, -0x40000000

    const/4 v2, 0x0

    const/high16 v9, 0x40000000

    const/high16 v8, 0x41f00000

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    aget v1, p2, v2

    add-float/2addr v1, v8

    invoke-virtual {v0, v10, v1}, Landroid/graphics/Path;->moveTo(FF)V

    aget v1, p1, v2

    aget v2, p2, v2

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v7, 0x0

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v7, v1, :cond_0

    aget v1, p1, v7

    aget v2, p3, v7

    add-float/2addr v1, v2

    aget v2, p2, v7

    aget v3, p4, v7

    add-float/2addr v2, v3

    add-float/2addr v2, v8

    add-int/lit8 v3, v7, 0x1

    aget v3, p1, v3

    add-int/lit8 v4, v7, 0x1

    aget v4, p3, v4

    sub-float/2addr v3, v4

    add-int/lit8 v4, v7, 0x1

    aget v4, p2, v4

    add-int/lit8 v5, v7, 0x1

    aget v5, p4, v5

    sub-float/2addr v4, v5

    add-float/2addr v4, v8

    add-int/lit8 v5, v7, 0x1

    aget v5, p1, v5

    add-int/lit8 v6, v7, 0x1

    aget v6, p2, v6

    add-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    aget v2, p2, v2

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v2, v2

    add-float/2addr v2, v9

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v1, v1

    add-float/2addr v1, v9

    add-float/2addr v1, v8

    invoke-virtual {v0, v10, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    return-object v0
.end method

.method private getTopCPoint([F[F[F)F
    .locals 4
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F

    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v1, v2

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    aget v2, p2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aget v2, p2, v0

    aget v3, p3, v0

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-int/lit8 v2, v0, 0x1

    aget v2, p2, v2

    aget v3, p3, v0

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget v2, p2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    return v1
.end method

.method private static initCPoints([F[F[F[F)V
    .locals 12
    .param p0    # [F
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F

    const/4 v9, 0x0

    const v11, 0x3eaaaaab

    const/4 v10, 0x0

    array-length v3, p0

    add-int/lit8 v6, v3, -0x1

    new-array v4, v6, [F

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_0

    add-int/lit8 v6, v2, 0x1

    aget v6, p1, v6

    aget v7, p1, v2

    sub-float/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget v7, p0, v7

    aget v8, p0, v2

    sub-float/2addr v7, v8

    div-float/2addr v6, v7

    aput v6, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    aget v6, v4, v9

    aput v6, p3, v9

    const/4 v2, 0x1

    :goto_1
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_1

    const/high16 v6, 0x3f000000

    add-int/lit8 v7, v2, -0x1

    aget v7, v4, v7

    aget v8, v4, v2

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    aput v6, p3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v6, v3, -0x1

    add-int/lit8 v7, v3, -0x2

    aget v7, v4, v7

    aput v7, p3, v6

    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_5

    aget v6, v4, v2

    cmpl-float v6, v6, v10

    if-nez v6, :cond_3

    aput v10, p3, v2

    add-int/lit8 v6, v2, 0x1

    aput v10, p3, v6

    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v6, p3, v2

    aget v7, v4, v2

    div-float v0, v6, v7

    add-int/lit8 v6, v2, 0x1

    aget v6, p3, v6

    aget v7, v4, v2

    div-float v1, v6, v7

    cmpl-float v6, v0, v10

    if-ltz v6, :cond_4

    cmpl-float v6, v1, v10

    if-ltz v6, :cond_4

    const-wide/high16 v6, 0x4008000000000000L

    mul-float v8, v0, v0

    mul-float v9, v1, v1

    add-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    double-to-float v5, v6

    const/high16 v6, 0x3f800000

    cmpg-float v6, v5, v6

    if-gez v6, :cond_2

    aget v6, p3, v2

    mul-float/2addr v6, v5

    aput v6, p3, v2

    add-int/lit8 v6, v2, 0x1

    aget v7, p3, v6

    mul-float/2addr v7, v5

    aput v7, p3, v6

    goto :goto_3

    :cond_4
    aput v10, p3, v2

    add-int/lit8 v6, v2, 0x1

    aput v10, p3, v6

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/lit8 v6, v3, -0x1

    if-ge v2, v6, :cond_6

    add-int/lit8 v6, v2, 0x1

    aget v6, p0, v6

    aget v7, p0, v2

    sub-float/2addr v6, v7

    mul-float/2addr v6, v11

    aput v6, p2, v2

    aget v6, p3, v2

    aget v7, p2, v2

    mul-float/2addr v6, v7

    aput v6, p3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    add-int/lit8 v6, v3, -0x1

    add-int/lit8 v7, v3, -0x1

    aget v7, p0, v7

    add-int/lit8 v8, v3, -0x2

    aget v8, p0, v8

    sub-float/2addr v7, v8

    mul-float/2addr v7, v11

    aput v7, p2, v6

    add-int/lit8 v6, v3, -0x1

    add-int/lit8 v7, v3, -0x1

    aget v7, p1, v7

    add-int/lit8 v8, v3, -0x2

    aget v8, p1, v8

    sub-float/2addr v7, v8

    mul-float/2addr v7, v11

    aput v7, p3, v6

    return-void
.end method

.method private initPoints([F[F[F)V
    .locals 4
    .param p1    # [F
    .param p2    # [F
    .param p3    # [F

    array-length v2, p1

    add-int/lit8 v0, v2, -0x1

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v3, v0

    div-float/2addr v2, v3

    aput v2, p2, v1

    aget v2, p1, v1

    aput v2, p3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    aput v2, p2, v0

    aget v2, p1, v0

    aput v2, p3, v0

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 27
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v16

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v6, v2

    sget-object v7, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topTemp:F

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->TEMP_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->TEMP_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->drawGradient(Landroid/graphics/Canvas;FLandroid/graphics/drawable/GradientDrawable;Landroid/graphics/drawable/GradientDrawable;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v6, v2

    sget-object v7, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topPrecipitation:F

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->RAIN_DAY_COLORS:Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->RAIN_NIGHT_COLORS:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->drawGradient(Landroid/graphics/Canvas;FLandroid/graphics/drawable/GradientDrawable;Landroid/graphics/drawable/GradientDrawable;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v6, v2

    sget-object v7, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->sDashedCurve:Landroid/graphics/DashPathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mCurve:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    add-int/lit8 v2, v2, 0x32

    add-int/lit8 v2, v2, -0x1

    int-to-float v6, v2

    sget-object v7, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    const/4 v14, 0x0

    :goto_0
    const/16 v2, 0xa

    if-gt v14, v2, :cond_1

    const/high16 v2, 0x41f00000

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    int-to-float v5, v5

    mul-float/2addr v3, v5

    const/high16 v5, 0x41200000

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float v4, v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    rem-int/lit8 v2, v14, 0x5

    if-nez v2, :cond_0

    const/16 v2, 0xa

    :goto_1
    sub-int v2, v3, v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v6, v4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x5

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    sub-int/2addr v2, v3

    div-int/lit8 v18, v2, 0x5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    div-int/lit8 v2, v2, 0x5

    rem-int/lit8 v11, v2, 0x2

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v18

    if-gt v14, v0, :cond_3

    const/high16 v2, 0x41f00000

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v3, v5

    move/from16 v0, v18

    int-to-float v5, v0

    div-float/2addr v3, v5

    add-float v4, v2, v3

    const/4 v3, 0x0

    add-int v2, v11, v14

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_2

    const/high16 v5, 0x41200000

    :goto_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v6, v4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_2
    const/high16 v5, 0x40a00000

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const-string v2, "100%"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0xc

    int-to-float v3, v3

    const/high16 v5, 0x42040000

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const-string v2, "0%"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0xc

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, 0x1e

    add-int/lit8 v5, v5, 0x3

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempHigh:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x41400000

    const/high16 v5, 0x42040000

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->tempLow:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u00b0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x41400000

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v5, v5, 0x1e

    add-int/lit8 v5, v5, 0x3

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long v19, v2, v7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    add-int/lit8 v2, v2, 0x1e

    add-int/lit8 v2, v2, 0x32

    add-int/lit8 v12, v2, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v21

    :goto_4
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    cmp-long v2, v21, v2

    if-gez v2, :cond_4

    const-wide/32 v2, 0x36ee80

    add-long v21, v21, v2

    goto :goto_4

    :cond_4
    move-wide/from16 v14, v21

    :goto_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    cmp-long v2, v14, v2

    if-gez v2, :cond_b

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long v2, v14, v2

    long-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move-wide/from16 v0, v19

    long-to-float v3, v0

    div-float v6, v2, v3

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v14, v15}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->getHours()I

    move-result v13

    rem-int/lit8 v2, v13, 0x3

    if-nez v2, :cond_6

    const/16 v2, 0xa

    :goto_6
    sub-int v2, v12, v2

    int-to-float v7, v2

    int-to-float v9, v12

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v8, v6

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    rem-int/lit8 v2, v13, 0x6

    if-nez v2, :cond_5

    const-string v23, ""

    div-int/lit8 v2, v13, 0x6

    packed-switch v2, :pswitch_data_0

    :goto_7
    add-int/lit8 v2, v12, -0x14

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mScale:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    const-wide/32 v2, 0x36ee80

    add-long/2addr v14, v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x5

    goto :goto_6

    :pswitch_0
    if-eqz v16, :cond_7

    const-string v23, "0"

    :goto_8
    goto :goto_7

    :cond_7
    const-string v23, "12 AM"

    goto :goto_8

    :pswitch_1
    if-eqz v16, :cond_8

    const-string v23, "6"

    :goto_9
    goto :goto_7

    :cond_8
    const-string v23, "6 AM"

    goto :goto_9

    :pswitch_2
    if-eqz v16, :cond_9

    const-string v23, "12"

    :goto_a
    goto :goto_7

    :cond_9
    const-string v23, "12 PM"

    goto :goto_a

    :pswitch_3
    if-eqz v16, :cond_a

    const-string v23, "18"

    :goto_b
    goto :goto_7

    :cond_a
    const-string v23, "6 PM"

    goto :goto_b

    :cond_b
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v2, 0xb

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v25

    const/16 v2, 0xc

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v26

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long/2addr v2, v7

    long-to-float v2, v2

    move-wide/from16 v0, v19

    long-to-float v3, v0

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float v6, v2, v3

    const/4 v7, 0x0

    add-int/lit8 v2, v12, -0x28

    int-to-float v9, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v8, v6

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v2, v12, -0xa

    int-to-float v7, v2

    int-to-float v9, v12

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v8, v6

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    if-eqz v16, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xa

    move/from16 v0, v26

    if-ge v0, v2, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    :goto_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v24

    const/high16 v2, 0x40000000

    div-float v2, v24, v2

    cmpg-float v2, v6, v2

    if-gez v2, :cond_11

    const/high16 v2, 0x40000000

    div-float v6, v24, v2

    :cond_c
    :goto_e
    add-int/lit8 v2, v12, -0x14

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void

    :cond_d
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_c

    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, v25, 0xb

    rem-int/lit8 v3, v3, 0xc

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xa

    move/from16 v0, v26

    if-ge v0, v2, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0xc

    move/from16 v0, v25

    if-ge v0, v2, :cond_10

    const-string v2, " AM"

    :goto_10
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto :goto_d

    :cond_f
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_f

    :cond_10
    const-string v2, " PM"

    goto :goto_10

    :cond_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v24, v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float/2addr v2, v3

    cmpl-float v2, v6, v2

    if-lez v2, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v24, v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000

    sub-float v6, v2, v3

    goto :goto_e

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v8, p4, p2

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    sub-int v8, p5, p3

    add-int/lit8 v8, v8, -0x1e

    add-int/lit8 v8, v8, -0x32

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->height:I

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v2, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v3, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v0, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    array-length v8, v8

    new-array v1, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v6, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v7, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v4, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    array-length v8, v8

    new-array v5, v8, [F

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitationData:[F

    invoke-direct {p0, v8}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->formatPrecipitation([F)[F

    move-result-object v8

    invoke-direct {p0, v8, v2, v3}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initPoints([F[F[F)V

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initCPoints([F[F[F[F)V

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getTopCPoint([F[F[F)F

    move-result v8

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topPrecipitation:F

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getCurve([F[F[F[F)Landroid/graphics/Path;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->precipitation:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperatureData:[F

    invoke-direct {p0, v8}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->formatTemperature([F)[F

    move-result-object v8

    invoke-direct {p0, v8, v6, v7}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initPoints([F[F[F)V

    invoke-static {v6, v7, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->initCPoints([F[F[F[F)V

    invoke-direct {p0, v6, v7, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getTopCPoint([F[F[F)F

    move-result v8

    iput v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->topTemp:F

    invoke-direct {p0, v6, v7, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getCurve([F[F[F[F)Landroid/graphics/Path;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->temperature:Landroid/graphics/Path;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->start(Landroid/view/MotionEvent;)V

    iput-boolean v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    :goto_0
    iget-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->endTime:J

    iget-wide v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    sub-long/2addr v4, v6

    long-to-float v2, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->width:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v2, v4

    float-to-long v4, v2

    iget-wide v6, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->startTime:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->listener:Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;

    iget-wide v4, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    invoke-interface {v2, v4, v5}, Lcom/google/android/apps/genie/geniewidget/view/CurveView$ForecastTimeChangedListener;->timeChanged(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->postInvalidate()V

    return v3

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->addMotionEvent(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->motionDetector:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;

    invoke-virtual {v2}, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector;->detect()Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->UNKNOWN:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-eq v1, v2, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->knewMotionIntent:Z

    sget-object v2, Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;->VIRTICAL_SCROLL:Lcom/google/android/apps/genie/geniewidget/ui/MotionDetector$MotionIntent;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1
.end method

.method public setCurrentTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->currentTime:J

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/view/CurveView;->postInvalidate()V

    return-void
.end method
