.class public Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;
.super Landroid/app/Activity;
.source "NewsWidgetConfigurationActivity.java"


# instance fields
.field private genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

.field private widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

.field private widgetId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->submit(I)V

    return-void
.end method

.method private createListEntry(ILjava/lang/String;)Ljava/util/HashMap;
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "icon"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "label"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private submit(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq p1, v5, :cond_0

    if-ne p1, v6, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    if-eqz p1, :cond_1

    if-ne p1, v6, :cond_2

    :cond_1
    move v4, v5

    :cond_2
    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignore_unconfigured_widget_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->updatePrefs(ZZ)V

    iget-object v5, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetManager()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetManager;->requestRedraw()V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v5, "appWidgetId"

    iget v6, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v5, -0x1

    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    return-void

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method private updatePrefs(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    invoke-virtual {v2, v3, p1, p2}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->setPrefs(IZZ)V

    const v2, 0x7f0c000a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    invoke-virtual {v3}, Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;->serialize()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const v1, 0x7f020019

    const v3, 0x7f0c001e

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->createListEntry(ILjava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f020017

    const v3, 0x7f0c001d

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->createListEntry(ILjava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f020018

    const v3, 0x7f0c001f

    invoke-virtual {p0, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->createListEntry(ILjava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->genie:Lcom/google/android/apps/genie/geniewidget/GenieApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getMiniWidgetController()Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController;->getWidgetConfig()Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetConfig:Lcom/google/android/apps/genie/geniewidget/utils/WidgetPrefsParser;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    if-eqz v9, :cond_0

    const-string v1, "appWidgetId"

    const/4 v3, 0x0

    invoke-virtual {v9, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    :cond_0
    iget v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    if-nez v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    const-string v1, "appWidgetId"

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    invoke-virtual {v13, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v13}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    new-instance v11, Landroid/widget/ListView;

    invoke-direct {v11, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f040003

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "icon"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "label"

    aput-object v5, v4, v1

    const/4 v1, 0x2

    new-array v5, v1, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v1, v3, :cond_2

    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    :goto_1
    const v1, 0x7f0c0010

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity$1;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    invoke-static {}, Lcom/google/android/apps/genie/geniewidget/GenieApplication;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ignore_unconfigured_widget_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;->widgetId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v8, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v1, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity$2;-><init>(Lcom/google/android/apps/genie/geniewidget/activities/NewsWidgetConfigurationActivity;)V

    invoke-virtual {v6, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    :cond_2
    new-instance v7, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x3

    invoke-direct {v7, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    goto :goto_1

    :array_0
    .array-data 4
        0x7f0b0009
        0x7f0b000a
    .end array-data
.end method
