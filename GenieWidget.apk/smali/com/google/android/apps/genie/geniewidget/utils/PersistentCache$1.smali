.class Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;
.super Ljava/util/LinkedHashMap;
.source "PersistentCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;-><init>(ILjava/lang/String;Landroid/content/Context;Lcom/google/android/apps/genie/geniewidget/utils/Clock;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;",
        "Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$Entry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;IFZ)V
    .locals 0
    .param p2    # I
    .param p3    # F
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .param p1    # Ljava/util/Map$Entry;

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache$1;->this$0:Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;

    # getter for: Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->maxSize:I
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;->access$000(Lcom/google/android/apps/genie/geniewidget/utils/PersistentCache;)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
