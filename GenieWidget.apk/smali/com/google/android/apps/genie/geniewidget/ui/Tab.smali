.class public Lcom/google/android/apps/genie/geniewidget/ui/Tab;
.super Landroid/widget/TextView;
.source "Tab.java"


# instance fields
.field private extra:Ljava/lang/String;

.field private index:I

.field private isFirst:Z

.field private isLast:Z

.field private leftX:I

.field private final quickAccessor:Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private rightX:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->quickAccessor:Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->quickAccessor:Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    invoke-direct {v0}, Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->quickAccessor:Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    return-void
.end method


# virtual methods
.method public calculatePosition(IZZ)I
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->leftX:I

    invoke-virtual {p0}, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->rightX:I

    iput-boolean p2, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isFirst:Z

    iput-boolean p3, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isLast:Z

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->rightX:I

    return v0
.end method

.method public getExtra()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->extra:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->index:I

    return v0
.end method

.method public getLeftPos()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->leftX:I

    return v0
.end method

.method public getQuickAccessor()Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/view/ViewBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->quickAccessor:Lcom/google/android/apps/genie/geniewidget/ui/QuickAccessor;

    return-object v0
.end method

.method public getRightPos()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->rightX:I

    return v0
.end method

.method public isFirst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isFirst:Z

    return v0
.end method

.method public isLast()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->isLast:Z

    return v0
.end method

.method public setExtra(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->extra:Ljava/lang/String;

    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->index:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tab: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/ui/Tab;->extra:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
