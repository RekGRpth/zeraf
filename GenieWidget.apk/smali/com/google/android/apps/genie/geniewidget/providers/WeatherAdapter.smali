.class public Lcom/google/android/apps/genie/geniewidget/providers/WeatherAdapter;
.super Ljava/lang/Object;
.source "WeatherAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forecastToContentValues(Lcom/google/android/apps/genie/geniewidget/GenieContext;Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;JLjava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 6
    .param p0    # Lcom/google/android/apps/genie/geniewidget/GenieContext;
    .param p1    # Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "begins"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "ends"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getEndTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "description"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "chancePrecipitation"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getProbPrecipitation()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "wind"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getWind()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "humidity"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHumidity()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "fakeLocation"

    invoke-virtual {v2, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "location"

    invoke-virtual {v2, v3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasTemperature()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "temperature"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getTemperature()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasHighTemp()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "highTemperature"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getHighTemp()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->hasLowTemp()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "lowTemperature"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getLowTemp()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getPointInTime()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v3, "pointInTime"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getPointInTime()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunrise()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string v3, "sunrise"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunrise()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunset()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v3, "sunset"

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getSunset()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/genie/geniewidget/items/WeatherForecastItem$WeatherCondition;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;->XL:Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;

    invoke-interface {p0, v1, v3}, Lcom/google/android/apps/genie/geniewidget/GenieContext;->getWeatherIconResource(Ljava/lang/String;Lcom/google/android/apps/genie/geniewidget/miniwidget/MiniWidgetController$IconSize;)I

    move-result v0

    const-string v3, "iconUrl"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "iconResId"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_6
    return-object v2
.end method
