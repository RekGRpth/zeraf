.class public abstract Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# instance fields
.field protected mActivity:Landroid/app/Activity;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public static createInstance(Landroid/app/Activity;)Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelper;
    .locals 2
    .param p0    # Landroid/app/Activity;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperICS;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperICS;-><init>(Landroid/app/Activity;)V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperHoneycomb;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperHoneycomb;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;

    invoke-direct {v0, p0}, Lcom/google/android/apps/genie/geniewidget/activities/actionbar/ActionBarHelperBase;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 0
    .param p1    # Landroid/view/MenuInflater;

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x1

    return v0
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method
