.class public Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;
.super Ljava/lang/Object;
.source "ParamParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Element"
.end annotation


# instance fields
.field private params:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->this$0:Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->params:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getParam(I)Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->params:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Param;

    return-object v0
.end method

.method public getParamCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/persistance/ParamParser$Element;->params:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
