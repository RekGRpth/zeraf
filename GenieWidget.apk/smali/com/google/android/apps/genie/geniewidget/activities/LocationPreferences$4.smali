.class Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;
.super Ljava/lang/Object;
.source "LocationPreferences.java"

# interfaces
.implements Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;

    instance-of v0, p1, Lcom/google/android/apps/genie/geniewidget/GenieException;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$300(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationFailed(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$400(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->networkFailure()V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 0

    return-void
.end method

.method public progress(Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;I)V
    .locals 0
    .param p1    # Lcom/google/android/apps/genie/geniewidget/miniwidget/ProgressObserver$Task;
    .param p2    # I

    return-void
.end method

.method public success()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    iget-object v1, p0, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences$4;->this$0:Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;

    # getter for: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->requestLocation:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$300(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->customLocationSuccess(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;->access$500(Lcom/google/android/apps/genie/geniewidget/activities/LocationPreferences;Ljava/lang/String;)V

    return-void
.end method
