.class public Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;
.super Ljava/lang/Object;
.source "GenieLogRequest.java"


# instance fields
.field private final clickType:I

.field private final eventId:[B

.field private final idx:I


# direct methods
.method public constructor <init>([BII)V
    .locals 0
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->eventId:[B

    iput p2, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->idx:I

    iput p3, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->clickType:I

    return-void
.end method


# virtual methods
.method public getClickType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->clickType:I

    return v0
.end method

.method public getEventId()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->eventId:[B

    return-object v0
.end method

.method public getIdx()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/genie/geniewidget/network/GenieLogRequest;->idx:I

    return v0
.end method

.method public getSearchAction()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
