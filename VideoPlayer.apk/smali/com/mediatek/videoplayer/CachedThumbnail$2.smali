.class Lcom/mediatek/videoplayer/CachedThumbnail$2;
.super Landroid/os/Handler;
.source "CachedThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/videoplayer/CachedThumbnail;->initTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/videoplayer/CachedThumbnail;


# direct methods
.method constructor <init>(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    iget v7, p1, Landroid/os/Message;->what:I

    if-ne v7, v11, :cond_0

    invoke-static {}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$300()Ljava/util/PriorityQueue;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    iget-object v9, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$300()Ljava/util/PriorityQueue;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    invoke-static {v9, v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$402(Lcom/mediatek/videoplayer/CachedThumbnail;Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$400(Lcom/mediatek/videoplayer/CachedThumbnail;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    move-result-object v7

    if-nez v7, :cond_1

    const-string v7, "CachedThumbnail"

    const-string v8, "wrong request, has request but no task params."

    invoke-static {v7, v8}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_1
    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$400(Lcom/mediatek/videoplayer/CachedThumbnail;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    move-result-object v5

    iget-wide v3, v5, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    const/4 v1, 0x0

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$500(Lcom/mediatek/videoplayer/CachedThumbnail;)Ljava/util/HashMap;

    move-result-object v8

    monitor-enter v8

    :try_start_2
    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$500(Lcom/mediatek/videoplayer/CachedThumbnail;)Ljava/util/HashMap;

    move-result-object v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    move-object v1, v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez v1, :cond_2

    const-string v7, "CachedThumbnail"

    const-string v8, "cached drawable was delete. may for clear."

    invoke-static {v7, v8}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_1
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v7

    :cond_2
    invoke-static {v1}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$100(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)I

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7, v3, v4}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$600(Lcom/mediatek/videoplayer/CachedThumbnail;J)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$700(Lcom/mediatek/videoplayer/CachedThumbnail;)I

    move-result v7

    iget-object v8, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v8}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$800(Lcom/mediatek/videoplayer/CachedThumbnail;)I

    move-result v8

    invoke-static {v6, v7, v8, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-static {v1}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$900(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7, v6}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$1000(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    :cond_3
    invoke-virtual {v1, v6, v11}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->set(Landroid/graphics/Bitmap;I)V

    :cond_4
    :goto_1
    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$400(Lcom/mediatek/videoplayer/CachedThumbnail;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    move-result-object v7

    if-eq v5, v7, :cond_6

    const-string v7, "CachedThumbnail"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "current request was changed by other thread. task="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", currentRequest="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v9}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$400(Lcom/mediatek/videoplayer/CachedThumbnail;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/videoplayer/MtkLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1, v12, v10}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->set(Landroid/graphics/Bitmap;I)V

    goto :goto_1

    :cond_6
    iput-object v1, v5, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v7}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$1100(Lcom/mediatek/videoplayer/CachedThumbnail;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iput-object v5, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    invoke-static {}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$300()Ljava/util/PriorityQueue;

    move-result-object v8

    monitor-enter v8

    :try_start_4
    iget-object v7, p0, Lcom/mediatek/videoplayer/CachedThumbnail$2;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$402(Lcom/mediatek/videoplayer/CachedThumbnail;Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    monitor-exit v8

    goto/16 :goto_0

    :catchall_2
    move-exception v7

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v7
.end method
