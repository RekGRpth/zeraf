.class public Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;
.super Ljava/lang/Object;
.source "CachedThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videoplayer/CachedThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaskParams"
.end annotation


# instance fields
.field mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

.field mPriority:J

.field mRowId:J


# direct methods
.method public constructor <init>(JJLcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    iput-wide p3, p0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    iput-object p5, p0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    return-void
.end method

.method static getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams$1;

    invoke-direct {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskInput(rowId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", drawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
