.class public Lcom/mediatek/videoplayer/CachedThumbnail;
.super Ljava/lang/Object;
.source "CachedThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;,
        Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;,
        Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;
    }
.end annotation


# static fields
.field private static final LOG:Z = false

.field private static final TAG:Ljava/lang/String; = "CachedThumbnail"

.field private static final TASK_GROUP_ID:I = 0x7cf

.field private static final TASK_QUEUE:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;",
            ">;"
        }
    .end annotation
.end field

.field private static final TASK_REQUEST_DONE:I = 0x1

.field private static final TASK_REQUEST_NEW:I = 0x2

.field public static final TYPE_LOADED_HAS_PREVIEW:I = 0x2

.field public static final TYPE_LOADED_NO_PREVIEW:I = 0x1

.field public static final TYPE_NEED_LOAD:I

.field private static sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

.field private static sLooper:Landroid/os/Looper;


# instance fields
.field private final mCachedPreview:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mCr:Landroid/content/ContentResolver;

.field private mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

.field private final mDefaultDrawable:Landroid/graphics/Bitmap;

.field private mDefaultDrawable3D:Landroid/graphics/Bitmap;

.field private final mDefaultIconHeight:I

.field private final mDefaultIconWidth:I

.field private final mDefaultOverlay3D:Landroid/graphics/Bitmap;

.field private mInitedTask:Z

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPrioritySeed:J

.field private mTaskHandler:Landroid/os/Handler;

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xa

    invoke-static {}, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->getComparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mListeners:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/videoplayer/CachedThumbnail$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videoplayer/CachedThumbnail$1;-><init>(Lcom/mediatek/videoplayer/CachedThumbnail;)V

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mUiHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCr:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable:Landroid/graphics/Bitmap;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultIconWidth:I

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultIconHeight:I

    iput-object p3, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/videoplayer/CachedThumbnail;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/mediatek/videoplayer/CachedThumbnail;->overlay3DImpl(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/videoplayer/CachedThumbnail;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/PriorityQueue;
    .locals 1

    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/videoplayer/CachedThumbnail;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/videoplayer/CachedThumbnail;Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;
    .param p1    # Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/videoplayer/CachedThumbnail;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/videoplayer/CachedThumbnail;J)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/mediatek/videoplayer/CachedThumbnail;->getThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/videoplayer/CachedThumbnail;)I
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultIconWidth:I

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/videoplayer/CachedThumbnail;)I
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail;

    iget v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultIconHeight:I

    return v0
.end method

.method private cancelThumbnail()V
    .locals 5

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCr:Landroid/content/ContentResolver;

    const-wide/16 v1, -0x1

    const-wide/16 v3, 0x7cf

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MediaStore$Video$Thumbnails;->cancelThumbnailRequest(Landroid/content/ContentResolver;JJ)V

    return-void
.end method

.method private clearTask()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mInitedTask:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    sget-object v1, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mTaskHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mTaskHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mInitedTask:Z

    invoke-direct {p0}, Lcom/mediatek/videoplayer/CachedThumbnail;->cancelThumbnail()V

    :cond_0
    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    sput-object v2, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private createNewRequest(JJZ)Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;
    .locals 10
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    monitor-enter v9

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    move-object v8, v0

    if-nez v8, :cond_1

    new-instance v1, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    invoke-direct {p0, p5}, Lcom/mediatek/videoplayer/CachedThumbnail;->getDefaultBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x0

    move-object v2, p0

    move-wide v5, p3

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;-><init>(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/graphics/Bitmap;IJZ)V

    iget-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v8, v1

    :cond_0
    :goto_0
    monitor-exit v9

    return-object v8

    :cond_1
    invoke-static {v8}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$1200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)J

    move-result-wide v2

    cmp-long v2, v2, p3

    if-eqz v2, :cond_0

    invoke-static {v8, p3, p4}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$1202(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;J)J

    const/4 v2, 0x0

    invoke-static {v8, v2}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$102(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;I)I

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getCachedManager(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/videoplayer/CachedThumbnail;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Bitmap;

    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-direct {v0, p0, p1, p2}, Lcom/mediatek/videoplayer/CachedThumbnail;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    sput-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    :cond_0
    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    return-object v0
.end method

.method private getDefaultBitmap(Z)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/mediatek/videoplayer/MtkUtils;->isSupport3d()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/mediatek/videoplayer/CachedThumbnail;->overlay3DImpl(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private getOldRequest(J)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;
    .locals 5
    .param p1    # J

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-wide v3, v2, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_0

    move-object v1, v2

    :cond_1
    return-object v1
.end method

.method private getThumbnail(J)Landroid/graphics/Bitmap;
    .locals 9
    .param p1    # J

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCr:Landroid/content/ContentResolver;

    const-wide/16 v3, 0x7cf

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-wide v1, p1

    invoke-static/range {v0 .. v6}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JJILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    move-object v8, v7

    if-eqz v7, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-object v8
.end method

.method private initTask()V
    .locals 4

    iget-boolean v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mInitedTask:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    const-class v2, Lcom/mediatek/videoplayer/CachedThumbnail;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    if-nez v1, :cond_1

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "cached-thumbnail-thread"

    const/16 v3, 0xa

    invoke-direct {v0, v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    sput-object v1, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Lcom/mediatek/videoplayer/CachedThumbnail$2;

    sget-object v2, Lcom/mediatek/videoplayer/CachedThumbnail;->sLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/videoplayer/CachedThumbnail$2;-><init>(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mTaskHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mInitedTask:Z

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private isProcessing(JJ)Z
    .locals 5
    .param p1    # J
    .param p3    # J

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-wide v3, v1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    cmp-long v1, v3, p1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCurrentRequest:Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-object v1, v1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    invoke-static {v1}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$1200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)J

    move-result-wide v3

    cmp-long v1, v3, p3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit v2

    :cond_1
    return v0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private overlay3DImpl(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v6, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v6, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int v5, v6, v3

    new-instance v1, Landroid/graphics/Rect;

    const/4 v6, 0x0

    add-int/lit8 v7, v4, 0x0

    add-int v8, v5, v3

    invoke-direct {v1, v6, v5, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v6, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultOverlay3D:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v6, v9, v1, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-object p1
.end method

.method public static releaseCachedManager()V
    .locals 1

    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-virtual {v0}, Lcom/mediatek/videoplayer/CachedThumbnail;->clearCachedPreview()V

    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/videoplayer/CachedThumbnail;->sCachedManager:Lcom/mediatek/videoplayer/CachedThumbnail;

    return-void
.end method


# virtual methods
.method public addListener(Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;)Z
    .locals 1
    .param p1    # Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public clearCachedPreview()V
    .locals 7

    invoke-direct {p0}, Lcom/mediatek/videoplayer/CachedThumbnail;->clearTask()V

    iget-object v4, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    iget-object v4, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable:Landroid/graphics/Bitmap;

    if-eq v4, v6, :cond_0

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v6, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mDefaultDrawable3D:Landroid/graphics/Bitmap;

    if-eq v4, v6, :cond_0

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public getCachedPreview(JJZZ)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # J
    .param p3    # J
    .param p5    # Z
    .param p6    # Z

    invoke-direct {p0}, Lcom/mediatek/videoplayer/CachedThumbnail;->initTask()V

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mCachedPreview:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    move-object v6, v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p6, :cond_3

    if-eqz v6, :cond_0

    invoke-static {v6}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$1200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)J

    move-result-wide v2

    cmp-long v2, v2, p3

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-static {v6, v2}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$102(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;I)I

    :cond_0
    if-eqz v6, :cond_1

    invoke-static {v6}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$100(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    iget-wide v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    sget-object v9, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    monitor-enter v9

    :try_start_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/videoplayer/CachedThumbnail;->isProcessing(JJ)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/mediatek/videoplayer/CachedThumbnail;->getOldRequest(J)Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    move-result-object v7

    if-nez v7, :cond_5

    invoke-direct/range {p0 .. p5}, Lcom/mediatek/videoplayer/CachedThumbnail;->createNewRequest(JJZ)Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$100(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    new-instance v1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-wide v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    neg-long v4, v2

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;-><init>(JJLcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)V

    sget-object v2, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mTaskHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    :goto_0
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    const/4 v8, 0x0

    if-eqz v6, :cond_4

    invoke-static {v6}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_6

    :cond_4
    invoke-direct {p0, p5}, Lcom/mediatek/videoplayer/CachedThumbnail;->getDefaultBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v8

    :goto_1
    return-object v8

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_5
    :try_start_3
    iget-wide v2, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mPrioritySeed:J

    neg-long v2, v2

    iput-wide v2, v7, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    iget-object v2, v7, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    invoke-static {v2, p3, p4}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$1202(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;J)J

    sget-object v2, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v7}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/mediatek/videoplayer/CachedThumbnail;->TASK_QUEUE:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v7}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_6
    invoke-static {v6}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v8

    goto :goto_1
.end method

.method public removeListener(Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;)Z
    .locals 1
    .param p1    # Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
