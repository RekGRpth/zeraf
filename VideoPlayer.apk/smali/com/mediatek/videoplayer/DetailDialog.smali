.class public Lcom/mediatek/videoplayer/DetailDialog;
.super Landroid/app/AlertDialog;
.source "DetailDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final BTN_OK:I = -0x1

.field private static final LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "DetailDialog"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDurationView:Landroid/widget/TextView;

.field private mFileSizeView:Landroid/widget/TextView;

.field private final mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

.field private mPathView:Landroid/widget/TextView;

.field private mTimeView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    const-string v0, "DetailDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LimitDialog() holder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/videoplayer/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const v0, 0x7f040009

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f030000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mTimeView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mDurationView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mPathView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mView:Landroid/view/View;

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mFileSizeView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040004

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v4, v4, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mTitle:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mPathView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040006

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v4, v4, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mData:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mDurationView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040008

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v4, v4, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDuration:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/mediatek/videoplayer/MtkUtils;->stringForTime(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mTimeView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040005

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v4, v4, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDateTaken:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/mediatek/videoplayer/MtkUtils;->localTime(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/videoplayer/DetailDialog;->mFileSizeView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040007

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/mediatek/videoplayer/DetailDialog;->mHolder:Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v5, v5, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mFileSize:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/mediatek/videoplayer/DetailDialog;->mContext:Landroid/content/Context;

    const v2, 0x104000a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method
