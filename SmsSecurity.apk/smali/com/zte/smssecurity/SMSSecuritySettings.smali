.class public Lcom/zte/smssecurity/SMSSecuritySettings;
.super Landroid/preference/PreferenceActivity;
.source "SMSSecuritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final EDITNUMBER:I = 0x1

.field private static final KEY_BOOTTIME:Ljava/lang/String; = "KEY_BOOTTIME"

.field private static final KEY_CALLTIME:Ljava/lang/String; = "KEY_CALLTIME"

.field private static final KEY_CONTENT_PRF:Ljava/lang/String; = "KEY_CONTENT_PRF"

.field private static final KEY_IMEI_NUMBER:Ljava/lang/String; = "KEY_IMEI_NUMBER"

.field private static final KEY_MAINBROAD_NUMBER:Ljava/lang/String; = "KEY_MAINBROAD_NUMBER"

.field private static final KEY_ONCECALLTIME:Ljava/lang/String; = "KEY_ONCECALLTIME"

.field private static final KEY_PREFIX01:Ljava/lang/String; = "KEY_PREFIX01"

.field private static final KEY_PREFIX02:Ljava/lang/String; = "KEY_PREFIX02"

.field private static final KEY_PREFIX03:Ljava/lang/String; = "KEY_PREFIX03"

.field private static final KEY_SERVICENUMBER_EDITOR:Ljava/lang/String; = "KEY_SERVICENUMBER_EDITOR"

.field private static final KEY_SERVICENUMBER_PRF:Ljava/lang/String; = "KEY_SERVICENUMBER_PRF"

.field private static final KEY_SERVICESTATE_CHECKBOX:Ljava/lang/String; = "KEY_SERVICESTATE_CHECKBOX"

.field private static final KEY_SERVICESTATE_PRF:Ljava/lang/String; = "KEY_SERVICESTATE_PRF"

.field private static final KEY_SMSSECURITYSETTING:Ljava/lang/String; = "KEY_SMSSECURITYSETTING"

.field private static final KEY_SOFTWARE_VERSION:Ljava/lang/String; = "KEY_SOFTWARE_VERSION"

.field private static final KEY_TIMESETTINGS_PRF:Ljava/lang/String; = "KEY_TIMESETTINGS_PRF"

.field private static final OPTION:Ljava/lang/String; = "option"

.field private static final SERVICESTATE:I = 0x2

.field private static final START_ACTION:Ljava/lang/String; = "com.zte.smssecurity.action.startservice"

.field private static final STOP_ACTION:Ljava/lang/String; = "com.zte.smssecurity.action.stopservice"

.field private static final TAG:Ljava/lang/String; = "SMSSecuritySettings"


# instance fields
.field private IMEI_NumberP:Landroid/preference/Preference;

.field private bootTimeList:Landroid/preference/ListPreference;

.field private callTimeList:Landroid/preference/ListPreference;

.field private dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

.field private mainbroadNumberP:Landroid/preference/Preference;

.field private onceCallTimeList:Landroid/preference/ListPreference;

.field private prefixList01:Landroid/preference/ListPreference;

.field private prefixList02:Landroid/preference/ListPreference;

.field private prefixList03:Landroid/preference/ListPreference;

.field private serviceNumberEditor:Landroid/preference/EditTextPreference;

.field private softwareVersionP:Landroid/preference/Preference;

.field private startServiceCheckBox:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->IMEI_NumberP:Landroid/preference/Preference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->mainbroadNumberP:Landroid/preference/Preference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->softwareVersionP:Landroid/preference/Preference;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/smssecurity/SMSSecuritySettings;)Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecuritySettings;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zte/smssecurity/SMSSecuritySettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecuritySettings;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private timeFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-wide/16 v10, 0xa

    const-wide/16 v8, 0x3c

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v5, "ms"

    if-ne v5, p2, :cond_0

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    :cond_0
    const-wide/16 v5, 0xe10

    div-long v5, v3, v5

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    div-long v1, v3, v8

    rem-long v5, v1, v8

    cmp-long v5, v5, v10

    if-gez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    rem-long v6, v1, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    rem-long v5, v3, v8

    cmp-long v5, v5, v10

    if-gez v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    rem-long v6, v3, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_1
    rem-long v5, v1, v8

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    :cond_2
    rem-long v5, v3, v8

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 24
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v3, 0x7f030000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    const-string v3, "KEY_SERVICESTATE_CHECKBOX"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_BOOTTIME"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_CALLTIME"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_ONCECALLTIME"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_PREFIX01"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_PREFIX02"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_PREFIX03"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_SERVICENUMBER_EDITOR"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/EditTextPreference;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v3, "KEY_IMEI_NUMBER"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->IMEI_NumberP:Landroid/preference/Preference;

    const-string v3, "KEY_MAINBROAD_NUMBER"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->mainbroadNumberP:Landroid/preference/Preference;

    const-string v3, "KEY_SOFTWARE_VERSION"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->softwareVersionP:Landroid/preference/Preference;

    const-string v3, "KEY_SMSSECURITYSETTING"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v20

    check-cast v20, Landroid/preference/PreferenceGroup;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "option"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    new-instance v3, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    const-string v4, "SMSSecurityDatabase"

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "SMSSecurityDatabase"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "TYPE"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "VALUE"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    const/16 v23, 0x0

    :goto_1
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v3, "SERVICE_NUMBER"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_2
    const-string v3, "SMSSecuritySettings"

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_0
    const-string v3, "KEY_SERVICESTATE_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string v3, "KEY_TIMESETTINGS_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string v3, "KEY_CONTENT_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSelectable(Z)V

    goto/16 :goto_0

    :pswitch_1
    const-string v3, "KEY_SERVICENUMBER_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string v3, "KEY_TIMESETTINGS_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string v3, "KEY_CONTENT_PRF"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_1
    const-string v3, "BOOTTIME"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ms"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/zte/smssecurity/SMSSecuritySettings;->timeFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_2
    const-string v3, "CALLTIME"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "s"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/zte/smssecurity/SMSSecuritySettings;->timeFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_3
    const-string v3, "ONCECALLTIME"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "s"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/zte/smssecurity/SMSSecuritySettings;->timeFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_4
    const-string v3, "PREFIX01"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_5
    const-string v3, "PREFIX02"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    const-string v3, "PREFIX03"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    const-string v3, "SERVICE_STATE"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const-string v3, "1"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto/16 :goto_2

    :cond_8
    const-string v3, "0"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto/16 :goto_2

    :cond_9
    invoke-virtual/range {p0 .. p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    array-length v0, v3

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v13, v0, [Ljava/lang/String;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v12

    array-length v0, v12

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_3
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_a

    aget-object v14, v12, v17

    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f050029

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v13, v16

    add-int/lit8 v16, v16, 0x1

    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3, v13}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    array-length v0, v3

    move/from16 v19, v0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v12

    array-length v0, v12

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_4
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_b

    aget-object v14, v12, v17

    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f05002a

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v13, v16

    add-int/lit8 v16, v16, 0x1

    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3, v13}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    array-length v0, v3

    move/from16 v19, v0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v12

    array-length v0, v12

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_5
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_c

    aget-object v14, v12, v17

    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f05002a

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v3, v13, v16

    add-int/lit8 v16, v16, 0x1

    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v3, v13}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteClosable;->close()V

    const/4 v10, 0x0

    const-string v3, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->IMEI_NumberP:Landroid/preference/Preference;

    invoke-virtual {v3, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xc

    if-le v3, v4, :cond_d

    const/4 v3, 0x0

    const/16 v4, 0xc

    invoke-virtual {v11, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->mainbroadNumberP:Landroid/preference/Preference;

    invoke-virtual {v3, v11}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/smssecurity/SMSSecuritySettings;->softwareVersionP:Landroid/preference/Preference;

    sget-object v4, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 13
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->startServiceCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object v8, p2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f050026

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v10, 0x7f050025

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v10, 0x7f050028

    new-instance v11, Lcom/zte/smssecurity/SMSSecuritySettings$1;

    invoke-direct {v11, p0, v8}, Lcom/zte/smssecurity/SMSSecuritySettings$1;-><init>(Lcom/zte/smssecurity/SMSSecuritySettings;Ljava/lang/Object;)V

    invoke-virtual {v1, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v10, 0x7f050027

    new-instance v11, Lcom/zte/smssecurity/SMSSecuritySettings$2;

    invoke-direct {v11, p0, v8}, Lcom/zte/smssecurity/SMSSecuritySettings$2;-><init>(Lcom/zte/smssecurity/SMSSecuritySettings;Ljava/lang/Object;)V

    invoke-virtual {v1, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    const/4 v10, 0x0

    :goto_1
    return v10

    :cond_1
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v11

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x1

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v7, v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_5

    aget-char v2, v0, v4

    const/16 v10, 0x39

    if-gt v2, v10, :cond_3

    const/16 v10, 0x30

    if-ge v2, v10, :cond_4

    :cond_3
    const/4 v5, 0x0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    if-eqz v5, :cond_7

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "update smssecuritydatabase set value = \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\" where type = \"SERVICE_NUMBER\";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteClosable;->close()V

    const-string v10, "SMSSecuritySettings"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "update ServiceNumber"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->serviceNumberEditor:Landroid/preference/EditTextPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_6
    const/4 v10, 0x1

    goto/16 :goto_1

    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_8
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->bootTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v10

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_9
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->callTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v10

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_a
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->onceCallTimeList:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v10

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_b
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v11

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_c
    move-object v8, p2

    check-cast v8, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    const-string v10, "ZTE"

    invoke-virtual {p2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :cond_d
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v10, "VALUE"

    invoke-virtual {v9, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v10, "SMSSecurityDatabase"

    const-string v11, "TYPE=\"PREFIX01\""

    const/4 v12, 0x0

    invoke-virtual {v3, v10, v9, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteClosable;->close()V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList01:Landroid/preference/ListPreference;

    invoke-virtual {v10, v8}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_e
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v11

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_f
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v11, "VALUE"

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v10, "SMSSecurityDatabase"

    const-string v11, "TYPE=\"PREFIX02\""

    const/4 v12, 0x0

    invoke-virtual {v3, v10, v9, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteClosable;->close()V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList02:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_10
    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    invoke-virtual {v10}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v11

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_11
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v11, "VALUE"

    move-object v10, p2

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v10, "SMSSecurityDatabase"

    const-string v11, "TYPE=\"PREFIX03\""

    const/4 v12, 0x0

    invoke-virtual {v3, v10, v9, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteClosable;->close()V

    iget-object v10, p0, Lcom/zte/smssecurity/SMSSecuritySettings;->prefixList03:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v10, p2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_12
    const/4 v10, 0x0

    goto/16 :goto_1
.end method
