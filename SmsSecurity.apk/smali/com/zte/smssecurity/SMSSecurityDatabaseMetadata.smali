.class public Lcom/zte/smssecurity/SMSSecurityDatabaseMetadata;
.super Ljava/lang/Object;
.source "SMSSecurityDatabaseMetadata.java"


# static fields
.field public static final BOOTTIME:Ljava/lang/String; = "BOOTTIME"

.field public static final CALLTIME:Ljava/lang/String; = "CALLTIME"

.field public static final LONGTIMEBOOT:Ljava/lang/String; = "LONGTIMEBOOT"

.field public static final ONCECALLTIME:Ljava/lang/String; = "ONCECALLTIME"

.field public static final PREFIX01:Ljava/lang/String; = "PREFIX01"

.field public static final PREFIX02:Ljava/lang/String; = "PREFIX02"

.field public static final PREFIX03:Ljava/lang/String; = "PREFIX03"

.field public static final SENDSMSNOW:Ljava/lang/String; = "SENDSMSNOW"

.field public static final SERVICE_NUMBER:Ljava/lang/String; = "SERVICE_NUMBER"

.field public static final SERVICE_STATE:Ljava/lang/String; = "SERVICE_STATE"

.field public static final SHOWREGISTERED:Ljava/lang/String; = "SHOWREGISTERED"

.field public static final TABLE_NAME:Ljava/lang/String; = "SMSSecurityDatabase"

.field public static final TYPE:Ljava/lang/String; = "TYPE"

.field public static final USER_CALLTIME:Ljava/lang/String; = "USER_CALLTIME"

.field public static final VALUE:Ljava/lang/String; = "VALUE"

.field public static final _ID:Ljava/lang/String; = "_ID"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
