.class public Lcom/zte/smssecurity/SMSSecurityService;
.super Landroid/app/Service;
.source "SMSSecurityService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/smssecurity/SMSSecurityService$SendSMS;,
        Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;
    }
.end annotation


# static fields
.field private static final BOOTTIME_TRIGGER:Ljava/lang/String; = "3"

.field private static final CALLTIME_TRIGGER:Ljava/lang/String; = "2"

.field private static final ONCECALLTIME_TRIGGER:Ljava/lang/String; = "4"

.field private static final SEND_INTENTACTION:Ljava/lang/String; = "SEND_INTENTACTION"

.field private static final TAG:Ljava/lang/String; = "SMSSecurityService"

.field private static mbInstance:Z

.field private static mbPhoneServiceState:Z

.field private static mbReceiverSucceed:Z

.field private static mbSendSucceed:Z

.field private static mbStart:Z

.field private static notToShowDialog:Z

.field private static prefix01:Ljava/lang/String;

.field private static prefix02:Ljava/lang/String;

.field private static prefix03:Ljava/lang/String;

.field private static serviceNumber:Ljava/lang/String;

.field private static triggerType:Ljava/lang/String;


# instance fields
.field private dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

.field private mPhoneServiceListener:Landroid/telephony/PhoneStateListener;

.field private receiverSMSReceiver:Landroid/content/BroadcastReceiver;

.field private sendSMSReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->triggerType:Ljava/lang/String;

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->serviceNumber:Ljava/lang/String;

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix01:Ljava/lang/String;

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix02:Ljava/lang/String;

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix03:Ljava/lang/String;

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbInstance:Z

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbStart:Z

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbSendSucceed:Z

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbReceiverSucceed:Z

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbPhoneServiceState:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->notToShowDialog:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    new-instance v0, Lcom/zte/smssecurity/SMSSecurityService$1;

    invoke-direct {v0, p0}, Lcom/zte/smssecurity/SMSSecurityService$1;-><init>(Lcom/zte/smssecurity/SMSSecurityService;)V

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->mPhoneServiceListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/zte/smssecurity/SMSSecurityService$2;

    invoke-direct {v0, p0}, Lcom/zte/smssecurity/SMSSecurityService$2;-><init>(Lcom/zte/smssecurity/SMSSecurityService;)V

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->sendSMSReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/zte/smssecurity/SMSSecurityService$3;

    invoke-direct {v0, p0}, Lcom/zte/smssecurity/SMSSecurityService$3;-><init>(Lcom/zte/smssecurity/SMSSecurityService;)V

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->receiverSMSReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$1000(Lcom/zte/smssecurity/SMSSecurityService;)V
    .locals 0
    .param p0    # Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {p0}, Lcom/zte/smssecurity/SMSSecurityService;->closedSMSSecurity()V

    return-void
.end method

.method static synthetic access$1100(Lcom/zte/smssecurity/SMSSecurityService;)Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecurityService;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zte/smssecurity/SMSSecurityService;)Landroid/telephony/PhoneStateListener;
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecurityService;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->mPhoneServiceListener:Landroid/telephony/PhoneStateListener;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->notToShowDialog:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/zte/smssecurity/SMSSecurityService;->notToShowDialog:Z

    return p0
.end method

.method static synthetic access$400(Lcom/zte/smssecurity/SMSSecurityService;)Z
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {p0}, Lcom/zte/smssecurity/SMSSecurityService;->isHome()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500()Z
    .locals 1

    sget-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbPhoneServiceState:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/zte/smssecurity/SMSSecurityService;->mbPhoneServiceState:Z

    return p0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/zte/smssecurity/SMSSecurityService;->serviceNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/zte/smssecurity/SMSSecurityService;->serviceNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/zte/smssecurity/SMSSecurityService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/zte/smssecurity/SMSSecurityService;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService;->sendSMSReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$800()Z
    .locals 1

    sget-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbSendSucceed:Z

    return v0
.end method

.method static synthetic access$900()Z
    .locals 1

    sget-boolean v0, Lcom/zte/smssecurity/SMSSecurityService;->mbReceiverSucceed:Z

    return v0
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/zte/smssecurity/SMSSecurityService;->mbReceiverSucceed:Z

    return p0
.end method

.method private closedSMSSecurity()V
    .locals 14

    const/4 v11, 0x2

    const/4 v13, 0x0

    const/4 v12, 0x1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    new-instance v1, Landroid/content/ComponentName;

    const-string v9, "com.zte.smssecurity"

    const-string v10, "com.zte.smssecurity.PhoneStateReceiver"

    invoke-direct {v1, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1, v11, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    const-string v9, "SMSSecurityService"

    const-string v10, "Set PhoneStateReceiver Disable"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/content/ComponentName;

    const-string v9, "com.zte.smssecurity"

    const-string v10, "com.zte.smssecurity.SMSSecurityReceiver"

    invoke-direct {v2, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v2, v11, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    invoke-static {}, Lcom/zte/smssecurity/Util;->writeCloseFlag()V

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "VALUE"

    const-string v10, "0"

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v9, "SMSSecurityDatabase"

    const-string v10, "TYPE=\"SERVICE_STATE\""

    const/4 v11, 0x0

    invoke-virtual {v4, v9, v8, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v9, "SMSSecurityService"

    const-string v10, "write database"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "3"

    sget-object v10, Lcom/zte/smssecurity/SMSSecurityService;->triggerType:Ljava/lang/String;

    if-eq v9, v10, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v7, Landroid/content/Intent;

    const-class v9, Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {v7, v3, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v9, "TriggerType"

    const-string v10, "3"

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v3, v13, v7, v13}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    const-string v9, "alarm"

    invoke-virtual {v3, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const-string v9, "SMSSecurityService"

    const-string v10, "Remove alarm"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sput-boolean v12, Lcom/zte/smssecurity/SMSSecurityService;->mbSendSucceed:Z

    return-void
.end method

.method private getHomes()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "android.intent.category.HOME"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private isHome()Z
    .locals 4

    const-string v2, "activity"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/zte/smssecurity/SMSSecurityService;->getHomes()Ljava/util/List;

    move-result-object v3

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 12

    const/4 v11, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x1

    sget-boolean v1, Lcom/zte/smssecurity/SMSSecurityService;->mbInstance:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void

    :cond_0
    new-instance v1, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    const-string v2, "SMSSecurityDatabase"

    invoke-direct {v1, p0, v2, v3, v10}, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SMSSecurityDatabase"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "TYPE"

    aput-object v4, v2, v11

    const-string v4, "VALUE"

    aput-object v4, v2, v10

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/4 v9, 0x0

    :cond_1
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v1, "SERVICE_NUMBER"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->serviceNumber:Ljava/lang/String;

    const-string v1, "SMSSecurityService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Service Number"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/zte/smssecurity/SMSSecurityService;->serviceNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v1, "PREFIX01"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix01:Ljava/lang/String;

    const-string v1, "SMSSecurityService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefix01"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/zte/smssecurity/SMSSecurityService;->prefix01:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/zte/smssecurity/SMSSecurityService;->prefix01:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v1, "PREFIX02"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix02:Ljava/lang/String;

    const-string v1, "SMSSecurityService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefix02"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/zte/smssecurity/SMSSecurityService;->prefix02:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    const-string v1, "PREFIX03"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->prefix03:Ljava/lang/String;

    const-string v1, "SMSSecurityService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prefix03"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/zte/smssecurity/SMSSecurityService;->prefix03:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteClosable;->close()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SMSSecurityService"

    const-string v1, "service destroy!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 14
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v1, "SMSSecurityService"

    const-string v2, "start service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/zte/smssecurity/SMSSecurityService;->mbStart:Z

    if-nez v1, :cond_8

    if-eqz p1, :cond_0

    const-string v1, "TriggerType"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "TriggerType"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/zte/smssecurity/SMSSecurityService;->triggerType:Ljava/lang/String;

    :cond_0
    sget-object v1, Lcom/zte/smssecurity/SMSSecurityService;->triggerType:Ljava/lang/String;

    if-eqz v1, :cond_9

    sget-object v1, Lcom/zte/smssecurity/SMSSecurityService;->triggerType:Ljava/lang/String;

    const-string v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "SMSSecurityService"

    const-string v2, "BOOTTIME_TRIGGER"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x1

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService;->dbh:Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SMSSecurityDatabase"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TYPE"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "VALUE"

    aput-object v4, v2, v3

    const-string v3, "TYPE=\"SENDSMSNOW\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    const/4 v10, 0x0

    :cond_1
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SENDSMSNOW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    if-ne v10, v1, :cond_3

    const-string v1, "SMSSecurityService"

    const-string v2, "intSendNow == 1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    new-instance v1, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;-><init>(Lcom/zte/smssecurity/SMSSecurityService;Lcom/zte/smssecurity/SMSSecurityService$1;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v1, 0x1

    sput-boolean v1, Lcom/zte/smssecurity/SMSSecurityService;->mbStart:Z

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    if-eqz v12, :cond_7

    const-string v1, "SMSSecurityDatabase"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TYPE"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "VALUE"

    aput-object v4, v2, v3

    const-string v3, "TYPE=\"LONGTIMEBOOT\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/4 v11, 0x0

    :cond_4
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "LONGTIMEBOOT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    :cond_5
    const/4 v1, 0x2

    if-lt v11, v1, :cond_6

    new-instance v1, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;-><init>(Lcom/zte/smssecurity/SMSSecurityService;Lcom/zte/smssecurity/SMSSecurityService$1;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_6
    add-int/lit8 v11, v11, 0x1

    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "VALUE"

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"LONGTIMEBOOT\""

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteClosable;->close()V

    :cond_8
    :goto_2
    invoke-super/range {p0 .. p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1

    :cond_9
    new-instance v1, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;-><init>(Lcom/zte/smssecurity/SMSSecurityService;Lcom/zte/smssecurity/SMSSecurityService$1;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v1, 0x1

    sput-boolean v1, Lcom/zte/smssecurity/SMSSecurityService;->mbStart:Z

    goto :goto_2
.end method
