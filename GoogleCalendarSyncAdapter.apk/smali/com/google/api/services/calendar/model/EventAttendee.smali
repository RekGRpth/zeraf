.class public final Lcom/google/api/services/calendar/model/EventAttendee;
.super Lcom/google/api/client/json/GenericJson;
.source "EventAttendee.java"


# instance fields
.field private displayName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private email:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private optional:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private organizer:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private responseStatus:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private self:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getOptional()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->optional:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getOrganizer()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->organizer:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getResponseStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->responseStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getSelf()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/model/EventAttendee;->self:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventAttendee;->displayName:Ljava/lang/String;

    return-object p0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventAttendee;->email:Ljava/lang/String;

    return-object p0
.end method

.method public setOptional(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/EventAttendee;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventAttendee;->optional:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setResponseStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/model/EventAttendee;->responseStatus:Ljava/lang/String;

    return-object p0
.end method
