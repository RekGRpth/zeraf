.class public interface abstract Lcom/google/api/client/http/BackOffPolicy;
.super Ljava/lang/Object;
.source "BackOffPolicy.java"


# virtual methods
.method public abstract getNextBackOffMillis()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract isBackOffRequired(I)Z
.end method

.method public abstract reset()V
.end method
