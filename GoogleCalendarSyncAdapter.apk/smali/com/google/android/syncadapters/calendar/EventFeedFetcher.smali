.class public Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
.super Lcom/google/android/apiary/FeedFetcher;
.source "EventFeedFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apiary/FeedFetcher",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccessRole:Ljava/lang/String;

.field private volatile mDefaultReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation
.end field

.field private final mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

.field private volatile mLastUpdated:Ljava/lang/String;

.field private mMaxAttendees:I

.field private final mMaxResults:I

.field private final mPrimary:Z

.field private final mThreadStatsTag:I

.field private volatile mTimeZone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/services/calendar/Calendar$Events$List;Ljava/util/concurrent/BlockingQueue;Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;IIZI)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/json/JsonFactory;",
            "Lcom/google/api/services/calendar/Calendar$Events$List;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Ljava/lang/String;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;",
            "IIZI)V"
        }
    .end annotation

    const-string v4, "items"

    const-class v5, Lcom/google/api/services/calendar/model/Event;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apiary/FeedFetcher;-><init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/json/JsonHttpRequest;Ljava/lang/String;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mPrimary:Z

    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    invoke-virtual {p0, p2}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;)V

    return-void
.end method

.method private setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V
    .locals 4
    .param p1    # Lcom/google/api/services/calendar/Calendar$Events$List;
    .param p2    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .param p3    # Z

    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    const-string v2, "updated"

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setOrderBy(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    const-string v2, "do_incremental_sync"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "feed_updated_time"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    invoke-static {v1}, Lcom/google/api/client/util/DateTime;->parseRfc3339(Ljava/lang/String;)Lcom/google/api/client/util/DateTime;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setUpdatedMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_0
    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    return-void
.end method


# virtual methods
.method public getAccessRole()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    return-object v0
.end method

.method public getLastUpdated()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method protected onExecute(Lcom/google/api/client/http/json/JsonHttpRequest;)V
    .locals 7
    .param p1    # Lcom/google/api/client/http/json/JsonHttpRequest;

    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Saving inProgress state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/api/client/http/json/JsonHttpRequest;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "calendarId"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    invoke-virtual {v4, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->setInProgressParams(Ljava/util/Map;)V

    return-void
.end method

.method protected parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x1

    const-string v0, "timeZone"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "updated"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v0, "defaultReminders"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    const-class v2, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseArray(Ljava/util/Collection;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "accessRole"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    invoke-super {p0}, Lcom/google/android/apiary/FeedFetcher;->run()V

    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    return-void
.end method

.method protected setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;)V
    .locals 22
    .param p1    # Lcom/google/api/services/calendar/Calendar$Events$List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getInProgressParams()Ljava/util/Map;

    move-result-object v10

    if-eqz v10, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Resuming inProgress sync: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v18, "timeMin"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "timeMax"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "updatedMin"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    :cond_0
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/api/client/util/DateTime;->parseRfc3339(Ljava/lang/String;)Lcom/google/api/client/util/DateTime;

    move-result-object v15

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, Lcom/google/api/services/calendar/Calendar$Events$List;->set(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->isGDataUpgrade()Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "upgrade_min_start"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "upgrade_max_start"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v13, v14, v1}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v11, v12, v1}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_3
    :goto_2
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "window_end"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "new_window_end"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    const-wide/16 v18, 0x0

    cmp-long v18, v7, v18

    if-nez v18, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mFeedSyncState: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", startMaxMs: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-wide/16 v18, 0x0

    cmp-long v18, v16, v18

    if-lez v18, :cond_6

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/Calendar$Events$List;->getUpdatedMin()Lcom/google/api/client/util/DateTime;

    move-result-object v18

    if-nez v18, :cond_3

    new-instance v6, Landroid/text/format/Time;

    const-string v18, "UTC"

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    iget v0, v6, Landroid/text/format/Time;->year:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    iput v0, v6, Landroid/text/format/Time;->year:I

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->normalize(Z)J

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v19

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-direct/range {v18 .. v21}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v7, v8, v1}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto/16 :goto_2
.end method
