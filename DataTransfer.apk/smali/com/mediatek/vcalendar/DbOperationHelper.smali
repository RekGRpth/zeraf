.class public Lcom/mediatek/vcalendar/DbOperationHelper;
.super Ljava/lang/Object;
.source "DbOperationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;,
        Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;
    }
.end annotation


# static fields
.field static final INSERT_MODE:I = 0x0

.field static final MULTI_ACCOUNT_QUERY_MODE:I = 0x2

.field static final QUERY_MODE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DbOperationHelper"


# instance fields
.field private final mCalendarAccountName:Ljava/lang/String;

.field private final mCalendarIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mEventsCursor:Landroid/database/Cursor;

.field private final mMode:I

.field private mQuerySelection:Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mVEventsCount:I


# direct methods
.method constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    iput p1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mMode:I

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mQuerySelection:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mMode:I

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mQuerySelection:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/vcalendar/DbOperationHelper;->initQuery(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DbOperationHelper"

    const-string v1, "Constructor : the query action failed when query events wity the given seletion"

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vcalendar/DbOperationHelper;->initCalendarIdList()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    iput-object p1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mMode:I

    iget-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-direct {p0}, Lcom/mediatek/vcalendar/DbOperationHelper;->queryCalendarId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calendar_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mQuerySelection:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mQuerySelection:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mediatek/vcalendar/DbOperationHelper;->initQuery(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "DbOperationHelper"

    const-string v3, "Constructor : the query action failed when query events wity the given seletion"

    invoke-static {v2, v3}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-void
.end method

.method private addEventIdForContentValuesList(Ljava/util/LinkedList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/AbstractSequentialList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    invoke-virtual {v1, p3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private initCalendarIdList()V
    .locals 8

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    const-string v7, "calendar_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v5, v5, v2

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method

.method private initQuery(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v7, 0x0

    const-string v0, "DbOperationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initQuery: selection = \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "calendar_id"

    move-object v3, p1

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    invoke-static {v6}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 5
    .param p0    # Landroid/database/Cursor;

    new-instance v2, Landroid/database/MatrixCursor;

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    const/4 v4, -0x1

    invoke-interface {p0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static matrixCursorFromCursorRow(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;
    .locals 6
    .param p0    # Landroid/database/Cursor;
    .param p1    # I

    new-instance v2, Landroid/database/MatrixCursor;

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v4, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    const/4 v4, -0x1

    if-gt v4, p1, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge p1, v4, :cond_0

    invoke-interface {p0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    return-object v2
.end method

.method private queryCalendarId()J
    .locals 9

    const/4 v2, 0x0

    const-wide/16 v6, -0x1

    const-string v0, "PC Sync"

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v6, 0x1

    :cond_0
    :goto_0
    return-wide v6

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account_name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v0, "DbOperationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showAccountListView() Select = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_2

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    :cond_2
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method public addNextContentValue(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;Z)Landroid/net/Uri;
    .locals 10
    .param p1    # Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;
    .param p2    # Z

    const/4 v4, 0x0

    iget v6, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mMode:I

    if-eqz v6, :cond_0

    const-string v6, "DbOperationHelper"

    const-string v7, "Write to DB method only can be called in INSERT_MODE"

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    :goto_0
    return-object v5

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    move-object v5, v4

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAlarmsList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->getAttendeesList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v1

    const-string v6, "DbOperationHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addNextContentValue,isEnd: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "DbOperationHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addNextContentValue,Alarms count: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "DbOperationHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addNextContentValue,Attendees count:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->access$000(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;)Landroid/content/ContentValues;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v3

    if-nez v3, :cond_2

    const-string v6, "DbOperationHelper"

    const-string v7, "addNextContentValue: Add event failed."

    invoke-static {v6, v7}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    const-string v6, "DbOperationHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Event inserted:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; eventId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_3

    invoke-static {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->access$100(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;)Ljava/util/LinkedList;

    move-result-object v6

    const-string v7, "event_id"

    invoke-direct {p0, v6, v2, v7}, Lcom/mediatek/vcalendar/DbOperationHelper;->addEventIdForContentValuesList(Ljava/util/LinkedList;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v8, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->access$100(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;)Ljava/util/LinkedList;

    move-result-object v6

    new-array v9, v0, [Landroid/content/ContentValues;

    invoke-virtual {v6, v9}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/ContentValues;

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    :cond_3
    if-lez v1, :cond_4

    invoke-static {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->access$200(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;)Ljava/util/LinkedList;

    move-result-object v6

    const-string v7, "event_id"

    invoke-direct {p0, v6, v2, v7}, Lcom/mediatek/vcalendar/DbOperationHelper;->addEventIdForContentValuesList(Ljava/util/LinkedList;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v8, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;->access$200(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventContentValues;)Ljava/util/LinkedList;

    move-result-object v6

    new-array v9, v1, [Landroid/content/ContentValues;

    invoke-virtual {v6, v9}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/ContentValues;

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    :cond_4
    move-object v5, v4

    goto/16 :goto_0
.end method

.method public getCalendarIdList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNextVEventInfo()Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    new-instance v10, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;

    invoke-direct {v10}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;-><init>()V

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursorRow(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->access$302(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    const-string v3, "calendar_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v10, v0, v1}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->access$402(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;J)J

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarAccountName:Ljava/lang/String;

    invoke-static {v10, v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->access$502(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;Ljava/lang/String;)Ljava/lang/String;

    const-wide/16 v8, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    const-string v3, "_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-eqz v0, :cond_0

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-nez v11, :cond_2

    const-string v0, "DbOperationHelper"

    const-string v1, "getNextVEventInfo, Get the reminder failed."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v0, "DbOperationHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException:\t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    invoke-static {v11}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->access$602(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v0, "DbOperationHelper"

    const-string v1, "getNextVEventInfo, Get the reminder failed."

    invoke-static {v0, v1}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {v6}, Lcom/mediatek/vcalendar/DbOperationHelper;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;->access$702(Lcom/mediatek/vcalendar/DbOperationHelper$SingleVEventCursorInfo;Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_4
    move-object v2, v10

    goto/16 :goto_0
.end method

.method public getVEventCount()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    return v0
.end method

.method public hasNextVEvent()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isGivenIdEventExisted(J)Z
    .locals 8
    .param p1    # J

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!=1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/vcalendar/DbOperationHelper;->initQuery(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v4, "DbOperationHelper"

    const-string v5, "isGivenIdEventExist(), query event cursor result is null"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    iput v4, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mVEventsCount:I

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mEventsCursor:Landroid/database/Cursor;

    const-string v7, "calendar_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v5, p0, Lcom/mediatek/vcalendar/DbOperationHelper;->mCalendarIdList:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v5, -0x1

    cmp-long v5, v0, v5

    if-nez v5, :cond_1

    const-string v4, "DbOperationHelper"

    const-string v5, "the Given Id Event must has the calendarId column"

    invoke-static {v4, v5}, Lcom/mediatek/vcalendar/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_0
.end method
