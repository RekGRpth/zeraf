.class public Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;
.super Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;
.source "PersonalDataBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PersonalDataBackupStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    return-void
.end method


# virtual methods
.method public onBackupEnd(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/RecordXmlInfo;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    new-instance v3, Lcom/mediatek/datatransfer/RecordXmlComposer;

    invoke-direct {v3}, Lcom/mediatek/datatransfer/RecordXmlComposer;-><init>()V

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RecordXmlComposer;->startCompose()Z

    invoke-virtual {v3, v0}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RecordXmlComposer;->endCompose()Z

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v4}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$900(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v4}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$900(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RecordXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v6}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$900(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "record.xml"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/Utils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v1, p1

    move-object v2, p2

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v4, v4, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v4, v4, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$1;

    invoke-direct {v5, p0, v1, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$1;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    if-nez p1, :cond_1

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onComposerChanged: error[composer is null]"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onComposerChanged: type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;Lcom/mediatek/datatransfer/modules/Composer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
