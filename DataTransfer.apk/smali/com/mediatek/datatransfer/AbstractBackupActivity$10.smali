.class Lcom/mediatek/datatransfer/AbstractBackupActivity$10;
.super Ljava/lang/Object;
.source "AbstractBackupActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AbstractBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    check-cast p2, Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    iput-object p2, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$200(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$200(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setOnBackupChangedListner(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->afterServiceConnected()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$000(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$000(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
