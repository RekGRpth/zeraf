.class public final Lcom/mediatek/datatransfer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activity_loading:I = 0x7f06003e

.field public static final air_mode_dialog_message:I = 0x7f060042

.field public static final air_mode_dialog_title:I = 0x7f060041

.field public static final app_module:I = 0x7f06000a

.field public static final app_name:I = 0x7f060000

.field public static final backup:I = 0x7f060001

.field public static final backup_app:I = 0x7f060029

.field public static final backup_app_data_history:I = 0x7f06002b

.field public static final backup_app_data_preference_title:I = 0x7f06002c

.field public static final backup_data:I = 0x7f060018

.field public static final backup_personal_data:I = 0x7f060028

.field public static final backup_personal_data_history:I = 0x7f06002a

.field public static final backup_result:I = 0x7f060020

.field public static final backup_running_toast:I = 0x7f060040

.field public static final backuping:I = 0x7f06000e

.field public static final backuptosd:I = 0x7f060003

.field public static final bookmark_module:I = 0x7f06000d

.field public static final bt_no:I = 0x7f060015

.field public static final bt_yes:I = 0x7f060014

.field public static final btn_ok:I = 0x7f060013

.field public static final calendar_module:I = 0x7f060009

.field public static final cancel_backup_confirm:I = 0x7f060011

.field public static final cancelling:I = 0x7f060012

.field public static final contact_all:I = 0x7f06002d

.field public static final contact_module:I = 0x7f060006

.field public static final contact_phone:I = 0x7f06002e

.field public static final contitue:I = 0x7f06001d

.field public static final create_folder_fail:I = 0x7f060036

.field public static final delete_please_wait:I = 0x7f060030

.field public static final detect_new_data_text:I = 0x7f060038

.field public static final detect_new_data_title:I = 0x7f060037

.field public static final edit_folder_name:I = 0x7f060032

.field public static final error:I = 0x7f060025

.field public static final file_no_exist_and_update:I = 0x7f060033

.field public static final less_1K:I = 0x7f060019

.field public static final loading_please_wait:I = 0x7f060031

.field public static final menu_delete:I = 0x7f060016

.field public static final message_mms:I = 0x7f06003d

.field public static final message_module:I = 0x7f060007

.field public static final message_sms:I = 0x7f06003c

.field public static final music_module:I = 0x7f06000b

.field public static final no_item_selected:I = 0x7f060034

.field public static final nosdcard_notice:I = 0x7f06001b

.field public static final notebook_module:I = 0x7f06000c

.field public static final notice:I = 0x7f06001a

.field public static final notification_backup_title:I = 0x7f060039

.field public static final notification_restore_title:I = 0x7f06003a

.field public static final picture_module:I = 0x7f060008

.field public static final restore:I = 0x7f060002

.field public static final restore_confirm_notice:I = 0x7f06001c

.field public static final restore_result:I = 0x7f060021

.field public static final restored_flag:I = 0x7f06003b

.field public static final restoring:I = 0x7f06000f

.field public static final result_fail:I = 0x7f060023

.field public static final result_no_content:I = 0x7f060024

.field public static final result_success:I = 0x7f060022

.field public static final sdcard_busy_message:I = 0x7f06003f

.field public static final sdcard_is_full:I = 0x7f06001f

.field public static final sdcard_removed:I = 0x7f06001e

.field public static final sdcard_swap_insert:I = 0x7f060027

.field public static final sdcard_swap_remove:I = 0x7f060026

.field public static final selectall:I = 0x7f060004

.field public static final selected:I = 0x7f06002f

.field public static final sim_locked_dialog_message:I = 0x7f060044

.field public static final sim_locked_dialog_title:I = 0x7f060043

.field public static final sim_locked_dialog_unlock:I = 0x7f060045

.field public static final unknown:I = 0x7f060017

.field public static final unselectall:I = 0x7f060005

.field public static final updating:I = 0x7f060035

.field public static final warning:I = 0x7f060010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
