.class public Lcom/mediatek/datatransfer/RestoreTabFragment;
.super Landroid/preference/PreferenceFragment;
.source "RestoreTabFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;,
        Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;
    }
.end annotation


# instance fields
.field private final CLASS_TAG:Ljava/lang/String;

.field private final START_ACTION_MODE_DELAY_TIME:I

.field private final STATE_CHECKED_ITEMS:Ljava/lang/String;

.field private final STATE_DELETE_MODE:Ljava/lang/String;

.field private mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

.field private mDeleteActionMode:Landroid/view/ActionMode;

.field private mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

.field private mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mListView:Landroid/widget/ListView;

.field private mLoadingDialog:Landroid/app/ProgressDialog;

.field mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->CLASS_TAG:Ljava/lang/String;

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->START_ACTION_MODE_DELAY_TIME:I

    const-string v0, "deleteMode"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->STATE_DELETE_MODE:Ljava/lang/String;

    const-string v0, "checkedItems"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->STATE_CHECKED_ITEMS:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mIsActive:Z

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/datatransfer/RestoreTabFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mDeleteActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/RestoreTabFragment;)Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/RestoreTabFragment;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mIsActive:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/RestoreTabFragment;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->startScanFiles()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/RestoreTabFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/datatransfer/RestoreTabFragment;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->showCheckBox(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/RestoreTabFragment;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;
    .param p1    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->addScanResultsAsPreferences(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/datatransfer/RestoreTabFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/datatransfer/RestoreTabFragment;Ljava/util/HashSet;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;
    .param p1    # Ljava/util/HashSet;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->startDeleteItems(Ljava/util/HashSet;)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/datatransfer/RestoreTabFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private addPreferenceCategory(Landroid/preference/PreferenceScreen;I)V
    .locals 2
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # I

    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method private addRestoreCheckBoxPreference(Landroid/preference/PreferenceScreen;Lcom/mediatek/datatransfer/utils/BackupFilePreview;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Lcom/mediatek/datatransfer/utils/BackupFilePreview;
    .param p3    # Ljava/lang/String;

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const-string v4, "DataTransfer/RestoreTabFragment"

    const-string v5, "addRestoreCheckBoxPreference: Error!"

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v3, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;-><init>(Landroid/content/Context;)V

    const-string v4, "app"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f06002c

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    :goto_1
    const-string v4, "DataTransfer/RestoreTabFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addRestoreCheckBoxPreference: type is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fileName = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const v4, 0x7f060018

    invoke-virtual {p0, v4}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFileSize()J

    move-result-wide v4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/mediatek/datatransfer/utils/FileUtils;->getDisplaySize(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->isRestored()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setRestored(Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mDeleteActionMode:Landroid/view/ActionMode;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->showCheckbox(Z)V

    :cond_2
    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setAccociateFile(Ljava/io/File;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v4, "app"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :goto_2
    const-string v4, "filename"

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p1, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_2
.end method

.method private addScanResultsAsPreferences(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceGroup;->removeAll()V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v3, p1

    check-cast v3, Ljava/util/HashMap;

    const-string v5, "personalData"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    const v5, 0x7f06002a

    invoke-direct {p0, v4, v5}, Lcom/mediatek/datatransfer/RestoreTabFragment;->addPreferenceCategory(Landroid/preference/PreferenceScreen;I)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    const-string v5, "personal data"

    invoke-direct {p0, v4, v1, v5}, Lcom/mediatek/datatransfer/RestoreTabFragment;->addRestoreCheckBoxPreference(Landroid/preference/PreferenceScreen;Lcom/mediatek/datatransfer/utils/BackupFilePreview;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v5, "appData"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    const v5, 0x7f06002b

    invoke-direct {p0, v4, v5}, Lcom/mediatek/datatransfer/RestoreTabFragment;->addPreferenceCategory(Landroid/preference/PreferenceScreen;I)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    const-string v5, "app"

    invoke-direct {p0, v4, v1, v5}, Lcom/mediatek/datatransfer/RestoreTabFragment;->addRestoreCheckBoxPreference(Landroid/preference/PreferenceScreen;Lcom/mediatek/datatransfer/utils/BackupFilePreview;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mDeleteActionMode:Landroid/view/ActionMode;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    if-eqz v5, :cond_0

    const-string v5, "DataTransfer/RestoreTabFragment"

    const-string v6, " confirmSyncCheckedPositons now!!!"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->confirmSyncCheckedPositons()V

    goto :goto_0
.end method

.method private init()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->initHandler()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->initListView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->initLoadingDialog()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->registerSDCardListener()V

    return-void
.end method

.method private initHandler()V
    .locals 1

    new-instance v0, Lcom/mediatek/datatransfer/RestoreTabFragment$4;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$4;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private initListView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/mediatek/datatransfer/RestoreTabFragment$3;

    invoke-direct {v2, p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$3;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    :cond_0
    return-void
.end method

.method private initLoadingDialog()V
    .locals 2

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f060031

    invoke-virtual {p0, v1}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    return-void
.end method

.method private registerSDCardListener()V
    .locals 2

    new-instance v1, Lcom/mediatek/datatransfer/RestoreTabFragment$2;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$2;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->registerOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    return-void
.end method

.method private showCheckBox(Z)V
    .locals 5
    .param p1    # Z

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v4, v1, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v4, :cond_0

    check-cast v1, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {v1, p1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->showCheckbox(Z)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private startDeleteItems(Ljava/util/HashSet;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_1

    invoke-virtual {v7, v5}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v6

    if-eqz v6, :cond_0

    instance-of v8, v6, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v8, :cond_0

    move-object v4, v6

    check-cast v4, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getAccociateFile()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/util/HashSet;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-virtual {v1, v8}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private startScanFiles()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    :goto_0
    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: startScanFiles"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->startScan()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->setHandler(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private unRegisteSDCardListener()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->unRegisterOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getmHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const-string v1, "DataTransfer/RestoreTabFragment"

    const-string v2, "RestoreTabFragment: onActivityCreated"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->init()V

    if-eqz p1, :cond_0

    const-string v1, "deleteMode"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/mediatek/datatransfer/RestoreTabFragment$1;

    invoke-direct {v2, p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment$1;-><init>(Lcom/mediatek/datatransfer/RestoreTabFragment;Landroid/os/Bundle;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onAttach"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mLoadingDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->setHandler(Landroid/os/Handler;)V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->unRegisteSDCardListener()V

    return-void
.end method

.method public onDetach()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onDetach"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onPasue"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->quitScan()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mIsActive:Z

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v5, 0x1

    const/4 v4, 0x0

    instance-of v6, p2, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v6, :cond_0

    move-object v3, p2

    check-cast v3, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    iget-object v6, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mDeleteActionMode:Landroid/view/ActionMode;

    if-nez v6, :cond_2

    invoke-virtual {v3}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v6, "filename"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f060033

    invoke-static {v6, v7, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->isChecked()Z

    move-result v7

    if-nez v7, :cond_3

    move v4, v5

    :cond_3
    invoke-virtual {v6, v3, v4}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->setItemChecked(Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mIsActive:Z

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->isSdCardAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->startScanFiles()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mDeleteActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    const-string v0, "deleteMode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment;->mActionModeListener:Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;

    invoke-virtual {v0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->saveState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStart()V

    const-string v0, "DataTransfer/RestoreTabFragment"

    const-string v1, "RestoreTabFragment: onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
