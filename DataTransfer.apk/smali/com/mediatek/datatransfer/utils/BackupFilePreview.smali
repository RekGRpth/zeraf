.class public Lcom/mediatek/datatransfer/utils/BackupFilePreview;
.super Ljava/lang/Object;
.source "BackupFilePreview.java"


# instance fields
.field private final CLASS_TAG:Ljava/lang/String;

.field private final UN_PARESED_TYPE:I

.field private backupTime:Ljava/lang/String;

.field private mFolderName:Ljava/io/File;

.field private mIsOtherBackup:Z

.field private mIsRestored:Z

.field private mIsSelfBackup:Z

.field private mNumberMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSize:J

.field private mTypes:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 4
    .param p1    # Ljava/io/File;

    const/4 v3, 0x1

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "DataTransfer/BackupFilePreview"

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->CLASS_TAG:Ljava/lang/String;

    iput v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->UN_PARESED_TYPE:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mSize:J

    iput v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsRestored:Z

    iput-boolean v3, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsOtherBackup:Z

    iput-boolean v3, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsSelfBackup:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mNumberMap:Ljava/util/HashMap;

    if-nez p1, :cond_0

    const-string v0, "DataTransfer/BackupFilePreview"

    const-string v1, "constractor error! file is null"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mNumberMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    const-string v0, "DataTransfer/BackupFilePreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new BackupFilePreview: file is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->computeSize()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->checkRestored()V

    goto :goto_0
.end method

.method private addCurrentSN(Ljava/util/List;Ljava/lang/String;)Z
    .locals 9
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/RecordXmlInfo;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-direct {v3}, Lcom/mediatek/datatransfer/RecordXmlInfo;-><init>()V

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/mediatek/datatransfer/RecordXmlComposer;

    invoke-direct {v6}, Lcom/mediatek/datatransfer/RecordXmlComposer;-><init>()V

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/RecordXmlComposer;->startCompose()Z

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-virtual {v6, v2}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v5, v4

    :goto_1
    return v5

    :cond_0
    :try_start_1
    invoke-virtual {v6}, Lcom/mediatek/datatransfer/RecordXmlComposer;->endCompose()Z

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/RecordXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2}, Lcom/mediatek/datatransfer/utils/Utils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v4, 0x1

    move v5, v4

    goto :goto_1
.end method

.method private addToCurrentBackupHistory(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/RecordXmlInfo;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/datatransfer/RecordXmlComposer;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;-><init>()V

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->startCompose()Z

    invoke-virtual {v1, v0}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->endCompose()Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/mediatek/datatransfer/utils/Utils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private checkRestored()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    iput-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsRestored:Z

    iput-boolean v9, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsOtherBackup:Z

    iput-boolean v9, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsSelfBackup:Z

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "record.xml"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-direct {p0, v6}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->addToCurrentBackupHistory(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v6}, Lcom/mediatek/datatransfer/utils/Utils;->readFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/datatransfer/RecordXmlParser;->parse(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v9, :cond_2

    iput-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsSelfBackup:Z

    :cond_2
    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getDevice()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iput-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsOtherBackup:Z

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RecordXmlInfo;->isRestore()Z

    move-result v7

    if-eqz v7, :cond_3

    iput-boolean v9, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsRestored:Z

    goto :goto_1

    :cond_4
    iget-boolean v7, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsOtherBackup:Z

    if-eqz v7, :cond_0

    invoke-direct {p0, v4, v6}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->addCurrentSN(Ljava/util/List;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private computeSize()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-static {v0}, Lcom/mediatek/datatransfer/utils/FileUtils;->computeAllFileSizeInFolder(Ljava/io/File;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mSize:J

    return-void
.end method

.method private formatString(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1    # Ljava/lang/String;

    const/16 v11, 0xc

    const/16 v10, 0xa

    const/16 v9, 0x8

    const/4 v8, 0x6

    const/4 v7, 0x4

    if-eqz p1, :cond_0

    const/4 v6, 0x0

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v6, 0xe

    invoke-virtual {p1, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private initNumByType(Landroid/content/Context;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    sparse-switch p2, :sswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/modules/Composer;->setParentFolderPath(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->init()Z

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v1

    const-string v2, "DataTransfer/BackupFilePreview"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initNumByType: count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mNumberMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void

    :sswitch_0
    new-instance v0, Lcom/mediatek/datatransfer/modules/ContactRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/ContactRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Lcom/mediatek/datatransfer/modules/CalendarRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/CalendarRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/mediatek/datatransfer/modules/MessageRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/MessageRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/mediatek/datatransfer/modules/MusicRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/MusicRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/mediatek/datatransfer/modules/NoteBookRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/NoteBookRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/mediatek/datatransfer/modules/PictureRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/PictureRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;

    invoke-direct {v0, p1}, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_1
        0x20 -> :sswitch_5
        0x40 -> :sswitch_2
        0x80 -> :sswitch_3
        0x100 -> :sswitch_4
        0x200 -> :sswitch_6
    .end sparse-switch
.end method

.method private peekBackupModules(Landroid/content/Context;)I
    .locals 14
    .param p1    # Landroid/content/Context;

    const/4 v13, 0x7

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    iput v12, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    if-eqz v3, :cond_2

    move-object v0, v3

    array-length v6, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v2, v0, v4

    new-array v7, v13, [Ljava/lang/String;

    const-string v10, "calendar"

    aput-object v10, v7, v12

    const/4 v10, 0x1

    const-string v11, "Contact"

    aput-object v11, v7, v10

    const/4 v10, 0x2

    const-string v11, "mms"

    aput-object v11, v7, v10

    const/4 v10, 0x3

    const-string v11, "music"

    aput-object v11, v7, v10

    const/4 v10, 0x4

    const-string v11, "picture"

    aput-object v11, v7, v10

    const/4 v10, 0x5

    const-string v11, "sms"

    aput-object v11, v7, v10

    const/4 v10, 0x6

    const-string v11, "bookmark"

    aput-object v11, v7, v10

    new-array v8, v13, [I

    fill-array-data v8, :array_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {v2}, Lcom/mediatek/datatransfer/utils/FileUtils;->isEmptyFolder(Ljava/io/File;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    array-length v1, v7

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_1

    aget-object v10, v7, v5

    invoke-virtual {v10, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    aget v11, v8, v5

    or-int/2addr v10, v11

    iput v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    aget v10, v8, v5

    invoke-direct {p0, p1, v10}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->initNumByType(Landroid/content/Context;I)V

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-string v10, "DataTransfer/BackupFilePreview"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parseItemTypes: mTypes =  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    iget v10, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    return v10

    nop

    :array_0
    .array-data 4
        0x8
        0x1
        0x40
        0x80
        0x20
        0x40
        0x200
    .end array-data
.end method


# virtual methods
.method public getBackupModules(Landroid/content/Context;)I
    .locals 2
    .param p1    # Landroid/content/Context;

    iget v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->peekBackupModules(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    :cond_0
    iget v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mTypes:I

    return v0
.end method

.method public getBackupTime()Ljava/lang/String;
    .locals 5

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->backupTime:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmmss"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->backupTime:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->backupTime:Ljava/lang/String;

    return-object v2
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 5

    iget-object v3, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mFolderName:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->formatString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    move-object v2, v1

    goto :goto_0

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method

.method public getFileSize()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mSize:J

    return-wide v0
.end method

.method public getItemCount(I)I
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mNumberMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "DataTransfer/BackupFilePreview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getItemCount: type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public isOtherDeviceBackup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsOtherBackup:Z

    return v0
.end method

.method public isRestored()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsRestored:Z

    return v0
.end method

.method public isSelfBackup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->mIsSelfBackup:Z

    return v0
.end method

.method public setBackupTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->backupTime:Ljava/lang/String;

    return-void
.end method
