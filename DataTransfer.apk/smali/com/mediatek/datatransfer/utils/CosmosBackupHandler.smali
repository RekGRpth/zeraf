.class public Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;
.super Ljava/lang/Object;
.source "CosmosBackupHandler.java"

# interfaces
.implements Lcom/mediatek/datatransfer/utils/BackupsHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "CosmosBackupHandler"

.field private static final PDU_END_INDEX:I = 0xb2

.field private static final PDU_START_INDEX:I = 0x3

.field private static final READ:I = 0x1

.field private static final SEND:I = 0x5

.field private static final STATUS_END_INDEX:I = 0x3

.field private static final STATUS_START_INDEX:I = 0x2

.field private static final TIME_END_INDEX:I = 0xb6

.field private static final UNREAD:I = 0x3

.field private static final mStorageName:Ljava/lang/String; = "sms.vmsg"


# instance fields
.field private mContactPath:Ljava/lang/String;

.field private mMMSPath:Ljava/lang/String;

.field private mSMSPath:Ljava/lang/String;

.field mWriter:Ljava/io/BufferedWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mContactPath:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mMMSPath:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mSMSPath:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    return-void
.end method

.method private filterFiles([Ljava/io/File;)Ljava/util/List;
    .locals 7
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 v5, 0x0

    :cond_0
    return-object v5

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v6, ".vcf"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "Contact_"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    const-string v6, ".s"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, ".m"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private formatTimeStampString(J)Ljava/lang/String;
    .locals 2
    .param p1    # J

    const-string v1, "yyyy-MM-dd kk:mm:ss"

    invoke-static {v1, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private generateSmsSmvg(Ljava/io/File;)Ljava/io/File;
    .locals 25
    .param p1    # Ljava/io/File;

    invoke-direct/range {p0 .. p1}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->parsePDUFile(Ljava/io/File;)Ljava/util/List;

    move-result-object v21

    const/4 v12, 0x0

    if-eqz v21, :cond_8

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_8

    new-instance v12, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "sms.vmsg"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_0

    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    :try_start_1
    new-instance v22, Ljava/io/BufferedWriter;

    new-instance v23, Ljava/io/OutputStreamWriter;

    new-instance v24, Ljava/io/FileOutputStream;

    move-object/from16 v0, v24

    invoke-direct {v0, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct/range {v23 .. v24}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct/range {v22 .. v23}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    const-string v22, "CosmosBackupHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "smss count is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;

    const-string v22, "CosmosBackupHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "message DisplayMessageBody type is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getMessage()Landroid/telephony/SmsMessage;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getMessage()Landroid/telephony/SmsMessage;

    move-result-object v20

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getTimeStamp()J

    move-result-wide v15

    move-object/from16 v0, p0

    move-wide v1, v15

    invoke-direct {v0, v1, v2}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->formatTimeStampString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getType()I

    move-result v22

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    const-string v4, "UNREAD"

    :goto_3
    const-string v10, "1"

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getType()I

    move-result v22

    const/16 v23, 0x5

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    const-string v5, "SENDBOX"

    :goto_4
    const-string v6, "0"

    invoke-virtual {v14}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->getType()I

    move-result v22

    const/16 v23, 0x5

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_6

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/SmsMessage;->getDestinationAddress()Ljava/lang/String;

    move-result-object v8

    :goto_5
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v22

    if-nez v22, :cond_7

    const-string v9, ""

    :goto_6
    new-instance v18, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const/16 v17, 0x0

    const-string v22, "END:VBODY"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v17

    :cond_2
    if-ltz v17, :cond_3

    const-string v22, "/"

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v22, "END:VBODY"

    add-int/lit8 v23, v17, 0x1

    const-string v24, "END:VBODY"

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    add-int v23, v23, v24

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;I)I

    move-result v17

    if-gez v17, :cond_2

    :cond_3
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v22, "CosmosBackupHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "timeStamp ="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "read = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " boxType = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "mSlotid = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " smsAddress = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " body = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " seen= "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "UNLOCKED"

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    move-object/from16 v22, v0

    invoke-static/range {v3 .. v10}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->combineVmsg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/io/BufferedWriter;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v11

    const-string v22, "CosmosBackupHandler"

    const-string v23, "mWriter.write() failed"

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_1
    move-exception v11

    const-string v22, "CosmosBackupHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "generateSmsSmvg():create file failed!"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_2
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    :cond_4
    const-string v4, "READ"

    goto/16 :goto_3

    :cond_5
    const-string v5, "INBOX"

    goto/16 :goto_4

    :cond_6
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_5

    :cond_7
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_6

    :cond_8
    const-string v22, "CosmosBackupHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "VMSG FILE IS "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    return-object v12
.end method

.method private parsePDUFile(Ljava/io/File;)Ljava/util/List;
    .locals 16
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;",
            ">;"
        }
    .end annotation

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    const/16 v1, 0xbe

    new-array v7, v1, [B

    const/4 v11, -0x1

    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v10, v7}, Ljava/io/InputStream;->read([B)I

    move-result v11

    const/4 v1, -0x1

    if-eq v11, v1, :cond_2

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-static {v7, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v13

    const/4 v1, 0x0

    aget-byte v4, v13, v1

    const/4 v1, 0x1

    if-eq v4, v1, :cond_1

    const/4 v1, 0x3

    if-eq v4, v1, :cond_1

    const/4 v1, 0x5

    if-ne v4, v1, :cond_0

    :cond_1
    const/4 v1, 0x3

    const/16 v2, 0xb2

    invoke-static {v7, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v14

    const/16 v1, 0xb2

    const/16 v2, 0xb6

    invoke-static {v7, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v15

    invoke-static {v14}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v3

    new-instance v1, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;

    invoke-static {v15}, Lcom/mediatek/datatransfer/utils/Utils;->getFPsendSMSTime([B)J

    move-result-wide v5

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;-><init>(Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;Landroid/telephony/SmsMessage;IJ)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xbe

    new-array v7, v1, [B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_2
    move-object v9, v10

    :goto_1
    return-object v12

    :catch_0
    move-exception v8

    :goto_2
    const-string v1, "CosmosBackupHandler"

    const-string v2, "@Tcard/SMS/PDU.o file read failed!"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v8

    move-object v9, v10

    goto :goto_2
.end method


# virtual methods
.method public cancel()V
    .locals 2

    const-string v0, "CosmosBackupHandler"

    const-string v1, "cancel()"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getBackupType()Ljava/lang/String;
    .locals 1

    const-string v0, "cosmos"

    return-object v0
.end method

.method public init()Z
    .locals 4

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BackUpRestore"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mContactPath:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@mms/mms_pdu"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mMMSPath:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@Tcard/SMS/PDU.o"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mSMSPath:Ljava/lang/String;

    const-string v1, "CosmosBackupHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init()=====>>mContactPath is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mContactPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onEnd()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    const-string v1, "CosmosBackupHandler"

    const-string v2, "onEnd()"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v3, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    :cond_0
    :goto_0
    sget-object v1, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->result:Ljava/util/List;

    return-object v1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v3, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mWriter:Ljava/io/BufferedWriter;

    throw v1
.end method

.method public onStart()V
    .locals 9

    const-string v6, "CosmosBackupHandler"

    const-string v7, "onStart()"

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mContactPath:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->filterFiles([Ljava/io/File;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/datatransfer/utils/FileUtils;->getNewestFile(Ljava/util/List;)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_3

    sget-object v6, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->result:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mMMSPath:Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->filterFiles([Ljava/io/File;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/datatransfer/utils/FileUtils;->getNewestFile(Ljava/util/List;)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_4

    sget-object v6, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->result:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mSMSPath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "CosmosBackupHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "smsParentFile is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->mSMSPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->generateSmsSmvg(Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_5

    sget-object v6, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->result:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    return-void

    :cond_3
    const-string v6, "CosmosBackupHandler"

    const-string v7, "contact file is null!!"

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v6, "CosmosBackupHandler"

    const-string v7, "mms file is null!!"

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v6, "CosmosBackupHandler"

    const-string v7, "vmsg file is null!!"

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public reset()V
    .locals 1

    sget-object v0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;->result:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
