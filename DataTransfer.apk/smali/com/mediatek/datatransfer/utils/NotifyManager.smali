.class public Lcom/mediatek/datatransfer/utils/NotifyManager;
.super Ljava/lang/Object;
.source "NotifyManager.java"


# static fields
.field public static final BACKUP_APPLICATION_INTENT:Ljava/lang/String; = "com.mediatek.backuprestore.intent.AppBackupActivity"

.field public static final BACKUP_PERSONALDATA_INTENT:Ljava/lang/String; = "com.mediatek.backuprestore.intent.PersonalDataBackupActivity"

.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/NotifyManager:"

.field public static final FP_NEW_DETECTION_INTENT_LIST:Ljava/lang/String; = "com.mediatek.backuprestore.intent.MainActivity"

.field public static final FP_NEW_DETECTION_NOTIFY_TYPE_DEAFAULT:I = 0x0

.field public static final FP_NEW_DETECTION_NOTIFY_TYPE_LIST:I = 0x1

.field public static final NOTIFY_BACKUPING:I = 0x2

.field public static final NOTIFY_NEW_DETECTION:I = 0x1

.field public static final NOTIFY_RESTOREING:I = 0x3

.field public static final RESTORE_APPLICATION_INTENT:Ljava/lang/String; = "com.mediatek.backuprestore.intent.AppRestoreActivity"

.field public static final RESTORE_PERSONALDATA_INTENT:Ljava/lang/String; = "com.mediatek.backuprestore.intent.PersonalDataRestoreActivity"

.field private static sNotifyManager:Lcom/mediatek/datatransfer/utils/NotifyManager;


# instance fields
.field private mMaxPercent:I

.field private mNotification:Landroid/app/Notification$Builder;

.field private mNotificationContext:Landroid/content/Context;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationType:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method private configAndShowNotification(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/app/PendingIntent;

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method private configAndShowNotification(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0, p4}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/datatransfer/utils/NotifyManager;->sNotifyManager:Lcom/mediatek/datatransfer/utils/NotifyManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/utils/NotifyManager;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/datatransfer/utils/NotifyManager;->sNotifyManager:Lcom/mediatek/datatransfer/utils/NotifyManager;

    :cond_0
    sget-object v0, Lcom/mediatek/datatransfer/utils/NotifyManager;->sNotifyManager:Lcom/mediatek/datatransfer/utils/NotifyManager;

    return-object v0
.end method

.method private getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private setNotificationProgress(ILjava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-direct {p0, p5}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getPendingIntenActivity(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-int/lit8 v2, p4, 0x64

    iget v3, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    iget v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p4, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v3, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public clearNotification()V
    .locals 3

    const-string v0, "DataTransfer/NotifyManager:"

    const-string v1, "clearNotification"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v0, "DataTransfer/NotifyManager:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearNotification+mNotificationType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotification:Landroid/app/Notification$Builder;

    :cond_0
    return-void
.end method

.method public setMaxPercent(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    return-void
.end method

.method public showBackupNotification(Ljava/lang/String;II)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v1, 0x7f060039

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v5, 0x0

    const/16 v0, 0x10

    if-ne p2, v0, :cond_1

    const-string v5, "com.mediatek.backuprestore.intent.AppBackupActivity"

    :goto_1
    const v1, 0x7f020001

    move-object v0, p0

    move-object v3, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datatransfer/utils/NotifyManager;->setNotificationProgress(ILjava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v5, "com.mediatek.backuprestore.intent.PersonalDataBackupActivity"

    goto :goto_1
.end method

.method public showNewDetectionNotification(ILjava/lang/String;)V
    .locals 10
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x1

    iput v9, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v7, 0x7f060037

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v7, 0x7f060038

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-ne p1, v9, :cond_1

    const-string v3, "com.mediatek.backuprestore.intent.MainActivity"

    :goto_0
    if-nez p1, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    const-string v6, "DataTransfer/NotifyManager:"

    const-string v7, "[showNewDetectionNotification] ERROR notification ! folder is null !"

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const-string v3, "com.mediatek.backuprestore.intent.PersonalDataRestoreActivity"

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.mediatek.datatransfer.newdata_transfer"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "filename"

    invoke-virtual {v2, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "notify_type"

    invoke-virtual {v2, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "DataTransfer/NotifyManager:"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[showNewDetectionNotification] Folder = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v2, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v4, Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    const v7, 0x7f02000e

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v9, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1
.end method

.method public showRestoreNotification(Ljava/lang/String;II)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mMaxPercent:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationType:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/NotifyManager;->mNotificationContext:Landroid/content/Context;

    const v1, 0x7f06003a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v5, 0x0

    const/16 v0, 0x10

    if-ne p2, v0, :cond_1

    const-string v5, "com.mediatek.backuprestore.intent.AppRestoreActivity"

    :goto_1
    const v1, 0x7f020001

    move-object v0, p0

    move-object v3, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datatransfer/utils/NotifyManager;->setNotificationProgress(ILjava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v5, "com.mediatek.backuprestore.intent.PersonalDataRestoreActivity"

    goto :goto_1
.end method
