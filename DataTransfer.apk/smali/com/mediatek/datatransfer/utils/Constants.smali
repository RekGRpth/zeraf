.class public Lcom/mediatek/datatransfer/utils/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/utils/Constants$BackupScanType;,
        Lcom/mediatek/datatransfer/utils/Constants$ContactType;,
        Lcom/mediatek/datatransfer/utils/Constants$LogTag;,
        Lcom/mediatek/datatransfer/utils/Constants$State;,
        Lcom/mediatek/datatransfer/utils/Constants$MessageID;,
        Lcom/mediatek/datatransfer/utils/Constants$DialogID;,
        Lcom/mediatek/datatransfer/utils/Constants$ModulePath;
    }
.end annotation


# static fields
.field public static final ACTION_NEW_DATA_DETECTED:Ljava/lang/String; = "com.mediatek.datatransfer.newdata"

.field public static final ACTION_NEW_DATA_DETECTED_TRANSFER:Ljava/lang/String; = "com.mediatek.datatransfer.newdata_transfer"

.field public static final ACTION_SD_EXIST:Ljava/lang/String; = "SD_EXIST"

.field public static final ANDROID:Ljava/lang/String; = "Android "

.field public static final BACKUP:Ljava/lang/String; = "backup"

.field public static final BACKUP_FILE_EXT:Ljava/lang/String; = "zip"

.field public static final BACKUP_FOLDER_NAME:Ljava/lang/String; = ".backup"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final FILE:Ljava/lang/String; = "file"

.field public static final FILENAME:Ljava/lang/String; = "filename"

.field public static final INTENT_SD_SWAP:Ljava/lang/String; = "com.mediatek.SD_SWAP"

.field public static final ITEM_NAME:Ljava/lang/String; = "name"

.field public static final ITEM_PACKAGENAME:Ljava/lang/String; = "packageName"

.field public static final ITEM_RESULT:Ljava/lang/String; = "result"

.field public static final ITEM_TEXT:Ljava/lang/String; = "text"

.field public static final KEY_SAVED_DATA:Ljava/lang/String; = "data"

.field public static final MESSAGE_BOX_TYPE_DRAFT:Ljava/lang/String; = "3"

.field public static final MESSAGE_BOX_TYPE_INBOX:Ljava/lang/String; = "1"

.field public static final MESSAGE_BOX_TYPE_OUTBOX:Ljava/lang/String; = "4"

.field public static final MESSAGE_BOX_TYPE_SENT:Ljava/lang/String; = "2"

.field public static final NOTIFY_TYPE:Ljava/lang/String; = "notify_type"

.field public static final NUMBER_IMPORT_CONTACTS_EACH:I = 0x1e0

.field public static final NUMBER_IMPORT_CONTACTS_ONE_SHOT:I = 0x5dc

.field public static final NUMBER_IMPORT_MMS_EACH:I = 0x5

.field public static final NUMBER_IMPORT_SMS_EACH:I = 0x14

.field public static final RECORD_XML:Ljava/lang/String; = "record.xml"

.field public static final RESET_FLAG_FILE:Ljava/lang/String; = "data/.backuprestore"

.field public static final RESTORE:Ljava/lang/String; = "restore"

.field public static final RESULT_KEY:Ljava/lang/String; = "result"

.field public static final SCAN_RESULT_KEY_APP_DATA:Ljava/lang/String; = "appData"

.field public static final SCAN_RESULT_KEY_PERSONAL_DATA:Ljava/lang/String; = "personalData"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final TIME_SLEEP_WHEN_COMPOSE_ONE:I = 0xc8

.field public static final URI_CALENDAR_IMPORTER_EVENTS:Ljava/lang/String; = "content://com.mediatek.calendarimporter/events"

.field public static final URI_MMS:Ljava/lang/String; = "content://mms/"

.field public static final URI_MMS_SMS:Ljava/lang/String; = "content://mms-sms/conversations/"

.field public static final URI_NOTEBOOK:Ljava/lang/String; = "content://com.mediatek.notebook.NotePad/notes"

.field public static final URI_SMS:Ljava/lang/String; = "content://sms"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
