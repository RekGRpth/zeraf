.class Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread$1;
.super Ljava/lang/Object;
.source "BackupFileScanner.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->sort(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/mediatek/datatransfer/utils/BackupFilePreview;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread$1;->this$1:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/mediatek/datatransfer/utils/BackupFilePreview;Lcom/mediatek/datatransfer/utils/BackupFilePreview;)I
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/utils/BackupFilePreview;
    .param p2    # Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getBackupTime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getBackupTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    check-cast p2, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread$1;->compare(Lcom/mediatek/datatransfer/utils/BackupFilePreview;Lcom/mediatek/datatransfer/utils/BackupFilePreview;)I

    move-result v0

    return v0
.end method
