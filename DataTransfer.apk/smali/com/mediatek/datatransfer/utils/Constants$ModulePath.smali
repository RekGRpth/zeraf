.class public Lcom/mediatek/datatransfer/utils/Constants$ModulePath;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ModulePath"
.end annotation


# static fields
.field public static final ALL_APK_FILES:Ljava/lang/String; = ".*\\.apk"

.field public static final FILE_EXT_APP:Ljava/lang/String; = ".apk"

.field public static final FILE_EXT_PDU:Ljava/lang/String; = ".pdu"

.field public static final FOLDER_APP:Ljava/lang/String; = "App"

.field public static final FOLDER_BOOKMARK:Ljava/lang/String; = "bookmark"

.field public static final FOLDER_CALENDAR:Ljava/lang/String; = "calendar"

.field public static final FOLDER_CONTACT:Ljava/lang/String; = "Contact"

.field public static final FOLDER_DATA:Ljava/lang/String; = "Data"

.field public static final FOLDER_MMS:Ljava/lang/String; = "mms"

.field public static final FOLDER_MUSIC:Ljava/lang/String; = "music"

.field public static final FOLDER_NOTEBOOK:Ljava/lang/String; = "notebook"

.field public static final FOLDER_PICTURE:Ljava/lang/String; = "picture"

.field public static final FOLDER_SETTINGS:Ljava/lang/String; = "settings"

.field public static final FOLDER_SMS:Ljava/lang/String; = "sms"

.field public static final FOLDER_TEMP:Ljava/lang/String; = "temp"

.field public static final MMS_XML:Ljava/lang/String; = "msg_box.xml"

.field public static final NAME_CALENDAR:Ljava/lang/String; = "calendar.vcs"

.field public static final NAME_CONTACT:Ljava/lang/String; = "contact.vcf"

.field public static final NAME_MMS:Ljava/lang/String; = "mms"

.field public static final NAME_SMS:Ljava/lang/String; = "sms"

.field public static final NOTEBOOK_XML:Ljava/lang/String; = "notebook.xml"

.field public static final SCHEMA_ALL_APK:Ljava/lang/String; = "apps/.*\\.apk"

.field public static final SCHEMA_ALL_CALENDAR:Ljava/lang/String; = "calendar/calendar[0-9]+\\.vcs"

.field public static final SCHEMA_ALL_CONTACT:Ljava/lang/String; = "contacts/contact[0-9]+\\.vcf"

.field public static final SCHEMA_ALL_MUSIC:Ljava/lang/String; = "music/.*"

.field public static final SCHEMA_ALL_PICTURE:Ljava/lang/String; = "picture/.*"

.field public static final SCHEMA_ALL_SMS:Ljava/lang/String; = "sms/sms[0-9]+"

.field public static final SETTINGS_XML:Ljava/lang/String; = "settings.xml"

.field public static final SMS_VMSG:Ljava/lang/String; = "sms.vmsg"


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/utils/Constants;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/utils/Constants;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/Constants$ModulePath;->this$0:Lcom/mediatek/datatransfer/utils/Constants;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
