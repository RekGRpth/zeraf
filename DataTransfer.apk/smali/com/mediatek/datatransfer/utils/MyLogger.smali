.class public Lcom/mediatek/datatransfer/utils/MyLogger;
.super Ljava/lang/Object;
.source "MyLogger.java"


# static fields
.field public static final APP_TAG:Ljava/lang/String; = "App: "

.field public static final BACKUP_ACTIVITY_TAG:Ljava/lang/String; = "BackupActivity: "

.field public static final BACKUP_ENGINE_TAG:Ljava/lang/String; = "BackupEngine: "

.field public static final BACKUP_SERVICE_TAG:Ljava/lang/String; = "BackupService: "

.field public static final BOOKMARK_TAG:Ljava/lang/String; = "Bookmark: "

.field public static final CONTACT_TAG:Ljava/lang/String; = "Contact: "

.field public static final LOG_TAG:Ljava/lang/String; = "DataTransfer"

.field public static final MESSAGE_TAG:Ljava/lang/String; = "Message: "

.field public static final MMS_TAG:Ljava/lang/String; = "Mms: "

.field public static final MUSIC_TAG:Ljava/lang/String; = "Music: "

.field public static final NOTEBOOK_TAG:Ljava/lang/String; = "NoteBook: "

.field public static final PICTURE_TAG:Ljava/lang/String; = "Picture: "

.field public static final SETTINGS_TAG:Ljava/lang/String; = "Settings: "

.field public static final SMS_TAG:Ljava/lang/String; = "SMS: "


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logD(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logE(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logV(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logW(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
