.class public Lcom/mediatek/datatransfer/utils/Constants$BackupScanType;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BackupScanType"
.end annotation


# static fields
.field public static final COSMOS:Ljava/lang/String; = "cosmos"

.field public static final COSMOS_CONTACT_PATH:Ljava/lang/String; = "BackUpRestore"

.field public static final COSMOS_MMS_PATH:Ljava/lang/String; = "@mms/mms_pdu"

.field public static final COSMOS_SMS_PATH:Ljava/lang/String; = "@Tcard/SMS/PDU.o"

.field public static final DATATRANSFER:Ljava/lang/String; = "datatransfer"

.field public static final MD5_EXIST:I = 0x1

.field public static final MD5_NOT_EXIST:I = 0x0

.field public static final NON_DATATRANSFER:Ljava/lang/String; = "non_datatransfer"

.field public static final PERFERENCE_TAG:Ljava/lang/String; = "md5"

.field public static final PLUTO:Ljava/lang/String; = "pluto"

.field public static final PLUTO_PATH:Ljava/lang/String; = "~vcard.vcf"


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/utils/Constants;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/utils/Constants;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/Constants$BackupScanType;->this$0:Lcom/mediatek/datatransfer/utils/Constants;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
