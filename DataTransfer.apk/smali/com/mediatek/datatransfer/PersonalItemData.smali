.class public Lcom/mediatek/datatransfer/PersonalItemData;
.super Ljava/lang/Object;
.source "PersonalItemData.java"


# instance fields
.field private mIsEnable:Z

.field private mType:I


# direct methods
.method public constructor <init>(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mType:I

    iput-boolean p2, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mIsEnable:Z

    return-void
.end method


# virtual methods
.method public getIconId()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mType:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f020008

    goto :goto_0

    :sswitch_1
    const v0, 0x7f02000c

    goto :goto_0

    :sswitch_2
    const v0, 0x7f020012

    goto :goto_0

    :sswitch_3
    const v0, 0x7f020007

    goto :goto_0

    :sswitch_4
    const v0, 0x7f02000d

    goto :goto_0

    :sswitch_5
    const v0, 0x7f020011

    goto :goto_0

    :sswitch_6
    const v0, 0x7f020004

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_3
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
        0x80 -> :sswitch_4
        0x100 -> :sswitch_5
        0x200 -> :sswitch_6
    .end sparse-switch
.end method

.method public getTextId()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mType:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f060006

    goto :goto_0

    :sswitch_1
    const v0, 0x7f060007

    goto :goto_0

    :sswitch_2
    const v0, 0x7f060008

    goto :goto_0

    :sswitch_3
    const v0, 0x7f060009

    goto :goto_0

    :sswitch_4
    const v0, 0x7f06000b

    goto :goto_0

    :sswitch_5
    const v0, 0x7f06000c

    goto :goto_0

    :sswitch_6
    const v0, 0x7f06000d

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_3
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
        0x80 -> :sswitch_4
        0x100 -> :sswitch_5
        0x200 -> :sswitch_6
    .end sparse-switch
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mType:I

    return v0
.end method

.method public isEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mIsEnable:Z

    return v0
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/PersonalItemData;->mIsEnable:Z

    return-void
.end method
