.class public Lcom/mediatek/datatransfer/BackupService;
.super Landroid/app/Service;
.source "BackupService.java"

# interfaces
.implements Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;
.implements Lcom/mediatek/datatransfer/ProgressReporter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;,
        Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;,
        Lcom/mediatek/datatransfer/BackupService$BackupBinder;,
        Lcom/mediatek/datatransfer/BackupService$BackupProgress;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/BackupService"


# instance fields
.field private mAppResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

.field private mBinder:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

.field private mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

.field mParasMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mResultType:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

.field private mState:I

.field private mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

.field notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;-><init>(Lcom/mediatek/datatransfer/BackupService;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBinder:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    new-instance v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/BackupService$BackupProgress;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/BackupService;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget v0, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/datatransfer/BackupService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/BackupService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/datatransfer/BackupService;Lcom/mediatek/datatransfer/BackupEngine;)Lcom/mediatek/datatransfer/BackupEngine;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/BackupService;
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine;

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mAppResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupService$BackupProgress;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/datatransfer/BackupService;Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/BackupService;
    .param p1    # Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mResultType:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBinder:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    const-string v1, "DataTransfer/BackupService"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;-><init>(Lcom/mediatek/datatransfer/BackupService;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.mediatek.datatransfer.newdata"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupEngine;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/BackupEngine;->setOnBackupDoneListner(Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mBackupEngine:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupEngine;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->notificationReceiver:Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;

    :cond_1
    return-void
.end method

.method public onEnd(Lcom/mediatek/datatransfer/modules/Composer;Z)V
    .locals 5
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v1, -0x2

    :cond_1
    :goto_0
    const-string v2, "DataTransfer/BackupService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "one Composer end: type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    invoke-direct {v0, v2, v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;-><init>(II)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onErr(Ljava/io/IOException;)V
    .locals 3
    .param p1    # Ljava/io/IOException;

    const-string v0, "DataTransfer/BackupService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onErr "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    invoke-interface {v0, p1}, Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;->onBackupErr(Ljava/io/IOException;)V

    :cond_0
    return-void
.end method

.method public onFinishBackup(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;)V
    .locals 6
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    const/4 v5, 0x5

    const-string v2, "DataTransfer/BackupService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFinishBackup result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupService;->mResultType:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    sget-object p1, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Cancel:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    :cond_0
    sget-object v2, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Success:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Cancel:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    if-eq p1, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->getResult()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->setResult(I)V

    goto :goto_0

    :cond_2
    iput v5, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iget-object v3, p0, Lcom/mediatek/datatransfer/BackupService;->mResultList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupService;->mAppResultList:Ljava/util/ArrayList;

    invoke-interface {v2, p1, v3, v4}, Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;->onBackupEnd(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :goto_1
    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/utils/NotifyManager;->clearNotification()V

    return-void

    :cond_3
    iput v5, p0, Lcom/mediatek/datatransfer/BackupService;->mState:I

    goto :goto_1
.end method

.method public onOneFinished(Lcom/mediatek/datatransfer/modules/Composer;Z)V
    .locals 6
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # Z

    const/16 v4, 0x10

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v3, v2, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mAppResultList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mAppResultList:Ljava/util/ArrayList;

    :cond_0
    if-eqz p2, :cond_4

    const/4 v1, 0x0

    :goto_0
    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    invoke-direct {v0, v4, v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;-><init>(II)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v3, v3, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mAppResultList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iget-object v3, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v3, v3, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-interface {v2, p1, v3}, Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;->onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v2, v2, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v3

    invoke-static {p0, v3}, Lcom/mediatek/datatransfer/utils/ModuleType;->getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v5, v5, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/mediatek/datatransfer/utils/NotifyManager;->showBackupNotification(Ljava/lang/String;II)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "onRebind"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iput-object p1, v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mComposer:Lcom/mediatek/datatransfer/modules/Composer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v1

    iput v1, v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mType:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v1

    iput v1, v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    const/4 v1, 0x0

    iput v1, v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mStatusListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    invoke-interface {v0, p1}, Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;->onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v0, v0, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService;->mCurrentProgress:Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    iget v1, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/NotifyManager;->setMaxPercent(I)V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
