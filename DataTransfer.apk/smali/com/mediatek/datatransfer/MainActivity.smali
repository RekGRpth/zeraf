.class public Lcom/mediatek/datatransfer/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/MainActivity$TabListener;,
        Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;
    }
.end annotation


# static fields
.field public static final ACTION_BACKUP:Ljava/lang/String; = "action_backup"

.field public static final ACTION_RESTORE:Ljava/lang/String; = "ction_restore"

.field public static final ACTION_WHERE:Ljava/lang/String; = "action_where"

.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/MainActivity"


# instance fields
.field private final STATE_TAB:Ljava/lang/String;

.field private mBackupFragment:Landroid/app/Fragment;

.field mMainOnSDCardStatusChangedListener:Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;

.field private mRestoreFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "tab"

    iput-object v0, p0, Lcom/mediatek/datatransfer/MainActivity;->STATE_TAB:Ljava/lang/String;

    new-instance v0, Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;-><init>(Lcom/mediatek/datatransfer/MainActivity;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/MainActivity;->mMainOnSDCardStatusChangedListener:Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/MainActivity;->sdCardCheck()V

    return-void
.end method

.method private initFragments(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "backup"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mBackupFragment:Landroid/app/Fragment;

    const-string v1, "restore"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mRestoreFragment:Landroid/app/Fragment;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mBackupFragment:Landroid/app/Fragment;

    if-nez v1, :cond_1

    new-instance v1, Lcom/mediatek/datatransfer/BackupTabFragment;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/BackupTabFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mBackupFragment:Landroid/app/Fragment;

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mRestoreFragment:Landroid/app/Fragment;

    if-nez v1, :cond_2

    new-instance v1, Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/RestoreTabFragment;-><init>()V

    iput-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mRestoreFragment:Landroid/app/Fragment;

    :cond_2
    return-void
.end method

.method private registerSDCardListener()V
    .locals 2

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mMainOnSDCardStatusChangedListener:Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->registerOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    return-void
.end method

.method private sdCardCheck()V
    .locals 2

    const/16 v1, 0x7d7

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->removeDialog(I)V

    goto :goto_0
.end method

.method private setupActionBar(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/16 v3, 0xa

    invoke-virtual {v0, v6, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    new-instance v3, Lcom/mediatek/datatransfer/MainActivity$TabListener;

    iget-object v4, p0, Lcom/mediatek/datatransfer/MainActivity;->mBackupFragment:Landroid/app/Fragment;

    const-string v5, "backup"

    invoke-direct {v3, p0, v4, v5}, Lcom/mediatek/datatransfer/MainActivity$TabListener;-><init>(Lcom/mediatek/datatransfer/MainActivity;Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    new-instance v3, Lcom/mediatek/datatransfer/MainActivity$TabListener;

    iget-object v4, p0, Lcom/mediatek/datatransfer/MainActivity;->mRestoreFragment:Landroid/app/Fragment;

    const-string v5, "restore"

    invoke-direct {v3, p0, v4, v5}, Lcom/mediatek/datatransfer/MainActivity$TabListener;-><init>(Lcom/mediatek/datatransfer/MainActivity;Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    if-eqz p1, :cond_0

    const-string v3, "tab"

    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    return-void
.end method

.method private unRegisterSDCardListener()V
    .locals 2

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/MainActivity;->mMainOnSDCardStatusChangedListener:Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->unRegisterOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    const-string v0, "DataTransfer/MainActivity"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "DataTransfer/MainActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/MainActivity;->initFragments(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/MainActivity;->setupActionBar(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/MainActivity;->registerSDCardListener()V

    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->clearNotification()V

    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "DataTransfer/MainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "oncreateDialog, id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    const v1, 0x7f060030

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    const v1, 0x7f060031

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "DataTransfer/MainActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/MainActivity;->unRegisterSDCardListener()V

    return-void
.end method

.method protected onResume()V
    .locals 7

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v4, "DataTransfer/MainActivity"

    const-string v5, "onResume"

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v4, "action_where"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "DataTransfer/MainActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "actionWhere is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_1

    const-string v4, "com.mediatek.backuprestore.intent.MainActivity"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    :cond_0
    :goto_0
    const-string v4, "action_where"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/datatransfer/MainActivity;->sdCardCheck()V

    return-void

    :cond_1
    if-eqz v3, :cond_2

    const-string v4, "ction_restore"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_0

    const-string v4, "action_backup"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    :cond_3
    const-string v4, "DataTransfer/MainActivity"

    const-string v5, "Intent is null"

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    const-string v2, "tab"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
