.class public Lcom/mediatek/datatransfer/RecordXmlInfo;
.super Ljava/lang/Object;
.source "RecordXmlInfo.java"


# static fields
.field public static final BACKUP:Ljava/lang/String; = "backup"

.field public static final DEVICE:Ljava/lang/String; = "device"

.field public static final RESTORE:Ljava/lang/String; = "restore"

.field public static final ROOT:Ljava/lang/String; = "root"

.field public static final TIME:Ljava/lang/String; = "time"


# instance fields
.field private mDevice:Ljava/lang/String;

.field private mIsRestore:Z

.field private mTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p3}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getDevice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mDevice:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mTime:Ljava/lang/String;

    return-object v0
.end method

.method public isRestore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mIsRestore:Z

    return v0
.end method

.method public setDevice(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mDevice:Ljava/lang/String;

    return-void
.end method

.method public setRestore(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mIsRestore:Z

    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RecordXmlInfo;->mTime:Ljava/lang/String;

    return-void
.end method
