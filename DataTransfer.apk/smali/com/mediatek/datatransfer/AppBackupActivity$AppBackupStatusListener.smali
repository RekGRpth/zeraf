.class Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;
.super Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;
.source "AppBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AppBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppBackupStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AppBackupActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppBackupActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/AppBackupActivity;
    .param p2    # Lcom/mediatek/datatransfer/AppBackupActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V

    return-void
.end method


# virtual methods
.method public onBackupEnd(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$3;

    invoke-direct {v1, p0, p1, p3}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$3;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$400(Lcom/mediatek/datatransfer/AppBackupActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onComposerChasetProgressBarIndeterminateVisibility(false);nged: error[composer is null]"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$400(Lcom/mediatek/datatransfer/AppBackupActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onComposerChanged: type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;

    invoke-direct {v1, p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;Lcom/mediatek/datatransfer/modules/Composer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$2;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;ILcom/mediatek/datatransfer/modules/Composer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
