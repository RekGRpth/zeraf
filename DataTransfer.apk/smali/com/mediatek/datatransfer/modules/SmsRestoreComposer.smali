.class public Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "SmsRestoreComposer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;
    }
.end annotation


# static fields
.field private static final BEGIN_VBODY:Ljava/lang/String; = "BEGIN:VBODY"

.field private static final BEGIN_VCARD:Ljava/lang/String; = "BEGIN:VCARD"

.field private static final BEGIN_VMSG:Ljava/lang/String; = "BEGIN:VMSG"

.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/SmsRestoreComposer"

.field private static final COLUMN_NAME_IMPORT_SMS:Ljava/lang/String; = "import_sms"

.field private static final DATE:Ljava/lang/String; = "Date:"

.field private static final END_VBODY:Ljava/lang/String; = "END:VBODY"

.field private static final END_VCARD:Ljava/lang/String; = "END:VCARD"

.field private static final END_VMSG:Ljava/lang/String; = "END:VMSG"

.field private static final FROMTEL:Ljava/lang/String; = "FROMTEL:"

.field private static final SUBJECT:Ljava/lang/String; = "Subject"

.field private static final VERSION:Ljava/lang/String; = "VERSION:"

.field private static final VMESSAGE_END_OF_COLON:Ljava/lang/String; = ":"

.field private static final VMESSAGE_END_OF_LINE:Ljava/lang/String; = "\r\n"

.field private static final XBOX:Ljava/lang/String; = "X-BOX:"

.field private static final XLOCKED:Ljava/lang/String; = "X-LOCKED:"

.field private static final XREAD:Ljava/lang/String; = "X-READ:"

.field private static final XSEEN:Ljava/lang/String; = "X-SEEN:"

.field private static final XSIMID:Ljava/lang/String; = "X-SIMID:"

.field private static final XTYPE:Ljava/lang/String; = "X-TYPE:"

.field private static final mSmsUriArray:[Landroid/net/Uri;


# instance fields
.field private mIndex:I

.field private mOperationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private mTime:J

.field private mVmessageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mSmsUriArray:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private deleteAllPhoneSms()Z
    .locals 11

    const/4 v10, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    const-string v3, "DataTransfer/SmsRestoreComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "begin delete:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://sms"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "type <> ?"

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "1"

    aput-object v7, v6, v9

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://sms"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "date < ?"

    new-array v6, v8, [Ljava/lang/String;

    iget-wide v7, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mTime:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/Telephony$WapPush;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const-string v3, "DataTransfer/SmsRestoreComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteAllPhoneSms():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sms deleted!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "wappush deleted!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    const-string v3, "DataTransfer/SmsRestoreComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end delete:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v2
.end method

.method private parsePdu(Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;)Landroid/content/ContentValues;
    .locals 8
    .param p1    # Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;

    const/4 v5, 0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "address"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getSmsAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "body"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "DataTransfer/SmsRestoreComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "readorunread :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getReadByte()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "read"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getReadByte()Ljava/lang/String;

    move-result-object v4

    const-string v7, "UNREAD"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "seen"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getSeen()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "locked"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getLocked()Ljava/lang/String;

    move-result-object v4

    const-string v7, "LOCKED"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "1"

    :goto_1
    invoke-virtual {v3, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "-1"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getSimCardid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    if-gez v2, :cond_2

    const-string v1, "-1"

    :goto_2
    const-string v4, "sim_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "date"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getTimeStamp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "type"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getBoxType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "INBOX"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v3

    :cond_0
    move v4, v5

    goto :goto_0

    :cond_1
    const-string v4, "0"

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/mediatek/telephony/SimInfoManager;->getSimInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v1, "-1"

    goto :goto_2

    :cond_3
    iget-wide v6, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    const/4 v5, 0x2

    goto :goto_3
.end method


# virtual methods
.method public getCount()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    :cond_0
    const-string v1, "DataTransfer/SmsRestoreComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCount():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public getModuleType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getSmsRestoreEntry()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;",
            ">;"
        }
    .end annotation

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/text/SimpleDateFormat;

    const-string v21, "yyyy-MM-dd kk:mm:ss"

    move-object/from16 v0, v21

    invoke-direct {v15, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v7, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "sms"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "sms.vmsg"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v10, 0x0

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v3, 0x0

    const/16 v17, 0x0

    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_11

    const-string v21, "BEGIN:VMSG"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    if-nez v3, :cond_1

    new-instance v17, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;-><init>(Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(BEGIN_VMSG)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v21, "FROMTEL:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    if-nez v3, :cond_2

    const-string v21, "FROMTEL:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setSmsAddress(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(FROMTEL)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v21, "X-BOX:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    if-nez v3, :cond_3

    const-string v21, "X-BOX:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setBoxType(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(XBOX)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v21, "X-READ:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    if-nez v3, :cond_4

    const-string v21, "X-READ:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setReadByte(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(XREAD)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v21, "X-SEEN:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_5

    if-nez v3, :cond_5

    const-string v21, "X-SEEN:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setSeen(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(XSEEN)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-string v21, "X-SIMID:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    if-nez v3, :cond_6

    const-string v21, "X-SIMID:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setSimCardid(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(XSIMID)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v21, "X-LOCKED:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_7

    if-nez v3, :cond_7

    const-string v21, "X-LOCKED:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setLocked(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(XLOCKED)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const-string v21, "Date:"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_8

    if-nez v3, :cond_8

    const-string v21, "Date:"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setTimeStamp(Ljava/lang/String;)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(DATE)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const-string v21, "Subject"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_c

    if-nez v3, :cond_c

    const-string v21, ":"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    const-string v21, "END:VBODY"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-lez v11, :cond_b

    new-instance v18, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    :cond_9
    if-lez v11, :cond_a

    add-int/lit8 v21, v11, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v21, "END:VBODY"

    const-string v22, "END:VBODY"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int v22, v22, v11

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;I)I

    move-result v11

    if-gtz v11, :cond_9

    :cond_a
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_b
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v3, 0x1

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(SUBJECT)"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v6

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "init failed"

    invoke-static/range {v21 .. v22}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v16

    :cond_c
    :try_start_1
    const-string v21, "END:VBODY"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_d

    const/4 v3, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->setBody(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->setLength(I)V

    const-string v21, "DataTransfer/SmsRestoreComposer"

    const-string v22, "startsWith(END_VBODY)"

    invoke-static/range {v21 .. v22}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    if-eqz v3, :cond_0

    const-string v21, "\r\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v21, "END:VBODY"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    if-lez v12, :cond_10

    new-instance v19, Ljava/lang/StringBuffer;

    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    :cond_e
    if-lez v12, :cond_f

    add-int/lit8 v21, v12, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    const-string v21, "END:VBODY"

    const-string v22, "END:VBODY"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int v22, v22, v12

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;I)I

    move-result v12

    if-gtz v12, :cond_e

    :cond_f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    :cond_10
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v21, "DataTransfer/SmsRestoreComposer"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "appendbody=true,tmpbody="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public implementComposeOneEntity()Z
    .locals 10

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    iget v7, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mIndex:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;

    if-eqz v3, :cond_7

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->parsePdu(Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;)Landroid/content/ContentValues;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v5, "DataTransfer/SmsRestoreComposer"

    const-string v6, "parsePdu():values=null"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    move v5, v2

    :cond_1
    return v5

    :cond_2
    const-string v6, "DataTransfer/SmsRestoreComposer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "begin restore:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->getBoxType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "INBOX"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v1, v5

    :goto_1
    const-string v6, "DataTransfer/SmsRestoreComposer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mboxType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v6, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mSmsUriArray:[Landroid/net/Uri;

    add-int/lit8 v7, v1, -0x1

    aget-object v6, v6, v7

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v6, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mIndex:I

    rem-int/lit8 v6, v6, 0x14

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->isAfterLast()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->isAfterLast()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "import_sms"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_4
    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_5

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "sms"

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    :cond_5
    :goto_2
    const-string v5, "DataTransfer/SmsRestoreComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "end restore:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x2

    goto :goto_1

    :catch_0
    move-exception v5

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :catch_1
    move-exception v5

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :catch_2
    move-exception v5

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    throw v5

    :cond_7
    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    invoke-interface {v5, v6}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    goto/16 :goto_0
.end method

.method public init()Z
    .locals 6

    const/4 v1, 0x0

    const-string v2, "DataTransfer/SmsRestoreComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "begin init:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mTime:J

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->getSmsRestoreEntry()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "DataTransfer/SmsRestoreComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "end init:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DataTransfer/SmsRestoreComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init():"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :catch_0
    move-exception v0

    const-string v2, "DataTransfer/SmsRestoreComposer"

    const-string v3, "init failed"

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isAfterLast()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mIndex:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    const-string v1, "DataTransfer/SmsRestoreComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAfterLast():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEnd()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mVmessageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;->mOperationList:Ljava/util/ArrayList;

    :cond_1
    const-string v0, "DataTransfer/SmsRestoreComposer"

    const-string v1, "onEnd()"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    const-string v0, "DataTransfer/SmsRestoreComposer"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
