.class public Lcom/mediatek/datatransfer/modules/MmsBackupComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "MmsBackupComposer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/modules/MmsBackupComposer$1;,
        Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;,
        Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/MmsBackupComposer"

.field private static final COLUMN_NAME_DATE:Ljava/lang/String; = "date"

.field private static final COLUMN_NAME_ID:Ljava/lang/String; = "_id"

.field private static final COLUMN_NAME_LOCKED:Ljava/lang/String; = "locked"

.field private static final COLUMN_NAME_MESSAGE_BOX:Ljava/lang/String; = "msg_box"

.field private static final COLUMN_NAME_READ:Ljava/lang/String; = "read"

.field private static final COLUMN_NAME_SIMID:Ljava/lang/String; = "sim_id"

.field private static final COLUMN_NAME_TYPE:Ljava/lang/String; = "m_type"

.field private static final MMS_EXCLUDE_TYPE:[Ljava/lang/String;

.field private static final mMmsUriArray:[Landroid/net/Uri;

.field private static final mStoragePath:Ljava/lang/String; = "mms"


# instance fields
.field private mLock:Ljava/lang/Object;

.field private mMmsCursorArray:[Landroid/database/Cursor;

.field private mMmsIndex:I

.field private mPduList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;",
            ">;"
        }
    .end annotation
.end field

.field private mTempPduList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;",
            ">;"
        }
    .end annotation
.end field

.field private mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "134"

    aput-object v1, v0, v2

    const-string v1, "130"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->MMS_EXCLUDE_TYPE:[Ljava/lang/String;

    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mTempPduList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Ljava/lang/String;[B)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->writeToFile(Ljava/lang/String;[B)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Lcom/mediatek/datatransfer/modules/MmsXmlComposer;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method private writeToFile(Ljava/lang/String;[B)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    array-length v3, p2

    invoke-virtual {v1, p2, v2, v3}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public composeOneEntity()Z
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->implementComposeOneEntity()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/2addr v1, v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string v5, "DataTransfer/MmsBackupComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getCount():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public getModuleType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public implementComposeOneEntity()Z
    .locals 27

    const/16 v16, 0x0

    const/4 v5, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v5, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    if-eqz v24, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v24

    if-nez v24, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "_id"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    sget-object v24, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    aget-object v24, v24, v5

    int-to-long v0, v6

    move-wide/from16 v25, v0

    invoke-static/range {v24 .. v26}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v14

    const-string v24, "DataTransfer/MmsBackupComposer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "id:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ",realUri:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v11

    :try_start_0
    sget-object v24, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    aget-object v24, v24, v5

    sget-object v25, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "m_type"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    const-string v24, "DataTransfer/MmsBackupComposer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "inbox, m_type:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v24, 0x82

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_6

    invoke-virtual {v11, v14}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v10

    check-cast v10, Lcom/google/android/mms/pdu/NotificationInd;

    new-instance v24, Lcom/google/android/mms/pdu/PduComposer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v10}, Lcom/google/android/mms/pdu/PduComposer;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/GenericPdu;)V

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/mms/pdu/PduComposer;->make(Z)[B

    move-result-object v12

    :goto_1
    if-eqz v12, :cond_1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsIndex:I

    move/from16 v25, v0

    add-int/lit8 v26, v25, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsIndex:I

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".pdu"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "read"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "msg_box"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "date"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "sim_id"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    const-string v21, "0"

    const-wide/16 v24, 0x0

    cmp-long v24, v18, v24

    if-ltz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/mediatek/telephony/SimInfoManager;->getSlotById(Landroid/content/Context;J)I

    move-result v20

    add-int/lit8 v24, v20, 0x1

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v25, v0

    aget-object v25, v25, v5

    const-string v26, "locked"

    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    invoke-interface/range {v24 .. v25}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v15, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;

    invoke-direct {v15}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;-><init>()V

    invoke-virtual {v15, v4}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setID(Ljava/lang/String;)V

    invoke-virtual {v15, v8}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setIsRead(Ljava/lang/String;)V

    invoke-virtual {v15, v9}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setMsgBox(Ljava/lang/String;)V

    invoke-virtual {v15, v3}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setDate(Ljava/lang/String;)V

    array-length v0, v12

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setSize(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setSimId(Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setIsLocked(Ljava/lang/String;)V

    new-instance v22, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;-><init>(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Lcom/mediatek/datatransfer/modules/MmsBackupComposer$1;)V

    move-object/from16 v0, v22

    iput-object v12, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->pduMid:[B

    move-object/from16 v0, v22

    iput-object v4, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->fileName:Ljava/lang/String;

    move-object/from16 v0, v22

    iput-object v15, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->record:Lcom/mediatek/datatransfer/modules/MmsXmlInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mTempPduList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsIndex:I

    move/from16 v24, v0

    rem-int/lit8 v24, v24, 0x5

    if-eqz v24, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsIndex:I

    move/from16 v24, v0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->getCount()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-eqz v24, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    move-object/from16 v25, v0

    monitor-enter v25
    :try_end_0
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v24, "DataTransfer/MmsBackupComposer"

    const-string v26, "Mms: wait for WriteFileThread:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    monitor-exit v25
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mTempPduList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    new-instance v24, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;-><init>(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Lcom/mediatek/datatransfer/modules/MmsBackupComposer$1;)V

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Thread;->start()V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->isAfterLast()Z

    move-result v24

    if-nez v24, :cond_4

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mTempPduList:Ljava/util/ArrayList;
    :try_end_3
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_4
    const/16 v16, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    move-object/from16 v24, v0

    aget-object v24, v24, v5

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    :cond_5
    const-string v24, "DataTransfer/MmsBackupComposer"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "implementComposeOneEntity:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v16

    :cond_6
    const/16 v24, 0x84

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_7

    const/16 v24, 0x1

    :try_start_4
    move/from16 v0, v24

    invoke-virtual {v11, v14, v0}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/RetrieveConf;

    new-instance v24, Lcom/google/android/mms/pdu/PduComposer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v13}, Lcom/google/android/mms/pdu/PduComposer;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/GenericPdu;)V

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/mms/pdu/PduComposer;->make(Z)[B

    move-result-object v12

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v11, v14}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v17

    check-cast v17, Lcom/google/android/mms/pdu/SendReq;

    new-instance v24, Lcom/google/android/mms/pdu/PduComposer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/mms/pdu/PduComposer;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/GenericPdu;)V

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/mms/pdu/PduComposer;->make()[B
    :try_end_4
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v12

    goto/16 :goto_1

    :catchall_0
    move-exception v24

    :try_start_5
    monitor-exit v25
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v24
    :try_end_6
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v24

    goto :goto_3

    :catchall_1
    move-exception v24

    throw v24

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :catch_1
    move-exception v24

    goto :goto_3

    :catch_2
    move-exception v24

    goto/16 :goto_2
.end method

.method public init()Z
    .locals 9

    const/4 v2, 0x0

    const/4 v7, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mTempPduList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    :goto_0
    sget-object v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    array-length v0, v0

    if-ge v6, v0, :cond_2

    sget-object v0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    aget-object v0, v0, v6

    sget-object v1, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    if-ne v0, v1, :cond_1

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    aget-object v1, v1, v6

    const-string v3, "m_type <> ? AND m_type <> ?"

    sget-object v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->MMS_EXCLUDE_TYPE:[Ljava/lang/String;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v8, v6

    :goto_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    aget-object v0, v0, v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    aget-object v0, v0, v6

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsUriArray:[Landroid/net/Uri;

    aget-object v1, v1, v6

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v8, v6

    goto :goto_1

    :cond_2
    const-string v0, "DataTransfer/MmsBackupComposer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v7
.end method

.method public isAfterLast()Z
    .locals 8

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v4, 0x0

    :cond_0
    const-string v5, "DataTransfer/MmsBackupComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isAfterLast():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onEnd()V
    .locals 9

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mPduList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    const-string v6, "DataTransfer/MmsBackupComposer"

    const-string v8, "Mms: wait for WriteFileThread:"

    invoke-static {v6, v8}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mLock:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->endCompose()Z

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/Composer;->getComposed()I

    move-result v6

    if-lez v6, :cond_1

    if-eqz v5, :cond_1

    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "mms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "msg_box.xml"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->writeToFile(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mMmsCursorArray:[Landroid/database/Cursor;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    :catch_0
    move-exception v2

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v6, v2}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    goto :goto_1

    :cond_3
    return-void

    :catch_1
    move-exception v6

    goto :goto_0
.end method

.method public onStart()V
    .locals 8

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->getCount()I

    move-result v6

    if-lez v6, :cond_1

    new-instance v6, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    invoke-direct {v6}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;-><init>()V

    iput-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->startCompose()Z

    :cond_0
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "mms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
