.class Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;
.super Ljava/lang/Thread;
.source "MmsRestoreComposer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MmsRestoreThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;
    .param p2    # Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;-><init>(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v3, 0x0

    :goto_0
    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-static {v10}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-static {v10}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v3, v10, :cond_c

    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-static {v10}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;

    iget-object v4, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgUri:Landroid/net/Uri;

    iget-object v5, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v10, v10, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v6

    iget-object v7, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mRetrieveConf:Lcom/google/android/mms/pdu/RetrieveConf;

    iget-object v2, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mIndConf:Lcom/google/android/mms/pdu/NotificationInd;

    iget-object v8, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mSendConf:Lcom/google/android/mms/pdu/SendReq;

    if-nez v7, :cond_0

    if-nez v2, :cond_0

    if-eqz v8, :cond_b

    :cond_0
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: MmsRestoreThread parse finish"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x0

    :try_start_0
    sget-object v10, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    if-ne v4, v10, :cond_5

    if-nez v7, :cond_1

    if-eqz v2, :cond_4

    :cond_1
    if-eqz v7, :cond_3

    :goto_1
    const/4 v10, 0x1

    invoke-virtual {v6, v7, v4, v10, v5}, Lcom/google/android/mms/pdu/PduPersister;->persist_ex(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZLjava/util/HashMap;)Landroid/net/Uri;

    move-result-object v9

    :goto_2
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: MmsRestoreThread persist finish"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v13, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    if-eqz v9, :cond_7

    move v10, v11

    :goto_3
    invoke-virtual {v13, v10}, Lcom/mediatek/datatransfer/modules/Composer;->increaseComposed(Z)V

    :cond_2
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    move-object v7, v2

    goto :goto_1

    :cond_4
    :try_start_1
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: retrieveConf and indConf is null"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_2
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: MmsRestoreThread MmsException"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v13, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    if-eqz v9, :cond_8

    move v10, v11

    :goto_5
    invoke-virtual {v13, v10}, Lcom/mediatek/datatransfer/modules/Composer;->increaseComposed(Z)V

    goto :goto_4

    :cond_5
    if-eqz v8, :cond_6

    :try_start_3
    invoke-virtual {v6, v8, v4, v5}, Lcom/google/android/mms/pdu/PduPersister;->persist_ex(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;Ljava/util/HashMap;)Landroid/net/Uri;

    move-result-object v9

    goto :goto_2

    :cond_6
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: sendConf is null"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/mms/MmsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v1

    :try_start_4
    const-string v10, "DataTransfer/MmsRestoreComposer"

    const-string v13, "Mms: MmsRestoreThread Exception"

    invoke-static {v10, v13}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v13, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    if-eqz v9, :cond_9

    move v10, v11

    :goto_6
    invoke-virtual {v13, v10}, Lcom/mediatek/datatransfer/modules/Composer;->increaseComposed(Z)V

    goto :goto_4

    :cond_7
    move v10, v12

    goto :goto_3

    :cond_8
    move v10, v12

    goto :goto_5

    :cond_9
    move v10, v12

    goto :goto_6

    :catchall_0
    move-exception v10

    iget-object v13, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    if-eqz v9, :cond_a

    :goto_7
    invoke-virtual {v13, v11}, Lcom/mediatek/datatransfer/modules/Composer;->increaseComposed(Z)V

    throw v10

    :cond_a
    move v11, v12

    goto :goto_7

    :cond_b
    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v10, v10, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v10, v10, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    new-instance v13, Ljava/io/IOException;

    invoke-direct {v13}, Ljava/io/IOException;-><init>()V

    invoke-interface {v10, v13}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    goto :goto_4

    :cond_c
    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-static {v10}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$300(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    :try_start_5
    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    const/4 v12, 0x0

    invoke-static {v10, v12}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$202(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    invoke-static {v10}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->access$300(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v11

    return-void

    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v10
.end method
