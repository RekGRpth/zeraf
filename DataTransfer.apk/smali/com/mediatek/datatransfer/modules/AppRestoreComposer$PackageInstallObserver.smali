.class Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "AppRestoreComposer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/modules/AppRestoreComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field private finished:Z

.field private result:I

.field final synthetic this$0:Lcom/mediatek/datatransfer/modules/AppRestoreComposer;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/modules/AppRestoreComposer;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->this$0:Lcom/mediatek/datatransfer/modules/AppRestoreComposer;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->finished:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->finished:Z

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;

    iget v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->result:I

    return v0
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->this$0:Lcom/mediatek/datatransfer/modules/AppRestoreComposer;

    invoke-static {v0}, Lcom/mediatek/datatransfer/modules/AppRestoreComposer;->access$200(Lcom/mediatek/datatransfer/modules/AppRestoreComposer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->finished:Z

    iput p2, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->result:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/AppRestoreComposer$PackageInstallObserver;->this$0:Lcom/mediatek/datatransfer/modules/AppRestoreComposer;

    invoke-static {v0}, Lcom/mediatek/datatransfer/modules/AppRestoreComposer;->access$200(Lcom/mediatek/datatransfer/modules/AppRestoreComposer;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
