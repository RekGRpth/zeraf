.class public Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;
.super Ljava/lang/Object;
.source "NoteBookXmlInfo.java"


# static fields
.field public static final CREATED:Ljava/lang/String; = "created"

.field public static final MODIFIED:Ljava/lang/String; = "modified"

.field public static final NOTE:Ljava/lang/String; = "note"

.field public static final NOTEGROUP:Ljava/lang/String; = "notegroup"

.field public static final RECORD:Ljava/lang/String; = "record"

.field public static final ROOT:Ljava/lang/String; = "NoteBook"

.field public static final TITLE:Ljava/lang/String; = "title"


# instance fields
.field private mCreated:Ljava/lang/String;

.field private mModified:Ljava/lang/String;

.field private mNote:Ljava/lang/String;

.field private mNoteGroup:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->setNote(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->setCreated(Ljava/lang/String;)V

    invoke-virtual {p0, p4}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->setModified(Ljava/lang/String;)V

    invoke-virtual {p0, p5}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->setNoteGroup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getCreated()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mCreated:Ljava/lang/String;

    return-object v0
.end method

.method public final getModified()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mModified:Ljava/lang/String;

    return-object v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mNote:Ljava/lang/String;

    return-object v0
.end method

.method public final getNoteGroup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mNoteGroup:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final setCreated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mCreated:Ljava/lang/String;

    return-void
.end method

.method public final setModified(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mModified:Ljava/lang/String;

    return-void
.end method

.method public final setNote(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mNote:Ljava/lang/String;

    return-void
.end method

.method public final setNoteGroup(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mNoteGroup:Ljava/lang/String;

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;->mTitle:Ljava/lang/String;

    return-void
.end method
